### Java Purity Checker (ExtendJ extension) ###

This project constructs a ExtendJ extension that verifies that a Java program is sufficiently 
sideeffect free (Pure) based on its purity Annotations.  The annotation system is primarily based 
on JPure but with hints from OpenJML.
The project is built using Gradle and the JastAdd Gradle plugin. 

# Cloning the Project #

There are several ways to clone and checkout the repository one way is via SourceTree automatic cloning freature another is via a git client.

Use these commands with a git client to clone the project and build it the first time:

```
git clone --recursive git@bitbucket.org:jastadd/puritytest-exjobb-extendjextension.git 
./gradlew no
```
Note the exact way to run gradle differs for each OS platform 

Make sure to include the --recursive in the clone command to get the ExtendJ submodule.
If you forgot the --recursive flag, don't worry, just go into the newly cloned project and run 
these commands:

```
git submodule init
git submodule update --depth 1

```

That should download the ExtendJ git repository into the local directory ExtendJ.

# Build and Run #
If you have Gradle installed you can issue the following commands to build the purity checker tool:

```
gradle -jar  
```

If you don't have Gradle installed you can use the gradlew.bat (on Windows) or gradlew (Unix) script instead.The gradlew scripts are wrapper scripts that will download a specific version of Gradle locally and run it. 

Using the script you can run the following commands in a command prompt/terminal :

```
.\gradlew jar 
to build the tool but not run the tests or 
.\gradlew
to run both or 
.\gradlew test
to only run the tests
```


#Tester#

The gradlew script automatically runs the tester after completing the build unless other specified. The tester can otherwise be run without rebuild using  
```
.\gradlew test
```
and 
```
.\gradlew test callTest
```

The second construction will run the test named callTest. A test is a folder with .java and .properties files to use for the analysis located under the relative path "testfiles\PurityTests\". 


The tool can be completely run from command line without using gradlew. For example run the following command to run the test callTest that checks that only compatiable annotated methods may be call from variously annotated methods.  
```
java -jar puritytest-exjobb-extendjextension.jar testfiles/PurityTests/callTest/*.java SideEffect.java 

```

And to run the full test suit 

```
java -classpath puritytest-exjobb-extendjextension.jar tests.TestPurity 
```

# Running the tool #

The purity checker is run using the following command
```
java -jar puritytest-exjobb-extendjextension.jar [PurityOptions] [classpath files] SideEffect.java [User InputFiles]
```

where UserInputFiles are the input files that needs to be analysed t.ex. 
```
Inputfolder/*.java
```
This construction assumes that the SideEffect.java file is seperate and thus needed to be specially placed on the inputpath. The order in which SideEffect.java is loaded is not importaint and if the input to the tool has been generated with a purity annotating version of JastAdd the command would by 

```
java -jar puritytest-exjobb-extendjextension.jar [PurityOptions] [classpath files] PathToAST\*.java
```

Please note that current verisions of the tool needs to be told in which package to find the SideEffect.java if its not in the default package "AST". This is done in the configuration file see instructions under the section about the configuration file "ExtJPure.properties". 
 
A modifierad version of JastAdd was developed in parallell with this tool to be used for providing purity checking for JastAdd program. The programmers would need to recompile with the modified JastAdd verision. The create annotations of all external libraries as well as manually updating any methods written in plain Java since the JastAdd tool is tweeked for inserting correct annotations only for RAG constructs. Consequently for large existing programs such as ExtendJ, JastAdd etc a large quantity of methods will lack nessessary annotations. In the next section the different options to address this is listed. 


## Options and Usage ##

The typical usage is with a command on the form 

```
Java -jar puritytest-exjobb-extendjextension.jar SideEffect.java Inputfolder/*.java

```

this will check all the code in the Inputfolder and use that source code and the facts provided in files with previously determined facts about external library annotations to determine that the code satisfy the conditions imposed by the annotations. This external facts are provided by default in files named (ExtJPureTrusted1.properties,ExtJPureTrusted2.properties,,,ExtJPureTrustedN.properties). 
Note that the class path contains SideEffect.java which contains the annotations. The annotations must be on the path otherwise the tool won't identify the annotations correctly.
For best result all needed classfile and java code should be on the path since only the can all method calls be tracked correctly instead of a best effort.
Result are also of less quality if the Java code is not fully valid even if the tool might be able to analyse anyway.

A more specific example is:
```
java -jar puritytest-exjobb-extendjextension.jar -config testfiles/PurityTests/ZJpureSimTest2/ExtJPure.properties  testfiles/PurityTests/ZJpureSimTest2/*.java SideEffect.java
```


The format of a general command is 

```
java [JavaOptions] -jar puritytest-exjobb-extendjextension.jar [Purity Options] [ExtendJ Compiler Options] [input files- java]
```

where [Purity Options] is a combination of the following flags:

### Help info: ###

-h : Show information about the usage of the tool.

-help : Show information about the usage of the tool.

### Configuration ### 

-config : Change the used configuration (ExtJPure.properties) to another provided file. Must be the first object if used.

### Testing/Debug: ###

-step : Pause after each analyzed file. 

-debug: Extensive Debug information and not just the warnings are provided.


### Output Options:  ###
If no output option is provided the default output is specified in the configuration file which may any number of the options (to terminal,to one file per java source,to a full program output file). 

These options override these settings if provided:  

-outF : Generate an single output file for the whole input set with all the program output. This 
option can be good to avoid getting the output spead out over several files.

There are options to only output in a specifical way. They can not be combined: 
-noP : No Printing to file. This flag overrides the information in the configuration file and always only print to console.

-noScreen : No printing to console. This flag overrides the information in the configuration file and always only print to file. 

### Generate or load required facts about external methods: ###

-list : List all methods needed to be annotated and store in a property file. Default is to store any external annotation in (ExtJPureTrustedX.properties) and any missing in the analysed source code in (ExtJPureNeededX.properties). The detection of needed methods is not perfect and several runs can be needed if generating the annotations missing in big projected in the current version since only searching one call level from previous annotated code. 

-listF [File] ; -list with specified file.
 
-trusted : Annotations that should be taken accepted without verification. Use a previous result 
for a standard library and thus don't have to have the source code annotated. Default is to load 
ExtJPureTrusted.properties.

-trustedF [File] : - trusted with specified file.

For ExtendJ commands please refer to extendJ 

## Configuration in the property file ##
Most settings can be given from the Configuration file
This is a file named ExtJPure.properties. If its not present it will be generated with the following default settings

```
#!java
annotation_package=lang.ast <-- !where to find SideEffect.java 
jpure_annotation_package=jpure.annotations
genericsDifferenciation=1 <-- !Changes precision used for the library properties.
externalConstructorsPure=1 <-- !constructors dont cause side effects)
step=0 <-- ! one file at a time
debug=0 <-- !print debug information
outScreen=1 <-- ! write to terminal /console
outFile=0 <-- !write to file
oneFile=0 <-- !write one output file for all input files
trusted0=ExtJPureTrusted0.properties
```

The settings are the package that own the purity annotations. By default the annotations are defined in the file SideEffect.java in lang.ast. 
externalConstructorsPure is a setting that change the behaviour of the checker to assume that all unknown constructors are pure. Most constructors are after all without any interesting side effects and the focus is on finding side effects in the analysed project.
The step,debug,outScreen,outFile,oneFile options changes the output behaviour. 

The trusted0 flag gives the name of the file with facts about external library methods that should accepted. Its format is the following

```
java.lang.Class<?\ extends\ List>.getName()=Pure
java.lang.String.toString()=Pure 
java.util.list.add(int)=Local
java.util.HashMap<java.lang.String,\ java.lang.String>.clone()=Pure
Inheritance_2.Parent.f()=Pure
java.io.PrintStream.println(java.lang.String)=Pure
java.lang.System.arraycopy(java.lang.Object,\ int,\ java.lang.Object,\ int,\ int)=Local-false-Local--Local--
lang.ast.List<lang.ast.Function>.add(lang.ast.Function)=Local,FreshIf
java.lang.FunctionList.add(java.lang.Function) = Local,FreshIf-FreshIf-

```


There are 2 forms :
1. In the simple case the format is (MethodSignature=level of purity)
   Information is being infered on this form with the assumptions that the parameters are all of the same level of purity as the method and the freshness of the return
   object depends only on the methods annotation.

2. In the complex form the format is MethodSignature=Method annotation(,Method annotation)*-(Parameter1 annotation(,Parameter1 annotation)*)-...)
   This construction allows for extra information to be specified. Annotations are first provided for the method itself separated by ",". "-" is the seperation for moving the parameters annotations first for the first parameter and the for the others.  

The annotations are Fresh,Pure,Local,FreshIf,NonFresh,Ignore.
   A FreshIf meaning same "Freshness" as caller is automatically infered determined in the situation that the method returns only the "this" object. The returned object is only "fresh" if all FreshIfs are satisfied.
 
Note that the following optimization is possible when writing manually. 
```
java.lang.Class<?\ extends\ ASTNode>.getName()=Pure
java.lang.Class<?\ extends\ Assign>.getName()=Pure
java.lang.Class<?\ extends\ Return>.getName()=Pure
java.lang.Class<?\ extends\ Great>.getName()=Pure
```

Can be compacted to 

```
java.lang.Class<Object>.getName()=Pure
```
Due to special rules used in the tool. If a specified example exist it will of course override this general variant. 

 
The external library methods suggested purity annotations is generated by the tool command  

```
java -jar puritytest-exjobb-extendjextension.jar -list [ExtendJ Compiler Options] SideEffects.java [input files- java]
```

If there was an methods that needed to be added by the tool the get placed in a file named
```
ExtJPureTrustedN.properties
```
where N the next number such that the file didn't previously exist. 
It will then contain the methods and the suggested annotations. 
The suggested annotation is only suggestions and must be manually verified otherwise the result from testing the code will be erronius.  

**This functionally is only for called libary code an any analysized method that has any annotation will not be noted in the generated** 
.    

# How to test an existing program using this checker #

* If it a Java program you want to test simply add the annotations that you want to check. Once the annotations are placed run the tool , first to generate external library annotations using option "-list" and then a second time for generating the warnings. 

* If it a JastAdd generated Java program then you can use the JastAdd verision that generates purity annotations to generate annotations for all you attributes leaving you to only update you plain java thats called by the attributes or which you also want to test. Once the annotations are placed run the tool , first to generate external library annotations using option "-list" and then a second time for generating the warnings. 


# How to annotate the java code #
The tool uses 8 annotations but some of them has several different usages. The annotation for attributs in case of testing an JastAdd project is generated with a specific verision of JastAdd. The JastAdd Purity Annotator which is available here:
 ```
https://bitbucket.org/jastadd/puritytest-exjobb
```
Follow the instruction for that repository to construct the annotator.

The annotations works on the consept of locality of object and object freshness.
The locality of an object is all its fields and the locality of objects assigned to Local fields. The fields of the referenced objects effectively become part of the same state. This is important in specifying the degree of freshness of an object or lack thereof that is required. It determines which locations across different object that are also fresh when an object is created without having to check interprocedurly.   

A object is Fresh if its locality can be changed without any risk of side effects. This applies to object that are newly created and didn't exist before the method since these can't influence anything until they are modified after leaving the method. Immutable object and primitive values is always in a fresh state since they don't have any fields that can be modified and thus no locality. Applies only for deep immutability meaning the object itself is immutable but also the objects or values for its fields. The checker only trusts that the Integral types and String is sufficiently immutable in the Java language.  
 
### Annotations ###

1.Secret(group=String) on fields:

Denote protected state that can only be accessed by one pure method 
with the same group e.i Secret(group="cache") can only be read by a 
method annotated Pure(group="cache"). 

The access can only be for the same object and the can be read cross several object. A objects secret state has to be access with a method on that object. Several fields can be annotated with the same group.
  
2.Pure or Pure(group=String) on methods:

Denote a method that is sideeffect free. In the case of JastAdd this
would apply to the method corrisponding to attribut equations. An syn or inh attribute thats not an nta should satisfy the Pure annotation requirements. 

The unparameterised annotation doesn't use any secret state and the parameterised variant does use a secret state group.


3. Fresh on both methods, parameter and field

The Fresh annotation deals both with purity and if an object is newly created state or not.  

* Fresh or Fresh(group=String) on methods:

Fresh on a method mean that the method has to return an newly created object and also be Pure in other regards if no other annotations are provided.
A Fresh object is a object created by a constructor that is Pure otherwise the new objects locality may not be fresh.
  
* Fresh on parameters:

Fresh on a parameter means that the method is free to assume that the object recived is a fresh object in the calling context and may be return from the method as a fresh object.

* Fresh on field:

Fresh on a field means that the field is supposed to point to a fresh object that could be considered as fresh at all times.


4.FreshIf on methods, parameters and fields

Determines that objects inside a container (Map) must also be fresh apart from the container itself in order to preserve Freshness.

* FreshIf on methods:

The return is fresh if the caller is fresh.

* FreshIf on parameter:

The return is fresh if the parameter is fresh.

* FreshIf on field:

The inner side of the container must be fresh for the container object.



5.Local on methods, parameters and fields

* Local on methods:
The method induces a change in the caller and the caller must be modifiable inform of fresh or ignored.

* Local on parameters:

The parameter must be allowed to be modified.

* Local on fields:

Part of the same locality and needs to be fresh if owner object is fresh and if not fresh then owner object is not fresh.



6.Ignore on methods, parameters and fields
Fully ignore annotated element from analysis. Methods with this annotation Never triggers any warnings for the inside code. Use with caution. Combination with other annotations will be considered when called.



7. NonFresh on Methods, parameters and fields:

The returned object is strictly not Fresh. The none freshness guarantee applies to all reference type field in the locality.     
* NonFresh on parameters:

The parameter must be allowed to be modified.

* NonFresh on fields:

The field must point to a Nonfresh object at all times.
 

8.Entity on Classes:

Entity is an annotation put on class that represent types that models entity values. The annotation applies to all subclasses.An method without a  fresh annotation may not return an object of Fresh or create an new object of entity type. This satisfy the ASTNode requirement of that new node of the AST may only be created in an equation for an nta attribute. 

Methods returning an Entity type object are default treated as if annotated
NonFresh.



Analysis field assignments without function calls including chained function calls. Only 
concrete classes work.


### How do I get set up? ###

Compiled using java 1.8.112 built using the same way as the base extension. Can also be built using older java verisions as 7.  

*Building with gradlew will automatically run the test suit. *

1. Run gradlew -test to only run tests without rebuilding


### How to use the annotations? ###

A method is a canidate for Local if * it only changes the containing object or objects pointed to 
by its fields or parameters annotated local. 
In other respects it side effect free and doesn't assign anything. 

A method is a canidate for Pure is a side effect free method. This means it doesn't in general 
assign any fields. The Secret field are the only exception for none constructors.
A constructor can assign the associated objects fields without being none pure since it all new 
state.

A method annotate Fresh must return a newly create object and also satisfy Pure.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Author: Mikael Johnsson (mr.mikael.j@hotmail.com)
* Other community or team contact

License : Same as ExtendJ which this an extension of (See LICENSE)
import jpure.annotations.*;
import java.util.*;

public class PureMethod_1 {
    @Local public ArrayList<String> array = new ArrayList<String>();
    
    public int siggesMuskler=0;
    
    @Fresh public boolean contains(String x) {
    	if (siggesMuskler>3)
    		siggesMuskler=0;
	for(String s : array) {
	    if(s == x) {
	    	siggesMuskler=3;
		return true;
	    }
	}
	siggesMuskler=x.length();
	return false;
    }
}
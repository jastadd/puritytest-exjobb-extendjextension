testfiles\PurityTests\callTest2\callTest.java
Method is annotated Pure.
For Java method not part of attribute calculation

Purity problem in category:METHODCALL
9:new callTest(); causes side effects.
For method call not externally specified.The method call callTest.callTest() is impure.

in testfiles\PurityTests\callTest2\callTest.java:callTest

1 warnings generated

import lang.ast.SideEffect.*;
public class Override02{
	@Fresh public int freshMethod(){return 1;}
	@Pure public int pureMethod(){return 1;}
	@Pure public int pureMethod2(){return 1;}
	@Pure public int pureMethod3(){return 1;}
}

public class inheritor01 extends Override02{
	// Wrong Due to Annotations!!!!
	public int freshMethod(){return 1;}
	// Wrong Due to Annotations!!!!
	public int pureMethod(){return 1;}
}

public class inheritor02 extends inheritor01{
	// Correct Due consistency with inheritor01
	public int freshMethod(){return 1;}
	public int pureMethod(){return 1;}
	
	//Wrong!!!
	@Local public int pureMethod2(){return 1;}
	// Okey!!!
	@Pure public int pureMethod3(){return 2;}
}
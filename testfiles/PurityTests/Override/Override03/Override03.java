import lang.ast.SideEffect.*;
public class Override02{
	public int pureMethod(){return 1;}
	public int pureMethod2(){return 1;}
	public int pureMethod3(){return 1;}
}

public class inheritor01 extends Override02{
	@Pure public int pureMethod(){return 1;}
	@Pure public int pureMethod2(){return 1;}
	@Pure public int pureMethod3(){return 1;}
}

public class inheritor02 extends inheritor01{
	@Pure public int pureMethod(){return 1;}
	@Pure public int pureMethod2(){return 1;}
	@Pure public int pureMethod3(){return 1;}
}
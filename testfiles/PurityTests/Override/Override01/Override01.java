import lang.ast.SideEffect.*;
public class Override01{
	@Fresh public int freshMethod(){return 1;}
	@Pure public int pureMethod(){return 1;}
}

public class inheritor01 extends Override01{
	// Wrong Due to Annotations!!!!
	public int freshMethod(){return 1;}
	public int pureMethod(){return 1;}
}
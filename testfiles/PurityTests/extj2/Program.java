/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package org.extendj.ast;
import java.util.HashSet;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;
import java.util.Set;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import beaver.*;
import java.util.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.jastadd.util.*;
import java.util.zip.*;
import java.io.*;
import org.jastadd.util.PrettyPrintable;
import org.jastadd.util.PrettyPrinter;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\grammar\\Java.ast:1
 * @production Program : {@link ASTNode} ::= <span class="component">{@link CompilationUnit}*</span>;

 */
public class Program extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect ClassPath
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:56
   */
  protected BytecodeReader bytecodeReader = defaultBytecodeReader();
  /**
   * @aspect ClassPath
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:58
   */
  public void initBytecodeReader(BytecodeReader r) {
    bytecodeReader = r;
  }
  /**
   * @aspect ClassPath
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:62
   */
  public static BytecodeReader defaultBytecodeReader() {
    return new BytecodeReader() {
      @Override
      public CompilationUnit read(InputStream is, String fullName, Program p)
          throws FileNotFoundException, IOException {
        return new BytecodeParser(is, fullName).parse(null, null, p);
      }
    };
  }
  /**
   * @aspect ClassPath
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:72
   */
  protected JavaParser javaParser = defaultJavaParser();
  /**
   * @aspect ClassPath
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:74
   */
  public void initJavaParser(JavaParser p) {
    javaParser = p;
  }
  /**
   * @aspect ClassPath
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:78
   */
  public static JavaParser defaultJavaParser() {
    return new JavaParser() {
      @Override
      public CompilationUnit parse(InputStream is, String fileName)
          throws IOException, beaver.Parser.Exception {
        return new org.extendj.parser.JavaParser().parse(is, fileName);
      }
    };
  }
  /**
   * Parse the source file and add the compilation unit to the list of
   * compilation units in the program.
   * 
   * @param fileName file name of the source file
   * @return The CompilationUnit representing the source file,
   * or <code>null</code> if the source file could not be parsed
   * @aspect ClassPath
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:119
   */
  public CompilationUnit addSourceFile(String fileName) throws IOException {
    SourceFilePath pathPart = new SourceFilePath(fileName);
    CompilationUnit cu = pathPart.getCompilationUnit(this, fileName);
    if (cu != emptyCompilationUnit()) {
      classPath.addPackage(cu.packageName());
      addCompilationUnit(cu);
    }
    return cu;
  }
  /**
   * Creates an iterator to iterate over compilation units parsed from source files.
   * @aspect ClassPath
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:132
   */
  public Iterator<CompilationUnit> compilationUnitIterator() {
    return new Iterator<CompilationUnit>() {
      int index = 0;

      @Override
      public boolean hasNext() {
        return index < getNumCompilationUnit();
      }

      @Override
      public CompilationUnit next() {
        if (getNumCompilationUnit() == index) {
          throw new java.util.NoSuchElementException();
        }
        return getCompilationUnit(index++);
      }

      @Override
      public void remove() {
        throw new UnsupportedOperationException();
      }
    };
  }
  /**
   * Get the input stream for a compilation unit specified using a canonical
   * name. This is used by the bytecode reader to load nested types.
   * @param name The canonical name of the compilation unit.
   * @aspect ClassPath
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:161
   */
  public InputStream getInputStream(String name) {
    return classPath.getInputStream(name);
  }
  /**
   * @aspect ClassPath
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:165
   */
  private final ClassPath classPath = new ClassPath(this);
  /**
   * @return <code>true</code> if there is a package with the given name on
   * the classpath
   * @aspect ClassPath
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:431
   */
  public boolean isPackage(String packageName) {
    return classPath.isPackage(packageName);
  }
  /**
   * Add a path part to the library class path.
   * @aspect ClassPath
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:454
   */
  public void addClassPath(PathPart pathPart) {
    classPath.addClassPath(pathPart);
  }
  /**
   * Add a path part to the user class path.
   * @aspect ClassPath
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:461
   */
  public void addSourcePath(PathPart pathPart) {
    classPath.addSourcePath(pathPart);
  }
  /**
   * @aspect FrontendMain
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\FrontendMain.jrag:37
   */
  public long javaParseTime;
  /**
   * @aspect FrontendMain
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\FrontendMain.jrag:38
   */
  public long bytecodeParseTime;
  /**
   * @aspect FrontendMain
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\FrontendMain.jrag:39
   */
  public long codeGenTime;
  /**
   * @aspect FrontendMain
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\FrontendMain.jrag:40
   */
  public long errorCheckTime;
  /**
   * @aspect FrontendMain
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\FrontendMain.jrag:41
   */
  public int numJavaFiles;
  /**
   * @aspect FrontendMain
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\FrontendMain.jrag:42
   */
  public int numClassFiles;
  /**
   * Reset the profile statistics.
   * @aspect FrontendMain
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\FrontendMain.jrag:47
   */
  public void resetStatistics() {
    javaParseTime = 0;
    bytecodeParseTime = 0;
    codeGenTime = 0;
    errorCheckTime = 0;
    numJavaFiles = 0;
    numClassFiles = 0;
  }
  /**
   * @aspect FrontendMain
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\FrontendMain.jrag:56
   */
  public void printStatistics(PrintStream out) {
    out.println("javaParseTime: " + javaParseTime);
    out.println("numJavaFiles: " + numJavaFiles);
    out.println("bytecodeParseTime: " + javaParseTime);
    out.println("numClassFiles: " + numClassFiles);
    out.println("errorCheckTime: " + errorCheckTime);
    out.println("codeGenTime: " + codeGenTime);
  }
  /**
   * Returns a robust iterator that can be iterated while the colleciton is updated.
   * @aspect LibraryCompilationUnits
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LibCompilationUnits.jadd:35
   */
  public Iterator<CompilationUnit> libraryCompilationUnitIterator() {
    return libraryCompilationUnitSet.iterator();
  }
  /**
   * @aspect LookupFullyQualifiedTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:133
   */
  public int classFileReadTime;
  /**
   * Extra cache for source type lookups. This cache is important in order to
   * make all source types shadow library types with matching names, even when
   * the source type lives in a compilation unit with a different simple name.
   * @aspect LookupFullyQualifiedTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:162
   */
  private final Map<String, TypeDecl> sourceTypeMap = new HashMap<String, TypeDecl>();
  /**
   * @aspect LookupFullyQualifiedTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:164
   */
  private boolean sourceTypeMapInitialized = false;
  /**
   * Lookup a type among source classes.
   * 
   * <p>Invoking this method may cause more than just the specified type to be
   * loaded, for example if there exists other types in the same source file,
   * the additional types are also loaded and cached for the next lookup.
   * 
   * <p>This method is not an attribute due to the necessary side-effects
   * caused by loading and caching of extra types.
   * @aspect LookupFullyQualifiedTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:176
   */
  protected TypeDecl lookupSourceType(String packageName, String typeName) {
    String fullName = packageName.equals("") ? typeName : packageName + "." + typeName;

    if (!sourceTypeMapInitialized) {
      initializeSourceTypeMap();
      sourceTypeMapInitialized = true;
    }

    if (sourceTypeMap.containsKey(fullName)) {
      return sourceTypeMap.get(fullName);
    } else {
      sourceTypeMap.put(fullName, unknownType());
    }

    // Look for a matching library type.
    return unknownType();
  }
  /**
   * Initialize source types in the source type map.  This puts all the types provided by
   * Program.addSourceFile() in a map for lookup by Program.lookupSourceType.
   * @aspect LookupFullyQualifiedTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:198
   */
  private void initializeSourceTypeMap() {
    // Initialize source type map with the compilation units supplied by Program.addSourceFile.
    for (int i = 0; i < getNumCompilationUnit(); i++) {
      CompilationUnit unit = getCompilationUnit(i);
      for (int j = 0; j < unit.getNumTypeDecl(); j++) {
        TypeDecl type = unit.getTypeDecl(j);
        sourceTypeMap.put(type.fullName(), type);
      }
    }
  }
  /**
   * Extra cache for library type lookups. This cache does not have a big
   * effect on performance due to the caching of the parameterized
   * lookupLibraryType attribute. The cache is needed to be able to track library types
   * that are declared in compilation units with a different simple name than the type itself.
   * @aspect LookupFullyQualifiedTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:215
   */
  private final Map<String, TypeDecl> libraryTypeMap = new HashMap<String, TypeDecl>();
  /**
   * @aspect LookupFullyQualifiedTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:217
   */
  private final Set<CompilationUnit> libraryCompilationUnitSet =
      new RobustSet<CompilationUnit>(new HashSet<CompilationUnit>());
  /**
   * @aspect LookupFullyQualifiedTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:220
   */
  private boolean libraryTypeMapInitialized = false;
  /**
   * Lookup a type among library classes. The lookup includes Jar and source files.
   * 
   * <p>Invoking this method may cause more than just the specified type to be loaded, for
   * example if there exists other types in the same source file, the additional
   * types are also loaded and cached for the next lookup.
   * 
   * <p>This method is not an attribute due to the necessary side-effects caused
   * by loading and caching of extra types.
   * @aspect LookupFullyQualifiedTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:232
   */
  protected TypeDecl lookupLibraryType(String packageName, String typeName) {
    String fullName = packageName.isEmpty() ? typeName : packageName + "." + typeName;

    if (!libraryTypeMapInitialized) {
      initializeLibraryTypeMap();
      libraryTypeMapInitialized = true;
    }

    if (libraryTypeMap.containsKey(fullName)) {
      return libraryTypeMap.get(fullName);
    }

    // Lookup the type in the library class path.
    CompilationUnit libraryUnit = getLibCompilationUnit(fullName);

    // Store the compilation unit in a set for later introspection of loaded compilation units.
    libraryCompilationUnitSet.add(libraryUnit);

    // Add all types from the compilation unit in the library type map so that we can find them on
    // the next type lookup. If we don't do this lookup might incorrectly miss a type that is not
    // declared in a Java source file with a matching name.
    for (int j = 0; j < libraryUnit.getNumTypeDecl(); j++) {
      TypeDecl type = libraryUnit.getTypeDecl(j);
      if (!libraryTypeMap.containsKey(type.fullName())) {
        libraryTypeMap.put(type.fullName(), type);
      }
    }

    if (libraryTypeMap.containsKey(fullName)) {
      return libraryTypeMap.get(fullName);
    } else {
      libraryTypeMap.put(fullName, unknownType());
      return unknownType();
    }
  }
  /** Initialize primitive types in the library type map.  
   * @aspect LookupFullyQualifiedTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:269
   */
  private void initializeLibraryTypeMap() {
      PrimitiveCompilationUnit unit = getPrimitiveCompilationUnit();
      libraryTypeMap.put(PRIMITIVE_PACKAGE_NAME + ".boolean", unit.typeBoolean());
      libraryTypeMap.put(PRIMITIVE_PACKAGE_NAME + ".byte", unit.typeByte());
      libraryTypeMap.put(PRIMITIVE_PACKAGE_NAME + ".short", unit.typeShort());
      libraryTypeMap.put(PRIMITIVE_PACKAGE_NAME + ".char", unit.typeChar());
      libraryTypeMap.put(PRIMITIVE_PACKAGE_NAME + ".int", unit.typeInt());
      libraryTypeMap.put(PRIMITIVE_PACKAGE_NAME + ".long", unit.typeLong());
      libraryTypeMap.put(PRIMITIVE_PACKAGE_NAME + ".float", unit.typeFloat());
      libraryTypeMap.put(PRIMITIVE_PACKAGE_NAME + ".double", unit.typeDouble());
      libraryTypeMap.put(PRIMITIVE_PACKAGE_NAME + ".null", unit.typeNull());
      libraryTypeMap.put(PRIMITIVE_PACKAGE_NAME + ".void", unit.typeVoid());
      libraryTypeMap.put(PRIMITIVE_PACKAGE_NAME + ".Unknown", unit.unknownType());
  }
  /**
   * @aspect AddOptionsToProgram
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\Options.jadd:34
   */
  public Options options = new Options();
  /**
   * @aspect AddOptionsToProgram
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\Options.jadd:36
   */
  public Options options() {
    return options;
  }
  /**
   * @aspect PrettyPrintUtil
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\PrettyPrintUtil.jrag:77
   */
  public void prettyPrint(PrettyPrinter out) {
    for (Iterator iter = compilationUnitIterator(); iter.hasNext(); ) {
      CompilationUnit cu = (CompilationUnit) iter.next();
      if (cu.fromSource()) {
        out.print(cu);
      }
    }
  }
  /**
   * @aspect GenerateClassfile
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\backend\\GenerateClassfile.jrag:33
   */
  public void generateClassfile() {
    for (Iterator iter = compilationUnitIterator(); iter.hasNext(); ) {
      CompilationUnit cu = (CompilationUnit) iter.next();
      cu.generateClassfile();
    }
  }
  /**
   * @declaredat ASTNode:1
   */
  public Program() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
    setChild(new List(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  public Program(List<CompilationUnit> p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:18
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:24
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    getCompilationUnit_String_reset();
    typeObject_reset();
    typeCloneable_reset();
    typeSerializable_reset();
    typeBoolean_reset();
    typeByte_reset();
    typeShort_reset();
    typeChar_reset();
    typeInt_reset();
    typeLong_reset();
    typeFloat_reset();
    typeDouble_reset();
    typeString_reset();
    typeVoid_reset();
    typeNull_reset();
    unknownType_reset();
    hasPackage_String_reset();
    lookupType_String_String_reset();
    getLibCompilationUnit_String_reset();
    emptyCompilationUnit_reset();
    getPrimitiveCompilationUnit_reset();
    unknownConstructor_reset();
    wildcards_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:55
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:59
   */
  @SideEffect.Fresh public Program clone() throws CloneNotSupportedException {
    Program node = (Program) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:64
   */
  @SideEffect.Fresh(group="_ASTNode") public Program copy() {
    try {
      Program node = (Program) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:83
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Program fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:93
   */
  @SideEffect.Fresh(group="_ASTNode") public Program treeCopyNoTransform() {
    Program tree = (Program) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:113
   */
  @SideEffect.Fresh(group="_ASTNode") public Program treeCopy() {
    Program tree = (Program) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:127
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the CompilationUnit list.
   * @param list The new list node to be used as the CompilationUnit list.
   * @apilevel high-level
   */
  public void setCompilationUnitList(List<CompilationUnit> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the CompilationUnit list.
   * @return Number of children in the CompilationUnit list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumCompilationUnit() {
    return getCompilationUnitList().getNumChild();
  }
  /**
   * Retrieves the number of children in the CompilationUnit list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the CompilationUnit list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumCompilationUnitNoTransform() {
    return getCompilationUnitListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the CompilationUnit list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the CompilationUnit list.
   * @apilevel high-level
   */
  @SideEffect.Pure public CompilationUnit getCompilationUnit(int i) {
    return (CompilationUnit) getCompilationUnitList().getChild(i);
  }
  /**
   * Check whether the CompilationUnit list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasCompilationUnit() {
    return getCompilationUnitList().getNumChild() != 0;
  }
  /**
   * Append an element to the CompilationUnit list.
   * @param node The element to append to the CompilationUnit list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addCompilationUnit(CompilationUnit node) {
    List<CompilationUnit> list = (parent == null) ? getCompilationUnitListNoTransform() : getCompilationUnitList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addCompilationUnitNoTransform(CompilationUnit node) {
    List<CompilationUnit> list = getCompilationUnitListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the CompilationUnit list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setCompilationUnit(CompilationUnit node, int i) {
    List<CompilationUnit> list = getCompilationUnitList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the CompilationUnit list.
   * @return The node representing the CompilationUnit list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="CompilationUnit")
  @SideEffect.Pure(group="_ASTNode") public List<CompilationUnit> getCompilationUnitList() {
    List<CompilationUnit> list = (List<CompilationUnit>) getChild(0);
    return list;
  }
  /**
   * Retrieves the CompilationUnit list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the CompilationUnit list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<CompilationUnit> getCompilationUnitListNoTransform() {
    return (List<CompilationUnit>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the CompilationUnit list without
   * triggering rewrites.
   */
  @SideEffect.Pure public CompilationUnit getCompilationUnitNoTransform(int i) {
    return (CompilationUnit) getCompilationUnitListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the CompilationUnit list.
   * @return The node representing the CompilationUnit list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<CompilationUnit> getCompilationUnits() {
    return getCompilationUnitList();
  }
  /**
   * Retrieves the CompilationUnit list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the CompilationUnit list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<CompilationUnit> getCompilationUnitsNoTransform() {
    return getCompilationUnitListNoTransform();
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void getCompilationUnit_String_reset() {
    getCompilationUnit_String_computed = new java.util.HashMap(4);
    getCompilationUnit_String_values = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="getCompilationUnit_String") protected java.util.Map getCompilationUnit_String_values;
  /** @apilevel internal */
  @SideEffect.Secret(group="getCompilationUnit_String") protected java.util.Map getCompilationUnit_String_computed;
  /**
   * Load a compilation unit from disk, selecting a class file
   * if one exists that is not older than a corresponding source
   * file, otherwise the source file is selected.
   * 
   * <p>This method is called by the LibCompilationUnit NTA.  We rely on the
   * result of this method being cached because it will return a newly parsed
   * compilation unit each time it is called.
   * 
   * @return the loaded compilation unit, or the empty compilation unit if no compilation unit was
   * found.
   * @attribute syn
   * @aspect ClassPath
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:424
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ClassPath", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:424")
  @SideEffect.Pure(group="getCompilationUnit_String") public CompilationUnit getCompilationUnit(String typeName) {
    Object _parameters = typeName;
    if (getCompilationUnit_String_computed == null) getCompilationUnit_String_computed = new java.util.HashMap(4);
    if (getCompilationUnit_String_values == null) getCompilationUnit_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (getCompilationUnit_String_values.containsKey(_parameters) && getCompilationUnit_String_computed != null
        && getCompilationUnit_String_computed.containsKey(_parameters)
        && (getCompilationUnit_String_computed.get(_parameters) == ASTState.NON_CYCLE || getCompilationUnit_String_computed.get(_parameters) == state().cycle())) {
      return (CompilationUnit) getCompilationUnit_String_values.get(_parameters);
    }
    CompilationUnit getCompilationUnit_String_value = classPath.getCompilationUnit(typeName, emptyCompilationUnit());
    if (state().inCircle()) {
      getCompilationUnit_String_values.put(_parameters, getCompilationUnit_String_value);
      getCompilationUnit_String_computed.put(_parameters, state().cycle());
    
    } else {
      getCompilationUnit_String_values.put(_parameters, getCompilationUnit_String_value);
      getCompilationUnit_String_computed.put(_parameters, ASTState.NON_CYCLE);
    
    }
    return getCompilationUnit_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void typeObject_reset() {
    typeObject_computed = null;
    typeObject_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="typeObject") protected ASTState.Cycle typeObject_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="typeObject") protected TypeDecl typeObject_value;

  /**
   * @attribute syn
   * @aspect SpecialClasses
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:37
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SpecialClasses", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:37")
  @SideEffect.Pure(group="typeObject") public TypeDecl typeObject() {
    ASTState state = state();
    if (typeObject_computed == ASTState.NON_CYCLE || typeObject_computed == state().cycle()) {
      return typeObject_value;
    }
    typeObject_value = lookupType("java.lang", "Object");
    if (state().inCircle()) {
      typeObject_computed = state().cycle();
    
    } else {
      typeObject_computed = ASTState.NON_CYCLE;
    
    }
    return typeObject_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void typeCloneable_reset() {
    typeCloneable_computed = null;
    typeCloneable_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="typeCloneable") protected ASTState.Cycle typeCloneable_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="typeCloneable") protected TypeDecl typeCloneable_value;

  /**
   * @attribute syn
   * @aspect SpecialClasses
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:38
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SpecialClasses", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:38")
  @SideEffect.Pure(group="typeCloneable") public TypeDecl typeCloneable() {
    ASTState state = state();
    if (typeCloneable_computed == ASTState.NON_CYCLE || typeCloneable_computed == state().cycle()) {
      return typeCloneable_value;
    }
    typeCloneable_value = lookupType("java.lang", "Cloneable");
    if (state().inCircle()) {
      typeCloneable_computed = state().cycle();
    
    } else {
      typeCloneable_computed = ASTState.NON_CYCLE;
    
    }
    return typeCloneable_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void typeSerializable_reset() {
    typeSerializable_computed = null;
    typeSerializable_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="typeSerializable") protected ASTState.Cycle typeSerializable_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="typeSerializable") protected TypeDecl typeSerializable_value;

  /**
   * @attribute syn
   * @aspect SpecialClasses
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:39
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SpecialClasses", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:39")
  @SideEffect.Pure(group="typeSerializable") public TypeDecl typeSerializable() {
    ASTState state = state();
    if (typeSerializable_computed == ASTState.NON_CYCLE || typeSerializable_computed == state().cycle()) {
      return typeSerializable_value;
    }
    typeSerializable_value = lookupType("java.io", "Serializable");
    if (state().inCircle()) {
      typeSerializable_computed = state().cycle();
    
    } else {
      typeSerializable_computed = ASTState.NON_CYCLE;
    
    }
    return typeSerializable_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void typeBoolean_reset() {
    typeBoolean_computed = null;
    typeBoolean_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="typeBoolean") protected ASTState.Cycle typeBoolean_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="typeBoolean") protected TypeDecl typeBoolean_value;

  /**
   * @attribute syn
   * @aspect SpecialClasses
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:45
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SpecialClasses", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:45")
  @SideEffect.Pure(group="typeBoolean") public TypeDecl typeBoolean() {
    ASTState state = state();
    if (typeBoolean_computed == ASTState.NON_CYCLE || typeBoolean_computed == state().cycle()) {
      return typeBoolean_value;
    }
    typeBoolean_value = getPrimitiveCompilationUnit().typeBoolean();
    if (state().inCircle()) {
      typeBoolean_computed = state().cycle();
    
    } else {
      typeBoolean_computed = ASTState.NON_CYCLE;
    
    }
    return typeBoolean_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void typeByte_reset() {
    typeByte_computed = null;
    typeByte_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="typeByte") protected ASTState.Cycle typeByte_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="typeByte") protected TypeDecl typeByte_value;

  /**
   * @attribute syn
   * @aspect SpecialClasses
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:46
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SpecialClasses", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:46")
  @SideEffect.Pure(group="typeByte") public TypeDecl typeByte() {
    ASTState state = state();
    if (typeByte_computed == ASTState.NON_CYCLE || typeByte_computed == state().cycle()) {
      return typeByte_value;
    }
    typeByte_value = getPrimitiveCompilationUnit().typeByte();
    if (state().inCircle()) {
      typeByte_computed = state().cycle();
    
    } else {
      typeByte_computed = ASTState.NON_CYCLE;
    
    }
    return typeByte_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void typeShort_reset() {
    typeShort_computed = null;
    typeShort_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="typeShort") protected ASTState.Cycle typeShort_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="typeShort") protected TypeDecl typeShort_value;

  /**
   * @attribute syn
   * @aspect SpecialClasses
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:47
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SpecialClasses", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:47")
  @SideEffect.Pure(group="typeShort") public TypeDecl typeShort() {
    ASTState state = state();
    if (typeShort_computed == ASTState.NON_CYCLE || typeShort_computed == state().cycle()) {
      return typeShort_value;
    }
    typeShort_value = getPrimitiveCompilationUnit().typeShort();
    if (state().inCircle()) {
      typeShort_computed = state().cycle();
    
    } else {
      typeShort_computed = ASTState.NON_CYCLE;
    
    }
    return typeShort_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void typeChar_reset() {
    typeChar_computed = null;
    typeChar_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="typeChar") protected ASTState.Cycle typeChar_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="typeChar") protected TypeDecl typeChar_value;

  /**
   * @attribute syn
   * @aspect SpecialClasses
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:48
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SpecialClasses", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:48")
  @SideEffect.Pure(group="typeChar") public TypeDecl typeChar() {
    ASTState state = state();
    if (typeChar_computed == ASTState.NON_CYCLE || typeChar_computed == state().cycle()) {
      return typeChar_value;
    }
    typeChar_value = getPrimitiveCompilationUnit().typeChar();
    if (state().inCircle()) {
      typeChar_computed = state().cycle();
    
    } else {
      typeChar_computed = ASTState.NON_CYCLE;
    
    }
    return typeChar_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void typeInt_reset() {
    typeInt_computed = null;
    typeInt_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="typeInt") protected ASTState.Cycle typeInt_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="typeInt") protected TypeDecl typeInt_value;

  /**
   * @attribute syn
   * @aspect SpecialClasses
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:49
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SpecialClasses", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:49")
  @SideEffect.Pure(group="typeInt") public TypeDecl typeInt() {
    ASTState state = state();
    if (typeInt_computed == ASTState.NON_CYCLE || typeInt_computed == state().cycle()) {
      return typeInt_value;
    }
    typeInt_value = getPrimitiveCompilationUnit().typeInt();
    if (state().inCircle()) {
      typeInt_computed = state().cycle();
    
    } else {
      typeInt_computed = ASTState.NON_CYCLE;
    
    }
    return typeInt_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void typeLong_reset() {
    typeLong_computed = null;
    typeLong_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="typeLong") protected ASTState.Cycle typeLong_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="typeLong") protected TypeDecl typeLong_value;

  /**
   * @attribute syn
   * @aspect SpecialClasses
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:50
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SpecialClasses", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:50")
  @SideEffect.Pure(group="typeLong") public TypeDecl typeLong() {
    ASTState state = state();
    if (typeLong_computed == ASTState.NON_CYCLE || typeLong_computed == state().cycle()) {
      return typeLong_value;
    }
    typeLong_value = getPrimitiveCompilationUnit().typeLong();
    if (state().inCircle()) {
      typeLong_computed = state().cycle();
    
    } else {
      typeLong_computed = ASTState.NON_CYCLE;
    
    }
    return typeLong_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void typeFloat_reset() {
    typeFloat_computed = null;
    typeFloat_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="typeFloat") protected ASTState.Cycle typeFloat_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="typeFloat") protected TypeDecl typeFloat_value;

  /**
   * @attribute syn
   * @aspect SpecialClasses
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:51
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SpecialClasses", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:51")
  @SideEffect.Pure(group="typeFloat") public TypeDecl typeFloat() {
    ASTState state = state();
    if (typeFloat_computed == ASTState.NON_CYCLE || typeFloat_computed == state().cycle()) {
      return typeFloat_value;
    }
    typeFloat_value = getPrimitiveCompilationUnit().typeFloat();
    if (state().inCircle()) {
      typeFloat_computed = state().cycle();
    
    } else {
      typeFloat_computed = ASTState.NON_CYCLE;
    
    }
    return typeFloat_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void typeDouble_reset() {
    typeDouble_computed = null;
    typeDouble_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="typeDouble") protected ASTState.Cycle typeDouble_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="typeDouble") protected TypeDecl typeDouble_value;

  /**
   * @attribute syn
   * @aspect SpecialClasses
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:52
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SpecialClasses", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:52")
  @SideEffect.Pure(group="typeDouble") public TypeDecl typeDouble() {
    ASTState state = state();
    if (typeDouble_computed == ASTState.NON_CYCLE || typeDouble_computed == state().cycle()) {
      return typeDouble_value;
    }
    typeDouble_value = getPrimitiveCompilationUnit().typeDouble();
    if (state().inCircle()) {
      typeDouble_computed = state().cycle();
    
    } else {
      typeDouble_computed = ASTState.NON_CYCLE;
    
    }
    return typeDouble_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void typeString_reset() {
    typeString_computed = null;
    typeString_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="typeString") protected ASTState.Cycle typeString_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="typeString") protected TypeDecl typeString_value;

  /**
   * @attribute syn
   * @aspect SpecialClasses
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:53
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SpecialClasses", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:53")
  @SideEffect.Pure(group="typeString") public TypeDecl typeString() {
    ASTState state = state();
    if (typeString_computed == ASTState.NON_CYCLE || typeString_computed == state().cycle()) {
      return typeString_value;
    }
    typeString_value = lookupType("java.lang", "String");
    if (state().inCircle()) {
      typeString_computed = state().cycle();
    
    } else {
      typeString_computed = ASTState.NON_CYCLE;
    
    }
    return typeString_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void typeVoid_reset() {
    typeVoid_computed = null;
    typeVoid_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="typeVoid") protected ASTState.Cycle typeVoid_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="typeVoid") protected TypeDecl typeVoid_value;

  /**
   * @attribute syn
   * @aspect SpecialClasses
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:65
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SpecialClasses", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:65")
  @SideEffect.Pure(group="typeVoid") public TypeDecl typeVoid() {
    ASTState state = state();
    if (typeVoid_computed == ASTState.NON_CYCLE || typeVoid_computed == state().cycle()) {
      return typeVoid_value;
    }
    typeVoid_value = getPrimitiveCompilationUnit().typeVoid();
    if (state().inCircle()) {
      typeVoid_computed = state().cycle();
    
    } else {
      typeVoid_computed = ASTState.NON_CYCLE;
    
    }
    return typeVoid_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void typeNull_reset() {
    typeNull_computed = null;
    typeNull_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="typeNull") protected ASTState.Cycle typeNull_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="typeNull") protected TypeDecl typeNull_value;

  /**
   * @attribute syn
   * @aspect SpecialClasses
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:68
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SpecialClasses", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:68")
  @SideEffect.Pure(group="typeNull") public TypeDecl typeNull() {
    ASTState state = state();
    if (typeNull_computed == ASTState.NON_CYCLE || typeNull_computed == state().cycle()) {
      return typeNull_value;
    }
    typeNull_value = getPrimitiveCompilationUnit().typeNull();
    if (state().inCircle()) {
      typeNull_computed = state().cycle();
    
    } else {
      typeNull_computed = ASTState.NON_CYCLE;
    
    }
    return typeNull_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void unknownType_reset() {
    unknownType_computed = null;
    unknownType_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="unknownType") protected ASTState.Cycle unknownType_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="unknownType") protected TypeDecl unknownType_value;

  /**
   * @attribute syn
   * @aspect SpecialClasses
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:71
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SpecialClasses", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:71")
  @SideEffect.Pure(group="unknownType") public TypeDecl unknownType() {
    ASTState state = state();
    if (unknownType_computed == ASTState.NON_CYCLE || unknownType_computed == state().cycle()) {
      return unknownType_value;
    }
    unknownType_value = getPrimitiveCompilationUnit().unknownType();
    if (state().inCircle()) {
      unknownType_computed = state().cycle();
    
    } else {
      unknownType_computed = ASTState.NON_CYCLE;
    
    }
    return unknownType_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void hasPackage_String_reset() {
    hasPackage_String_computed = new java.util.HashMap(4);
    hasPackage_String_values = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="hasPackage_String") protected java.util.Map hasPackage_String_values;
  /** @apilevel internal */
  @SideEffect.Secret(group="hasPackage_String") protected java.util.Map hasPackage_String_computed;
  /**
   * @attribute syn
   * @aspect LookupFullyQualifiedTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:101
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="LookupFullyQualifiedTypes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:101")
  @SideEffect.Pure(group="hasPackage_String") public boolean hasPackage(String packageName) {
    Object _parameters = packageName;
    if (hasPackage_String_computed == null) hasPackage_String_computed = new java.util.HashMap(4);
    if (hasPackage_String_values == null) hasPackage_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (hasPackage_String_values.containsKey(_parameters) && hasPackage_String_computed != null
        && hasPackage_String_computed.containsKey(_parameters)
        && (hasPackage_String_computed.get(_parameters) == ASTState.NON_CYCLE || hasPackage_String_computed.get(_parameters) == state().cycle())) {
      return (Boolean) hasPackage_String_values.get(_parameters);
    }
    boolean hasPackage_String_value = isPackage(packageName);
    if (state().inCircle()) {
      hasPackage_String_values.put(_parameters, hasPackage_String_value);
      hasPackage_String_computed.put(_parameters, state().cycle());
    
    } else {
      hasPackage_String_values.put(_parameters, hasPackage_String_value);
      hasPackage_String_computed.put(_parameters, ASTState.NON_CYCLE);
    
    }
    return hasPackage_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void lookupType_String_String_reset() {
    lookupType_String_String_computed = new java.util.HashMap(4);
    lookupType_String_String_values = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="lookupType_String_String") protected java.util.Map lookupType_String_String_values;
  /** @apilevel internal */
  @SideEffect.Secret(group="lookupType_String_String") protected java.util.Map lookupType_String_String_computed;
  /**
   * Checks from-source compilation units for the given type.
   * If no matching compilation unit is found the library compliation units
   * will be searched.
   * @attribute syn
   * @aspect LookupFullyQualifiedTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:146
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="LookupFullyQualifiedTypes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:146")
  @SideEffect.Pure(group="lookupType_String_String") public TypeDecl lookupType(String packageName, String typeName) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(packageName);
    _parameters.add(typeName);
    if (lookupType_String_String_computed == null) lookupType_String_String_computed = new java.util.HashMap(4);
    if (lookupType_String_String_values == null) lookupType_String_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (lookupType_String_String_values.containsKey(_parameters) && lookupType_String_String_computed != null
        && lookupType_String_String_computed.containsKey(_parameters)
        && (lookupType_String_String_computed.get(_parameters) == ASTState.NON_CYCLE || lookupType_String_String_computed.get(_parameters) == state().cycle())) {
      return (TypeDecl) lookupType_String_String_values.get(_parameters);
    }
    TypeDecl lookupType_String_String_value = lookupType_compute(packageName, typeName);
    if (state().inCircle()) {
      lookupType_String_String_values.put(_parameters, lookupType_String_String_value);
      lookupType_String_String_computed.put(_parameters, state().cycle());
    
    } else {
      lookupType_String_String_values.put(_parameters, lookupType_String_String_value);
      lookupType_String_String_computed.put(_parameters, ASTState.NON_CYCLE);
    
    }
    return lookupType_String_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private TypeDecl lookupType_compute(String packageName, String typeName) {
      // Look for a matching source type.
      TypeDecl sourceType = lookupSourceType(packageName, typeName);
      if (!sourceType.isUnknown()) {
        return sourceType;
      }
  
      // Look for a matching library type.
      return lookupLibraryType(packageName, typeName);
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void getLibCompilationUnit_String_reset() {
    getLibCompilationUnit_String_values = null;
    getLibCompilationUnit_String_proxy = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="getLibCompilationUnit_String") protected ASTNode getLibCompilationUnit_String_proxy;
  /** @apilevel internal */
  @SideEffect.Secret(group="getLibCompilationUnit_String") protected java.util.Map getLibCompilationUnit_String_values;

  /**
   * This attribute is used to cache library compilation units, by storing the compilation units in
   * a parameterized NTA.
   * @attribute syn
   * @aspect LookupFullyQualifiedTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:288
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="LookupFullyQualifiedTypes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:288")
  @SideEffect.Pure(group="getLibCompilationUnit_String") public CompilationUnit getLibCompilationUnit(String typeName) {
    Object _parameters = typeName;
    if (getLibCompilationUnit_String_values == null) getLibCompilationUnit_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (getLibCompilationUnit_String_values.containsKey(_parameters)) {
      return (CompilationUnit) getLibCompilationUnit_String_values.get(_parameters);
    }
    state().enterLazyAttribute();
    CompilationUnit getLibCompilationUnit_String_value = getCompilationUnit(typeName);
    if (getLibCompilationUnit_String_proxy == null) {
      getLibCompilationUnit_String_proxy = new ASTNode();
      getLibCompilationUnit_String_proxy.setParent(this);
    }
    if (getLibCompilationUnit_String_value != null) {
      getLibCompilationUnit_String_value.setParent(getLibCompilationUnit_String_proxy);
      if (getLibCompilationUnit_String_value.mayHaveRewrite()) {
        getLibCompilationUnit_String_value = (CompilationUnit) getLibCompilationUnit_String_value.rewrittenNode();
        getLibCompilationUnit_String_value.setParent(getLibCompilationUnit_String_proxy);
      }
    }
    getLibCompilationUnit_String_values.put(_parameters, getLibCompilationUnit_String_value);
    state().leaveLazyAttribute();
    return getLibCompilationUnit_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void emptyCompilationUnit_reset() {
    emptyCompilationUnit_computed = false;
    
    emptyCompilationUnit_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="emptyCompilationUnit") protected boolean emptyCompilationUnit_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="emptyCompilationUnit") protected CompilationUnit emptyCompilationUnit_value;

  /**
   * @attribute syn
   * @aspect LookupFullyQualifiedTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:291
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="LookupFullyQualifiedTypes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:291")
  @SideEffect.Pure(group="emptyCompilationUnit") public CompilationUnit emptyCompilationUnit() {
    ASTState state = state();
    if (emptyCompilationUnit_computed) {
      return emptyCompilationUnit_value;
    }
    state().enterLazyAttribute();
    emptyCompilationUnit_value = new CompilationUnit();
    emptyCompilationUnit_value.setParent(this);
    emptyCompilationUnit_computed = true;
    state().leaveLazyAttribute();
    return emptyCompilationUnit_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void getPrimitiveCompilationUnit_reset() {
    getPrimitiveCompilationUnit_computed = false;
    
    getPrimitiveCompilationUnit_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="getPrimitiveCompilationUnit") protected boolean getPrimitiveCompilationUnit_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="getPrimitiveCompilationUnit") protected PrimitiveCompilationUnit getPrimitiveCompilationUnit_value;

  /** Creates a compilation unit with primitive types. 
   * @attribute syn
   * @aspect PrimitiveTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\PrimitiveTypes.jrag:155
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="PrimitiveTypes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\PrimitiveTypes.jrag:155")
  @SideEffect.Pure(group="getPrimitiveCompilationUnit") public PrimitiveCompilationUnit getPrimitiveCompilationUnit() {
    ASTState state = state();
    if (getPrimitiveCompilationUnit_computed) {
      return getPrimitiveCompilationUnit_value;
    }
    state().enterLazyAttribute();
    getPrimitiveCompilationUnit_value = getPrimitiveCompilationUnit_compute();
    getPrimitiveCompilationUnit_value.setParent(this);
    getPrimitiveCompilationUnit_computed = true;
    state().leaveLazyAttribute();
    return getPrimitiveCompilationUnit_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh private PrimitiveCompilationUnit getPrimitiveCompilationUnit_compute() {
      PrimitiveCompilationUnit u = new PrimitiveCompilationUnit();
      u.setPackageDecl(PRIMITIVE_PACKAGE_NAME);
      return u;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void unknownConstructor_reset() {
    unknownConstructor_computed = null;
    unknownConstructor_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="unknownConstructor") protected ASTState.Cycle unknownConstructor_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="unknownConstructor") protected ConstructorDecl unknownConstructor_value;

  /**
   * @attribute syn
   * @aspect TypeAnalysis
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeAnalysis.jrag:264
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeAnalysis.jrag:264")
  @SideEffect.Pure(group="unknownConstructor") public ConstructorDecl unknownConstructor() {
    ASTState state = state();
    if (unknownConstructor_computed == ASTState.NON_CYCLE || unknownConstructor_computed == state().cycle()) {
      return unknownConstructor_value;
    }
    unknownConstructor_value = unknownType().constructors().iterator().next();
    if (state().inCircle()) {
      unknownConstructor_computed = state().cycle();
    
    } else {
      unknownConstructor_computed = ASTState.NON_CYCLE;
    
    }
    return unknownConstructor_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void wildcards_reset() {
    wildcards_computed = false;
    
    wildcards_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="wildcards") protected boolean wildcards_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="wildcards") protected WildcardsCompilationUnit wildcards_value;

  /**
   * @attribute syn
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\Generics.jrag:1569
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\Generics.jrag:1569")
  @SideEffect.Pure(group="wildcards") public WildcardsCompilationUnit wildcards() {
    ASTState state = state();
    if (wildcards_computed) {
      return wildcards_value;
    }
    state().enterLazyAttribute();
    wildcards_value = new WildcardsCompilationUnit(
              "wildcards",
              new List(),
              new List());
    wildcards_value.setParent(this);
    wildcards_computed = true;
    state().leaveLazyAttribute();
    return wildcards_value;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\AnonymousClasses.jrag:33
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_superType(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return null;
  }
  @SideEffect.Pure protected boolean canDefine_superType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\AnonymousClasses.jrag:39
   * @apilevel internal
   */
 @SideEffect.Pure public ConstructorDecl Define_constructorDecl(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return unknownConstructor();
  }
  @SideEffect.Pure protected boolean canDefine_constructorDecl(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\Arrays.jrag:56
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_componentType(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return unknownType();
  }
  @SideEffect.Pure protected boolean canDefine_componentType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\BranchTarget.jrag:255
   * @apilevel internal
   */
 @SideEffect.Pure public LabeledStmt Define_lookupLabel(ASTNode _callerNode, ASTNode _childNode, String name) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return null;
  }
  @SideEffect.Pure protected boolean canDefine_lookupLabel(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:105
   * @apilevel internal
   */
 @SideEffect.Pure public CompilationUnit Define_compilationUnit(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return null;
  }
  @SideEffect.Pure protected boolean canDefine_compilationUnit(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DeclareBeforeUse.jrag:35
   * @apilevel internal
   */
 @SideEffect.Pure public int Define_blockIndex(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return -1;
  }
  @SideEffect.Pure protected boolean canDefine_blockIndex(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DeclareBeforeUse.jrag:58
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_declaredBefore(ASTNode _callerNode, ASTNode _childNode, Variable decl) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_declaredBefore(ASTNode _callerNode, ASTNode _childNode, Variable decl) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:34
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isDest(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_isDest(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:44
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isSource(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return true;
  }
  @SideEffect.Pure protected boolean canDefine_isSource(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:66
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isIncOrDec(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_isIncOrDec(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:256
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_assignedBefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_assignedBefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:891
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_unassignedBefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return true;
  }
  @SideEffect.Pure protected boolean canDefine_unassignedBefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ExceptionHandling.jrag:40
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_typeException(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return lookupType("java.lang", "Exception");
  }
  @SideEffect.Pure protected boolean canDefine_typeException(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\TryWithResources.jrag:142
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_typeRuntimeException(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return lookupType("java.lang", "RuntimeException");
  }
  @SideEffect.Pure protected boolean canDefine_typeRuntimeException(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\TryWithResources.jrag:140
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_typeError(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return lookupType("java.lang", "Error");
  }
  @SideEffect.Pure protected boolean canDefine_typeError(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ExceptionHandling.jrag:49
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_typeNullPointerException(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return lookupType("java.lang", "NullPointerException");
  }
  @SideEffect.Pure protected boolean canDefine_typeNullPointerException(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:93
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_typeThrowable(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return lookupType("java.lang", "Throwable");
  }
  @SideEffect.Pure protected boolean canDefine_typeThrowable(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\TryWithResources.jrag:115
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_handlesException(ASTNode _callerNode, ASTNode _childNode, TypeDecl exceptionType) {
    int childIndex = this.getIndexOfChild(_callerNode);
    {
        throw new Error("Operation handlesException not supported");
      }
  }
  @SideEffect.Pure protected boolean canDefine_handlesException(ASTNode _callerNode, ASTNode _childNode, TypeDecl exceptionType) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupConstructor.jrag:35
   * @apilevel internal
   */
 @SideEffect.Pure public Collection<ConstructorDecl> Define_lookupConstructor(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return Collections.emptyList();
  }
  @SideEffect.Pure protected boolean canDefine_lookupConstructor(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupConstructor.jrag:43
   * @apilevel internal
   */
 @SideEffect.Pure public Collection<ConstructorDecl> Define_lookupSuperConstructor(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return Collections.emptyList();
  }
  @SideEffect.Pure protected boolean canDefine_lookupSuperConstructor(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupMethod.jrag:42
   * @apilevel internal
   */
 @SideEffect.Pure public Expr Define_nestedScope(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    {
        throw new UnsupportedOperationException();
      }
  }
  @SideEffect.Pure protected boolean canDefine_nestedScope(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupMethod.jrag:52
   * @apilevel internal
   */
 @SideEffect.Pure public Collection<MethodDecl> Define_lookupMethod(ASTNode _callerNode, ASTNode _childNode, String name) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return Collections.EMPTY_LIST;
  }
  @SideEffect.Pure protected boolean canDefine_lookupMethod(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\Generics.jrag:1158
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_typeObject(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return typeObject();
  }
  @SideEffect.Pure protected boolean canDefine_typeObject(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeAnalysis.jrag:152
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_typeCloneable(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return typeCloneable();
  }
  @SideEffect.Pure protected boolean canDefine_typeCloneable(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeAnalysis.jrag:151
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_typeSerializable(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return typeSerializable();
  }
  @SideEffect.Pure protected boolean canDefine_typeSerializable(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:74
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_typeBoolean(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return typeBoolean();
  }
  @SideEffect.Pure protected boolean canDefine_typeBoolean(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:75
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_typeByte(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return typeByte();
  }
  @SideEffect.Pure protected boolean canDefine_typeByte(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:76
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_typeShort(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return typeShort();
  }
  @SideEffect.Pure protected boolean canDefine_typeShort(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:77
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_typeChar(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return typeChar();
  }
  @SideEffect.Pure protected boolean canDefine_typeChar(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:86
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_typeInt(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return typeInt();
  }
  @SideEffect.Pure protected boolean canDefine_typeInt(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:88
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_typeLong(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return typeLong();
  }
  @SideEffect.Pure protected boolean canDefine_typeLong(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:80
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_typeFloat(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return typeFloat();
  }
  @SideEffect.Pure protected boolean canDefine_typeFloat(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:81
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_typeDouble(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return typeDouble();
  }
  @SideEffect.Pure protected boolean canDefine_typeDouble(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\Enums.jrag:538
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_typeString(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return typeString();
  }
  @SideEffect.Pure protected boolean canDefine_typeString(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:83
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_typeVoid(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return typeVoid();
  }
  @SideEffect.Pure protected boolean canDefine_typeVoid(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\Generics.jrag:1168
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_typeNull(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return typeNull();
  }
  @SideEffect.Pure protected boolean canDefine_typeNull(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\Annotations.jrag:729
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_unknownType(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return unknownType();
  }
  @SideEffect.Pure protected boolean canDefine_unknownType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:113
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_hasPackage(ASTNode _callerNode, ASTNode _childNode, String packageName) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return hasPackage(packageName);
  }
  @SideEffect.Pure protected boolean canDefine_hasPackage(ASTNode _callerNode, ASTNode _childNode, String packageName) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\TryWithResources.jrag:40
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_lookupType(ASTNode _callerNode, ASTNode _childNode, String packageName, String typeName) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return lookupType(packageName, typeName);
  }
  @SideEffect.Pure protected boolean canDefine_lookupType(ASTNode _callerNode, ASTNode _childNode, String packageName, String typeName) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\GenericMethods.jrag:225
   * @apilevel internal
   */
 @SideEffect.Pure public SimpleSet<TypeDecl> Define_lookupType(ASTNode _callerNode, ASTNode _childNode, String name) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return emptySet();
  }
  @SideEffect.Pure protected boolean canDefine_lookupType(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\TryWithResources.jrag:192
   * @apilevel internal
   */
 @SideEffect.Pure public SimpleSet<Variable> Define_lookupVariable(ASTNode _callerNode, ASTNode _childNode, String name) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return emptySet();
  }
  @SideEffect.Pure protected boolean canDefine_lookupVariable(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\Modifiers.jrag:433
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_mayBePublic(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_mayBePublic(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\Modifiers.jrag:435
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_mayBeProtected(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_mayBeProtected(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\Modifiers.jrag:434
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_mayBePrivate(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_mayBePrivate(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\Modifiers.jrag:436
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_mayBeStatic(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_mayBeStatic(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\Modifiers.jrag:437
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_mayBeFinal(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_mayBeFinal(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\Modifiers.jrag:438
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_mayBeAbstract(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_mayBeAbstract(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\Modifiers.jrag:439
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_mayBeVolatile(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_mayBeVolatile(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\Modifiers.jrag:440
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_mayBeTransient(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_mayBeTransient(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\Modifiers.jrag:441
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_mayBeStrictfp(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_mayBeStrictfp(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\Modifiers.jrag:442
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_mayBeSynchronized(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_mayBeSynchronized(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\Modifiers.jrag:443
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_mayBeNative(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_mayBeNative(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\NameCheck.jrag:356
   * @apilevel internal
   */
 @SideEffect.Pure public ASTNode Define_enclosingBlock(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return null;
  }
  @SideEffect.Pure protected boolean canDefine_enclosingBlock(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\MultiCatch.jrag:226
   * @apilevel internal
   */
 @SideEffect.Pure public VariableScope Define_outerScope(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    {
        throw new UnsupportedOperationException("outerScope() not defined");
      }
  }
  @SideEffect.Pure protected boolean canDefine_outerScope(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\NameCheck.jrag:504
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_insideLoop(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_insideLoop(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\NameCheck.jrag:512
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_insideSwitch(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_insideSwitch(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\NameCheck.jrag:569
   * @apilevel internal
   */
 @SideEffect.Pure public Case Define_bind(ASTNode _callerNode, ASTNode _childNode, Case c) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return null;
  }
  @SideEffect.Pure protected boolean canDefine_bind(ASTNode _callerNode, ASTNode _childNode, Case c) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\Options.jadd:40
   * @apilevel internal
   */
 @SideEffect.Pure public Program Define_program(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return this;
  }
  @SideEffect.Pure protected boolean canDefine_program(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\SyntacticClassification.jrag:36
   * @apilevel internal
   */
 @SideEffect.Pure public NameType Define_nameType(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return NameType.NOT_CLASSIFIED;
  }
  @SideEffect.Pure protected boolean canDefine_nameType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeAnalysis.jrag:232
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isAnonymous(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_isAnonymous(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupVariable.jrag:355
   * @apilevel internal
   */
 @SideEffect.Pure public Variable Define_unknownField(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return unknownType().findSingleVariable("unknown");
  }
  @SideEffect.Pure protected boolean canDefine_unknownField(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupMethod.jrag:36
   * @apilevel internal
   */
 @SideEffect.Pure public MethodDecl Define_unknownMethod(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    {
        for (MethodDecl m : unknownType().memberMethods("unknown")) {
          return m;
        }
        throw new Error("Could not find method unknown in type Unknown");
      }
  }
  @SideEffect.Pure protected boolean canDefine_unknownMethod(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupConstructor.jrag:126
   * @apilevel internal
   */
 @SideEffect.Pure public ConstructorDecl Define_unknownConstructor(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return unknownConstructor();
  }
  @SideEffect.Pure protected boolean canDefine_unknownConstructor(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\Annotations.jrag:713
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_declType(ASTNode _callerNode, ASTNode _childNode) {
    int i = this.getIndexOfChild(_callerNode);
    return null;
  }
  @SideEffect.Pure protected boolean canDefine_declType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\MultiCatch.jrag:227
   * @apilevel internal
   */
 @SideEffect.Pure public BodyDecl Define_enclosingBodyDecl(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return null;
  }
  @SideEffect.Pure protected boolean canDefine_enclosingBodyDecl(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeAnalysis.jrag:580
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isMemberType(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_isMemberType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\MultiCatch.jrag:76
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_hostType(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return null;
  }
  @SideEffect.Pure protected boolean canDefine_hostType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeCheck.jrag:482
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_switchType(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return unknownType();
  }
  @SideEffect.Pure protected boolean canDefine_switchType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeCheck.jrag:534
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_returnType(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return typeVoid();
  }
  @SideEffect.Pure protected boolean canDefine_returnType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeCheck.jrag:667
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_enclosingInstance(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return null;
  }
  @SideEffect.Pure protected boolean canDefine_enclosingInstance(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeHierarchyCheck.jrag:33
   * @apilevel internal
   */
 @SideEffect.Pure public String Define_methodHost(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    {
        throw new Error("Needs extra equation for methodHost()");
      }
  }
  @SideEffect.Pure protected boolean canDefine_methodHost(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeHierarchyCheck.jrag:189
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_inExplicitConstructorInvocation(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_inExplicitConstructorInvocation(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeHierarchyCheck.jrag:197
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_enclosingExplicitConstructorHostType(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return null;
  }
  @SideEffect.Pure protected boolean canDefine_enclosingExplicitConstructorHostType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeHierarchyCheck.jrag:208
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_inStaticContext(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_inStaticContext(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\PreciseRethrow.jrag:280
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_reportUnreachable(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return true;
  }
  @SideEffect.Pure protected boolean canDefine_reportUnreachable(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\MultiCatch.jrag:44
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isMethodParameter(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_isMethodParameter(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\MultiCatch.jrag:45
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isConstructorParameter(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_isConstructorParameter(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\MultiCatch.jrag:46
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isExceptionHandlerParameter(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_isExceptionHandlerParameter(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\backend\\CodeGeneration.jrag:71
   * @apilevel internal
   */
 @SideEffect.Pure public int Define_variableScopeEndLabel(ASTNode _callerNode, ASTNode _childNode, CodeGeneration gen) {
    int i = this.getIndexOfChild(_callerNode);
    {
        throw new Error("variableScopeEndLabel not valid from here");
      }
  }
  @SideEffect.Pure protected boolean canDefine_variableScopeEndLabel(ASTNode _callerNode, ASTNode _childNode, CodeGeneration gen) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\backend\\InnerClasses.jrag:104
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_expectedType(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return null;
  }
  @SideEffect.Pure protected boolean canDefine_expectedType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\backend\\InnerClasses.jrag:440
   * @apilevel internal
   */
 @SideEffect.Pure public ClassInstanceExpr Define_classInstanceExpression(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    {
        throw new Error("Missing enclosing class instance.");
      }
  }
  @SideEffect.Pure protected boolean canDefine_classInstanceExpression(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\backend\\MultiCatch.jrag:64
   * @apilevel internal
   */
 @SideEffect.Pure public int Define_localNum(ASTNode _callerNode, ASTNode _childNode) {
    int index = this.getIndexOfChild(_callerNode);
    return 0;
  }
  @SideEffect.Pure protected boolean canDefine_localNum(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\backend\\LocalNum.jrag:100
   * @apilevel internal
   */
 @SideEffect.Pure public int Define_resultSaveLocalNum(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    {
        throw new Error("Unsupported operation resultSaveLocalNum");
      }
  }
  @SideEffect.Pure protected boolean canDefine_resultSaveLocalNum(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\Annotations.jrag:131
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_mayUseAnnotationTarget(ASTNode _callerNode, ASTNode _childNode, String name) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_mayUseAnnotationTarget(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\Annotations.jrag:278
   * @apilevel internal
   */
 @SideEffect.Pure public ElementValue Define_lookupElementTypeValue(ASTNode _callerNode, ASTNode _childNode, String name) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return null;
  }
  @SideEffect.Pure protected boolean canDefine_lookupElementTypeValue(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\SuppressWarnings.jrag:37
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_withinSuppressWarnings(ASTNode _callerNode, ASTNode _childNode, String annot) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_withinSuppressWarnings(ASTNode _callerNode, ASTNode _childNode, String annot) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\Annotations.jrag:536
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_withinDeprecatedAnnotation(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_withinDeprecatedAnnotation(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\Annotations.jrag:604
   * @apilevel internal
   */
 @SideEffect.Pure public Annotation Define_lookupAnnotation(ASTNode _callerNode, ASTNode _childNode, TypeDecl typeDecl) {
    int i = this.getIndexOfChild(_callerNode);
    return null;
  }
  @SideEffect.Pure protected boolean canDefine_lookupAnnotation(ASTNode _callerNode, ASTNode _childNode, TypeDecl typeDecl) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\Annotations.jrag:648
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_enclosingAnnotationDecl(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return unknownType();
  }
  @SideEffect.Pure protected boolean canDefine_enclosingAnnotationDecl(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\GenericMethodsInference.jrag:65
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_assignConvertedType(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return typeNull();
  }
  @SideEffect.Pure protected boolean canDefine_assignConvertedType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\Generics.jrag:341
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_inExtendsOrImplements(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_inExtendsOrImplements(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\Generics.jrag:1249
   * @apilevel internal
   */
 @SideEffect.Pure public FieldDecl Define_fieldDecl(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return null;
  }
  @SideEffect.Pure protected boolean canDefine_fieldDecl(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\Generics.jrag:1583
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_typeWildcard(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return wildcards().typeWildcard();
  }
  @SideEffect.Pure protected boolean canDefine_typeWildcard(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\Generics.jrag:1582
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_lookupWildcardExtends(ASTNode _callerNode, ASTNode _childNode, TypeDecl typeDecl) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return wildcards().lookupWildcardExtends(typeDecl);
  }
  @SideEffect.Pure protected boolean canDefine_lookupWildcardExtends(ASTNode _callerNode, ASTNode _childNode, TypeDecl typeDecl) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\Generics.jrag:1581
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_lookupWildcardSuper(ASTNode _callerNode, ASTNode _childNode, TypeDecl typeDecl) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return wildcards().lookupWildcardSuper(typeDecl);
  }
  @SideEffect.Pure protected boolean canDefine_lookupWildcardSuper(ASTNode _callerNode, ASTNode _childNode, TypeDecl typeDecl) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\MultiCatch.jrag:210
   * @apilevel internal
   */
 @SideEffect.Pure public LUBType Define_lookupLUBType(ASTNode _callerNode, ASTNode _childNode, Collection<TypeDecl> bounds) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return wildcards().lookupLUBType(bounds);
  }
  @SideEffect.Pure protected boolean canDefine_lookupLUBType(ASTNode _callerNode, ASTNode _childNode, Collection<TypeDecl> bounds) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\Generics.jrag:1683
   * @apilevel internal
   */
 @SideEffect.Pure public GLBType Define_lookupGLBType(ASTNode _callerNode, ASTNode _childNode, Collection<TypeDecl> bounds) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return wildcards().lookupGLBType(bounds);
  }
  @SideEffect.Pure protected boolean canDefine_lookupGLBType(ASTNode _callerNode, ASTNode _childNode, Collection<TypeDecl> bounds) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\GenericsParTypeDecl.jrag:74
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_genericDecl(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return null;
  }
  @SideEffect.Pure protected boolean canDefine_genericDecl(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\VariableArityParameters.jrag:46
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_variableArityValid(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_variableArityValid(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\Diamond.jrag:94
   * @apilevel internal
   */
 @SideEffect.Pure public ClassInstanceExpr Define_getClassInstanceExpr(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return null;
  }
  @SideEffect.Pure protected boolean canDefine_getClassInstanceExpr(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\Diamond.jrag:409
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isAnonymousDecl(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_isAnonymousDecl(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\Diamond.jrag:425
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isExplicitGenericConstructorAccess(ASTNode _callerNode, ASTNode _childNode) {
    int i = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_isExplicitGenericConstructorAccess(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\PreciseRethrow.jrag:202
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isCatchParam(ASTNode _callerNode, ASTNode _childNode) {
    int i = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_isCatchParam(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\PreciseRethrow.jrag:209
   * @apilevel internal
   */
 @SideEffect.Pure public CatchClause Define_catchClause(ASTNode _callerNode, ASTNode _childNode) {
    int i = this.getIndexOfChild(_callerNode);
    {
        throw new IllegalStateException("Could not find parent " + "catch clause");
      }
  }
  @SideEffect.Pure protected boolean canDefine_catchClause(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\TryWithResources.jrag:198
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_resourcePreviouslyDeclared(ASTNode _callerNode, ASTNode _childNode, String name) {
    int i = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_resourcePreviouslyDeclared(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Pure public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }
}

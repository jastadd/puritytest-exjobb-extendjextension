/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package org.extendj.ast;
import java.util.HashSet;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;
import java.util.Set;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import beaver.*;
import java.util.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.jastadd.util.*;
import java.util.zip.*;
import java.io.*;
import org.jastadd.util.PrettyPrintable;
import org.jastadd.util.PrettyPrinter;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\grammar\\Java.ast:210
 * @production BreakStmt : {@link Stmt} ::= <span class="component">&lt;Label:String&gt;</span> <span class="component">[Finally:{@link Block}]</span>;

 */
public class BreakStmt extends Stmt implements Cloneable {
  /**
   * @aspect BranchTarget
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\BranchTarget.jrag:104
   */
  public void collectBranches(Collection<Stmt> c) {
    c.add(this);
  }
  /**
   * @aspect Java4PrettyPrint
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\PrettyPrint.jadd:129
   */
  public void prettyPrint(PrettyPrinter out) {
    out.print("break");
    if (hasLabel()) {
      out.print(" ");
      out.print(getLabel());
    }
    out.print(";");
  }
  /**
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\backend\\CreateBCode.jrag:1777
   */
  public void createBCode(CodeGeneration gen) {
    super.createBCode(gen);
    if (hasFinally()) {
      int nextRange = gen.constantPool().newLabel();
      getFinally().createBCode(gen);
      gen.emitGoto(targetStmt().break_label());
      gen.addLabel(nextRange);
      gen.monitorRangesStart(this, nextRange);
    } else {
      gen.emitGoto(targetStmt().break_label());
    }
  }
  /**
   * @declaredat ASTNode:1
   */
  public BreakStmt() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
    setChild(new Opt(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  public BreakStmt(String p0) {
    setLabel(p0);
  }
  /**
   * @declaredat ASTNode:17
   */
  public BreakStmt(beaver.Symbol p0) {
    setLabel(p0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:21
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:27
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:31
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    targetStmt_reset();
    assignedAfter_Variable_reset();
    unassignedAfterReachedFinallyBlocks_Variable_reset();
    assignedAfterReachedFinallyBlocks_Variable_reset();
    unassignedAfter_Variable_reset();
    getFinallyOpt_reset();
    canCompleteNormally_reset();
    lookupLabel_String_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:47
   */
  @SideEffect.Fresh public BreakStmt clone() throws CloneNotSupportedException {
    BreakStmt node = (BreakStmt) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:52
   */
  @SideEffect.Fresh(group="_ASTNode") public BreakStmt copy() {
    try {
      BreakStmt node = (BreakStmt) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:71
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public BreakStmt fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:81
   */
  @SideEffect.Fresh(group="_ASTNode") public BreakStmt treeCopyNoTransform() {
    BreakStmt tree = (BreakStmt) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 0:
          tree.children[i] = new Opt();
          continue;
        }
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:106
   */
  @SideEffect.Fresh(group="_ASTNode") public BreakStmt treeCopy() {
    BreakStmt tree = (BreakStmt) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 0:
          tree.children[i] = new Opt();
          continue;
        }
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:125
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_Label == ((BreakStmt) node).tokenString_Label);    
  }
  /**
   * Replaces the lexeme Label.
   * @param value The new value for the lexeme Label.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setLabel(String value) {
    tokenString_Label = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_Label;
  /**
   */
  public int Labelstart;
  /**
   */
  public int Labelend;
  /**
   * JastAdd-internal setter for lexeme Label using the Beaver parser.
   * @param symbol Symbol containing the new value for the lexeme Label
   * @apilevel internal
   */
  @SideEffect.Local(group="_ASTNode") public void setLabel(beaver.Symbol symbol) {
    if (symbol.value != null && !(symbol.value instanceof String))
    throw new UnsupportedOperationException("setLabel is only valid for String lexemes");
    tokenString_Label = (String)symbol.value;
    Labelstart = symbol.getStart();
    Labelend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Label.
   * @return The value for the lexeme Label.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Label")
  @SideEffect.Pure(group="_ASTNode") public String getLabel() {
    return tokenString_Label != null ? tokenString_Label : "";
  }
  /**
   * Replaces the (optional) Finally child.
   * @param node The new node to be used as the Finally child.
   * @apilevel high-level
   */
  public void setFinally(Block node) {
    getFinallyOpt().setChild(node, 0);
  }
  /**
   * Check whether the optional Finally child exists.
   * @return {@code true} if the optional Finally child exists, {@code false} if it does not.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasFinally() {
    return getFinallyOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) Finally child.
   * @return The Finally child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   */
  @SideEffect.Pure public Block getFinally() {
    return (Block) getFinallyOpt().getChild(0);
  }
  /**
   * Retrieves the optional node for child Finally. This is the <code>Opt</code> node containing the child Finally, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child Finally.
   * @apilevel low-level
   */
  @SideEffect.Pure public Opt<Block> getFinallyOptNoTransform() {
    return (Opt<Block>) getChildNoTransform(0);
  }
  /**
   * Retrieves the child position of the optional child Finally.
   * @return The the child position of the optional child Finally.
   * @apilevel low-level
   */
  @SideEffect.Pure protected int getFinallyOptChildPosition() {
    return 0;
  }
  /**
   * @attribute syn
   * @aspect BranchTarget
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\BranchTarget.jrag:122
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="BranchTarget", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\BranchTarget.jrag:122")
  @SideEffect.Pure(group="hasLabel") public boolean hasLabel() {
    boolean hasLabel_value = !getLabel().equals("");
    return hasLabel_value;
  }
  /**
   * @return <code>true</code> if this statement can branch to
   * the target statement.
   * @attribute syn
   * @aspect BranchTarget
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\BranchTarget.jrag:182
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="BranchTarget", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\BranchTarget.jrag:182")
  @SideEffect.Pure(group="canBranchTo_BranchTargetStmt") public boolean canBranchTo(BranchTargetStmt target) {
    boolean canBranchTo_BranchTargetStmt_value = !hasLabel();
    return canBranchTo_BranchTargetStmt_value;
  }
  /**
   * @attribute syn
   * @aspect BranchTarget
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\BranchTarget.jrag:184
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="BranchTarget", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\BranchTarget.jrag:184")
  @SideEffect.Pure(group="canBranchTo_LabeledStmt") public boolean canBranchTo(LabeledStmt target) {
    boolean canBranchTo_LabeledStmt_value = hasLabel() && target.getLabel().equals(getLabel());
    return canBranchTo_LabeledStmt_value;
  }
  /**
   * @attribute syn
   * @aspect BranchTarget
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\BranchTarget.jrag:186
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="BranchTarget", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\BranchTarget.jrag:186")
  @SideEffect.Pure(group="canBranchTo_SwitchStmt") public boolean canBranchTo(SwitchStmt target) {
    boolean canBranchTo_SwitchStmt_value = !hasLabel();
    return canBranchTo_SwitchStmt_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void targetStmt_reset() {
    targetStmt_computed = null;
    targetStmt_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="targetStmt") protected ASTState.Cycle targetStmt_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="targetStmt") protected Stmt targetStmt_value;

  /**
   * Find the target statement for break and continue. This can be a try
   * statement with a finally block that can not complete normally.
   * @attribute syn
   * @aspect BranchTarget
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\BranchTarget.jrag:40
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="BranchTarget", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\BranchTarget.jrag:40")
  @SideEffect.Pure(group="targetStmt") public Stmt targetStmt() {
    ASTState state = state();
    if (targetStmt_computed == ASTState.NON_CYCLE || targetStmt_computed == state().cycle()) {
      return targetStmt_value;
    }
    targetStmt_value = branchTarget(this);
    if (state().inCircle()) {
      targetStmt_computed = state().cycle();
    
    } else {
      targetStmt_computed = ASTState.NON_CYCLE;
    
    }
    return targetStmt_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void assignedAfter_Variable_reset() {
    assignedAfter_Variable_values = null;
  }
  @SideEffect.Secret(group="assignedAfter_Variable") protected java.util.Map assignedAfter_Variable_values;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="DefiniteAssignment", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:264")
  @SideEffect.Pure(group="assignedAfter_Variable") public boolean assignedAfter(Variable v) {
    Object _parameters = v;
    if (assignedAfter_Variable_values == null) assignedAfter_Variable_values = new java.util.HashMap(4);
    ASTState.CircularValue _value;
    if (assignedAfter_Variable_values.containsKey(_parameters)) {
      Object _cache = assignedAfter_Variable_values.get(_parameters);
      if (!(_cache instanceof ASTState.CircularValue)) {
        return (Boolean) _cache;
      } else {
        _value = (ASTState.CircularValue) _cache;
      }
    } else {
      _value = new ASTState.CircularValue();
      assignedAfter_Variable_values.put(_parameters, _value);
      _value.value = true;
    }
    ASTState state = state();
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      boolean new_assignedAfter_Variable_value;
      do {
        _value.cycle = state.nextCycle();
        new_assignedAfter_Variable_value = true;
        if (new_assignedAfter_Variable_value != ((Boolean)_value.value)) {
          state.setChangeInCycle();
          _value.value = new_assignedAfter_Variable_value;
        }
      } while (state.testAndClearChangeInCycle());
      assignedAfter_Variable_values.put(_parameters, new_assignedAfter_Variable_value);

      state.leaveCircle();
      return new_assignedAfter_Variable_value;
    } else if (_value.cycle != state.cycle()) {
      _value.cycle = state.cycle();
      boolean new_assignedAfter_Variable_value = true;
      if (new_assignedAfter_Variable_value != ((Boolean)_value.value)) {
        state.setChangeInCycle();
        _value.value = new_assignedAfter_Variable_value;
      }
      return new_assignedAfter_Variable_value;
    } else {
      return (Boolean) _value.value;
    }
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void unassignedAfterReachedFinallyBlocks_Variable_reset() {
    unassignedAfterReachedFinallyBlocks_Variable_values = null;
  }
  @SideEffect.Secret(group="unassignedAfterReachedFinallyBlocks_Variable") protected java.util.Map unassignedAfterReachedFinallyBlocks_Variable_values;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="DefiniteUnassignment", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:1254")
  @SideEffect.Pure(group="unassignedAfterReachedFinallyBlocks_Variable") public boolean unassignedAfterReachedFinallyBlocks(Variable v) {
    Object _parameters = v;
    if (unassignedAfterReachedFinallyBlocks_Variable_values == null) unassignedAfterReachedFinallyBlocks_Variable_values = new java.util.HashMap(4);
    ASTState.CircularValue _value;
    if (unassignedAfterReachedFinallyBlocks_Variable_values.containsKey(_parameters)) {
      Object _cache = unassignedAfterReachedFinallyBlocks_Variable_values.get(_parameters);
      if (!(_cache instanceof ASTState.CircularValue)) {
        return (Boolean) _cache;
      } else {
        _value = (ASTState.CircularValue) _cache;
      }
    } else {
      _value = new ASTState.CircularValue();
      unassignedAfterReachedFinallyBlocks_Variable_values.put(_parameters, _value);
      _value.value = true;
    }
    ASTState state = state();
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      boolean new_unassignedAfterReachedFinallyBlocks_Variable_value;
      do {
        _value.cycle = state.nextCycle();
        new_unassignedAfterReachedFinallyBlocks_Variable_value = unassignedAfterReachedFinallyBlocks_compute(v);
        if (new_unassignedAfterReachedFinallyBlocks_Variable_value != ((Boolean)_value.value)) {
          state.setChangeInCycle();
          _value.value = new_unassignedAfterReachedFinallyBlocks_Variable_value;
        }
      } while (state.testAndClearChangeInCycle());
      unassignedAfterReachedFinallyBlocks_Variable_values.put(_parameters, new_unassignedAfterReachedFinallyBlocks_Variable_value);

      state.leaveCircle();
      return new_unassignedAfterReachedFinallyBlocks_Variable_value;
    } else if (_value.cycle != state.cycle()) {
      _value.cycle = state.cycle();
      boolean new_unassignedAfterReachedFinallyBlocks_Variable_value = unassignedAfterReachedFinallyBlocks_compute(v);
      if (new_unassignedAfterReachedFinallyBlocks_Variable_value != ((Boolean)_value.value)) {
        state.setChangeInCycle();
        _value.value = new_unassignedAfterReachedFinallyBlocks_Variable_value;
      }
      return new_unassignedAfterReachedFinallyBlocks_Variable_value;
    } else {
      return (Boolean) _value.value;
    }
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean unassignedAfterReachedFinallyBlocks_compute(Variable v) {
      Iterator<FinallyHost> iter = finallyIterator();
      if (!unassignedBefore(v) && !iter.hasNext()) {
        return false;
      }
      while (iter.hasNext()) {
        FinallyHost f = iter.next();
        if (!f.unassignedAfterFinally(v)) {
          return false;
        }
      }
      return true;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void assignedAfterReachedFinallyBlocks_Variable_reset() {
    assignedAfterReachedFinallyBlocks_Variable_values = null;
  }
  @SideEffect.Secret(group="assignedAfterReachedFinallyBlocks_Variable") protected java.util.Map assignedAfterReachedFinallyBlocks_Variable_values;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="DefiniteUnassignment", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:1296")
  @SideEffect.Pure(group="assignedAfterReachedFinallyBlocks_Variable") public boolean assignedAfterReachedFinallyBlocks(Variable v) {
    Object _parameters = v;
    if (assignedAfterReachedFinallyBlocks_Variable_values == null) assignedAfterReachedFinallyBlocks_Variable_values = new java.util.HashMap(4);
    ASTState.CircularValue _value;
    if (assignedAfterReachedFinallyBlocks_Variable_values.containsKey(_parameters)) {
      Object _cache = assignedAfterReachedFinallyBlocks_Variable_values.get(_parameters);
      if (!(_cache instanceof ASTState.CircularValue)) {
        return (Boolean) _cache;
      } else {
        _value = (ASTState.CircularValue) _cache;
      }
    } else {
      _value = new ASTState.CircularValue();
      assignedAfterReachedFinallyBlocks_Variable_values.put(_parameters, _value);
      _value.value = true;
    }
    ASTState state = state();
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      boolean new_assignedAfterReachedFinallyBlocks_Variable_value;
      do {
        _value.cycle = state.nextCycle();
        new_assignedAfterReachedFinallyBlocks_Variable_value = assignedAfterReachedFinallyBlocks_compute(v);
        if (new_assignedAfterReachedFinallyBlocks_Variable_value != ((Boolean)_value.value)) {
          state.setChangeInCycle();
          _value.value = new_assignedAfterReachedFinallyBlocks_Variable_value;
        }
      } while (state.testAndClearChangeInCycle());
      assignedAfterReachedFinallyBlocks_Variable_values.put(_parameters, new_assignedAfterReachedFinallyBlocks_Variable_value);

      state.leaveCircle();
      return new_assignedAfterReachedFinallyBlocks_Variable_value;
    } else if (_value.cycle != state.cycle()) {
      _value.cycle = state.cycle();
      boolean new_assignedAfterReachedFinallyBlocks_Variable_value = assignedAfterReachedFinallyBlocks_compute(v);
      if (new_assignedAfterReachedFinallyBlocks_Variable_value != ((Boolean)_value.value)) {
        state.setChangeInCycle();
        _value.value = new_assignedAfterReachedFinallyBlocks_Variable_value;
      }
      return new_assignedAfterReachedFinallyBlocks_Variable_value;
    } else {
      return (Boolean) _value.value;
    }
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean assignedAfterReachedFinallyBlocks_compute(Variable v) {
      if (assignedBefore(v)) {
        return true;
      }
      Iterator<FinallyHost> iter = finallyIterator();
      if (!iter.hasNext()) {
        return false;
      }
      while (iter.hasNext()) {
        FinallyHost f = iter.next();
        if (!f.assignedAfterFinally(v)) {
          return false;
        }
      }
      return true;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void unassignedAfter_Variable_reset() {
    unassignedAfter_Variable_values = null;
  }
  @SideEffect.Secret(group="unassignedAfter_Variable") protected java.util.Map unassignedAfter_Variable_values;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="DefiniteUnassignment", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:899")
  @SideEffect.Pure(group="unassignedAfter_Variable") public boolean unassignedAfter(Variable v) {
    Object _parameters = v;
    if (unassignedAfter_Variable_values == null) unassignedAfter_Variable_values = new java.util.HashMap(4);
    ASTState.CircularValue _value;
    if (unassignedAfter_Variable_values.containsKey(_parameters)) {
      Object _cache = unassignedAfter_Variable_values.get(_parameters);
      if (!(_cache instanceof ASTState.CircularValue)) {
        return (Boolean) _cache;
      } else {
        _value = (ASTState.CircularValue) _cache;
      }
    } else {
      _value = new ASTState.CircularValue();
      unassignedAfter_Variable_values.put(_parameters, _value);
      _value.value = true;
    }
    ASTState state = state();
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      boolean new_unassignedAfter_Variable_value;
      do {
        _value.cycle = state.nextCycle();
        new_unassignedAfter_Variable_value = true;
        if (new_unassignedAfter_Variable_value != ((Boolean)_value.value)) {
          state.setChangeInCycle();
          _value.value = new_unassignedAfter_Variable_value;
        }
      } while (state.testAndClearChangeInCycle());
      unassignedAfter_Variable_values.put(_parameters, new_unassignedAfter_Variable_value);

      state.leaveCircle();
      return new_unassignedAfter_Variable_value;
    } else if (_value.cycle != state.cycle()) {
      _value.cycle = state.cycle();
      boolean new_unassignedAfter_Variable_value = true;
      if (new_unassignedAfter_Variable_value != ((Boolean)_value.value)) {
        state.setChangeInCycle();
        _value.value = new_unassignedAfter_Variable_value;
      }
      return new_unassignedAfter_Variable_value;
    } else {
      return (Boolean) _value.value;
    }
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void getFinallyOpt_reset() {
    getFinallyOpt_computed = false;
    
    getFinallyOpt_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="getFinallyOpt") protected boolean getFinallyOpt_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="getFinallyOpt") protected Opt<Block> getFinallyOpt_value;

  /**
   * @attribute syn nta
   * @aspect NTAFinally
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\NTAFinally.jrag:46
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="NTAFinally", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\NTAFinally.jrag:46")
  @SideEffect.Pure(group="getFinallyOpt") public Opt<Block> getFinallyOpt() {
    ASTState state = state();
    if (getFinallyOpt_computed) {
      return (Opt<Block>) getChild(getFinallyOptChildPosition());
    }
    state().enterLazyAttribute();
    getFinallyOpt_value = getFinallyOpt_compute();
    setChild(getFinallyOpt_value, getFinallyOptChildPosition());
    getFinallyOpt_computed = true;
    state().leaveLazyAttribute();
    Opt<Block> node = (Opt<Block>) this.getChild(getFinallyOptChildPosition());
    return node;
  }
  /** @apilevel internal */
  @SideEffect.Fresh private Opt<Block> getFinallyOpt_compute() {
      return branchFinallyOpt();
    }
  /**
   * @attribute syn
   * @aspect NameCheck
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\NameCheck.jrag:519
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameCheck", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\NameCheck.jrag:519")
  @SideEffect.Pure(group="nameProblems") public Collection<Problem> nameProblems() {
    {
        if (!hasLabel() && !insideLoop() && !insideSwitch()) {
          return Collections.singletonList(error("break outside switch or loop"));
        } else if (hasLabel()) {
          LabeledStmt label = lookupLabel(getLabel());
          if (label == null) {
            return Collections.singletonList(error("labeled break must have visible matching label"));
          }
        }
        return Collections.emptyList();
      }
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void canCompleteNormally_reset() {
    canCompleteNormally_computed = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="canCompleteNormally") protected ASTState.Cycle canCompleteNormally_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="canCompleteNormally") protected boolean canCompleteNormally_value;

  /**
   * @attribute syn
   * @aspect UnreachableStatements
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\UnreachableStatements.jrag:50
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="UnreachableStatements", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\UnreachableStatements.jrag:50")
  @SideEffect.Pure(group="canCompleteNormally") public boolean canCompleteNormally() {
    ASTState state = state();
    if (canCompleteNormally_computed == ASTState.NON_CYCLE || canCompleteNormally_computed == state().cycle()) {
      return canCompleteNormally_value;
    }
    canCompleteNormally_value = false;
    if (state().inCircle()) {
      canCompleteNormally_computed = state().cycle();
    
    } else {
      canCompleteNormally_computed = ASTState.NON_CYCLE;
    
    }
    return canCompleteNormally_value;
  }
  /**
   * @attribute syn
   * @aspect PreciseRethrow
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\PreciseRethrow.jrag:78
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="PreciseRethrow", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\PreciseRethrow.jrag:78")
  @SideEffect.Pure(group="modifiedInScope_Variable") public boolean modifiedInScope(Variable var) {
    boolean modifiedInScope_Variable_value = false;
    return modifiedInScope_Variable_value;
  }
  /** Lookup visible label. 
   * @attribute inh
   * @aspect BranchTarget
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\BranchTarget.jrag:252
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="BranchTarget", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\BranchTarget.jrag:252")
  @SideEffect.Pure(group="lookupLabel_String") public LabeledStmt lookupLabel(String name) {
    Object _parameters = name;
    if (lookupLabel_String_computed == null) lookupLabel_String_computed = new java.util.HashMap(4);
    if (lookupLabel_String_values == null) lookupLabel_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (lookupLabel_String_values.containsKey(_parameters) && lookupLabel_String_computed != null
        && lookupLabel_String_computed.containsKey(_parameters)
        && (lookupLabel_String_computed.get(_parameters) == ASTState.NON_CYCLE || lookupLabel_String_computed.get(_parameters) == state().cycle())) {
      return (LabeledStmt) lookupLabel_String_values.get(_parameters);
    }
    LabeledStmt lookupLabel_String_value = getParent().Define_lookupLabel(this, null, name);
    if (state().inCircle()) {
      lookupLabel_String_values.put(_parameters, lookupLabel_String_value);
      lookupLabel_String_computed.put(_parameters, state().cycle());
    
    } else {
      lookupLabel_String_values.put(_parameters, lookupLabel_String_value);
      lookupLabel_String_computed.put(_parameters, ASTState.NON_CYCLE);
    
    }
    return lookupLabel_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void lookupLabel_String_reset() {
    lookupLabel_String_computed = new java.util.HashMap(4);
    lookupLabel_String_values = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="lookupLabel_String") protected java.util.Map lookupLabel_String_values;
  /** @apilevel internal */
  @SideEffect.Secret(group="lookupLabel_String") protected java.util.Map lookupLabel_String_computed;
  /**
   * @attribute inh
   * @aspect NameCheck
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\NameCheck.jrag:503
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameCheck", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\NameCheck.jrag:503")
  @SideEffect.Pure(group="insideLoop") public boolean insideLoop() {
    boolean insideLoop_value = getParent().Define_insideLoop(this, null);
    return insideLoop_value;
  }
  /**
   * @attribute inh
   * @aspect NameCheck
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\NameCheck.jrag:512
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameCheck", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\NameCheck.jrag:512")
  @SideEffect.Pure(group="insideSwitch") public boolean insideSwitch() {
    boolean insideSwitch_value = getParent().Define_insideSwitch(this, null);
    return insideSwitch_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }
  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_CompilationUnit_problems(CompilationUnit _root, @SideEffect.Local java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\NameCheck.jrag:517
    {
      java.util.Set<ASTNode> contributors = _map.get(_root);
      if (contributors == null) {
        contributors = new java.util.LinkedHashSet<ASTNode>();
        _map.put((ASTNode) _root, contributors);
      }
      contributors.add(this);
    }
    super.collect_contributors_CompilationUnit_problems(_root, _map);
  }
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_CompilationUnit_problems(LinkedList<Problem> collection) {
    super.contributeTo_CompilationUnit_problems(collection);
    for (Problem value : nameProblems()) {
      collection.add(value);
    }
  }
}

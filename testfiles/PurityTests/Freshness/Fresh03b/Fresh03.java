import lang.ast.SideEffect.*;
public class Fresh03{
	int i=0;
	@Fresh public Link simple(){
		Link link=new Link();
		return link; 
	}
    
	@Fresh public Link caller01(){
		return simple(); 
	}
	
	@Fresh public Link caller02(){
		Link link=simple();
		return link; 
	}	
	
	@Fresh public Link caller03(){
		Link link;
		link=simple(); 
		return simple(); 
	}
	
	@Pure public Link noFresh(){
		if (i>0)
			return new Link();
		if (i==10)
			return l;
		return null;
	}
	
	@Fresh public Link caller04(){
		return noFresh(); 
	}
	
	@Fresh public Link caller05(){
		Link link=noFresh();
		return link; 
	}
	
	@Fresh public Link caller05(){
		Link link=noFresh();
		Link b=new Link();
		b=link;
		return b; 
	}
	
	
    public Link l;
	
    class Link{
        int x;
        Link la;
        @Fresh public Link(){
        	x=10;
        }
    }
}
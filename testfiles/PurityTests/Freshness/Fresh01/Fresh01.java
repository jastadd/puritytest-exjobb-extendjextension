import lang.ast.SideEffect.*;
public class Fresh01{
	@Fresh public Link freshLink(){
		Link l=new Link();
		Link f;
		f=l;
		return f;
	}
	
	@Fresh public Link freshLink(Link p){
		Link l=p;
		Link f;
		f=l;
		return f; // Wrong 
	}
	
	@Fresh public Link freshLink2(){
		Link l=new Link();
		Link f; // effectivly null!!
		return f;
	}
    
    
    class Link{
        int x;
        Link la;
    }
}
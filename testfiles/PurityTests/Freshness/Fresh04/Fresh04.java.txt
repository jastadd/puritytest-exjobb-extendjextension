testfiles\PurityTests\Freshness\Fresh04\Fresh04.java
Method is annotated Fresh.
For Java method not part of attribute calculation

Purity problem in category:FRESHRETURN
21:return link;of Freshness Maybe can't be returned from a @Fresh method because is not fresh because 
->20:link = L2 which which is an assignment of a value which effects the locality of (19_link:19_link:2)
->parameter L2 thats not assumed fresh on entry but only Maybe
->the variable link is initizised to new Link(L2, 0, 1) that's not fresh.
It's only of freshness Fresh
-> the constructor new Link(L2, 0, 1) is not Fresh
->parameter L2 thats not assumed fresh on entry but only Maybe.

in testfiles\PurityTests\Freshness\Fresh04\Fresh04.java:Fresh03

1 warnings generated

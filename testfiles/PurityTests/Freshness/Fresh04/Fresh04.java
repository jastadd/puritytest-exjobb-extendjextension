import lang.ast.SideEffect.*;
public class Fresh03{

    @Fresh public Link FreshX(@Fresh Link L){
        return L; 
    }
    
    @Pure public Link PureX(@Fresh Link L){
        return L; 
    }
    
    //Okay
    @Fresh public Link FreshX2(@Fresh Link L){
        Link link=new Link(L,0,1);
    	return link; 
    }
    
    @Fresh public Link FreshX3(@Fresh Link L,Link L2){
        Link link=new Link(L2,0,1);
        link = L2; 
    	return link; //Wrong (1)
    }
    
    @Fresh public Link FreshX4(@Fresh Link L,Link L2){
        Link link=new Link(L2,0,1);
        L = L2; // Wrong(2)
        link = L ;
    	return link; 
    }
    
    @Fresh public Link FreshX5(@Fresh Link L,Link L2){
        Link link=new Link(L2,0,1);
        L2 = L; 
        link = L2 ;
    	return link; 
    }
    
    @Fresh public Link FreshX6(@Fresh Link L,Link L2){
        Link link=new Link(L2,0,1);
        L.la=L2; // Wrong(3)
        link = L ; //Should be okay
    	return link; 
    }
 
    @Fresh public Link FreshX7(@Fresh Link L,Link L2){
        Link link=new Link(L2,0,1);
        L = L2; // Wrong(4)
        L.la=L2; //Wrong(5)
        L=new Link(L2,0,1);
        link = L ;
    	return link; 
    }
    
    @Fresh public Link FreshX7b(@Fresh Link L,Link L2){
        Link link=new Link(L2,0,1);
        L2 = L;
        L.la=link; 
        L=L2;
        link = L ;
    	return link; 
    }
    
    @Fresh public Link FreshX7b(@Fresh Link L,Link L2){
    Link link=new Link(L2,0,1);
    Link s;
    L2=L;
    Link s=L2;
    L = L2; 
    L.la=L2; 
	return s.la;
    }
    
    @Fresh public Link FreshX7c(@Fresh Link L,Link L2){
        Link link=new Link(L2,0,1);
        Link s;
        link=L2;
        L2=L;
        L.la=link; //Wrong(6)
        link.la=L; //Wrong(7)
        L = L2;
        s=L;
        L.la=L2; 
    	return s.la;
   }
        
    
    @Fresh public Link useA(){
        Link link=new Link();
        Link link2 = FreshX(link);
        return link2; 
    }
    
    @Fresh public Link useB(){
        Link link=new Link();
        link = FreshX(link);
        return link; 
    }
    
    
    class Link{
        int x;
        @Local Link la;
        
        @Fresh public Link(){
        	
        }
        
        public Link(@Fresh Link la){
        	this.la = La;
        }
        
        @Fresh public Link(@Fresh Link la,int a){
        x=a;
        this.la = la;
        }   
        
        @Fresh public Link(Link la,int a,int c){
        x=a;
        this.la = la; //Wrong(8)
        } 
    }
}
import lang.ast.SideEffect.*;
public class Fresh01{
	Link l;
	@Fresh public Link freshLink(){	
		return this;
	}
	@Fresh public Link freshLink2(){
		return this; 
	}
    
	@Fresh public Link freshLink3(){
		Fresh01 f=this;
		return f; 
	}
    
    class Link{
        int x;
        Link la;
    }
}
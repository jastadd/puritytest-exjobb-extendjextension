import lang.ast.SideEffect.*;
public class Fresh02{
	
	@Fresh public Link ParameterLink1(@Local Link p){
		Link l=p;
		Link f;
		f=l;
		return f; //Wrong!!
	}
	
	@Fresh public Link ParameterLink2(@Local Link p){
		Link l=p;
		Link f;
		f=p;
		f.x++; //Wrong!!
		return f; //Wrong!!
	}
	
	@Fresh public Link ParameterLink3(Link p){
		Link l=p;
		Link f=new Link();
		f=l;
		return f; //Wrong!!
	}
	
	@Fresh public Link ParameterLink4(Link p){
		Link l=new Link();
		Link f=p;
		f=l;
		return f; 
	}
	
	@Fresh public Link ParameterLink5(@Local Link p){
		Link link=p;
		return link;  //Wrong!!
	}
	
	@Fresh public Link ParameterLink6(@Ignore Link p){
		Link link=p;
		return link; 
	}
	
	@Fresh public Link ParameterLink7(@NonFresh Link p){
		Link link=p;
		return link; //Wrong!!
	}
	
	@Fresh public Link ParameterLink8(Link p){
		Link link=p;
		return link; //Wrong!!
	}
	
    
    class Link{
        int x;
        Link la;
    }
}
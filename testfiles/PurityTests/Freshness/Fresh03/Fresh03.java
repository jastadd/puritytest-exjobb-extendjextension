import lang.ast.SideEffect.*;
public class Fresh03{
	
	@Fresh public Link simple(){
		Link link=new Link();
		return link; 
	}
    
	@Fresh public Link caller01(){
		return simple(); 
	}
	
	@Fresh public Link caller02(){
		Link link=simple();
		return link; 
	}	
	
	@Fresh public Link caller03(){
		Link link;
		link=simple(); 
		return simple(); 
	}
	
	@Pure public Link noFresh(){
		return new Link();
	}
	
	@Fresh public Link caller04(){
		return noFresh(); 
	}
	
	@Fresh public Link caller05(){
		Link link=noFresh();
		return link; 
	}
	
	@Fresh public Link caller05(){
		Link link=noFresh();
		Link b=new Link();
		b=link;
		return b; 
	}
	
	
    
	
    class Link{
        int x;
        Link la;
        @Fresh public Link(){
        	x=10;
        }
    }
}
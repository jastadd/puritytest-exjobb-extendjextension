import lang.ast.SideEffect.*;
public class Fresh01{
	@Fresh public Link freshLink(){
		Link l=new Link();
		Link f=l.la;
		return f;
	}
	
	@Fresh public int[] freshIntArray(){
		return new int[4]; // Correct (This is a object after all {0,0,0,0}
	}
	
	@Fresh public int[] freshIntArray(){
		return new int[]{0,1,2,3}; // Correct (This is a object after all {0,0,0,0}
	}
	
	@Fresh public Link[] freshLinkArray(){
		return new Link[4]; // Correct 
	}

	@Fresh public Link[] freshLink(){
		Link[] links = new Link[4]; // Correct
		return links;
	}

	 Link laX;
	
	@Fresh public Link freshLink2(){
		Link l=new Link();
		//l.la=laX;
		Link f=l.la; // effectivly null!!
		return f;
	}
	
	@Fresh public Link freshLink3(){
		Link l=new Link();
		Link q=new Link();
		//l.la=laX;
		q=l.la;
		l.la=q;
		Link f=l.la; // effectivly null!!
		return f;
	}
    
	@Fresh public Link freshLink4c(){
		Link l=new Link();
		Link q=new Link();
		Link z=new Link();
		Link k=new Link();
		z=l.la;
		k.la=z;
		z=l;
		k.la.la=laX;
		q=l;
		z.la=laX;
		// effectivly null!!
		return l;
	}
	
	@Fresh public Link freshLink4(){
		Link l=new Link();
		Link q=new Link();
		Link z=new Link();
		l=z;
		z=z.la;
		q=z;
		// effectivly null!!
		return z;
	}
	
	
	
	@Fresh public Link freshLink4(){
		Link l=new Link();
		Link q=new Link();
		l.la=q;
		q=q.la;
		q=l.la;
		l.la=q;
		Link f=l.la; // effectivly null!!
		return f;
	}
	
	@Fresh public Link freshLink5(){
		Link l=new Link();
		Link q=new Link();
		q=l.la;
		l.la=l;
		l.la=q;
		Link f=l.la; // effectivly null!!
		return f;
	}
	
	@Fresh public Link freshLink6(){
		Link l=new Link();
		Link q=new Link();
		//l.la=laX;
		q=l.la; 
		l.la=q;
		Link f=l.la; // effectivly null!!
		return f;
	}
    
    class Link{
        int x;
        Link la=laX;
    }
}
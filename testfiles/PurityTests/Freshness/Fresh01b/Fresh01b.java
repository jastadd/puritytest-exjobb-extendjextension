import lang.ast.SideEffect.*;
public class Fresh01{
	@Fresh public Link freshLink(){
		Link l=new Link();
		Link f;
		return f;
	}
	
	@Fresh public int[] freshIntArray(){
		return new int[4]; // Correct (This is a object after all {0,0,0,0}
	}
	
	@Fresh public int[] freshIntArray(){
		return new int[]{0,1,2,3}; // Correct (This is a object after all {0,0,0,0}
	}
	
	@Fresh public Link[] freshLinkArray(){
		return new Link[4]; // Correct 
	}

	@Fresh public Link[] freshLink(){
		Link[] links = new Link[4]; // Correct
		return links;
	}

	
	@Fresh public Link freshLink2(){
		Link l=new Link();
		Link f; // effectivly null!!
		return f;
	}
    
    
    class Link{
        int x;
        Link la;
    }
}
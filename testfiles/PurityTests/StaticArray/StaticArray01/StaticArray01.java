import lang.ast.SideEffect.*;
public class StaticArray01{
	static int[] primArray;
	static String[] objArray;
	static AObj[] aArray;
	static AObj aObj;
	
	// Constructor Still Doesn't Make the Static fields writeable
	@Pure public StaticArray01(){
		primArray[1]=2; // Wrong Due to locality of static!
		primArray= new int[3]; //Wrong static field!
		primArray[0]=3;  // Wrong!
		aObj[1]=new AObj(); //Wrong! - Due to locality of static!
		aObj = new AObj[3]; // Wrong static field!
		aObj[2] = new AObj();  // Okay since now A Fresh Location!!
	}
	
}
public class AObj{
	int[] inner;
	AObj[] aArray;
	String[] delta;
	
	@Fresh public AObj(){
		
	}
	
	public AObj(int[] in, AObj[] aAr, String[] del){
		inner=in;
		aArray=aAr;
		this.delta=del;
	}
}
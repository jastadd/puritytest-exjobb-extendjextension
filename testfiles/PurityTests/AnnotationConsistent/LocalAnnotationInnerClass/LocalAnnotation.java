import lang.ast.SideEffect.*;

// LocalAnnotation Checks
public class LocalAnnotation{
	@Local static int[] SprimArray; 
	@Local static String[] SobjArray;
	@Local static AObj[] SaArray;
	@Local static AObj SaObj;
	@Local InnerA.Deeper deep;
	LocalAnnotation old;
	
	public class InnerA {
		@Local public InnerA() {
			
		}
		
		@FreshIf @Local public InnerA(int i) {
			
		}
		
		public class Deeper {
			@FreshIf public Deeper() {
				
			}
			
			public class Deepest {
				@FreshIf public Deepest() {
					
				}
			}
		}
	}
	
	@NonFresh public AObj a(){
		return SaObj; // Okey under Default Assumption of Entities (since aObj is entity And in Locality)
	}
	
	@NonFresh public AObj a(){
		return aObj; // Okey under Default Assumption of Entities (since aObj is entity And in Locality)
	}
	
	@Local public LocalAnnotation(){
		deep = new InnerA().new Deeper();
	}
	
	@Fresh public InnerA.Deeper innerDeep(){
		LocalAnnotation loc = new LocalAnnotation();
		return loc.new InnerA().new Deeper();
	}

	@Fresh public InnerA.Deeper innerDeep2(){
		LocalAnnotation loc = new LocalAnnotation();
		InnerA modified = old.new InnerA();
		return modified.new Deeper();
	}
	
	@Fresh public InnerA.Deeper innerDeep3(){
		LocalAnnotation loc = new LocalAnnotation();
		InnerA modified = old.new InnerA(2);
		return modified.new Deeper(); // modified.new Deeper();
	}
	
	@Fresh public InnerA.Deeper.Deepest innerDeep4(int i){
		LocalAnnotation loc = new LocalAnnotation();
		InnerA modified = null;
		if (i> 0) {
			modified = old.new InnerA(2);
		}else {
			modified = loc.new InnerA(2);
		}
		return modified.new Deeper().new Deepest(); // modified.new Deeper();
	}
	
	public @Entity class AObj{
		int[] inner;
		AObj[] aArray;
		String[] delta;
		//No Errors
		@Fresh public AObj(){
			
		}
		//No Errors
		public AObj(int[] in, AObj[] aAr, String[] del){
			inner=in;
			aArray=aAr;
			this.delta=del;
		}
	}
}
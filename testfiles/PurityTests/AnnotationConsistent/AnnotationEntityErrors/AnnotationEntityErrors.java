import lang.ast.SideEffect.*;

public class AnnotationEntityErrors{
	public EntityTest a;
	public NonEntity t;

	@Pure public AnnotationEntityErrors(EntityTest et){
		a = et; // okey!! unless denotated with fresh entity tests must be 
		t = new NonEntity(); 
	}
	
	@Pure public AnnotationEntityErrors(){
		a = new EntityTest(); // Okay !!
		t = new NonEntity(); 
	}
	
	@Pure public EntityTest returnEntity(EntityTest t){
		return t; 
	}
	// 2 errors
	@Pure public EntityTest returnEntity(){
		return new EntityTest(); //Wrong! Because Entity may only be return by fresh methods!!
		// Creation is also wrong !
	}
	
	
	@Pure public EntityTest returnEntity2(){
		EntityTest x= new EntityTest(); //Wrong! Because Entity may only be created by fresh methods!!
		return x; // Return fresh;
	}
	
	@Local public AnnotationEntityErrors(EntityTest et,int a){
		this.a = et; // okey!! unless annotated @Local 
		t = new NonEntity(); 
	}
	
	@Local public AnnotationEntityErrors(int a){
		this.a = new EntityTest();
		t = new NonEntity(); 
	}
	
	@Local public EntityTest returnEntityLocal(EntityTest t){
		return t; 
	}
	
	@Local public EntityTest returnEntityLocal(){
		return new EntityTest(); //Wrong! Because Entity may only be return by fresh methods!!
	}
	
	
	
}

@Entity public class EntityTest{
	
}

public class NonEntity{
	
}
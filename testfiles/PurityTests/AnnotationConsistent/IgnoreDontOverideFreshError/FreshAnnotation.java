import lang.ast.SideEffect.*;

public class AnnotationErrors {
	
	@Fresh final SuperA x1 = new SuperA();
	SuperA y1;
	static Super y2;
	static int counter = 0;

	//Not allowed since Freshness is still present.
	@NonFresh public SuperA TestMethod(){
		return a1();
	}
	
	@Fresh @Ignore public SuperA a1(){
		counter++;
		return x1;
	}
}

public class SuperA {
	
}

import lang.ast.SideEffect.*;

// Perfectly Allowed since Ignore
public class AnnotationErrors {
	
	@Fresh final SuperA x1 = new SuperA();
	SuperA y1;
	static Super y2;
	static int counter = 0;
	
	@Fresh public AnnotationErrors(){
		a1();
	}
	
	@Fresh @Ignore public SuperA a1(){
		counter++;
		return x1;
	}

}

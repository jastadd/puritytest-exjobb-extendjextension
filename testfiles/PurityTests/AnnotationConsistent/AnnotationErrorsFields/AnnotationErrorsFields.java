import lang.ast.SideEffect.*;

// All the annotation combinations on fields (All other shuld be invalid by java construction)
public class AnnotationErrorsFields{
	@Local int a; // 
	@Secret(group="b") int b=0;
	@Secret(group="b") @Local Integer c=null;
	@Secret(group="b") @Local @Fresh Integer d=null;
	@Secret(group="b") @Pure int e=3; // Wrong!!
	@Local Integer f;
	
	@Secret(group="asf") Integer ep; // Wrong not used!!
}
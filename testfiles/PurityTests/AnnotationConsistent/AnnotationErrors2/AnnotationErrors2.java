import lang.ast.SideEffect.*;

public class AnnotationErrors2{
	// Wrong due to combination
	@Fresh @NonFresh public EntityTest a1(){
		return new EntityTest(); //Wrong!!
	}
    
	@Fresh public EntityTest a2(){
		return new EntityTest(); // Okey!
	}
	
	@NonFresh public EntitTest a2(){
		return new EntityTest(); // Wrong!
	}
	
	@Fresh public EntityTest a3(@Fresh EntityTest a){
		return a; // Okey!
	}
	
	@NonFresh public EntitTest a4(@Fresh EntityTest a){
		return a; // Wrong!
	}
	
	@NonFresh public EntitTest a4(EntityTest a){
		return a; // Okey! Since EntityType
	}
	
	@Entity class EntityTest{
	}
}
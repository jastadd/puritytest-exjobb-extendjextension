import lang.ast.SideEffect.*;

public enum Test{
		A,B,C,D;
		
		@Fresh private Test() {
			//Allowed;
		}
	}

public enum TestD{
	A,B,C,D;
	static final int okayWithConstant = 1;
}


public enum TestP{
		A,B,C,D;
		
		@Pure private TestP() {
			//Allowed;
		}
	}

public enum TestL{
	A,B,C,D;
	
	@Local private TestL() {
		//Allowed;
	}
}

@Fresh public enum TestR{
	A,B,C,D;
	public static int x;
	@Local private int a;
	private int b;
	private AnnotationErrors c;
	
	@Pure private TestR() {
		//Wrong;
		x++;
		a = x;
	}
	
	@Ignore void increase(){
		x++;
	}

	@Pure void increaseP(){
		x++; //Not allowed
	}

	@Ignore void increaseA(){
		a++;
	}

	@Pure void increaseB(){
		a++; //Not allowed
	}

	@Local void increaseB(){
		a++;
	}

	@Local void increaseB(){
		b++;
	}

	@Local void increaseB(){
		c.i++; //Wrong
	}
} 

public enum BadEnum{
	A,B,C,D;
	public int myNum;
	//Not Fresh!!
	public setMyNum() {
		myNum++;
	}
	
	private BadEnum() {
		AnnotationErrors.s++; // Definitly not okay
	}
}
// Perfectly Correct Way Of using Secret
public class AnnotationErrors{
	public static int s;
	int i;
	
	@Fresh public Test A(){
		return Test.A; //Allowed
	}

	@Fresh public TestP B(){
		return TestP.A;
	}
	
	@Fresh public TestL C(){
		return TestL.A;
	}
	
	@Fresh public Test D(){
		return TestR.A; //Wrong!!
	}
	
	@Fresh public Test E(){
		return BadEnum.A; //Wrong!!
	}
	
	@Fresh public Test E(){
		return TestD.A;
	}
}
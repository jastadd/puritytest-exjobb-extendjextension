import lang.ast.SideEffect.*;

// Perfectly Correct Way Of using Secret
public class AnnotationErrors extends SuperA{
	
	@Fresh SuperA x1= new SuperA();
	@Fresh SuperA x2;
	@Fresh SuperA x3=null;
	@Fresh SuperA x4=y1;
	@Fresh SuperA x5=y2;
	static @Fresh SuperA x4=y1;
	SuperA y1;
	static Super y2;
	
	@Fresh public AnnotationErrors(){
		super();
	}
	
	@Fresh public AnnotationErrors(){
		super();
		return super.a1();
	}
	
	@Fresh public SuperA a1(){
		return super.a1();
	}

}

public class SuperA{
	@Fresh public SuperA(){
		
	}
	
	@Pure public SuperA a1(){
		return this;
	}
}
import lang.ast.SideEffect.*;

// Perfectly Correct Way Of using Secret
public class AnnotationErrors{
	
	// Redundant 1.
	@Fresh @Pure public AnnotationErrors(){
		
	}

	
	// Redundant 2.
	@Fresh @Local public AnnotationErrors(int a){
		
	}
	
	// Redundant and wrong pga no return fresh!! 3,4
	@Fresh @Pure public void a1(){
		
	}
	// Incompatible 5.
	@Pure @Local public void a2(){
		
	}
	
	@Ignore @Local public void a3(){
		
	}
	
	// Void type wrong 6.
	@Ignore @Fresh public void a4(){
		
	}
	
	@Ignore @Pure public void a5(){
		
	}
	
	@Ignore @Pure(group="X") public void  a6(){
		
	}
	
	// void type wrong 7,8.
    @Ignore @Fresh @Pure public void a7(){
		
	}
    // Compatible Okey 
   @Fresh public int[] a8(){
    	return new int[]{2,3};
    }
    
    // Wrong can't have Readonly without return anything
    @Pure public void a8(){
    }
    
}
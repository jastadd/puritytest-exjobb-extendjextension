import lang.ast.SideEffect.*;

// LocalAnnotation Checks
public class LocalAnnotation{
	@Local static int[] SprimArray; 
	@Local static String[] SobjArray;
	@Local static AObj[] SaArray;
	@Local static AObj SaObj;
	// 5-8 Wrong! (4 errors)
	@Local int[] primArray;
	@Local String[] objArray;
	@Local AObj[] aArray;
	@Local AObj aObj;
	
	@NonFresh public AObj a(){
		return SaObj; // Okey under Default Assumption of Entities (since aObj is entity And in Locality)
	}
	
	@NonFresh public AObj a(){
		return aObj; // Okey under Default Assumption of Entities (since aObj is entity And in Locality)
	}
	
	// NonFresh Constructor Erronius
	@NonFresh public LocalAnnotation(){
		
	}
	
	public @Entity class AObj{
		int[] inner;
		AObj[] aArray;
		String[] delta;
		//No Errors
		@Fresh public AObj(){
			
		}
		//No Errors
		public AObj(int[] in, AObj[] aAr, String[] del){
			inner=in;
			aArray=aAr;
			this.delta=del;
		}
	}
}
testfiles\PurityTests\ArrayNotOk\ArrayNot6\ArrayOk.java
Method is annotated Fresh.
For Java method not part of attribute calculation

Purity problem in category:FRESHRETURN
32:return myLA[3];of Freshness Maybe can't be returned from a @Fresh method because is not fresh because 
->29_myLA.[3] not containing fresh objects
->29_myLA
->the variable myLA is initizised to new LocalA[4] that's not fresh.
It's only of freshness Fresh.

in testfiles\PurityTests\ArrayNotOk\ArrayNot6\ArrayOk.java:Tester

1 warnings generated

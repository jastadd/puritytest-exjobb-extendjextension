import lang.ast.SideEffect.*;
public class LocalA{
	public int St=0;
    public LocalA p;
    @Local public LocalA a;
    @Local public LocalB b;
    @Pure public LocalA(){
    }
    
}

public class Tester{
	public int[] myA;
	public LocalA[] myLA;
	public String[] myS;
	
	@Fresh public Tester(int x){
		LocalA[] myLA = new LocalA[4];
		myLA[2]=new LocalA();
	}
	
	@Fresh public LocalA localTest(){
		LocalA[] myLA = new LocalA[4];
		myLA[2]=new LocalA();
		return myLA[2];
	}
	
	@Fresh public LocalA localTestA(){
		LocalA[] myLA = new LocalA[4]; //Local
		myLA[2]=new LocalA(); //Okey!!
		myLA[3]=p; //Okey!! //No longer deeply fresh myLA
		return myLA[3]; //No!!!
	}
}


import lang.ast.SideEffect.*;
public class LocalA{
	public int St=0;
    public LocalA p;
    @Local public LocalA a;
    @Local public LocalB b;
    @Pure public LocalA(){
    }
    
}

public class Tester{
	public int[] myA;
	public LocalA[] myLA;
	public String[] myS;
	
	@Pure public void localTest(){
		myA[1]=3; // 1
		myA[2]=myA[1]; // 2
		myA = new int[4]; //3
		myA[0]=myA[2]*myA[3];
		myA[1]=myA[3].length;
		myS[1]="3"; //4
		myS[2]=myS[1]; // 5
		myS = new String[4]; // 6
		myS[0]=myS[2]+myS[3];
		myS[1]=myS[3].length;
		myS[3]="hello "+"world";
	}
}


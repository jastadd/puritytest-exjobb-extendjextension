import lang.ast.SideEffect.*;
public class DFA02{
    int a;
    //Assignment :: Not okey
    @Fresh public void DFA(int z){
    	a=z;
    }
    
    // Trivially okey since x is Fresh by default.
    @Fresh public DFA02(int x){
    	a=x;
    }
    
    @Fresh public void DFA2(int z){
       int x = z;
    }
    
    @Fresh public void DFA3(int a){
    	this.a=a;
    }
    
    @Fresh public void DFA4(int a){
    	int z = a;
    	a=z;
    }
}
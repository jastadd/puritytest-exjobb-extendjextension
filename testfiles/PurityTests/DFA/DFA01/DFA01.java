import lang.ast.SideEffect.*;
public class DFA01{
    int a;
    //Assignment :: Not okey
    @Pure public void DFA(int z){
    	a=z;
    }
    
    // Trivially okey since x is Fresh by default.
    @Pure public DFA01(int x){
    	a=x;
    }
    
    @Pure public void DFA2(int z){
       int x = z;
    }
    
    @Pure public void DFA3(int a){
    	this.a=a;
    }
    
    @Pure public void DFA4(int a){
    	int z = a;
    	a=z;
    }
}
/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/fastTests\refine_rewrite\rewrite1;
import java.util.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\refine_rewrite\\rewrite1\\Small.ast:3
 * @astdecl B : ASTNode ::= <ID:String>;
 * @production B : {@link ASTNode} ::= <span class="component">&lt;ID:String&gt;</span>;

 */
public class B extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect Rewrite
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\refine_rewrite\\rewrite1\\rewrite1.jrag:13
   */
  int conf = 3;
  /**
   * @declaredat ASTNode:1
   */
  public B() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /**
   * @declaredat ASTNode:12
   */
  @ASTNodeAnnotation.Constructor(
    name = {"ID"},
    type = {"String"},
    kind = {"Token"}
  )
  public B(String p0) {
    setID(p0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:21
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:27
   */
  public boolean mayHaveRewrite() {
    return true;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:31
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    rewrittenNode_reset();
    called_reset();
    ImpureCond_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:42
   */
  @SideEffect.Ignore @SideEffect.Fresh public B clone() throws CloneNotSupportedException {
    B node = (B) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:47
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public B copy() {
    try {
      B node = (B) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:66
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public B fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:76
   */
  @SideEffect.Fresh(group="_ASTNode") public B treeCopyNoTransform() {
    B tree = (B) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:96
   */
  @SideEffect.Fresh(group="_ASTNode") public B treeCopy() {
    B tree = (B) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:110
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_ID == ((B) node).tokenString_ID);    
  }
  /**
   * Replaces the lexeme ID.
   * @param value The new value for the lexeme ID.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setID(String value) {
    tokenString_ID = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_ID;
  /**
   * Retrieves the value for the lexeme ID.
   * @return The value for the lexeme ID.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="ID")
  @SideEffect.Pure(group="_ASTNode") public String getID() {
    return tokenString_ID != null ? tokenString_ID : "";
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean called_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void called_reset() {
    called_computed = false;
    called_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean called_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int called_value;

  /**
   * @attribute syn
   * @aspect Rewrite
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\refine_rewrite\\rewrite1\\rewrite1.jrag:14
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Rewrite", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\refine_rewrite\\rewrite1\\rewrite1.jrag:14")
  @SideEffect.Pure(group="_ASTNode") public int called() {
    ASTState state = state();
    if (called_computed) {
      return called_value;
    }
    if (called_visited) {
      throw new RuntimeException("Circular definition of attribute B.called().");
    }
    called_visited = true;
    state().enterLazyAttribute();
    called_value = called_compute();
    called_computed = true;
    state().leaveLazyAttribute();
    called_visited = false;
    return called_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private int called_compute() {
  	int a=conf;
  	conf++;
  	return a;
  
  	}
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean ImpureCond_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void ImpureCond_reset() {
    ImpureCond_computed = false;
    ImpureCond_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean ImpureCond_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected int ImpureCond_value;

  /**
   * @attribute syn
   * @aspect Rewrite
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\refine_rewrite\\rewrite1\\rewrite1.jrag:20
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Rewrite", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\refine_rewrite\\rewrite1\\rewrite1.jrag:20")
  @SideEffect.Pure(group="_ASTNode") public int ImpureCond() {
    ASTState state = state();
    if (ImpureCond_computed) {
      return ImpureCond_value;
    }
    if (ImpureCond_visited) {
      throw new RuntimeException("Circular definition of attribute B.ImpureCond().");
    }
    ImpureCond_visited = true;
    state().enterLazyAttribute();
    ImpureCond_value = ImpureCond_compute();
    ImpureCond_computed = true;
    state().leaveLazyAttribute();
    ImpureCond_visited = false;
    return ImpureCond_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private int ImpureCond_compute() {
  	return conf;
  	}
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    // Declared at C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\refine_rewrite\\rewrite1\\rewrite1.jrag:7
    if (ImpureCond()>3) {
      return rewriteRule0();
    }
    return super.rewriteTo();
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\refine_rewrite\\rewrite1\\rewrite1.jrag:7
   * @apilevel internal
   */
  @SideEffect.Fresh(self=true) private D rewriteRule0() {
{
		return new D();
		}  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    // Declared at C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\refine_rewrite\\rewrite1\\rewrite1.jrag:7
    if (ImpureCond()>3) {
      return true;
    }
    return false;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void rewrittenNode_reset() {
    rewrittenNode_computed = false;
    rewrittenNode_initialized = false;
    rewrittenNode_value = null;
    rewrittenNode_cycle = null;
  }
/** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTState.Cycle rewrittenNode_cycle = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean rewrittenNode_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected ASTNode rewrittenNode_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean rewrittenNode_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="", declaredAt=":0")
  @SideEffect.Pure(group="_ASTNode") public ASTNode rewrittenNode() {
    if (rewrittenNode_computed) {
      return rewrittenNode_value;
    }
    ASTState state = state();
    if (!rewrittenNode_initialized) {
      rewrittenNode_initialized = true;
      rewrittenNode_value = this;
      if (rewrittenNode_value != null) {
        rewrittenNode_value.setParent(getParent());
      }
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        rewrittenNode_cycle = state.nextCycle();
        ASTNode new_rewrittenNode_value = rewrittenNode_value.rewriteTo();
        if (new_rewrittenNode_value != rewrittenNode_value || new_rewrittenNode_value.canRewrite()) {
          state.setChangeInCycle();
        }
        rewrittenNode_value = new_rewrittenNode_value;
        if (rewrittenNode_value != null) {
          rewrittenNode_value.setParent(getParent());
        }
      } while (state.testAndClearChangeInCycle());
      rewrittenNode_computed = true;
      state.startLastCycle();
      ASTNode $tmp = rewrittenNode_value.rewriteTo();

      state.leaveCircle();
    } else if (rewrittenNode_cycle != state.cycle()) {
      rewrittenNode_cycle = state.cycle();
      if (state.lastCycle()) {
        rewrittenNode_computed = true;
        ASTNode new_rewrittenNode_value = rewrittenNode_value.rewriteTo();
        return new_rewrittenNode_value;
      }
      ASTNode new_rewrittenNode_value = rewrittenNode_value.rewriteTo();
      if (new_rewrittenNode_value != rewrittenNode_value || new_rewrittenNode_value.canRewrite()) {
        state.setChangeInCycle();
      }
      rewrittenNode_value = new_rewrittenNode_value;
      if (rewrittenNode_value != null) {
        rewrittenNode_value.setParent(getParent());
      }
    } else {
    }
    return rewrittenNode_value;
  }
}

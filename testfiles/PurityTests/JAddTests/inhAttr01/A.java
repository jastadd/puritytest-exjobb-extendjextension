/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package lang.ast;
/**
 * @ast node
 * @declaredat D:\\Minimal\\backup Simplic\\src\\jastadd\\Simplic.ast:3
 * @production A : {@link ASTNode} ::= <span class="component">B1:{@link B}</span>;

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
  }
  /**
   * @declaredat ASTNode:13
   */
  public A(B p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:17
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:21
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    b2_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:26
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:30
   */
  @SideEffect.Fresh public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:35
   */
  @SideEffect.Fresh(group="_ASTNode") public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:54
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:64
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:84
   */
  @SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:98
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the B1 child.
   * @param node The new node to replace the B1 child.
   * @apilevel high-level
   */
  public void setB1(B node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the B1 child.
   * @return The current node used as the B1 child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="B1")
  @SideEffect.Pure public B getB1() {
    return (B) getChild(0);
  }
  /**
   * Retrieves the B1 child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the B1 child.
   * @apilevel low-level
   */
  @SideEffect.Pure public B getB1NoTransform() {
    return (B) getChildNoTransform(0);
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean b2_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void b2_reset() {
    b2_computed = false;
    
    b2_value = null;
    b2_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected boolean b2_computed = false;
  /** @apilevel internal */
  @SideEffect.Secret(group="_ASTNode") protected B b2_value;

  /**
   * @attribute syn
   * @aspect TypeAnalysis
   * @declaredat D:\\Minimal\\backup Simplic\\src\\jastadd\\Test.jrag:5
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="D:\\Minimal\\backup Simplic\\src\\jastadd\\Test.jrag:5")
  @SideEffect.Pure(group="_ASTNode") public B b2() {
    ASTState state = state();
    if (b2_computed) {
      return b2_value;
    }
    if (b2_visited) {
      throw new RuntimeException("Circular definition of attribute A.b2().");
    }
    b2_visited = true;
    state().enterLazyAttribute();
    b2_value = new B();
    b2_value.setParent(this);
    b2_computed = true;
    state().leaveLazyAttribute();
    b2_visited = false;
    return b2_value;
  }
  /**
   * @declaredat D:\\Minimal\\backup Simplic\\src\\jastadd\\Test.jrag:2
   * @apilevel internal
   */
 @SideEffect.Pure(group="_ASTNode") public int Define_attr(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == b2_value) {
      // @declaredat D:\\Minimal\\backup Simplic\\src\\jastadd\\Test.jrag:6
      return 2;
    }
    else if (_callerNode == getB1NoTransform()) {
      // @declaredat D:\\Minimal\\backup Simplic\\src\\jastadd\\Test.jrag:3
      return 1;
    }
    else {
      return getParent().Define_attr(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_attr(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
}

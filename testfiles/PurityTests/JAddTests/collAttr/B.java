/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.0 */
package testfiles/gen/fastTests\collAttr;
import java.util.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\collAttr\\Simplic.ast:4
 * @astdecl B : ASTNode ::= <ID:String> A*;
 * @production B : {@link ASTNode} ::= <span class="component">&lt;ID:String&gt;</span> <span class="component">{@link A}*</span>;

 */
public class B extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect Visitor
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\collAttr\\Visitor.jrag:35
   */
  public Object accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
  /**
   * @declaredat ASTNode:1
   */
  public B() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
    setChild(new List(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"ID", "A"},
    type = {"String", "List<A>"},
    kind = {"Token", "List"}
  )
  public B(String p0, List<A> p1) {
    setID(p0);
    setChild(p1, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:24
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:30
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:34
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:42
   */
  @SideEffect.Ignore @SideEffect.Fresh public B clone() throws CloneNotSupportedException {
    B node = (B) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:47
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public B copy() {
    try {
      B node = (B) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:66
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public B fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:76
   */
  @SideEffect.Fresh(group="_ASTNode") public B treeCopyNoTransform() {
    B tree = (B) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:96
   */
  @SideEffect.Fresh(group="_ASTNode") public B treeCopy() {
    B tree = (B) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:110
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_ID == ((B) node).tokenString_ID);    
  }
  /**
   * Replaces the lexeme ID.
   * @param value The new value for the lexeme ID.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setID(String value) {
    tokenString_ID = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_ID;
  /**
   * Retrieves the value for the lexeme ID.
   * @return The value for the lexeme ID.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="ID")
  @SideEffect.Pure(group="_ASTNode") public String getID() {
    return tokenString_ID != null ? tokenString_ID : "";
  }
  /**
   * Replaces the A list.
   * @param list The new list node to be used as the A list.
   * @apilevel high-level
   */
  public void setAList(List<A> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the A list.
   * @return Number of children in the A list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumA() {
    return getAList().getNumChild();
  }
  /**
   * Retrieves the number of children in the A list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the A list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumANoTransform() {
    return getAListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the A list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the A list.
   * @apilevel high-level
   */
  @SideEffect.Pure public A getA(int i) {
    return (A) getAList().getChild(i);
  }
  /**
   * Check whether the A list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasA() {
    return getAList().getNumChild() != 0;
  }
  /**
   * Append an element to the A list.
   * @param node The element to append to the A list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addA(A node) {
    List<A> list = (parent == null) ? getAListNoTransform() : getAList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addANoTransform(A node) {
    List<A> list = getAListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the A list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setA(A node, int i) {
    List<A> list = getAList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the A list.
   * @return The node representing the A list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="A")
  @SideEffect.Pure(group="_ASTNode") @SideEffect.FreshIf public List<A> getAList() {
    List<A> list = (List<A>) getChild(0);
    return list;
  }
  /**
   * Retrieves the A list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the A list.
   * @apilevel low-level
   */
  @SideEffect.Pure @SideEffect.FreshIf public List<A> getAListNoTransform() {
    return (List<A>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the A list without
   * triggering rewrites.
   */
  @SideEffect.Pure public A getANoTransform(int i) {
    return (A) getAListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the A list.
   * @return The node representing the A list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<A> getAs() {
    return getAList();
  }
  /**
   * Retrieves the A list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the A list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<A> getAsNoTransform() {
    return getAListNoTransform();
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean condition_visited = false;
  /**
   * @attribute syn
   * @aspect a
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\collAttr\\collAttr.jrag:5
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="a", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\collAttr\\collAttr.jrag:5")
  @SideEffect.Pure(group="_ASTNode") public boolean condition() {
    if (condition_visited) {
      throw new RuntimeException("Circular definition of attribute B.condition().");
    }
    condition_visited = true;
    boolean condition_value = true;
    condition_visited = false;
    return condition_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  @SideEffect.Pure public boolean canRewrite() {
    return false;
  }
  /** @apilevel internal */
  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_A_allchildren(A _root, @SideEffect.Ignore java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\collAttr\\collAttr.jrag:7
    if (condition()) {
      {
        java.util.Set<ASTNode> contributors = _map.get(_root);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) _root, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_A_allchildren(_root, _map);
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_A_allchildren(ArrayList<String> collection) {
    super.contributeTo_A_allchildren(collection);
    if (condition()) {
      collection.add(getID());
    }
  }
}

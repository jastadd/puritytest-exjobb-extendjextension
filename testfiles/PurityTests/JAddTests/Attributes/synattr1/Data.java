package testfiles/gen/fastTests\Attributes\synattr1;

/**
 * @ast class
 * @aspect synattr1
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Attributes\\synattr1\\synattr1.jrag:2
 */
 class Data extends java.lang.Object {
  
		public int number;

  
		public String id;

  
		public ASTNode creator;

  
		public Data(int i,String id,ASTNode creator){
			number = i;
			this.id = id;
			this.creator=creator;
		}


}

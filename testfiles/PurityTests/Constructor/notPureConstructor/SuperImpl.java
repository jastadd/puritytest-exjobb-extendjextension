import lang.ast.SideEffect.*;
// Parent!
public class Parent{
    public static int open;

    public Parent(int x){
    open=x;
    }
    
    @Public void dostuff(int x){
    	
    }
    
    public Parent(int x,int x){
    	dostuff(open++);
    }
    
}

public class subsubClass extends SubClass{
	int y;
	   public subsubClass(){
		   super();
	   }
	   
	   @Pure public subsubClass(int x){
		   super(x++,null); 
		   y=4;
	   }
	   
	   @Pure public subsubClass(int x,int k){
		   super(x++,k,null); //Not Pure behaviour!!
		   y=4;
	   }
	   
	   
	   @Fresh public Parent pv(){
		   new Parent(0,0); //Not Pure behaviour!!
		   return new Parent(0); //Not Pure behaviour!!
	   }
	   
	   @Fresh public Parent pv2(){
		   new Parent(0); //Not Pure behaviour!!
		   return new Parent(0,0); //Not Pure behaviour!!
	   }
	   
	   @Fresh public subsubClass oce(){
		   return new subsubClass(); 
	   }
}

public class SubClass extends Parent{
//Okey!!
	public Parent p;
	public SubClass(int x,int y, Parent p){
		super(x); //Wrong!!
		this.p=p;
	}
	
	@Pure public SubClass(int x,Parent p){
		super(x); //Wrong!!
		this.p=p;
	}
	//Override
	@Pure public int getOpen(){
		return Parent.open;
	}
	
	//Okey
	@Pure public int getOpenthis(){
		return ((SubClass)p).getOpen();
	}
	
}
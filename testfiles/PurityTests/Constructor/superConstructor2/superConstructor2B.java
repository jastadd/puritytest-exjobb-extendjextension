package p;
import lang.ast.SideEffect.*;
class Extender{
	public @Fresh Extender() { (new Outer()).super(); }
	public @Pure Extender(int i) { (new Outer(1)).super(); } //Should fail!!
	public @Pure Extender(int i,int k) { (new Outer.Inner()).super(1); } //Should fail!!
    public @Pure doex() { (new Outer()).super(); }
    public @Fresh doey() { (new Outer()).super(); }
    public @Local doez() { (new Outer()).super(); }
}

public class ExtenderA extends Outer{
  
}

public class ExtenderB extends Outer{
    public @Pure ExtenderB(int i,int k) { (new ExtenderA()).super(1); 
    } //Should fail!!
  
}
import lang.ast.SideEffect.*;
// Parent!
public class Parent{
    public static int open;

    public Parent(int x){
    open=x;
    }
    //Wrong!!
    @Fresh public Parent(int x,int y){
    	open++;
    }
    
    public int getOpen(){
    	return open++;
    }
}

public class SubClass extends Parent{
//Okey!!
	public Parent p;
	@Pure public SubClass(int x,Parent p){
		super(x); //Wrong!!
		this.p=p;
	}
	//Override
	@Pure public int getOpen(){
		return Parent.open;
	}
	
	//Wrong !!!
	@Pure public int getPOpen(){
		return p.getOpen();
	}
	
	//Wrong !!!
	@Pure public int getPOpen(){
		return ((Parent)this).getOpen();
	}
	
	//Okey
	@Pure public int getOpenthis(){
		return this.getOpen();
	}
	
	//Okey
	@Pure public int getOpenthis(){
		return ((SubClass)p).getOpen();
	}
	
}
import lang.ast.SideEffect.*;
// Parent!
public class Parent{
    public int open;

    @Impure public Parent(int x){
    open=x;
    }
    
    @Impure public Parent(int x,int x){
    }
    
    public Parent(){
    }
}

public class SubClass extends Parent{
//Okey!!
	public Parent p;
	@Pure public SubClass(int x,Parent p){
		super(x); //Wrong!!
		this.p=p;
	}
	
	@Pure public SubClass(int x,int y,Parent p){
		super(x,x); //Wrong!!
		this.p=p;
	}
	
	//Okey
	@pure public SubClass(){
		super();
	}
	
	@Pure public void classy(int x,int y,Parent p){
		new Parent(x,x); //Wrong!!
		new Parent(x); //Wrong!!
	}
}
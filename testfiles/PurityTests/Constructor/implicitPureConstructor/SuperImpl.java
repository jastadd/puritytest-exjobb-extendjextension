import lang.ast.SideEffect.*;
// Parent!
public class Parent{
    public int open;

    public Parent(int x){
    open=x;
    }
    
    public Parent(int x,int x){
    }
    
}

public class subsubClass extends SubClass{
	int y;
	   public subsubClass(){
		   super();
	   }
	   
	   @Pure public subsubClass(int x){
		   super(x++,null);
		   y=4;
	   }
	   
	   @Fresh public Parent pv(){
		   new Parent(0,0);
		   return new Parent(0); 
	   }
	   
	   @Fresh public Parent pv2(){
		   new Parent(0);
		   return new Parent(0,0); 
	   }
	   
	   @Fresh public subsubClass oce(){
		   return new subsubClass();
	   }
}

public class SubClass extends Parent{
//Okey!!
	public Parent p;
	@Pure public SubClass(int x,Parent p){
		super(x); //Wrong!!
		this.p=p;
	}
	//Override
	@Pure public int getOpen(){
		return Parent.open;
	}
	
	//Okey
	@Pure public int getOpenthis(){
		return ((SubClass)p).getOpen();
	}
	
}
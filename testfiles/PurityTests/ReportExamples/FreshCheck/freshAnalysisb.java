import lang.ast.SideEffect.*;
public class Test {
	@Local Test LT;
	int x;
	@Fresh public Test(int i) {
		x=i;
	}
	
	@Fresh public test FreshAnalysis(Test param){
		Test test = new Test(2);
		if (x > param.x) {
			if (x = 10) {
				test = new test(3);
				param = test;
			} else {
				test = LT;
			}
			test = param;
		} else {
			test = new test(5);
		}
		return test.LT; 
	}
}

 













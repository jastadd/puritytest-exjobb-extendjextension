import lang.ast.SideEffect.*;
public class Secret00{
	
    @Secret(group="group1") boolean a;
    @Secret(group="group2") boolean b;
    @Secret(group="group2") int c;
    private int open;

    // May not change the Secret fields
    @Pure(group="group2") public Secret00(int x){
    	open+= b ? 1 : 3;
    	open+= c;
    	open=x;
    }
    
    @Pure(group="group1") boolean Read2(int x){
    	return open > a;
    }
    
}

import lang.ast.SideEffect.*;
// Perfectly Correct Way Of using Secret
public class SecretTop{
	  @Secret(group="group1") boolean a=false;
}

public class Secret01 extends SecretTop{
    private int open;
    // May not change the Secret fields
    public Secret01(int x){
    open=x;
    }
    
    @Pure boolean Read(int x){
    return a;
    }
    
    //correct!!
    @Pure(group="group1") boolean Read2(int x){
    return a;
    }
    
}

import lang.ast.SideEffect.*;
public class Secret00{
	
    @Secret(group="group1") boolean a;
    @Secret(group="group2") boolean b;
    @Secret(group="group2") int c;
    private int open;

    // May not change the Secret fields
    @Pure(group="group2") public Secret00(int x){
    	c++;
    	b = x!=0;
    }
    
    @Pure(group="group1") boolean Read2(int x){
    	a = x!=0;
    	return a;
    }
    
    @Pure(group="group1") boolean Read3(int x){
    	a = x==0;
    	return a;
    }
    
}

import lang.ast.SideEffect.*;
import java.util.List;
// Perfectly Correct Way Of using Secret
public class SecretExam{

    @Secret(group="group1") boolean b;
    @Secret(group="group1") List<String> secretlist;
    @Secret(group="group2") int a; 
    @Secret(group="group3") boolean c;
    @Secret(group="group3") List<String> secretlist2;
    private int open;

    // May not change the Secret fields
    public SecretExam(int x){
    open=x;
    }
    // May change group2 fields and un annotated
    @Pure(group="group2") Secret(int x,int y){
    open=x;
    a=y;
    }
       @Pure(group="group1") public List<String> worker(){
    	if (b)
    	return secretlist;
    	secretlist=calcSecretA();  // Not Annotated As pure (Wrong Under no Infer) 1.
    	b=true;
    	return secretlist;
    }
    
    private List<String> calcSecretA(){
    	return new List<String>(); 
    }  
    
    @Pure(group="group3") public List<String> correct(){
    	if (c)
    	return secretlist2;
    	secretlist2=calcSecret();  
    	c=true;
    	return secretlist2;
    }
    
    @Fresh private List<String> calcSecret(){
    	return new List<String>(); 
    }  
    
     // May not read secret field
    int hello(int x,int y){
    open=10;
    a=y; // Error 2. 
    return a; //Error 3.
    }
    
    // Don't Need to have group annotation since 
    @Pure public List<String> Intresting(){
    	return correct();
    }
    
}
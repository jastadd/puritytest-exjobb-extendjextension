import lang.ast.SideEffect.*;
// Perfectly Correct Way Of using Secret
public class SecretCrossObject{
	
    @Secret(group="group1") boolean a=true;
    @Secret(group="group1") SecretA secret=null;
    private int open;

    // May not change the Secret fields
    public SecretCrossObject(int x){
    open=x;
    }
    
    //okey Error in errorgetB!!
    @Pure(group="group1") int CrossRead(int x){
    return secret.errorgetB();
    }
    
    //correct!!
    @Pure(group="group1") int CrossRead2(int x){
    return secret.getB();
    }
    
    // Read secret from a different unrelated type -not okey
    @Pure(group="group1") int CrossRead3(int x){
    return secret.b; //Error
    }
    // Still read of secret in other object.
    @Pure(group="extra") int CrossRead3(int x){
    return secret.b; //2 Error 
    }
    
}

public class SecretA{
	 @Secret(group="extra") public int b=0;
	@Pure(group="extra") public int getB(){
		return b;
	}
	@Pure public int errorgetB(){
		return b; //Error
	}	
	
}
import lang.ast.SideEffect.*;
public class Secret05{
	
	static final @Fresh Secret05 deo;
	
    @Secret(group="group1") @Fresh Secret05 a=deo; 
    @Secret(group="group1") @Fresh Secret05 c=new Secret(0); 
    private int open;

    // May not change the Secret fields
    @Pure(group="group1") public Secret05(int x){
    	c=new Secret(0); 
    }
    
    @Pure(group="group1") boolean Read2(int x){
    	c=new Secret(0);
    	a=deo; 
    	return a;
    }
    
    @Pure(group="group1") boolean Read3(int x){
    	c=new Secret(0); 
    	return a;
    }
    
}

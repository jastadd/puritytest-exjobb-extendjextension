import lang.ast.SideEffect.*;
import lang.ast.*;
// May not read multiple levels
public class SecretReadAcross{
	
    @Secret(group="group1") boolean a=false;
    @Secret(group="group1") SecretReadAcross s=null;
    private @Secret(group="group1") ListM<SecretReadAcross> s2=null;
    private int open;

    public class ListM<T>{
    	T d;
    	@Pure public T getM(int a){
    		return d;
    	}
    }
    
    // May not change or read the Secret fields or any field via pointer from secret field
    public SecretReadAcross(int x){
    open=x;
    a=true; //Wrong!
    s.a =true; // Wrong!
    s=new SecretReadAcross(4); //Wrong!
    }
    
    //correct!!
    @Pure(group="group1") boolean Read(int x){
    return a || (s !=null);
    }
    
    //Wrong!!
    @Pure(group="group1") boolean Read2(int x){
    return s.a; // Wrong Access secret field in another object // Only valid to return Secret via the object
    }
    @Pure(group="group1") boolean Read2b(int x){
    return s2.getM(0); // Wrong Access secret field in another object // Only valid to return Secret via the object
    }
    @Fresh(group="group1") SecretReadAcross Read2b(int x){
    return s2.getM(0); // Wrong Access secret field in another object // Only valid to return Secret via the object
    }
}

import lang.ast.SideEffect.*;
public class Secret05{
	
	static Secret05 deo;
    @Secret(group="group1") @Fresh Secret05 a=deo; //Wrong
    @Secret(group="group1") @Fresh Secret05 c=this; //Wrong
    private int open;

    // May not change the Secret fields
    @Pure(group="group2") public Secret05(int x){
    	c=this; //Wrong!
    }
    
    @Pure(group="group1") boolean Read2(int x){
    	c=this;//Wrong
    	a=this; //Wrong
    	return a;
    }
    
    @Pure(group="group1") boolean Read3(int x){
    	c=this; //Wrong
    	return a;
    }
    
}

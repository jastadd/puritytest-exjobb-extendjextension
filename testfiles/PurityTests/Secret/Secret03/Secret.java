import lang.ast.SideEffect.*;
import lang.ast.*;
// Perfectly Correct Way Of using Secret
public class SecretX{
	
    @Secret(group="group1") boolean a=false;
    private int open;

    // May not change the Secret fields
     public SecretX(int x){
     open=x;
     }
    
    // Error 
    @Pure boolean Read(int x){
     return a;
    }
    
    //correct!!
    @Pure(group="group1") boolean Read2(int x){
    return a;
    }
    
}

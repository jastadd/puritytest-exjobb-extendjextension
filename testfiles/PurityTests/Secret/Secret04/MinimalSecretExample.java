class MinimalSecretExample {
  @Secret("A") int field;
  @Pure("A") void method() {field=field+1;}
}

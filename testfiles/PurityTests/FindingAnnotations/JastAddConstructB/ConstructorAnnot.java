package test;
import lang.ast.SideEffect.*;
public class ConstructorAnnot{
	@ASTNodeAnnotation.Constructor(
		name = {"Cond", "ID"},
		type = {"Expr", "String"},
		kind = {"Token", "Token"}
		//Token is not assumed Fresh since it should only be set by compiler.
	) public ConstructorAnnot( Expr p0, String p1) {
		setChild(p0, 0);
		setID(p1);
	}
	
	@ASTNodeAnnotation.Constructor(
			name = {"Cond", "ID"},
			type = {"Expr", "String"},
			kind = {"Token", "Token"}
		)
	//Token is not assumed Fresh since it should only be set by compiler.
	@Fresh public Expr M(Expr p0, String p1){
		setChild(p0,0);
		setChildB(p0,p0,0);
		return p0;
	}
  
	@Fresh public Expr setChildB(@Fresh Expr p0,@Fresh Expr p1, int i){
		  return p0;
	}
  
	
	@Pure public void setChild(@Fresh Expr p0,int i){
	  
	}
  
	@Pure public void setID(Object obj){
	  
	}
}

public class Expr{
	
}
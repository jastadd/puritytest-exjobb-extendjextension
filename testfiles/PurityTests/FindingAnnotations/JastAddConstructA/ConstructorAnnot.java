package test;
import lang.ast.SideEffect.*;
public class ConstructorAnnot{
	@ASTNodeAnnotation.Constructor(
		name = {"Cond", "ID"},
		type = {"Expr", "String"},
		kind = {"Child", "Token"}
	) public ConstructorAnnot( Expr p0, String p1) {
		setChild(p0, 0);
		setID(p1);
	}
	
	@ASTNodeAnnotation.Constructor(
			name = {"Cond", "ID"},
			type = {"Expr", "String"},
			kind = {"Child", "Token"}
		)
	
	// Problem Function triggers 3 errors.
	@Fresh public Expr M(Expr p0, String p1){
		setChild(p0,0); // Warning
		setChildB(p0,p0,0); // Warning
		return p0; //Warning
	}
  
	@Fresh public Expr setChildB(@Fresh Expr p0,@Fresh Expr p1, int i){
		  return p0;
	}
  
	
	@Pure public void setChild(@Fresh Expr p0,int i){
	  
	}
  
	@Pure public void setID(Object obj){
	  
	}
}

public class Expr{
	
}
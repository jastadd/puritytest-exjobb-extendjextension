package test;
import lang.ast.SideEffect.*;
public class ConstructorAnnot{
	@ASTNodeAnnotation.Constructor(
		name = {"Cond", "ID"},
		type = {"Expr", "String"},
		kind = {"Child", "Token"}
	)
	public ConstructorAnnot(Expr p0, String p1) {
		setChild(p0, 0);
		p0.localChange();
		setID(p1);
	}
  
	@Pure public void setChild(@Fresh Expr p0,int i){
	  
	}
  
	@Pure public void setID(Object obj){
	  
	}
}

@Entity public class Expr{
	@Local public void localChange(){
		
	}
}
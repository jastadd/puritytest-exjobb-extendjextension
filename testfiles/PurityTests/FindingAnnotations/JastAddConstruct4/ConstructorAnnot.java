package test;
import lang.ast.SideEffect.*;
public class ConstructorAnnot{
	@ASTNodeAnnotation.Constructor(
		name = {"Cond", "ID"},
		type = {"Expr", "String"},
		kind = {"Child", "Token"}
	)
	public ConstructorAnnot(Expr p0, Expr p1,Expr1 p2) {
		setChild(p0, 0);
		p1.localChange(); //Wrong
		p2.localChange();//Wrong
		setChild(p1, 1); //Wrong
		p0.localChange();
		setID(p1);
	}
  
	@Pure public void setChild(@Fresh Expr p0,int i){
	  
	}
  
	@Pure public void setID(Object obj){
	  
	}
}

public class Expr1{
	@Local public void localChange(){
		
	}
}

@Entity public class Expr{
	@Local public void localChange(){
		
	}
}
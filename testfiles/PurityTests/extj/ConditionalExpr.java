/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package org.extendj.ast;
import java.util.HashSet;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;
import java.util.Set;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import beaver.*;
import java.util.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.jastadd.util.*;
import java.util.zip.*;
import java.io.*;
import org.jastadd.util.PrettyPrintable;
import org.jastadd.util.PrettyPrinter;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\grammar\\Java.ast:186
 * @production ConditionalExpr : {@link Expr} ::= <span class="component">Condition:{@link Expr}</span> <span class="component">TrueExpr:{@link Expr}</span> <span class="component">FalseExpr:{@link Expr}</span>;

 */
public class ConditionalExpr extends Expr implements Cloneable {
  /**
   * @aspect Java4PrettyPrint
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\PrettyPrint.jadd:246
   */
  public void prettyPrint(PrettyPrinter out) {
    out.print(getCondition());
    out.print(" ? ");
    out.print(getTrueExpr());
    out.print(" : ");
    out.print(getFalseExpr());
  }
  /**
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\backend\\CreateBCode.jrag:1188
   */
  public void createBCode(CodeGeneration gen) {
    int end_label = -1;
    int false_label = -1;
    if (!getCondition().isConstant()) {
      false_label = gen.constantPool().newLabel();
      getCondition().branchFalse(gen, false_label);
    }
    if (getCondition().canBeTrue()) {
      getTrueExpr().createBCode(gen);
      getTrueExpr().type().emitCastTo(gen, type());
      if (getCondition().canBeFalse()) {
        end_label = gen.constantPool().newLabel();
        gen.emitGoto(end_label);
        gen.changeStackDepth(-type().variableSize());
      }
    }
    if (false_label != -1) {
      gen.addLabel(false_label);
    }
    if (getCondition().canBeFalse()) {
      getFalseExpr().createBCode(gen);
      getFalseExpr().type().emitCastTo(gen, type());
    }
    if (end_label != -1) {
      gen.addLabel(end_label);
    }
  }
  /**
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\backend\\CreateBCode.jrag:1279
   */
  public void branchTrue(CodeGeneration gen, int target) {
    // Branch when true.
    int else_label = -1;
    int end_label = -1;
    if (!getCondition().isConstant()) {
      else_label = gen.constantPool().newLabel();
      getCondition().branchFalse(gen, else_label);
    }
    if (getCondition().canBeTrue()) {
      getTrueExpr().branchTrue(gen, target);
      if (getCondition().canBeFalse() && getTrueExpr().canBeFalse()) {
        end_label = gen.constantPool().newLabel();
        gen.emitGoto(end_label);
        //gen.GOTO(end_label);
      }
    }
    if (else_label != -1) {
      gen.addLabel(else_label);
    }
    if (getCondition().canBeFalse()) {
      getFalseExpr().branchTrue(gen, target);
    }
    if (end_label != -1) {
      gen.addLabel(end_label);
    }
  }
  /**
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\backend\\CreateBCode.jrag:1391
   */
  public void branchFalse(CodeGeneration gen, int target) {
    // Branch when false.
    int else_label = -1;
    int end_label = -1;
    if (!getCondition().isConstant()) {
      else_label = gen.constantPool().newLabel();
      getCondition().branchFalse(gen, else_label);
    }
    if (getCondition().canBeTrue()) {
      getTrueExpr().branchFalse(gen, target);
      if (getCondition().canBeFalse() && getTrueExpr().canBeTrue()) {
        end_label = gen.constantPool().newLabel();
        gen.emitGoto(end_label);
      }
    }
    if (else_label != -1) {
      gen.addLabel(else_label);
    }
    if (getCondition().canBeFalse()) {
      getFalseExpr().branchFalse(gen, target);
    }
    if (end_label != -1) {
      gen.addLabel(end_label);
    }
  }
  /**
   * @aspect AutoBoxingCodegen
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\backend\\AutoBoxingCodegen.jrag:490
   */
  public void emitBooleanCondition(CodeGeneration gen) {
    super.emitBooleanCondition(gen);
    if (type().isReferenceType()) {
      type().emitBoxingOperation(gen);
    }
  }
  /**
   * @declaredat ASTNode:1
   */
  public ConditionalExpr() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[3];
  }
  /**
   * @declaredat ASTNode:13
   */
  public ConditionalExpr(Expr p0, Expr p1, Expr p2) {
    setChild(p0, 0);
    setChild(p1, 1);
    setChild(p2, 2);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:19
   */
  @SideEffect.Pure protected int numChildren() {
    return 3;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:25
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:29
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    constant_reset();
    isConstant_reset();
    booleanOperator_reset();
    unassignedAfterTrue_Variable_reset();
    unassignedAfterFalse_Variable_reset();
    unassignedAfter_Variable_reset();
    type_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:44
   */
  @SideEffect.Fresh public ConditionalExpr clone() throws CloneNotSupportedException {
    ConditionalExpr node = (ConditionalExpr) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:49
   */
  @SideEffect.Fresh(group="_ASTNode") public ConditionalExpr copy() {
    try {
      ConditionalExpr node = (ConditionalExpr) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:68
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public ConditionalExpr fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:78
   */
  @SideEffect.Fresh(group="_ASTNode") public ConditionalExpr treeCopyNoTransform() {
    ConditionalExpr tree = (ConditionalExpr) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:98
   */
  @SideEffect.Fresh(group="_ASTNode") public ConditionalExpr treeCopy() {
    ConditionalExpr tree = (ConditionalExpr) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:112
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the Condition child.
   * @param node The new node to replace the Condition child.
   * @apilevel high-level
   */
  public void setCondition(Expr node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Condition child.
   * @return The current node used as the Condition child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Condition")
  @SideEffect.Pure public Expr getCondition() {
    return (Expr) getChild(0);
  }
  /**
   * Retrieves the Condition child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Condition child.
   * @apilevel low-level
   */
  @SideEffect.Pure public Expr getConditionNoTransform() {
    return (Expr) getChildNoTransform(0);
  }
  /**
   * Replaces the TrueExpr child.
   * @param node The new node to replace the TrueExpr child.
   * @apilevel high-level
   */
  public void setTrueExpr(Expr node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the TrueExpr child.
   * @return The current node used as the TrueExpr child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="TrueExpr")
  @SideEffect.Pure public Expr getTrueExpr() {
    return (Expr) getChild(1);
  }
  /**
   * Retrieves the TrueExpr child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the TrueExpr child.
   * @apilevel low-level
   */
  @SideEffect.Pure public Expr getTrueExprNoTransform() {
    return (Expr) getChildNoTransform(1);
  }
  /**
   * Replaces the FalseExpr child.
   * @param node The new node to replace the FalseExpr child.
   * @apilevel high-level
   */
  public void setFalseExpr(Expr node) {
    setChild(node, 2);
  }
  /**
   * Retrieves the FalseExpr child.
   * @return The current node used as the FalseExpr child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="FalseExpr")
  @SideEffect.Pure public Expr getFalseExpr() {
    return (Expr) getChild(2);
  }
  /**
   * Retrieves the FalseExpr child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the FalseExpr child.
   * @apilevel low-level
   */
  @SideEffect.Pure public Expr getFalseExprNoTransform() {
    return (Expr) getChildNoTransform(2);
  }
  /**
   * @aspect AutoBoxing
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\AutoBoxing.jrag:237
   */
  private TypeDecl refined_AutoBoxing_ConditionalExpr_type()
{
    TypeDecl second = getTrueExpr().type();
    TypeDecl third = getFalseExpr().type();

    if (second == third) {
      return second;
    } else if (second.isReferenceType() && third.isNull()) {
      return second;
    } else if (second.isNull() && third.isReferenceType()) {
      return third;
    }

    if (second.isPrimitiveType()) {
      if (!second.isNull() && third.isNull()) {
        return second;
      }
    } else if (!second.unboxed().isUnknown()) {
      second = second.unboxed();
    }

    if (third.isPrimitiveType()) {
      if (!third.isNull() && second.isNull()) {
        return third;
      }
    } else if (!third.unboxed().isUnknown()) {
      third = third.unboxed();
    }

    return conditionalExprType(second, third);
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void constant_reset() {
    constant_computed = null;
    constant_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="constant") protected ASTState.Cycle constant_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="constant") protected Constant constant_value;

  /**
   * @attribute syn
   * @aspect ConstantExpression
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ConstantExpression.jrag:85
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantExpression", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ConstantExpression.jrag:85")
  @SideEffect.Pure(group="constant") public Constant constant() {
    ASTState state = state();
    if (constant_computed == ASTState.NON_CYCLE || constant_computed == state().cycle()) {
      return constant_value;
    }
    constant_value = type().questionColon(getCondition().constant(),
              getTrueExpr().constant(),getFalseExpr().constant());
    if (state().inCircle()) {
      constant_computed = state().cycle();
    
    } else {
      constant_computed = ASTState.NON_CYCLE;
    
    }
    return constant_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void isConstant_reset() {
    isConstant_computed = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="isConstant") protected ASTState.Cycle isConstant_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="isConstant") protected boolean isConstant_value;

  /**
   * @attribute syn
   * @aspect ConstantExpression
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ConstantExpression.jrag:406
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantExpression", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ConstantExpression.jrag:406")
  @SideEffect.Pure(group="isConstant") public boolean isConstant() {
    ASTState state = state();
    if (isConstant_computed == ASTState.NON_CYCLE || isConstant_computed == state().cycle()) {
      return isConstant_value;
    }
    isConstant_value = getCondition().isConstant() && getTrueExpr().isConstant() && getFalseExpr().isConstant();
    if (state().inCircle()) {
      isConstant_computed = state().cycle();
    
    } else {
      isConstant_computed = ASTState.NON_CYCLE;
    
    }
    return isConstant_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void booleanOperator_reset() {
    booleanOperator_computed = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="booleanOperator") protected ASTState.Cycle booleanOperator_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="booleanOperator") protected boolean booleanOperator_value;

  /**
   * @attribute syn
   * @aspect DefiniteAssignment
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:253
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="DefiniteAssignment", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:253")
  @SideEffect.Pure(group="booleanOperator") public boolean booleanOperator() {
    ASTState state = state();
    if (booleanOperator_computed == ASTState.NON_CYCLE || booleanOperator_computed == state().cycle()) {
      return booleanOperator_value;
    }
    booleanOperator_value = getTrueExpr().type().isBoolean() && getFalseExpr().type().isBoolean();
    if (state().inCircle()) {
      booleanOperator_computed = state().cycle();
    
    } else {
      booleanOperator_computed = ASTState.NON_CYCLE;
    
    }
    return booleanOperator_value;
  }
  /**
   * @attribute syn
   * @aspect DefiniteAssignment
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:378
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="DefiniteAssignment", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:378")
  @SideEffect.Pure(group="assignedAfterTrue_Variable") public boolean assignedAfterTrue(Variable v) {
    boolean assignedAfterTrue_Variable_value = isFalse() || (getTrueExpr().assignedAfterTrue(v) && getFalseExpr().assignedAfterTrue(v));
    return assignedAfterTrue_Variable_value;
  }
  /**
   * @attribute syn
   * @aspect DefiniteAssignment
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:380
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="DefiniteAssignment", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:380")
  @SideEffect.Pure(group="assignedAfterFalse_Variable") public boolean assignedAfterFalse(Variable v) {
    boolean assignedAfterFalse_Variable_value = isTrue() || (getTrueExpr().assignedAfterFalse(v) && getFalseExpr().assignedAfterFalse(v));
    return assignedAfterFalse_Variable_value;
  }
  /**
   * @attribute syn
   * @aspect DefiniteAssignment
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:268
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="DefiniteAssignment", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:268")
  @SideEffect.Pure(group="assignedAfter_Variable") public boolean assignedAfter(Variable v) {
    boolean assignedAfter_Variable_value = booleanOperator()
          ? assignedAfterTrue(v) && assignedAfterFalse(v)
          : getTrueExpr().assignedAfter(v) && getFalseExpr().assignedAfter(v);
    return assignedAfter_Variable_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void unassignedAfterTrue_Variable_reset() {
    unassignedAfterTrue_Variable_values = null;
  }
  @SideEffect.Secret(group="unassignedAfterTrue_Variable") protected java.util.Map unassignedAfterTrue_Variable_values;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="DefiniteUnassignment", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:909")
  @SideEffect.Pure(group="unassignedAfterTrue_Variable") public boolean unassignedAfterTrue(Variable v) {
    Object _parameters = v;
    if (unassignedAfterTrue_Variable_values == null) unassignedAfterTrue_Variable_values = new java.util.HashMap(4);
    ASTState.CircularValue _value;
    if (unassignedAfterTrue_Variable_values.containsKey(_parameters)) {
      Object _cache = unassignedAfterTrue_Variable_values.get(_parameters);
      if (!(_cache instanceof ASTState.CircularValue)) {
        return (Boolean) _cache;
      } else {
        _value = (ASTState.CircularValue) _cache;
      }
    } else {
      _value = new ASTState.CircularValue();
      unassignedAfterTrue_Variable_values.put(_parameters, _value);
      _value.value = true;
    }
    ASTState state = state();
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      boolean new_unassignedAfterTrue_Variable_value;
      do {
        _value.cycle = state.nextCycle();
        new_unassignedAfterTrue_Variable_value = getTrueExpr().unassignedAfterTrue(v) && getFalseExpr().unassignedAfterTrue(v);
        if (new_unassignedAfterTrue_Variable_value != ((Boolean)_value.value)) {
          state.setChangeInCycle();
          _value.value = new_unassignedAfterTrue_Variable_value;
        }
      } while (state.testAndClearChangeInCycle());
      unassignedAfterTrue_Variable_values.put(_parameters, new_unassignedAfterTrue_Variable_value);

      state.leaveCircle();
      return new_unassignedAfterTrue_Variable_value;
    } else if (_value.cycle != state.cycle()) {
      _value.cycle = state.cycle();
      boolean new_unassignedAfterTrue_Variable_value = getTrueExpr().unassignedAfterTrue(v) && getFalseExpr().unassignedAfterTrue(v);
      if (new_unassignedAfterTrue_Variable_value != ((Boolean)_value.value)) {
        state.setChangeInCycle();
        _value.value = new_unassignedAfterTrue_Variable_value;
      }
      return new_unassignedAfterTrue_Variable_value;
    } else {
      return (Boolean) _value.value;
    }
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void unassignedAfterFalse_Variable_reset() {
    unassignedAfterFalse_Variable_values = null;
  }
  @SideEffect.Secret(group="unassignedAfterFalse_Variable") protected java.util.Map unassignedAfterFalse_Variable_values;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="DefiniteUnassignment", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:911")
  @SideEffect.Pure(group="unassignedAfterFalse_Variable") public boolean unassignedAfterFalse(Variable v) {
    Object _parameters = v;
    if (unassignedAfterFalse_Variable_values == null) unassignedAfterFalse_Variable_values = new java.util.HashMap(4);
    ASTState.CircularValue _value;
    if (unassignedAfterFalse_Variable_values.containsKey(_parameters)) {
      Object _cache = unassignedAfterFalse_Variable_values.get(_parameters);
      if (!(_cache instanceof ASTState.CircularValue)) {
        return (Boolean) _cache;
      } else {
        _value = (ASTState.CircularValue) _cache;
      }
    } else {
      _value = new ASTState.CircularValue();
      unassignedAfterFalse_Variable_values.put(_parameters, _value);
      _value.value = true;
    }
    ASTState state = state();
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      boolean new_unassignedAfterFalse_Variable_value;
      do {
        _value.cycle = state.nextCycle();
        new_unassignedAfterFalse_Variable_value = getTrueExpr().unassignedAfterFalse(v) && getFalseExpr().unassignedAfterFalse(v);
        if (new_unassignedAfterFalse_Variable_value != ((Boolean)_value.value)) {
          state.setChangeInCycle();
          _value.value = new_unassignedAfterFalse_Variable_value;
        }
      } while (state.testAndClearChangeInCycle());
      unassignedAfterFalse_Variable_values.put(_parameters, new_unassignedAfterFalse_Variable_value);

      state.leaveCircle();
      return new_unassignedAfterFalse_Variable_value;
    } else if (_value.cycle != state.cycle()) {
      _value.cycle = state.cycle();
      boolean new_unassignedAfterFalse_Variable_value = getTrueExpr().unassignedAfterFalse(v) && getFalseExpr().unassignedAfterFalse(v);
      if (new_unassignedAfterFalse_Variable_value != ((Boolean)_value.value)) {
        state.setChangeInCycle();
        _value.value = new_unassignedAfterFalse_Variable_value;
      }
      return new_unassignedAfterFalse_Variable_value;
    } else {
      return (Boolean) _value.value;
    }
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void unassignedAfter_Variable_reset() {
    unassignedAfter_Variable_values = null;
  }
  @SideEffect.Secret(group="unassignedAfter_Variable") protected java.util.Map unassignedAfter_Variable_values;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="DefiniteUnassignment", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:903")
  @SideEffect.Pure(group="unassignedAfter_Variable") public boolean unassignedAfter(Variable v) {
    Object _parameters = v;
    if (unassignedAfter_Variable_values == null) unassignedAfter_Variable_values = new java.util.HashMap(4);
    ASTState.CircularValue _value;
    if (unassignedAfter_Variable_values.containsKey(_parameters)) {
      Object _cache = unassignedAfter_Variable_values.get(_parameters);
      if (!(_cache instanceof ASTState.CircularValue)) {
        return (Boolean) _cache;
      } else {
        _value = (ASTState.CircularValue) _cache;
      }
    } else {
      _value = new ASTState.CircularValue();
      unassignedAfter_Variable_values.put(_parameters, _value);
      _value.value = true;
    }
    ASTState state = state();
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      boolean new_unassignedAfter_Variable_value;
      do {
        _value.cycle = state.nextCycle();
        new_unassignedAfter_Variable_value = booleanOperator()
              ? unassignedAfterTrue(v) && unassignedAfterFalse(v)
              : getTrueExpr().unassignedAfter(v) && getFalseExpr().unassignedAfter(v);
        if (new_unassignedAfter_Variable_value != ((Boolean)_value.value)) {
          state.setChangeInCycle();
          _value.value = new_unassignedAfter_Variable_value;
        }
      } while (state.testAndClearChangeInCycle());
      unassignedAfter_Variable_values.put(_parameters, new_unassignedAfter_Variable_value);

      state.leaveCircle();
      return new_unassignedAfter_Variable_value;
    } else if (_value.cycle != state.cycle()) {
      _value.cycle = state.cycle();
      boolean new_unassignedAfter_Variable_value = booleanOperator()
            ? unassignedAfterTrue(v) && unassignedAfterFalse(v)
            : getTrueExpr().unassignedAfter(v) && getFalseExpr().unassignedAfter(v);
      if (new_unassignedAfter_Variable_value != ((Boolean)_value.value)) {
        state.setChangeInCycle();
        _value.value = new_unassignedAfter_Variable_value;
      }
      return new_unassignedAfter_Variable_value;
    } else {
      return (Boolean) _value.value;
    }
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void type_reset() {
    type_computed = null;
    type_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="type") protected ASTState.Cycle type_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="type") protected TypeDecl type_value;

  /**
   * @attribute syn
   * @aspect TypeAnalysis
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeAnalysis.jrag:296
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeAnalysis.jrag:296")
  @SideEffect.Pure(group="type") public TypeDecl type() {
    ASTState state = state();
    if (type_computed == ASTState.NON_CYCLE || type_computed == state().cycle()) {
      return type_value;
    }
    type_value = type_compute();
    if (state().inCircle()) {
      type_computed = state().cycle();
    
    } else {
      type_computed = ASTState.NON_CYCLE;
    
    }
    return type_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private TypeDecl type_compute() {
      TypeDecl type = refined_AutoBoxing_ConditionalExpr_type();
      if (type.isUnknown()) {
        TypeDecl trueType = getTrueExpr().type();
        TypeDecl falseType = getFalseExpr().type();
        if (!trueType.isReferenceType() && !trueType.boxed().isUnknown()) {
          trueType = trueType.boxed();
        }
        if (!falseType.isReferenceType() && !falseType.boxed().isUnknown()) {
          falseType = falseType.boxed();
        }
        ArrayList<TypeDecl> list = new ArrayList<TypeDecl>();
        list.add(trueType);
        list.add(falseType);
        return type.lookupLUBType(list).lub();
      }
      return type;
    }
  /**
   * Returns the type of a conditional expression, given the types of the
   * second and third operands.
   * @attribute syn
   * @aspect TypeAnalysis
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeAnalysis.jrag:396
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeAnalysis.jrag:396")
  @SideEffect.Pure(group="conditionalExprType_TypeDecl_TypeDecl") public TypeDecl conditionalExprType(TypeDecl second, TypeDecl third) {
    {
        if (second == third) {
          return second;
        }
    
        if (second.isNumericType() && third.isNumericType()) {
          if (second.isByte() && third.isShort()) {
            return third;
          }
          if (second.isShort() && third.isByte()) {
            return second;
          }
          if ((second.isByte() || second.isShort() || second.isChar())
              && third.isInt() && getFalseExpr().isConstant()
              && getFalseExpr().representableIn(second)) {
            return second;
          }
          if ((third.isByte() || third.isShort() || third.isChar())
              && second.isInt() && getTrueExpr().isConstant()
              && getTrueExpr().representableIn(third)) {
            return third;
          }
          return second.binaryNumericPromotion(third);
        } else if (second.isBoolean() && third.isBoolean()) {
          return second;
        } else if (second.isReferenceType() && third.isNull()) {
          return second;
        } else if (second.isNull() && third.isReferenceType()) {
          return third;
        } else if (second.isReferenceType() && third.isReferenceType()) {
          if (second.assignConversionTo(third, null)) {
            return third;
          }
          if (third.assignConversionTo(second, null)) {
            return second;
          }
          return unknownType();
        } else {
          return unknownType();
        }
      }
  }
  /**
   * @attribute syn
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\backend\\CreateBCode.jrag:1134
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CreateBCode", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\backend\\CreateBCode.jrag:1134")
  @SideEffect.Pure(group="canBeTrue") public boolean canBeTrue() {
    boolean canBeTrue_value = type().isBoolean()
          && (getCondition().canBeTrue() && getTrueExpr().canBeTrue()
              || getCondition().canBeFalse() && getFalseExpr().canBeTrue());
    return canBeTrue_value;
  }
  /**
   * @attribute syn
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\backend\\CreateBCode.jrag:1146
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CreateBCode", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\backend\\CreateBCode.jrag:1146")
  @SideEffect.Pure(group="canBeFalse") public boolean canBeFalse() {
    boolean canBeFalse_value = type().isBoolean()
          && (getCondition().canBeTrue() && getTrueExpr().canBeFalse()
              || getCondition().canBeFalse() && getFalseExpr().canBeFalse());
    return canBeFalse_value;
  }
  /**
   * @attribute syn
   * @aspect PreciseRethrow
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\PreciseRethrow.jrag:145
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="PreciseRethrow", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\PreciseRethrow.jrag:145")
  @SideEffect.Pure(group="modifiedInScope_Variable") public boolean modifiedInScope(Variable var) {
    boolean modifiedInScope_Variable_value = getCondition().modifiedInScope(var)
          || getTrueExpr().modifiedInScope(var)
          || getFalseExpr().modifiedInScope(var);
    return modifiedInScope_Variable_value;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:256
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_assignedBefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    if (getFalseExprNoTransform() != null && _callerNode == getFalseExpr()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:466
      return getCondition().assignedAfterFalse(v);
    }
    else if (getTrueExprNoTransform() != null && _callerNode == getTrueExpr()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:464
      return getCondition().assignedAfterTrue(v);
    }
    else if (getConditionNoTransform() != null && _callerNode == getCondition()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:462
      return assignedBefore(v);
    }
    else {
      return getParent().Define_assignedBefore(this, _callerNode, v);
    }
  }
  @SideEffect.Pure protected boolean canDefine_assignedBefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:891
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_unassignedBefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    if (getFalseExprNoTransform() != null && _callerNode == getFalseExpr()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:1062
      return getCondition().unassignedAfterFalse(v);
    }
    else if (getTrueExprNoTransform() != null && _callerNode == getTrueExpr()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:1059
      return getCondition().unassignedAfterTrue(v);
    }
    else if (getConditionNoTransform() != null && _callerNode == getCondition()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:1057
      return unassignedBefore(v);
    }
    else {
      return getParent().Define_unassignedBefore(this, _callerNode, v);
    }
  }
  @SideEffect.Pure protected boolean canDefine_unassignedBefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Pure public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }
  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_CompilationUnit_problems(CompilationUnit _root, @SideEffect.Local java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeCheck.jrag:738
    if (!getCondition().type().isBoolean()) {
      {
        java.util.Set<ASTNode> contributors = _map.get(_root);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) _root, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeCheck.jrag:743
    if (type().isUnknown() && !getTrueExpr().type().isUnknown()
              && !getFalseExpr().type().isUnknown()) {
      {
        java.util.Set<ASTNode> contributors = _map.get(_root);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) _root, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_CompilationUnit_problems(_root, _map);
  }
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_CompilationUnit_problems(LinkedList<Problem> collection) {
    super.contributeTo_CompilationUnit_problems(collection);
    if (!getCondition().type().isBoolean()) {
      collection.add(error("The first operand of a conditional expression must be a boolean"));
    }
    if (type().isUnknown() && !getTrueExpr().type().isUnknown()
              && !getFalseExpr().type().isUnknown()) {
      collection.add(error("The types of the second and third operand in "
                + "this conditional expression do not match"));
    }
  }
}

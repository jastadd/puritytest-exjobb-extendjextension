/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package org.extendj.ast;
import java.util.HashSet;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;
import java.util.Set;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import beaver.*;
import java.util.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.jastadd.util.*;
import java.util.zip.*;
import java.io.*;
import org.jastadd.util.PrettyPrintable;
import org.jastadd.util.PrettyPrinter;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\grammar\\Java.ast:4
 * @production CompilationUnit : {@link ASTNode} ::= <span class="component">&lt;PackageDecl:String&gt;</span> <span class="component">{@link ImportDecl}*</span> <span class="component">{@link TypeDecl}*</span>;

 */
public class CompilationUnit extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect ClassPath
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:435
   */
  private ClassSource classSource = ClassSource.NONE;
  /**
   * @aspect ClassPath
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:437
   */
  private boolean fromSource = false;
  /**
   * @aspect ClassPath
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:439
   */
  public void setClassSource(ClassSource source) {
    this.classSource = source;
  }
  /**
   * @aspect ClassPath
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:443
   */
  public ClassSource getClassSource() {
    return classSource;
  }
  /**
   * @aspect ClassPath
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:447
   */
  public void setFromSource(boolean value) {
    this.fromSource = value;
  }
  /**
   * @aspect ErrorCheck
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ErrorCheck.jrag:95
   */
  public Collection<Problem> parseErrors() {
    return parseErrors;
  }
  /**
   * @aspect ErrorCheck
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ErrorCheck.jrag:99
   */
  public void addParseError(Problem msg) {
    parseErrors.add(msg);
  }
  /**
   * @aspect ErrorCheck
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ErrorCheck.jrag:103
   */
  protected Collection<Problem> parseErrors = new ArrayList<Problem>();
  /**
   * @aspect Java4PrettyPrint
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\PrettyPrint.jadd:223
   */
  public void prettyPrint(PrettyPrinter out) {
    if (hasPackageDecl()) {
      out.print("package ");
      out.print(getPackageDecl());
      out.print(";");
      out.println();
    }
    if (!out.isNewLine()) {
      out.println();
    }
    out.print(getImportDeclList());
    if (!out.isNewLine()) {
      out.println();
    }
    out.println();
    out.join(getTypeDecls(), new PrettyPrinter.Joiner() {
      @Override
      public void printSeparator(PrettyPrinter out) {
        out.println();
        out.println();
      }
    });
  }
  /**
   * @aspect GenerateClassfile
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\backend\\GenerateClassfile.jrag:40
   */
  public void generateClassfile() {
    if (fromSource()) {
      for (int i = 0; i < getNumTypeDecl(); i++) {
        getTypeDecl(i).generateClassfile();
      }
    }
  }
  /**
   * @declaredat ASTNode:1
   */
  public CompilationUnit() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
    setChild(new List(), 0);
    setChild(new List(), 1);
  }
  /**
   * @declaredat ASTNode:15
   */
  public CompilationUnit(String p0, List<ImportDecl> p1, List<TypeDecl> p2) {
    setPackageDecl(p0);
    setChild(p1, 0);
    setChild(p2, 1);
  }
  /**
   * @declaredat ASTNode:20
   */
  public CompilationUnit(beaver.Symbol p0, List<ImportDecl> p1, List<TypeDecl> p2) {
    setPackageDecl(p0);
    setChild(p1, 0);
    setChild(p2, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:26
   */
  @SideEffect.Pure protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:32
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:36
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    packageName_reset();
    destinationPath_reset();
    lookupType_String_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
    CompilationUnit_problems_computed = null;
    CompilationUnit_problems_value = null;
    contributorMap_CompilationUnit_problems = null;
    contributorMap_TypeDecl_accessors = null;
    contributorMap_TypeDecl_nestedTypes = null;
    contributorMap_TypeDecl_enumSwitchStatements = null;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:53
   */
  @SideEffect.Fresh public CompilationUnit clone() throws CloneNotSupportedException {
    CompilationUnit node = (CompilationUnit) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:58
   */
  @SideEffect.Fresh(group="_ASTNode") public CompilationUnit copy() {
    try {
      CompilationUnit node = (CompilationUnit) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:77
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public CompilationUnit fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:87
   */
  @SideEffect.Fresh(group="_ASTNode") public CompilationUnit treeCopyNoTransform() {
    CompilationUnit tree = (CompilationUnit) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:107
   */
  @SideEffect.Fresh(group="_ASTNode") public CompilationUnit treeCopy() {
    CompilationUnit tree = (CompilationUnit) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:121
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_PackageDecl == ((CompilationUnit) node).tokenString_PackageDecl);    
  }
  /**
   * Replaces the lexeme PackageDecl.
   * @param value The new value for the lexeme PackageDecl.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setPackageDecl(String value) {
    tokenString_PackageDecl = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_PackageDecl;
  /**
   */
  public int PackageDeclstart;
  /**
   */
  public int PackageDeclend;
  /**
   * JastAdd-internal setter for lexeme PackageDecl using the Beaver parser.
   * @param symbol Symbol containing the new value for the lexeme PackageDecl
   * @apilevel internal
   */
  @SideEffect.Local(group="_ASTNode") public void setPackageDecl(beaver.Symbol symbol) {
    if (symbol.value != null && !(symbol.value instanceof String))
    throw new UnsupportedOperationException("setPackageDecl is only valid for String lexemes");
    tokenString_PackageDecl = (String)symbol.value;
    PackageDeclstart = symbol.getStart();
    PackageDeclend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme PackageDecl.
   * @return The value for the lexeme PackageDecl.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="PackageDecl")
  @SideEffect.Pure(group="_ASTNode") public String getPackageDecl() {
    return tokenString_PackageDecl != null ? tokenString_PackageDecl : "";
  }
  /**
   * Replaces the ImportDecl list.
   * @param list The new list node to be used as the ImportDecl list.
   * @apilevel high-level
   */
  public void setImportDeclList(List<ImportDecl> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the ImportDecl list.
   * @return Number of children in the ImportDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumImportDecl() {
    return getImportDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the ImportDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the ImportDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumImportDeclNoTransform() {
    return getImportDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the ImportDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the ImportDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public ImportDecl getImportDecl(int i) {
    return (ImportDecl) getImportDeclList().getChild(i);
  }
  /**
   * Check whether the ImportDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasImportDecl() {
    return getImportDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the ImportDecl list.
   * @param node The element to append to the ImportDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addImportDecl(ImportDecl node) {
    List<ImportDecl> list = (parent == null) ? getImportDeclListNoTransform() : getImportDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addImportDeclNoTransform(ImportDecl node) {
    List<ImportDecl> list = getImportDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the ImportDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setImportDecl(ImportDecl node, int i) {
    List<ImportDecl> list = getImportDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the ImportDecl list.
   * @return The node representing the ImportDecl list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="ImportDecl")
  @SideEffect.Pure(group="_ASTNode") public List<ImportDecl> getImportDeclList() {
    List<ImportDecl> list = (List<ImportDecl>) getChild(0);
    return list;
  }
  /**
   * Retrieves the ImportDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the ImportDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<ImportDecl> getImportDeclListNoTransform() {
    return (List<ImportDecl>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the ImportDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public ImportDecl getImportDeclNoTransform(int i) {
    return (ImportDecl) getImportDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the ImportDecl list.
   * @return The node representing the ImportDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<ImportDecl> getImportDecls() {
    return getImportDeclList();
  }
  /**
   * Retrieves the ImportDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the ImportDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<ImportDecl> getImportDeclsNoTransform() {
    return getImportDeclListNoTransform();
  }
  /**
   * Replaces the TypeDecl list.
   * @param list The new list node to be used as the TypeDecl list.
   * @apilevel high-level
   */
  public void setTypeDeclList(List<TypeDecl> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the TypeDecl list.
   * @return Number of children in the TypeDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumTypeDecl() {
    return getTypeDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the TypeDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the TypeDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumTypeDeclNoTransform() {
    return getTypeDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the TypeDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the TypeDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public TypeDecl getTypeDecl(int i) {
    return (TypeDecl) getTypeDeclList().getChild(i);
  }
  /**
   * Check whether the TypeDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasTypeDecl() {
    return getTypeDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the TypeDecl list.
   * @param node The element to append to the TypeDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addTypeDecl(TypeDecl node) {
    List<TypeDecl> list = (parent == null) ? getTypeDeclListNoTransform() : getTypeDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addTypeDeclNoTransform(TypeDecl node) {
    List<TypeDecl> list = getTypeDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the TypeDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setTypeDecl(TypeDecl node, int i) {
    List<TypeDecl> list = getTypeDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the TypeDecl list.
   * @return The node representing the TypeDecl list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="TypeDecl")
  @SideEffect.Pure(group="_ASTNode") public List<TypeDecl> getTypeDeclList() {
    List<TypeDecl> list = (List<TypeDecl>) getChild(1);
    return list;
  }
  /**
   * Retrieves the TypeDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the TypeDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<TypeDecl> getTypeDeclListNoTransform() {
    return (List<TypeDecl>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the TypeDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public TypeDecl getTypeDeclNoTransform(int i) {
    return (TypeDecl) getTypeDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the TypeDecl list.
   * @return The node representing the TypeDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<TypeDecl> getTypeDecls() {
    return getTypeDeclList();
  }
  /**
   * Retrieves the TypeDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the TypeDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<TypeDecl> getTypeDeclsNoTransform() {
    return getTypeDeclListNoTransform();
  }
  /**
   * @aspect NameCheck
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\NameCheck.jrag:68
   */
  private Collection<Problem> refined_NameCheck_CompilationUnit_nameProblems()
{
    Collection<Problem> problems = new LinkedList<Problem>();
    for (int i = 0; i < getNumImportDecl(); i++) {
      ImportDecl decl = getImportDecl(i);
      if (!decl.isOnDemand()) {
        for (TypeDecl importedType : decl.importedTypes()) {
          for (TypeDecl local : localLookupType(importedType.name())) {
            if (local != importedType) {
              problems.add(errorf("imported type %s conflicts with visible type", decl.typeName()));
            }
          }
        }
      }
    }
    return problems;
  }
  /**
   * @aspect TypeScopePropagation
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:361
   */
  private SimpleSet<TypeDecl> refined_TypeScopePropagation_CompilationUnit_Child_lookupType_String(String name)
{
    // Locally declared types in the compilation unit.
    SimpleSet<TypeDecl> result = localLookupType(name);
    if (!result.isEmpty()) {
      return result;
    }

    // Imported types.
    result = importedTypes(name);
    if (!result.isEmpty()) {
      return result;
    }

    // Types in the same package.
    TypeDecl pkgType = lookupType(packageName(), name);
    if (pkgType.accessibleFromPackage(packageName())) {
      return pkgType;
    }

    // Types imported on demand.
    result = importedTypesOnDemand(name);
    if (!result.isEmpty()) {
      return result;
    }

    // Include primitive types.
    TypeDecl primitiveType = lookupType(PRIMITIVE_PACKAGE_NAME, name);
    if (!primitiveType.isUnknown()) {
      return primitiveType;
    }

    // 7.5.5 Automatic Imports
    TypeDecl defaultType = lookupType("java.lang", name);
    if (defaultType.accessibleFromPackage(packageName())) {
      return defaultType;
    }
    return lookupType(name);
  }
  /**
   * @aspect <NoAspect>
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ErrorCheck.jrag:278
   */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map<ASTNode, java.util.Set<ASTNode>> contributorMap_CompilationUnit_problems = null;

  @SideEffect.Ignore protected void survey_CompilationUnit_problems() {
    if (contributorMap_CompilationUnit_problems == null) {
      contributorMap_CompilationUnit_problems = new java.util.IdentityHashMap<ASTNode, java.util.Set<ASTNode>>();
      collect_contributors_CompilationUnit_problems(this, contributorMap_CompilationUnit_problems);
    }
  }

  /**
   * @aspect <NoAspect>
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\backend\\GenerateClassfile.jrag:346
   */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map<ASTNode, java.util.Set<ASTNode>> contributorMap_TypeDecl_accessors = null;

  @SideEffect.Ignore protected void survey_TypeDecl_accessors() {
    if (contributorMap_TypeDecl_accessors == null) {
      contributorMap_TypeDecl_accessors = new java.util.IdentityHashMap<ASTNode, java.util.Set<ASTNode>>();
      collect_contributors_TypeDecl_accessors(this, contributorMap_TypeDecl_accessors);
    }
  }

  /**
   * @aspect <NoAspect>
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\backend\\InnerClasses.jrag:155
   */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map<ASTNode, java.util.Set<ASTNode>> contributorMap_TypeDecl_nestedTypes = null;

  @SideEffect.Ignore protected void survey_TypeDecl_nestedTypes() {
    if (contributorMap_TypeDecl_nestedTypes == null) {
      contributorMap_TypeDecl_nestedTypes = new java.util.IdentityHashMap<ASTNode, java.util.Set<ASTNode>>();
      collect_contributors_TypeDecl_nestedTypes(this, contributorMap_TypeDecl_nestedTypes);
    }
  }

  /**
   * @aspect <NoAspect>
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\backend\\EnumsCodegen.jrag:115
   */
  @SideEffect.Secret(group="_ASTNode") protected java.util.Map<ASTNode, java.util.Set<ASTNode>> contributorMap_TypeDecl_enumSwitchStatements = null;

  @SideEffect.Ignore protected void survey_TypeDecl_enumSwitchStatements() {
    if (contributorMap_TypeDecl_enumSwitchStatements == null) {
      contributorMap_TypeDecl_enumSwitchStatements = new java.util.IdentityHashMap<ASTNode, java.util.Set<ASTNode>>();
      collect_contributors_TypeDecl_enumSwitchStatements(this, contributorMap_TypeDecl_enumSwitchStatements);
    }
  }

  /**
   * @return The path to the source file, or the path to the file inside a Jar
   * file.
   * @attribute syn
   * @aspect ClassPath
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:92
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ClassPath", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:92")
  @SideEffect.Pure(group="relativeName") public String relativeName() {
    String relativeName_value = getClassSource().relativeName();
    return relativeName_value;
  }
  /**
   * @return The path to the source file, or the enclosing Jar file.
   * @attribute syn
   * @aspect ClassPath
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:97
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ClassPath", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:97")
  @SideEffect.Pure(group="pathName") public String pathName() {
    String pathName_value = getClassSource().pathName();
    return pathName_value;
  }
  /**
   * @return {@code true} if this compilation unit was parsed from source.
   * @attribute syn
   * @aspect ClassPath
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:102
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ClassPath", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:102")
  @SideEffect.Pure(group="fromSource") public boolean fromSource() {
    boolean fromSource_value = fromSource;
    return fromSource_value;
  }
  /**
   * @attribute syn
   * @aspect ErrorCheck
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ErrorCheck.jrag:280
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ErrorCheck", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ErrorCheck.jrag:280")
  @SideEffect.Pure(group="errors") public Collection<Problem> errors() {
    {
        Collection<Problem> errors = new LinkedList<Problem>();
        for (Problem problem : problems()) {
          if (problem.severity() == Problem.Severity.ERROR) {
            errors.add(problem);
          }
        }
        return errors;
      }
  }
  /**
   * @attribute syn
   * @aspect ErrorCheck
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ErrorCheck.jrag:290
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ErrorCheck", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ErrorCheck.jrag:290")
  @SideEffect.Pure(group="warnings") public Collection<Problem> warnings() {
    {
        Collection<Problem> warnings = new LinkedList<Problem>();
        for (Problem problem : problems()) {
          if (problem.severity() == Problem.Severity.WARNING) {
            warnings.add(problem);
          }
        }
        return warnings;
      }
  }
  /** Searches for a type with the given simple name in this compilation unit. 
   * @attribute syn
   * @aspect TypeScopePropagation
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:401
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeScopePropagation", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:401")
  @SideEffect.Pure(group="localLookupType_String") public SimpleSet<TypeDecl> localLookupType(String name) {
    {
        for (int i = 0; i < getNumTypeDecl(); i++) {
          if (getTypeDecl(i).name().equals(name)) {
            return getTypeDecl(i);
          }
        }
        return emptySet();
      }
  }
  /**
   * @attribute syn
   * @aspect TypeScopePropagation
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:410
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeScopePropagation", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:410")
  @SideEffect.Pure(group="importedTypes_String") public SimpleSet<TypeDecl> importedTypes(String name) {
    {
        SimpleSet<TypeDecl> result = emptySet();
        for (int i = 0; i < getNumImportDecl(); i++) {
          if (!getImportDecl(i).isOnDemand()) {
            for (TypeDecl type : getImportDecl(i).importedTypes(name)) {
              result = result.add(type);
            }
          }
        }
        return result;
      }
  }
  /**
   * @attribute syn
   * @aspect TypeScopePropagation
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:422
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeScopePropagation", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:422")
  @SideEffect.Pure(group="importedTypesOnDemand_String") public SimpleSet<TypeDecl> importedTypesOnDemand(String name) {
    {
        SimpleSet<TypeDecl> result = emptySet();
        for (int i = 0; i < getNumImportDecl(); i++) {
          if (getImportDecl(i).isOnDemand()) {
            for (TypeDecl type : getImportDecl(i).importedTypes(name)) {
              result = result.add(type);
            }
          }
        }
        return result;
      }
  }
  /**
   * @attribute syn
   * @aspect NameCheck
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\NameCheck.jrag:68
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameCheck", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\NameCheck.jrag:68")
  @SideEffect.Pure(group="nameProblems") public Collection<Problem> nameProblems() {
    {
        Collection<Problem> problems = refined_NameCheck_CompilationUnit_nameProblems();
        for (int i = 0; i < getNumImportDecl(); i++) {
          if (getImportDecl(i) instanceof SingleStaticImportDecl) {
            SingleStaticImportDecl decl = (SingleStaticImportDecl) getImportDecl(i);
            String name = decl.name();
            if (!decl.importedTypes(name).isEmpty()) {
              TypeDecl type = decl.importedTypes(name).iterator().next();
              if (localLookupType(name).contains(type)) {
                problems.add(decl.errorf("the imported name %s.%s is already declared in this compilation unit",
                    packageName(), name));
              }
            }
          }
        }
        return problems;
      }
  }
  /**
   * @attribute syn
   * @aspect PrettyPrintUtil
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\PrettyPrintUtil.jrag:233
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="PrettyPrintUtil", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\PrettyPrintUtil.jrag:233")
  @SideEffect.Pure(group="hasPackageDecl") public boolean hasPackageDecl() {
    boolean hasPackageDecl_value = !getPackageDecl().isEmpty();
    return hasPackageDecl_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void packageName_reset() {
    packageName_computed = null;
    packageName_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="packageName") protected ASTState.Cycle packageName_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="packageName") protected String packageName_value;

  /**
   * @attribute syn
   * @aspect TypeName
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\QualifiedNames.jrag:112
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeName", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\QualifiedNames.jrag:112")
  @SideEffect.Pure(group="packageName") public String packageName() {
    ASTState state = state();
    if (packageName_computed == ASTState.NON_CYCLE || packageName_computed == state().cycle()) {
      return packageName_value;
    }
    packageName_value = packageName_compute();
    if (state().inCircle()) {
      packageName_computed = state().cycle();
    
    } else {
      packageName_computed = ASTState.NON_CYCLE;
    
    }
    return packageName_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private String packageName_compute() {return getPackageDecl();}
  /** @apilevel internal */
  @SideEffect.Ignore private void destinationPath_reset() {
    destinationPath_computed = null;
    destinationPath_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="destinationPath") protected ASTState.Cycle destinationPath_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="destinationPath") protected String destinationPath_value;

  /**
   * Computes the path to the parent directory of the source file of this
   * compilation unit.
   * 
   * <p>If the parent directory of the source file could not be computed
   * the path to the working directory is returned.
   * 
   * @return The path to the parent directory of the source file for this
   * compilation unit, or "." if the path could not be computed.
   * @attribute syn
   * @aspect ConstantPoolNames
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\backend\\ConstantPoolNames.jrag:165
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantPoolNames", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\backend\\ConstantPoolNames.jrag:165")
  @SideEffect.Pure(group="destinationPath") public String destinationPath() {
    ASTState state = state();
    if (destinationPath_computed == ASTState.NON_CYCLE || destinationPath_computed == state().cycle()) {
      return destinationPath_value;
    }
    destinationPath_value = destinationPath_compute();
    if (state().inCircle()) {
      destinationPath_computed = state().cycle();
    
    } else {
      destinationPath_computed = ASTState.NON_CYCLE;
    
    }
    return destinationPath_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private String destinationPath_compute() {
      String parentPath = new File(pathName()).getParent();
      if (parentPath == null) {
        return ".";
      } else {
        return parentPath;
      }
    }
  /**
   * @attribute syn
   * @aspect StaticImports
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\StaticImports.jrag:191
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="StaticImports", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\StaticImports.jrag:191")
  @SideEffect.Pure(group="importedFields_String") public SimpleSet<Variable> importedFields(String name) {
    {
        SimpleSet<Variable> set = emptySet();
        for (int i = 0; i < getNumImportDecl(); i++) {
          if (!getImportDecl(i).isOnDemand()) {
            for (Variable field : getImportDecl(i).importedFields(name)) {
              set = set.add(field);
            }
          }
        }
        return set;
      }
  }
  /**
   * @attribute syn
   * @aspect StaticImports
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\StaticImports.jrag:203
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="StaticImports", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\StaticImports.jrag:203")
  @SideEffect.Pure(group="importedFieldsOnDemand_String") public SimpleSet<Variable> importedFieldsOnDemand(String name) {
    {
        SimpleSet<Variable> set = emptySet();
        for (int i = 0; i < getNumImportDecl(); i++) {
          if (getImportDecl(i).isOnDemand()) {
            for (Variable field : getImportDecl(i).importedFields(name)) {
              set = set.add(field);
            }
          }
        }
        return set;
      }
  }
  /**
   * @attribute syn
   * @aspect StaticImports
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\StaticImports.jrag:233
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="StaticImports", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\StaticImports.jrag:233")
  @SideEffect.Pure(group="importedMethods_String") public Collection<MethodDecl> importedMethods(String name) {
    {
        Collection<MethodDecl> methods = new ArrayList<MethodDecl>();
        for (int i = 0; i < getNumImportDecl(); i++) {
          if (!getImportDecl(i).isOnDemand()) {
            methods.addAll(getImportDecl(i).importedMethods(name));
          }
        }
        return methods;
      }
  }
  /**
   * @attribute syn
   * @aspect StaticImports
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\StaticImports.jrag:243
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="StaticImports", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\StaticImports.jrag:243")
  @SideEffect.Pure(group="importedMethodsOnDemand_String") public Collection<MethodDecl> importedMethodsOnDemand(String name) {
    {
        Collection<MethodDecl> methods = new ArrayList<MethodDecl>();
        for (int i = 0; i < getNumImportDecl(); i++) {
          if (getImportDecl(i).isOnDemand()) {
            methods.addAll(getImportDecl(i).importedMethods(name));
          }
        }
        return methods;
      }
  }
  /**
   * @attribute inh
   * @aspect LookupFullyQualifiedTypes
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:131
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="LookupFullyQualifiedTypes", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:131")
  @SideEffect.Pure(group="lookupType_String_String") public TypeDecl lookupType(String packageName, String typeName) {
    TypeDecl lookupType_String_String_value = getParent().Define_lookupType(this, null, packageName, typeName);
    return lookupType_String_String_value;
  }
  /**
   * @attribute inh
   * @aspect TypeScopePropagation
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:350
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="TypeScopePropagation", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:350")
  @SideEffect.Pure(group="lookupType_String") public SimpleSet<TypeDecl> lookupType(String name) {
    Object _parameters = name;
    if (lookupType_String_computed == null) lookupType_String_computed = new java.util.HashMap(4);
    if (lookupType_String_values == null) lookupType_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (lookupType_String_values.containsKey(_parameters) && lookupType_String_computed != null
        && lookupType_String_computed.containsKey(_parameters)
        && (lookupType_String_computed.get(_parameters) == ASTState.NON_CYCLE || lookupType_String_computed.get(_parameters) == state().cycle())) {
      return (SimpleSet<TypeDecl>) lookupType_String_values.get(_parameters);
    }
    SimpleSet<TypeDecl> lookupType_String_value = getParent().Define_lookupType(this, null, name);
    if (state().inCircle()) {
      lookupType_String_values.put(_parameters, lookupType_String_value);
      lookupType_String_computed.put(_parameters, state().cycle());
    
    } else {
      lookupType_String_values.put(_parameters, lookupType_String_value);
      lookupType_String_computed.put(_parameters, ASTState.NON_CYCLE);
    
    }
    return lookupType_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void lookupType_String_reset() {
    lookupType_String_computed = new java.util.HashMap(4);
    lookupType_String_values = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="lookupType_String") protected java.util.Map lookupType_String_values;
  /** @apilevel internal */
  @SideEffect.Secret(group="lookupType_String") protected java.util.Map lookupType_String_computed;
  /**
   * @attribute inh
   * @aspect StaticImports
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\StaticImports.jrag:189
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="StaticImports", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\StaticImports.jrag:189")
  @SideEffect.Pure(group="lookupVariable_String") public SimpleSet<Variable> lookupVariable(String name) {
    SimpleSet<Variable> lookupVariable_String_value = getParent().Define_lookupVariable(this, null, name);
    return lookupVariable_String_value;
  }
  /**
   * @attribute inh
   * @aspect StaticImports
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\StaticImports.jrag:231
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="StaticImports", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\StaticImports.jrag:231")
  @SideEffect.Pure(group="lookupMethod_String") public Collection<MethodDecl> lookupMethod(String name) {
    Collection<MethodDecl> lookupMethod_String_value = getParent().Define_lookupMethod(this, null, name);
    return lookupMethod_String_value;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ClassPath.jrag:105
   * @apilevel internal
   */
 @SideEffect.Pure public CompilationUnit Define_compilationUnit(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return this;
  }
  @SideEffect.Pure protected boolean canDefine_compilationUnit(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:66
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isIncOrDec(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getTypeDeclListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\DefiniteAssignment.jrag:68
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return false;
    }
    else {
      return getParent().Define_isIncOrDec(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_isIncOrDec(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\TryWithResources.jrag:115
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_handlesException(ASTNode _callerNode, ASTNode _childNode, TypeDecl exceptionType) {
    if (_callerNode == getImportDeclListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\StaticImports.jrag:299
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return false;
    }
    else if (_callerNode == getTypeDeclListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ExceptionHandling.jrag:191
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return false;
    }
    else {
      return getParent().Define_handlesException(this, _callerNode, exceptionType);
    }
  }
  @SideEffect.Pure protected boolean canDefine_handlesException(ASTNode _callerNode, ASTNode _childNode, TypeDecl exceptionType) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\GenericMethods.jrag:225
   * @apilevel internal
   */
 @SideEffect.Pure public SimpleSet<TypeDecl> Define_lookupType(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getImportDeclListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupType.jrag:486
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return lookupType(name);
    }
    else {
      int childIndex = this.getIndexOfChild(_callerNode);
      {
          SimpleSet<TypeDecl> result = emptySet();
          for (TypeDecl typeDecl : refined_TypeScopePropagation_CompilationUnit_Child_lookupType_String(name)) {
            if (typeDecl instanceof ParTypeDecl) {
              result = result.add(((ParTypeDecl) typeDecl).genericDecl());
            } else {
              result = result.add(typeDecl);
            }
          }
          return result;
        }
    }
  }
  @SideEffect.Pure protected boolean canDefine_lookupType(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\NameCheck.jrag:50
   * @apilevel internal
   */
 @SideEffect.Pure public SimpleSet<TypeDecl> Define_allImportedTypes(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getImportDeclListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\NameCheck.jrag:52
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return importedTypes(name);
    }
    else {
      return getParent().Define_allImportedTypes(this, _callerNode, name);
    }
  }
  @SideEffect.Pure protected boolean canDefine_allImportedTypes(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\QualifiedNames.jrag:108
   * @apilevel internal
   */
 @SideEffect.Pure public String Define_packageName(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return packageName();
  }
  @SideEffect.Pure protected boolean canDefine_packageName(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\SyntacticClassification.jrag:36
   * @apilevel internal
   */
 @SideEffect.Pure public NameType Define_nameType(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getImportDeclListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\SyntacticClassification.jrag:93
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return NameType.PACKAGE_NAME;
    }
    else {
      return getParent().Define_nameType(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_nameType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeAnalysis.jrag:547
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_enclosingType(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return null;
  }
  @SideEffect.Pure protected boolean canDefine_enclosingType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\MultiCatch.jrag:227
   * @apilevel internal
   */
 @SideEffect.Pure public BodyDecl Define_enclosingBodyDecl(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return null;
  }
  @SideEffect.Pure protected boolean canDefine_enclosingBodyDecl(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeAnalysis.jrag:572
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isNestedType(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_isNestedType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeAnalysis.jrag:580
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isMemberType(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getTypeDeclListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeAnalysis.jrag:583
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return false;
    }
    else {
      return getParent().Define_isMemberType(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_isMemberType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeAnalysis.jrag:598
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isLocalClass(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_isLocalClass(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeAnalysis.jrag:633
   * @apilevel internal
   */
 @SideEffect.Pure public String Define_hostPackage(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return packageName();
  }
  @SideEffect.Pure protected boolean canDefine_hostPackage(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\MultiCatch.jrag:76
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_hostType(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getImportDeclListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\TypeAnalysis.jrag:646
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return null;
    }
    else {
      return getParent().Define_hostType(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_hostType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\Annotations.jrag:95
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_inComplexAnnotation(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_inComplexAnnotation(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\Enums.jrag:127
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isOriginalEnumConstructor(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_isOriginalEnumConstructor(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\Enums.jrag:563
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_inEnumInitializer(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return false;
  }
  @SideEffect.Pure protected boolean canDefine_inEnumInitializer(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\Generics.jrag:789
   * @apilevel internal
   */
 @SideEffect.Pure public String Define_typeVariableContext(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return "";
  }
  @SideEffect.Pure protected boolean canDefine_typeVariableContext(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java7\\frontend\\TryWithResources.jrag:192
   * @apilevel internal
   */
 @SideEffect.Pure public SimpleSet<Variable> Define_lookupVariable(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getTypeDeclListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\StaticImports.jrag:177
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      {
          SimpleSet<Variable> set = importedFields(name);
          if (!set.isEmpty()) {
            return set;
          }
          set = importedFieldsOnDemand(name);
          if (!set.isEmpty()) {
            return set;
          }
          return lookupVariable(name);
        }
    }
    else {
      return getParent().Define_lookupVariable(this, _callerNode, name);
    }
  }
  @SideEffect.Pure protected boolean canDefine_lookupVariable(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\LookupMethod.jrag:52
   * @apilevel internal
   */
 @SideEffect.Pure public Collection<MethodDecl> Define_lookupMethod(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getTypeDeclListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java5\\frontend\\StaticImports.jrag:219
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      {
          Collection<MethodDecl> methods = importedMethods(name);
          if (!methods.isEmpty()) {
            return methods;
          }
          methods = importedMethodsOnDemand(name);
          if (!methods.isEmpty()) {
            return methods;
          }
          return lookupMethod(name);
        }
    }
    else {
      return getParent().Define_lookupMethod(this, _callerNode, name);
    }
  }
  @SideEffect.Pure protected boolean canDefine_lookupMethod(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Pure public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }
  /**
   * Collects semantic errors in the AST.
   * 
   * <p>Separate error checks are added using individual contribution statements.
   * @attribute coll
   * @aspect ErrorCheck
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ErrorCheck.jrag:278
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.COLL)
  @ASTNodeAnnotation.Source(aspect="ErrorCheck", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\ErrorCheck.jrag:278")
  @SideEffect.Pure(group="CompilationUnit_problems") public LinkedList<Problem> problems() {
    ASTState state = state();
    if (CompilationUnit_problems_computed == ASTState.NON_CYCLE || CompilationUnit_problems_computed == state().cycle()) {
      return CompilationUnit_problems_value;
    }
    CompilationUnit_problems_value = problems_compute();
    if (state().inCircle()) {
      CompilationUnit_problems_computed = state().cycle();
    
    } else {
      CompilationUnit_problems_computed = ASTState.NON_CYCLE;
    
    }
    return CompilationUnit_problems_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="CompilationUnit_problems") private LinkedList<Problem> problems_compute() {
    ASTNode node = this;
    while (node != null && !(node instanceof CompilationUnit)) {
      node = node.getParent();
    }
    CompilationUnit root = (CompilationUnit) node;
    root.survey_CompilationUnit_problems();
    LinkedList<Problem> _computedValue = new LinkedList<Problem>();
    if (root.contributorMap_CompilationUnit_problems.containsKey(this)) {
      for (ASTNode contributor : root.contributorMap_CompilationUnit_problems.get(this)) {
        contributor.contributeTo_CompilationUnit_problems(_computedValue);
      }
    }
    return _computedValue;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="CompilationUnit_problems") protected ASTState.Cycle CompilationUnit_problems_computed = null;
  /** @apilevel internal */
  @SideEffect.Secret(group="CompilationUnit_problems") protected LinkedList<Problem> CompilationUnit_problems_value;

  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_CompilationUnit_problems(CompilationUnit _root, @SideEffect.Local java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\extendjComp\\java4\\frontend\\NameCheck.jrag:66
    {
      java.util.Set<ASTNode> contributors = _map.get(_root);
      if (contributors == null) {
        contributors = new java.util.LinkedHashSet<ASTNode>();
        _map.put((ASTNode) _root, contributors);
      }
      contributors.add(this);
    }
    super.collect_contributors_CompilationUnit_problems(_root, _map);
  }
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_CompilationUnit_problems(LinkedList<Problem> collection) {
    super.contributeTo_CompilationUnit_problems(collection);
    for (Problem value : nameProblems()) {
      collection.add(value);
    }
  }
}

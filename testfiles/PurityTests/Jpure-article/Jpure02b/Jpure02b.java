import lang.ast.SideEffect.*;
import java.util.*;
 class Parent {
	 private List<Tests> items;
	 // Test extended For - should be considered pure aslong as items is not modifier or i!!!
	 @Pure boolean has(String x) {
	 for(Tests i : items) {
	 if(x == i.name) { return true; }
	 }
	 return false;
	 }
	 
	 @Pure boolean replaces(String x,int v) {
	 for(Tests i : items) {
	 if(x == i.name) { 
		 i.x = v; // Not okey
		 return true; }
	 }
	 return false;
	 }
	 
	 public class Tests{
		 int x;
		 String name;
	 }

 }

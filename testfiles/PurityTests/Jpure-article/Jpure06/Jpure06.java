import lang.ast.SideEffect.*;
import java.util.Iterator;
 class TypedList {
  private int length;
  private @Local Object[] data;
  private @Fresh Object op;
  private Object op2;
  private Type elementType;

  @Local public TypedList(Type type, int maxSize) {
  length = 0;
  data = new Object[maxSize];
  elementType = type;
  }

 @Local public void copy(@Local TypedList dst) {
  length = dst.length;
  elementType = dst.elementType;
  data = new Object[dst.length];
  for(int i=0;i!=length;++i) { data[i] = op;  data[i] = op2;
  data = dst.data;
  dst.data = data;
  }
  } 
}

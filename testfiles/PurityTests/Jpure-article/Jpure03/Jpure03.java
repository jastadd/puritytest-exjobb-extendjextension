import lang.ast.SideEffect.*;
 class Parent {
	 // String concatination generates a new object and not an manipulation
	 @Pure public String f(String x) { return x + "Hello"; }
 }

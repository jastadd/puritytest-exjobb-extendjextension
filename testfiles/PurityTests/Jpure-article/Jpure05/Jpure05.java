import lang.ast.SideEffect.*;
import java.util.Iterator;
 class TypedList {
  private int length;
  private @Local Object[] data;
  private Type elementType;

  @Local public TypedList(Type type, int maxSize) {
  length = 0;
  data = new Object[maxSize];
  elementType = type;
  }

 @Local public void copy(TypedList dst) {
  length = dst.length;
  elementType = dst.elementType;
  data = new Object[dst.length];
  for(int i=0;i!=length;++i) { data[i] = dst.data[i]; }
  } 
}

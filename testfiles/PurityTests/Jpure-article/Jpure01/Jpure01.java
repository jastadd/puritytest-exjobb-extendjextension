import lang.ast.SideEffect.*;
 class Parent {
 @Pure void f() {}
 }

 class Child extends Parent {
 int x;
 @Pure void f() { x=1; }
 }
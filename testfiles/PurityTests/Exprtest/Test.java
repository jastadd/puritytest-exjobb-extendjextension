import lang.ast.SideEffect.*;
public class Test {
  void f() {
  }

  // Primitive x may be freely changed
  @Pure int m(int x) {
    return x++ * x++;
  }
  // All aritmethetic operations are okey
  @Pure long l(long l) {
	  long q = l*10-3+l % 4;
	  return (l+q) * --q+l++;  
  }
}
import lang.ast.SideEffect.*;
import java.lang.UnsupportedOperationException;
public class test{

	@Pure public T getChild(int i){
		return this;
	}
	
	@Pure public int getNumChild(){
		return 0;
	}
	
	@Fresh public java.util.Iterator<T> astChildIterator() {
    	return new java.util.Iterator<T>() {
    		private int index = 0;

      		@Override
      		@Pure public boolean hasNext() {
    	  		return index < getNumChild();
      		}

      		@Override
      		@Fresh public T next() {
    	  		return hasNext() ? (T) getChild(index++) : null; //Wrong 2errors!!
      		}

      		@Override //Wrong 1 error!!
      		@Fresh public void remove() {
    	  		throw new UnsupportedOperationException(); //Wrong Unknown Method!!
      		}
    	};
	}
}

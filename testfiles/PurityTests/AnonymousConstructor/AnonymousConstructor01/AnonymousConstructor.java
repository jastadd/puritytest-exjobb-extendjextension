import lang.ast.SideEffect.*;
import java.lang.UnsupportedOperationException;
public class test{

	@Pure public T getChild(int i){
		return null;
	}
	
	@Pure public int getNumChild(){
		return 0;
	}
	
	@Fresh public java.util.Iterator<T> astChildIterator() {
    	return new java.util.Iterator<T>() {
    		private int index = 0;

      		@Override
      		@Pure public boolean hasNext() {
    	  		return index < getNumChild();
      		}

      		@Override
      		public T next() {
    	  		return hasNext() ? (T) getChild(index++) : null;
      		}

      		@Override
      		public void remove() {
    	  		throw new UnsupportedOperationException();
      		}
    	};
	}
}

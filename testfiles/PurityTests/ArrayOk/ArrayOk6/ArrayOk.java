import lang.ast.SideEffect.*;
public class LocalA{
	public int St=0;
    public LocalA p;
    @Local public LocalA a;
    @Local public LocalB b;
    @Pure public LocalA(){
    }
    
}

public class Tester{
	public int[] myA;
	public LocalA[] myLA;
	public String[] myS;
	
	@Fresh public Tester(int x){
		int[] myA = new int[4];
		myA[1]=3;
		myA[2]=myA[1];
		myA[0]=myA[2]*myA[3];
		myA[1]=myA[3].length;
		String[] myS = new String[4];
		myS[1]="3";
		myS[2]=myS[1];
		myS[0]=myS[2]+myS[3];
		myS[1]=myS[3].length;
		myS[3]="hello "+"world";
		return myS;
	}
	
	@Local public Tester xo(@Local Tester[] args){
		args[2]=new Tester(3);
		args[8]=this;
		args[2].myA[2]=3; //wrong!!
		args[2].myA=new int[3]; //wrong!!
	}
	
	@Fresh public Tester[] localTest(){
		Tester[] c = new Tester[]{this};
		return c;
	}
}


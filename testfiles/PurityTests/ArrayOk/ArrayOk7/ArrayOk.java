import lang.ast.SideEffect.*;
// Passing Fresh
public class LocalA{
	public int St=0;
    public LocalA p;
    @Local public LocalA a;
    @Local public LocalB b;
    @Pure public LocalA(){
    }
    
}

public class Tester{
	public int[] myA;
	public LocalA[] myLA;
	public String[] myS;
	
	@Fresh public Tester(int x){
	}
	
	//Okay
	@Pure public void test(){
		Tester test = new tester(2); 
		Tester[] tests = new Tester[]{test,test,test};
		passed(tests);
	}
	
	// Args == Fresh Tester[];
	@Pure public void passed(@Local Tester... args){
		args[2]=new Tester(3);
		args[8]=this;
		args[2].myA[2]=3;
		args[2].myA=new int[3];
	}
	
	@Fresh public Tester[] localTest(){
		Tester test = new tester(2); 
		Tester[] c = new Tester[]{test};
		return c;
	}
}


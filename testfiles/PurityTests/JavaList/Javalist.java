import java.util.List;
import java.util.ArrayList;

import lang.ast.SideEffect.*;
class test{
	ArrayList<test> list;
	
	@Local ArrayList<test> localList;
	static ArrayList<test> slist;
	
	//Obviously Okay
	@Fresh public ArrayList<test> for2() {
		ArrayList<test> temp = new ArrayList<test>();
		temp.add(new test());
		return temp;
	}
	
	//Fresh by default rules. No consept of Inner by default.
	@Fresh public ArrayList<test> for21() {
		ArrayList<test> temp = new ArrayList<test>();
		temp.add(this);
		return temp;
	}
	
	// Not Okay!!
	@Fresh public test keyPoint() {
		ArrayList<test> temp = new ArrayList<test>();
		temp.add(this);
		return temp.get(0);
	}
	
	// Not Okay!!
	@Fresh public test keyPoint2() {
		ArrayList<test> temp = new ArrayList<test>();
		temp.add(new temp());
		return temp.get(0);
	}
	
}
import lang.ast.SideEffect.*;
public class FreshInMethod{
    
    @Fresh public Delta main(String[] args){
    	return new Delta(null).add(new Delta(null));
    }
  
 public @Entity class Delta{
       Delta x;
       @Fresh public Delta(Delta y){
    	x=y;
       } 
    
       @Local @FreshIf public Delta add(Delta d){
    		if (x==null){
    			x=d;
    		}else{
    			x.add(d);
    		}
    		return this;
       }
    
    }  
 
}

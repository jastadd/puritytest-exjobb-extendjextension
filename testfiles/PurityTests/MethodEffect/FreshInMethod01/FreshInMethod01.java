import lang.ast.SideEffect.*;
public class FreshInMethod{
    
    @Fresh public static Delta main(String[] args){
    	Delta x=new Delta(40);
    	x.inc(x,0); //Does change anything Fresh will remain Fresh!!
    	return x;
    }
    
 
}

   public class Delta{
       int x;
       @Fresh public Delta(int y){
        	x=y;
    	} 
    
       @Local public void inc(@Local Delta modified,int b){
    		modified.x++;
       }
    
    }
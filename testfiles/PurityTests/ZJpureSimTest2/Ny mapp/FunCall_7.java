import lang.ast.SideEffect.*;
public class FunCall_7 {
    @Local private ArrayList<String> list = new ArrayList<String>();

    public boolean contains(String s) {
	for(String l : list) {
	    if(l.equals(s)) {
		return true;
	    }
	}
	return false;
    }
}

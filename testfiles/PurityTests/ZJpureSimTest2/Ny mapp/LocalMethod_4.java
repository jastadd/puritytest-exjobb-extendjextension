import lang.ast.SideEffect.*;
public class LocalMethod_4 {
    int x;

    @Pure public LocalMethod_4(int x) {
	this.x = x;
    }

    @Pure public void f() {
	new LocalMethod_4(1);
    }
}

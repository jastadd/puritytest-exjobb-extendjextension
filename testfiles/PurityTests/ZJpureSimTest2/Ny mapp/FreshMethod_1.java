import lang.ast.SideEffect.*;
public class FreshMethod_1 {
    @Fresh int[] f() {
	return new int[10];
    }

    @Fresh int[] g() {
	int[] a =new int[]{1,2,3};
	return a;
    }
}

import lang.ast.SideEffect.*;
public class FreshMethod_5 {
    @Pure int[] f(int x, int[] xs) {
	int[] a = new int[10];
	if(x > 0) {
	    return a;
	} else {
	    return null;
	}
    }

    @Fresh int[] g() {
	return f(1,null);
    }
}

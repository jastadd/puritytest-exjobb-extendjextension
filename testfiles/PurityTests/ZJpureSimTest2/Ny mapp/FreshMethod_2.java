import lang.ast.SideEffect.*;
public class FreshMethod_2 {
    @Fresh int[] f() {
	int[] a = new int[10];
	int[] x;
	x = a;
	return x;
    }
}

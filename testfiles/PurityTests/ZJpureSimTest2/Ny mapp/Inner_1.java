import lang.ast.SideEffect.*;
public class Inner_1 {
    @Local private String field;

    public class Inner {
	public void g(String f) {	       
	    field = f;
	}
    }
}

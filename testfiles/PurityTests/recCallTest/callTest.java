import lang.ast.SideEffect.*;

public class TypeDecl {
		 @Fresh @FreshIf @Pure public TypeDecl substitute(TypeDecl typeVariable) {
		    if(isTopLevelType())
		      return typeVariable;
		    return enclosingType().substitute(typeVariable);
		  }
		 
		 @Pure public boolean isTopLevelType(){
			 return true;
		 }
		 
		 @Pure(group="_ASTNode") public TypeDecl enclosingType() {
		     return this;
		 }
}

public class ClassDecl extends TypeDecl{

}

public class ParClassDecl extends ClassDecl{
	  // Gives Error due to super.substitute() ->
	@Fresh public TypeDecl substitute(TypeDecl typeVariable) {
	    return super.substitute(typeVariable);
	  }
}

interface ParTypeDecl extends Parameterization {

    //syn String name();
     
    //syn String name();
	 @Pure int getNumArgument();
    Access getArgument(int index);
    @Pure public String typeName();
@Fresh public TypeDecl substitute(TypeDecl typeVariable);

@Pure public int numTypeParameter();

public TypeVariable typeParameter(int index);

public Access substitute(Parameterization parTypeDecl);

}

interface Parameterization {
    boolean isRawType();
    @Fresh TypeDecl substitute(TypeDecl typeVariable);
}
import lang.ast.SideEffect.*;
import java.util.List;
class Statement {
int a;
  public class X {
    int b;
    public void doThing() {
    b=b*1;
    b++;
    }
  }

  @Pure("") void f(List<X> list) {
    a=5;  
    for (X item : list) {
      item.doThing();
    }
  }
}
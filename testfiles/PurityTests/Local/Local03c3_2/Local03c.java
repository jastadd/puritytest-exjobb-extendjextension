import lang.ast.SideEffect.*;
public class LocalA{
	public int St=0;
    public LocalA p;
    @Local public LocalA a;
    @Local public LocalB b;
    @Pure public LocalA(){
    }
    
}

public class LocalB{
	public int St=0;
    public LocalA unA;
    public LocalB unB;
    @Local public LocalA a;
    @Local public LocalB b;
    @Fresh public LocalB(){
    }
    
}

public class Tester{
		LocalB B;
		@Local LocalB LB;
		//Allowed Constructor!!!
	@Local public Tester(){
		
	}
	
	@Local public void NoMyAllowed(){
		B.St++; // Wrong!!
		B.unA.St++; // Wrong!!
		B.unB.St++; // Wrong!!
		B.unA.a.St++; //Wrong!!
		B.unA.b.St++; //Wrong!!
		B.unA.p.St++; //Wrong!!
		B.unA.p.a.a.St++; //Wrong!!
		B.unB.unA.St++; //Wrong!!
		B.unA.St++; //Wrong!!
		B.unA=new LocalA(); // Wrong!!
	}
   
}


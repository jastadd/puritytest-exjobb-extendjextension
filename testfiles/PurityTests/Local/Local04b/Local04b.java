import lang.ast.SideEffect.*;
import lang.ast.*;
public class ASTNode<T>{
	@Local protected ASTNode[] children;
}
public class List<T> extends ASTNode<T>{
	int i=0;
	public ASTNode(T... initialChildren) {
		children = new ASTNode[initialChildren.length];
		for (int i = 0; i < children.length; ++i) {
			addChild(initialChildren[i]);
		}
	}
		  /**
		   * @declaredat ASTNode:25
		   */
	public List<T> addAll(Iterable<? extends T> c) {
		for (T node : c) {
			addChild(node);
		}
		return this;
	}
		  
	@SideEffect.Local public void addChild(T node) {
		children[i++]=node;
	}
}

import lang.ast.SideEffect.*;
public class Parent{
	public int St=0;
    public Parent p;
	@Local Parent poL;
    @Local public Parent s;
    @Fresh public Parent(){
    }
    
}

public class Tester{
		Parent po;
		@Local Parent poL;
   @Local public void Test01(){
	   Parent P=new Parent();
	   Parent Q=new Parent();
	   P.p=Q; // new State so okey; 
	   P.p.St++; // Okey due to known alias!
	   P.s.St++; // Okey due to extended locality
	   P.St++; // new state so okey!!
	   po=P; // okey!!
	   po.poL=P; // okey due to Alias with P : All the state is still Fresh!
	   po.poL=poL; // P is no longer Fresh
   }

//   @Local public void Test02(){
//	   Parent P=new Parent();
//	   Parent Q=new Parent();
//	   P.p.St++; // wrong p is not part of locality so okey!
//	   P.St++; // new state so okey!!
//	   po.p=P; // Okay due to P Fresh!!
//	   po.p=poL; // Wrong locality ends with po access!!
//     }
    
//   @Local public void Test01(){
//       Parent P=new Parent();
//	   Parent Q=new Parent();
//	   P.p.St++; //  wrong p is not part of locality so okey!
//	   po.p=P; // Wrong locality ends with the po access!!
//	   po=P; // okey!!
//     }
}


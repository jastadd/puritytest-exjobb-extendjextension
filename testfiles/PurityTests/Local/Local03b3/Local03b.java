import lang.ast.SideEffect.*;
public class LocalA{
	public int St=0;
    public LocalA p;
    @Local public LocalA a;
    @Local public LocalB b;
    @Pure public LocalA(){
    }
    
}

public class LocalB{
	public int St=0;
    public LocalA unA;
    public LocalB unB;
    @Local public LocalA a;
    @Local public LocalB b;
    @Fresh public LocalB(){
    }
    
}

public class Tester{
		LocalB B;
		@Local LocalB LB;
		//Allowed Constructor!!!
	@Local public Tester(){
		
	}
		
   @Local public void LocalAllowed(){
	 LocalB b = new LocalB(); // Fresh
	 b.St++; // Okey!!
	 b.a.St++; //Okay!!
	 b.b.St++; // Okay!!
	 b.a.a.St++; //Okay!!
	 b.a.b.St++; //Okay!!
	 b.a.p.St++; //Wrong!!
	 b.a.p.a.a.St++; //Wrong!!
	 b.b.unA.St++; //Wrong!!
	 b.a.St++; //Okay!!
	 b.b=new LocalA(); // Okey!!
   }
   
}


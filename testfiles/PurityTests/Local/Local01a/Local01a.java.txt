testfiles\PurityTests\Local\Local01a\Local01.java
Method is annotated Local.
For Java method not part of attribute calculation

Purity problem in category:METHODCALL
17:Parent P = new Parent(); causes side effects.
For method call not externally specified.The method call Parent.Parent() is impure.

in testfiles\PurityTests\Local\Local01a\Local01.java:Tester

Purity problem in category:METHODCALL
18:Parent Q = new Parent(); causes side effects.
For method call not externally specified.The method call Parent.Parent() is impure.

in testfiles\PurityTests\Local\Local01a\Local01.java:Tester

Purity problem in category:VARWRITE
19:P.p.St++; violates the annotations at St
19:17_P.p.St: write to visible field not in the locality and not in a fresh object which is not fresh because 
->the variable P is initizised to new Parent() that's not fresh.
It's only of freshness Fresh
-> the constructor new Parent() is not Fresh
->the variable P is initizised to new Parent() that's not fresh.
It's only of freshness Fresh.

in testfiles\PurityTests\Local\Local01a\Local01.java:Tester

Purity problem in category:VARWRITE
21:po.p = P; violates the annotations at p
21:po.p: write to visible field not in the locality and not in a fresh object which is not fresh because 
->po is a object data field and represent unfresh State since not in constructor.

in testfiles\PurityTests\Local\Local01a\Local01.java:Tester

Purity problem in category:VARWRITE
22:po.p = poL; violates the annotations at p
22:po.p: write to visible field not in the locality and not in a fresh object which is not fresh because 
->po is a object data field and represent unfresh State since not in constructor.

in testfiles\PurityTests\Local\Local01a\Local01.java:Tester

5 warnings generated

import lang.ast.SideEffect.*;
public class Parent{
	public int St=0;
    public Parent p;
	@Local Parent poL;
    @Local public Parent s;
    @Fresh public Parent(){
    }
    
}

public class Tester{
		Parent po;
		@Local Parent poL;

   @Local public void Test02(){
	   Parent P=new Parent();
	   Parent Q=new Parent();
	   P.p.St++; // wrong p is not part of locality so okey!
	   P.St++; // new state so okey!!
	   po.p=P; // Okay due to P Fresh!!
	   po.p=poL; // Wrong locality ends with po access!!
     }
}


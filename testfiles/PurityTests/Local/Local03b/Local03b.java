import lang.ast.SideEffect.*;
public class LocalA{
	public int St=0;
    public LocalA p;
    @Local public LocalA a;
    @Local public LocalB b;
    @Pure public LocalA(){
    }
    
}

public class LocalB{
	public int St=0;
    public LocalA unA;
    public LocalB unB;
    @Local public LocalA a;
    @Local public LocalB b;
    @Fresh public LocalB(){
    }
    
}

public class Tester{
		LocalB B;
		@Local LocalB LB;
		//Allowed Constructor!!!
	@Local public Tester(){
		
	}
		
	public void MyAllowed(){
		LocalB b = new LocalB(); // Fresh
		b.St++;
		b.unA.St++; 
		b.unB.St++; 
		b.unA.a.St++; 
		b.unA.b.St++; 
		b.unA.p.St++; 
		b.unA.p.a.a.St++; 
		b.unB.unA.St++; 
		b.unA.St++; 
		b.unA=new LocalA();
	}
   
}


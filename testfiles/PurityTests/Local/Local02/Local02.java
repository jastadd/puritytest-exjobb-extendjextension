import lang.ast.SideEffect.*;
public class Parent{
	@Local public int St=0; //Wrong!!
    public Parent p;
    @Local public Parent s;
    @Fresh public Parent(){
    }
    
}

public class Tester{
		Parent po;
		@Local Parent poL;
   @Local public void Test01(){
	   Parent P=new Parent();
	   Parent Q=new Parent();
	   P.p=Q; //Assign Fresh Okey 
	   P.p.St++; // correct under Q!!!
	   P.St++; // new state so okey!!
	   po=P; // okey!!
	   po.p=P; // correct due to aliasing (po=P)
   }
   
   @Local public void Test02(){
	   Parent P=new Parent();
	   Parent Q=new Parent();
	   P.s.St++; // correct!!!
	   P.St++; // new state so okey!!
	   po.p=P; // Wrong locality ends with the po access!! this should give error
   }
   
   @Local public void Test01(){
	   Parent P=new Parent();
	   Parent Q=new Parent();
	   P.s.St++; // new state so okey!
	   po.p=P; // Wrong locality ends with the po access!!
	   po=P; // okey!!
   }
}


import lang.ast.SideEffect.*;
public class LocalA{
	public int St=0;
    public LocalA p;
    @Local public LocalA a;
    @Local public LocalB b;
    @Pure public LocalA(){
    }
    
}

public class LocalB{
	public int St=0;
    public LocalA unA;
    public LocalB unB;
    @Local public LocalA a;
    @Local public LocalB b;
    @Fresh public LocalB(){
    }
    
}

public class Tester{
		LocalB B;
		@Local LocalB LB;
		//Allowed Constructor!!!
	@Local public Tester(){
		
	}
		
   @Local public void LocalAllowed(){
	 LocalB b = new LocalB(); // Fresh
	 b.St++; // Okey!!
	 b.unA.St++; // Wrong!!
	 b.unB.St++; // Wrong!!
	 b.unA.a.St++; //Wrong!!
	 b.unA.b.St++; //Wrong!!
	 b.unA.p.St++; //Wrong!!
	 b.unA.p.a.a.St++; //Wrong!!
	 b.unB.unA.St++; //Wrong!!
	 b.unA.St++; //Wrong!!
	 b.unA=new LocalA(); // Okey!!
   }
   
}


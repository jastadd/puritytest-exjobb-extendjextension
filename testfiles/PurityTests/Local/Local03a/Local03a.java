import lang.ast.SideEffect.*;
public class LocalA{
	public int St=0;
    public LocalA p;
    @Local public LocalA a;
    @Local public LocalB b;
    @Pure public LocalA(){
    }
    
}

public class LocalB{
	public int St=0;
    public LocalA unA;
    public LocalB unB;
    @Local public LocalA a;
    @Local public LocalB b;
    @Fresh public LocalB(){
    }
}

public class Tester{
		LocalB B;
		@Local LocalB LB;
		//Allowed Constructor!!!
	@Local public Tester(){
		
	}

	@Local public void NoLocalAllowed(){
		LB.St++; // Okey!!
		LB.unA.St++; // Wrong!!
		LB.unB.St++; // Wrong!!
		LB.unA.a.St++; //Wrong!!
		LB.unA.b.St++; //Wrong!!
		LB.unA.p.St++; //Wrong!!
		LB.unA.p.a.a.St++; //Wrong!!
		LB.unB.unA.St++; //Wrong!!
		LB.unA.St++; //Wrong!!
		LB.unA=new LocalA(); // Okey!!
	}
}


import lang.ast.SideEffect.*;
public class Parent{
	public int St=0;
    public Parent p;
	@Local Parent poL;
    @Local public Parent s;
    @Fresh public Parent(){
    }
    
}

public class Tester{
		Parent po;
		@Local Parent poL;
		
   @Local public void Test01(){
       Parent P=new Parent();
	   Parent Q=new Parent();
	   P.p.St++; //  wrong p is not part of locality so okey!
	   po.p=P; // Wrong locality ends with the po access!!
	   po=P; // okey!!
     }
}


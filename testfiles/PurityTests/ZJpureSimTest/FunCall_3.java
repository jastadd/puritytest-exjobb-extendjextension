import lang.ast.SideEffect.*;
public class FunCall_3 {
    @Pure public void f(String... args) {
	for(String s : args) {
	    System.out.println(s.toString());
	}
    }

    @Pure public void g(String x, String y) {
	f(x,y);
    }
}

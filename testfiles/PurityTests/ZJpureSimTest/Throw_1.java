import lang.ast.SideEffect.*;
public class Throw_1 {
    @Pure public void g() {
	
    }

    @Pure public void f() {
	try {
	    g();
	} catch(NullPointerException e) {
	    throw e;
	}
    }
}

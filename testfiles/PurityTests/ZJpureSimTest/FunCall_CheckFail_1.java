import lang.ast.SideEffect.*;
public class FunCall_3 {
    @Pure public void f(String... args) {
	for(String s : args) {
	    System.out.println(s.toString());
	}
    }

    public void g(String x) {
	f(x,null);
    }
}

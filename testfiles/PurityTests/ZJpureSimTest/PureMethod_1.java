import lang.ast.SideEffect.*;
import java.util.*;

public class PureMethod_1 {
    @Local public ArrayList<String> array = new ArrayList<String>();
    
    @Fresh public boolean contains(String x) {
	for(String s : array) {
	    if(s == x) {
		return true;
	    }
	}
	return false;
    }
}

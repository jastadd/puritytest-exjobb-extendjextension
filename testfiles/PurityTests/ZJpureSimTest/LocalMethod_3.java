import lang.ast.SideEffect.*;
public class LocalMethod_3 {
    int x;

    @Local public void f() {
	x = 1;
    }

    @Local public void g() {
	f();
    }
}

import lang.ast.SideEffect.*;
public class Do{
		String x;
	@Fresh public Do(){
			
	}
	
	@Local public void example1(){
		int i=0;
		do {
			x=x+"i:"+i;
			i++;
		   }
		while(i>10);
	}

	@Fresh public Do example(){
		Do ex=new Do();
		int i=0;
		do {
			ex=new Do();
			i++;
		}while(i>10);
			return ex;
	}
	
    class Link{
        int x;
        @Local Link Alpha;
        Link Beta;
    }
}
package lang.ast;
import lang.ast.SideEffect.*;
import java.util.*;
public class FreshExample1{
	  @Secret(group="assignedAfter_Variable") protected java.util.Map assignedAfter_Variable_values=null;

	  @Pure public void testerA(@Fresh ASTState.CircularValue p){
		  
	  }
	  
	  @Pure public void tester(@Fresh Object p){
		  
	  }
	  
	  @Pure(group="assignedAfter_Variable") public boolean assignedAfter(Object v) {
	    Object _parameters = v;
	    //assignedAfter_Variable_values = new java.util.HashMap(4);
	    if (assignedAfter_Variable_values == null) assignedAfter_Variable_values = new java.util.HashMap(4);
	    ASTState.CircularValue _value;
	    if (assignedAfter_Variable_values.containsKey(_parameters)) {
	       Object _cache = assignedAfter_Variable_values.get(_parameters);
	      if (!(_cache instanceof ASTState.CircularValue)) {
	        return (Boolean) _cache;
	      } else {
	        _value = (ASTState.CircularValue) _cache;
	      }
	    } else {
	      _value = new ASTState.CircularValue(); 
	      assignedAfter_Variable_values.put(_parameters, _value);
	      _value.value = true;
	    } 	
	    tester(_cache);
	    ASTState state = state();
	    if (!state.inCircle() || state.calledByLazyAttribute()) {
	      state.enterCircle();
	      boolean new_assignedAfter_Variable_value;
	      do { 
	    	_value.Loc();
	    	tester(_value);
	    
	        _value.cycle = state.nextCycle();
	        new_assignedAfter_Variable_value = assignedAfter_compute(v);
	        if (new_assignedAfter_Variable_value != ((Boolean)_value.value)) {
	          state.setChangeInCycle();
	          _value.value = new_assignedAfter_Variable_value;
	        }
	      } while (state.testAndClearChangeInCycle());
	      assignedAfter_Variable_values.put(_parameters, new_assignedAfter_Variable_value);

	      state.leaveCircle();
	      return new_assignedAfter_Variable_value;
	    } else if (_value.cycle != state.cycle()) {
	      _value.cycle = state.cycle();
	      boolean new_assignedAfter_Variable_value = assignedAfter_compute(v);
	      if (new_assignedAfter_Variable_value != ((Boolean)_value.value)) {
	        state.setChangeInCycle();
	        _value.value = new_assignedAfter_Variable_value;
	      }
	      return new_assignedAfter_Variable_value;
	    } else {
	      return (Boolean) _value.value;
	    }
	  }
}
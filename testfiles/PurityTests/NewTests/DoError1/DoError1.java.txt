testfiles\PurityTests\NewTests\DoError1\Do.java
Method is annotated Pure.
For Java method not part of attribute calculation

Purity problem in category:VARWRITE
12:x = x + "i:" + i; violates the annotations at x
12:x:write effects the this object but this object is not being constructed or locked and thus the method is impure

in testfiles\PurityTests\NewTests\DoError1\Do.java:Do

1 warnings generated

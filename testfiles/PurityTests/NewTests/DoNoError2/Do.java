import lang.ast.SideEffect.*;
public class Do{
		String x;
	@Fresh public Do(){
			
	}
	
	@Fresh public Do example(){
		Do ex=null;
		int i=0;
		do {
			ex=null;
			i++;
		}while(i>10);
		ex=new Do();
			return ex;
	}
	
    class Link{
        int x;
        @Local Link Alpha;
        Link Beta;
    }
}
import lang.ast.SideEffect.*;
public class NonFreshExample2{
	static Link l; 
	static ELink le; 
	// Correct !!
	@NonFresh public Link ReturnLink(Link alpha,int x){
		Link newlink = new Link(); // Fresh
		newlink.x=x;
		newlink.Beta = alpha; //Okey
		newlink.Alpha = alpha; // Wrong newlink is MayBeFresh!! (1)
		return alpha; //Wrong!! Pga line:9 (2)
	}
	
	@NonFresh public Link ReturnLink2(@NonFresh Link alpha,int x){
		Link newlink=ReturnLink(alpha,5);
		newlink.Alpha.x=5+5; //Incorrect since In locality of NONFRESH (3)
		newlink.Alpha = Link; // Wrong!! (4)
		newlink.Alpha = l; // Wrong!! since static -> default is NONFRESH (5)
		return newlink; 
	}
	
	// Correct !!
	@NonFresh public ELink ReturnLinkE(ELink alpha,int x){
		ELink newlink = new ELink(); // Fresh + Wrong context for Entity creation!! (6)
		newlink.x=x;
		newlink.Beta = alpha; //Okey since type is Entity -> default position is that entitys are assumed NonFresh 
		newlink.Alpha = alpha; // Wrong newlink is MayBeFresh!! (7)
		return alpha; 
	}
	
	@NonFresh public ELink ReturnLink2E(ELink alpha,int x){
		Link newlink=ReturnLink(alpha,5);
		ELink link = new ELink(); //Wrong context for Entity creation!!
		newlink.Alpha.x=5+5; //Wrong Since link.alpha is in locality and is fresh if newlink2 is.
		newlink.Alpha = link; // Wrong!!
		newlink.Alpha = l; // Still Wrong!! since static -> default is static NONFRESH
		return newlink; 
	}
	public class Link{
        int x;
        @Local Link Alpha;
        Link Beta;
    }
    
	@Entity public class ELink extends Link{
        int x;
        @Local ELink Alpha;
        ELink Beta;
    }
    
}  


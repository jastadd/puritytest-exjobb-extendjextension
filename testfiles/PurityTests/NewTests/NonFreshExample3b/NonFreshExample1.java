import lang.ast.SideEffect.*;
public class NonFreshExample1{
	static Link l; 
	// Correct !!
	@NonFresh public Link ReturnLink(@NonFresh Link alpha,int x){
		Link newlink = new Link(); // Fresh
		newlink.x=x;
		newlink.Beta = alpha;
		newlink.Alpha = alpha; // newlink is MayBeFresh!!
		return alpha; 
	}
	
	@NonFresh public Link ReturnLink2(@NonFresh Link alpha,int x){
		Link newlink=ReturnLink(alpha,5);
		newlink.Alpha.x=5+5; //Wrong Since link.alpha is in locality and is fresh if newlink2 is.
		newlink.Alpha = Link; // Wrong!!
		return newlink; 
	}
	
	
    @Entity class Link{
        int x;
        @Local Link Alpha;
        Link Beta;
        
        public Link(){
        	x=5;
        	//2 errors (Alpha + Beta)
        }
        
    }
}
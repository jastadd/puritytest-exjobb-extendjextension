import lang.ast.SideEffect.*;
public class Do{
		String x;
		Do d;
	@Fresh public Do(){
			
	}
	
	@Pure public void example1(){
		int i=0;
		do {
			x=x+"i:"+i;
			i++;
		}while(i>10);
	}

	@Fresh public Do example(){
		Do ex=d; 
		int i=0;
		while(i>10){
			ex=new Do();
			i++;
		}
		return ex;
	}
	
    class Link{
        int x;
        @Local Link Alpha;
        Link Beta;
    }
}
import lang.ast.SideEffect.*;
public class FreshExample1{
	
	// Correct !!
	@Fresh public Link CreateLink(Link alpha,int x){
		Link newlink = new Link(); // Fresh
		newlink.x =x;
		newlink.Beta = alpha;
		newlink.Alpha = newlink; // Okey
		return newlink; 
	}
	
	@Fresh public Link CreateLink2(Link alpha,int x){
		Link newlink=new Link();
		Link newlink2=CreateLink(newlink,5) ;
		newlink2.Alpha.x=5+5; //Okay Since link.alpha is in locality and is fresh if newlink2 is.
		newlink2.Alpha = newlink; // Okey
		newlink2.Beta = newlink;
		return newlink2; 
	}
	
	
    class Link{
        int x;
        @Local Link Alpha;
        Link Beta;
    }
}
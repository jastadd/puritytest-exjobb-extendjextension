import lang.ast.SideEffect.*;
public class Main{
    
    @Pure public static Delta main(String[] args){
    Delta x=new Delta(40);
    Delta y=new Delta(30);
    x=y;
    y.inc();
    return x;
    }
    
    public class Delta{
       int x;
    @Pure public Delta(int y){
        x=y;
    } 
    
    @Local public void inc(){
        x++;
    }
    
    }
}


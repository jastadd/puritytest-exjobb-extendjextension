import lang.ast.SideEffect.*;
public class While1 {
    int x=0;
    @Pure void pureAction(){
        while(x>10){
            x++;
        }
        x++;
    }
    
    @Local void LocalAction(){
        while(x>10){
            x++;
        }
        x++;
    }
}
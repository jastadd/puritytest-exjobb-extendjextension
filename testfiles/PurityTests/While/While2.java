import lang.ast.SideEffect.*;
public class While2 {
    int x=0;
    @Local void pureAction(){
        while(x>10){
            x++;
        }
    }
}
import lang.ast.SideEffect.*;
public class While3 {
    int x=0;
    @Pure void pureAction(){
        while(x>10){
            x++;
        }
    }
}
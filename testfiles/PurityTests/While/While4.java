import lang.ast.SideEffect.*;
public class While4 {
    int x=0;
    @Pure void pureAction(){
        while(x>10){
            System.out.println(x);
        }
    }
    public while3(){
        x++;
    }
}
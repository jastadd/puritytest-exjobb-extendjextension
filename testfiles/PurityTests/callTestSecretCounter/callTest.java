import lang.ast.SideEffect.*;
public class callTest{
	@Secret(group="a") static int i;
	//Impure
	
	@Pure(group="a") public callTest(){
		 i++;
	}
	@Pure public void PureZ(){
		new callTest(); //static counter
	}
}
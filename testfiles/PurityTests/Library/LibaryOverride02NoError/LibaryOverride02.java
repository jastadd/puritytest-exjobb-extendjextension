import lang.ast.SideEffect.*;
import java.util.ArrayList;
public class Libary01<T> extends ArrayList<T>{
	
	@Fresh public Libary01(){
		super();
	}
	
	@Local @Override public T add(T ex){
		super.add(ex);
	}
	
}

package t;
import lang.ast.SideEffect.*;
public class Tests {
	
  @Fresh public Object test(){
    String test="WERTO";
    Object o= this;
    o.getClass();
    return o.getClass();
  }
  
  @Fresh public Object g(){
	  return A(); //Is Fresh!
  }
  
  @Fresh public Tests(){
	    String test="WERTO";
	    Object o= this;
	    o.getClass();
	    test.toLowerCase();
	  }
  
  public Object A(){
	  Object o= new Tests();
	  return o;
  }
	  
}

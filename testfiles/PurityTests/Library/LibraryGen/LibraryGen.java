import lang.ast.SideEffect.*;
@Entity public class ASTNode<T extends ASTNode> extends beaver.Symbol implements Cloneable {
	
  @Fresh public ASTNode<T> clone() throws CloneNotSupportedException {
    ASTNode node = (ASTNode) super.clone();
    node.flushAttrAndCollectionCache();
    return node;
  }
  
  @Ignore public void flushAttrAndCollectionCache(){
	  
  }
  
}


@Entity public class ASTNodeD{
	@Fresh public ASTNodeD clone() throws CloneNotSupportedException {
		    ASTNodeD node = (ASTNodeD) super.clone();
		    node.flushAttrAndCollectionCache();
		    return node;
	}
		  
	@Ignore public void flushAttrAndCollectionCache(){
			  
	}
}
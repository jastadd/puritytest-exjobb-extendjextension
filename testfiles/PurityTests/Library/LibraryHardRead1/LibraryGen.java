import lang.ast.SideEffect.*;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.*;
@Entity public class ASTNode<T extends ASTNode> extends beaver.Symbol implements Cloneable {
	
	  /** @apilevel internal 
	   * @declaredat ASTNode:43
	   */
	  @Fresh(group="_ASTNode") public Abs copy() {
	    try {
	      Abs node = (Abs) clone();
	      node.parent = null;
	      if (children != null) {
	        node.children = (ASTNode[]) children.clone();
	      }
	      return node;
	    } catch (CloneNotSupportedException e) {
	      throw new Error("Error: clone not supported for " + getClass().getName());
	    }
	  }
  
}


import lang.ast.SideEffect.*;
// Use Libary to modify string!!
public class Libary01 extends Delta{
	@Pure public String x(){
		return delta(); //Error by assumption
	}
	
	@Local public String delta(){ //Override error via Pure
		String x="nejman";
		return x;
	}
}
public class Delta{
	String v;
	public String delta(){
		v="aman"; // Need Local but have Pure
		return v;
	}
}

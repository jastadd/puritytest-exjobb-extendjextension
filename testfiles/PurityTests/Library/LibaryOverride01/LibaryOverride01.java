import lang.ast.SideEffect.*;
// Perfectly Correct Way Of using Secret
public class Libary01{
	int x=0;
	
	
	@Override public String toString(){
		x++;
		return "helloWerro!";
	} 
	
	@Pure public void Do(){
		toString();
	}
	
	//okey since String should be seperate from this toString object!!!
	@Pure public void Zeta(){
		String s="myWorldo";
		s.toString();
	}
}

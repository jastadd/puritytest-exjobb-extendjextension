import lang.ast.SideEffect.*;
// Use Libary to modify string!!
public class Libary01 extends Delta{
	@Pure public String x(){
		return delta();
	}
	
	@Local public String delta(){
		String x="nejman";
		return x;
	}
}
public class Delta{
	@Local public String delta(){
		String v="aman";
		return v;
	}
}

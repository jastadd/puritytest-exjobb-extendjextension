testfiles\PurityTests\Library\Libary01\Libary01.java
Method is annotated Pure.
For Java method not part of attribute calculation

Purity problem in category:METHODCALL
6:return r.trim(); causes side effects.
For method call not externally specified.The method call java.lang.String.trim() called for 5_r is impure.

in testfiles\PurityTests\Library\Libary01\Libary01.java:Libary01

Method is annotated Pure.
For Java method not part of attribute calculation

Purity problem in category:METHODCALL
11:return (r + " plus  !!  ").trim(); causes side effects.
For method call not externally specified.The method call java.lang.String.trim() called for Primitive is impure.

in testfiles\PurityTests\Library\Libary01\Libary01.java:Libary01

2 warnings generated

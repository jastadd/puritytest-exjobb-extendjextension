import lang.ast.SideEffect.*;
// Use Libary to modify string!!
public class Libary01{
	@Pure public String x(){
		String r="hello world";
		return r.trim();
	}
	
    @Pure public String x(){
		String r="hello world";
		return (r+" plus  !!  ").trim();
	}
}

import lang.ast.SideEffect.*;
public class ListAdd{
	private ArrayList<Object> list;

	@Pure public ArrayList<Object> list(){
		return list;
	}

	@Pure public ArrayList<Object> largerlist(){
		ArrayList<Object> list=list();
		list.add(this);
		return list; 
	}

}
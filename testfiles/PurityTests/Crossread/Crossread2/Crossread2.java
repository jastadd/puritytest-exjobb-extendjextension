import lang.ast.SideEffect.*;
public class Crossread2{
    Link l;
    int[] a;
    int k;

    //Okay
    @Pure public Crossread2(){
        l=new Link();
    	l.x = 4;
        a = new int[4];
        a[3]= 4;
       
    }
    
    @Pure public void xDo(){
        a = new int[4]; //Nope!!
        a[2]=2;
        a[3]=4;
        l.x = 4; //Nope!!
        a[3]= 4;
       
    }
    
    @Pure public void xDo2(){
        Link laz=new Link();
        l=laz; //Wrong!!
        l.x=4; //Okay!!
        laz.x = 4;
        a = new int[4];
        a[3]= 4;
    }
    
    @Pure public int B(){
        return l.la.x;
    }
    
    @Pure public int A(){
        return l.la.x++; //Nope!!
    }
    
    class Link{
        int x;
        Link la;
    }
}
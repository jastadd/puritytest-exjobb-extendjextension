import lang.ast.SideEffect.*;
public class Override02{
	 @Secret(group="group1") @NonFresh Override02 a;
	 @Secret(group="group1") SecretA secret=null;
	
	 //Wrong!!
	@Fresh(group="group1") public Override02 freshMethod(){return a;}
	
	@Pure public int pureMethod(){return 1;}
	@Pure public int pureMethod2(){return 1;}
	@Pure public int pureMethod3(){return 1;}
}

public class inheritor01 extends Override02{
	 //Wrong!!
	@Fresh(group="group1") public Override02 freshMethod(){return a;}
	
	@Pure(group="group1")  public Override02 pureMethod(){return a;}
	@Local(group="group1") public Override02 localMethod(){return a;}
}

public class inheritor02 extends inheritor01{
	// Wrong
	@Fresh(group="group1") public Override02 freshMethod(){return a;}
	
	@Pure(group="group1")  public Override02 pureMethod(){return a;}
	@Local(group="group1") public Override02 localMethod(){return a;}
}
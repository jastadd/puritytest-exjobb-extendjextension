import lang.ast.SideEffect.*;
public class Override02{
	 @Secret(group="group1") @Fresh Override02 a;
	 @Secret(group="group1") SecretA secret=null;
	 @Secret(group="group1") Override02 b;
	
	
	@Fresh(group="group1") public Override02 freshMethod(){return a;}
	
	//Wrong!!
	@Pure(group="group1") public void mixer(){
		a=b;
	}
	//Wrong!!
	@Pure(group="group1") public void mixer2(){
		b=a;
	}	
	
	@Pure public int pureMethod(){return 1;}
	@Pure public int pureMethod2(){return 1;}
	@Pure public int pureMethod3(){return 1;}
}

public class inheritor01 extends Override02{
	@Fresh(group="group1") public Override02 freshMethod(){return a;}
	@Pure(group="group1")  public Override02 pureMethod(){return a;}
	@Local(group="group1") public Override02 localMethod(){return a;}
}

public class inheritor02 extends inheritor01{
	@Fresh(group="group1") public Override02 freshMethod(){return a;}
	@Pure(group="group1")  public Override02 pureMethod(){return a;}
	@Local(group="group1") public Override02 localMethod(){return a;}
}
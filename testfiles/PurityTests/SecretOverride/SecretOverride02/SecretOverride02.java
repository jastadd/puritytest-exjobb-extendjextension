import lang.ast.SideEffect.*;
public class Override02{
	 @Secret(group="group1") Override02 a=null;
	 @Secret(group="group1") SecretA secret=null;
	@Pure public int pureMethod(){return 1;}
	@Pure public int pureMethod2(){return 1;}
	@Pure public int pureMethod3(){return 1;}
}

public class inheritor01 extends Override02{
	@Local(group="group1") public Override02 localMethod(){return a;}
}

public class inheritor02 extends inheritor01{
	@Local(group="group1") public Override02 localMethod(){return a;}
}
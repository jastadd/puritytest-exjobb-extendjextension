testfiles\PurityTests\SecretOverride\SecretOverride03\SecretOverride03.java
Method is unannotated.
For Java method not part of attribute calculation

Purity problem in category:MISSINGMETHOD
11:pureMethod(): Has annotation problems:
The overriding methods must have stronger or equal annotation. This method are missing Pure or stronger from the supertype Override02


in testfiles\PurityTests\SecretOverride\SecretOverride03\SecretOverride03.java:inheritor01

Purity problem in category:SECRETREAD
11:super.a access secret state without any purity annotation at a

in testfiles\PurityTests\SecretOverride\SecretOverride03\SecretOverride03.java:inheritor01

Method is unannotated.
For Java method not part of attribute calculation

Purity problem in category:SECRETREAD
12:super.a access secret state without any purity annotation at a

in testfiles\PurityTests\SecretOverride\SecretOverride03\SecretOverride03.java:inheritor01

Method is annotated with group "group1",Pure.
For Java method not part of attribute calculation

Purity problem in category:SECRETREAD
16:super.a reads through secret without the secret group "", have "group1" at a

in testfiles\PurityTests\SecretOverride\SecretOverride03\SecretOverride03.java:inheritor02

Method is annotated with group "group1",Local.
For Java method not part of attribute calculation

Purity problem in category:SECRETREAD
17:super.a reads through secret without the secret group "", have "group1" at a

in testfiles\PurityTests\SecretOverride\SecretOverride03\SecretOverride03.java:inheritor02

5 warnings generated

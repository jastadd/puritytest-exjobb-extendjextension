import lang.ast.SideEffect.*;
public class Override02{
	 @Secret(group="group1") Integer a=null;
	 @Secret(group="group1") SecretA secret=null;
	
	@Fresh(group="group1") public Integer freshMethod(){return a;}
	
	@Pure public int pureMethod(){return 1;}
	@Pure public int pureMethod2(){return 1;}
	@Pure public int pureMethod3(){return 1;}
}

public class inheritor01 extends Override02{
	@Fresh(group="group1") public Integer freshMethod(){return a;}
	
	@Pure(group="group1")  public int pureMethod(){return a;}
	@Local(group="group1") public Integer localMethod(){return a;}
}

public class inheritor02 extends inheritor01{
	@Fresh(group="group1") public Integer freshMethod(){return a;}
	
	@Pure(group="group1")  public int pureMethod(){return a;}
	@Local(group="group1") public Integer localMethod(){return a;}
}
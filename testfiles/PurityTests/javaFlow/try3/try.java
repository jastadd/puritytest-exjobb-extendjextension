import lang.ast.SideEffect.*;
class test{

	int myX;
	@Local test l;
	static test sl;
	@Fresh public test(int x){
		myX=x;
	}
	
    @Pure public test(){
		myX=x;
		l=sl;
	}
	
		//Wrong! No initiation
	@Fresh public test try1(@Fresh test a){
		test t=new test(1);
		test b;
		try{
			t=b;
		}catch(Exception e){
			t=a;
		}finally{
			b=t;
		}
		return b;
	}

	@Fresh public test try2(@Fresh test a){
		test t=new test(1);
		test b=a;
		try{
			t=sl;
			a=b;
		}catch(Exception e){
			t=new test(1);
			b=a;
		}finally{
			b=t;
			t=a;
		}
		return b;
	}
	
	@Fresh public test try3(@Fresh test a){
		test t=new test(1);
		test b=sl;
		try{
			b=new test();
			t=new test(1);
			b=t;
		}catch(Exception e){
			t=sl;
			a=b;
			t=b;
		}finally{
			t=a;
		}
		return t;
	}
	@Fresh public test try3(@Fresh test a){
		test t=new test(1);
		test b;
		try{
			b=new test();
			t=new test(1);
			b=t;
		}catch(Exception e){
			t=sl;
			a=b;
			t=b;
		}finally{
			t=a;
		}
		return t;
	}
	@Fresh public test try4(test a){
		test t=new test(1);
		try{
			t=new test(1);
		}catch(Exception e){
			t=new test(1);
		}finally{
			t=sl;
		}
		return t;
	}
}
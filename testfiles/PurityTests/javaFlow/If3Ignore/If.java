import lang.ast.SideEffect.*;
class test{
	test[] arr;
	//Okay works ass Fresh!!
	@Ignore public test obtainer(){
		return arr[2]; 
	}
	
	@Fresh public test if4(){
		int x =4;
		test t=new test();
		if (x==5){
		t = obtainer();
		}else{
		t = new test();
		}
		return t;
	}
	
	@Fresh public test if5(){
		int x =4;
		test t=new test();
		if (x==5){
		t = new test();
		}else{
		t = obtainer();
		}
		return t;
	}
	
	@Fresh public test if6(){
		int x =4;
		test t=new test();
		if (x==5){
		t = new test();
		}else{
		}
		return t;
	}
	
	//Correct
	@Fresh public test if7(){
		int x =4;
		test t=new test();
		if (x==5){
		}else{
		t = new test();
		}
		return t;
	}
}
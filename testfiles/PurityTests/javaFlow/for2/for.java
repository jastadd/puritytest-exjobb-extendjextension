import lang.ast.SideEffect.*;
class test{
	test ts;
    @Fresh public void inito(){
		int i=4;
		if (i>0)
			return new test();
		return ts;
	}
	
	//Wrong! No initiation
	@Pure public test for1(){
		int x =4;
		test t=new test();
		for (int i=0;i<20;i++){
			t=new test();
			for (int k=0;i<k;k++){
				t=inito();
			}
		}
		return t;
	}

	//Wrong! No initiation
	@Fresh public test for2(){
		int x =4;
		test t=new test();
		for (int i=0;i<20;i++){
			t=inito();
			for (int k=0;i<k;k++){
				t=new test();
			}
		}
		return t;
	}
}
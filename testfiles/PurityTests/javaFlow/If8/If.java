import lang.ast.SideEffect.*;
class test{
	@Fresh public test(){
		
	}
	test to;
	
	@Fresh public test if1(){
		int x =4;
		test t=new test();
		if (x>30){
			if (x%30){
				t=to;
			}else if (x%2){
				t=new test();
			}else{
				t=new test();
			}
		}else{
			if (x%30){
				t=new test();
			}else if (x%2){
				t=new test();
			}else{
				t=new test();
			}
		}
		return t;
	}

	@Fresh public test if2(){
		int x =4;
		test t=new test();
		if (x>30){
			if (x%30){
				t=new test();
			}else if (x%2){
				t=to;
			}else{
				t=new test();
			}
		}else{
			if (x%30){
				t=new test();
			}else if (x%2){
				t=new test();
			}else{
				t=new test();
			}
		}
		return t;
	}
	
	@Fresh public test if3(){
		int x =4;
		test t=new test();
		if (x>30){
			if (x%30){
				t=new test();
			}else if (x%2){
				t=new test();
			}else{
				t=to;
			}
		}else{
			if (x%30){
				t=new test();
			}else if (x%2){
				t=new test();
			}else{
				t=new test();
			}
		}
		return t;
	}
	
	@Fresh public test if4(){
		int x =4;
		test t=new test();
		if (x>30){
			if (x%30){
				t=new test();
			}else if (x%2){
				t=new test();
			}else{
				t=new test();
			}
		}else{
			if (x%30){
				t=to;
			}else if (x%2){
				t=new test();
			}else{
				t=new test();
			}
		}
		return t;
	}
	
	@Fresh public test if5(){
		int x =4;
		test t=new test();
		if (x>30){
			if (x%30){
				t=new test();
			}else if (x%2){
				t=new test();
			}else{
				t=new test();
			}
		}else{
			if (x%30){
				t=new test();
			}else if (x%2){
				t=to;
			}else{
				t=new test();
			}
		}
		return t;
	}

}
import lang.ast.SideEffect.*;
class test{
	//Correct
	@Fresh public test if3(){
		int x =4;
		test t=new test();
		if (x==5){
		t = new test();
		}else{
		t = new test();
		}
		return t;
	}	
}
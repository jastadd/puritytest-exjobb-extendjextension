import lang.ast.SideEffect.*;
class test{
	test x;
	@Pure public test inito(int a){
		return a == 0 ? this : x;
	}
	//Wrong! No initiation
	@Fresh public test if1(){
		int x =4;
		test t=inito(2);
		if (x==5){
		test t = new test();
		}else{
		test t = new test();
		}
		return t;
	}
}
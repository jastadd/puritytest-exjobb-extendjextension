import java.util.Set;
import java.util.HashSet;

import lang.ast.SideEffect.*;
class test{
	Set<Integer> iter;
	@Fresh public test for1(){
	  	Set<Integer> temp = new HashSet<Integer>();
	  	
	  	for(Integer f : iter)
	  	{
	  		temp.add(enhance(f));
	  		temp.add(1);
	  	}
	  	
	  	return temp;
	}
	
	@Pure public int enhance(int f){
		return (f*f*f)%10000;
	}
}
import lang.ast.SideEffect.*;
class test{
	@Fresh public test(){
		
	}
	test to;
	//Wrong! Since not analyzing forstmt
	@Fresh public test if1(){
		int x =4;
		test t=to;
		if (x>30){
			if (x%30){
				t=new test();
			}else if (x%2){
				t=new test();
			}else{
				for(int i=0;i<20;i++){
					t=new test();
				}
			}
		}else{
			t=new test();
		}
		return t;
	}
	//Wrong!!
	@Fresh public test if2(int z){
		int x =4;
		test t=to;
		if (x>30){
			if (x%30){
				t=new test();
			}else if (x%2){
				t=new test();
			}else{
				for(int i=z;i<20;i++){
					t=to;
				}
			}
		}else{
			t=new test();
		}
		return t;
	}
	//Wrong!!
	@Fresh public test if3(int z){
		int x =4;
		test t=to;
		if (x>30){
			if (x%30){
				t=new test();
			}else if (x%2){
				t=new test();
			}else{
				for(int i=z;i<20;i++){
					t=new test();
				}
			}
		}else{
			t=new test();
		}
		return t;
	}
	
	//Correct
	@Fresh public test if3(int z){
		int x =4;
		test t=to;
		if (x>30){
			if (x%30){
				t=new test();
			}else if (x%2){
				t=new test();
			}else{
				t=new test();
				for(int i=z;i<20;i++){
					t=new test();
				}
			}
		}else{
			t=new test();
		}
		return t;
	}
	
	
	@Fresh public test if4(){
		int x =4;
		test t=to;
		if (x>30){
			if (x%30){
				t=new test();
			}else if (x%2){
				t=new test();
			}else{
				t=new test();
			}
		}else{
			if (x%30){
				t=new test();
			}else if (x%2){
				t=new test();
			}else{
				t=new test();
			}
		}
		return t;
	}


}
import lang.ast.SideEffect.*;
class test{

	int myX;
	@Fresh public test(int x){
		myX=x;
	}
	
	@Pure public test(){
	}
	
	@Pure boolean h(int i){
		return i>0;
	}
	
	@Pure boolean checker(test t){
		int d=myX;
		int i=2;
		while(d>1){
			if (d%i){
				d=d/i;
				i++;
			}else{
				return false;
			}
		}
		return true;
	}
		//Wrong! No initiation
	@Fresh public test while1(){
		int x =4;
		test t=new test();
		while(h(x--)){
			if (checker(t))
				t=new test(x);
			t=new test(0);
		}
		return t;
	}
}
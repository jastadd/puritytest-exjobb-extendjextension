import lang.ast.SideEffect.*;
class test{
	@Fresh public test if4(){
		int x =4;
		test t=new test();
		if (x==5){
		t = null;
		}else{
		t = new test();
		}
		return t;
	}
	
	@Fresh public test if5(){
		int x =4;
		test t=new test();
		if (x==5){
		t = new test();
		}else{
		t = null;
		}
		return t;
	}
	
	@Fresh public test if6(){
		int x =4;
		test t=new test();
		if (x==5){
		t = new test();
		}else{
		}
		return t;
	}
	
	//Correct
	@Fresh public test if7(){
		int x =4;
		test t=new test();
		if (x==5){
		}else{
		t = new test();
		}
		return t;
	}
}
import lang.ast.SideEffect.*;
class test{

	int myX;
	@Local test l;
	static test sl;
	@Fresh public test(int x){
		myX=x;
	}
	
    @Pure public test(){
		myX=x;
		l=sl;
	}
	
		
	@Fresh public test try1(test a){
		test t=new test(1);
		try{
			t=new test(1);
		}catch(Exception e){
			t=new test(1);
		}finally{
			t=new test(1);
		}
		return t;
	}
	
	@Fresh public test try2(test a){
		test t=new test(1);
		try{
			t=sl;
		}catch(Exception e){
			t=new test(1);
		}finally{
			t=new test(1);
		}
		return t;
	}
	
	@Fresh public test try3(test a){
		test t=new test(1);
		try{
			t=new test(1);
		}catch(Exception e){
			t=sl;
		}finally{
			t=new test(1);
		}
		return t;
	}
	// Wrong sl in finally
	@Fresh public test try4(test a){
		test t=new test(1);
		try{
			t=new test(1);
		}catch(Exception e){
			t=new test(1);
		}finally{
			t=sl;
		}
		return t;
	}
}
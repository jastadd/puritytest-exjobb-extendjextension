import lang.ast.SideEffect.*;
class test{
	@Fresh public test(){
		
	}
	test to;
	//Wrong! No initiation
	@Fresh public test if1(){
		int x =4;
		test t=to;
		if (x==5){
			if (x=10){
			t = new test();
			}else{
			t = to;
			}
		}else{
		test t = new test();
		}
		return t;
	}
	//Wrong!
	@Fresh public test if2(){
		int x =4;
		test t;
		if (x==5){
			if (x=10){
			t = new test();
			}else{
			t = if1();
			}
		}else{
		test t = new test();
		}
		return t;
	}	
	//Correct
	@Fresh public test if3(){
		int x =4;
		test t;
		if (x==5){
			if (x=10){
			t = to;
			}else{
			t = null;
			}
			t=new test();
		}else{
		test t = new test();
		}
		return t;
	}	
	
	@Fresh public test if4(){
		int x =4;
		test t=new test();
		if (x==5){
			if (x=10){
			t = to;
			}else{
			t = null;
			}
		}else{
		test t = new test();
		}
		return t;
	}

	@Fresh public test if5(){
		int x =4;
		test t=new test();
		if (x==5){
		t = new test();
		}else{
		t = to;
		}
		return t;
	}

	@Fresh public test if6(){
		int x =4;
		test t=new test();
		if (x==5){
		t = new test();
		}else{
		}
		return t;
	}

	@Fresh public test if7(){
		int x =4;
		test t=new test();
		if (x==5){
		}else{
		t = new test();
		}
		return t;
	}
}
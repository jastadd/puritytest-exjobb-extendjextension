import lang.ast.SideEffect.*;
class test{

	int myX;
	@Fresh public test(int x){
		myX=x;
	}
	
	@Pure public test(){
		myX=x;
	}
	
	@Pure boolean h(int i){
		return i>0;
	}
	
	@Pure boolean checker(test t){
		return t.myX!=0;
	}
		//Wrong! No initiation
	@Fresh public test while1(test a){
		int x =4;
		test t=a;
		while(h(x--)){
			if (checker(t))
				t=new test(x);
		}
		return t;
	}
}
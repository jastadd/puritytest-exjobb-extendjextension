import lang.ast.SideEffect.*;
class test{
	test tx;
	// Auto promoted to fresh
	@Pure public test inito(){
		int x =4;
		test t=tx;
		if (x==5){
		test t = new test();
		}else{
		test t = new test();
		}
		return t;
	}
	//Wrong! No initiation
	@Fresh public test if1(){
		int x =4;
		test t=inito();
		for (int i=0;i<20;i++){
			t=new test();
			for (int k=0;i<k;k++){
				t=inito();
			}
		}
		return t;
	}
}
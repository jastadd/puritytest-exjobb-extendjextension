testfiles\PurityTests\javaFlow\for1\for.java
Method is annotated Pure.
For Java method not part of attribute calculation

Purity problem in category:METHODCALL
6:return i > 0 ? tx : new test(); causes side effects.
For method call not externally specified.The method call test.test() is impure.

in testfiles\PurityTests\javaFlow\for1\for.java:test

Method is annotated Fresh.
For Java method not part of attribute calculation

Purity problem in category:FRESHRETURN
18:return t;of Freshness NonFresh can't be returned from a @Fresh method because is not fresh because 
->the variable t is initizised to inito() that's not fresh.
It's only of freshness NonFresh
->The method call test.inito() is not annotated FreshIf,Fresh,Pure or Ignore so can't be assumed to return a newly allocated object.

in testfiles\PurityTests\javaFlow\for1\for.java:test

2 warnings generated

import lang.ast.SideEffect.*;
class test{
	test  tx;
	int i=2;
	@Pure public test inito(){
		return i>0 ? tx : new test();
	}
	//Wrong! No initiation
	@Fresh public test if1(){
		int x =4;
		test t=inito();
		for (int i=0;i<20;i++){
			t=new test();
			for (int k=0;i<k;k++){
				t=inito();
			}
		}
		return t;
	}
}
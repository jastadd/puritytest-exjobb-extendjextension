import lang.ast.SideEffect.*;
class test{
	//Correct!
	@Fresh public test if2(){
		int x =4;
		test t;
		if (x==5){
		t = new test();
		}else{
		t = new test();
		}
		return t;
	}	
}
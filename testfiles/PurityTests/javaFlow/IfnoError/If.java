import lang.ast.SideEffect.*;
class test{
	//Wrong! No initiation
	@Fresh public test if1(){
		int x =4;
		test t;
		if (x==5){
			if (x=10){
			t = new test();
			}else{
			t = prod();
			}
		}else{
		test t = new test();
		}
		return t;
	}

	@Fresh public test if2(){
		int x =4;
		test t;
		if (x==5){
			if (x=10){
			t = new test();
			}else{
			t = if1();
			}
		}else{
		test t = new test();
		}
		return t;
	}	
	
	@Fresh public test if3(){
		int x =4;
		test t;
		if (x==5){
			if (x=10){
			t = prod();
			}else{
			t = prod();
			}
			t=new test();
		}else{
		test t = new test();
		}
		return t;
	}	
	
	@Fresh public test if4(){
		int x =4;
		test t=new test();
		if (x==5){
			if (x=10){
			t = prod();
			}else{
			t = prod();
			}
		}else{
		test t = new test();
		}
		return t;
	}
	
	@Pure public test prod(){
		return new test();
	}
	
	@Fresh public test if5(){
		int x =4;
		test t=new test();
		if (x==5){
		t = new test();
		}else{
		t = prod();
		}
		return t;
	}
	
	@Fresh public test if6(){
		int x =4;
		test t=new test();
		if (x==5){
		t = new test();
		}else{
		}
		return t;
	}
	
	@Fresh public test if7(){
		int x =4;
		test t=new test();
		if (x==5){
		}else{
		t = new test();
		}
		return t;
	}
}
import lang.ast.SideEffect.*;
class test{

	@Local test t0;
	//Correct!
	@Fresh public test if2(){
		int x =4;
		test t;
		if (x==5){
			if (x=10){
			t = new test();
			}else{
			t = if3();
			}
		}else{
		 t = new test();
		}
		return t;
	}	
	//Correct
	@Fresh public test if3(){
		int x =4;
		test q=new test();
		test t;
		if (x==5){
			if (x=10){
			t = q.t0;
			}else{
			t = q.t0;
			}
			t=new test();
		}else{
		 t = new test();
		}
		return t;
	}	
	
	//Incorrect
	@Fresh public test if4(){
		int x =4;
		test q=new test();
		test t=new test();
		if (x==5){
			if (x=10){
			t =q.t0;
			}else{
			t = q.t0;
			}
		}else{
		test t = new test();
		}
		return t;
	}
	
	//Correct
	@Fresh public test if5(){
		int x =4;
		test q=new test();
		test t=new test();
		if (x==5){
		t = new test();
		}else{
		t = q.t0;
		}
		t=new test();
		return t;
	}
	
	//Correct
	@Fresh public test if6(){
		int x =4;
		test t=new test();
		if (x==5){
		t = new test();
		}else{
		}
		return t;
	}
	
	//Correct
	@Fresh public test if7(){
		int x =4;
		test t=new test();
		if (x==5){
		}else{
		t = new test();
		}
		return t;
	}
}
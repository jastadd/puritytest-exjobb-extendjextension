testfiles\PurityTests\javaFlow\try1\try.java
Method is annotated Pure.
For Java method not part of attribute calculation

Purity problem in category:VARWRITE
13:l = sl; violates the annotations at l
13:l:write of unFresh object to visible field in part of the extended locality from this.l which is Fresh. The unfresh object is obtained from Maybe is not fresh because 
->sl is a object data field and represent unfresh State since not in constructor.

in testfiles\PurityTests\javaFlow\try1\try.java:test

Method is annotated Fresh.
For Java method not part of attribute calculation

Purity problem in category:FRESHRETURN
62:return t;of Freshness Maybe can't be returned from a @Fresh method because is not fresh because 
->60:t = sl which which is an assignment of a value which effects the locality of (54_t:54_t:2)
->sl is a object data field and represent unfresh State since not in constructor
->the variable t is initizised to new test(1) that's not fresh.
It's only of freshness Fresh
-> the constructor new test(1) is not Fresh
->sl is a object data field and represent unfresh State since not in constructor.

in testfiles\PurityTests\javaFlow\try1\try.java:test

2 warnings generated

import lang.ast.SideEffect.*;
import java.lang.Exception;
public class throw2 {
    int x=0;
    @Fresh void pureAction(){
    	try{
    		throwAction();
    	}catch (Exception e){
    		return e;
    	}
    	return new throw2();
    }
	@Pure void throwAction() throws IOException{
    	throw new IOException();
    }
}
import lang.ast.SideEffect.*;
import java.io.IOException;
public class throw1 {
    
	@Fresh Object thrower(){
		try{
			throwAction();
		}catch(IOException e){
			return e;
		}
		return new throw1();
	}
	
	@Fresh Object throwAction() throws IOException{
    	throw new IOException();
    }
    
}
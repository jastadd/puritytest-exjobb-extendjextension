import lang.ast.SideEffect.*;
import java.io.IOException;
public class throw1 {
    
	@Pure void pureAction() throws IOException{
    	throw new IOException();
    }
    
    @Fresh void freshAction() throws IOException{
    	if (1>2)
    	throw new IOException();
    	new throw1();
    }
}
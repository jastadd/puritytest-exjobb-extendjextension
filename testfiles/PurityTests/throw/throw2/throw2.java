import lang.ast.SideEffect.*;
public class throw2{
    
	@Pure void pureAction() throws IOException{
		throw new Error("Error: clone not supported for " + getClass().getName());
    }
    //Wrong !! type!!
    @Fresh void freshAction() {
    	throw new Error("Error: clone not supported for " + getClass().getName());
    }
}
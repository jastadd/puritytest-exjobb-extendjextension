import lang.ast.SideEffect.*;
public class FunCall_3 {
    @Pure public void f(@Local FunCall_3... args) {
	for(FunCall_3 s : args) {
	    System.out.println(s.modify());
		}
    }

    @Pure public void fx(@Ignore FunCall_3... args) {
    	args[2].modify();
    	f(args);
    }
    
    @Local public String modify(){
    	return "";
    }
    
    @Pure public void g(FunCall_3 x) {
	f(x,null); //Wrong!!
    }
    
    @NonFresh public FunCall_3 gx(FunCall_3 x) {
	return x; //Wrong!!
    }
    
    @NonFresh public FunCall_3 gx() {
    	return this;
    }
}

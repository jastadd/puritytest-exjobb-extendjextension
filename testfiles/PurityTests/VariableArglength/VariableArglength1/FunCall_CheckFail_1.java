import lang.ast.SideEffect.*;
public class FunCall_3 {
    @Pure public void f(FunCall_3... args) {
	for(FunCall_3 s : args) {
	    System.out.println(s.modify());
	    s.modifyx();
		}
    }

    @Local public String modify(){
    	return "";
    }
    
    @Local public FunCall_3 modifyx(){
    	return this;
    }
    
    FunCall_3[] arr; 
   @Local public void g(FunCall_3 x) {
	arr[4]=x;
	f(x,null);
    }
}

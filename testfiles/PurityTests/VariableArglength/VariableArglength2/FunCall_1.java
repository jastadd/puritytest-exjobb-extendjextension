import lang.ast.SideEffect.*;
public class FunCall_3 {
    @Pure public void f(@Fresh FunCall_3... args) {
	for(FunCall_3 s : args) {
	    System.out.println(s.modify());
		}
    }
    
    @Fresh public FunCall_3 f(String... args,@Fresh FunCall_3 ap) {
    	    System.out.println(ap.modify());
    	    FunCall_3[] delta = new FunCall_3[3];
    	    return ap;
    }

    @Local public String modify(){
    	return "";
    }
    
    @Pure public void g(FunCall_3 x) {
	f("hello!",x);
	f(x);
	f(x,x,x,x,x,x);
    }
}

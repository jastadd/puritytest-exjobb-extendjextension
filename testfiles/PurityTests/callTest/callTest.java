import lang.ast.SideEffect.*;
public class callTest{
	@Ignore public void Ignore(){
		Ignore();
		Fresh();
		Pure();
		Local();
		Impure();
	}
	@Fresh public void Fresh(){
		Ignore();
		Fresh();
		Pure();
		Local(); // Error
		Impure();
	}
	@Pure public void Pure(){
		Ignore();
		Fresh();
		Pure();
		Local(); //Error
		Impure();
	}

	@Local public void Local(){
		Ignore();
		Fresh();
		Pure();
		Local();
		Impure(); //Error
	}
	public void Impure(){
		Ignore();
		Fresh();
		Pure();
		Local();
		Impure();
	}
}
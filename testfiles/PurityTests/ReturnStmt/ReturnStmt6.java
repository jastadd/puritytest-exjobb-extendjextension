import lang.ast.SideEffect.*;
public class ReturnStmt6 {

class D{
    
}

@Fresh public D wrong(){
    return freshannot();
}

@Fresh public D freshannot(){
    return new D();
}

}
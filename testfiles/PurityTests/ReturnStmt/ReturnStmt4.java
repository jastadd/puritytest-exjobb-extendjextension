import lang.ast.SideEffect.*;
public class ReturnStmt4 {

class D{
    
}

@Fresh public void wrong(){
    return new D();
}

@Fresh public D freshannot(){
    return new D();
}

}
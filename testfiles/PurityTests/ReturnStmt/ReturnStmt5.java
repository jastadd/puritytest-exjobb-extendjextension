import lang.ast.SideEffect.*;
public class ReturnStmt5 {

class D{
    
}

@Fresh public void wrong(){
    return ;
}

@Fresh public D freshannot(){
    return new D();
}

}
import lang.ast.SideEffect.*;
// Parent!
public class EntityCreation{
	  @SideEffect.Fresh(group="_ASTNode") public EntityP fullCopy() {
		    return treeCopyNoTransform();
	  }
	  /**
	   * Create a deep copy of the AST subtree at this node.
	   * The copy is dangling, i.e. has no parent.
	   * @return dangling copy of the subtree at this node
	   * @apilevel low-level
	   * @declaredat ASTNode:66
	   */
	  @SideEffect.Fresh(group="_ASTNode") public EntityP treeCopyNoTransform() {
		  EntityP tree = (EntityP) copy();
		  return tree;
	  }
}

@Entity public class EntityP{
	@Secret(group="A") public EntityP p=null;

	@Fresh public EntityP(){
		
	}
}
import lang.ast.SideEffect.*;
public class Main{
	@Pure public static void main(String[] args){
	A p=new A();
	A x=new B();
	x =new B();
	p.getOpen(); //Wrong!!
	x.getOpen(); 
	}
}

public class A{
    public int getOpen(){
    	return 5;}
}

public class B extends A{
	@Pure public int getOpen(){
		return 5;}
}
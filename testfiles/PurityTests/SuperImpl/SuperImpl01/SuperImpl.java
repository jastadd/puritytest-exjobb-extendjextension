import lang.ast.SideEffect.*;
// Parent!
public class Parent{
    private int open;

    public Parent(int x){
    open=x;
    }
    public int getOpen(){
    	return open++;
    }
}

public class SubClass extends Parent{
//Okey!!
	public Parent p;
	@Pure public SubClass(int x,Parent p){
		super(x);
		this.p=p;
	}
	//Override
	@Pure public int getOpen(){
		return super.open;
	}
	
	//Wrong !!!
	@Pure public int getPOpen(){
		return p.getOpen();
	}
	
	//Wrong !!!
	@Pure public int getPOpen(){
		return ((Parent)this).getOpen();
	}
	
	//Okey
	@Pure public int getOpenthis(){
		return this.getOpen();
	}
	
	//Okey
	@Pure public int getOpenthis(){
		return ((SubClass)p).getOpen();
	}
	
}
import lang.ast.SideEffect.*;
public class Main{	
	@Pure public static void main(String[] args){
	Parent p=new Parent(10);
	Parent x=new SubClass(100,p);
	p.getOpen();
	x.getOpen(); // Okey since local state change
	}
	
	@Pure public void dox(Parent P){
		P.getOpen(); // Wrong
	}
	
	@Pure public void doy(SubClass P){
		P.getOpen(); // Okey
	}
}


// Parent!
public class Parent{
    @Local private int open; //Wrong!!

    @Pure public Parent(int x){
    open=x;
    }
    //okey!!!
    @Local public int getOpen(){
    	return open++;
    }
    // Correct!!!
    @Local public int AltgetOpen(){
    	return open++;
    }
}

public class SubClass extends Parent{
//Okey!!
	public Parent p;
	@Fresh public SubClass(int x,Parent p){
		super(x);
		this.p=p;
	}
	//Override
	@Pure public int getOpen(){
		return super.open;
	}
	
	//Wrong !!!
	@Pure public int getPOpen(){
		return p.getOpen();
	}
	
	//Wrong !!!
	@Pure public int getPOpen(){
		return ((Parent)this).getOpen();
	}
	
	//Okey
	@Pure public int getOpenthis(){
		return this.getOpen();
	}
	
}
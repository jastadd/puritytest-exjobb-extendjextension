import lang.ast.SideEffect.*;
public interface test{
	@Pure public int increase();
	@Local public int increaseL();
	@Fresh public Parent ParentX();
}

// Parent!
public class Parent{
	public int St=0;
    public Parent p;
	private int open;
   @Fresh public Parent(int x){
    open=x;
    p=new Parent();
    }
    
}


public class SubClass extends Parent implements test {
    public SubClass(int x){
		super(x);
	}
    
    //Wrong!!!
    @Local public int betta(){
    	// Local by this -> p but not -> St++)
    	p.St++;
    	return p.St;
    }
    
    @Fresh public Parent ParentX(){
    	return new SubClass(0);
    }
    
    //Okey!!!
    @Local public int increaseL(){
    	return St++;
    }
}
import lang.ast.SideEffect.*;
public interface test{
	@Pure public int increase();
	@Local public int increaseL();
	@Fresh public Parent ParentX();
	@Fresh public Parent ParentX2();
}

// Parent!
public class Parent{
	public int St=0;
    public Parent p;
	private int open;
	
	@Pure public Parent(){
		
	}
	
    @Fresh public Parent(int x){
    open=x;
    p=new Parent();
    }
    
}


public class SubClass extends Parent implements test {
    @Fresh public SubClass(int x){
		super(x);
	}
    
    //Wrong!!!
    public Parent ParentX2(){
    	return this;
    }
    
    //Okey!!!
   @Pure public int increase(){
    	return St+1;
    }
    
    @Fresh public Parent ParentX(){
    	return new SubClass(0);
    }
    
    
    //Okey!!!
    @Local public int increaseL(){
    	return St++;
    }
}
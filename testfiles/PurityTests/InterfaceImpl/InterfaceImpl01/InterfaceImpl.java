import lang.ast.SideEffect.*;
public interface test{
	@Pure public int increase();
	@Local public int increaseL();
	@Fresh public Parent ParentX();
}

// Parent!
public class Parent{
	public int St=0;
    public Parent p;
	private int open;
    public Parent(int x){
    open=x;
    p=new Parent();
    }
    @Pure @Fresh public Parent(){
    	
    }
    
}


public class SubClass extends Parent implements test {
    public SubClass(int x){
		super(x);
	}
    
    @Fresh public SubClass(){
    	super();
    }
    //Wrong!!!
    public int increase(){
    	return St++;
    }
    
    @Fresh public Parent ParentX(){
    	return new SubClass();
    }
    
    //Okey!!!
    @Local public int increaseL(){
    	return St++;
    }
}
import lang.ast.SideEffect.*;
public class CacheExample {
    @Secret(group="X") private int a=0;
    @Secret(group="X") private boolean b=false;
    private int[] arrayAccess;
    private int c;
    private int d;
    
    // Using the Secret Group Scheme
    @Pure(group="X") public int delta(){
       if (b)
       return a;
       a=4;
       b=true; 
       deltaB();
       return a;
    }
        
    // Using the Secret Group Scheme
    @Pure(group="X") public int deltaB(){
       if (b)
       return c;
       c=4; // Normally Wrong by detecting cache Behaviour its not
       b=true; 
       return c;
    }
    
    @Pure(group="X") public int deltaB2(){
        b=true;  
        d=4; // This is wrong
        return d;
     }
        // Using the Secret Group Scheme
    @Pure public int deltaC(){
       if (c==0)
       return c;
       c=4;
       return c;
    }
    
    @Pure public void arrayMod(){
    arrayAccess[2]=4;
    }
    
    @Pure public void localAlias(){
    int[] p=arrayAccess;
    p[2]=4;
    }
    @Pure public void localFresh(){
    int[] p=new int[]{2,3,5};
    p[2]=4;
    }
    @Pure public void callerofImpure(){
    modf()[1]=3; // Shuld Fail Need to Fix
    }
    
    @Pure public void callerofUnannotatedFreshContained(){
    freshC()[1]=3; //Okey
    }
    
    @Pure public void manipulationViaGetter(){
    getterS()[1]=3;
    }
    
    @Pure public int[] modf(){
    return arrayAccess;
    }
    
    @Fresh public int[] freshC(){
    return new int[]{3,5,6};
    }
    
    int[] store=new int[]{3,5,6};
    @Fresh public int[] getterS(){
    return store; // fail (not a new creation!!!)
    }
    
}
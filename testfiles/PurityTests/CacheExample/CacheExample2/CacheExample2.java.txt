testfiles\PurityTests\CacheExample\CacheExample2\CacheExample.java
Field is unannotated.

3:the secret field a is not assign anywhere in it's class or subclasses whichis the only place it can be used.
Remove or use the variable somewhere in the class.

in testfiles\PurityTests\CacheExample\CacheExample2\CacheExample.java:CacheExample

Method is annotated with group "X",Pure.
For Java method not part of attribute calculation

Purity problem in category:SECRETREAD
11:this.b reads through secret without the secret group "", have "X" at b

in testfiles\PurityTests\CacheExample\CacheExample2\CacheExample.java:CacheExample

Purity problem in category:SECRETREAD
12:this.a reads through secret without the secret group "", have "X" at a

in testfiles\PurityTests\CacheExample\CacheExample2\CacheExample.java:CacheExample

Purity problem in category:SECRETREAD
13:this.a write secret state without the secret group "", have "X" at a

in testfiles\PurityTests\CacheExample\CacheExample2\CacheExample.java:CacheExample

Purity problem in category:SECRETREAD
14:this.b write secret state without the secret group "", have "X" at b

in testfiles\PurityTests\CacheExample\CacheExample2\CacheExample.java:CacheExample

Purity problem in category:SECRETREAD
15:this.a reads through secret without the secret group "", have "X" at a

in testfiles\PurityTests\CacheExample\CacheExample2\CacheExample.java:CacheExample

Method is annotated with group "X",Pure.
Called by CacheExample.delta() on line 13


Purity problem in category:VARWRITE
22:c = 4; violates the annotations at c
22:c:write effects the this object but this object is not being constructed or locked and thus the method is impure

in testfiles\PurityTests\CacheExample\CacheExample2\CacheExample.java:CacheExample

Purity problem in category:SECRETREAD
20:this.b reads through secret without the secret group "", have "X" at b

in testfiles\PurityTests\CacheExample\CacheExample2\CacheExample.java:CacheExample

Purity problem in category:SECRETREAD
23:this.b write secret state without the secret group "", have "X" at b

in testfiles\PurityTests\CacheExample\CacheExample2\CacheExample.java:CacheExample

Method is annotated with group "X",Pure.
For Java method not part of attribute calculation

Purity problem in category:VARWRITE
29:d = 4; violates the annotations at d
29:d:write effects the this object but this object is not being constructed or locked and thus the method is impure

in testfiles\PurityTests\CacheExample\CacheExample2\CacheExample.java:CacheExample

Purity problem in category:SECRETREAD
28:this.b write secret state without the secret group "", have "X" at b

in testfiles\PurityTests\CacheExample\CacheExample2\CacheExample.java:CacheExample

Method is annotated Pure.
For Java method not part of attribute calculation

Purity problem in category:VARWRITE
34:arrayAccess[2] = "Wrong"; violates the annotations at [2]
34:arrayAccess.[2]:write effects the this object but this object is not being constructed or locked and thus the method is impure

in testfiles\PurityTests\CacheExample\CacheExample2\CacheExample.java:CacheExample

Method is annotated Pure.
For Java method not part of attribute calculation

Purity problem in category:VARWRITE
39:p[2] = "Wrong"; violates the annotations at [2]
39:38_p.[2]:38_p is not fresh because 
->the variable p is initizised to arrayAccess that's not fresh.
It's only of freshness Maybe
->this.arrayAccess
->the variable p is initizised to arrayAccess that's not fresh.
It's only of freshness Maybe.

in testfiles\PurityTests\CacheExample\CacheExample2\CacheExample.java:CacheExample

Method is annotated Pure.
For Java method not part of attribute calculation

Purity problem in category:VARWRITE
46:modf()[1] = "Wrong"; violates the annotations at [1]
46:CacheExample.modf().[1]:write to a field in a array that only maybe fresh

in testfiles\PurityTests\CacheExample\CacheExample2\CacheExample.java:CacheExample

Method is annotated Pure.
For Java method not part of attribute calculation

Purity problem in category:METHODCALL
50:freshC()[1] = 3; causes side effects.
For method call not externally specified.The method call CacheExample.freshC() is impure.

in testfiles\PurityTests\CacheExample\CacheExample2\CacheExample.java:CacheExample

15 warnings generated

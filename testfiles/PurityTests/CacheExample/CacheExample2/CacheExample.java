import lang.ast.SideEffect.*;
public class CacheExample {
    @Secret(group="X") private int[] a;
    @Secret(group="X") private boolean b=false;
    private String[] arrayAccess;
    private int c;
    private int[] d;
    
    // Using the Secret Group Scheme
    @Pure(group="X") public int[] delta(){
       if (b)
       return a;
       a=computeA();
       b=true; 
       return a;
    }
        
    // Using the Secret Group Scheme
    @Pure(group="X") public int[] computeA(){
       if (b)
       return c;
       c=4; // Normally Wrong by detecting cache Behaviour its not
       b=true; 
       return c;
    }
    
    @Pure(group="X") public int[] computeB(){
        b=true;  
        d=4; // This is wrong
        return d;
     }
    
    @Pure public void arrayMod(){
    arrayAccess[2]="Wrong";
    }
    
    @Pure public void localAlias(){
    String[] p=arrayAccess;
    p[2]="Wrong";
    }
    @Pure public void localFresh(){
    String[] p=new String[]{"0","1","2"};
    p[2]="Correct!!";
    }
    @Pure public void callerofImpure(){
    modf()[1]="Wrong"; // Error
    }
    
    @Pure public void callerofUnannotatedFreshContained(){
    freshC()[1]=3; //Okey
    }
    
    @Pure public String[] modf(){
    return arrayAccess;
    }
    
    @Fresh public int[] freshA(){
    	return new int[]{0,1,2};
    }
    
    @Fresh public String[] freshC(){
    return new String[]{"0","1","2"};
    }
    
    
}
testfiles\PurityTests\ConditionalFresh\ConditionalFresh.java
Method is annotated Pure.
For Java method not part of attribute calculation

Purity problem in category:METHODCALL
19:return statT.b ? new t() : new t(); causes side effects.
For method call not externally specified.The method call t.t() is impure.

in testfiles\PurityTests\ConditionalFresh\ConditionalFresh.java:t

Purity problem in category:METHODCALL
19:return statT.b ? new t() : new t(); causes side effects.
For method call not externally specified.The method call t.t() is impure.

in testfiles\PurityTests\ConditionalFresh\ConditionalFresh.java:t

Method is annotated Pure.
For Java method not part of attribute calculation

Purity problem in category:METHODCALL
24:return statT.b ? unFresh() : new t(); causes side effects.
For method call not externally specified.The method call t.unFresh() is impure.

in testfiles\PurityTests\ConditionalFresh\ConditionalFresh.java:t

Purity problem in category:METHODCALL
24:return statT.b ? unFresh() : new t(); causes side effects.
For method call not externally specified.The method call t.t() is impure.

in testfiles\PurityTests\ConditionalFresh\ConditionalFresh.java:t

Method is annotated Pure.
For Java method not part of attribute calculation

Purity problem in category:METHODCALL
29:return statT.b ? new t() : unFresh(); causes side effects.
For method call not externally specified.The method call t.t() is impure.

in testfiles\PurityTests\ConditionalFresh\ConditionalFresh.java:t

Purity problem in category:METHODCALL
29:return statT.b ? new t() : unFresh(); causes side effects.
For method call not externally specified.The method call t.unFresh() is impure.

in testfiles\PurityTests\ConditionalFresh\ConditionalFresh.java:t

Method is annotated Pure.
For Java method not part of attribute calculation

Purity problem in category:METHODCALL
34:return unFresh() != null ? new t() : new t(); causes side effects.
For method call not externally specified.The method call t.unFresh() is impure.

in testfiles\PurityTests\ConditionalFresh\ConditionalFresh.java:t

Purity problem in category:METHODCALL
34:return unFresh() != null ? new t() : new t(); causes side effects.
For method call not externally specified.The method call t.t() is impure.

in testfiles\PurityTests\ConditionalFresh\ConditionalFresh.java:t

Purity problem in category:METHODCALL
34:return unFresh() != null ? new t() : new t(); causes side effects.
For method call not externally specified.The method call t.t() is impure.

in testfiles\PurityTests\ConditionalFresh\ConditionalFresh.java:t

Method is annotated Fresh.
For Java method not part of attribute calculation

Purity problem in category:FRESHRETURN
43:return statT.b ? unPFresh() : new t();of Freshness NonFresh can't be returned from a @Fresh method because is not fresh because 
->The method call t.unPFresh() is not annotated FreshIf,Fresh,Pure or Ignore so can't be assumed to return a newly allocated object.

in testfiles\PurityTests\ConditionalFresh\ConditionalFresh.java:t

Method is annotated Fresh.
For Java method not part of attribute calculation

Purity problem in category:FRESHRETURN
48:return statT.b ? new t() : unPFresh();of Freshness NonFresh can't be returned from a @Fresh method because is not fresh because 
->The method call t.unPFresh() is not annotated FreshIf,Fresh,Pure or Ignore so can't be assumed to return a newly allocated object.

in testfiles\PurityTests\ConditionalFresh\ConditionalFresh.java:t

Method is annotated Pure.
For Java method not part of attribute calculation

Purity problem in category:METHODCALL
56:return statT.b ? unPFresh() : new t(); causes side effects.
For method call not externally specified.The method call t.t() is impure.

in testfiles\PurityTests\ConditionalFresh\ConditionalFresh.java:t

Method is annotated Pure.
For Java method not part of attribute calculation

Purity problem in category:METHODCALL
60:return statT.b ? new t() : unPFresh(); causes side effects.
For method call not externally specified.The method call t.t() is impure.

in testfiles\PurityTests\ConditionalFresh\ConditionalFresh.java:t

Method is annotated Pure.
For Java method not part of attribute calculation

Purity problem in category:METHODCALL
64:return unPFresh() != null ? new t() : new t(); causes side effects.
For method call not externally specified.The method call t.t() is impure.

in testfiles\PurityTests\ConditionalFresh\ConditionalFresh.java:t

Purity problem in category:METHODCALL
64:return unPFresh() != null ? new t() : new t(); causes side effects.
For method call not externally specified.The method call t.t() is impure.

in testfiles\PurityTests\ConditionalFresh\ConditionalFresh.java:t

15 warnings generated

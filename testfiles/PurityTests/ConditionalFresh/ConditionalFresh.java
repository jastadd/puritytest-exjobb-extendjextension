import lang.ast.SideEffect.*;
public class t{
	public boolean b;
	public static t statT;  
  @Fresh public t(){
	  
  }
  
  public t unFresh(){
	  return t;
  }
  
  @Pure public t unPFresh(){
	  return t;
  }
  
  
  @Pure public t test(){
	  return statT.b ? new t() : new t(); 
  }
  
  //Error
  @Pure public t test2(){
	  return statT.b ? unFresh() : new t(); 
  }
  
  //Error
  @Pure public t test3(){
	  return statT.b ? new t() :  unFresh(); 
  }
  
  //Error
  @Pure public t test3(){
	  return unFresh()!=null ? new t() :  new t(); 
  }
  
  @Fresh public t test(){
	  return statT.b ? new t() : new t(); 
  }
  
  //Error
  @Fresh public t test2(){
	  return statT.b ? unPFresh() : new t(); 
  }
  
  //Error
  @Fresh public t test3(){
	  return statT.b ? new t() :  unPFresh(); 
  }
  
  @Fresh public t test3(){
	  return unPFresh()!=null ? new t() :  new t(); 
  }
  
  @Pure public t test2(){
	  return statT.b ? unPFresh() : new t(); 
  }
  
  @Pure public t test3(){
	  return statT.b ? new t() :  unPFresh(); 
  }
  
  @Pure public t test3(){
	  return unPFresh()!=null ? new t() :  new t(); 
  }
  
}
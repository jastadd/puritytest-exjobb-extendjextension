import lang.ast.SideEffect.*;
import lang.ast.*;
public class X{
   Int k;
   X link;
   @Local public X obtainselected(){
	   X a= new X(); a.k=10;
	   X b= new X(); b.k=3;
	   link = new X();
	   return a.selectX(link).selectX(b);
   }
   
   @FreshIf public X selectX(@FreshIf X a){	
	   // complex selection process!!!
	   return a.k<this.k ? a : this;
   }
}

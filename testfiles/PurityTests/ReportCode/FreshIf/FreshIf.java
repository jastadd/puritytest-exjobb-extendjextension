import java.util.Set;
import java.util.Map;
import java.util.TreeMap;
import java.util.HashSet;
import lang.ast.SideEffect.*;
import lang.ast.*;
public class Test{
   int y = 9;
	@FreshIf @Local public Test freshIf(){
		return this;
	}
	@Fresh public Test freshIf2(){
		return this;
	}
	@Local public Test freshIf3(){
		return this;
	}
	
	@Fresh public Map<Test, Set<Test>> freshIf3(){ 
		Map<Test, Set<Test>> x= new TreeMap<Test,Set<Test>>();
		x.put(this,new HashSet<Test>());
		modSet(x.get(this)); 
		// Can only work under
		// @Local Map<...,...>.put(@FreshIf x)
		// @FreshIf Map<...,...>.get(this);
		return x;
	}
	
	@Local public void modSet(@Local Set<Test> t){ 
		t.add(this);
	}
}

testfiles\PurityTests\ReportCode\FreshIf\FreshIf.java
Method is annotated Fresh.
For Java method not part of attribute calculation

Purity problem in category:FRESHRETURN
13:return this;of Freshness LOCAL can't be returned from a @Fresh method because is not fresh because 
->this is unfresh because not in a constructor.

in testfiles\PurityTests\ReportCode\FreshIf\FreshIf.java:Test

Method is annotated Pure.
For Java method not part of attribute calculation

Purity problem in category:METHODCALL
30:t.add(this); causes side effects.
For method call not externally specified.The method call java.util.Set.add(Test) called for t are local but the caller is not fresh or in a modifiable locality.
The caller is not fresh because 
->parameter t thats not assumed fresh on entry but only Maybe
->parameter t thats not assumed fresh on entry but only Maybe.

in testfiles\PurityTests\ReportCode\FreshIf\FreshIf.java:Test

2 warnings generated

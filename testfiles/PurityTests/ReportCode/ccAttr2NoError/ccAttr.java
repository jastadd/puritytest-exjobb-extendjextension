import lang.ast.SideEffect.*;
import lang.ast.*;
public @Entity class A{
    /** @apilevel internal */
  @Secret(group="_ASTNode")
  protected boolean circAttr_computed = false;
  @Secret(group="_ASTNode") protected boolean circAttr_visited = false;
  @Secret(group="_ASTNode") protected String circAttr_value;
  @Secret(group="_ASTNode") protected boolean circAttr_initialized = false;
  @Secret(group="_ASTNode") protected ASTState.Cycle circAttr_cycle = null;

  @Pure private A circAttr_compute() {
	 return this;
  }
  
  // Wrong!!
  @Fresh private A initial() {
	return new A();
  }
  @SideEffect.Ignore private void circAttr_String_reset() {}
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, 
		  isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Test", declaredAt="X.jrag:3")
  @SideEffect.Pure(group="_ASTNode") public A circAttr() {
		if (circAttr_computed) {
			return circAttr_value;
		}
		ASTState state = state();
		if (!circAttr_initialized) {
			circAttr_initialized = true;
			circAttr_value = inital();
		}
		if (!state.inCircle() || state.calledByLazyAttribute()) {
			state.enterCircle();
			do {
				circAttr_cycle = state.nextCycle();
				boolean new_circAttr_value = circAttr_compute();
				if (new_circAttr_value != circAttr_value) {
					state.setChangeInCycle();
				}
				circAttr_value = new_circAttr_value;
			} while (state.testAndClearChangeInCycle());
			circAttr_computed = true;
			state.startLastCycle();
			boolean $tmp = a();

			state.leaveCircle();
		} else if (circAttr_cycle != state.cycle()) {
			circAttr_cycle = state.cycle();
			if (state.lastCycle()) {
				circAttr_computed = true;
				A new_circAttr_value = circAttr_compute();
				return new_circAttr_value;
			}
			boolean new_circAttr_value = circAttr();
			if (new_circAttr_value != circAttr_value) {
				state.setChangeInCycle();
			}
			circAttr_value = new_circAttr_value;
		} else {
		}
		return circAttr_value;
	}
}

import lang.ast.SideEffect.*;
import lang.ast.*;
@SideEffect.Entity public class A extends ASTNode{
		
	}
@SideEffect.Entity public class ASTNode{
	/** @apilevel internal */
	@SideEffect.Secret(group="_ASTNode") 
	protected boolean getA_visited = false;
	@SideEffect.Secret(group="_ASTNode")
	protected boolean getA_computed = false;
	@SideEffect.Secret(group="_ASTNode") 
	protected A getA_value=null;
	
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN,
			isNTA=true)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="X.jrag:2")
	@SideEffect.Pure(group="_ASTNode") public A getA() {
		A getA_value = getA_compute();
		setChild(getA_value, getAChildPosition());
		A node = (A) this.getChild(getAChildPosition());
		return node;
	}
	
	@SideEffect.Fresh private A getA_compute() {
		// Pure effects
		A node = new A(new B(ID), new B(ID));
		return node;
	}
	
	@SideEffect.Ignore @SideEffect.Local public void setChild(ASTNode node, int i) {
		    if (children == null) {
		      children = new ASTNode[(i + 1 > 4 || !(this instanceof List)) ? i + 1 : 4];
		    } else if (i >= children.length) {
		      ASTNode c[] = new ASTNode[i << 1];
		      System.arraycopy(children, 0, c, 0, children.length);
		      children = c;
		    }
		    children[i] = node;
		    if (i >= numChildren) {
		      numChildren = i+1;
		    }
		    if (node != null) {
		      node.setParent(this);
		      node.childIndex = i;
		    }
		  }
	
	@SideEffect.Pure private int getAChildPosition() {
		return 1;
	}
	
	@SideEffect.Local protected ASTNode[] children;
	
	 @SideEffect.Local(group="_ASTNode") public void setParent(ASTNode node) {
		    parent = node;
		  }
	 
	 @SideEffect.Pure(group="_ASTNode") public final T getChildNoTransform(int i) {
		    if (children == null) {
		      return null;
		    }
		    T child = (T)children[i];
		    return child;
	 }
	 
	 @SideEffect.Pure public boolean mayHaveRewrite() {
		    return false;
		  }
	 
	 @SideEffect.Fresh(self=true) public T rewrittenNode(){
		 return null;
	 }
	
	@SideEffect.Pure public T getChild(int i) {
	    ASTNode node = this.getChildNoTransform(i);
	    if (node != null && node.mayHaveRewrite()) {
	      ASTNode rewritten = node.rewrittenNode();
	      if (rewritten != node) {
	        rewritten.setParent(this);
	        node = rewritten;
	      }
	    }
	    return (T) node;
	  }
}

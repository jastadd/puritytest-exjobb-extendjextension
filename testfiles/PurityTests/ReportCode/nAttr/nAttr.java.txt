testfiles\PurityTests\ReportCode\nAttr\nAttr.java
Method is annotated _ASTNode,,Fresh.
For Java method not part of attribute calculation

22:return node;of Freshness NonFresh can't be returned from a @Fresh method because the variable node is initizised to (A) this.getChild(getAChildPosition()) that's not fresh.
It's only of freshness NonFresh.->The method call ASTNode.getChild(int) is not annotated FreshIf,Fresh,Pure or Ignore so can't be assumed to return a newly allocated object.

in testfiles\PurityTests\ReportCode\nAttr\nAttr.java:ASTNode

1 warnings generated

import lang.ast.SideEffect.*;
import lang.ast.*;
import java.util.ArrayList;
public class A{ 
	@Secret(group="_ASTNode") protected java.util.Map<ASTNode, java.util.Set<ASTNode>> contributorMap_A_coll = null;
	@Secret(group="_ASTNode") protected boolean A_coll_visited = false;
	@Secret(group="_ASTNode") protected boolean A_coll_computed = false;
	@SideEffect.Secret(group="_ASTNode") protected ArrayList<String> A_coll_value;
	
	
	@Pure public String getID(){
		return "test";
	}
	
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.COLL)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="X.jrag:10")
	@Pure(group="_ASTNode") public ArrayList<String> coll() {
		ASTState state = state();
		if (A_coll_computed) {
			return A_coll_value;
		}
		if (A_coll_visited) {
			throw new RuntimeException("Circular definition of attribute A.set().");
		}
		A_coll_visited = true;
		state().enterLazyAttribute();
		A_coll_value = coll_compute();
		A_coll_computed = true;
		state().leaveLazyAttribute();
		A_coll_visited = false;
		return A_coll_value;
	}

	@Fresh(group="_ASTNode") private ArrayList<String> coll_compute() {
		ASTNode node = this;
		while (node != null && !(node instanceof A)) {
			node = node.getParent();
		}
		A root = (A) node;
		root.survey_A_coll();
		ArrayList<String> _computedValue = new ArrayList<String>();
		if (root.contributorMap_A_coll.containsKey(this)) {
		for (ASTNode contributor : root.contributorMap_A_coll.get(this)) {
			contributor.contributeTo_A_coll(_computedValue);
			}
		}
		return _computedValue;
	}
	// Helper method
	@Pure(group="_ASTNode") protected void survey_A_coll() {
	    if (contributorMap_A_coll == null) {
	      contributorMap_A_coll = new java.util.IdentityHashMap<ASTNode, java.util.Set<ASTNode>>();
	      collect_contributors_A_coll(this, contributorMap_A_coll);
	    }
	  }
	
	@Pure protected void collect_contributers_A_coll(A _root, @Local HashMap<ASTNode, java.util.Set<ASTNode>> _map){
		// Collect contribution
		if (condition()){
			A target = (A) (a());
			java.util.Set<ASTNode> contributors = _map.get(target);
			if (contributors == null) {
			contributors = new java.util.LinkedHashSet<ASTNode>();
			_map.put((ASTNode) target, contributors);
			}
		}
		super.collect_contributors_A_set(_root, _map);
	}

	@Pure protected void contributeTo_A_coll(@Local ArrayList<String> collection) {
	    super.contributeTo_A_coll(collection);
	    if (condition()) 
	      collection.add(getID());
	  }	
}


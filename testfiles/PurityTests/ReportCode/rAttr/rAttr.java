package a;

import lang.ast.SideEffect.*;
import lang.ast.*;
public class ASTNode{
	@SideEffect.Local public ASTNode rewriteTo() {
		return this;
	}
}

public class A extends ASTNode{
	@Local ASTNode[] children;
	@SideEffect.Local public ASTNode rewriteTo() {
		if (RewriteCondition0()) 
			return rewriteRule0();
		return super.rewriteTo();
	}
	// Rewrites should allow itself to be rewriteable
	@Fresh(self=true) private D rewriteRule0() {
		// Create new C node
		D newD= new D();
		newD.children[1]=this.children[2];
		newD.children[0]=this.children[1];
		newD.setB(getB());
		return newD;
	}
	/** @apilevel internal */
	@Pure public boolean canRewrite() {
		if (RewriteCondition0()) 
			return true;
		return false;
	}
	
	@Pure public boolean RewriteCondition0(){
		return true;
	}
	
	@Pure public B getB() {
		return null;
	}
	
	
	@Pure public boolean mayHaveRewrite() {
		return true;
	}
	
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, 
			isCircular=true, isNTA=true)
	@ASTNodeAnnotation.Source(aspect="", declaredAt=":0")
	@SideEffect.Local(group="_ASTNode") public ASTNode rewrittenNode() {
		// ... circular NTA more or less ...
	}
	// Circular NTA fields ....
}

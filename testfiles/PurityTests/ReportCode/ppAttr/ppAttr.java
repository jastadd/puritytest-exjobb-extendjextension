package test;
import lang.ast.SideEffect.*;
import lang.ast.*;
public class A{
	@Secret(group="_ASTNode")
	protected java.util.Set synAttr_String_visited=null;
	@Secret(group="_ASTNode")
	protected java.util.Map synAttr_String_values;
	
	@Ignore private void synAttr_String_reset() {
		synAttr_String_values = null;
		synAttr_String_visited = null;}
	
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="x.jrag:2")
	@Pure(group="_ASTNode") public boolean synAttr(String s) {
	Object _parameters = s;
	if (synAttr_String_values == null) 
		synAttr_String_values = new java.util.HashMap(4);
	if (synAttr_String_values.containsKey(_parameters)) 
		return (Boolean) synAttr_String_values.get(_parameters);
	boolean synAttr_String_value = s.equals("");
		synAttr_String_values.put(_parameters, synAttr_String_value);
		return synAttr_String_value;
	}
	
	@Pure public boolean synAttr_compute(String s) {
		// Something pure
		return true;}
}

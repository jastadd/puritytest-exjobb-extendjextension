import lang.ast.SideEffect.*;
public class A{
	// Constructing AST
	@SideEffect.Secret(group="_ASTNode") private int childIndex = -1;
	@SideEffect.Secret(group="_ASTNode") protected ASTNode parent;
	// Local to control rewrites authority and modifiablity
	@SideEffect.Local protected ASTNode[] children;
	@SideEffect.Ignore public void init$Children() {
		children = new ASTNode[1];
		setChild(new List(), 0);
	}
	// Force all childs to be newly created nodes to assure
	@SideEffect.Local public void addChild(@Fresh T node) {
		setChild(node, getNumChildNoTransform());
	}
	
	@SideEffect.Ignore public void setChild(@Fresh ASTNode node, int i) {
		children[i]=node;
	}
}
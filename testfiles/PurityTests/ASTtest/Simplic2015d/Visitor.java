package lang.ast;
import java.util.*;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
/**
 * @ast interface
 * @aspect Visitor
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\puritytest-extension-tester\\testfiles\\source\\fastTests\\Simplic2015\\Visitor.jrag:6
 */
public interface Visitor {

		 
		public Object visit(List node, Object data);

		 
		public Object visit(Program node, Object data);

		 
		public Object visit(Function node, Object data);

		 
		public Object visit(While node, Object data);

		 
		public Object visit(If node, Object data);

		 
		public Object visit(Return node, Object data);

		 
		public Object visit(Decl node, Object data);

		 
		public Object visit(Call node, Object data);

		 
		public Object visit(Assign node, Object data);

		 
		public Object visit(Block node, Object data);

		 
		public Object visit(Mul node, Object data);

		 
		public Object visit(Add node, Object data);

		 
		public Object visit(Sub node, Object data);

		 
		public Object visit(Div node, Object data);

		 
		public Object visit(Mod node, Object data);

		 
		public Object visit(Eq node, Object data);

		 
		public Object visit(Neq node, Object data);

		 
		public Object visit(Leq node, Object data);

		 
		public Object visit(Less node, Object data);

		 
		public Object visit(Geq node, Object data);

		 
		public Object visit(Great node, Object data);

		 
		public Object visit(Numeral node, Object data);

		 
		public Object visit(IDExpr node, Object data);

		 
		public Object visit(FunctionExpr node, Object data);

		 
		public Object visit(Opt node, Object data);

		 
		public Object visit(FunctionDecl node, Object data);
}

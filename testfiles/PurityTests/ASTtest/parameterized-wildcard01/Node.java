/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
import java.util.Collection;
/**
 * @ast node
 * @declaredat tests\\syn\\parameterized-wildcard01\\Test.ast:1
 * @production Node : {@link ASTNode};

 */
public class Node extends ASTNode<ASTNode> implements Cloneable {
	/**
	 * @declaredat ASTNode:1
	 */
	public Node() {
		super();
	}
	/**
	 * Initializes the child array to the correct size.
	 * Initializes List and Opt nta children.
	 * @apilevel internal
	 * @ast method
	 * @declaredat ASTNode:10
	 */
	public void init$Children() {
	}
	/** @apilevel low-level 
	 * @declaredat ASTNode:13
	 */
	protected int numChildren() {
		return 0;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:17
	 */
	public void flushAttrCache() {
		super.flushAttrCache();
		wildcardAttribute_Collection____reset();
		wildcardAttribute2_Collection___extends_ASTNode__reset();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:23
	 */
	public void flushCollectionCache() {
		super.flushCollectionCache();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:27
	 */
	public Node clone() throws CloneNotSupportedException {
		Node node = (Node) super.clone();
		return node;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:32
	 */
	public Node copy() {
		try {
			Node node = (Node) clone();
			node.parent = null;
			if (children != null) {
				node.children = (ASTNode[]) children.clone();
			}
			return node;
		} catch (CloneNotSupportedException e) {
			throw new Error("Error: clone not supported for " + getClass().getName());
		}
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @deprecated Please use treeCopy or treeCopyNoTransform instead
	 * @declaredat ASTNode:51
	 */
	@Deprecated
	public Node fullCopy() {
		return treeCopyNoTransform();
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:61
	 */
	public Node treeCopyNoTransform() {
		Node tree = (Node) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) children[i];
				if (child != null) {
					child = child.treeCopyNoTransform();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The subtree of this node is traversed to trigger rewrites before copy.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:81
	 */
	public Node treeCopy() {
		Node tree = (Node) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) getChild(i);
				if (child != null) {
					child = child.treeCopy();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:95
	 */
	protected boolean is$Equal(ASTNode node) {
		return super.is$Equal(node);    
	}
/** @apilevel internal */
@SideEffect.Secret(group="wildcardAttribute_Collection___") protected java.util.Set wildcardAttribute_Collection____visited;
	/** @apilevel internal */
	private void wildcardAttribute_Collection____reset() {
		wildcardAttribute_Collection____values = null;
		wildcardAttribute_Collection____visited = null;
	}
	/** @apilevel internal */
	 @SideEffect.Secret(group="wildcardAttribute_Collection___") protected java.util.Map wildcardAttribute_Collection____values;

	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\parameterized-wildcard01\\Test.jrag:4
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\parameterized-wildcard01\\Test.jrag:4")
	@SideEffect.Pure(group="wildcardAttribute_Collection___") public Collection<?> wildcardAttribute(Collection<?> list) {
		Object _parameters = list;
		if (wildcardAttribute_Collection____visited == null) wildcardAttribute_Collection____visited = new java.util.HashSet(4);
		if (wildcardAttribute_Collection____values == null) wildcardAttribute_Collection____values = new java.util.HashMap(4);
		ASTState state = state();
		if (wildcardAttribute_Collection____values.containsKey(_parameters)) {
			return (Collection<?>) wildcardAttribute_Collection____values.get(_parameters);
		}
		if (wildcardAttribute_Collection____visited.contains(_parameters)) {
			throw new RuntimeException("Circular definition of attribute Node.wildcardAttribute(Collection___).");
		}
		wildcardAttribute_Collection____visited.add(_parameters);
		state().enterLazyAttribute();
		Collection<?> wildcardAttribute_Collection____value = wildcardAttribute_compute(list);
		wildcardAttribute_Collection____values.put(_parameters, wildcardAttribute_Collection____value);
		state().leaveLazyAttribute();
		wildcardAttribute_Collection____visited.remove(_parameters);
		return wildcardAttribute_Collection____value;
	}
	/** @apilevel internal */
	@SideEffect.Pure private Collection<?> wildcardAttribute_compute(Collection<?> list) {
			return list;
		}
/** @apilevel internal */
@SideEffect.Secret(group="wildcardAttribute2_Collection___extends_ASTNode_") protected java.util.Set wildcardAttribute2_Collection___extends_ASTNode__visited;
	/** @apilevel internal */
	private void wildcardAttribute2_Collection___extends_ASTNode__reset() {
		wildcardAttribute2_Collection___extends_ASTNode__values = null;
		wildcardAttribute2_Collection___extends_ASTNode__visited = null;
	}
	/** @apilevel internal */
	 @SideEffect.Secret(group="wildcardAttribute2_Collection___extends_ASTNode_") protected java.util.Map wildcardAttribute2_Collection___extends_ASTNode__values;

	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\parameterized-wildcard01\\Test.jrag:7
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\parameterized-wildcard01\\Test.jrag:7")
	@SideEffect.Pure(group="wildcardAttribute2_Collection___extends_ASTNode_") public boolean wildcardAttribute2(Collection<? extends ASTNode> list) {
		Object _parameters = list;
		if (wildcardAttribute2_Collection___extends_ASTNode__visited == null) wildcardAttribute2_Collection___extends_ASTNode__visited = new java.util.HashSet(4);
		if (wildcardAttribute2_Collection___extends_ASTNode__values == null) wildcardAttribute2_Collection___extends_ASTNode__values = new java.util.HashMap(4);
		ASTState state = state();
		if (wildcardAttribute2_Collection___extends_ASTNode__values.containsKey(_parameters)) {
			return (Boolean) wildcardAttribute2_Collection___extends_ASTNode__values.get(_parameters);
		}
		if (wildcardAttribute2_Collection___extends_ASTNode__visited.contains(_parameters)) {
			throw new RuntimeException("Circular definition of attribute Node.wildcardAttribute2(Collection___extends_ASTNode_).");
		}
		wildcardAttribute2_Collection___extends_ASTNode__visited.add(_parameters);
		state().enterLazyAttribute();
		boolean wildcardAttribute2_Collection___extends_ASTNode__value = wildcardAttribute2_compute(list);
		wildcardAttribute2_Collection___extends_ASTNode__values.put(_parameters, wildcardAttribute2_Collection___extends_ASTNode__value);
		state().leaveLazyAttribute();
		wildcardAttribute2_Collection___extends_ASTNode__visited.remove(_parameters);
		return wildcardAttribute2_Collection___extends_ASTNode__value;
	}
	/** @apilevel internal */
	@SideEffect.Pure private boolean wildcardAttribute2_compute(Collection<? extends ASTNode> list) {
			return true;
		}
}

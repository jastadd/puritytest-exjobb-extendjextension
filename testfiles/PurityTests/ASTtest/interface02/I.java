/**
 * @ast interface
 * @aspect Test
 * @declaredat tests\\syn\\interface02\\Test.jrag:2
 */
 interface I {
	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\interface02\\Test.jrag:5
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\interface02\\Test.jrag:5")
	@SideEffect.Pure(group="attr") public boolean attr();
}

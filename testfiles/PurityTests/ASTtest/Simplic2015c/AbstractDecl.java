/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package lang.ast;
import java.util.*;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
/**
 * @ast node
 * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Simplic.ast:10
 * @production AbstractDecl : {@link Stmt};

 */
public abstract class AbstractDecl extends Stmt implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public AbstractDecl() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:17
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:21
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:25
   */
  @SideEffect.Fresh public AbstractDecl clone() throws CloneNotSupportedException {
    AbstractDecl node = (AbstractDecl) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:36
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public abstract AbstractDecl fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:44
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract AbstractDecl treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:52
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract AbstractDecl treeCopy();
/** @apilevel internal */
@SideEffect.Secret(group="address") protected boolean address_visited = false;
  /**
   * @attribute syn
   * @aspect CodeGen
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:21
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CodeGen", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:21")
  @SideEffect.Pure(group="address") public String address() {
    if (address_visited) {
      throw new RuntimeException("Circular definition of attribute AbstractDecl.address().");
    }
    address_visited = true;
    String address_value = "";
    address_visited = false;
    return address_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="uniqueName") protected boolean uniqueName_visited = false;
  /**
   * @attribute syn
   * @aspect UniqueName
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\uniqueName.jrag:6
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="UniqueName", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\uniqueName.jrag:6")
  @SideEffect.Pure(group="uniqueName") public String uniqueName() {
    if (uniqueName_visited) {
      throw new RuntimeException("Circular definition of attribute AbstractDecl.uniqueName().");
    }
    uniqueName_visited = true;
    String uniqueName_value = "";
    uniqueName_visited = false;
    return uniqueName_value;
  }
}

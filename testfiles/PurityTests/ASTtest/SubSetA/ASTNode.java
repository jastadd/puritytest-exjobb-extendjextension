/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package lang.ast;
import java.util.*;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.lang.RuntimeException;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
/**
 * @ast node
 * @production ASTNode;

 */
public class UnknownDecl{
	
}

public class ASTNode<T extends ASTNode> extends beaver.Symbol implements Cloneable {
  /**
   * @aspect DumpTree
   * @declaredat D:\\backup Simplic\\backup Simplic\\src\\jastadd\\DumpTree.jrag:6
   */
  private String DUMP_TREE_INDENT = "  ";
  /**
   * @aspect DumpTree
   * @declaredat D:\\backup Simplic\\backup Simplic\\src\\jastadd\\DumpTree.jrag:8
   */
  public String dumpTree() {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		dumpTree(new PrintStream(bytes));
		return bytes.toString();
	}
  /**
   * @aspect DumpTree
   * @declaredat D:\\backup Simplic\\backup Simplic\\src\\jastadd\\DumpTree.jrag:14
   */
  public void dumpTree(PrintStream out) {
		dumpTree(out, 0);
		out.flush();
	}
  /**
   * @aspect DumpTree
   * @declaredat D:\\backup Simplic\\backup Simplic\\src\\jastadd\\DumpTree.jrag:19
   */
  public void dumpTree(PrintStream out, int indent) {
		for (int i = 0; i < indent; i++) {
			out.print(DUMP_TREE_INDENT);
		}
		out.println(getClass().getSimpleName());
		for (ASTNode child: astChildren()) {
			child.dumpTree(out, indent+1);
		}
	}
  /**
   * @aspect Errors
   * @declaredat D:\\backup Simplic\\backup Simplic\\src\\jastadd\\Errors.jrag:22
   */
  protected ErrorMessage error(String message) {
		return new ErrorMessage(message, getLine(getStart()));
	}
  /**
   * @aspect Visitor
   * @declaredat D:\\backup Simplic\\backup Simplic\\src\\jastadd\\Visitor.jrag:35
   */
  public Object accept(Visitor visitor, Object data) {
		throw new Error("Visitor: accept method not available for " + getClass().getName());
	}
  /**
   * @declaredat ASTNode:1
   */
  public ASTNode() {
    super();
    init$Children();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:11
   */
  @SideEffect.Pure @SideEffect.Ignore public void init$Children() {
  }
  /**
   * Cached child index. Child indices are assumed to never change (AST should
   * not change after construction).
   * @apilevel internal
   * @declaredat ASTNode:18
   */
  private int childIndex = -1;
  /** @apilevel low-level 
   * @declaredat ASTNode:21
   */
  @SideEffect.Ignore public int getIndexOfChild(ASTNode node) {
    if (node == null) {
      return -1;
    }
    if (node.childIndex >= 0) {
      return node.childIndex;
    }
    for (int i = 0; children != null && i < children.length; i++) {
      if (children[i] == node) {
        node.childIndex = i;
        return i;
      }
    }
    return -1;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  public static final boolean generatedWithCacheCycle = true;
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  public static final boolean generatedWithComponentCheck = false;
  /** @apilevel low-level 
   * @declaredat ASTNode:44
   */
  protected ASTNode parent;
  /** @apilevel low-level 
   * @declaredat ASTNode:47
   */
  protected ASTNode[] children;
  /** @apilevel internal 
   * @declaredat ASTNode:51
   */
  private static ASTState state = new ASTState();
  /** @apilevel internal 
   * @declaredat ASTNode:54
   */
  public final ASTState state() {
    return state;
  }
  /**
   * @return an iterator that can be used to iterate over the children of this node.
   * The iterator does not allow removing children.
   * @declaredat ASTNode:63
   */
  public java.util.Iterator<T> astChildIterator() {
    return new java.util.Iterator<T>() {
      private int index = 0;

      @Override
      public boolean hasNext() {
        return index < getNumChild();
      }

      @Override
      public T next() {
        return hasNext() ? (T) getChild(index++) : null;
      }

      @Override
      public void remove() {
        throw new UnsupportedOperationException();
      }
    };
  }
  /** @return an object that can be used to iterate over the children of this node 
   * @declaredat ASTNode:85
   */
  public Iterable<T> astChildren() {
    return new Iterable<T>() {
      @Override
      public java.util.Iterator<T> iterator() {
        return astChildIterator();
      }
    };
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:95
   */
  @SideEffect.Pure public T getChild(int i) {
    ASTNode child = getChildNoTransform(i);
    return (T) child;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:100
   */
  public void addChild(T node) {
    setChild(node, getNumChildNoTransform());
  }
  /**
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @apilevel low-level
   * @declaredat ASTNode:107
   */
  public final T getChildNoTransform(int i) {
    if (children == null) {
      return null;
    }
    T child = (T)children[i];
    return child;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:115
   */
  protected int numChildren;
  /** @apilevel low-level 
   * @declaredat ASTNode:118
   */
  @SideEffect.Pure protected int numChildren() {
    return numChildren;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:123
   */
  @SideEffect.Pure public int getNumChild() {
    return numChildren();
  }
  /**
   * Behaves like getNumChild, but does not invoke AST transformations (rewrites).
   * @apilevel low-level
   * @declaredat ASTNode:131
   */
  public final int getNumChildNoTransform() {
    return numChildren();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:135
   */
  public void setChild(ASTNode node, int i) {
    if (children == null) {
      children = new ASTNode[(i + 1 > 4 || !(this instanceof List)) ? i + 1 : 4];
    } else if (i >= children.length) {
      ASTNode c[] = new ASTNode[i << 1];
      System.arraycopy(children, 0, c, 0, children.length);
      children = c;
    }
    children[i] = node;
    if (i >= numChildren) {
      numChildren = i+1;
    }
    if (node != null) {
      node.setParent(this);
      node.childIndex = i;
    }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:153
   */
  public void insertChild(ASTNode node, int i) {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:178
   */
  public void removeChild(int i) {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:205
   */
  @SideEffect.Pure public ASTNode getParent() {
    return (ASTNode) parent;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:209
   */
  public void setParent(ASTNode node) {
    parent = node;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:321
   */
  public void flushTreeCache() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:332
   */
  public void flushCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:336
   */
  public void flushAttrAndCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:341
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:344
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:347
   */
  public ASTNode<T> clone() throws CloneNotSupportedException {
    ASTNode node = (ASTNode) super.clone();
    node.flushAttrAndCollectionCache();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:353
   */
  public ASTNode<T> copy() {
    try {
      ASTNode node = (ASTNode) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:372
   */
  @Deprecated
  public ASTNode<T> fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:382
   */
  public ASTNode<T> treeCopyNoTransform() {
    ASTNode tree = (ASTNode) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:402
   */
  public ASTNode<T> treeCopy() {
    ASTNode tree = (ASTNode) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Performs a full traversal of the tree using getChild to trigger rewrites
   * @apilevel low-level
   * @declaredat ASTNode:419
   */
  public void doFullTraversal() {
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).doFullTraversal();
    }
  }
  /** @apilevel internal 
   * @declaredat ASTNode:425
   */
  protected boolean is$Equal(ASTNode n1, ASTNode n2) {
    if (n1 == null && n2 == null) return true;
    if (n1 == null || n2 == null) return false;
    return n1.is$Equal(n2);
  }
  /** @apilevel internal 
   * @declaredat ASTNode:431
   */
  protected boolean is$Equal(ASTNode node) {
    if (getClass() != node.getClass()) {
      return false;
    }
    if (numChildren != node.numChildren) {
      return false;
    }
    for (int i = 0; i < numChildren; i++) {
      if (children[i] == null && node.children[i] != null) {
        return false;
      }
      if (!((ASTNode)children[i]).is$Equal(((ASTNode)node.children[i]))) {
        return false;
      }
    }
    return true;
  }
  /**
   * @aspect <NoAspect>
   * @declaredat D:\\backup Simplic\\backup Simplic\\src\\jastadd\\Errors.jrag:26
   */
    @SideEffect.Pure protected void collect_contributors_Program_errors(Program _root, java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).collect_contributors_Program_errors(_root, _map);
    }
  }
  @SideEffect.Pure protected void contributeTo_Program_errors(Set<ErrorMessage> collection) {
  }

/** @apilevel internal */
@SideEffect.Secret(group="isFunction") protected boolean isFunction_visited = false;
  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat D:\\backup Simplic\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:33
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="D:\\backup Simplic\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:33")
  @SideEffect.Pure(group="isFunction") public boolean isFunction() {
    if (isFunction_visited) {
      throw new RuntimeException("Circular definition of attribute ASTNode.isFunction().");
    }
    isFunction_visited = true;
    boolean isFunction_value = false;
    isFunction_visited = false;
    return isFunction_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="nbrParams") protected boolean nbrParams_visited = false;
  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat D:\\backup Simplic\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:38
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="D:\\backup Simplic\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:38")
  @SideEffect.Pure(group="nbrParams") public int nbrParams() {
    if (nbrParams_visited) {
      throw new RuntimeException("Circular definition of attribute ASTNode.nbrParams().");
    }
    nbrParams_visited = true;
    int nbrParams_value = 0;
    nbrParams_visited = false;
    return nbrParams_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="localLookup_String_int") protected java.util.Set localLookup_String_int_visited;
  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat D:\\backup Simplic\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:107
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="D:\\backup Simplic\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:107")
  @SideEffect.Pure(group="localLookup_String_int") public AbstractDecl localLookup(String name, int until) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(name);
    _parameters.add(until);
    if (localLookup_String_int_visited == null) localLookup_String_int_visited = new java.util.HashSet(4);
    if (localLookup_String_int_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute ASTNode.localLookup(String,int).");
    }
    localLookup_String_int_visited.add(_parameters);
    try {
    	return new UnknownDecl();	
    	}
    finally {
      localLookup_String_int_visited.remove(_parameters);
    }
  }
  /**
   * @attribute inh
   * @aspect CodeGen
   * @declaredat D:\\backup Simplic\\backup Simplic\\src\\jastadd\\CodeGen.jrag:321
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="CodeGen", declaredAt="D:\\backup Simplic\\backup Simplic\\src\\jastadd\\CodeGen.jrag:321")
  @SideEffect.Pure(group="prevNode") public ASTNode prevNode() {
    if (prevNode_visited) {
      throw new RuntimeException("Circular definition of attribute ASTNode.prevNode().");
    }
    prevNode_visited = true;
    ASTNode prevNode_value = getParent().Define_prevNode(this, null);
    prevNode_visited = false;
    return prevNode_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="prevNode") protected boolean prevNode_visited = false;
  /**
   * @attribute inh
   * @aspect Errors
   * @declaredat D:\\backup Simplic\\backup Simplic\\src\\jastadd\\Errors.jrag:28
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Errors", declaredAt="D:\\backup Simplic\\backup Simplic\\src\\jastadd\\Errors.jrag:28")
  @SideEffect.Pure(group="program") public Program program() {
    if (program_visited) {
      throw new RuntimeException("Circular definition of attribute ASTNode.program().");
    }
    program_visited = true;
    Program program_value = getParent().Define_program(this, null);
    program_visited = false;
    return program_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="program") protected boolean program_visited = false;
  /**
   * @declaredat D:\\backup Simplic\\backup Simplic\\src\\jastadd\\CodeGen.jrag:321
   * @apilevel internal
   */
 @SideEffect.Pure public ASTNode Define_prevNode(ASTNode _callerNode, ASTNode _childNode) {
    int i = this.getIndexOfChild(_callerNode);
    return prevNode();
  }
  protected boolean canDefine_prevNode(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
}

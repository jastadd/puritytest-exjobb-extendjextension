/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
/**
 * @ast node
 * @declaredat tests\\syn\\lazy_02p\\Test.ast:1
 * @production A : {@link ASTNode};

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
	/**
	 * @declaredat ASTNode:1
	 */
	public A() {
		super();
	}
	/**
	 * Initializes the child array to the correct size.
	 * Initializes List and Opt nta children.
	 * @apilevel internal
	 * @ast method
	 * @declaredat ASTNode:10
	 */
	public void init$Children() {
	}
	/** @apilevel low-level 
	 * @declaredat ASTNode:13
	 */
	protected int numChildren() {
		return 0;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:17
	 */
	public void flushAttrCache() {
		super.flushAttrCache();
		byteValue_reset();
		shortValue_reset();
		intValue_reset();
		charValue_reset();
		longValue_reset();
		floatValue_reset();
		doubleValue_reset();
		booleanValue_reset();
		stringValue_reset();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:30
	 */
	public void flushCollectionCache() {
		super.flushCollectionCache();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:34
	 */
	public A clone() throws CloneNotSupportedException {
		A node = (A) super.clone();
		return node;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:39
	 */
	public A copy() {
		try {
			A node = (A) clone();
			node.parent = null;
			if (children != null) {
				node.children = (ASTNode[]) children.clone();
			}
			return node;
		} catch (CloneNotSupportedException e) {
			throw new Error("Error: clone not supported for " + getClass().getName());
		}
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @deprecated Please use treeCopy or treeCopyNoTransform instead
	 * @declaredat ASTNode:58
	 */
	@Deprecated
	public A fullCopy() {
		return treeCopyNoTransform();
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:68
	 */
	public A treeCopyNoTransform() {
		A tree = (A) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) children[i];
				if (child != null) {
					child = child.treeCopyNoTransform();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The subtree of this node is traversed to trigger rewrites before copy.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:88
	 */
	public A treeCopy() {
		A tree = (A) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) getChild(i);
				if (child != null) {
					child = child.treeCopy();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:102
	 */
	protected boolean is$Equal(ASTNode node) {
		return super.is$Equal(node);    
	}
/** @apilevel internal */
@SideEffect.Secret(group="byteValue") protected boolean byteValue_visited = false;
	/** @apilevel internal */
	private void byteValue_reset() {
		byteValue_computed = false;
		byteValue_visited = false;
	}
	/** @apilevel internal */
	@SideEffect.Secret(group="byteValue") protected boolean byteValue_computed = false;

	/** @apilevel internal */
	@SideEffect.Secret(group="byteValue") protected byte byteValue_value;

	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\lazy_02p\\Test.jrag:2
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\lazy_02p\\Test.jrag:2")
	@SideEffect.Pure(group="byteValue") public byte byteValue() {
		ASTState state = state();
		if (byteValue_computed) {
			return byteValue_value;
		}
		if (byteValue_visited) {
			throw new RuntimeException("Circular definition of attribute A.byteValue().");
		}
		byteValue_visited = true;
		state().enterLazyAttribute();
		byteValue_value = 0;
		byteValue_computed = true;
		state().leaveLazyAttribute();
		byteValue_visited = false;
		return byteValue_value;
	}
/** @apilevel internal */
@SideEffect.Secret(group="shortValue") protected boolean shortValue_visited = false;
	/** @apilevel internal */
	private void shortValue_reset() {
		shortValue_computed = false;
		shortValue_visited = false;
	}
	/** @apilevel internal */
	@SideEffect.Secret(group="shortValue") protected boolean shortValue_computed = false;

	/** @apilevel internal */
	@SideEffect.Secret(group="shortValue") protected short shortValue_value;

	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\lazy_02p\\Test.jrag:3
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\lazy_02p\\Test.jrag:3")
	@SideEffect.Pure(group="shortValue") public short shortValue() {
		ASTState state = state();
		if (shortValue_computed) {
			return shortValue_value;
		}
		if (shortValue_visited) {
			throw new RuntimeException("Circular definition of attribute A.shortValue().");
		}
		shortValue_visited = true;
		state().enterLazyAttribute();
		shortValue_value = 1;
		shortValue_computed = true;
		state().leaveLazyAttribute();
		shortValue_visited = false;
		return shortValue_value;
	}
/** @apilevel internal */
@SideEffect.Secret(group="intValue") protected boolean intValue_visited = false;
	/** @apilevel internal */
	private void intValue_reset() {
		intValue_computed = false;
		intValue_visited = false;
	}
	/** @apilevel internal */
	@SideEffect.Secret(group="intValue") protected boolean intValue_computed = false;

	/** @apilevel internal */
	@SideEffect.Secret(group="intValue") protected int intValue_value;

	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\lazy_02p\\Test.jrag:4
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\lazy_02p\\Test.jrag:4")
	@SideEffect.Pure(group="intValue") public int intValue() {
		ASTState state = state();
		if (intValue_computed) {
			return intValue_value;
		}
		if (intValue_visited) {
			throw new RuntimeException("Circular definition of attribute A.intValue().");
		}
		intValue_visited = true;
		state().enterLazyAttribute();
		intValue_value = 2;
		intValue_computed = true;
		state().leaveLazyAttribute();
		intValue_visited = false;
		return intValue_value;
	}
/** @apilevel internal */
@SideEffect.Secret(group="charValue") protected boolean charValue_visited = false;
	/** @apilevel internal */
	private void charValue_reset() {
		charValue_computed = false;
		charValue_visited = false;
	}
	/** @apilevel internal */
	@SideEffect.Secret(group="charValue") protected boolean charValue_computed = false;

	/** @apilevel internal */
	@SideEffect.Secret(group="charValue") protected char charValue_value;

	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\lazy_02p\\Test.jrag:5
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\lazy_02p\\Test.jrag:5")
	@SideEffect.Pure(group="charValue") public char charValue() {
		ASTState state = state();
		if (charValue_computed) {
			return charValue_value;
		}
		if (charValue_visited) {
			throw new RuntimeException("Circular definition of attribute A.charValue().");
		}
		charValue_visited = true;
		state().enterLazyAttribute();
		charValue_value = 'a';
		charValue_computed = true;
		state().leaveLazyAttribute();
		charValue_visited = false;
		return charValue_value;
	}
/** @apilevel internal */
@SideEffect.Secret(group="longValue") protected boolean longValue_visited = false;
	/** @apilevel internal */
	private void longValue_reset() {
		longValue_computed = false;
		longValue_visited = false;
	}
	/** @apilevel internal */
	@SideEffect.Secret(group="longValue") protected boolean longValue_computed = false;

	/** @apilevel internal */
	@SideEffect.Secret(group="longValue") protected long longValue_value;

	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\lazy_02p\\Test.jrag:6
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\lazy_02p\\Test.jrag:6")
	@SideEffect.Pure(group="longValue") public long longValue() {
		ASTState state = state();
		if (longValue_computed) {
			return longValue_value;
		}
		if (longValue_visited) {
			throw new RuntimeException("Circular definition of attribute A.longValue().");
		}
		longValue_visited = true;
		state().enterLazyAttribute();
		longValue_value = 3L;
		longValue_computed = true;
		state().leaveLazyAttribute();
		longValue_visited = false;
		return longValue_value;
	}
/** @apilevel internal */
@SideEffect.Secret(group="floatValue") protected boolean floatValue_visited = false;
	/** @apilevel internal */
	private void floatValue_reset() {
		floatValue_computed = false;
		floatValue_visited = false;
	}
	/** @apilevel internal */
	@SideEffect.Secret(group="floatValue") protected boolean floatValue_computed = false;

	/** @apilevel internal */
	@SideEffect.Secret(group="floatValue") protected float floatValue_value;

	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\lazy_02p\\Test.jrag:7
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\lazy_02p\\Test.jrag:7")
	@SideEffect.Pure(group="floatValue") public float floatValue() {
		ASTState state = state();
		if (floatValue_computed) {
			return floatValue_value;
		}
		if (floatValue_visited) {
			throw new RuntimeException("Circular definition of attribute A.floatValue().");
		}
		floatValue_visited = true;
		state().enterLazyAttribute();
		floatValue_value = 0.5f;
		floatValue_computed = true;
		state().leaveLazyAttribute();
		floatValue_visited = false;
		return floatValue_value;
	}
/** @apilevel internal */
@SideEffect.Secret(group="doubleValue") protected boolean doubleValue_visited = false;
	/** @apilevel internal */
	private void doubleValue_reset() {
		doubleValue_computed = false;
		doubleValue_visited = false;
	}
	/** @apilevel internal */
	@SideEffect.Secret(group="doubleValue") protected boolean doubleValue_computed = false;

	/** @apilevel internal */
	@SideEffect.Secret(group="doubleValue") protected double doubleValue_value;

	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\lazy_02p\\Test.jrag:8
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\lazy_02p\\Test.jrag:8")
	@SideEffect.Pure(group="doubleValue") public double doubleValue() {
		ASTState state = state();
		if (doubleValue_computed) {
			return doubleValue_value;
		}
		if (doubleValue_visited) {
			throw new RuntimeException("Circular definition of attribute A.doubleValue().");
		}
		doubleValue_visited = true;
		state().enterLazyAttribute();
		doubleValue_value = 0.6d;
		doubleValue_computed = true;
		state().leaveLazyAttribute();
		doubleValue_visited = false;
		return doubleValue_value;
	}
/** @apilevel internal */
@SideEffect.Secret(group="booleanValue") protected boolean booleanValue_visited = false;
	/** @apilevel internal */
	private void booleanValue_reset() {
		booleanValue_computed = false;
		booleanValue_visited = false;
	}
	/** @apilevel internal */
	@SideEffect.Secret(group="booleanValue") protected boolean booleanValue_computed = false;

	/** @apilevel internal */
	@SideEffect.Secret(group="booleanValue") protected boolean booleanValue_value;

	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\lazy_02p\\Test.jrag:9
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\lazy_02p\\Test.jrag:9")
	@SideEffect.Pure(group="booleanValue") public boolean booleanValue() {
		ASTState state = state();
		if (booleanValue_computed) {
			return booleanValue_value;
		}
		if (booleanValue_visited) {
			throw new RuntimeException("Circular definition of attribute A.booleanValue().");
		}
		booleanValue_visited = true;
		state().enterLazyAttribute();
		booleanValue_value = true;
		booleanValue_computed = true;
		state().leaveLazyAttribute();
		booleanValue_visited = false;
		return booleanValue_value;
	}
/** @apilevel internal */
@SideEffect.Secret(group="stringValue") protected boolean stringValue_visited = false;
	/** @apilevel internal */
	private void stringValue_reset() {
		stringValue_computed = false;
		
		stringValue_value = null;
		stringValue_visited = false;
	}
	/** @apilevel internal */
	@SideEffect.Secret(group="stringValue") protected boolean stringValue_computed = false;

	/** @apilevel internal */
	@SideEffect.Secret(group="stringValue") protected String stringValue_value;

	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\lazy_02p\\Test.jrag:10
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\lazy_02p\\Test.jrag:10")
	@SideEffect.Pure(group="stringValue") public String stringValue() {
		ASTState state = state();
		if (stringValue_computed) {
			return stringValue_value;
		}
		if (stringValue_visited) {
			throw new RuntimeException("Circular definition of attribute A.stringValue().");
		}
		stringValue_visited = true;
		state().enterLazyAttribute();
		stringValue_value = "str";
		stringValue_computed = true;
		state().leaveLazyAttribute();
		stringValue_visited = false;
		return stringValue_value;
	}
}

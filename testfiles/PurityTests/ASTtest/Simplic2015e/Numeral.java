/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.3 */
package lang.ast;
import java.util.*;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
/**
 * @ast node
 * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Simplic.ast:35
 * @astdecl Numeral : Expr ::= <NUMERAL:String>;
 * @production Numeral : {@link Expr} ::= <span class="component">&lt;NUMERAL:String&gt;</span>;

 */
public class Numeral extends Expr implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:156
   */
  public void genCode(PrintStream out){
	out.println("	movq $"+ Integer.parseInt(getNUMERAL())+ ", %rax");
}
  /**
   * @aspect Interpreter
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Interpreter.jrag:221
   */
  public int eval(ActivationRecord actrec)
{
	return Integer.parseInt(getNUMERAL());
}
  /**
   * @aspect PrettyPrint
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\PrettyPrint.jrag:73
   */
  public void prettyPrint(PrintStream out, String ind) {
		out.print(getNUMERAL());
	}
  /**
   * @aspect Visitor
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Visitor.jrag:104
   */
  public Object accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
  /**
   * @declaredat ASTNode:1
   */
  public Numeral() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /**
   * @declaredat ASTNode:12
   */
  @ASTNodeAnnotation.Constructor(
    name = {"NUMERAL"},
    type = {"String"},
    kind = {"Token"}
  )
  public Numeral(String p0) {
    setNUMERAL(p0);
  }
  /**
   * @declaredat ASTNode:20
   */
  public Numeral(beaver.Symbol p0) {
    setNUMERAL(p0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:24
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:32
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:36
   */
  @SideEffect.Ignore @SideEffect.Fresh public Numeral clone() throws CloneNotSupportedException {
    Numeral node = (Numeral) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public Numeral copy() {
    try {
      Numeral node = (Numeral) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:60
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Numeral fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:70
   */
  @SideEffect.Fresh(group="_ASTNode") public Numeral treeCopyNoTransform() {
    Numeral tree = (Numeral) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:90
   */
  @SideEffect.Fresh(group="_ASTNode") public Numeral treeCopy() {
    Numeral tree = (Numeral) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:104
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_NUMERAL == ((Numeral) node).tokenString_NUMERAL);    
  }
  /**
   * Replaces the lexeme NUMERAL.
   * @param value The new value for the lexeme NUMERAL.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setNUMERAL(String value) {
    tokenString_NUMERAL = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_NUMERAL;
  /**
   */
  public int NUMERALstart;
  /**
   */
  public int NUMERALend;
  /**
   * JastAdd-internal setter for lexeme NUMERAL using the Beaver parser.
   * @param symbol Symbol containing the new value for the lexeme NUMERAL
   * @apilevel internal
   */
  @SideEffect.Local(group="_ASTNode") public void setNUMERAL(beaver.Symbol symbol) {
    if (symbol.value != null && !(symbol.value instanceof String))
    throw new UnsupportedOperationException("setNUMERAL is only valid for String lexemes");
    tokenString_NUMERAL = (String)symbol.value;
    NUMERALstart = symbol.getStart();
    NUMERALend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme NUMERAL.
   * @return The value for the lexeme NUMERAL.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="NUMERAL")
  @SideEffect.Pure(group="_ASTNode") public String getNUMERAL() {
    return tokenString_NUMERAL != null ? tokenString_NUMERAL : "";
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean Type_visited = false;
  /**
   * @attribute syn
   * @aspect TypeAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:20
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:20")
  @SideEffect.Pure(group="_ASTNode") public Type Type() {
    if (Type_visited) {
      throw new RuntimeException("Circular definition of attribute Numeral.Type().");
    }
    Type_visited = true;
    Type Type_value = IntType();
    Type_visited = false;
    return Type_value;
  }
}

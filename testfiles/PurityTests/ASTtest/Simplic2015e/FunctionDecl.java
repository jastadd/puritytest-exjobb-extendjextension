/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.3 */
package lang.ast;
import java.util.*;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
/**
 * @ast node
 * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Simplic.ast:12
 * @astdecl FunctionDecl : AbstractDecl ::= <ID:String>;
 * @production FunctionDecl : {@link AbstractDecl} ::= <span class="component">&lt;ID:String&gt;</span>;

 */
public class FunctionDecl extends AbstractDecl implements Cloneable {
  /**
   * @aspect PrettyPrint
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\PrettyPrint.jrag:100
   */
  public void prettyPrint(PrintStream out, String ind) {

		out.print("int ");		
		out.print(getID());
	}
  /**
   * @aspect Visitor
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Visitor.jrag:113
   */
  public Object accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
  /**
   * @declaredat ASTNode:1
   */
  public FunctionDecl() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /**
   * @declaredat ASTNode:12
   */
  @ASTNodeAnnotation.Constructor(
    name = {"ID"},
    type = {"String"},
    kind = {"Token"}
  )
  public FunctionDecl(String p0) {
    setID(p0);
  }
  /**
   * @declaredat ASTNode:20
   */
  public FunctionDecl(beaver.Symbol p0) {
    setID(p0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:24
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:32
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:36
   */
  @SideEffect.Ignore @SideEffect.Fresh public FunctionDecl clone() throws CloneNotSupportedException {
    FunctionDecl node = (FunctionDecl) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  @SideEffect.Fresh(group="_ASTNode") @SideEffect.Ignore public FunctionDecl copy() {
    try {
      FunctionDecl node = (FunctionDecl) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:60
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public FunctionDecl fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:70
   */
  @SideEffect.Fresh(group="_ASTNode") public FunctionDecl treeCopyNoTransform() {
    FunctionDecl tree = (FunctionDecl) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:90
   */
  @SideEffect.Fresh(group="_ASTNode") public FunctionDecl treeCopy() {
    FunctionDecl tree = (FunctionDecl) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:104
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_ID == ((FunctionDecl) node).tokenString_ID);    
  }
  /**
   * Replaces the lexeme ID.
   * @param value The new value for the lexeme ID.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setID(String value) {
    tokenString_ID = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_ID;
  /**
   */
  public int IDstart;
  /**
   */
  public int IDend;
  /**
   * JastAdd-internal setter for lexeme ID using the Beaver parser.
   * @param symbol Symbol containing the new value for the lexeme ID
   * @apilevel internal
   */
  @SideEffect.Local(group="_ASTNode") public void setID(beaver.Symbol symbol) {
    if (symbol.value != null && !(symbol.value instanceof String))
    throw new UnsupportedOperationException("setID is only valid for String lexemes");
    tokenString_ID = (String)symbol.value;
    IDstart = symbol.getStart();
    IDend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme ID.
   * @return The value for the lexeme ID.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="ID")
  @SideEffect.Pure(group="_ASTNode") public String getID() {
    return tokenString_ID != null ? tokenString_ID : "";
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean address_visited = false;
  /**
   * @attribute syn
   * @aspect CodeGen
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:20
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CodeGen", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:20")
  @SideEffect.Pure(group="_ASTNode") public String address() {
    if (address_visited) {
      throw new RuntimeException("Circular definition of attribute FunctionDecl.address().");
    }
    address_visited = true;
    String address_value = (16 +getIndex()*8)+"(%rbp)";
    address_visited = false;
    return address_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean isVariable_visited = false;
  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:32
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:32")
  @SideEffect.Pure(group="_ASTNode") public boolean isVariable() {
    if (isVariable_visited) {
      throw new RuntimeException("Circular definition of attribute FunctionDecl.isVariable().");
    }
    isVariable_visited = true;
    boolean isVariable_value = false;
    isVariable_visited = false;
    return isVariable_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean isFunction_visited = false;
  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:35
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:35")
  @SideEffect.Pure(group="_ASTNode") public boolean isFunction() {
    if (isFunction_visited) {
      throw new RuntimeException("Circular definition of attribute FunctionDecl.isFunction().");
    }
    isFunction_visited = true;
    boolean isFunction_value = false;
    isFunction_visited = false;
    return isFunction_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Set localLookup_String_int_visited;
  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:114
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:114")
  @SideEffect.Pure(group="_ASTNode") public AbstractDecl localLookup(String name, int index) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(name);
    _parameters.add(index);
    if (localLookup_String_int_visited == null) localLookup_String_int_visited = new java.util.HashSet(4);
    if (localLookup_String_int_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute FunctionDecl.localLookup(String,int).");
    }
    localLookup_String_int_visited.add(_parameters);
    try {
    	if (getID().equals(name))
    		return this;
    	return UnknownDecl();
    	}
    finally {
      localLookup_String_int_visited.remove(_parameters);
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean uniqueName_visited = false;
  /**
   * @attribute syn
   * @aspect UniqueName
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\uniqueName.jrag:5
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="UniqueName", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\uniqueName.jrag:5")
  @SideEffect.Pure(group="_ASTNode") public String uniqueName() {
    if (uniqueName_visited) {
      throw new RuntimeException("Circular definition of attribute FunctionDecl.uniqueName().");
    }
    uniqueName_visited = true;
    String uniqueName_value = nameLookup(getID());
    uniqueName_visited = false;
    return uniqueName_value;
  }
  /**
   * @attribute inh
   * @aspect CodeGen
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:9
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="CodeGen", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:9")
  @SideEffect.Pure(group="_ASTNode") public int getIndex() {
    if (getIndex_visited) {
      throw new RuntimeException("Circular definition of attribute FunctionDecl.getIndex().");
    }
    getIndex_visited = true;
    int getIndex_value = getParent().Define_getIndex(this, null);
    getIndex_visited = false;
    return getIndex_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean getIndex_visited = false;
  /**
   * @attribute inh
   * @aspect UniqueName
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\uniqueName.jrag:8
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="UniqueName", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\uniqueName.jrag:8")
  @SideEffect.Pure(group="_ASTNode") public String nameLookup(String name) {
    Object _parameters = name;
    if (nameLookup_String_visited == null) nameLookup_String_visited = new java.util.HashSet(4);
    if (nameLookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute FunctionDecl.nameLookup(String).");
    }
    nameLookup_String_visited.add(_parameters);
    String nameLookup_String_value = getParent().Define_nameLookup(this, null, name);
    nameLookup_String_visited.remove(_parameters);
    return nameLookup_String_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected java.util.Set nameLookup_String_visited;
}

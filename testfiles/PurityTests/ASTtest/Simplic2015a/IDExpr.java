/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package lang.ast;
import java.util.*;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
/**
 * @ast node
 * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Simplic.ast:36
 * @production IDExpr : {@link Expr} ::= <span class="component">&lt;ID:String&gt;</span>;

 */
 public class IDExpr extends Expr implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:144
   */
  public void genCode(PrintStream out){
	out.println("movq "+((AbstractDecl)decl()).address()+", %rax");
}
  /**
   * @aspect Interpreter
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Interpreter.jrag:177
   */
  public int eval(ActivationRecord actrec) 
{
return actrec.get(((AbstractDecl)decl()).uniqueName());
}
  /**
   * @aspect PrettyPrint
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\PrettyPrint.jrag:77
   */
  public void prettyPrint(PrintStream out, String ind) {
		out.print(getID());
	}
  /**
   * @aspect Visitor
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Visitor.jrag:107
   */
  public Object accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
  /**
   * @declaredat ASTNode:1
   */
  public IDExpr() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /**
   * @declaredat ASTNode:12
   */
  public IDExpr(String p0) {
    setID(p0);
  }
  /**
   * @declaredat ASTNode:15
   */
  public IDExpr(beaver.Symbol p0) {
    setID(p0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:19
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:23
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:27
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:31
   */
  @SideEffect.Fresh public IDExpr clone() throws CloneNotSupportedException {
    IDExpr node = (IDExpr) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:36
   */
  @SideEffect.Fresh(group="_ASTNode") public IDExpr copy() {
    try {
      IDExpr node = (IDExpr) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:55
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public IDExpr fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:65
   */
  @SideEffect.Fresh(group="_ASTNode") public IDExpr treeCopyNoTransform() {
    IDExpr tree = (IDExpr) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:85
   */
  @SideEffect.Fresh(group="_ASTNode") public IDExpr treeCopy() {
    IDExpr tree = (IDExpr) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:99
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_ID == ((IDExpr) node).tokenString_ID);    
  }
  /**
   * Replaces the lexeme ID.
   * @param value The new value for the lexeme ID.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setID(String value) {
    tokenString_ID = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_ID;
  /**
   */
  public int IDstart;
  /**
   */
  public int IDend;
  /**
   * JastAdd-internal setter for lexeme ID using the Beaver parser.
   * @param symbol Symbol containing the new value for the lexeme ID
   * @apilevel internal
   */
  @SideEffect.Local(group="_ASTNode") public void setID(beaver.Symbol symbol) {
    if (symbol.value != null && !(symbol.value instanceof String))
    throw new UnsupportedOperationException("setID is only valid for String lexemes");
    tokenString_ID = (String)symbol.value;
    IDstart = symbol.getStart();
    IDend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme ID.
   * @return The value for the lexeme ID.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="ID")
  @SideEffect.Pure(group="_ASTNode") public String getID() {
    return tokenString_ID != null ? tokenString_ID : "";
  }
/** @apilevel internal */
@SideEffect.Secret(group="decl") protected boolean decl_visited = false;
  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:9
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:9")
  @SideEffect.Pure(group="decl") public ASTNode decl() {
    if (decl_visited) {
      throw new RuntimeException("Circular definition of attribute IDExpr.decl().");
    }
    decl_visited = true;
    ASTNode decl_value = lookup(getID());
    decl_visited = false;
    return decl_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="Type") protected boolean Type_visited = false;
  /**
   * @attribute syn
   * @aspect TypeAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:21
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:21")
  @SideEffect.Pure(group="Type") public Type Type() {
    if (Type_visited) {
      throw new RuntimeException("Circular definition of attribute IDExpr.Type().");
    }
    Type_visited = true;
    Type Type_value = IntType();
    Type_visited = false;
    return Type_value;
  }
  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:11
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:11")
  @SideEffect.Pure(group="lookup_String") public ASTNode lookup(String name) {
    Object _parameters = name;
    if (lookup_String_visited == null) lookup_String_visited = new java.util.HashSet(4);
    if (lookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute IDExpr.lookup(String).");
    }
    lookup_String_visited.add(_parameters);
    ASTNode lookup_String_value = getParent().Define_lookup(this, null, name);
    lookup_String_visited.remove(_parameters);
    return lookup_String_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="lookup_String") protected java.util.Set lookup_String_visited;
  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_Program_errors(Program _root, @SideEffect.Local java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Errors.jrag:34
    if (decl().isUnknown()) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    // @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Errors.jrag:37
    if (decl().isFunction()) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Program_errors(_root, _map);
  }
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_Program_errors(Set<ErrorMessage> collection) {
    super.contributeTo_Program_errors(collection);
    if (decl().isUnknown()) {
      collection.add(error("symbol '" + getID() + "' is not declared"));
    }
    if (decl().isFunction()) {
      collection.add(error("symbol '" + getID() + "' is function not variable"));
    }
  }
}

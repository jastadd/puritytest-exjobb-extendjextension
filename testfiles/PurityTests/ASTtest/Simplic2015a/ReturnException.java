package lang.ast;

import java.util.*;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
/**
 * @ast class
 * @aspect Interpreter
 * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Interpreter.jrag:30
 */
 class ReturnException extends Exception {
  

	private int value;

  
	
	public ReturnException(int i, String s)
	{
		super(s);
		value= i;
	}

  

	public int getValue(){
		return value;
	}


}

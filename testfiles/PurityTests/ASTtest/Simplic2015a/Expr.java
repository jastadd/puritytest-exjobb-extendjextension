/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package lang.ast;
import java.util.*;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
/**
 * @ast node
 * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Simplic.ast:23
 * @production Expr : {@link ASTNode};

 */
public abstract class Expr extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:148
   */
  public void genCode(PrintStream out){
	out.println("	movq $0, %rax #error: abstact expr");
}
  /**
   * @aspect CodeGen
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:152
   */
  public void genCode(PrintStream out,String label){
	out.println("# error General Expr as logic ");
}
  /**
   * @aspect Interpreter
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Interpreter.jrag:127
   */
  public int eval(ActivationRecord actrec)
{
	return 0;
}
  /**
   * @declaredat ASTNode:1
   */
  public Expr() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:17
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:21
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:25
   */
  @SideEffect.Fresh public Expr clone() throws CloneNotSupportedException {
    Expr node = (Expr) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:36
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public abstract Expr fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:44
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract Expr treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:52
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract Expr treeCopy();
/** @apilevel internal */
@SideEffect.Secret(group="collectFunctionCalls_Set_Function_") protected java.util.Set collectFunctionCalls_Set_Function__visited;
  /**
   * @attribute syn
   * @aspect FunctionCallsAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\FunctionCallsAnalysis.jrag:81
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="FunctionCallsAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\FunctionCallsAnalysis.jrag:81")
  @SideEffect.Pure(group="collectFunctionCalls_Set_Function_") public Set<Function> collectFunctionCalls(Set<Function> functions) {
    Object _parameters = functions;
    if (collectFunctionCalls_Set_Function__visited == null) collectFunctionCalls_Set_Function__visited = new java.util.HashSet(4);
    if (collectFunctionCalls_Set_Function__visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute Expr.collectFunctionCalls(Set_Function_).");
    }
    collectFunctionCalls_Set_Function__visited.add(_parameters);
    try {
    		return functions;
    	}
    finally {
      collectFunctionCalls_Set_Function__visited.remove(_parameters);
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="Type") protected boolean Type_visited = false;
  /**
   * @attribute syn
   * @aspect TypeAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:13
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:13")
  @SideEffect.Pure(group="Type") public Type Type() {
    if (Type_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.Type().");
    }
    Type_visited = true;
    Type Type_value = BoolType();
    Type_visited = false;
    return Type_value;
  }
  /**
   * @attribute inh
   * @aspect TypeAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:33
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:33")
  @SideEffect.Pure(group="expectedType") public Type expectedType() {
    if (expectedType_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.expectedType().");
    }
    expectedType_visited = true;
    Type expectedType_value = getParent().Define_expectedType(this, null);
    expectedType_visited = false;
    return expectedType_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="expectedType") protected boolean expectedType_visited = false;
  /**
   * @attribute inh
   * @aspect TypeAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:58
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:58")
  @SideEffect.Pure(group="BoolType") public Type BoolType() {
    if (BoolType_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.BoolType().");
    }
    BoolType_visited = true;
    Type BoolType_value = getParent().Define_BoolType(this, null);
    BoolType_visited = false;
    return BoolType_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="BoolType") protected boolean BoolType_visited = false;
  /**
   * @attribute inh
   * @aspect TypeAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:59
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:59")
  @SideEffect.Pure(group="IntType") public Type IntType() {
    if (IntType_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.IntType().");
    }
    IntType_visited = true;
    Type IntType_value = getParent().Define_IntType(this, null);
    IntType_visited = false;
    return IntType_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="IntType") protected boolean IntType_visited = false;
  /**
   * @attribute inh
   * @aspect TypeAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:60
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:60")
  @SideEffect.Pure(group="UnknownType") public Type UnknownType() {
    if (UnknownType_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.UnknownType().");
    }
    UnknownType_visited = true;
    Type UnknownType_value = getParent().Define_UnknownType(this, null);
    UnknownType_visited = false;
    return UnknownType_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="UnknownType") protected boolean UnknownType_visited = false;
  /**
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:33
   * @apilevel internal
   */
 @SideEffect.Pure public Type Define_expectedType(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return IntType();
  }
  @SideEffect.Pure protected boolean canDefine_expectedType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_Program_errors(Program _root, @SideEffect.Local java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Errors.jrag:66
    if (! Type().CompatibleType(expectedType())) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Program_errors(_root, _map);
  }
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_Program_errors(Set<ErrorMessage> collection) {
    super.contributeTo_Program_errors(collection);
    if (! Type().CompatibleType(expectedType())) {
      collection.add(error( Type().toString() + "is incompatible with" + expectedType().toString()));
    }
  }
}

/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package lang.ast;
import java.util.*;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
/**
 * @ast node
 * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Simplic.ast:3
 * @production Function : {@link ASTNode} ::= <span class="component">&lt;ID:String&gt;</span> <span class="component">{@link FunctionDecl}*</span> <span class="component">{@link Block}</span>;

 */
public class Function extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:115
   */
  public void genCode(PrintStream out) {
	out.println(getID() +":");
	out.println("	pushq %rbp");
	out.println("	movq %rsp, %rbp");
	out.println(" subq $"+numLocals()*8+", %rsp"); 
	getBlock().genCode(out);
	out.println(getID()+"_FEND:");
	out.println(" addq $"+numLocals()*8+", %rsp");
	out.println("	popq %rbp");
	out.println("	ret"); 
}
  /**
   * @aspect Interpreter
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Interpreter.jrag:59
   */
  public int eval(ActivationRecord actrec)
{
	try{
	switch (getID())
	{
		case "print": System.out.println(actrec.get("print")); break;
		case "read": return 2;
		default: getBlock().eval(actrec); break;
	}
	} catch (ReturnException e)
	{
		return e.getValue();
	}
	return 0;
}
  /**
   * @aspect PrettyPrint
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\PrettyPrint.jrag:173
   */
  public void prettyPrint(PrintStream out, String ind) {
		out.print("int ");
		out.print(getID());
		out.print("(");
		for(int i=0;i<getNumFunctionDecl()-1;i++){
		getFunctionDecl(i).prettyPrint(out, ind);
		out.print(",");
		}
		
		if (getNumFunctionDecl()!=0)
		getFunctionDecl(getNumFunctionDecl()-1).prettyPrint(out, ind);
		out.print(") {\n");
		ind=ind+"\t";
		getBlock().prettyPrint(out,ind);
		ind=ind.substring(0,ind.length()-1);
		out.print("}\n");
	}
  /**
   * @aspect Visitor
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Visitor.jrag:47
   */
  public Object accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
  /**
   * @declaredat ASTNode:1
   */
  public Function() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
    setChild(new List(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  public Function(String p0, List<FunctionDecl> p1, Block p2) {
    setID(p0);
    setChild(p1, 0);
    setChild(p2, 1);
  }
  /**
   * @declaredat ASTNode:19
   */
  public Function(beaver.Symbol p0, List<FunctionDecl> p1, Block p2) {
    setID(p0);
    setChild(p1, 0);
    setChild(p2, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:25
   */
  @SideEffect.Pure protected int numChildren() {
    return 2;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:29
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    reachable_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:34
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  @SideEffect.Fresh public Function clone() throws CloneNotSupportedException {
    Function node = (Function) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  @SideEffect.Fresh(group="_ASTNode") public Function copy() {
    try {
      Function node = (Function) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:62
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Function fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:72
   */
  @SideEffect.Fresh(group="_ASTNode") public Function treeCopyNoTransform() {
    Function tree = (Function) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:92
   */
  @SideEffect.Fresh(group="_ASTNode") public Function treeCopy() {
    Function tree = (Function) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:106
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_ID == ((Function) node).tokenString_ID);    
  }
  /**
   * Replaces the lexeme ID.
   * @param value The new value for the lexeme ID.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setID(String value) {
    tokenString_ID = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_ID;
  /**
   */
  public int IDstart;
  /**
   */
  public int IDend;
  /**
   * JastAdd-internal setter for lexeme ID using the Beaver parser.
   * @param symbol Symbol containing the new value for the lexeme ID
   * @apilevel internal
   */
  @SideEffect.Local(group="_ASTNode") public void setID(beaver.Symbol symbol) {
    if (symbol.value != null && !(symbol.value instanceof String))
    throw new UnsupportedOperationException("setID is only valid for String lexemes");
    tokenString_ID = (String)symbol.value;
    IDstart = symbol.getStart();
    IDend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme ID.
   * @return The value for the lexeme ID.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="ID")
  @SideEffect.Pure(group="_ASTNode") public String getID() {
    return tokenString_ID != null ? tokenString_ID : "";
  }
  /**
   * Replaces the FunctionDecl list.
   * @param list The new list node to be used as the FunctionDecl list.
   * @apilevel high-level
   */
  public void setFunctionDeclList(List<FunctionDecl> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the FunctionDecl list.
   * @return Number of children in the FunctionDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumFunctionDecl() {
    return getFunctionDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the FunctionDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the FunctionDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumFunctionDeclNoTransform() {
    return getFunctionDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the FunctionDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the FunctionDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public FunctionDecl getFunctionDecl(int i) {
    return (FunctionDecl) getFunctionDeclList().getChild(i);
  }
  /**
   * Check whether the FunctionDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasFunctionDecl() {
    return getFunctionDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the FunctionDecl list.
   * @param node The element to append to the FunctionDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addFunctionDecl(FunctionDecl node) {
    List<FunctionDecl> list = (parent == null) ? getFunctionDeclListNoTransform() : getFunctionDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addFunctionDeclNoTransform(FunctionDecl node) {
    List<FunctionDecl> list = getFunctionDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the FunctionDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setFunctionDecl(FunctionDecl node, int i) {
    List<FunctionDecl> list = getFunctionDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the FunctionDecl list.
   * @return The node representing the FunctionDecl list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="FunctionDecl")
  @SideEffect.Pure(group="_ASTNode") public List<FunctionDecl> getFunctionDeclList() {
    List<FunctionDecl> list = (List<FunctionDecl>) getChild(0);
    return list;
  }
  /**
   * Retrieves the FunctionDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the FunctionDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<FunctionDecl> getFunctionDeclListNoTransform() {
    return (List<FunctionDecl>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the FunctionDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public FunctionDecl getFunctionDeclNoTransform(int i) {
    return (FunctionDecl) getFunctionDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the FunctionDecl list.
   * @return The node representing the FunctionDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<FunctionDecl> getFunctionDecls() {
    return getFunctionDeclList();
  }
  /**
   * Retrieves the FunctionDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the FunctionDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<FunctionDecl> getFunctionDeclsNoTransform() {
    return getFunctionDeclListNoTransform();
  }
  /**
   * Replaces the Block child.
   * @param node The new node to replace the Block child.
   * @apilevel high-level
   */
  public void setBlock(Block node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the Block child.
   * @return The current node used as the Block child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Block")
  @SideEffect.Pure public Block getBlock() {
    return (Block) getChild(1);
  }
  /**
   * Retrieves the Block child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Block child.
   * @apilevel low-level
   */
  @SideEffect.Pure public Block getBlockNoTransform() {
    return (Block) getChildNoTransform(1);
  }
/** @apilevel internal */
@SideEffect.Secret(group="localIndex") protected boolean localIndex_visited = false;
  /**
   * Local variable numbering.
   * @attribute syn
   * @aspect CodeGen
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:316
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CodeGen", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:316")
  @SideEffect.Pure(group="localIndex") public int localIndex() {
    if (localIndex_visited) {
      throw new RuntimeException("Circular definition of attribute ASTNode.localIndex().");
    }
    localIndex_visited = true;
    int localIndex_value = 0;
    localIndex_visited = false;
    return localIndex_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="collectFunctionCalls") protected boolean collectFunctionCalls_visited = false;
  /**
   * @attribute syn
   * @aspect FunctionCallsAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\FunctionCallsAnalysis.jrag:20
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="FunctionCallsAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\FunctionCallsAnalysis.jrag:20")
  @SideEffect.Pure(group="collectFunctionCalls") public Set<Function> collectFunctionCalls() {
    if (collectFunctionCalls_visited) {
      throw new RuntimeException("Circular definition of attribute Function.collectFunctionCalls().");
    }
    collectFunctionCalls_visited = true;
    try {
    		Set<Function> functions = new HashSet<Function>();
    		return getBlock().collectFunctionCalls(functions);
    	}
    finally {
      collectFunctionCalls_visited = false;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="isMultipledeclared") protected boolean isMultipledeclared_visited = false;
  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:22
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:22")
  @SideEffect.Pure(group="isMultipledeclared") public boolean isMultipledeclared() {
    if (isMultipledeclared_visited) {
      throw new RuntimeException("Circular definition of attribute Function.isMultipledeclared().");
    }
    isMultipledeclared_visited = true;
    boolean isMultipledeclared_value = lookup(getID()) != this;
    isMultipledeclared_visited = false;
    return isMultipledeclared_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="nbrParams") protected boolean nbrParams_visited = false;
  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:27
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:27")
  @SideEffect.Pure(group="nbrParams") public int nbrParams() {
    if (nbrParams_visited) {
      throw new RuntimeException("Circular definition of attribute Function.nbrParams().");
    }
    nbrParams_visited = true;
    int nbrParams_value = getNumFunctionDecl();
    nbrParams_visited = false;
    return nbrParams_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="isVariable") protected boolean isVariable_visited = false;
  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:30
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:30")
  @SideEffect.Pure(group="isVariable") public boolean isVariable() {
    if (isVariable_visited) {
      throw new RuntimeException("Circular definition of attribute Function.isVariable().");
    }
    isVariable_visited = true;
    boolean isVariable_value = false;
    isVariable_visited = false;
    return isVariable_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="isFunction") protected boolean isFunction_visited = false;
  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:33
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:33")
  @SideEffect.Pure(group="isFunction") public boolean isFunction() {
    if (isFunction_visited) {
      throw new RuntimeException("Circular definition of attribute Function.isFunction().");
    }
    isFunction_visited = true;
    boolean isFunction_value = true;
    isFunction_visited = false;
    return isFunction_value;
  }
/** @apilevel internal */
  @SideEffect.Secret(group="reachable") protected ASTState.Cycle reachable_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void reachable_reset() {
    reachable_computed = false;
    reachable_initialized = false;
    reachable_value = null;
    reachable_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="reachable") protected boolean reachable_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="reachable") protected Set<Function> reachable_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="reachable") protected boolean reachable_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Reachable", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Reachable.jrag:24")
  @SideEffect.Pure(group="reachable") public Set<Function> reachable() {
    if (reachable_computed) {
      return reachable_value;
    }
    ASTState state = state();
    if (!reachable_initialized) {
      reachable_initialized = true;
      reachable_value = new HashSet<Function>();
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      do {
        reachable_cycle = state.nextCycle();
        Set<Function> new_reachable_value = reachable_compute();
        if ((new_reachable_value == null && reachable_value != null) || (new_reachable_value != null && !new_reachable_value.equals(reachable_value))) {
          state.setChangeInCycle();
        }
        reachable_value = new_reachable_value;
      } while (state.testAndClearChangeInCycle());
      reachable_computed = true;
      state.startLastCycle();
      Set<Function> $tmp = reachable_compute();

      state.leaveCircle();
    } else if (reachable_cycle != state.cycle()) {
      reachable_cycle = state.cycle();
      if (state.lastCycle()) {
        reachable_computed = true;
        Set<Function> new_reachable_value = reachable_compute();
        return new_reachable_value;
      }
      Set<Function> new_reachable_value = reachable_compute();
      if ((new_reachable_value == null && reachable_value != null) || (new_reachable_value != null && !new_reachable_value.equals(reachable_value))) {
        state.setChangeInCycle();
      }
      reachable_value = new_reachable_value;
    } else {
    }
    return reachable_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Set<Function> reachable_compute() {
  	
  	Set<Function> temp = new HashSet<Function>(collectFunctionCalls());
  	
  	for(Function f : collectFunctionCalls())
  	{
  		temp.addAll(f.reachable());
  
  	}
  	
  	return temp;
  
  }
  /**
   * @attribute inh
   * @aspect NameAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:13
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:13")
  @SideEffect.Pure(group="lookup_String") public ASTNode lookup(String name) {
    Object _parameters = name;
    if (lookup_String_visited == null) lookup_String_visited = new java.util.HashSet(4);
    if (lookup_String_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute Function.lookup(String).");
    }
    lookup_String_visited.add(_parameters);
    ASTNode lookup_String_value = getParent().Define_lookup(this, null, name);
    lookup_String_visited.remove(_parameters);
    return lookup_String_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="lookup_String") protected java.util.Set lookup_String_visited;
  /**
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:10
   * @apilevel internal
   */
 @SideEffect.Pure public String Define_FunctionID(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    {
    return getID();
    }
  }
  @SideEffect.Pure protected boolean canDefine_FunctionID(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:9
   * @apilevel internal
   */
 @SideEffect.Pure public int Define_getIndex(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getFunctionDeclListNoTransform()) {
      // @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:15
      int index = _callerNode.getIndexOfChild(_childNode);
      {
      	return index;
      }
    }
    else if (_callerNode == getBlockNoTransform()) {
      // @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:14
      return 0;
    }
    else {
      return getParent().Define_getIndex(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_getIndex(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:10
   * @apilevel internal
   */
 @SideEffect.Pure public ASTNode Define_lookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getBlockNoTransform()) {
      // @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:82
      {
      		AbstractDecl decl;
      		for (int i = 0; i < getNumFunctionDecl(); i++) {
      			decl = getFunctionDecl(i).localLookup(name, 0);
      			if(!decl.isUnknown())
      			{
      				return decl;
      			}	
      	}
      	return 	lookup(name);
      	}
    }
    else {
      return getParent().Define_lookup(this, _callerNode, name);
    }
  }
  @SideEffect.Pure protected boolean canDefine_lookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\uniqueName.jrag:9
   * @apilevel internal
   */
 @SideEffect.Pure public String Define_nameLookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    int childIndex = this.getIndexOfChild(_callerNode);
    {
    		return name;
    	}
  }
  @SideEffect.Pure protected boolean canDefine_nameLookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_Program_errors(Program _root, @SideEffect.Local java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Errors.jrag:62
    if (isMultipledeclared()) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Program_errors(_root, _map);
  }
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_Program_errors(Set<ErrorMessage> collection) {
    super.contributeTo_Program_errors(collection);
    if (isMultipledeclared()) {
      collection.add(error("Declaration of function '" + getID() + "' is multipled declared"));
    }
  }
}

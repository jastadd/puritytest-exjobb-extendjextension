/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package lang.ast;
import java.util.*;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
/**
 * @ast node
 * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Simplic.ast:7
 * @production If : {@link Stmt} ::= <span class="component">Cond:{@link Expr}</span> <span class="component">Do:{@link Block}</span> <span class="component">Else:{@link Block}</span>;

 */
public class If extends Stmt implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:216
   */
  public void genCode(PrintStream out){

	getCond().genCode(out,"else_lbl"+data.num_if);
	getDo().genCode(out);
	out.println("	jmp fi"+data.num_if);
	out.println("else_lbl"+data.num_if+":");
	getElse().genCode(out);
	out.println("fi"+data.num_if+":");
	data.num_if++;

}
  /**
   * @aspect Interpreter
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Interpreter.jrag:87
   */
  public void eval(ActivationRecord actrec) throws ReturnException
{
	if(getCond().eval(actrec) == 1)
	{
		getDo().eval(actrec);
	}else if (getElse().hasStmt())
	{
		getElse().eval(actrec);
	}
}
  /**
   * @aspect PrettyPrint
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\PrettyPrint.jrag:118
   */
  public void prettyPrint(PrintStream out, String ind) {
		out.print(ind);	
		out.print("if (");
		getCond().prettyPrint(out, ind);
		out.print(") {\n");
		ind=ind+"\t";
		getDo().prettyPrint(out, ind);
		ind=ind.substring(0,ind.length()-1);
		out.print(ind);
		out.print("}");
		if (getElse().hasStmt()){
		out.print(" else {\n");
		ind=ind+"\t";
		getElse().prettyPrint(out, ind);
		out.print(ind);
		out.print("}");
		ind=ind.substring(0,ind.length()-1);
		}
	}
  /**
   * @aspect Visitor
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Visitor.jrag:53
   */
  public Object accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
  /**
   * @declaredat ASTNode:1
   */
  public If() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[3];
  }
  /**
   * @declaredat ASTNode:13
   */
  public If(Expr p0, Block p1, Block p2) {
    setChild(p0, 0);
    setChild(p1, 1);
    setChild(p2, 2);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:19
   */
  @SideEffect.Pure protected int numChildren() {
    return 3;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:23
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:27
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:31
   */
  @SideEffect.Fresh public If clone() throws CloneNotSupportedException {
    If node = (If) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:36
   */
  @SideEffect.Fresh(group="_ASTNode") public If copy() {
    try {
      If node = (If) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:55
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public If fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:65
   */
  @SideEffect.Fresh(group="_ASTNode") public If treeCopyNoTransform() {
    If tree = (If) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:85
   */
  @SideEffect.Fresh(group="_ASTNode") public If treeCopy() {
    If tree = (If) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:99
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the Cond child.
   * @param node The new node to replace the Cond child.
   * @apilevel high-level
   */
  public void setCond(Expr node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Cond child.
   * @return The current node used as the Cond child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Cond")
  @SideEffect.Pure public Expr getCond() {
    return (Expr) getChild(0);
  }
  /**
   * Retrieves the Cond child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Cond child.
   * @apilevel low-level
   */
  @SideEffect.Pure public Expr getCondNoTransform() {
    return (Expr) getChildNoTransform(0);
  }
  /**
   * Replaces the Do child.
   * @param node The new node to replace the Do child.
   * @apilevel high-level
   */
  public void setDo(Block node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the Do child.
   * @return The current node used as the Do child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Do")
  @SideEffect.Pure public Block getDo() {
    return (Block) getChild(1);
  }
  /**
   * Retrieves the Do child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Do child.
   * @apilevel low-level
   */
  @SideEffect.Pure public Block getDoNoTransform() {
    return (Block) getChildNoTransform(1);
  }
  /**
   * Replaces the Else child.
   * @param node The new node to replace the Else child.
   * @apilevel high-level
   */
  public void setElse(Block node) {
    setChild(node, 2);
  }
  /**
   * Retrieves the Else child.
   * @return The current node used as the Else child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Else")
  @SideEffect.Pure public Block getElse() {
    return (Block) getChild(2);
  }
  /**
   * Retrieves the Else child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Else child.
   * @apilevel low-level
   */
  @SideEffect.Pure public Block getElseNoTransform() {
    return (Block) getChildNoTransform(2);
  }
/** @apilevel internal */
@SideEffect.Secret(group="collectFunctionCalls_Set_Function_") protected java.util.Set collectFunctionCalls_Set_Function__visited;
  /**
   * @attribute syn
   * @aspect FunctionCallsAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\FunctionCallsAnalysis.jrag:45
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="FunctionCallsAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\FunctionCallsAnalysis.jrag:45")
  @SideEffect.Pure(group="collectFunctionCalls_Set_Function_") public Set<Function> collectFunctionCalls(Set<Function> functions) {
    Object _parameters = functions;
    if (collectFunctionCalls_Set_Function__visited == null) collectFunctionCalls_Set_Function__visited = new java.util.HashSet(4);
    if (collectFunctionCalls_Set_Function__visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute If.collectFunctionCalls(Set_Function_).");
    }
    collectFunctionCalls_Set_Function__visited.add(_parameters);
    try {
    		getDo().collectFunctionCalls(functions);
    		if (getElse().hasStmt()){
    			getElse().collectFunctionCalls(functions);
    		}
    		return functions;
    	}
    finally {
      collectFunctionCalls_Set_Function__visited.remove(_parameters);
    }
  }
  /**
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:33
   * @apilevel internal
   */
 @SideEffect.Pure public Type Define_expectedType(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getCondNoTransform()) {
      // @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:39
      return BoolType();
    }
    else {
      return getParent().Define_expectedType(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_expectedType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
}

/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package lang.ast;
import java.util.*;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
/**
 * @ast node
 * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Simplic.ast:5
 * @production Stmt : {@link ASTNode};

 */
public abstract class Stmt extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:134
   */
  public void genCode(PrintStream out) {
}
  /**
   * @aspect Interpreter
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Interpreter.jrag:123
   */
  public void eval(ActivationRecord actrec) throws ReturnException
{
}
  /**
   * @declaredat ASTNode:1
   */
  public Stmt() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:17
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:21
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:25
   */
  @SideEffect.Fresh public Stmt clone() throws CloneNotSupportedException {
    Stmt node = (Stmt) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:36
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public abstract Stmt fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:44
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract Stmt treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:52
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract Stmt treeCopy();
/** @apilevel internal */
@SideEffect.Secret(group="collectFunctionCalls_Set_Function_") protected java.util.Set collectFunctionCalls_Set_Function__visited;
  /**
   * @attribute syn
   * @aspect FunctionCallsAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\FunctionCallsAnalysis.jrag:35
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="FunctionCallsAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\FunctionCallsAnalysis.jrag:35")
  @SideEffect.Pure(group="collectFunctionCalls_Set_Function_") public Set<Function> collectFunctionCalls(Set<Function> functions) {
    Object _parameters = functions;
    if (collectFunctionCalls_Set_Function__visited == null) collectFunctionCalls_Set_Function__visited = new java.util.HashSet(4);
    if (collectFunctionCalls_Set_Function__visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute Stmt.collectFunctionCalls(Set_Function_).");
    }
    collectFunctionCalls_Set_Function__visited.add(_parameters);
    try {
    		return functions;
    	}
    finally {
      collectFunctionCalls_Set_Function__visited.remove(_parameters);
    }
  }
  /**
   * @attribute inh
   * @aspect TypeAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:34
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:34")
  @SideEffect.Pure(group="BoolType") public Type BoolType() {
    if (BoolType_visited) {
      throw new RuntimeException("Circular definition of attribute Stmt.BoolType().");
    }
    BoolType_visited = true;
    Type BoolType_value = getParent().Define_BoolType(this, null);
    BoolType_visited = false;
    return BoolType_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="BoolType") protected boolean BoolType_visited = false;
  /**
   * @attribute inh
   * @aspect TypeAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:35
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:35")
  @SideEffect.Pure(group="IntType") public Type IntType() {
    if (IntType_visited) {
      throw new RuntimeException("Circular definition of attribute Stmt.IntType().");
    }
    IntType_visited = true;
    Type IntType_value = getParent().Define_IntType(this, null);
    IntType_visited = false;
    return IntType_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="IntType") protected boolean IntType_visited = false;
  /**
   * @attribute inh
   * @aspect TypeAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:36
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:36")
  @SideEffect.Pure(group="UnknownType") public Type UnknownType() {
    if (UnknownType_visited) {
      throw new RuntimeException("Circular definition of attribute Stmt.UnknownType().");
    }
    UnknownType_visited = true;
    Type UnknownType_value = getParent().Define_UnknownType(this, null);
    UnknownType_visited = false;
    return UnknownType_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="UnknownType") protected boolean UnknownType_visited = false;
}

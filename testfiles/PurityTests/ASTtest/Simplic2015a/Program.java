/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package lang.ast;
import java.util.*;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
/**
 * @ast node
 * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Simplic.ast:1
 * @production Program : {@link ASTNode} ::= <span class="component">{@link Function}*</span>;

 */
public class Program extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:23
   */
  public void genCode(PrintStream out) {

	out.println(".global _start");
	out.println(".data");
	out.println("buf: .skip 1024");
			out.println();
	out.println(".text");
	out.println("_start:");
	out.println("	call main");

	out.println("	movq %rax, %rdi");
	out.println("	movq $60, %rax");
	out.println("	syscall");

	for (Function decl: getFunctionList()) {
		decl.genCode(out);
	}
// helper procedures (print/read)
		out.println("# Procedure to read number from stdin");
		out.println("# C signature: long int read(void)");
		out.println("read:");
		out.println("        pushq %rbp");
		out.println("        movq %rsp, %rbp");
		out.println("        movq $0, %rdi");
		out.println("        movq $buf, %rsi");
		out.println("        movq $1024, %rdx");
		out.println("        movq $0, %rax");
		out.println("        syscall                 # %rax = sys_read(0, buf, 1024)");
		out.println("        ### convert string to integer:");
		out.println("        ### %rax contains nchar");
		out.println("        ### %rsi contains ptr");
		out.println("        movq $0, %rdx           # sum = 0");
		out.println("atoi_loop:");
		out.println("        cmpq $0, %rax           # while (nchar > 0)");
		out.println("        jle atoi_done           # leave loop if nchar <= 0");
		out.println("        movzbq (%rsi), %rbx     # move byte, and sign extend to qword");
		out.println("        cmpq $0x30, %rbx        # test if < '0'");
		out.println("        jl atoi_done            # character is not numeric");
		out.println("        cmpq $0x39, %rbx        # test if > '9'");
		out.println("        jg atoi_done            # character is not numeric");
		out.println("        imulq $10, %rdx         # multiply sum by 10");
		out.println("        subq $0x30, %rbx        # value of character");
		out.println("        addq %rbx, %rdx         # add to sum");
		out.println("        incq %rsi               # step to next char");
		out.println("        decq %rax               # nchar--");
		out.println("        jmp atoi_loop           # loop back");
		out.println("atoi_done:");
		out.println("        movq %rdx, %rax         # return value in RAX");
		out.println("        popq %rbp");
		out.println("        ret");
		out.println();
		out.println("# Procedure to print number to stdout");
		out.println("# C signature: void print(long int)");
		out.println("print:");
		out.println("        pushq %rbp");
		out.println("        movq %rsp, %rbp");
		out.println("        ### convert integer to string");
		out.println("        movq 16(%rbp), %rax     # parameter");
		out.println("        movq $(buf+1023), %rsi  # write ptr (start from end of buf)");
		out.println("        movb $0x0a, (%rsi)      # insert newline");
		out.println("        movq $1, %rcx           # string length");
		out.println("itoa_loop:                      # do.. while (at least one iteration)");
		out.println("        movq $10, %rbx");
		out.println("        movq $0, %rdx");
		out.println("        idivq %rbx              # divide rdx:rax by 10");
		out.println("        addb $0x30, %dl         # remainder + '0'");
		out.println("        decq %rsi               # move string pointer");
		out.println("        movb %dl, (%rsi)");
		out.println("        incq %rcx               # increment string length");
		out.println("        cmpq $0, %rax");
		out.println("        jg itoa_loop            # produce more digits");
		out.println("itoa_done:");
		out.println("        movq $1, %rdi");
		out.println("        movq %rcx, %rdx");
		out.println("        movq $1, %rax");
		out.println("        syscall");
		out.println("        popq %rbp");
		out.println("        ret");
		out.println();
		out.println("print_string:");
		out.println("        pushq %rbp");
		out.println("        movq %rsp, %rbp");
		out.println("        movq $1, %rdi");
		out.println("        movq 16(%rbp), %rsi");
		out.println("        movq 24(%rbp), %rdx");
		out.println("        movq $1, %rax");
		out.println("        syscall");
		out.println("        popq %rbp");
		out.println("        ret");
}
  /**
   * @aspect Interpreter
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Interpreter.jrag:48
   */
  public void eval()
{
	ASTNode  main = ((Function)(getChild(0).getChild(0))).lookup("main");
	if (!main.isFunction()){
		throw new RuntimeException("No Main function :(");
	}
	 ((Function) main).eval(new ActivationRecord());

}
  /**
   * @aspect PrettyPrint
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\PrettyPrint.jrag:190
   */
  public void prettyPrint(PrintStream out, String ind) {

		for(int i=0;i<getNumFunction();i++){
		getFunction(i).prettyPrint(out, ind);
		}

	}
  /**
   * @aspect Visitor
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Visitor.jrag:41
   */
  public Object accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
  /**
   * @declaredat ASTNode:1
   */
  public Program() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
    setChild(new List(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  public Program(List<Function> p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:18
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:22
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    UnknownDecl_reset();
    predefinedFunctions_reset();
    BoolType_reset();
    IntType_reset();
    UnknownType_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:31
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
    Program_errors_visited = false;
    Program_errors_computed = false;
    
    Program_errors_value = null;
    contributorMap_Program_errors = null;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  @SideEffect.Fresh public Program clone() throws CloneNotSupportedException {
    Program node = (Program) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  @SideEffect.Fresh(group="_ASTNode") public Program copy() {
    try {
      Program node = (Program) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:64
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Program fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:74
   */
  @SideEffect.Fresh(group="_ASTNode") public Program treeCopyNoTransform() {
    Program tree = (Program) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:94
   */
  @SideEffect.Fresh(group="_ASTNode") public Program treeCopy() {
    Program tree = (Program) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:108
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the Function list.
   * @param list The new list node to be used as the Function list.
   * @apilevel high-level
   */
  public void setFunctionList(List<Function> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the Function list.
   * @return Number of children in the Function list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumFunction() {
    return getFunctionList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Function list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Function list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumFunctionNoTransform() {
    return getFunctionListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Function list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Function list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Function getFunction(int i) {
    return (Function) getFunctionList().getChild(i);
  }
  /**
   * Check whether the Function list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasFunction() {
    return getFunctionList().getNumChild() != 0;
  }
  /**
   * Append an element to the Function list.
   * @param node The element to append to the Function list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addFunction(Function node) {
    List<Function> list = (parent == null) ? getFunctionListNoTransform() : getFunctionList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addFunctionNoTransform(Function node) {
    List<Function> list = getFunctionListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Function list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setFunction(Function node, int i) {
    List<Function> list = getFunctionList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Function list.
   * @return The node representing the Function list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Function")
  @SideEffect.Pure(group="_ASTNode") public List<Function> getFunctionList() {
    List<Function> list = (List<Function>) getChild(0);
    return list;
  }
  /**
   * Retrieves the Function list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Function list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Function> getFunctionListNoTransform() {
    return (List<Function>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the Function list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Function getFunctionNoTransform(int i) {
    return (Function) getFunctionListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Function list.
   * @return The node representing the Function list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Function> getFunctions() {
    return getFunctionList();
  }
  /**
   * Retrieves the Function list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Function list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Function> getFunctionsNoTransform() {
    return getFunctionListNoTransform();
  }
  /**
   * @aspect <NoAspect>
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Errors.jrag:26
   */
  @SideEffect.Local protected java.util.Map<ASTNode, java.util.Set<ASTNode>> contributorMap_Program_errors = null;

  @SideEffect.Ignore protected void survey_Program_errors() {
    if (contributorMap_Program_errors == null) {
      contributorMap_Program_errors = new java.util.IdentityHashMap<ASTNode, java.util.Set<ASTNode>>();
      collect_contributors_Program_errors(this, contributorMap_Program_errors);
    }
  }

/** @apilevel internal */
@SideEffect.Secret(group="localIndex") protected boolean localIndex_visited = false;
  /**
   * Local variable numbering.
   * @attribute syn
   * @aspect CodeGen
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:316
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CodeGen", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:316")
  @SideEffect.Pure(group="localIndex") public int localIndex() {
    if (localIndex_visited) {
      throw new RuntimeException("Circular definition of attribute ASTNode.localIndex().");
    }
    localIndex_visited = true;
    int localIndex_value = 0;
    localIndex_visited = false;
    return localIndex_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="functionCalls") protected boolean functionCalls_visited = false;
  /**
   * @attribute syn
   * @aspect FunctionCallsAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\FunctionCallsAnalysis.jrag:4
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="FunctionCallsAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\FunctionCallsAnalysis.jrag:4")
  @SideEffect.Pure(group="functionCalls") public Map<Function, Set<Function>> functionCalls() {
    if (functionCalls_visited) {
      throw new RuntimeException("Circular definition of attribute Program.functionCalls().");
    }
    functionCalls_visited = true;
    Map<Function, Set<Function>> functionCalls_value = collectFunctionCalls();
    functionCalls_visited = false;
    return functionCalls_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="collectFunctionCalls") protected boolean collectFunctionCalls_visited = false;
  /**
   * @attribute syn
   * @aspect FunctionCallsAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\FunctionCallsAnalysis.jrag:9
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="FunctionCallsAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\FunctionCallsAnalysis.jrag:9")
  @SideEffect.Pure(group="collectFunctionCalls") public Map<Function, Set<Function>> collectFunctionCalls() {
    if (collectFunctionCalls_visited) {
      throw new RuntimeException("Circular definition of attribute Program.collectFunctionCalls().");
    }
    collectFunctionCalls_visited = true;
    try {
    		Map<Function, Set<Function>> functionCalls = new HashMap<Function, Set<Function>>();
    		
    		for (int i = 0; i < getNumFunction(); i++) {
    			functionCalls.put(getFunction(i),getFunction(i).collectFunctionCalls());
    		}
    		
    		return functionCalls;
    	}
    finally {
      collectFunctionCalls_visited = false;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="UnknownDecl") protected boolean UnknownDecl_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void UnknownDecl_reset() {
    UnknownDecl_computed = false;
    
    UnknownDecl_value = null;
    UnknownDecl_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="UnknownDecl") protected boolean UnknownDecl_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="UnknownDecl") protected UnknownDecl UnknownDecl_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:4
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:4")
  @SideEffect.Pure(group="UnknownDecl") public UnknownDecl UnknownDecl() {
    ASTState state = state();
    if (UnknownDecl_computed) {
      return UnknownDecl_value;
    }
    if (UnknownDecl_visited) {
      throw new RuntimeException("Circular definition of attribute Program.UnknownDecl().");
    }
    UnknownDecl_visited = true;
    state().enterLazyAttribute();
    UnknownDecl_value = new UnknownDecl();
    UnknownDecl_value.setParent(this);
    UnknownDecl_computed = true;
    state().leaveLazyAttribute();
    UnknownDecl_visited = false;
    return UnknownDecl_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="predefinedFunctions") protected boolean predefinedFunctions_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void predefinedFunctions_reset() {
    predefinedFunctions_computed = false;
    
    predefinedFunctions_value = null;
    predefinedFunctions_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="predefinedFunctions") protected boolean predefinedFunctions_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="predefinedFunctions") protected List<Function> predefinedFunctions_value;

  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:130
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:130")
  @SideEffect.Pure(group="predefinedFunctions") public List<Function> predefinedFunctions() {
    ASTState state = state();
    if (predefinedFunctions_computed) {
      return predefinedFunctions_value;
    }
    if (predefinedFunctions_visited) {
      throw new RuntimeException("Circular definition of attribute Program.predefinedFunctions().");
    }
    predefinedFunctions_visited = true;
    state().enterLazyAttribute();
    predefinedFunctions_value = predefinedFunctions_compute();
    predefinedFunctions_value.setParent(this);
    predefinedFunctions_computed = true;
    state().leaveLazyAttribute();
    predefinedFunctions_visited = false;
    return predefinedFunctions_value;
  }
  /** @apilevel internal */
  @SideEffect.Fresh private List<Function> predefinedFunctions_compute() {
  		List<Function> list = new List<Function>();
  		Function temp = new Function ("print", new List<FunctionDecl>(),new Block());
  		temp.addFunctionDecl( new FunctionDecl("print"));
  		list.add(new Function("read",new List<FunctionDecl>(),new Block()));
  		list.add(temp);
  		return list;
  	}
/** @apilevel internal */
@SideEffect.Secret(group="deadfunctions") protected boolean deadfunctions_visited = false;
  /**
   * @attribute syn
   * @aspect Reachable
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Reachable.jrag:5
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Reachable", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Reachable.jrag:5")
  @SideEffect.Pure(group="deadfunctions") public Set<Function> deadfunctions() {
    if (deadfunctions_visited) {
      throw new RuntimeException("Circular definition of attribute Program.deadfunctions().");
    }
    deadfunctions_visited = true;
    try {
    
    	Set<Function> AllFunctions = new HashSet<Function>();
    	Set<Function> Reachable = new HashSet<Function>();
    	
    	for (int i = 0; i < getNumFunction(); i++) {
    	
    		AllFunctions.add(getFunction(i));
    		
    		if (getFunction(i).getID().equals("main")){
    			Reachable = getFunction(i).reachable();
    			Reachable.add(getFunction(i));
    		}
    	}
    	
    	AllFunctions.removeAll(Reachable);
    	return AllFunctions;
    }
    finally {
      deadfunctions_visited = false;
    }
  }
/** @apilevel internal */
@SideEffect.Secret(group="BoolType") protected boolean BoolType_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void BoolType_reset() {
    BoolType_computed = false;
    
    BoolType_value = null;
    BoolType_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="BoolType") protected boolean BoolType_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="BoolType") protected BoolType BoolType_value;

  /**
   * @attribute syn
   * @aspect TypeAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:62
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:62")
  @SideEffect.Pure(group="BoolType") public BoolType BoolType() {
    ASTState state = state();
    if (BoolType_computed) {
      return BoolType_value;
    }
    if (BoolType_visited) {
      throw new RuntimeException("Circular definition of attribute Program.BoolType().");
    }
    BoolType_visited = true;
    state().enterLazyAttribute();
    BoolType_value = new BoolType();
    BoolType_value.setParent(this);
    BoolType_computed = true;
    state().leaveLazyAttribute();
    BoolType_visited = false;
    return BoolType_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="IntType") protected boolean IntType_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void IntType_reset() {
    IntType_computed = false;
    
    IntType_value = null;
    IntType_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="IntType") protected boolean IntType_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="IntType") protected IntType IntType_value;

  /**
   * @attribute syn
   * @aspect TypeAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:63
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:63")
  @SideEffect.Pure(group="IntType") public IntType IntType() {
    ASTState state = state();
    if (IntType_computed) {
      return IntType_value;
    }
    if (IntType_visited) {
      throw new RuntimeException("Circular definition of attribute Program.IntType().");
    }
    IntType_visited = true;
    state().enterLazyAttribute();
    IntType_value = new IntType();
    IntType_value.setParent(this);
    IntType_computed = true;
    state().leaveLazyAttribute();
    IntType_visited = false;
    return IntType_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="UnknownType") protected boolean UnknownType_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void UnknownType_reset() {
    UnknownType_computed = false;
    
    UnknownType_value = null;
    UnknownType_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="UnknownType") protected boolean UnknownType_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="UnknownType") protected UnknownType UnknownType_value;

  /**
   * @attribute syn
   * @aspect TypeAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:64
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:64")
  @SideEffect.Pure(group="UnknownType") public UnknownType UnknownType() {
    ASTState state = state();
    if (UnknownType_computed) {
      return UnknownType_value;
    }
    if (UnknownType_visited) {
      throw new RuntimeException("Circular definition of attribute Program.UnknownType().");
    }
    UnknownType_visited = true;
    state().enterLazyAttribute();
    UnknownType_value = new UnknownType();
    UnknownType_value.setParent(this);
    UnknownType_computed = true;
    state().leaveLazyAttribute();
    UnknownType_visited = false;
    return UnknownType_value;
  }
  /**
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Errors.jrag:28
   * @apilevel internal
   */
 @SideEffect.Pure public Program Define_program(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return this;
  }
  @SideEffect.Pure protected boolean canDefine_program(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:5
   * @apilevel internal
   */
 @SideEffect.Pure public UnknownDecl Define_UnknownDecl(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return UnknownDecl();
  }
  @SideEffect.Pure protected boolean canDefine_UnknownDecl(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:10
   * @apilevel internal
   */
 @SideEffect.Pure public ASTNode Define_lookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    int childIndex = this.getIndexOfChild(_callerNode);
    {
    
    		for (int i = 0; i < getNumFunction(); i++) {
    			if(getFunction(i).getID().equals(name)) 
    			return getFunction(i);
    		}
    
    		List<Function> l =predefinedFunctions();
    		for(int i = 0; i< l.getNumChild() ; i++)
    		{
    			if(l.getChild(i).getID().equals(name)) 
    			return l.getChild(i);
    		}
    	return UnknownDecl();
    	}
  }
  @SideEffect.Pure protected boolean canDefine_lookup(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:58
   * @apilevel internal
   */
 @SideEffect.Pure public Type Define_BoolType(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return BoolType();
  }
  @SideEffect.Pure protected boolean canDefine_BoolType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:59
   * @apilevel internal
   */
 @SideEffect.Pure public Type Define_IntType(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return IntType();
  }
  @SideEffect.Pure protected boolean canDefine_IntType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:60
   * @apilevel internal
   */
 @SideEffect.Pure public Type Define_UnknownType(ASTNode _callerNode, ASTNode _childNode) {
    int childIndex = this.getIndexOfChild(_callerNode);
    return UnknownType();
  }
  @SideEffect.Pure protected boolean canDefine_UnknownType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
/** @apilevel internal */
@SideEffect.Secret(group="Program_errors") protected boolean Program_errors_visited = false;
  /**
   * @attribute coll
   * @aspect Errors
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Errors.jrag:26
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.COLL)
  @ASTNodeAnnotation.Source(aspect="Errors", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Errors.jrag:26")
  @SideEffect.Pure(group="Program_errors") public Set<ErrorMessage> errors() {
    ASTState state = state();
    if (Program_errors_computed) {
      return Program_errors_value;
    }
    if (Program_errors_visited) {
      throw new RuntimeException("Circular definition of attribute Program.errors().");
    }
    Program_errors_visited = true;
    state().enterLazyAttribute();
    Program_errors_value = errors_compute();
    Program_errors_computed = true;
    state().leaveLazyAttribute();
    Program_errors_visited = false;
    return Program_errors_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="Program_errors") private Set<ErrorMessage> errors_compute() {
    ASTNode node = this;
    while (node != null && !(node instanceof Program)) {
      node = node.getParent();
    }
    Program root = (Program) node;
    root.survey_Program_errors();
    Set<ErrorMessage> _computedValue = new TreeSet<ErrorMessage>();
    if (root.contributorMap_Program_errors.containsKey(this)) {
      for (ASTNode contributor : root.contributorMap_Program_errors.get(this)) {
        contributor.contributeTo_Program_errors(_computedValue);
      }
    }
    return _computedValue;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="Program_errors") protected boolean Program_errors_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="Program_errors") protected Set<ErrorMessage> Program_errors_value;

}

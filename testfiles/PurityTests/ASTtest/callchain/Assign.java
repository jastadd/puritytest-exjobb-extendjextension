/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package lang.ast;
import java.util.*;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
/**
 * @ast node
 * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Simplic.ast:14
 * @production Assign : {@link Stmt} ::= <span class="component">{@link IDExpr}</span> <span class="component">{@link Expr}</span>;

 */
public class Assign extends Expr  {
 
  @ASTNodeAnnotation.Child(name="IDExpr")
  @SideEffect.Pure public Assign getIDExpr() {
    return (Assign) getChild(0);
  }
  
  @SideEffect.Pure(Ignore=true) public Assign getChild(int i) {
	    return this;
	  }
  
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="FunctionCallsAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\FunctionCallsAnalysis.jrag:70")
  @SideEffect.Pure public Assign decl() {
	    return (Assign) getChild(0);
	  }  
  

  @SideEffect.Pure(group="_ASTNode") protected Assign contributeTo_Program_errors(Set<ErrorMessage> collection) {
    if (getIDExpr().decl()==this) {
    	return this;
    }
  }
}

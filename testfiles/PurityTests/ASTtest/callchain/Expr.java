/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package lang.ast;
import java.util.*;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
/**
 * @ast node
 * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Simplic.ast:23
 * @production Expr : {@link ASTNode};

 */
public @SideEffect.Entity abstract class Expr extends ASTNode<ASTNode> implements Cloneable {
  public Expr() {
    super();
  }
}

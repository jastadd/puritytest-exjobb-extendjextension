/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package lang.ast;
import java.util.*;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
/**
 * @ast node
 * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Simplic.ast:36
 * @production IDExpr : {@link Expr} ::= <span class="component">&lt;ID:String&gt;</span>;

 */
public class IDExpr extends Expr implements Cloneable {
 
  public IDExpr() {
    super();
  }

@SideEffect.Secret(group="decl") protected boolean decl_visited = false;
  /**
   * @attribute syn
   * @aspect NameAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:9
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\NameAnalysis.jrag:9")
  @SideEffect.Pure(group="decl") public ASTNode decl() {
    decl_visited = true;
    ASTNode decl_value = this;
    decl_visited = false;
    return decl_value;
  }

}

/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
/**
 * @ast node
 * @declaredat tests\\nta\\basic02\\Test.ast:2
 * @production NodeB : {@link ASTNode};

 */
public class NodeB extends ASTNode<ASTNode> implements Cloneable {
	/**
	 * @aspect Test
	 * @declaredat tests\\nta\\basic02\\Test.jrag:5
	 */
	@SideEffect.Fresh @SideEffect.Local public B theB = new B();
	/**
	 * @declaredat ASTNode:1
	 */
	public NodeB() {
		super();
	}
	/**
	 * Initializes the child array to the correct size.
	 * Initializes List and Opt nta children.
	 * @apilevel internal
	 * @ast method
	 * @declaredat ASTNode:10
	 */
	@SideEffect.Ignore public void init$Children() {
	}
	/** @apilevel low-level 
	 * @declaredat ASTNode:13
	 */
	@SideEffect.Pure protected int numChildren() {
		return 0;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:17
	 */
	@SideEffect.Ignore public void flushAttrCache() {
		super.flushAttrCache();
		getNtaB_reset();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:22
	 */
	@SideEffect.Ignore public void flushCollectionCache() {
		super.flushCollectionCache();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:26
	 */
	@SideEffect.Fresh public NodeB clone() throws CloneNotSupportedException {
		NodeB node = (NodeB) super.clone();
		return node;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:31
	 */
	@SideEffect.Fresh(group="_ASTNode") public NodeB copy() {
		try {
			NodeB node = (NodeB) clone();
			node.parent = null;
			if (children != null) {
				node.children = (ASTNode[]) children.clone();
			}
			return node;
		} catch (CloneNotSupportedException e) {
			throw new Error("Error: clone not supported for " + getClass().getName());
		}
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @deprecated Please use treeCopy or treeCopyNoTransform instead
	 * @declaredat ASTNode:50
	 */
	@Deprecated
	@SideEffect.Fresh(group="_ASTNode") public NodeB fullCopy() {
		return treeCopyNoTransform();
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:60
	 */
	@SideEffect.Fresh(group="_ASTNode") public NodeB treeCopyNoTransform() {
		NodeB tree = (NodeB) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) children[i];
				if (child != null) {
					child = child.treeCopyNoTransform();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The subtree of this node is traversed to trigger rewrites before copy.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:80
	 */
	@SideEffect.Fresh(group="_ASTNode") public NodeB treeCopy() {
		NodeB tree = (NodeB) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) getChild(i);
				if (child != null) {
					child = child.treeCopy();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:94
	 */
	@SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
		return super.is$Equal(node);    
	}
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean getNtaB_visited = false;
	/** @apilevel internal */
	@SideEffect.Ignore private void getNtaB_reset() {
		getNtaB_computed = false;
		
		getNtaB_value = null;
		getNtaB_visited = false;
	}
	/** @apilevel internal */
	@SideEffect.Secret(group="_ASTNode") protected boolean getNtaB_computed = false;
	/** @apilevel internal */
	@SideEffect.Secret(group="_ASTNode") protected B getNtaB_value;

	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\nta\\basic02\\Test.jrag:6
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\nta\\basic02\\Test.jrag:6")
	@SideEffect.Pure(group="_ASTNode") public B getNtaB() {
		ASTState state = state();
		if (getNtaB_computed) {
			return getNtaB_value;
		}
		if (getNtaB_visited) {
			throw new RuntimeException("Circular definition of attribute NodeB.getNtaB().");
		}
		getNtaB_visited = true;
		state().enterLazyAttribute();
		getNtaB_value = getNtaB_compute();
		getNtaB_value.setParent(this);
		getNtaB_computed = true;
		state().leaveLazyAttribute();
		getNtaB_visited = false;
		return getNtaB_value;
	}
	/** @apilevel internal */
	@SideEffect.Fresh private B getNtaB_compute() {
			return theB;
		}
}

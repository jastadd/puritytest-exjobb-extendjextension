/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
/**
 * @ast node
 * @declaredat tests\\nta\\basic02\\Test.ast:1
 * @production NodeA : {@link ASTNode};

 */
public class NodeA extends ASTNode<ASTNode> implements Cloneable {
	/**
	 * @aspect Test
	 * @declaredat tests\\nta\\basic02\\Test.jrag:2
	 */
	@SideEffect.Fresh public A theA = new A();
	/**
	 * @declaredat ASTNode:1
	 */
	public NodeA() {
		super();
	}
	/**
	 * Initializes the child array to the correct size.
	 * Initializes List and Opt nta children.
	 * @apilevel internal
	 * @ast method
	 * @declaredat ASTNode:10
	 */
	@SideEffect.Ignore public void init$Children() {
	}
	/** @apilevel low-level 
	 * @declaredat ASTNode:13
	 */
	@SideEffect.Pure protected int numChildren() {
		return 0;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:17
	 */
	@SideEffect.Ignore public void flushAttrCache() {
		super.flushAttrCache();
		getA_reset();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:22
	 */
	@SideEffect.Ignore public void flushCollectionCache() {
		super.flushCollectionCache();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:26
	 */
	@SideEffect.Fresh public NodeA clone() throws CloneNotSupportedException {
		NodeA node = (NodeA) super.clone();
		return node;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:31
	 */
	@SideEffect.Fresh(group="_ASTNode") public NodeA copy() {
		try {
			NodeA node = (NodeA) clone();
			node.parent = null;
			if (children != null) {
				node.children = (ASTNode[]) children.clone();
			}
			return node;
		} catch (CloneNotSupportedException e) {
			throw new Error("Error: clone not supported for " + getClass().getName());
		}
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @deprecated Please use treeCopy or treeCopyNoTransform instead
	 * @declaredat ASTNode:50
	 */
	@Deprecated
	@SideEffect.Fresh(group="_ASTNode") public NodeA fullCopy() {
		return treeCopyNoTransform();
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:60
	 */
	@SideEffect.Fresh(group="_ASTNode") public NodeA treeCopyNoTransform() {
		NodeA tree = (NodeA) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) children[i];
				if (child != null) {
					child = child.treeCopyNoTransform();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The subtree of this node is traversed to trigger rewrites before copy.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:80
	 */
	@SideEffect.Fresh(group="_ASTNode") public NodeA treeCopy() {
		NodeA tree = (NodeA) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) getChild(i);
				if (child != null) {
					child = child.treeCopy();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:94
	 */
	@SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
		return super.is$Equal(node);    
	}
/** @apilevel internal */
@SideEffect.Secret(group="_ASTNode") protected boolean getA_visited = false;
	/** @apilevel internal */
	@SideEffect.Ignore private void getA_reset() {
		getA_computed = false;
		
		getA_value = null;
		getA_visited = false;
	}
	/** @apilevel internal */
	@SideEffect.Secret(group="_ASTNode") protected boolean getA_computed = false;
	/** @apilevel internal */
	@SideEffect.Secret(group="_ASTNode") protected A getA_value;

	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\nta\\basic02\\Test.jrag:3
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\nta\\basic02\\Test.jrag:3")
	@SideEffect.Pure(group="_ASTNode") public A getA() {
		ASTState state = state();
		if (getA_computed) {
			return getA_value;
		}
		if (getA_visited) {
			throw new RuntimeException("Circular definition of attribute NodeA.getA().");
		}
		getA_visited = true;
		state().enterLazyAttribute();
		getA_value = getAp();
		getA_value.setParent(this);
		getA_computed = true;
		state().leaveLazyAttribute();
		getA_visited = false;
		return getA_value;
	}
	
	@SideEffect.Fresh() public A getAp() {
		return theA;
	}
	
	@SideEffect.Pure public A getApZ(){
		thaA=this;
		return theA;
	}
}

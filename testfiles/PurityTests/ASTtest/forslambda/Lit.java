/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package lang.ast;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.*;
import lang.ast.SideEffect.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\lang.ast:6
 * @production Lit : {@link Expr} ::= <span class="component">&lt;Value:int&gt;</span>;

 */
public class Lit extends Expr implements Cloneable {
  /**
   * @aspect Substitution
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:25
   */
  @SideEffect.Fresh public Expr compSubst(String x,@Fresh Expr s) {
		return Lit(getValue());
	}
  /**
   * @aspect Evaluation
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:43
   */
  @SideEffect.Fresh public Expr compEval() {
		return Lit(getValue());
	}
  /**
   * @declaredat ASTNode:1
   */
  public Lit() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /**
   * @declaredat ASTNode:12
   */
  public Lit(int p0) {
    setValue(p0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:16
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:20
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    pp_reset();
    prio_reset();
    fv_reset();
    isLit_reset();
    litValue_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:29
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:33
   */
  @SideEffect.Fresh public Lit clone() throws CloneNotSupportedException {
    Lit node = (Lit) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  @SideEffect.Fresh(group="_ASTNode") public Lit copy() {
    try {
      Lit node = (Lit) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:57
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Lit fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:67
   */
  @SideEffect.Fresh(group="_ASTNode") public Lit treeCopyNoTransform() {
    Lit tree = (Lit) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:87
   */
  @SideEffect.Fresh(group="_ASTNode") public Lit treeCopy() {
    Lit tree = (Lit) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:101
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenint_Value == ((Lit) node).tokenint_Value);    
  }
  /**
   * Replaces the lexeme Value.
   * @param value The new value for the lexeme Value.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setValue(int value) {
    tokenint_Value = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected int tokenint_Value;
  /**
   * Retrieves the value for the lexeme Value.
   * @return The value for the lexeme Value.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Value")
  @SideEffect.Pure(group="_ASTNode") public int getValue() {
    return tokenint_Value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="pp") protected boolean pp_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void pp_reset() {
    pp_computed = false;
    
    pp_value = null;
    pp_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="pp") protected boolean pp_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="pp") protected String pp_value;

  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\PrettyPrint.jrag:2
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="PrettyPrint", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\PrettyPrint.jrag:2")
  @SideEffect.Pure(group="pp") public String pp() {
    ASTState state = state();
    if (pp_computed) {
      return pp_value;
    }
    if (pp_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.pp().");
    }
    pp_visited = true;
    state().enterLazyAttribute();
    pp_value = "" + getValue();
    pp_computed = true;
    state().leaveLazyAttribute();
    pp_visited = false;
    return pp_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="prio") protected boolean prio_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void prio_reset() {
    prio_computed = false;
    prio_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="prio") protected boolean prio_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="prio") protected int prio_value;

  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\PrettyPrint.jrag:21
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="PrettyPrint", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\PrettyPrint.jrag:21")
  @SideEffect.Pure(group="prio") public int prio() {
    ASTState state = state();
    if (prio_computed) {
      return prio_value;
    }
    if (prio_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.prio().");
    }
    prio_visited = true;
    state().enterLazyAttribute();
    prio_value = 1;
    prio_computed = true;
    state().leaveLazyAttribute();
    prio_visited = false;
    return prio_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="fv") protected boolean fv_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void fv_reset() {
    fv_computed = false;
    
    fv_value = null;
    fv_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="fv") protected boolean fv_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="fv") protected Set<String> fv_value;

  /**
   * @attribute syn
   * @aspect FreeVariables
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:6
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="FreeVariables", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:6")
  @SideEffect.Pure(group="fv") public Set<String> fv() {
    ASTState state = state();
    if (fv_computed) {
      return fv_value;
    }
    if (fv_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.fv().");
    }
    fv_visited = true;
    state().enterLazyAttribute();
    fv_value = Collections.emptySet();
    fv_computed = true;
    state().leaveLazyAttribute();
    fv_visited = false;
    return fv_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="isLit") protected boolean isLit_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void isLit_reset() {
    isLit_computed = false;
    isLit_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="isLit") protected boolean isLit_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="isLit") protected boolean isLit_value;

  /**
   * @attribute syn
   * @aspect Evaluation
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:62
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Evaluation", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:62")
  @SideEffect.Pure(group="isLit") public boolean isLit() {
    ASTState state = state();
    if (isLit_computed) {
      return isLit_value;
    }
    if (isLit_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.isLit().");
    }
    isLit_visited = true;
    state().enterLazyAttribute();
    isLit_value = true;
    isLit_computed = true;
    state().leaveLazyAttribute();
    isLit_visited = false;
    return isLit_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="litValue") protected boolean litValue_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void litValue_reset() {
    litValue_computed = false;
    litValue_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="litValue") protected boolean litValue_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="litValue") protected int litValue_value;

  /**
   * @attribute syn
   * @aspect Evaluation
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:64
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Evaluation", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:64")
  @SideEffect.Pure(group="litValue") public int litValue() {
    ASTState state = state();
    if (litValue_computed) {
      return litValue_value;
    }
    if (litValue_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.litValue().");
    }
    litValue_visited = true;
    state().enterLazyAttribute();
    litValue_value = getValue();
    litValue_computed = true;
    state().leaveLazyAttribute();
    litValue_visited = false;
    return litValue_value;
  }
}

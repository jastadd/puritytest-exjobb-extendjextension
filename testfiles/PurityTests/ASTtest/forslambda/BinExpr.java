/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package lang.ast;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.*;
import lang.ast.SideEffect.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\lang.ast:7
 * @production BinExpr : {@link Expr} ::= <span class="component">Left:{@link Expr}</span> <span class="component">Right:{@link Expr}</span>;

 */
public abstract class BinExpr extends Expr implements Cloneable {
  /**
   * @aspect Substitution
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:28
   */
  @SideEffect.Fresh public Expr compSubst(String x,@Fresh Expr s) {
		return newBinExpr(getLeft().compSubst(x, s), getRight().compSubst(x, s));
	}
  /**
   * @aspect Evaluation
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:46
   */
  @SideEffect.Fresh public Expr compEval() {
		return compEval(getLeft().compEval(), getRight().compEval());
	}
  /**
   * @aspect Evaluation
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:49
   */
  @SideEffect.Fresh public abstract Expr compEval(Expr l, Expr r);
  /**
   * @aspect ConstantFunctionElimination
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:86
   */
  @SideEffect.Fresh public Expr compFuncElim() {
		return newBinExpr(getLeft().compFuncElim(), getRight().compFuncElim());
	}
  /**
   * @aspect ConstructorMethods
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Util.jadd:8
   */
  @SideEffect.Fresh public abstract BinExpr newBinExpr(Expr l, Expr r);
  /**
   * @declaredat ASTNode:1
   */
  public BinExpr() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
  }
  /**
   * @declaredat ASTNode:13
   */
  public BinExpr(Expr p0, Expr p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:18
   */
  @SideEffect.Pure protected int numChildren() {
    return 2;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:22
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    pp_reset();
    fv_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:32
   */
  @SideEffect.Fresh public BinExpr clone() throws CloneNotSupportedException {
    BinExpr node = (BinExpr) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:43
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public abstract BinExpr fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:51
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract BinExpr treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:59
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract BinExpr treeCopy();
  /**
   * Replaces the Left child.
   * @param node The new node to replace the Left child.
   * @apilevel high-level
   */
  public void setLeft(Expr node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Left child.
   * @return The current node used as the Left child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Left")
  @SideEffect.Pure public Expr getLeft() {
    return (Expr) getChild(0);
  }
  /**
   * Retrieves the Left child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Left child.
   * @apilevel low-level
   */
  @SideEffect.Pure public Expr getLeftNoTransform() {
    return (Expr) getChildNoTransform(0);
  }
  /**
   * Replaces the Right child.
   * @param node The new node to replace the Right child.
   * @apilevel high-level
   */
  public void setRight(Expr node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the Right child.
   * @return The current node used as the Right child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Right")
  @SideEffect.Pure public Expr getRight() {
    return (Expr) getChild(1);
  }
  /**
   * Retrieves the Right child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Right child.
   * @apilevel low-level
   */
  @SideEffect.Pure public Expr getRightNoTransform() {
    return (Expr) getChildNoTransform(1);
  }
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\PrettyPrint.jrag:17
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="PrettyPrint", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\PrettyPrint.jrag:17")
  @SideEffect.Pure(group="binOpSymbol") public abstract String binOpSymbol();
/** @apilevel internal */
@SideEffect.Secret(group="pp") protected boolean pp_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void pp_reset() {
    pp_computed = false;
    
    pp_value = null;
    pp_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="pp") protected boolean pp_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="pp") protected String pp_value;

  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\PrettyPrint.jrag:2
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="PrettyPrint", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\PrettyPrint.jrag:2")
  @SideEffect.Pure(group="pp") public String pp() {
    ASTState state = state();
    if (pp_computed) {
      return pp_value;
    }
    if (pp_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.pp().");
    }
    pp_visited = true;
    state().enterLazyAttribute();
    pp_value = pp_compute();
    pp_computed = true;
    state().leaveLazyAttribute();
    pp_visited = false;
    return pp_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private String pp_compute() {
  		String left = getLeft().pp();
  		if (getLeft().prio() >= prio()) {
  			left = "(" + left + ")";
  		}
  		String right = getRight().pp();
  		if (getRight().prio() > prio()) {
  			right = "(" + right + ")";
  		}
  		return left + binOpSymbol() + right;
  	}
/** @apilevel internal */
@SideEffect.Secret(group="fv") protected boolean fv_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void fv_reset() {
    fv_computed = false;
    
    fv_value = null;
    fv_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="fv") protected boolean fv_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="fv") protected Set<String> fv_value;

  /**
   * @attribute syn
   * @aspect FreeVariables
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:6
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="FreeVariables", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:6")
  @SideEffect.Pure(group="fv") public Set<String> fv() {
    ASTState state = state();
    if (fv_computed) {
      return fv_value;
    }
    if (fv_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.fv().");
    }
    fv_visited = true;
    state().enterLazyAttribute();
    fv_value = union(getLeft().fv(), getRight().fv());
    fv_computed = true;
    state().leaveLazyAttribute();
    fv_visited = false;
    return fv_value;
  }
}

/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package lang.ast;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.*;
import lang.ast.SideEffect.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\lang.ast:9
 * @production Sub : {@link BinExpr};

 */
public class Sub extends BinExpr implements Cloneable {
  /**
   * @aspect Evaluation
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:53
   */
  @SideEffect.Fresh public Expr compEval(Expr l, Expr r) {
		return l.isLit() && r.isLit()
			? Lit(l.litValue() - r.litValue())
			: Sub(l, r);
	}
  /**
   * @aspect ConstructorMethods
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Util.jadd:10
   */
  @SideEffect.Fresh public Sub newBinExpr(Expr l, Expr r) { return Sub(l, r); }
  /**
   * @declaredat ASTNode:1
   */
  public Sub() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
  }
  /**
   * @declaredat ASTNode:13
   */
  public Sub(Expr p0, Expr p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:18
   */
  @SideEffect.Pure protected int numChildren() {
    return 2;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:22
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    binOpSymbol_reset();
    prio_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:32
   */
  @SideEffect.Fresh public Sub clone() throws CloneNotSupportedException {
    Sub node = (Sub) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  @SideEffect.Fresh(group="_ASTNode") public Sub copy() {
    try {
      Sub node = (Sub) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:56
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Sub fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:66
   */
  @SideEffect.Fresh(group="_ASTNode") public Sub treeCopyNoTransform() {
    Sub tree = (Sub) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:86
   */
  @SideEffect.Fresh(group="_ASTNode") public Sub treeCopy() {
    Sub tree = (Sub) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:100
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the Left child.
   * @param node The new node to replace the Left child.
   * @apilevel high-level
   */
  public void setLeft(Expr node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Left child.
   * @return The current node used as the Left child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Left")
  @SideEffect.Pure public Expr getLeft() {
    return (Expr) getChild(0);
  }
  /**
   * Retrieves the Left child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Left child.
   * @apilevel low-level
   */
  @SideEffect.Pure public Expr getLeftNoTransform() {
    return (Expr) getChildNoTransform(0);
  }
  /**
   * Replaces the Right child.
   * @param node The new node to replace the Right child.
   * @apilevel high-level
   */
  public void setRight(Expr node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the Right child.
   * @return The current node used as the Right child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Right")
  @SideEffect.Pure public Expr getRight() {
    return (Expr) getChild(1);
  }
  /**
   * Retrieves the Right child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Right child.
   * @apilevel low-level
   */
  @SideEffect.Pure public Expr getRightNoTransform() {
    return (Expr) getChildNoTransform(1);
  }
/** @apilevel internal */
@SideEffect.Secret(group="binOpSymbol") protected boolean binOpSymbol_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void binOpSymbol_reset() {
    binOpSymbol_computed = false;
    
    binOpSymbol_value = null;
    binOpSymbol_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="binOpSymbol") protected boolean binOpSymbol_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="binOpSymbol") protected String binOpSymbol_value;

  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\PrettyPrint.jrag:17
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="PrettyPrint", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\PrettyPrint.jrag:17")
  @SideEffect.Pure(group="binOpSymbol") public String binOpSymbol() {
    ASTState state = state();
    if (binOpSymbol_computed) {
      return binOpSymbol_value;
    }
    if (binOpSymbol_visited) {
      throw new RuntimeException("Circular definition of attribute BinExpr.binOpSymbol().");
    }
    binOpSymbol_visited = true;
    state().enterLazyAttribute();
    binOpSymbol_value = "-";
    binOpSymbol_computed = true;
    state().leaveLazyAttribute();
    binOpSymbol_visited = false;
    return binOpSymbol_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="prio") protected boolean prio_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void prio_reset() {
    prio_computed = false;
    prio_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="prio") protected boolean prio_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="prio") protected int prio_value;

  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\PrettyPrint.jrag:21
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="PrettyPrint", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\PrettyPrint.jrag:21")
  @SideEffect.Pure(group="prio") public int prio() {
    ASTState state = state();
    if (prio_computed) {
      return prio_value;
    }
    if (prio_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.prio().");
    }
    prio_visited = true;
    state().enterLazyAttribute();
    prio_value = 3;
    prio_computed = true;
    state().leaveLazyAttribute();
    prio_visited = false;
    return prio_value;
  }
}

/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package lang.ast;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.*;
import lang.ast.SideEffect.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\lang.ast:3
 * @production Expr : {@link ASTNode};

 */
public abstract class Expr extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect Substitution
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:16
   */
  @SideEffect.Fresh public abstract Expr compSubst(String x, @Local Expr s);
  /**
   * @aspect Evaluation
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:36
   */
  @SideEffect.Fresh public abstract Expr compEval();
  /**
   * @aspect Evaluation
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:66
   */
  @SideEffect.Fresh public Expr evalAbs(Expr s) {
		return null;
	}
  /**
   * @aspect ConstantFunctionElimination
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:78
   */
  @SideEffect.Fresh public Expr compFuncElim() {
		return treeCopy();
	}
  /**
   * @declaredat ASTNode:1
   */
  public Expr() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:17
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    subst_String_Expr_reset();
    eval_reset();
    isAbs_reset();
    isLit_reset();
    litValue_reset();
    funcElim_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:27
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:31
   */
  @SideEffect.Fresh public Expr clone() throws CloneNotSupportedException {
    Expr node = (Expr) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:42
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public abstract Expr fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:50
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract Expr treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:58
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract Expr treeCopy();
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\PrettyPrint.jrag:2
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="PrettyPrint", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\PrettyPrint.jrag:2")
  @SideEffect.Pure(group="pp") public abstract String pp();
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\PrettyPrint.jrag:21
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="PrettyPrint", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\PrettyPrint.jrag:21")
  @SideEffect.Pure(group="prio") public abstract int prio();
  /**
   * @attribute syn
   * @aspect FreeVariables
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:6
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="FreeVariables", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:6")
  @SideEffect.Pure(group="fv") public abstract Set<String> fv();
/** @apilevel internal */
@SideEffect.Secret(group="subst_String_Expr") protected java.util.Set subst_String_Expr_visited;
  /** @apilevel internal */
  @SideEffect.Ignore private void subst_String_Expr_reset() {
    subst_String_Expr_values = null;
    subst_String_Expr_proxy = null;
    subst_String_Expr_visited = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="subst_String_Expr") protected ASTNode subst_String_Expr_proxy;
  /** @apilevel internal */
   @SideEffect.Secret(group="subst_String_Expr") protected java.util.Map subst_String_Expr_values;

  /**
   * @attribute syn
   * @aspect Substitution
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:14
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Substitution", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:14")
  @SideEffect.Pure(group="subst_String_Expr") public Expr subst(String x,@Fresh Expr s) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(x);
    _parameters.add(s);
    if (subst_String_Expr_visited == null) subst_String_Expr_visited = new java.util.HashSet(4);
    if (subst_String_Expr_values == null) subst_String_Expr_values = new java.util.HashMap(4);
    ASTState state = state();
    if (subst_String_Expr_values.containsKey(_parameters)) {
      return (Expr) subst_String_Expr_values.get(_parameters);
    }
    if (subst_String_Expr_visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute Expr.subst(String,Expr).");
    }
    subst_String_Expr_visited.add(_parameters);
    state().enterLazyAttribute();
    Expr subst_String_Expr_value = compSubst(x, s);
    if (subst_String_Expr_proxy == null) {
      subst_String_Expr_proxy = new ASTNode();
      subst_String_Expr_proxy.setParent(this);
    }
    if (subst_String_Expr_value != null) {
      subst_String_Expr_value.setParent(subst_String_Expr_proxy);
    }
    subst_String_Expr_values.put(_parameters, subst_String_Expr_value);
    state().leaveLazyAttribute();
    subst_String_Expr_visited.remove(_parameters);
    return subst_String_Expr_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="eval") protected boolean eval_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void eval_reset() {
    eval_computed = false;
    
    eval_value = null;
    eval_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="eval") protected boolean eval_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="eval") protected Expr eval_value;

  /**
   * @attribute syn
   * @aspect Evaluation
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:34
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Evaluation", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:34")
  @SideEffect.Pure(group="eval") public Expr eval() {
    ASTState state = state();
    if (eval_computed) {
      return eval_value;
    }
    if (eval_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.eval().");
    }
    eval_visited = true;
    state().enterLazyAttribute();
    eval_value = compEval();
    eval_value.setParent(this);
    eval_computed = true;
    state().leaveLazyAttribute();
    eval_visited = false;
    return eval_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="isAbs") protected boolean isAbs_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void isAbs_reset() {
    isAbs_computed = false;
    isAbs_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="isAbs") protected boolean isAbs_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="isAbs") protected boolean isAbs_value;

  /**
   * @attribute syn
   * @aspect Evaluation
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:60
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Evaluation", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:60")
  @SideEffect.Pure(group="isAbs") public boolean isAbs() {
    ASTState state = state();
    if (isAbs_computed) {
      return isAbs_value;
    }
    if (isAbs_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.isAbs().");
    }
    isAbs_visited = true;
    state().enterLazyAttribute();
    isAbs_value = false;
    isAbs_computed = true;
    state().leaveLazyAttribute();
    isAbs_visited = false;
    return isAbs_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="isLit") protected boolean isLit_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void isLit_reset() {
    isLit_computed = false;
    isLit_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="isLit") protected boolean isLit_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="isLit") protected boolean isLit_value;

  /**
   * @attribute syn
   * @aspect Evaluation
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:62
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Evaluation", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:62")
  @SideEffect.Pure(group="isLit") public boolean isLit() {
    ASTState state = state();
    if (isLit_computed) {
      return isLit_value;
    }
    if (isLit_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.isLit().");
    }
    isLit_visited = true;
    state().enterLazyAttribute();
    isLit_value = false;
    isLit_computed = true;
    state().leaveLazyAttribute();
    isLit_visited = false;
    return isLit_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="litValue") protected boolean litValue_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void litValue_reset() {
    litValue_computed = false;
    litValue_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="litValue") protected boolean litValue_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="litValue") protected int litValue_value;

  /**
   * @attribute syn
   * @aspect Evaluation
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:64
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Evaluation", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:64")
  @SideEffect.Pure(group="litValue") public int litValue() {
    ASTState state = state();
    if (litValue_computed) {
      return litValue_value;
    }
    if (litValue_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.litValue().");
    }
    litValue_visited = true;
    state().enterLazyAttribute();
    litValue_value = -1;
    litValue_computed = true;
    state().leaveLazyAttribute();
    litValue_visited = false;
    return litValue_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="funcElim") protected boolean funcElim_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void funcElim_reset() {
    funcElim_computed = false;
    
    funcElim_value = null;
    funcElim_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="funcElim") protected boolean funcElim_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="funcElim") protected Expr funcElim_value;

  /**
   * @attribute syn
   * @aspect ConstantFunctionElimination
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:76
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="ConstantFunctionElimination", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:76")
  @SideEffect.Pure(group="funcElim") public Expr funcElim() {
    ASTState state = state();
    if (funcElim_computed) {
      return funcElim_value;
    }
    if (funcElim_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.funcElim().");
    }
    funcElim_visited = true;
    state().enterLazyAttribute();
    funcElim_value = compFuncElim();
    funcElim_value.setParent(this);
    funcElim_computed = true;
    state().leaveLazyAttribute();
    funcElim_visited = false;
    return funcElim_value;
  }
}

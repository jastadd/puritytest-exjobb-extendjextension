/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package lang.ast;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.*;
import lang.ast.SideEffect.*;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\lang.ast:4
 * @production Var : {@link Expr} ::= <span class="component">&lt;ID:String&gt;</span>;

 */
public class Var extends Expr implements Cloneable {
  /**
   * @aspect Substitution
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:17
   */
  @SideEffect.Fresh public Expr compSubst(String x, @Fresh Expr s) {
		return getID().equals(x) ? s : Var(getID());
	}
  /**
   * @aspect Evaluation
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:37
   */
  @SideEffect.Fresh public Expr compEval() { 
		return Var(getID()); 
	}
  /**
   * @declaredat ASTNode:1
   */
  public Var() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /**
   * @declaredat ASTNode:12
   */
  public Var(String p0) {
    setID(p0);
  }
  /**
   * @declaredat ASTNode:15
   */
  public Var(beaver.Symbol p0) {
    setID(p0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:19
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:23
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    pp_reset();
    prio_reset();
    fv_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:30
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:34
   */
  @SideEffect.Fresh public Var clone() throws CloneNotSupportedException {
    Var node = (Var) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  @SideEffect.Fresh(group="_ASTNode") public Var copy() {
    try {
      Var node = (Var) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:58
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Var fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:68
   */
  @SideEffect.Fresh(group="_ASTNode") public Var treeCopyNoTransform() {
    Var tree = (Var) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:88
   */
  @SideEffect.Fresh(group="_ASTNode") public Var treeCopy() {
    Var tree = (Var) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:102
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_ID == ((Var) node).tokenString_ID);    
  }
  /**
   * Replaces the lexeme ID.
   * @param value The new value for the lexeme ID.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setID(String value) {
    tokenString_ID = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_ID;
  /**
   */
  public int IDstart;
  /**
   */
  public int IDend;
  /**
   * JastAdd-internal setter for lexeme ID using the Beaver parser.
   * @param symbol Symbol containing the new value for the lexeme ID
   * @apilevel internal
   */
  @SideEffect.Local(group="_ASTNode") public void setID(beaver.Symbol symbol) {
    if (symbol.value != null && !(symbol.value instanceof String))
    throw new UnsupportedOperationException("setID is only valid for String lexemes");
    tokenString_ID = (String)symbol.value;
    IDstart = symbol.getStart();
    IDend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme ID.
   * @return The value for the lexeme ID.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="ID")
  @SideEffect.Pure(group="_ASTNode") public String getID() {
    return tokenString_ID != null ? tokenString_ID : "";
  }
/** @apilevel internal */
@SideEffect.Secret(group="pp") protected boolean pp_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void pp_reset() {
    pp_computed = false;
    
    pp_value = null;
    pp_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="pp") protected boolean pp_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="pp") protected String pp_value;

  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\PrettyPrint.jrag:2
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="PrettyPrint", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\PrettyPrint.jrag:2")
  @SideEffect.Pure(group="pp") public String pp() {
    ASTState state = state();
    if (pp_computed) {
      return pp_value;
    }
    if (pp_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.pp().");
    }
    pp_visited = true;
    state().enterLazyAttribute();
    pp_value = getID();
    pp_computed = true;
    state().leaveLazyAttribute();
    pp_visited = false;
    return pp_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="prio") protected boolean prio_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void prio_reset() {
    prio_computed = false;
    prio_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="prio") protected boolean prio_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="prio") protected int prio_value;

  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\PrettyPrint.jrag:21
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="PrettyPrint", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\PrettyPrint.jrag:21")
  @SideEffect.Pure(group="prio") public int prio() {
    ASTState state = state();
    if (prio_computed) {
      return prio_value;
    }
    if (prio_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.prio().");
    }
    prio_visited = true;
    state().enterLazyAttribute();
    prio_value = 1;
    prio_computed = true;
    state().leaveLazyAttribute();
    prio_visited = false;
    return prio_value;
  }
/** @apilevel internal */
@SideEffect.Secret(group="fv") protected boolean fv_visited = false;
  /** @apilevel internal */
  @SideEffect.Ignore private void fv_reset() {
    fv_computed = false;
    
    fv_value = null;
    fv_visited = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="fv") protected boolean fv_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="fv") protected Set<String> fv_value;

  /**
   * @attribute syn
   * @aspect FreeVariables
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:6
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="FreeVariables", declaredAt="C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Semantics.jrag:6")
  @SideEffect.Pure(group="fv") public Set<String> fv() {
    ASTState state = state();
    if (fv_computed) {
      return fv_value;
    }
    if (fv_visited) {
      throw new RuntimeException("Circular definition of attribute Expr.fv().");
    }
    fv_visited = true;
    state().enterLazyAttribute();
    fv_value = Collections.singleton(getID());
    fv_computed = true;
    state().leaveLazyAttribute();
    fv_visited = false;
    return fv_value;
  }
}

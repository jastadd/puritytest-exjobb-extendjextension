/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package lang.ast;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.*;
import lang.ast.SideEffect.*;
/**
 * @ast node
 * @production ASTNode;

 */
@SideEffect.Entity public class ASTNode<T extends ASTNode> extends beaver.Symbol implements Cloneable {
  /**
   * @aspect DumpTree
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\DumpTree.jrag:9
   */
  private String DUMP_TREE_INDENT = "  ";
  /**
   * @aspect DumpTree
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\DumpTree.jrag:11
   */
  public String dumpTree() {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		dumpTree(new PrintStream(bytes));
		return bytes.toString();
	}
  /**
   * @aspect DumpTree
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\DumpTree.jrag:17
   */
  public void dumpTree(PrintStream out) {
		dumpTree(out, "");
		out.flush();
	}
  /**
   * @aspect DumpTree
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\DumpTree.jrag:22
   */
  public void dumpTree(PrintStream out, String indent) {
		out.print(indent + getClass().getSimpleName());
		out.println(getTokens());
		String childIndent = indent + DUMP_TREE_INDENT;
		for (ASTNode child : astChildren()) {
			if (child == null)  {
				out.println(childIndent + "null");
			} else {
				child.dumpTree(out, childIndent);
			}
		}
	}
  /**
   * @aspect DumpTree
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\DumpTree.jrag:35
   */
  public String getTokens() {
		StringBuilder sb = new StringBuilder();
		java.lang.reflect.Method[] methods = getClass().getMethods();
		for (java.lang.reflect.Method method : getClass().getMethods()) {
			ASTNodeAnnotation.Token token = method.getAnnotation(ASTNodeAnnotation.Token.class);
			if (token != null) {
				try {
					sb.append(" " + token.name() + "=\"" + method.invoke(this) + "\"");
				} catch (IllegalAccessException e) {
				} catch (InvocationTargetException e) {
				}
			}
		}
		return sb.toString();
	}
  /**
   * @aspect ConstructorMethods
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Util.jadd:2
   */
  @SideEffect.Fresh public static Var Var(String id) { return new Var(id); }
  /**
   * @aspect ConstructorMethods
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Util.jadd:3
   */
  @SideEffect.Fresh public static Abs Abs(String id, Expr e) { return new Abs(id, e); }
  /**
   * @aspect ConstructorMethods
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Util.jadd:4
   */
  @SideEffect.Fresh public static Lit Lit(int i) { return new Lit(i); }
  /**
   * @aspect ConstructorMethods
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Util.jadd:5
   */
  @SideEffect.Fresh public static App App(Expr l, Expr r) { return new App(l, r); }
  /**
   * @aspect ConstructorMethods
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Util.jadd:6
   */
  @SideEffect.Fresh public static Sub Sub(Expr l, Expr r) { return new Sub(l, r); }
  /**
   * @aspect Util
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Util.jadd:14
   */
  @SideEffect.Fresh
	public static <T> Set<T> union(Set<T> a, Set<T> b) {
		return Stream.concat(a.stream(), b.stream())
			.collect(Collectors.toSet());
	}
  /**
   * @aspect Util
   * @declaredat C:\\Users\\Mikael\\SkyDrive\\puritytest-exjobb\\fors-lambda-calculus-438d004178f7\\fors-lambda-calculus-438d004178f7\\src\\jastadd\\Util.jadd:19
   */
  @SideEffect.Fresh
	public static <T> Set<T> filter(Set<T> a, T t) {
		Set<T> newSet = new HashSet<>(a);
		newSet.remove(t);
		return newSet;
	}
  /**
   * @declaredat ASTNode:1
   */
  public ASTNode() {
    super();
    init$Children();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:11
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /**
   * Cached child index. Child indices are assumed to never change (AST should
   * not change after construction).
   * @apilevel internal
   * @declaredat ASTNode:18
   */
  @SideEffect.Secret(group="_ASTNode")  private int childIndex = -1;
  /** @apilevel low-level 
   * @declaredat ASTNode:21
   */
  @SideEffect.Ignore public int getIndexOfChild(ASTNode node) {
    if (node == null) {
      return -1;
    }
    if (node.childIndex >= 0) {
      return node.childIndex;
    }
    for (int i = 0; children != null && i < children.length; i++) {
      if (children[i] == node) {
        node.childIndex = i;
        return i;
      }
    }
    return -1;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  @SideEffect.Secret(group="_ASTNode") public static final boolean generatedWithCacheCycle = true;
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  @SideEffect.Secret(group="_ASTNode") public static final boolean generatedWithComponentCheck = false;
  /** @apilevel low-level 
   * @declaredat ASTNode:44
   */
  @SideEffect.Secret(group="_ASTNode") protected ASTNode parent;
  /** @apilevel low-level 
   * @declaredat ASTNode:47
   */
  @SideEffect.Local protected ASTNode[] children;
  /** @apilevel internal 
   * @declaredat ASTNode:51
   */
  @SideEffect.Secret(group="_ASTNode") private static ASTState state = new ASTState();
  /** @apilevel internal 
   * @declaredat ASTNode:54
   */
  @SideEffect.Pure(group="_ASTNode") public final ASTState state() {
    return state;
  }
  /**
   * @return an iterator that can be used to iterate over the children of this node.
   * The iterator does not allow removing children.
   * @declaredat ASTNode:63
   */
  @SideEffect.Fresh public java.util.Iterator<T> astChildIterator() {
    return new java.util.Iterator<T>() {
      private int index = 0;

      @Override
      public boolean hasNext() {
        return index < getNumChild();
      }

      @Override
      public T next() {
        return hasNext() ? (T) getChild(index++) : null;
      }

      @Override
      public void remove() {
        throw new UnsupportedOperationException();
      }
    };
  }
  /** @return an object that can be used to iterate over the children of this node 
   * @declaredat ASTNode:85
   */
  @SideEffect.Fresh public Iterable<T> astChildren() {
    return new Iterable<T>() {
      @Override
      public java.util.Iterator<T> iterator() {
        return astChildIterator();
      }
    };
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:95
   */
  @SideEffect.Pure(Ignore=true) public T getChild(int i) {
    ASTNode child = getChildNoTransform(i);
    return (T) child;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:100
   */
  @SideEffect.Local public void addChild(T node) {
    setChild(node, getNumChildNoTransform());
  }
  /**
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @apilevel low-level
   * @declaredat ASTNode:107
   */
  @SideEffect.Pure(group="_ASTNode") public final T getChildNoTransform(int i) {
    if (children == null) {
      return null;
    }
    T child = (T)children[i];
    return child;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:115
   */
  protected int numChildren;
  /** @apilevel low-level 
   * @declaredat ASTNode:118
   */
  @SideEffect.Pure protected int numChildren() {
    return numChildren;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:123
   */
  @SideEffect.Pure public int getNumChild() {
    return numChildren();
  }
  /**
   * Behaves like getNumChild, but does not invoke AST transformations (rewrites).
   * @apilevel low-level
   * @declaredat ASTNode:131
   */
  @SideEffect.Pure public final int getNumChildNoTransform() {
    return numChildren();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:135
   */
  @SideEffect.Ignore @SideEffect.Local public void setChild(ASTNode node, int i) {
    if (children == null) {
      children = new ASTNode[(i + 1 > 4 || !(this instanceof List)) ? i + 1 : 4];
    } else if (i >= children.length) {
      ASTNode c[] = new ASTNode[i << 1];
      System.arraycopy(children, 0, c, 0, children.length);
      children = c;
    }
    children[i] = node;
    if (i >= numChildren) {
      numChildren = i+1;
    }
    if (node != null) {
      node.setParent(this);
      node.childIndex = i;
    }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:153
   */
  @SideEffect.Ignore @SideEffect.Local public void insertChild(ASTNode node, int i) {
    if (children == null) {
      children = new ASTNode[(i + 1 > 4 || !(this instanceof List)) ? i + 1 : 4];
      children[i] = node;
    } else {
      ASTNode c[] = new ASTNode[children.length + 1];
      System.arraycopy(children, 0, c, 0, i);
      c[i] = node;
      if (i < children.length) {
        System.arraycopy(children, i, c, i+1, children.length-i);
        for(int j = i+1; j < c.length; ++j) {
          if (c[j] != null) {
            c[j].childIndex = j;
          }
        }
      }
      children = c;
    }
    numChildren++;
    if (node != null) {
      node.setParent(this);
      node.childIndex = i;
    }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:178
   */
  @SideEffect.Ignore @SideEffect.Local public void removeChild(int i) {
    if (children != null) {
      ASTNode child = (ASTNode) children[i];
      if (child != null) {
        child.parent = null;
        child.childIndex = -1;
      }
      // Adding a check of this instance to make sure its a List, a move of children doesn't make
      // any sense for a node unless its a list. Also, there is a problem if a child of a non-List node is removed
      // and siblings are moved one step to the right, with null at the end.
      if (this instanceof List || this instanceof Opt) {
        System.arraycopy(children, i+1, children, i, children.length-i-1);
        children[children.length-1] = null;
        numChildren--;
        // fix child indices
        for(int j = i; j < numChildren; ++j) {
          if (children[j] != null) {
            child = (ASTNode) children[j];
            child.childIndex = j;
          }
        }
      } else {
        children[i] = null;
      }
    }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:205
   */
  @SideEffect.Pure(group="_ASTNode") public ASTNode getParent() {
    return (ASTNode) parent;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:209
   */
  @SideEffect.Pure(group="_ASTNode") public void setParent(ASTNode node) {
    parent = node;
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:322
   */
  @SideEffect.Ignore public void flushTreeCache() {
    flushCache();
    if (children != null) {
      for (int i = 0; i < children.length; i++) {
        if (children[i] != null) {
          ((ASTNode) children[i]).flushTreeCache();
        }
      }
    }
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:333
   */
  @SideEffect.Ignore public void flushCache() {
    flushAttrAndCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:337
   */
  @SideEffect.Ignore public void flushAttrAndCollectionCache() {
    flushAttrCache();
    flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:342
   */
  @SideEffect.Ignore public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:345
   */
  @SideEffect.Ignore public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:348
   */
  @SideEffect.Fresh public ASTNode<T> clone() throws CloneNotSupportedException {
    ASTNode node = (ASTNode) super.clone();
    node.flushAttrAndCollectionCache();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:354
   */
  @SideEffect.Fresh(group="_ASTNode") public ASTNode<T> copy() {
    try {
      ASTNode node = (ASTNode) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:373
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public ASTNode<T> fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:383
   */
  @SideEffect.Fresh(group="_ASTNode") public ASTNode<T> treeCopyNoTransform() {
    ASTNode tree = (ASTNode) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:403
   */
  @SideEffect.Fresh(group="_ASTNode") public ASTNode<T> treeCopy() {
    ASTNode tree = (ASTNode) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Performs a full traversal of the tree using getChild to trigger rewrites
   * @apilevel low-level
   * @declaredat ASTNode:420
   */
  @SideEffect.Pure public void doFullTraversal() {
    for (int i = 0; i < getNumChild(); i++) {
      getChild(i).doFullTraversal();
    }
  }
  /** @apilevel internal 
   * @declaredat ASTNode:426
   */
  @SideEffect.Pure protected boolean is$Equal(ASTNode n1, ASTNode n2) {
    if (n1 == null && n2 == null) return true;
    if (n1 == null || n2 == null) return false;
    return n1.is$Equal(n2);
  }
  /** @apilevel internal 
   * @declaredat ASTNode:432
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    if (getClass() != node.getClass()) {
      return false;
    }
    if (numChildren != node.numChildren) {
      return false;
    }
    for (int i = 0; i < numChildren; i++) {
      if (children[i] == null && node.children[i] != null) {
        return false;
      }
      if (!((ASTNode)children[i]).is$Equal(((ASTNode)node.children[i]))) {
        return false;
      }
    }
    return true;
  }
public ASTNode rewrittenNode() { throw new Error("rewrittenNode is undefined for ASTNode"); }
}

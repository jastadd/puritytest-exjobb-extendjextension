/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
/**
 * @ast node
 * @declaredat tests\\annotation\\generated\\tokens01\\Test.ast:1
 * @production A : {@link ASTNode} ::= <span class="component">&lt;ID:String&gt;</span> <span class="component">&lt;I:int&gt;</span> <span class="component">&lt;B:B&gt;</span>;

 */
import lang.ast.SideEffect.*;
import lang.ast.*;
public class A extends ASTNode<ASTNode> implements Cloneable {
	/**
	 * @declaredat ASTNode:1
	 */
	public A() {
		super();
	}
	/**
	 * Initializes the child array to the correct size.
	 * Initializes List and Opt nta children.
	 * @apilevel internal
	 * @ast method
	 * @declaredat ASTNode:10
	 */
	public void init$Children() {
	}
	/**
	 * @declaredat ASTNode:12
	 */
	public A(String p0, int p1, B p2) {
		setID(p0);
		setI(p1);
		setB(p2);
	}
	/** @apilevel low-level 
	 * @declaredat ASTNode:18
	 */
	protected int numChildren() {
		return 0;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:22
	 */
	public void flushAttrCache() {
		super.flushAttrCache();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:26
	 */
	public void flushCollectionCache() {
		super.flushCollectionCache();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:30
	 */
	public A clone() throws CloneNotSupportedException {
		A node = (A) super.clone();
		return node;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:35
	 */
	public A copy() {
		try {
			A node = (A) clone();
			node.parent = null;
			if (children != null) {
				node.children = (ASTNode[]) children.clone();
			}
			return node;
		} catch (CloneNotSupportedException e) {
			throw new Error("Error: clone not supported for " + getClass().getName());
		}
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @deprecated Please use treeCopy or treeCopyNoTransform instead
	 * @declaredat ASTNode:54
	 */
	@Deprecated
	public A fullCopy() {
		return treeCopyNoTransform();
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:64
	 */
	public A treeCopyNoTransform() {
		A tree = (A) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) children[i];
				if (child != null) {
					child = child.treeCopyNoTransform();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The subtree of this node is traversed to trigger rewrites before copy.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:84
	 */
	public A treeCopy() {
		A tree = (A) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) getChild(i);
				if (child != null) {
					child = child.treeCopy();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:98
	 */
	protected boolean is$Equal(ASTNode node) {
		return super.is$Equal(node) && (tokenString_ID == ((A) node).tokenString_ID) && (tokenint_I == ((A) node).tokenint_I) && (tokenB_B == ((A) node).tokenB_B);    
	}
	/**
	 * Replaces the lexeme ID.
	 * @param value The new value for the lexeme ID.
	 * @apilevel high-level
	 */
	@Local public void setID(String value) {
		tokenString_ID = value;
	}
	/** @apilevel internal 
	 */
	protected String tokenString_ID;
	/**
	 * Retrieves the value for the lexeme ID.
	 * @return The value for the lexeme ID.
	 * @apilevel high-level
	 */
	@ASTNodeAnnotation.Token(name="ID")
	public String getID() {
		return tokenString_ID != null ? tokenString_ID : "";
	}
	/**
	 * Replaces the lexeme I.
	 * @param value The new value for the lexeme I.
	 * @apilevel high-level
	 */
	@Local public void setI(int value) {
		tokenint_I = value;
	}
	/** @apilevel internal 
	 */
	protected int tokenint_I;
	/**
	 * Retrieves the value for the lexeme I.
	 * @return The value for the lexeme I.
	 * @apilevel high-level
	 */
	@ASTNodeAnnotation.Token(name="I")
	public int getI() {
		return tokenint_I;
	}
	/**
	 * Replaces the lexeme B.
	 * @param value The new value for the lexeme B.
	 * @apilevel high-level
	 */
	@Local public void setB(B value) {
		tokenB_B = value;
	}
	/** @apilevel internal 
	 */
	protected B tokenB_B;
	/**
	 * Retrieves the value for the lexeme B.
	 * @return The value for the lexeme B.
	 * @apilevel high-level
	 */
	@ASTNodeAnnotation.Token(name="B")
	public B getB() {
		return tokenB_B;
	}
}

/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package lang.ast;
import java.util.*;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
/**
 * @ast node
 * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Simplic.ast:31
 * @production Leq : {@link Expr} ::= <span class="component">Left:{@link Expr}</span> <span class="component">Right:{@link Expr}</span>;

 */
public class Leq extends Expr implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:229
   */
  public void genCode(PrintStream out,String label){
	getRight().genCode(out); //left in %rax
	out.println("	pushq %rax");
	getLeft().genCode(out); 
	out.println("	popq %rbx");
	out.println("	cmpq %rbx,%rax");
	out.println("	jg " +label);
}
  /**
   * @aspect Interpreter
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Interpreter.jrag:195
   */
  public int eval(ActivationRecord actrec)
{
	if (getLeft().eval(actrec)<=getRight().eval(actrec))
		return 1;
	return 0;
}
  /**
   * @aspect PrettyPrint
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\PrettyPrint.jrag:52
   */
  public void prettyPrint(PrintStream out, String ind) {
		getLeft().prettyPrint(out, ind);
		out.print(" <= ");
		getRight().prettyPrint(out, ind);
	}
  /**
   * @aspect Visitor
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Visitor.jrag:92
   */
  public Object accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
  /**
   * @declaredat ASTNode:1
   */
  public Leq() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
  }
  /**
   * @declaredat ASTNode:13
   */
  public Leq(Expr p0, Expr p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:18
   */
  @SideEffect.Pure protected int numChildren() {
    return 2;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:22
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:26
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:30
   */
  @SideEffect.Fresh public Leq clone() throws CloneNotSupportedException {
    Leq node = (Leq) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:35
   */
  @SideEffect.Fresh(group="_ASTNode") public Leq copy() {
    try {
      Leq node = (Leq) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:54
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Leq fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:64
   */
  @SideEffect.Fresh(group="_ASTNode") public Leq treeCopyNoTransform() {
    Leq tree = (Leq) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:84
   */
  @SideEffect.Fresh(group="_ASTNode") public Leq treeCopy() {
    Leq tree = (Leq) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:98
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the Left child.
   * @param node The new node to replace the Left child.
   * @apilevel high-level
   */
  public void setLeft(Expr node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Left child.
   * @return The current node used as the Left child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Left")
  @SideEffect.Pure public Expr getLeft() {
    return (Expr) getChild(0);
  }
  /**
   * Retrieves the Left child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Left child.
   * @apilevel low-level
   */
  @SideEffect.Pure public Expr getLeftNoTransform() {
    return (Expr) getChildNoTransform(0);
  }
  /**
   * Replaces the Right child.
   * @param node The new node to replace the Right child.
   * @apilevel high-level
   */
  public void setRight(Expr node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the Right child.
   * @return The current node used as the Right child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Right")
  @SideEffect.Pure public Expr getRight() {
    return (Expr) getChild(1);
  }
  /**
   * Retrieves the Right child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Right child.
   * @apilevel low-level
   */
  @SideEffect.Pure public Expr getRightNoTransform() {
    return (Expr) getChildNoTransform(1);
  }
}

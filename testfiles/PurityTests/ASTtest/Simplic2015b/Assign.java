/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package lang.ast;
import java.util.*;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Set;
import java.util.TreeSet;
/**
 * @ast node
 * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Simplic.ast:14
 * @production Assign : {@link Stmt} ::= <span class="component">{@link IDExpr}</span> <span class="component">{@link Expr}</span>;

 */
public class Assign extends Stmt implements Cloneable {
  /**
   * @aspect CodeGen
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\CodeGen.jrag:284
   */
  public void genCode(PrintStream out){
 getExpr().genCode(out);
 out.println("movq %rax,"+((AbstractDecl)getIDExpr().decl()).address());
}
  /**
   * @aspect Interpreter
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Interpreter.jrag:109
   */
  public void eval(ActivationRecord actrec)
{
	actrec.set(((AbstractDecl)getIDExpr().decl()).uniqueName(),getExpr().eval(actrec));
}
  /**
   * @aspect PrettyPrint
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\PrettyPrint.jrag:148
   */
  public void prettyPrint(PrintStream out, String ind) {
		out.print(ind);
		out.print(getIDExpr().getID());
		out.print(" = ");
		getExpr().prettyPrint(out, ind);
		out.print(";");
	}
  /**
   * @aspect Visitor
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Visitor.jrag:65
   */
  public Object accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
  /**
   * @declaredat ASTNode:1
   */
  public Assign() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
  }
  /**
   * @declaredat ASTNode:13
   */
  public Assign(IDExpr p0, Expr p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:18
   */
  @SideEffect.Pure protected int numChildren() {
    return 2;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:22
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:26
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:30
   */
  @SideEffect.Fresh public Assign clone() throws CloneNotSupportedException {
    Assign node = (Assign) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:35
   */
  @SideEffect.Fresh(group="_ASTNode") public Assign copy() {
    try {
      Assign node = (Assign) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:54
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Assign fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:64
   */
  @SideEffect.Fresh(group="_ASTNode") public Assign treeCopyNoTransform() {
    Assign tree = (Assign) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:84
   */
  @SideEffect.Fresh(group="_ASTNode") public Assign treeCopy() {
    Assign tree = (Assign) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:98
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the IDExpr child.
   * @param node The new node to replace the IDExpr child.
   * @apilevel high-level
   */
  public void setIDExpr(IDExpr node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the IDExpr child.
   * @return The current node used as the IDExpr child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="IDExpr")
  @SideEffect.Pure public IDExpr getIDExpr() {
    return (IDExpr) getChild(0);
  }
  /**
   * Retrieves the IDExpr child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the IDExpr child.
   * @apilevel low-level
   */
  @SideEffect.Pure public IDExpr getIDExprNoTransform() {
    return (IDExpr) getChildNoTransform(0);
  }
  /**
   * Replaces the Expr child.
   * @param node The new node to replace the Expr child.
   * @apilevel high-level
   */
  public void setExpr(Expr node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the Expr child.
   * @return The current node used as the Expr child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Expr")
  @SideEffect.Pure public Expr getExpr() {
    return (Expr) getChild(1);
  }
  /**
   * Retrieves the Expr child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Expr child.
   * @apilevel low-level
   */
  @SideEffect.Pure public Expr getExprNoTransform() {
    return (Expr) getChildNoTransform(1);
  }
/** @apilevel internal */
@SideEffect.Secret(group="collectFunctionCalls_Set_Function_") protected java.util.Set collectFunctionCalls_Set_Function__visited;
  /**
   * @attribute syn
   * @aspect FunctionCallsAnalysis
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\FunctionCallsAnalysis.jrag:70
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="FunctionCallsAnalysis", declaredAt="D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\FunctionCallsAnalysis.jrag:70")
  @SideEffect.Pure(group="collectFunctionCalls_Set_Function_") public Set<Function> collectFunctionCalls(Set<Function> functions) {
    Object _parameters = functions;
    if (collectFunctionCalls_Set_Function__visited == null) collectFunctionCalls_Set_Function__visited = new java.util.HashSet(4);
    if (collectFunctionCalls_Set_Function__visited.contains(_parameters)) {
      throw new RuntimeException("Circular definition of attribute Assign.collectFunctionCalls(Set_Function_).");
    }
    collectFunctionCalls_Set_Function__visited.add(_parameters);
    try {
    		return getExpr().collectFunctionCalls(functions);
    	}
    finally {
      collectFunctionCalls_Set_Function__visited.remove(_parameters);
    }
  }
  /**
   * @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:33
   * @apilevel internal
   */
 @SideEffect.Pure public Type Define_expectedType(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getIDExprNoTransform()) {
      // @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:43
      return UnknownType();
    }
    else if (_callerNode == getExprNoTransform()) {
      // @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\TypeAnalysis.jrag:42
      return UnknownType();
    }
    else {
      return getParent().Define_expectedType(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_expectedType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  @SideEffect.Local(group="_ASTNode") protected void collect_contributors_Program_errors(Program _root, @SideEffect.Ignore java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat D:\\backup Simplic - kopia\\backup Simplic\\src\\jastadd\\Errors.jrag:40
    if (getIDExpr().decl().isMultipledeclared()) {
      {
        Program target = (Program) (program());
        java.util.Set<ASTNode> contributors = _map.get(target);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) target, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_Program_errors(_root, _map);
  }
  @SideEffect.Pure(group="_ASTNode") protected void contributeTo_Program_errors(Set<ErrorMessage> collection) {
    super.contributeTo_Program_errors(collection);
    if (getIDExpr().decl().isMultipledeclared()) {
      collection.add(error("symbol '" + getIDExpr().getID() + "' is multipled declared"));
    }
  }
}

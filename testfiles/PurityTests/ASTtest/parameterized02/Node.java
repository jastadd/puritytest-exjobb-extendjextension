/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
/**
 * @ast node
 * @declaredat tests\\syn\\parameterized02\\Test.ast:1
 * @production Node : {@link ASTNode};

 */
public class Node extends ASTNode<ASTNode> implements Cloneable {
	/**
	 * @declaredat ASTNode:1
	 */
	public Node() {
		super();
	}
	/**
	 * Initializes the child array to the correct size.
	 * Initializes List and Opt nta children.
	 * @apilevel internal
	 * @ast method
	 * @declaredat ASTNode:10
	 */
	public void init$Children() {
	}
	/** @apilevel low-level 
	 * @declaredat ASTNode:13
	 */
	protected int numChildren() {
		return 0;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:17
	 */
	public void flushAttrCache() {
		super.flushAttrCache();
		attr_String_reset();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:22
	 */
	public void flushCollectionCache() {
		super.flushCollectionCache();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:26
	 */
	public Node clone() throws CloneNotSupportedException {
		Node node = (Node) super.clone();
		return node;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:31
	 */
	public Node copy() {
		try {
			Node node = (Node) clone();
			node.parent = null;
			if (children != null) {
				node.children = (ASTNode[]) children.clone();
			}
			return node;
		} catch (CloneNotSupportedException e) {
			throw new Error("Error: clone not supported for " + getClass().getName());
		}
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @deprecated Please use treeCopy or treeCopyNoTransform instead
	 * @declaredat ASTNode:50
	 */
	@Deprecated
	public Node fullCopy() {
		return treeCopyNoTransform();
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:60
	 */
	public Node treeCopyNoTransform() {
		Node tree = (Node) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) children[i];
				if (child != null) {
					child = child.treeCopyNoTransform();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The subtree of this node is traversed to trigger rewrites before copy.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:80
	 */
	public Node treeCopy() {
		Node tree = (Node) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) getChild(i);
				if (child != null) {
					child = child.treeCopy();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:94
	 */
	protected boolean is$Equal(ASTNode node) {
		return super.is$Equal(node);    
	}
/** @apilevel internal */
@SideEffect.Secret(group="attr_String") protected java.util.Set attr_String_visited;
	/** @apilevel internal */
	private void attr_String_reset() {
		attr_String_values = null;
		attr_String_visited = null;
	}
	/** @apilevel internal */
	 @SideEffect.Secret(group="attr_String") protected java.util.Map attr_String_values;

	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\parameterized02\\Test.jrag:2
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\parameterized02\\Test.jrag:2")
	@SideEffect.Pure(group="attr_String") public boolean attr(String s) {
		Object _parameters = s;
		if (attr_String_visited == null) attr_String_visited = new java.util.HashSet(4);
		if (attr_String_values == null) attr_String_values = new java.util.HashMap(4);
		ASTState state = state();
		if (attr_String_values.containsKey(_parameters)) {
			return (Boolean) attr_String_values.get(_parameters);
		}
		if (attr_String_visited.contains(_parameters)) {
			throw new RuntimeException("Circular definition of attribute Node.attr(String).");
		}
		attr_String_visited.add(_parameters);
		state().enterLazyAttribute();
		boolean attr_String_value = s.equals("");
		attr_String_values.put(_parameters, attr_String_value);
		state().leaveLazyAttribute();
		attr_String_visited.remove(_parameters);
		return attr_String_value;
	}
}

/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
/**
 * @ast node
 * @declaredat tests\\syn\\override02\\Test.ast:2
 * @production SubNode : {@link Node};

 */
public class SubNode extends Node implements Cloneable {
	/**
	 * @declaredat ASTNode:1
	 */
	public SubNode() {
		super();
	}
	/**
	 * Initializes the child array to the correct size.
	 * Initializes List and Opt nta children.
	 * @apilevel internal
	 * @ast method
	 * @declaredat ASTNode:10
	 */
	public void init$Children() {
	}
	/** @apilevel low-level 
	 * @declaredat ASTNode:13
	 */
	protected int numChildren() {
		return 0;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:17
	 */
	public void flushAttrCache() {
		super.flushAttrCache();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:21
	 */
	public void flushCollectionCache() {
		super.flushCollectionCache();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:25
	 */
	public SubNode clone() throws CloneNotSupportedException {
		SubNode node = (SubNode) super.clone();
		return node;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:30
	 */
	public SubNode copy() {
		try {
			SubNode node = (SubNode) clone();
			node.parent = null;
			if (children != null) {
				node.children = (ASTNode[]) children.clone();
			}
			return node;
		} catch (CloneNotSupportedException e) {
			throw new Error("Error: clone not supported for " + getClass().getName());
		}
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @deprecated Please use treeCopy or treeCopyNoTransform instead
	 * @declaredat ASTNode:49
	 */
	@Deprecated
	public SubNode fullCopy() {
		return treeCopyNoTransform();
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:59
	 */
	public SubNode treeCopyNoTransform() {
		SubNode tree = (SubNode) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) children[i];
				if (child != null) {
					child = child.treeCopyNoTransform();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The subtree of this node is traversed to trigger rewrites before copy.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:79
	 */
	public SubNode treeCopy() {
		SubNode tree = (SubNode) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) getChild(i);
				if (child != null) {
					child = child.treeCopy();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:93
	 */
	protected boolean is$Equal(ASTNode node) {
		return super.is$Equal(node);    
	}
/** @apilevel internal */
@SideEffect.Secret(group="calc_int") protected java.util.Set calc_int_visited;
	/**
	 * @attribute syn
	 * @aspect A
	 * @declaredat tests\\syn\\override02\\Test.jrag:2
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="A", declaredAt="tests\\syn\\override02\\Test.jrag:2")
	@SideEffect.Pure(group="calc_int") public int calc(int value) {
		Object _parameters = value;
		if (calc_int_visited == null) calc_int_visited = new java.util.HashSet(4);
		if (calc_int_visited.contains(_parameters)) {
			throw new RuntimeException("Circular definition of attribute Node.calc(int).");
		}
		calc_int_visited.add(_parameters);
		int calc_int_value = value - 1;
		calc_int_visited.remove(_parameters);
		return calc_int_value;
	}
}

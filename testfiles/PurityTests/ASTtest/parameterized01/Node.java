/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
/**
 * @ast node
 * @declaredat tests\\syn\\parameterized01\\Test.ast:1
 * @production Node : {@link ASTNode};

 */
public class Node extends ASTNode<ASTNode> implements Cloneable {
	/**
	 * @declaredat ASTNode:1
	 */
	public Node() {
		super();
	}
	/**
	 * Initializes the child array to the correct size.
	 * Initializes List and Opt nta children.
	 * @apilevel internal
	 * @ast method
	 * @declaredat ASTNode:10
	 */
	public void init$Children() {
	}
	/** @apilevel low-level 
	 * @declaredat ASTNode:13
	 */
	protected int numChildren() {
		return 0;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:17
	 */
	public void flushAttrCache() {
		super.flushAttrCache();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:21
	 */
	public void flushCollectionCache() {
		super.flushCollectionCache();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:25
	 */
	public Node clone() throws CloneNotSupportedException {
		Node node = (Node) super.clone();
		return node;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:30
	 */
	public Node copy() {
		try {
			Node node = (Node) clone();
			node.parent = null;
			if (children != null) {
				node.children = (ASTNode[]) children.clone();
			}
			return node;
		} catch (CloneNotSupportedException e) {
			throw new Error("Error: clone not supported for " + getClass().getName());
		}
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @deprecated Please use treeCopy or treeCopyNoTransform instead
	 * @declaredat ASTNode:49
	 */
	@Deprecated
	public Node fullCopy() {
		return treeCopyNoTransform();
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:59
	 */
	public Node treeCopyNoTransform() {
		Node tree = (Node) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) children[i];
				if (child != null) {
					child = child.treeCopyNoTransform();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The subtree of this node is traversed to trigger rewrites before copy.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:79
	 */
	public Node treeCopy() {
		Node tree = (Node) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) getChild(i);
				if (child != null) {
					child = child.treeCopy();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:93
	 */
	protected boolean is$Equal(ASTNode node) {
		return super.is$Equal(node);    
	}
/** @apilevel internal */
@SideEffect.Secret(group="paramAttr_int") protected java.util.Set paramAttr_int_visited;
	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\parameterized01\\Test.jrag:2
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\parameterized01\\Test.jrag:2")
	@SideEffect.Pure(group="paramAttr_int") public int paramAttr(int i) {
		Object _parameters = i;
		if (paramAttr_int_visited == null) paramAttr_int_visited = new java.util.HashSet(4);
		if (paramAttr_int_visited.contains(_parameters)) {
			throw new RuntimeException("Circular definition of attribute Node.paramAttr(int).");
		}
		paramAttr_int_visited.add(_parameters);
		int paramAttr_int_value = i + 13;
		paramAttr_int_visited.remove(_parameters);
		return paramAttr_int_value;
	}
}

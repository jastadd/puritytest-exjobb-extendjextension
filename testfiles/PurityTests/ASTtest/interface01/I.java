/**
 * @ast interface
 * @aspect Test
 * @declaredat tests\\syn\\interface01\\Test.jrag:2
 */
 interface I {
	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\interface01\\Test.jrag:5
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\interface01\\Test.jrag:5")
	@SideEffect.Pure(group="attr") public boolean attr();
	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\interface01\\Test.jrag:9
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\interface01\\Test.jrag:9")
	@SideEffect.Pure(group="attr2") public boolean attr2();
}

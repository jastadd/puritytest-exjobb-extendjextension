/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
/**
 * @ast node
 * @declaredat tests\\syn\\interface01\\Test.ast:1
 * @production Node : {@link ASTNode};

 */
public class Node extends ASTNode<ASTNode> implements Cloneable, I {
	/**
	 * @declaredat ASTNode:1
	 */
	public Node() {
		super();
	}
	/**
	 * Initializes the child array to the correct size.
	 * Initializes List and Opt nta children.
	 * @apilevel internal
	 * @ast method
	 * @declaredat ASTNode:10
	 */
	public void init$Children() {
	}
	/** @apilevel low-level 
	 * @declaredat ASTNode:13
	 */
	protected int numChildren() {
		return 0;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:17
	 */
	public void flushAttrCache() {
		super.flushAttrCache();
		attr2_reset();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:22
	 */
	public void flushCollectionCache() {
		super.flushCollectionCache();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:26
	 */
	public Node clone() throws CloneNotSupportedException {
		Node node = (Node) super.clone();
		return node;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:31
	 */
	public Node copy() {
		try {
			Node node = (Node) clone();
			node.parent = null;
			if (children != null) {
				node.children = (ASTNode[]) children.clone();
			}
			return node;
		} catch (CloneNotSupportedException e) {
			throw new Error("Error: clone not supported for " + getClass().getName());
		}
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @deprecated Please use treeCopy or treeCopyNoTransform instead
	 * @declaredat ASTNode:50
	 */
	@Deprecated
	public Node fullCopy() {
		return treeCopyNoTransform();
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:60
	 */
	public Node treeCopyNoTransform() {
		Node tree = (Node) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) children[i];
				if (child != null) {
					child = child.treeCopyNoTransform();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The subtree of this node is traversed to trigger rewrites before copy.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:80
	 */
	public Node treeCopy() {
		Node tree = (Node) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) getChild(i);
				if (child != null) {
					child = child.treeCopy();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:94
	 */
	protected boolean is$Equal(ASTNode node) {
		return super.is$Equal(node);    
	}
/** @apilevel internal */
@SideEffect.Secret(group="attr") protected boolean attr_visited = false;
	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\interface01\\Test.jrag:5
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\interface01\\Test.jrag:5")
	@SideEffect.Pure(group="attr") public boolean attr() {
		if (attr_visited) {
			throw new RuntimeException("Circular definition of attribute Node.attr().");
		}
		attr_visited = true;
		boolean attr_value = false;
		attr_visited = false;
		return attr_value;
	}
/** @apilevel internal */
protected ASTState.Cycle attr2_cycle = null;
	/** @apilevel internal */
	private void attr2_reset() {
		attr2_computed = false;
		attr2_initialized = false;
		attr2_cycle = null;
	}
	/** @apilevel internal */
	@SideEffect.Secret(group="attr2") protected boolean attr2_computed = false;

	/** @apilevel internal */
	@SideEffect.Secret(group="attr2") protected boolean attr2_value;
	/** @apilevel internal */
	@SideEffect.Secret(group="attr2") protected boolean attr2_initialized = false;
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\interface01\\Test.jrag:9")
	@SideEffect.Pure(group="attr2") public boolean attr2() {
		if (attr2_computed) {
			return attr2_value;
		}
		ASTState state = state();
		if (!attr2_initialized) {
			attr2_initialized = true;
			attr2_value = true;
		}
		if (!state.inCircle() || state.calledByLazyAttribute()) {
			state.enterCircle();
			do {
				attr2_cycle = state.nextCycle();
				boolean new_attr2_value = attr() && attr2();
				if (new_attr2_value != attr2_value) {
					state.setChangeInCycle();
				}
				attr2_value = new_attr2_value;
			} while (state.testAndClearChangeInCycle());
			attr2_computed = true;
			state.startLastCycle();
			boolean $tmp = attr() && attr2();

			state.leaveCircle();
		} else if (attr2_cycle != state.cycle()) {
			attr2_cycle = state.cycle();
			if (state.lastCycle()) {
				attr2_computed = true;
				boolean new_attr2_value = attr() && attr2();
				return new_attr2_value;
			}
			boolean new_attr2_value = attr() && attr2();
			if (new_attr2_value != attr2_value) {
				state.setChangeInCycle();
			}
			attr2_value = new_attr2_value;
		} else {
		}
		return attr2_value;
	}
}

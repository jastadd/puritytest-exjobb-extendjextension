/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
/**
 * @ast node
 * @declaredat tests\\syn\\lazy_03p\\Test.ast:1
 * @production A : {@link ASTNode};

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
	/**
	 * @declaredat ASTNode:1
	 */
	public A() {
		super();
	}
	/**
	 * Initializes the child array to the correct size.
	 * Initializes List and Opt nta children.
	 * @apilevel internal
	 * @ast method
	 * @declaredat ASTNode:10
	 */
	public void init$Children() {
	}
	/** @apilevel low-level 
	 * @declaredat ASTNode:13
	 */
	protected int numChildren() {
		return 0;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:17
	 */
	public void flushAttrCache() {
		super.flushAttrCache();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:21
	 */
	public void flushCollectionCache() {
		super.flushCollectionCache();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:25
	 */
	public A clone() throws CloneNotSupportedException {
		A node = (A) super.clone();
		return node;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:30
	 */
	public A copy() {
		try {
			A node = (A) clone();
			node.parent = null;
			if (children != null) {
				node.children = (ASTNode[]) children.clone();
			}
			return node;
		} catch (CloneNotSupportedException e) {
			throw new Error("Error: clone not supported for " + getClass().getName());
		}
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @deprecated Please use treeCopy or treeCopyNoTransform instead
	 * @declaredat ASTNode:49
	 */
	@Deprecated
	public A fullCopy() {
		return treeCopyNoTransform();
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:59
	 */
	public A treeCopyNoTransform() {
		A tree = (A) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) children[i];
				if (child != null) {
					child = child.treeCopyNoTransform();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The subtree of this node is traversed to trigger rewrites before copy.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:79
	 */
	public A treeCopy() {
		A tree = (A) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) getChild(i);
				if (child != null) {
					child = child.treeCopy();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:93
	 */
	protected boolean is$Equal(ASTNode node) {
		return super.is$Equal(node);    
	}
/** @apilevel internal */
@SideEffect.Secret(group="attr") protected boolean attr_visited = false;
	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\lazy_03p\\Test.jrag:2
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\lazy_03p\\Test.jrag:2")
	@SideEffect.Pure(group="attr") public Object attr() {
		if (attr_visited) {
			throw new RuntimeException("Circular definition of attribute A.attr().");
		}
		attr_visited = true;
		Object attr_value = new Object();
		attr_visited = false;
		return attr_value;
	}
/** @apilevel internal */
@SideEffect.Secret(group="isAttrCached") protected boolean isAttrCached_visited = false;
	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\lazy_03p\\Test.jrag:8
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\lazy_03p\\Test.jrag:8")
	@SideEffect.Pure(group="isAttrCached") public boolean isAttrCached() {
		if (isAttrCached_visited) {
			throw new RuntimeException("Circular definition of attribute A.isAttrCached().");
		}
		isAttrCached_visited = true;
		boolean isAttrCached_value = attr() == attr();
		isAttrCached_visited = false;
		return isAttrCached_value;
	}
}

/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
/**
 * @ast node
 * @declaredat tests\\syn\\lazy_03p\\Test.ast:2
 * @production B : {@link A};

 */
public class B extends A implements Cloneable {
	/**
	 * @declaredat ASTNode:1
	 */
	public B() {
		super();
	}
	/**
	 * Initializes the child array to the correct size.
	 * Initializes List and Opt nta children.
	 * @apilevel internal
	 * @ast method
	 * @declaredat ASTNode:10
	 */
	public void init$Children() {
	}
	/** @apilevel low-level 
	 * @declaredat ASTNode:13
	 */
	protected int numChildren() {
		return 0;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:17
	 */
	public void flushAttrCache() {
		super.flushAttrCache();
		attr_reset();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:22
	 */
	public void flushCollectionCache() {
		super.flushCollectionCache();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:26
	 */
	public B clone() throws CloneNotSupportedException {
		B node = (B) super.clone();
		return node;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:31
	 */
	public B copy() {
		try {
			B node = (B) clone();
			node.parent = null;
			if (children != null) {
				node.children = (ASTNode[]) children.clone();
			}
			return node;
		} catch (CloneNotSupportedException e) {
			throw new Error("Error: clone not supported for " + getClass().getName());
		}
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @deprecated Please use treeCopy or treeCopyNoTransform instead
	 * @declaredat ASTNode:50
	 */
	@Deprecated
	public B fullCopy() {
		return treeCopyNoTransform();
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:60
	 */
	public B treeCopyNoTransform() {
		B tree = (B) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) children[i];
				if (child != null) {
					child = child.treeCopyNoTransform();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The subtree of this node is traversed to trigger rewrites before copy.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:80
	 */
	public B treeCopy() {
		B tree = (B) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) getChild(i);
				if (child != null) {
					child = child.treeCopy();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:94
	 */
	protected boolean is$Equal(ASTNode node) {
		return super.is$Equal(node);    
	}
/** @apilevel internal */
@SideEffect.Secret(group="attr") protected boolean attr_visited = false;
	/** @apilevel internal */
	private void attr_reset() {
		attr_computed = false;
		
		attr_value = null;
		attr_visited = false;
	}
	/** @apilevel internal */
	@SideEffect.Secret(group="attr") protected boolean attr_computed = false;

	/** @apilevel internal */
	@SideEffect.Secret(group="attr") protected Object attr_value;

	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\lazy_03p\\Test.jrag:3
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\lazy_03p\\Test.jrag:3")
	@SideEffect.Pure(group="attr") public Object attr() {
		ASTState state = state();
		if (attr_computed) {
			return attr_value;
		}
		if (attr_visited) {
			throw new RuntimeException("Circular definition of attribute B.attr().");
		}
		attr_visited = true;
		state().enterLazyAttribute();
		attr_value = new Object();
		attr_computed = true;
		state().leaveLazyAttribute();
		attr_visited = false;
		return attr_value;
	}
}

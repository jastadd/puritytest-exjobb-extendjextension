/**
 * @ast interface
 * @aspect InterfaceOrder
 * @declaredat tests\\syn\\interface06\\Test.jrag:4
 */
public interface MyInterface {
	/**
	 * @attribute syn
	 * @aspect InterfaceOrder
	 * @declaredat tests\\syn\\interface06\\Test.jrag:2
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="InterfaceOrder", declaredAt="tests\\syn\\interface06\\Test.jrag:2")
	@SideEffect.Pure(group="myValue1") public int myValue1();
	/**
	 * @attribute syn
	 * @aspect InterfaceOrder
	 * @declaredat tests\\syn\\interface06\\Test.jrag:7
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="InterfaceOrder", declaredAt="tests\\syn\\interface06\\Test.jrag:7")
	@SideEffect.Pure(group="myValue2") public int myValue2();
}

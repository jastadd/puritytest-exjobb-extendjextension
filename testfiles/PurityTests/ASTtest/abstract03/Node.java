/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
/**
 * @ast node
 * @declaredat tests\\syn\\abstract03\\Test.ast:1
 * @production Node : {@link ASTNode};

 */
public abstract class Node extends ASTNode<ASTNode> implements Cloneable {
	/**
	 * @declaredat ASTNode:1
	 */
	public Node() {
		super();
	}
	/**
	 * Initializes the child array to the correct size.
	 * Initializes List and Opt nta children.
	 * @apilevel internal
	 * @ast method
	 * @declaredat ASTNode:10
	 */
	public void init$Children() {
	}
	/** @apilevel low-level 
	 * @declaredat ASTNode:13
	 */
	protected int numChildren() {
		return 0;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:17
	 */
	public void flushAttrCache() {
		super.flushAttrCache();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:21
	 */
	public void flushCollectionCache() {
		super.flushCollectionCache();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:25
	 */
	public Node clone() throws CloneNotSupportedException {
		Node node = (Node) super.clone();
		return node;
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @deprecated Please use treeCopy or treeCopyNoTransform instead
	 * @declaredat ASTNode:36
	 */
	@Deprecated
	public abstract Node fullCopy();
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:44
	 */
	public abstract Node treeCopyNoTransform();
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The subtree of this node is traversed to trigger rewrites before copy.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:52
	 */
	public abstract Node treeCopy();
	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\abstract03\\Test.jrag:2
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\abstract03\\Test.jrag:2")
	@SideEffect.Pure(group="value") public abstract int value();
/** @apilevel internal */
@SideEffect.Secret(group="value_int") protected java.util.Set value_int_visited;
	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\abstract03\\Test.jrag:5
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\abstract03\\Test.jrag:5")
	@SideEffect.Pure(group="value_int") public int value(int i) {
		Object _parameters = i;
		if (value_int_visited == null) value_int_visited = new java.util.HashSet(4);
		if (value_int_visited.contains(_parameters)) {
			throw new RuntimeException("Circular definition of attribute Node.value(int).");
		}
		value_int_visited.add(_parameters);
		int value_int_value = -32;
		value_int_visited.remove(_parameters);
		return value_int_value;
	}
}

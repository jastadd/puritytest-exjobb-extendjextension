/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package collAttr;
import java.util.*;
/**
 * @ast node
 * @declaredat testfiles\\source\\collAttr\\Simplic.ast:4
 * @production B : {@link ASTNode} ::= <span class="component">&lt;ID:String&gt;</span> <span class="component">{@link A}*</span>;

 */
public class B extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect Visitor
   * @declaredat testfiles\\source\\collAttr\\Visitor.jrag:35
   */
  public Object accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
  /**
   * @declaredat ASTNode:1
   */
  public B() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[1];
    setChild(new List(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  public B(String p0, List<A> p1) {
    setID(p0);
    setChild(p1, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:19
   */
  protected int numChildren() {
    return 1;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:23
   */
  public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:27
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:31
   */
  public B clone() throws CloneNotSupportedException {
    B node = (B) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:36
   */
  public B copy() {
    try {
      B node = (B) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:55
   */
  @Deprecated
  public B fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:65
   */
  public B treeCopyNoTransform() {
    B tree = (B) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:85
   */
  public B treeCopy() {
    B tree = (B) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:99
   */
  protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_ID == ((B) node).tokenString_ID);    
  }
  /**
   * Replaces the lexeme ID.
   * @param value The new value for the lexeme ID.
   * @apilevel high-level
   */
  public void setID(String value) {
    tokenString_ID = value;
  }
  /** @apilevel internal 
   */
  protected String tokenString_ID;
  /**
   * Retrieves the value for the lexeme ID.
   * @return The value for the lexeme ID.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="ID")
  public String getID() {
    return tokenString_ID != null ? tokenString_ID : "";
  }
  /**
   * Replaces the A list.
   * @param list The new list node to be used as the A list.
   * @apilevel high-level
   */
  public void setAList(List<A> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the A list.
   * @return Number of children in the A list.
   * @apilevel high-level
   */
  public int getNumA() {
    return getAList().getNumChild();
  }
  /**
   * Retrieves the number of children in the A list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the A list.
   * @apilevel low-level
   */
  public int getNumANoTransform() {
    return getAListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the A list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the A list.
   * @apilevel high-level
   */
  public A getA(int i) {
    return (A) getAList().getChild(i);
  }
  /**
   * Check whether the A list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean hasA() {
    return getAList().getNumChild() != 0;
  }
  /**
   * Append an element to the A list.
   * @param node The element to append to the A list.
   * @apilevel high-level
   */
  public void addA(A node) {
    List<A> list = (parent == null) ? getAListNoTransform() : getAList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addANoTransform(A node) {
    List<A> list = getAListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the A list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setA(A node, int i) {
    List<A> list = getAList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the A list.
   * @return The node representing the A list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="A")
  public List<A> getAList() {
    List<A> list = (List<A>) getChild(0);
    return list;
  }
  /**
   * Retrieves the A list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the A list.
   * @apilevel low-level
   */
  public List<A> getAListNoTransform() {
    return (List<A>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the A list without
   * triggering rewrites.
   */
  public A getANoTransform(int i) {
    return (A) getAListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the A list.
   * @return The node representing the A list.
   * @apilevel high-level
   */
  public List<A> getAs() {
    return getAList();
  }
  /**
   * Retrieves the A list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the A list.
   * @apilevel low-level
   */
  public List<A> getAsNoTransform() {
    return getAListNoTransform();
  }
/** @apilevel internal */
@SideEffect.Secret(group="condition") protected boolean condition_visited = false;
  /**
   * @attribute syn
   * @aspect a
   * @declaredat testfiles\\source\\collAttr\\collAttr.jrag:5
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="a", declaredAt="testfiles\\source\\collAttr\\collAttr.jrag:5")
  @SideEffect.Pure(group="condition") public boolean condition() {
    if (condition_visited) {
      throw new RuntimeException("Circular definition of attribute B.condition().");
    }
    condition_visited = true;
    boolean condition_value = true;
    condition_visited = false;
    return condition_value;
  }
  @SideEffect.Pure protected void collect_contributors_A_allchildren(A _root, java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
    // @declaredat testfiles\\source\\collAttr\\collAttr.jrag:7
    if (condition()) {
      {
        java.util.Set<ASTNode> contributors = _map.get(_root);
        if (contributors == null) {
          contributors = new java.util.LinkedHashSet<ASTNode>();
          _map.put((ASTNode) _root, contributors);
        }
        contributors.add(this);
      }
    }
    super.collect_contributors_A_allchildren(_root, _map);
  }
  @SideEffect.Pure protected void contributeTo_A_allchildren(ArrayList<String> collection) {
    super.contributeTo_A_allchildren(collection);
    if (condition()) {
      collection.add(getID());
    }
  }
}

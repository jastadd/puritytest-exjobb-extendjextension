/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package collAttr;
import java.util.*;
/**
 * @ast node
 * @declaredat testfiles\\source\\collAttr\\Simplic.ast:3
 * @production A : {@link ASTNode} ::= <span class="component">&lt;ID:String&gt;</span> <span class="component">{@link C}*</span> <span class="component">{@link B}</span>;

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect Visitor
   * @declaredat testfiles\\source\\collAttr\\Visitor.jrag:32
   */
  public Object accept(Visitor visitor, Object data) {
		return visitor.visit(this, data);
	}
  /**
   * @declaredat ASTNode:1
   */
  public A() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[2];
    setChild(new List(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  public A(String p0, List<C> p1, B p2) {
    setID(p0);
    setChild(p1, 0);
    setChild(p2, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:20
   */
  protected int numChildren() {
    return 2;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:24
   */
  public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  public void flushCollectionCache() {
    super.flushCollectionCache();
    A_allchildren_visited = false;
    A_allchildren_computed = false;
    
    A_allchildren_value = null;
    contributorMap_A_allchildren = null;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  public A clone() throws CloneNotSupportedException {
    A node = (A) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:42
   */
  public A copy() {
    try {
      A node = (A) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:61
   */
  @Deprecated
  public A fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:71
   */
  public A treeCopyNoTransform() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:91
   */
  public A treeCopy() {
    A tree = (A) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:105
   */
  protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_ID == ((A) node).tokenString_ID);    
  }
  /**
   * Replaces the lexeme ID.
   * @param value The new value for the lexeme ID.
   * @apilevel high-level
   */
  public void setID(String value) {
    tokenString_ID = value;
  }
  /** @apilevel internal 
   */
  protected String tokenString_ID;
  /**
   * Retrieves the value for the lexeme ID.
   * @return The value for the lexeme ID.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="ID")
  public String getID() {
    return tokenString_ID != null ? tokenString_ID : "";
  }
  /**
   * Replaces the C list.
   * @param list The new list node to be used as the C list.
   * @apilevel high-level
   */
  public void setCList(List<C> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the C list.
   * @return Number of children in the C list.
   * @apilevel high-level
   */
  public int getNumC() {
    return getCList().getNumChild();
  }
  /**
   * Retrieves the number of children in the C list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the C list.
   * @apilevel low-level
   */
  public int getNumCNoTransform() {
    return getCListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the C list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the C list.
   * @apilevel high-level
   */
  public C getC(int i) {
    return (C) getCList().getChild(i);
  }
  /**
   * Check whether the C list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean hasC() {
    return getCList().getNumChild() != 0;
  }
  /**
   * Append an element to the C list.
   * @param node The element to append to the C list.
   * @apilevel high-level
   */
  public void addC(C node) {
    List<C> list = (parent == null) ? getCListNoTransform() : getCList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addCNoTransform(C node) {
    List<C> list = getCListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the C list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setC(C node, int i) {
    List<C> list = getCList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the C list.
   * @return The node representing the C list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="C")
  public List<C> getCList() {
    List<C> list = (List<C>) getChild(0);
    return list;
  }
  /**
   * Retrieves the C list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the C list.
   * @apilevel low-level
   */
  public List<C> getCListNoTransform() {
    return (List<C>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the C list without
   * triggering rewrites.
   */
  public C getCNoTransform(int i) {
    return (C) getCListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the C list.
   * @return The node representing the C list.
   * @apilevel high-level
   */
  public List<C> getCs() {
    return getCList();
  }
  /**
   * Retrieves the C list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the C list.
   * @apilevel low-level
   */
  public List<C> getCsNoTransform() {
    return getCListNoTransform();
  }
  /**
   * Replaces the B child.
   * @param node The new node to replace the B child.
   * @apilevel high-level
   */
  public void setB(B node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the B child.
   * @return The current node used as the B child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="B")
  public B getB() {
    return (B) getChild(1);
  }
  /**
   * Retrieves the B child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the B child.
   * @apilevel low-level
   */
  public B getBNoTransform() {
    return (B) getChildNoTransform(1);
  }
  /**
   * @aspect <NoAspect>
   * @declaredat testfiles\\source\\collAttr\\collAttr.jrag:6
   */
  protected java.util.Map<ASTNode, java.util.Set<ASTNode>> contributorMap_A_allchildren = null;

  protected void survey_A_allchildren() {
    if (contributorMap_A_allchildren == null) {
      contributorMap_A_allchildren = new java.util.IdentityHashMap<ASTNode, java.util.Set<ASTNode>>();
      collect_contributors_A_allchildren(this, contributorMap_A_allchildren);
    }
  }

/** @apilevel internal */
@SideEffect.Secret(group="A_allchildren") protected boolean A_allchildren_visited = false;
  /**
   * @attribute coll
   * @aspect a
   * @declaredat testfiles\\source\\collAttr\\collAttr.jrag:6
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.COLL)
  @ASTNodeAnnotation.Source(aspect="a", declaredAt="testfiles\\source\\collAttr\\collAttr.jrag:6")
  @SideEffect.Pure(group="A_allchildren") public ArrayList<String> allchildren() {
    ASTState state = state();
    if (A_allchildren_computed) {
      return A_allchildren_value;
    }
    if (A_allchildren_visited) {
      throw new RuntimeException("Circular definition of attribute A.allchildren().");
    }
    A_allchildren_visited = true;
    state().enterLazyAttribute();
    A_allchildren_value = allchildren_compute();
    A_allchildren_computed = true;
    state().leaveLazyAttribute();
    A_allchildren_visited = false;
    return A_allchildren_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure(group="A_allchildren") private ArrayList<String> allchildren_compute() {
    ASTNode node = this;
    while (node != null && !(node instanceof A)) {
      node = node.getParent();
    }
    A root = (A) node;
    root.survey_A_allchildren();
    ArrayList<String> _computedValue = new ArrayList<String>();
    if (root.contributorMap_A_allchildren.containsKey(this)) {
      for (ASTNode contributor : root.contributorMap_A_allchildren.get(this)) {
        contributor.contributeTo_A_allchildren(_computedValue);
      }
    }
    return _computedValue;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="A_allchildren") protected boolean A_allchildren_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="A_allchildren") protected ArrayList<String> A_allchildren_value;

}

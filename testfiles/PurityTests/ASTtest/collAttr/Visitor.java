package collAttr;

import java.util.*;
/**
 * @ast interface
 * @aspect Visitor
 * @declaredat testfiles\\source\\collAttr\\Visitor.jrag:6
 */
public interface Visitor {

		 
		public Object visit(A node, Object data);

		 
		public Object visit(B node, Object data);

		 
		public Object visit(C node, Object data);

		 
		public Object visit(List node, Object data);

		 
		public Object visit(D node, Object data);

		 
		public Object visit(E node, Object data);

		 
		public Object visit(Tree node, Object data);

		 
		public Object visit(Opt node, Object data);

		 
		public Object visit(Numeral node, Object data);
}

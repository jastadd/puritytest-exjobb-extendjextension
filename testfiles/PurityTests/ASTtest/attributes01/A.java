/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
import java.util.HashSet;
/**
 * @ast node
 * @declaredat tests\\annotation\\generated\\attributes01\\Test.ast:1
 * @production A : {@link ASTNode} ::= <span class="component">{@link B}</span>;

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
	/**
	 * @declaredat ASTNode:1
	 */
	public A() {
		super();
	}
	/**
	 * Initializes the child array to the correct size.
	 * Initializes List and Opt nta children.
	 * @apilevel internal
	 * @ast method
	 * @declaredat ASTNode:10
	 */
	public void init$Children() {
		children = new ASTNode[1];
	}
	/**
	 * @declaredat ASTNode:13
	 */
	public A(B p0) {
		setChild(p0, 0);
	}
	/** @apilevel low-level 
	 * @declaredat ASTNode:17
	 */
	protected int numChildren() {
		return 1;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:21
	 */
	public void flushAttrCache() {
		super.flushAttrCache();
		ntaA_reset();
		ntaParA_int_reset();
		circularA_reset();
		circularParA_int_reset();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:29
	 */
	public void flushCollectionCache() {
		super.flushCollectionCache();
		A_collA_visited = false;
		A_collA_computed = false;
		
		A_collA_value = null;
		contributorMap_A_collA = null;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:38
	 */
	public A clone() throws CloneNotSupportedException {
		A node = (A) super.clone();
		return node;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:43
	 */
	public A copy() {
		try {
			A node = (A) clone();
			node.parent = null;
			if (children != null) {
				node.children = (ASTNode[]) children.clone();
			}
			return node;
		} catch (CloneNotSupportedException e) {
			throw new Error("Error: clone not supported for " + getClass().getName());
		}
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @deprecated Please use treeCopy or treeCopyNoTransform instead
	 * @declaredat ASTNode:62
	 */
	@Deprecated
	public A fullCopy() {
		return treeCopyNoTransform();
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:72
	 */
	public A treeCopyNoTransform() {
		A tree = (A) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) children[i];
				if (child != null) {
					child = child.treeCopyNoTransform();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The subtree of this node is traversed to trigger rewrites before copy.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:92
	 */
	public A treeCopy() {
		A tree = (A) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) getChild(i);
				if (child != null) {
					child = child.treeCopy();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:106
	 */
	protected boolean is$Equal(ASTNode node) {
		return super.is$Equal(node);    
	}
	/**
	 * Replaces the B child.
	 * @param node The new node to replace the B child.
	 * @apilevel high-level
	 */
	public void setB(B node) {
		setChild(node, 0);
	}
	/**
	 * Retrieves the B child.
	 * @return The current node used as the B child.
	 * @apilevel high-level
	 */
	@ASTNodeAnnotation.Child(name="B")
	public B getB() {
		return (B) getChild(0);
	}
	/**
	 * Retrieves the B child.
	 * <p><em>This method does not invoke AST transformations.</em></p>
	 * @return The current node used as the B child.
	 * @apilevel low-level
	 */
	public B getBNoTransform() {
		return (B) getChildNoTransform(0);
	}
	/**
	 * @aspect <NoAspect>
	 * @declaredat tests\\annotation\\generated\\attributes01\\Test.jrag:13
	 */
	protected java.util.Map<ASTNode, java.util.Set<ASTNode>> contributorMap_A_collA = null;

	protected void survey_A_collA() {
		if (contributorMap_A_collA == null) {
			contributorMap_A_collA = new java.util.IdentityHashMap<ASTNode, java.util.Set<ASTNode>>();
			collect_contributors_A_collA(this, contributorMap_A_collA);
		}
	}

/** @apilevel internal */
@SideEffect.Secret(group="synA") protected boolean synA_visited = false;
	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\annotation\\generated\\attributes01\\Test.jrag:4
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\annotation\\generated\\attributes01\\Test.jrag:4")
	@SideEffect.Pure(group="synA") public boolean synA() {
		if (synA_visited) {
			throw new RuntimeException("Circular definition of attribute A.synA().");
		}
		synA_visited = true;
		boolean synA_value = true;
		synA_visited = false;
		return synA_value;
	}
/** @apilevel internal */
@SideEffect.Secret(group="synParA_int") protected java.util.Set synParA_int_visited;
	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\annotation\\generated\\attributes01\\Test.jrag:5
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\annotation\\generated\\attributes01\\Test.jrag:5")
	@SideEffect.Pure(group="synParA_int") public boolean synParA(int i) {
		Object _parameters = i;
		if (synParA_int_visited == null) synParA_int_visited = new java.util.HashSet(4);
		if (synParA_int_visited.contains(_parameters)) {
			throw new RuntimeException("Circular definition of attribute A.synParA(int).");
		}
		synParA_int_visited.add(_parameters);
		boolean synParA_int_value = true;
		synParA_int_visited.remove(_parameters);
		return synParA_int_value;
	}
/** @apilevel internal */
@SideEffect.Secret(group="ntaA") protected boolean ntaA_visited = false;
	/** @apilevel internal */
	private void ntaA_reset() {
		ntaA_computed = false;
		
		ntaA_value = null;
		ntaA_visited = false;
	}
	/** @apilevel internal */
	@SideEffect.Secret(group="ntaA") protected boolean ntaA_computed = false;

	/** @apilevel internal */
	@SideEffect.Secret(group="ntaA") protected B ntaA_value;

	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\annotation\\generated\\attributes01\\Test.jrag:7
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\annotation\\generated\\attributes01\\Test.jrag:7")
	@SideEffect.Pure(group="ntaA") public B ntaA() {
		ASTState state = state();
		if (ntaA_computed) {
			return ntaA_value;
		}
		if (ntaA_visited) {
			throw new RuntimeException("Circular definition of attribute A.ntaA().");
		}
		ntaA_visited = true;
		state().enterLazyAttribute();
		ntaA_value = new B();
		ntaA_value.setParent(this);
		ntaA_computed = true;
		state().leaveLazyAttribute();
		ntaA_visited = false;
		return ntaA_value;
	}
/** @apilevel internal */
@SideEffect.Secret(group="ntaParA_int") protected java.util.Set ntaParA_int_visited;
	/** @apilevel internal */
	private void ntaParA_int_reset() {
		ntaParA_int_values = null;
		ntaParA_int_proxy = null;
		ntaParA_int_visited = null;
	}
	/** @apilevel internal */
	 @SideEffect.Secret(group="ntaParA_int") protected ASTNode ntaParA_int_proxy;
	/** @apilevel internal */
	 @SideEffect.Secret(group="ntaParA_int") protected java.util.Map ntaParA_int_values;

	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\annotation\\generated\\attributes01\\Test.jrag:8
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\annotation\\generated\\attributes01\\Test.jrag:8")
	@SideEffect.Pure(group="ntaParA_int") public B ntaParA(int i) {
		Object _parameters = i;
		if (ntaParA_int_visited == null) ntaParA_int_visited = new java.util.HashSet(4);
		if (ntaParA_int_values == null) ntaParA_int_values = new java.util.HashMap(4);
		ASTState state = state();
		if (ntaParA_int_values.containsKey(_parameters)) {
			return (B) ntaParA_int_values.get(_parameters);
		}
		if (ntaParA_int_visited.contains(_parameters)) {
			throw new RuntimeException("Circular definition of attribute A.ntaParA(int).");
		}
		ntaParA_int_visited.add(_parameters);
		state().enterLazyAttribute();
		B ntaParA_int_value = new B();
		if (ntaParA_int_proxy == null) {
			ntaParA_int_proxy = new ASTNode();
			ntaParA_int_proxy.setParent(this);
		}
		if (ntaParA_int_value != null) {
			ntaParA_int_value.setParent(ntaParA_int_proxy);
		}
		ntaParA_int_values.put(_parameters, ntaParA_int_value);
		state().leaveLazyAttribute();
		ntaParA_int_visited.remove(_parameters);
		return ntaParA_int_value;
	}
/** @apilevel internal */
protected ASTState.Cycle circularA_cycle = null;
	/** @apilevel internal */
	private void circularA_reset() {
		circularA_computed = false;
		circularA_initialized = false;
		circularA_cycle = null;
	}
	/** @apilevel internal */
	@SideEffect.Secret(group="circularA") protected boolean circularA_computed = false;

	/** @apilevel internal */
	@SideEffect.Secret(group="circularA") protected boolean circularA_value;
	/** @apilevel internal */
	@SideEffect.Secret(group="circularA") protected boolean circularA_initialized = false;
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\annotation\\generated\\attributes01\\Test.jrag:10")
	@SideEffect.Pure(group="circularA") public boolean circularA() {
		if (circularA_computed) {
			return circularA_value;
		}
		ASTState state = state();
		if (!circularA_initialized) {
			circularA_initialized = true;
			circularA_value = true;
		}
		if (!state.inCircle() || state.calledByLazyAttribute()) {
			state.enterCircle();
			do {
				circularA_cycle = state.nextCycle();
				boolean new_circularA_value = circularA();
				if (new_circularA_value != circularA_value) {
					state.setChangeInCycle();
				}
				circularA_value = new_circularA_value;
			} while (state.testAndClearChangeInCycle());
			circularA_computed = true;
			state.startLastCycle();
			boolean $tmp = circularA();

			state.leaveCircle();
		} else if (circularA_cycle != state.cycle()) {
			circularA_cycle = state.cycle();
			if (state.lastCycle()) {
				circularA_computed = true;
				boolean new_circularA_value = circularA();
				return new_circularA_value;
			}
			boolean new_circularA_value = circularA();
			if (new_circularA_value != circularA_value) {
				state.setChangeInCycle();
			}
			circularA_value = new_circularA_value;
		} else {
		}
		return circularA_value;
	}
	/** @apilevel internal */
	private void circularParA_int_reset() {
		circularParA_int_values = null;
	}
	protected java.util.Map circularParA_int_values;
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\annotation\\generated\\attributes01\\Test.jrag:11")
	@SideEffect.Pure(group="circularParA_int") public boolean circularParA(int i) {
		Object _parameters = i;
		if (circularParA_int_values == null) circularParA_int_values = new java.util.HashMap(4);
		ASTState.CircularValue _value;
		if (circularParA_int_values.containsKey(_parameters)) {
			Object _cache = circularParA_int_values.get(_parameters);
			if (!(_cache instanceof ASTState.CircularValue)) {
				return (Boolean) _cache;
			} else {
				_value = (ASTState.CircularValue) _cache;
			}
		} else {
			_value = new ASTState.CircularValue();
			circularParA_int_values.put(_parameters, _value);
			_value.value = true;
		}
		ASTState state = state();
		if (!state.inCircle() || state.calledByLazyAttribute()) {
			state.enterCircle();
			boolean new_circularParA_int_value;
			do {
				_value.cycle = state.nextCycle();
				new_circularParA_int_value = circularParA(i);
				if (new_circularParA_int_value != ((Boolean)_value.value)) {
					state.setChangeInCycle();
					_value.value = new_circularParA_int_value;
				}
			} while (state.testAndClearChangeInCycle());
			circularParA_int_values.put(_parameters, new_circularParA_int_value);
			state.startLastCycle();
			boolean $tmp = circularParA(i);

			state.leaveCircle();
			return new_circularParA_int_value;
		} else if (_value.cycle != state.cycle()) {
			_value.cycle = state.cycle();
			boolean new_circularParA_int_value = circularParA(i);
			if (state.lastCycle()) {
				circularParA_int_values.put(_parameters, new_circularParA_int_value);
			}
			if (new_circularParA_int_value != ((Boolean)_value.value)) {
				state.setChangeInCycle();
				_value.value = new_circularParA_int_value;
			}
			return new_circularParA_int_value;
		} else {
			return (Boolean) _value.value;
		}
	}
	/**
	 * @declaredat tests\\annotation\\generated\\attributes01\\Test.jrag:17
	 * @apilevel internal
	 */
 @SideEffect.Pure public boolean Define_inhB(ASTNode _callerNode, ASTNode _childNode) {
		if (_callerNode == getBNoTransform()) {
			// @declaredat tests\\annotation\\generated\\attributes01\\Test.jrag:18
			return true;
		}
		else {
			return getParent().Define_inhB(this, _callerNode);
		}
	}
	protected boolean canDefine_inhB(ASTNode _callerNode, ASTNode _childNode) {
		return true;
	}
	/**
	 * @declaredat tests\\annotation\\generated\\attributes01\\Test.jrag:19
	 * @apilevel internal
	 */
 @SideEffect.Pure public boolean Define_inhParB(ASTNode _callerNode, ASTNode _childNode, int i) {
		if (_callerNode == getBNoTransform()) {
			// @declaredat tests\\annotation\\generated\\attributes01\\Test.jrag:20
			return true;
		}
		else {
			return getParent().Define_inhParB(this, _callerNode, i);
		}
	}
	protected boolean canDefine_inhParB(ASTNode _callerNode, ASTNode _childNode, int i) {
		return true;
	}
/** @apilevel internal */
@SideEffect.Secret(group="A_collA") protected boolean A_collA_visited = false;
	/**
	 * @attribute coll
	 * @aspect Test
	 * @declaredat tests\\annotation\\generated\\attributes01\\Test.jrag:13
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.COLL)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\annotation\\generated\\attributes01\\Test.jrag:13")
	@SideEffect.Pure(group="A_collA") public HashSet<B> collA() {
		ASTState state = state();
		if (A_collA_computed) {
			return A_collA_value;
		}
		if (A_collA_visited) {
			throw new RuntimeException("Circular definition of attribute A.collA().");
		}
		A_collA_visited = true;
		state().enterLazyAttribute();
		A_collA_value = collA_compute();
		A_collA_computed = true;
		state().leaveLazyAttribute();
		A_collA_visited = false;
		return A_collA_value;
	}
	/** @apilevel internal */
	@SideEffect.Pure(group="A_collA") private HashSet<B> collA_compute() {
		ASTNode node = this;
		while (node != null && !(node instanceof A)) {
			node = node.getParent();
		}
		A root = (A) node;
		root.survey_A_collA();
		HashSet<B> _computedValue = new HashSet<B>();
		if (root.contributorMap_A_collA.containsKey(this)) {
			for (ASTNode contributor : root.contributorMap_A_collA.get(this)) {
				contributor.contributeTo_A_collA(_computedValue);
			}
		}
		return _computedValue;
	}
	/** @apilevel internal */
	@SideEffect.Secret(group="A_collA") protected boolean A_collA_computed = false;

	/** @apilevel internal */
	@SideEffect.Secret(group="A_collA") protected HashSet<B> A_collA_value;

}

/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
import java.util.HashSet;
/**
 * @ast node
 * @declaredat tests\\annotation\\generated\\attributes01\\Test.ast:2
 * @production B : {@link ASTNode} ::= <span class="component">&lt;ID:String&gt;</span>;

 */
public class B extends ASTNode<ASTNode> implements Cloneable {
	/**
	 * @declaredat ASTNode:1
	 */
	public B() {
		super();
	}
	/**
	 * Initializes the child array to the correct size.
	 * Initializes List and Opt nta children.
	 * @apilevel internal
	 * @ast method
	 * @declaredat ASTNode:10
	 */
	public void init$Children() {
	}
	/**
	 * @declaredat ASTNode:12
	 */
	public B(String p0) {
		setID(p0);
	}
	/** @apilevel low-level 
	 * @declaredat ASTNode:16
	 */
	protected int numChildren() {
		return 0;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:20
	 */
	public void flushAttrCache() {
		super.flushAttrCache();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:24
	 */
	public void flushCollectionCache() {
		super.flushCollectionCache();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:28
	 */
	public B clone() throws CloneNotSupportedException {
		B node = (B) super.clone();
		return node;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:33
	 */
	public B copy() {
		try {
			B node = (B) clone();
			node.parent = null;
			if (children != null) {
				node.children = (ASTNode[]) children.clone();
			}
			return node;
		} catch (CloneNotSupportedException e) {
			throw new Error("Error: clone not supported for " + getClass().getName());
		}
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @deprecated Please use treeCopy or treeCopyNoTransform instead
	 * @declaredat ASTNode:52
	 */
	@Deprecated
	public B fullCopy() {
		return treeCopyNoTransform();
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:62
	 */
	public B treeCopyNoTransform() {
		B tree = (B) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) children[i];
				if (child != null) {
					child = child.treeCopyNoTransform();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The subtree of this node is traversed to trigger rewrites before copy.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:82
	 */
	public B treeCopy() {
		B tree = (B) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) getChild(i);
				if (child != null) {
					child = child.treeCopy();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:96
	 */
	protected boolean is$Equal(ASTNode node) {
		return super.is$Equal(node) && (tokenString_ID == ((B) node).tokenString_ID);    
	}
	/**
	 * Replaces the lexeme ID.
	 * @param value The new value for the lexeme ID.
	 * @apilevel high-level
	 */
	public void setID(String value) {
		tokenString_ID = value;
	}
	/** @apilevel internal 
	 */
	protected String tokenString_ID;
	/**
	 * Retrieves the value for the lexeme ID.
	 * @return The value for the lexeme ID.
	 * @apilevel high-level
	 */
	@ASTNodeAnnotation.Token(name="ID")
	public String getID() {
		return tokenString_ID != null ? tokenString_ID : "";
	}
	/**
	 * @attribute inh
	 * @aspect Test
	 * @declaredat tests\\annotation\\generated\\attributes01\\Test.jrag:17
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\annotation\\generated\\attributes01\\Test.jrag:17")
	@SideEffect.Pure(group="inhB") public boolean inhB() {
		if (inhB_visited) {
			throw new RuntimeException("Circular definition of attribute B.inhB().");
		}
		inhB_visited = true;
		boolean inhB_value = getParent().Define_inhB(this, null);
		inhB_visited = false;
		return inhB_value;
	}
/** @apilevel internal */
@SideEffect.Secret(group="inhB") protected boolean inhB_visited = false;
	/**
	 * @attribute inh
	 * @aspect Test
	 * @declaredat tests\\annotation\\generated\\attributes01\\Test.jrag:19
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\annotation\\generated\\attributes01\\Test.jrag:19")
	@SideEffect.Pure(group="inhParB_int") public boolean inhParB(int i) {
		Object _parameters = i;
		if (inhParB_int_visited == null) inhParB_int_visited = new java.util.HashSet(4);
		if (inhParB_int_visited.contains(_parameters)) {
			throw new RuntimeException("Circular definition of attribute B.inhParB(int).");
		}
		inhParB_int_visited.add(_parameters);
		boolean inhParB_int_value = getParent().Define_inhParB(this, null, i);
		inhParB_int_visited.remove(_parameters);
		return inhParB_int_value;
	}
/** @apilevel internal */
@SideEffect.Secret(group="inhParB_int") protected java.util.Set inhParB_int_visited;
	@SideEffect.Pure protected void collect_contributors_A_collA(A _root, java.util.Map<ASTNode, java.util.Set<ASTNode>> _map) {
		// @declaredat tests\\annotation\\generated\\attributes01\\Test.jrag:14
		{
			A target = (A) (getParent());
			java.util.Set<ASTNode> contributors = _map.get(target);
			if (contributors == null) {
				contributors = new java.util.LinkedHashSet<ASTNode>();
				_map.put((ASTNode) target, contributors);
			}
			contributors.add(this);
		}
		super.collect_contributors_A_collA(_root, _map);
	}
	@SideEffect.Pure protected void contributeTo_A_collA(HashSet<B> collection) {
		super.contributeTo_A_collA(collection);
		collection.add(this);
	}
}

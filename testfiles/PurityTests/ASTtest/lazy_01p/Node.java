/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
/**
 * @ast node
 * @declaredat tests\\syn\\lazy_01p\\Test.ast:1
 * @production Node : {@link ASTNode};

 */
public class Node extends ASTNode<ASTNode> implements Cloneable {
	/**
	 * @aspect Test
	 * @declaredat tests\\syn\\lazy_01p\\Test.jrag:2
	 */
	public int lazyEvalCount = 0;
	/**
	 * @aspect Test
	 * @declaredat tests\\syn\\lazy_01p\\Test.jrag:3
	 */
	public int nonLazyEvalCount = 0;
	/**
	 * @declaredat ASTNode:1
	 */
	public Node() {
		super();
	}
	/**
	 * Initializes the child array to the correct size.
	 * Initializes List and Opt nta children.
	 * @apilevel internal
	 * @ast method
	 * @declaredat ASTNode:10
	 */
	public void init$Children() {
	}
	/** @apilevel low-level 
	 * @declaredat ASTNode:13
	 */
	protected int numChildren() {
		return 0;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:17
	 */
	public void flushAttrCache() {
		super.flushAttrCache();
		lazyAttr_reset();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:22
	 */
	public void flushCollectionCache() {
		super.flushCollectionCache();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:26
	 */
	public Node clone() throws CloneNotSupportedException {
		Node node = (Node) super.clone();
		return node;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:31
	 */
	public Node copy() {
		try {
			Node node = (Node) clone();
			node.parent = null;
			if (children != null) {
				node.children = (ASTNode[]) children.clone();
			}
			return node;
		} catch (CloneNotSupportedException e) {
			throw new Error("Error: clone not supported for " + getClass().getName());
		}
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @deprecated Please use treeCopy or treeCopyNoTransform instead
	 * @declaredat ASTNode:50
	 */
	@Deprecated
	public Node fullCopy() {
		return treeCopyNoTransform();
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:60
	 */
	public Node treeCopyNoTransform() {
		Node tree = (Node) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) children[i];
				if (child != null) {
					child = child.treeCopyNoTransform();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The subtree of this node is traversed to trigger rewrites before copy.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:80
	 */
	public Node treeCopy() {
		Node tree = (Node) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) getChild(i);
				if (child != null) {
					child = child.treeCopy();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:94
	 */
	protected boolean is$Equal(ASTNode node) {
		return super.is$Equal(node);    
	}
/** @apilevel internal */
@SideEffect.Secret(group="lazyAttr") protected boolean lazyAttr_visited = false;
	/** @apilevel internal */
	private void lazyAttr_reset() {
		lazyAttr_computed = false;
		lazyAttr_visited = false;
	}
	/** @apilevel internal */
	@SideEffect.Secret(group="lazyAttr") protected boolean lazyAttr_computed = false;

	/** @apilevel internal */
	@SideEffect.Secret(group="lazyAttr") protected int lazyAttr_value;

	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\lazy_01p\\Test.jrag:5
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\lazy_01p\\Test.jrag:5")
	@SideEffect.Pure(group="lazyAttr") public int lazyAttr() {
		ASTState state = state();
		if (lazyAttr_computed) {
			return lazyAttr_value;
		}
		if (lazyAttr_visited) {
			throw new RuntimeException("Circular definition of attribute Node.lazyAttr().");
		}
		lazyAttr_visited = true;
		state().enterLazyAttribute();
		lazyAttr_value = lazyAttr_compute();
		lazyAttr_computed = true;
		state().leaveLazyAttribute();
		lazyAttr_visited = false;
		return lazyAttr_value;
	}
	/** @apilevel internal */
	@SideEffect.Pure private int lazyAttr_compute() {
			lazyEvalCount += 1;// side effect to test attribute evaluations
			return 0;
		}
/** @apilevel internal */
@SideEffect.Secret(group="nonLazyAttr") protected boolean nonLazyAttr_visited = false;
	/**
	 * @attribute syn
	 * @aspect Test
	 * @declaredat tests\\syn\\lazy_01p\\Test.jrag:10
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test", declaredAt="tests\\syn\\lazy_01p\\Test.jrag:10")
	@SideEffect.Pure(group="nonLazyAttr") public int nonLazyAttr() {
		if (nonLazyAttr_visited) {
			throw new RuntimeException("Circular definition of attribute Node.nonLazyAttr().");
		}
		nonLazyAttr_visited = true;
		try {
				nonLazyEvalCount += 1;// side effect to test attribute evaluations
				return 0;
			}
		finally {
			nonLazyAttr_visited = false;
		}
	}
}

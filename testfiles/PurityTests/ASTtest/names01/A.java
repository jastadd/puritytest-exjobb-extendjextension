/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
/**
 * @ast node
 * @declaredat tests\\annotation\\generated\\names01\\Test.ast:1
 * @production A : {@link ASTNode} ::= <span class="component">B1:{@link B}</span> <span class="component">B2:{@link B}*</span> <span class="component">[B3:{@link B}]</span>;

 */
public class A extends ASTNode<ASTNode> implements Cloneable {
	/**
	 * @declaredat ASTNode:1
	 */
	public A() {
		super();
	}
	/**
	 * Initializes the child array to the correct size.
	 * Initializes List and Opt nta children.
	 * @apilevel internal
	 * @ast method
	 * @declaredat ASTNode:10
	 */
	@SideEffect.Ignore public void init$Children() {
		children = new ASTNode[3];
		setChild(new List(), 1);
		setChild(new Opt(), 2);
	}
	/**
	 * @declaredat ASTNode:15
	 */
	public A(B p0, List<B> p1, Opt<B> p2) {
		setChild(p0, 0);
		setChild(p1, 1);
		setChild(p2, 2);
	}
	/** @apilevel low-level 
	 * @declaredat ASTNode:21
	 */
	@SideEffect.Pure protected int numChildren() {
		return 3;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:25
	 */
	@SideEffect.Ignore public void flushAttrCache() {
		super.flushAttrCache();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:29
	 */
	@SideEffect.Ignore public void flushCollectionCache() {
		super.flushCollectionCache();
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:33
	 */
	@SideEffect.Fresh public A clone() throws CloneNotSupportedException {
		A node = (A) super.clone();
		return node;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:38
	 */
	@SideEffect.Fresh(group="_ASTNode") public A copy() {
		try {
			A node = (A) clone();
			node.parent = null;
			if (children != null) {
				node.children = (ASTNode[]) children.clone();
			}
			return node;
		} catch (CloneNotSupportedException e) {
			throw new Error("Error: clone not supported for " + getClass().getName());
		}
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @deprecated Please use treeCopy or treeCopyNoTransform instead
	 * @declaredat ASTNode:57
	 */
	@Deprecated
	@SideEffect.Fresh(group="_ASTNode") public A fullCopy() {
		return treeCopyNoTransform();
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:67
	 */
	@SideEffect.Fresh(group="_ASTNode") public A treeCopyNoTransform() {
		A tree = (A) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) children[i];
				if (child != null) {
					child = child.treeCopyNoTransform();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/**
	 * Create a deep copy of the AST subtree at this node.
	 * The subtree of this node is traversed to trigger rewrites before copy.
	 * The copy is dangling, i.e. has no parent.
	 * @return dangling copy of the subtree at this node
	 * @apilevel low-level
	 * @declaredat ASTNode:87
	 */
	@SideEffect.Fresh(group="_ASTNode") public A treeCopy() {
		A tree = (A) copy();
		if (children != null) {
			for (int i = 0; i < children.length; ++i) {
				ASTNode child = (ASTNode) getChild(i);
				if (child != null) {
					child = child.treeCopy();
					tree.setChild(child, i);
				}
			}
		}
		return tree;
	}
	/** @apilevel internal 
	 * @declaredat ASTNode:101
	 */
	@SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
		return super.is$Equal(node);    
	}
	/**
	 * Replaces the B1 child.
	 * @param node The new node to replace the B1 child.
	 * @apilevel high-level
	 */
	public void setB1(B node) {
		setChild(node, 0);
	}
	/**
	 * Retrieves the B1 child.
	 * @return The current node used as the B1 child.
	 * @apilevel high-level
	 */
	@ASTNodeAnnotation.Child(name="B1")
	@SideEffect.Pure public B getB1() {
		return (B) getChild(0);
	}
	/**
	 * Retrieves the B1 child.
	 * <p><em>This method does not invoke AST transformations.</em></p>
	 * @return The current node used as the B1 child.
	 * @apilevel low-level
	 */
	@SideEffect.Pure public B getB1NoTransform() {
		return (B) getChildNoTransform(0);
	}
	/**
	 * Replaces the B2 list.
	 * @param list The new list node to be used as the B2 list.
	 * @apilevel high-level
	 */
	public void setB2List(List<B> list) {
		setChild(list, 1);
	}
	/**
	 * Retrieves the number of children in the B2 list.
	 * @return Number of children in the B2 list.
	 * @apilevel high-level
	 */
	@SideEffect.Pure public int getNumB2() {
		return getB2List().getNumChild();
	}
	/**
	 * Retrieves the number of children in the B2 list.
	 * Calling this method will not trigger rewrites.
	 * @return Number of children in the B2 list.
	 * @apilevel low-level
	 */
	@SideEffect.Pure public int getNumB2NoTransform() {
		return getB2ListNoTransform().getNumChildNoTransform();
	}
	/**
	 * Retrieves the element at index {@code i} in the B2 list.
	 * @param i Index of the element to return.
	 * @return The element at position {@code i} in the B2 list.
	 * @apilevel high-level
	 */
	@SideEffect.Pure public B getB2(int i) {
		return (B) getB2List().getChild(i);
	}
	/**
	 * Check whether the B2 list has any children.
	 * @return {@code true} if it has at least one child, {@code false} otherwise.
	 * @apilevel high-level
	 */
	@SideEffect.Pure public boolean hasB2() {
		return getB2List().getNumChild() != 0;
	}
	/**
	 * Append an element to the B2 list.
	 * @param node The element to append to the B2 list.
	 * @apilevel high-level
	 */
	@SideEffect.Local(group="_ASTNode") public void addB2(B node) {
		List<B> list = (parent == null) ? getB2ListNoTransform() : getB2List();
		list.addChild(node);
	}
	/** @apilevel low-level 
	 */
	public void addB2NoTransform(B node) {
		List<B> list = getB2ListNoTransform();
		list.addChild(node);
	}
	/**
	 * Replaces the B2 list element at index {@code i} with the new node {@code node}.
	 * @param node The new node to replace the old list element.
	 * @param i The list index of the node to be replaced.
	 * @apilevel high-level
	 */
	public void setB2(B node, int i) {
		List<B> list = getB2List();
		list.setChild(node, i);
	}
	/**
	 * Retrieves the B2 list.
	 * @return The node representing the B2 list.
	 * @apilevel high-level
	 */
	@ASTNodeAnnotation.ListChild(name="B2")
	@SideEffect.Pure(group="_ASTNode") public List<B> getB2List() {
		List<B> list = (List<B>) getChild(1);
		return list;
	}
	/**
	 * Retrieves the B2 list.
	 * <p><em>This method does not invoke AST transformations.</em></p>
	 * @return The node representing the B2 list.
	 * @apilevel low-level
	 */
	@SideEffect.Pure public List<B> getB2ListNoTransform() {
		return (List<B>) getChildNoTransform(1);
	}
	/**
	 * @return the element at index {@code i} in the B2 list without
	 * triggering rewrites.
	 */
	@SideEffect.Pure public B getB2NoTransform(int i) {
		return (B) getB2ListNoTransform().getChildNoTransform(i);
	}
	/**
	 * Retrieves the B2 list.
	 * @return The node representing the B2 list.
	 * @apilevel high-level
	 */
	@SideEffect.Pure public List<B> getB2s() {
		return getB2List();
	}
	/**
	 * Retrieves the B2 list.
	 * <p><em>This method does not invoke AST transformations.</em></p>
	 * @return The node representing the B2 list.
	 * @apilevel low-level
	 */
	@SideEffect.Pure public List<B> getB2sNoTransform() {
		return getB2ListNoTransform();
	}
	/**
	 * Replaces the optional node for the B3 child. This is the <code>Opt</code>
	 * node containing the child B3, not the actual child!
	 * @param opt The new node to be used as the optional node for the B3 child.
	 * @apilevel low-level
	 */
	public void setB3Opt(Opt<B> opt) {
		setChild(opt, 2);
	}
	/**
	 * Replaces the (optional) B3 child.
	 * @param node The new node to be used as the B3 child.
	 * @apilevel high-level
	 */
	public void setB3(B node) {
		getB3Opt().setChild(node, 0);
	}
	/**
	 * Check whether the optional B3 child exists.
	 * @return {@code true} if the optional B3 child exists, {@code false} if it does not.
	 * @apilevel high-level
	 */
	@SideEffect.Pure public boolean hasB3() {
		return getB3Opt().getNumChild() != 0;
	}
	/**
	 * Retrieves the (optional) B3 child.
	 * @return The B3 child, if it exists. Returns {@code null} otherwise.
	 * @apilevel low-level
	 */
	@SideEffect.Pure public B getB3() {
		return (B) getB3Opt().getChild(0);
	}
	/**
	 * Retrieves the optional node for the B3 child. This is the <code>Opt</code> node containing the child B3, not the actual child!
	 * @return The optional node for child the B3 child.
	 * @apilevel low-level
	 */
	@ASTNodeAnnotation.OptChild(name="B3")
	@SideEffect.Pure public Opt<B> getB3Opt() {
		return (Opt<B>) getChild(2);
	}
	/**
	 * Retrieves the optional node for child B3. This is the <code>Opt</code> node containing the child B3, not the actual child!
	 * <p><em>This method does not invoke AST transformations.</em></p>
	 * @return The optional node for child B3.
	 * @apilevel low-level
	 */
	@SideEffect.Pure public Opt<B> getB3OptNoTransform() {
		return (Opt<B>) getChildNoTransform(2);
	}
}

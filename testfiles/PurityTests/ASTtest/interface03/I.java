/**
 * @ast interface
 * @aspect Test1
 * @declaredat tests\\syn\\interface03\\Test.jrag:2
 */
 interface I {
	/**
	 * @attribute syn
	 * @aspect Test1
	 * @declaredat tests\\syn\\interface03\\Test.jrag:5
	 */
	@ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
	@ASTNodeAnnotation.Source(aspect="Test1", declaredAt="tests\\syn\\interface03\\Test.jrag:5")
	@SideEffect.Pure(group="attr") public boolean attr();
}

/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package AST;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.ast:34
 * @production InterfaceDeclSubstituted : {@link InterfaceDecl} ::= <span class="component">&lt;Original:TypeDecl&gt;</span>;

 */
public class InterfaceDeclSubstituted extends InterfaceDecl implements Cloneable, MemberSubstitutor {
  /**
   * @declaredat ASTNode:1
   */
  public InterfaceDeclSubstituted() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[3];
    setChild(new List(), 1);
    setChild(new List(), 2);
  }
  /**
   * @declaredat ASTNode:15
   */
  public InterfaceDeclSubstituted(Modifiers p0, String p1, List<Access> p2, List<BodyDecl> p3, TypeDecl p4) {
    setChild(p0, 0);
    setID(p1);
    setChild(p2, 1);
    setChild(p3, 2);
    setOriginal(p4);
  }
  /**
   * @declaredat ASTNode:22
   */
  public InterfaceDeclSubstituted(Modifiers p0, beaver.Symbol p1, List<Access> p2, List<BodyDecl> p3, TypeDecl p4) {
    setChild(p0, 0);
    setID(p1);
    setChild(p2, 1);
    setChild(p3, 2);
    setOriginal(p4);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:30
   */
  @SideEffect.Pure protected int numChildren() {
    return 3;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:36
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    sourceTypeDecl_reset();
    instanceOf_TypeDecl_reset();
    subtype_TypeDecl_reset();
    typeDescriptor_reset();
    constantPoolName_reset();
    uniqueIndex_reset();
    localMethodsSignatureMap_reset();
    localFields_String_reset();
    localTypeDecls_String_reset();
    constructors_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:54
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:58
   */
  @SideEffect.Fresh public InterfaceDeclSubstituted clone() throws CloneNotSupportedException {
    InterfaceDeclSubstituted node = (InterfaceDeclSubstituted) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:63
   */
  @SideEffect.Fresh(group="_ASTNode") public InterfaceDeclSubstituted copy() {
    try {
      InterfaceDeclSubstituted node = (InterfaceDeclSubstituted) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:82
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public InterfaceDeclSubstituted fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:92
   */
  @SideEffect.Fresh(group="_ASTNode") public InterfaceDeclSubstituted treeCopyNoTransform() {
    InterfaceDeclSubstituted tree = (InterfaceDeclSubstituted) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:112
   */
  @SideEffect.Fresh(group="_ASTNode") public InterfaceDeclSubstituted treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:117
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_ID == ((InterfaceDeclSubstituted) node).tokenString_ID) && (tokenTypeDecl_Original == ((InterfaceDeclSubstituted) node).tokenTypeDecl_Original);    
  }
  /**
   * Replaces the Modifiers child.
   * @param node The new node to replace the Modifiers child.
   * @apilevel high-level
   */
  public void setModifiers(Modifiers node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Modifiers child.
   * @return The current node used as the Modifiers child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Modifiers")
  @SideEffect.Pure public Modifiers getModifiers() {
    return (Modifiers) getChild(0);
  }
  /**
   * Retrieves the Modifiers child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Modifiers child.
   * @apilevel low-level
   */
  public Modifiers getModifiersNoTransform() {
    return (Modifiers) getChildNoTransform(0);
  }
  /**
   * Replaces the lexeme ID.
   * @param value The new value for the lexeme ID.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setID(String value) {
    tokenString_ID = value;
  }
  /**
   * JastAdd-internal setter for lexeme ID using the Beaver parser.
   * @param symbol Symbol containing the new value for the lexeme ID
   * @apilevel internal
   */
  public void setID(beaver.Symbol symbol) {
    if (symbol.value != null && !(symbol.value instanceof String))
    throw new UnsupportedOperationException("setID is only valid for String lexemes");
    tokenString_ID = (String)symbol.value;
    IDstart = symbol.getStart();
    IDend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme ID.
   * @return The value for the lexeme ID.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="ID")
  @SideEffect.Pure(group="_ASTNode") public String getID() {
    return tokenString_ID != null ? tokenString_ID : "";
  }
  /**
   * Replaces the SuperInterfaceId list.
   * @param list The new list node to be used as the SuperInterfaceId list.
   * @apilevel high-level
   */
  public void setSuperInterfaceIdList(List<Access> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the SuperInterfaceId list.
   * @return Number of children in the SuperInterfaceId list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumSuperInterfaceId() {
    return getSuperInterfaceIdList().getNumChild();
  }
  /**
   * Retrieves the number of children in the SuperInterfaceId list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the SuperInterfaceId list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumSuperInterfaceIdNoTransform() {
    return getSuperInterfaceIdListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the SuperInterfaceId list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the SuperInterfaceId list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Access getSuperInterfaceId(int i) {
    return (Access) getSuperInterfaceIdList().getChild(i);
  }
  /**
   * Check whether the SuperInterfaceId list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasSuperInterfaceId() {
    return getSuperInterfaceIdList().getNumChild() != 0;
  }
  /**
   * Append an element to the SuperInterfaceId list.
   * @param node The element to append to the SuperInterfaceId list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addSuperInterfaceId(Access node) {
    List<Access> list = (parent == null) ? getSuperInterfaceIdListNoTransform() : getSuperInterfaceIdList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addSuperInterfaceIdNoTransform(Access node) {
    List<Access> list = getSuperInterfaceIdListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the SuperInterfaceId list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setSuperInterfaceId(Access node, int i) {
    List<Access> list = getSuperInterfaceIdList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the SuperInterfaceId list.
   * @return The node representing the SuperInterfaceId list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="SuperInterfaceId")
  @SideEffect.Pure(group="_ASTNode") public List<Access> getSuperInterfaceIdList() {
    List<Access> list = (List<Access>) getChild(1);
    return list;
  }
  /**
   * Retrieves the SuperInterfaceId list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SuperInterfaceId list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Access> getSuperInterfaceIdListNoTransform() {
    return (List<Access>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the SuperInterfaceId list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Access getSuperInterfaceIdNoTransform(int i) {
    return (Access) getSuperInterfaceIdListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the SuperInterfaceId list.
   * @return The node representing the SuperInterfaceId list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Access> getSuperInterfaceIds() {
    return getSuperInterfaceIdList();
  }
  /**
   * Retrieves the SuperInterfaceId list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SuperInterfaceId list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Access> getSuperInterfaceIdsNoTransform() {
    return getSuperInterfaceIdListNoTransform();
  }
  /**
   * Replaces the BodyDecl list.
   * @param list The new list node to be used as the BodyDecl list.
   * @apilevel high-level
   */
  public void setBodyDeclList(List<BodyDecl> list) {
    setChild(list, 2);
  }
  /**
   * Retrieves the number of children in the BodyDecl list.
   * @return Number of children in the BodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumBodyDecl() {
    return getBodyDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the BodyDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the BodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumBodyDeclNoTransform() {
    return getBodyDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the BodyDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the BodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public BodyDecl getBodyDecl(int i) {
    return (BodyDecl) getBodyDeclList().getChild(i);
  }
  /**
   * Check whether the BodyDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasBodyDecl() {
    return getBodyDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the BodyDecl list.
   * @param node The element to append to the BodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addBodyDecl(BodyDecl node) {
    List<BodyDecl> list = (parent == null) ? getBodyDeclListNoTransform() : getBodyDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addBodyDeclNoTransform(BodyDecl node) {
    List<BodyDecl> list = getBodyDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the BodyDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setBodyDecl(BodyDecl node, int i) {
    List<BodyDecl> list = getBodyDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the BodyDecl list.
   * @return The node representing the BodyDecl list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="BodyDecl")
  @SideEffect.Pure(group="_ASTNode") public List<BodyDecl> getBodyDeclList() {
    List<BodyDecl> list = (List<BodyDecl>) getChild(2);
    return list;
  }
  /**
   * Retrieves the BodyDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the BodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<BodyDecl> getBodyDeclListNoTransform() {
    return (List<BodyDecl>) getChildNoTransform(2);
  }
  /**
   * @return the element at index {@code i} in the BodyDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public BodyDecl getBodyDeclNoTransform(int i) {
    return (BodyDecl) getBodyDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the BodyDecl list.
   * @return The node representing the BodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<BodyDecl> getBodyDecls() {
    return getBodyDeclList();
  }
  /**
   * Retrieves the BodyDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the BodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<BodyDecl> getBodyDeclsNoTransform() {
    return getBodyDeclListNoTransform();
  }
  /**
   * Replaces the lexeme Original.
   * @param value The new value for the lexeme Original.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setOriginal(TypeDecl value) {
    tokenTypeDecl_Original = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected TypeDecl tokenTypeDecl_Original;
  /**
   * Retrieves the value for the lexeme Original.
   * @return The value for the lexeme Original.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Original")
  @SideEffect.Pure(group="_ASTNode") public TypeDecl getOriginal() {
    return tokenTypeDecl_Original;
  }
  /**
   * @attribute syn
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1068
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1068")
  @SideEffect.Pure(group="original") public TypeDecl original() {
    TypeDecl original_value = getOriginal().original();
    return original_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void sourceTypeDecl_reset() {
    sourceTypeDecl_computed = false;
    
    sourceTypeDecl_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="sourceTypeDecl") protected boolean sourceTypeDecl_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="sourceTypeDecl") protected TypeDecl sourceTypeDecl_value;

  /**
   * @attribute syn
   * @aspect SourceDeclarations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1266
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SourceDeclarations", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1266")
  @SideEffect.Pure(group="sourceTypeDecl") public TypeDecl sourceTypeDecl() {
    ASTState state = state();
    if (sourceTypeDecl_computed) {
      return sourceTypeDecl_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    sourceTypeDecl_value = original().sourceTypeDecl();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    sourceTypeDecl_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return sourceTypeDecl_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void instanceOf_TypeDecl_reset() {
    instanceOf_TypeDecl_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="instanceOf_TypeDecl") protected java.util.Map instanceOf_TypeDecl_values;

  /**
   * @attribute syn
   * @aspect TypeWideningAndIdentity
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:407
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeWideningAndIdentity", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:407")
  @SideEffect.Pure(group="instanceOf_TypeDecl") public boolean instanceOf(TypeDecl type) {
    Object _parameters = type;
    if (instanceOf_TypeDecl_values == null) instanceOf_TypeDecl_values = new java.util.HashMap(4);
    ASTState state = state();
    if (instanceOf_TypeDecl_values.containsKey(_parameters)) {
      return (Boolean) instanceOf_TypeDecl_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boolean instanceOf_TypeDecl_value = subtype(type);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    instanceOf_TypeDecl_values.put(_parameters, instanceOf_TypeDecl_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return instanceOf_TypeDecl_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void subtype_TypeDecl_reset() {
    subtype_TypeDecl_values = null;
  }
  protected java.util.Map subtype_TypeDecl_values;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="GenericsSubtype", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:405")
  @SideEffect.Pure(group="subtype_TypeDecl") public boolean subtype(TypeDecl type) {
    Object _parameters = type;
    if (subtype_TypeDecl_values == null) subtype_TypeDecl_values = new java.util.HashMap(4);
    ASTState.CircularValue _value;
    if (subtype_TypeDecl_values.containsKey(_parameters)) {
      Object _cache = subtype_TypeDecl_values.get(_parameters);
      if (!(_cache instanceof ASTState.CircularValue)) {
        return (Boolean) _cache;
      } else {
        _value = (ASTState.CircularValue) _cache;
      }
    } else {
      _value = new ASTState.CircularValue();
      subtype_TypeDecl_values.put(_parameters, _value);
      _value.value = true;
    }
    ASTState state = state();
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      int _boundaries = state.boundariesCrossed;
      boolean isFinal = this.is$Final();
      boolean new_subtype_TypeDecl_value;
      do {
        _value.cycle = state.nextCycle();
        new_subtype_TypeDecl_value = type.supertypeInterfaceDeclSubstituted(this);
        if (new_subtype_TypeDecl_value != ((Boolean)_value.value)) {
          state.setChangeInCycle();
          _value.value = new_subtype_TypeDecl_value;
        }
      } while (state.testAndClearChangeInCycle());
      if (isFinal && _boundaries == state().boundariesCrossed) {
        subtype_TypeDecl_values.put(_parameters, new_subtype_TypeDecl_value);
      } else {
        subtype_TypeDecl_values.remove(_parameters);
        state.startResetCycle();
        boolean $tmp = type.supertypeInterfaceDeclSubstituted(this);
      }
      state.leaveCircle();
      return new_subtype_TypeDecl_value;
    } else if (_value.cycle != state.cycle()) {
      _value.cycle = state.cycle();
      boolean new_subtype_TypeDecl_value = type.supertypeInterfaceDeclSubstituted(this);
      if (state.resetCycle()) {
        subtype_TypeDecl_values.remove(_parameters);
      }
      else if (new_subtype_TypeDecl_value != ((Boolean)_value.value)) {
        state.setChangeInCycle();
        _value.value = new_subtype_TypeDecl_value;
      }
      return new_subtype_TypeDecl_value;
    } else {
      return (Boolean) _value.value;
    }
  }
  /**
   * @attribute syn
   * @aspect GenericsSubtype
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:503
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="GenericsSubtype", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:503")
  @SideEffect.Pure(group="supertypeInterfaceDeclSubstituted_InterfaceDeclSubstituted") public boolean supertypeInterfaceDeclSubstituted(InterfaceDeclSubstituted type) {
    boolean supertypeInterfaceDeclSubstituted_InterfaceDeclSubstituted_value = original() == type.original() && type.enclosingType().subtype(enclosingType()) || super.supertypeInterfaceDeclSubstituted(type);
    return supertypeInterfaceDeclSubstituted_InterfaceDeclSubstituted_value;
  }
  /**
   * @attribute syn
   * @aspect GenericsSubtype
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:437
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="GenericsSubtype", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:437")
  @SideEffect.Pure(group="supertypeInterfaceDecl_InterfaceDecl") public boolean supertypeInterfaceDecl(InterfaceDecl type) {
    boolean supertypeInterfaceDecl_InterfaceDecl_value = super.supertypeInterfaceDecl(type) || original().supertypeInterfaceDecl(type);
    return supertypeInterfaceDecl_InterfaceDecl_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void typeDescriptor_reset() {
    typeDescriptor_computed = false;
    
    typeDescriptor_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="typeDescriptor") protected boolean typeDescriptor_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="typeDescriptor") protected String typeDescriptor_value;

  /**
   * @attribute syn
   * @aspect ConstantPoolNames
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\ConstantPoolNames.jrag:12
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantPoolNames", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\ConstantPoolNames.jrag:12")
  @SideEffect.Pure(group="typeDescriptor") public String typeDescriptor() {
    ASTState state = state();
    if (typeDescriptor_computed) {
      return typeDescriptor_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    typeDescriptor_value = original().typeDescriptor();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    typeDescriptor_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return typeDescriptor_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void constantPoolName_reset() {
    constantPoolName_computed = false;
    
    constantPoolName_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="constantPoolName") protected boolean constantPoolName_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="constantPoolName") protected String constantPoolName_value;

  /**
   * @attribute syn
   * @aspect ConstantPool
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\ConstantPool.jrag:16
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantPool", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\ConstantPool.jrag:16")
  @SideEffect.Pure(group="constantPoolName") public String constantPoolName() {
    ASTState state = state();
    if (constantPoolName_computed) {
      return constantPoolName_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    constantPoolName_value = original().constantPoolName();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    constantPoolName_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return constantPoolName_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void uniqueIndex_reset() {
    uniqueIndex_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="uniqueIndex") protected boolean uniqueIndex_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="uniqueIndex") protected int uniqueIndex_value;

  /**
   * @attribute syn
   * @aspect Java2Rewrites
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Java2Rewrites.jrag:12
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Java2Rewrites", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Java2Rewrites.jrag:12")
  @SideEffect.Pure(group="uniqueIndex") public int uniqueIndex() {
    ASTState state = state();
    if (uniqueIndex_computed) {
      return uniqueIndex_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    uniqueIndex_value = original().uniqueIndex();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    uniqueIndex_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return uniqueIndex_value;
  }
  /**
   * @attribute syn
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:818
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CreateBCode", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:818")
  @SideEffect.Pure(group="arrayTypeDescriptor") public String arrayTypeDescriptor() {
    String arrayTypeDescriptor_value = original().arrayTypeDescriptor();
    return arrayTypeDescriptor_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void localMethodsSignatureMap_reset() {
    localMethodsSignatureMap_computed = false;
    
    localMethodsSignatureMap_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="localMethodsSignatureMap") protected boolean localMethodsSignatureMap_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="localMethodsSignatureMap") protected HashMap localMethodsSignatureMap_value;

  /**
   * @attribute syn
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:928
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:928")
  @SideEffect.Pure(group="localMethodsSignatureMap") public HashMap localMethodsSignatureMap() {
    ASTState state = state();
    if (localMethodsSignatureMap_computed) {
      return localMethodsSignatureMap_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    localMethodsSignatureMap_value = localMethodsSignatureMap_compute();
    if (true) {
    localMethodsSignatureMap_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return localMethodsSignatureMap_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private HashMap localMethodsSignatureMap_compute() {
      HashMap map = new HashMap();
      for(Iterator iter = original().localMethodsIterator(); iter.hasNext(); ) {
        MethodDecl decl = (MethodDecl)iter.next();
        if(!decl.isStatic() && (decl.usesTypeVariable() || isRawType())) {
          BodyDecl b = decl.p(this);
          b.is$Final = true;
          addBodyDecl(b);
          decl = (MethodDecl)b;
        }
        map.put(decl.signature(), decl);
      }
      return map;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void localFields_String_reset() {
    localFields_String_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="localFields_String") protected java.util.Map localFields_String_values;

  /**
   * @attribute syn
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:943
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:943")
  @SideEffect.Pure(group="localFields_String") public SimpleSet localFields(String name) {
    Object _parameters = name;
    if (localFields_String_values == null) localFields_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (localFields_String_values.containsKey(_parameters)) {
      return (SimpleSet) localFields_String_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    SimpleSet localFields_String_value = localFields_compute(name);
    if (true) {
    localFields_String_values.put(_parameters, localFields_String_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return localFields_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private SimpleSet localFields_compute(String name) {
      SimpleSet set = SimpleSet.emptySet;
      for(Iterator iter = original().localFields(name).iterator(); iter.hasNext(); ) {
        FieldDeclaration f = (FieldDeclaration)iter.next();
        if(!f.isStatic() && (f.usesTypeVariable() || isRawType())) {
          BodyDecl b = f.p(this);
          b.is$Final = true;
          addBodyDecl(b);
          f = (FieldDeclaration)b;
        }
        set = set.add(f);
      }
      return set;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void localTypeDecls_String_reset() {
    localTypeDecls_String_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="localTypeDecls_String") protected java.util.Map localTypeDecls_String_values;

  /**
   * @attribute syn
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:958
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:958")
  @SideEffect.Pure(group="localTypeDecls_String") public SimpleSet localTypeDecls(String name) {
    Object _parameters = name;
    if (localTypeDecls_String_values == null) localTypeDecls_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (localTypeDecls_String_values.containsKey(_parameters)) {
      return (SimpleSet) localTypeDecls_String_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    SimpleSet localTypeDecls_String_value = localTypeDecls_compute(name);
    if (true) {
    localTypeDecls_String_values.put(_parameters, localTypeDecls_String_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return localTypeDecls_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private SimpleSet localTypeDecls_compute(String name) {
      SimpleSet set = SimpleSet.emptySet;
      for(Iterator iter = original().localTypeDecls(name).iterator(); iter.hasNext(); ) {
        TypeDecl t = (TypeDecl)iter.next();
        if(t.isStatic())
          set = set.add(t);
        else {
          BodyDecl b;
          TypeDecl typeDecl;
          if(t instanceof ClassDecl) {
            ClassDecl classDecl = (ClassDecl)t;
            typeDecl = classDecl.p(this);
            b = new MemberClassDecl((ClassDecl)typeDecl);
            b.is$Final = true;
            addBodyDecl(b);
            set = set.add(typeDecl);
          }
          else if(t instanceof InterfaceDecl) {
            InterfaceDecl interfaceDecl = (InterfaceDecl)t;
            typeDecl = interfaceDecl.p(this);
            b = new MemberInterfaceDecl((InterfaceDecl)typeDecl);
            b.is$Final = true;
            addBodyDecl(b);
            set = set.add(typeDecl);
          }
        }
      }
      return set;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void constructors_reset() {
    constructors_computed = false;
    
    constructors_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="constructors") protected boolean constructors_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="constructors") protected Collection constructors_value;

  /**
   * @attribute syn
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:988
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:988")
  @SideEffect.Pure(group="constructors") public Collection constructors() {
    ASTState state = state();
    if (constructors_computed) {
      return constructors_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    constructors_value = constructors_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    constructors_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return constructors_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Collection constructors_compute() {
      Collection set = new ArrayList();
      for(Iterator iter = original().constructors().iterator(); iter.hasNext(); ) {
        ConstructorDecl c = (ConstructorDecl)iter.next();
        BodyDecl b = c.p(this);
        b.is$Final = true;
        addBodyDecl(b);
        set.add(b);
      }
      return set;
    }
  /** @apilevel internal */
  @SideEffect.Pure public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}

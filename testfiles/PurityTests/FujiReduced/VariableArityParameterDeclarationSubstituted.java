/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package AST;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.ast:31
 * @production VariableArityParameterDeclarationSubstituted : {@link VariableArityParameterDeclaration} ::= <span class="component">&lt;Original:VariableArityParameterDeclaration&gt;</span>;

 */
public class VariableArityParameterDeclarationSubstituted extends VariableArityParameterDeclaration implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public VariableArityParameterDeclarationSubstituted() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
  }
  /**
   * @declaredat ASTNode:13
   */
  public VariableArityParameterDeclarationSubstituted(Modifiers p0, Access p1, String p2, VariableArityParameterDeclaration p3) {
    setChild(p0, 0);
    setChild(p1, 1);
    setID(p2);
    setOriginal(p3);
  }
  /**
   * @declaredat ASTNode:19
   */
  public VariableArityParameterDeclarationSubstituted(Modifiers p0, Access p1, beaver.Symbol p2, VariableArityParameterDeclaration p3) {
    setChild(p0, 0);
    setChild(p1, 1);
    setID(p2);
    setOriginal(p3);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:26
   */
  @SideEffect.Pure protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:32
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:36
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:44
   */
  @SideEffect.Fresh public VariableArityParameterDeclarationSubstituted clone() throws CloneNotSupportedException {
    VariableArityParameterDeclarationSubstituted node = (VariableArityParameterDeclarationSubstituted) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:49
   */
  @SideEffect.Fresh(group="_ASTNode") public VariableArityParameterDeclarationSubstituted copy() {
    try {
      VariableArityParameterDeclarationSubstituted node = (VariableArityParameterDeclarationSubstituted) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:68
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public VariableArityParameterDeclarationSubstituted fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:78
   */
  @SideEffect.Fresh(group="_ASTNode") public VariableArityParameterDeclarationSubstituted treeCopyNoTransform() {
    VariableArityParameterDeclarationSubstituted tree = (VariableArityParameterDeclarationSubstituted) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:98
   */
  @SideEffect.Fresh(group="_ASTNode") public VariableArityParameterDeclarationSubstituted treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:103
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_ID == ((VariableArityParameterDeclarationSubstituted) node).tokenString_ID) && (tokenVariableArityParameterDeclaration_Original == ((VariableArityParameterDeclarationSubstituted) node).tokenVariableArityParameterDeclaration_Original);    
  }
  /**
   * Replaces the Modifiers child.
   * @param node The new node to replace the Modifiers child.
   * @apilevel high-level
   */
  public void setModifiers(Modifiers node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Modifiers child.
   * @return The current node used as the Modifiers child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Modifiers")
  @SideEffect.Pure public Modifiers getModifiers() {
    return (Modifiers) getChild(0);
  }
  /**
   * Retrieves the Modifiers child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Modifiers child.
   * @apilevel low-level
   */
  public Modifiers getModifiersNoTransform() {
    return (Modifiers) getChildNoTransform(0);
  }
  /**
   * Replaces the TypeAccess child.
   * @param node The new node to replace the TypeAccess child.
   * @apilevel high-level
   */
  public void setTypeAccess(Access node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the TypeAccess child.
   * @return The current node used as the TypeAccess child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="TypeAccess")
  @SideEffect.Pure public Access getTypeAccess() {
    return (Access) getChild(1);
  }
  /**
   * Retrieves the TypeAccess child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the TypeAccess child.
   * @apilevel low-level
   */
  public Access getTypeAccessNoTransform() {
    return (Access) getChildNoTransform(1);
  }
  /**
   * Replaces the lexeme ID.
   * @param value The new value for the lexeme ID.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setID(String value) {
    tokenString_ID = value;
  }
  /**
   * JastAdd-internal setter for lexeme ID using the Beaver parser.
   * @param symbol Symbol containing the new value for the lexeme ID
   * @apilevel internal
   */
  public void setID(beaver.Symbol symbol) {
    if (symbol.value != null && !(symbol.value instanceof String))
    throw new UnsupportedOperationException("setID is only valid for String lexemes");
    tokenString_ID = (String)symbol.value;
    IDstart = symbol.getStart();
    IDend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme ID.
   * @return The value for the lexeme ID.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="ID")
  @SideEffect.Pure(group="_ASTNode") public String getID() {
    return tokenString_ID != null ? tokenString_ID : "";
  }
  /**
   * Replaces the lexeme Original.
   * @param value The new value for the lexeme Original.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setOriginal(VariableArityParameterDeclaration value) {
    tokenVariableArityParameterDeclaration_Original = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected VariableArityParameterDeclaration tokenVariableArityParameterDeclaration_Original;
  /**
   * Retrieves the value for the lexeme Original.
   * @return The value for the lexeme Original.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Original")
  @SideEffect.Pure(group="_ASTNode") public VariableArityParameterDeclaration getOriginal() {
    return tokenVariableArityParameterDeclaration_Original;
  }
  /** @apilevel internal */
  @SideEffect.Pure public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}

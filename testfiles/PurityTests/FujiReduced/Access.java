/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package AST;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\java.ast:11
 * @production Access : {@link Expr};

 */
public abstract class Access extends Expr implements Cloneable {
  /**
   * @aspect QualifiedNames
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\ResolveAmbiguousNames.jrag:148
   */
  public Access addArrayDims(List list) {
    Access a = this;
    for(int i = 0; i < list.getNumChildNoTransform(); i++) {
      Dims dims = (Dims)list.getChildNoTransform(i);
      Opt opt = dims.getExprOpt();
      if(opt.getNumChildNoTransform() == 1)
        a = new ArrayTypeWithSizeAccess(a, (Expr)opt.getChildNoTransform(0));
      else
        a = new ArrayTypeAccess(a);
      a.setStart(dims.start());
      a.setEnd(dims.end());
    }
    return a;
  }
  /**
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:477
   */
  public void emitLoadLocalInNestedClass(CodeGeneration gen, Variable v) {
    if(inExplicitConstructorInvocation() && enclosingBodyDecl() instanceof ConstructorDecl) {
      ConstructorDecl c = (ConstructorDecl)enclosingBodyDecl();
      v.type().emitLoadLocal(gen, c.localIndexOfEnclosingVariable(v));
    }
    else {
      String classname = hostType().constantPoolName();
      String      desc = v.type().typeDescriptor();
      String      name = "val$" + v.name();
      int index = gen.constantPool().addFieldref(classname, name, desc);
      gen.emit(Bytecode.ALOAD_0);
      gen.emit(Bytecode.GETFIELD, v.type().variableSize() - 1).add2(index);
    }
  }
  /**
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:577
   */
  public void emitThis(CodeGeneration gen, TypeDecl targetDecl) {
    if(targetDecl == hostType())
      gen.emit(Bytecode.ALOAD_0);
    else {
      TypeDecl enclosing = hostType();
      if(inExplicitConstructorInvocation()) {
        gen.emit(Bytecode.ALOAD_1);
        enclosing = enclosing.enclosing();
      }
      else {
        gen.emit(Bytecode.ALOAD_0);
      }
      while(enclosing != targetDecl) {
        String classname = enclosing.constantPoolName();
        enclosing = enclosing.enclosingType();
        String desc = enclosing.typeDescriptor();
        int index = gen.constantPool().addFieldref(classname, "this$0", desc);
        gen.emit(Bytecode.GETFIELD, 0).add2(index);
      }
    }
  }
  /**
   * @aspect InnerClasses
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\InnerClasses.jrag:103
   */
  protected TypeDecl superConstructorQualifier(TypeDecl targetEnclosingType) {
    TypeDecl enclosing = hostType();
    while(!enclosing.instanceOf(targetEnclosingType))
      enclosing = enclosing.enclosingType();
    return enclosing;
  }
  /**
   * @declaredat ASTNode:1
   */
  public Access() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:19
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:23
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    prevExpr_reset();
    hasPrevExpr_reset();
    type_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:30
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:34
   */
  @SideEffect.Fresh public Access clone() throws CloneNotSupportedException {
    Access node = (Access) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:45
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public abstract Access fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:53
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract Access treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:61
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract Access treeCopy();
  /**
   * @attribute syn
   * @aspect LookupMethod
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupMethod.jrag:17
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="LookupMethod", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupMethod.jrag:17")
  @SideEffect.Pure(group="unqualifiedScope") public Expr unqualifiedScope() {
    Expr unqualifiedScope_value = isQualified() ? nestedScope() : this;
    return unqualifiedScope_value;
  }
  /**
   * @attribute syn
   * @aspect QualifiedNames
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\ResolveAmbiguousNames.jrag:58
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="QualifiedNames", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\ResolveAmbiguousNames.jrag:58")
  @SideEffect.Pure(group="isQualified") public boolean isQualified() {
    boolean isQualified_value = hasPrevExpr();
    return isQualified_value;
  }
  /**
   * @attribute syn
   * @aspect QualifiedNames
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\ResolveAmbiguousNames.jrag:61
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="QualifiedNames", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\ResolveAmbiguousNames.jrag:61")
  @SideEffect.Pure(group="qualifier") public Expr qualifier() {
    Expr qualifier_value = prevExpr();
    return qualifier_value;
  }
  /**
   * @attribute syn
   * @aspect QualifiedNames
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\ResolveAmbiguousNames.jrag:66
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="QualifiedNames", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\ResolveAmbiguousNames.jrag:66")
  @SideEffect.Pure(group="lastAccess") public Access lastAccess() {
    Access lastAccess_value = this;
    return lastAccess_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void prevExpr_reset() {
    prevExpr_computed = false;
    
    prevExpr_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="prevExpr") protected boolean prevExpr_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="prevExpr") protected Expr prevExpr_value;

  /**
   * @attribute syn
   * @aspect QualifiedNames
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\ResolveAmbiguousNames.jrag:78
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="QualifiedNames", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\ResolveAmbiguousNames.jrag:78")
  @SideEffect.Pure(group="prevExpr") public Expr prevExpr() {
    ASTState state = state();
    if (prevExpr_computed) {
      return prevExpr_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    prevExpr_value = prevExpr_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    prevExpr_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return prevExpr_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Expr prevExpr_compute() {
      if(isLeftChildOfDot()) {
        if(parentDot().isRightChildOfDot())
          return parentDot().parentDot().leftSide();
      }
      else if(isRightChildOfDot())
        return parentDot().leftSide();
      throw new Error(this + " does not have a previous expression");
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void hasPrevExpr_reset() {
    hasPrevExpr_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="hasPrevExpr") protected boolean hasPrevExpr_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="hasPrevExpr") protected boolean hasPrevExpr_value;

  /**
   * @attribute syn
   * @aspect QualifiedNames
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\ResolveAmbiguousNames.jrag:89
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="QualifiedNames", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\ResolveAmbiguousNames.jrag:89")
  @SideEffect.Pure(group="hasPrevExpr") public boolean hasPrevExpr() {
    ASTState state = state();
    if (hasPrevExpr_computed) {
      return hasPrevExpr_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    hasPrevExpr_value = hasPrevExpr_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    hasPrevExpr_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return hasPrevExpr_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean hasPrevExpr_compute() {
      if(isLeftChildOfDot()) {
        if(parentDot().isRightChildOfDot())
          return true;
      }
      else if(isRightChildOfDot())
        return true;
      return false;
    }
  /**
   * @attribute syn
   * @aspect SyntacticClassification
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\SyntacticClassification.jrag:56
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SyntacticClassification", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\SyntacticClassification.jrag:56")
  @SideEffect.Pure(group="predNameType") public NameType predNameType() {
    NameType predNameType_value = NameType.NO_NAME;
    return predNameType_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void type_reset() {
    type_computed = false;
    
    type_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="type") protected boolean type_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="type") protected TypeDecl type_value;

  /**
   * @attribute syn
   * @aspect TypeAnalysis
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:276
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:276")
  @SideEffect.Pure(group="type") public TypeDecl type() {
    ASTState state = state();
    if (type_computed) {
      return type_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    type_value = unknownType();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    type_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return type_value;
  }
  /**
   * @attribute syn
   * @aspect CodeGeneration
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:15
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CodeGeneration", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:15")
  @SideEffect.Pure(group="sourceLineNumber") public int sourceLineNumber() {
    int sourceLineNumber_value = findFirstSourceLineNumber();
    return sourceLineNumber_value;
  }
  /**
   * @attribute syn
   * @aspect TypeInference
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\TypeInference.jrag:65
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeInference", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\TypeInference.jrag:65")
  @SideEffect.Pure(group="isDiamond") public boolean isDiamond() {
    boolean isDiamond_value = false;
    return isDiamond_value;
  }
  /**
   * Builds a copy of this Access node where all occurrences
   * of type variables in the original type parameter list have been replaced
   * by the substitution type parameters.
   * 
   * @return the substituted Access node
   * @attribute syn
   * @aspect TypeInference
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\TypeInference.jrag:406
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeInference", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\TypeInference.jrag:406")
  @SideEffect.Pure(group="substituted_Collection_TypeVariable__List_TypeVariable_") public Access substituted(Collection<TypeVariable> original, List<TypeVariable> substitution) {
    Access substituted_Collection_TypeVariable__List_TypeVariable__value = (Access) cloneSubtree();
    return substituted_Collection_TypeVariable__List_TypeVariable__value;
  }
  /**
   * @attribute inh
   * @aspect LookupMethod
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupMethod.jrag:18
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="LookupMethod", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupMethod.jrag:18")
  @SideEffect.Pure(group="nestedScope") public Expr nestedScope() {
    Expr nestedScope_value = getParent().Define_nestedScope(this, null);
    return nestedScope_value;
  }
  /**
   * @attribute inh
   * @aspect TypeScopePropagation
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupType.jrag:133
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="TypeScopePropagation", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupType.jrag:133")
  @SideEffect.Pure(group="unknownType") public TypeDecl unknownType() {
    TypeDecl unknownType_value = getParent().Define_unknownType(this, null);
    return unknownType_value;
  }
  /**
   * @attribute inh
   * @aspect VariableScopePropagation
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupVariable.jrag:230
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="VariableScopePropagation", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupVariable.jrag:230")
  @SideEffect.Pure(group="unknownField") public Variable unknownField() {
    Variable unknownField_value = getParent().Define_unknownField(this, null);
    return unknownField_value;
  }
  /**
   * @attribute inh
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:476
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="CreateBCode", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:476")
  @SideEffect.Pure(group="inExplicitConstructorInvocation") public boolean inExplicitConstructorInvocation() {
    boolean inExplicitConstructorInvocation_value = getParent().Define_inExplicitConstructorInvocation(this, null);
    return inExplicitConstructorInvocation_value;
  }
  /**
   * @attribute inh
   * @aspect Annotations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:268
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Annotations", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:268")
  @SideEffect.Pure(group="withinSuppressWarnings_String") public boolean withinSuppressWarnings(String s) {
    boolean withinSuppressWarnings_String_value = getParent().Define_withinSuppressWarnings(this, null, s);
    return withinSuppressWarnings_String_value;
  }
  /**
   * @attribute inh
   * @aspect Annotations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:372
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Annotations", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:372")
  @SideEffect.Pure(group="withinDeprecatedAnnotation") public boolean withinDeprecatedAnnotation() {
    boolean withinDeprecatedAnnotation_value = getParent().Define_withinDeprecatedAnnotation(this, null);
    return withinDeprecatedAnnotation_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}

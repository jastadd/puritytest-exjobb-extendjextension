/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package AST;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.ast:9
 * @production ParInterfaceDecl : {@link InterfaceDecl} ::= <span class="component">Argument:{@link Access}*</span> <span class="component">SuperInterfaceId:{@link Access}*</span> <span class="component">{@link BodyDecl}*</span>;

 */
public class ParInterfaceDecl extends InterfaceDecl implements Cloneable, ParTypeDecl, MemberSubstitutor {
  /**
   * @aspect GenericsNameBinding
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:527
   */
  public void collectErrors() {
    // Disable error check for ParInterfaceDecl which is an instanciated GenericInterfaceDecl
  }
  /**
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:696
   */
  public TypeDecl substitute(TypeVariable typeVariable) {
    for(int i = 0; i < numTypeParameter(); i++)
      if(typeParameter(i) == typeVariable)
        return getArgument(i).type();
    return super.substitute(typeVariable);
  }
  /**
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:709
   */
  public int numTypeParameter() {
    return ((GenericTypeDecl)original()).getNumTypeParameter(); 
  }
  /**
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:712
   */
  public TypeVariable typeParameter(int index) {
    return ((GenericTypeDecl)original()).getTypeParameter(index);
  }
  /**
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:744
   */
  public Access substitute(Parameterization parTypeDecl) {
    // TODO: include nesting as well....
    if(parTypeDecl.isRawType())
      return ((GenericTypeDecl)genericDecl()).rawType().createBoundAccess();
    if(!usesTypeVariable())
      return super.substitute(parTypeDecl);
    List list = new List();
    for(int i = 0; i < getNumArgument(); i++)
      list.add(getArgument(i).type().substitute(parTypeDecl));
    return new ParTypeAccess(genericDecl().createQualifiedAccess(), list);
  }
  /**
   * @aspect GenericsParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsParTypeDecl.jrag:73
   */
  public Access createQualifiedAccess() {
    List typeArgumentList = new List();
    for(int i = 0; i < getNumArgument(); i++) {
      Access a = (Access)getArgument(i);
      if(a instanceof TypeAccess)
        typeArgumentList.add(a.type().createQualifiedAccess());
      else
        typeArgumentList.add(a.fullCopy());
    }
    if(!isTopLevelType()) {
      if(isRawType())
        return enclosingType().createQualifiedAccess().qualifiesAccess(
          new TypeAccess("", getID())
        );
      else
        return enclosingType().createQualifiedAccess().qualifiesAccess(
          new ParTypeAccess(new TypeAccess("", getID()), typeArgumentList)
        );
    }
    else {
      if(isRawType())
        return new TypeAccess(packageName(), getID());
      else
        return new ParTypeAccess(new TypeAccess(packageName(), getID()), typeArgumentList);
    }
  }
  /**
   * @aspect GenericsCodegen
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Backend\\GenericsCodegen.jrag:300
   */
  public void transformation() {
  }
  /**
   * @declaredat ASTNode:1
   */
  public ParInterfaceDecl() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[4];
    setChild(new List(), 1);
    setChild(new List(), 2);
    setChild(new List(), 3);
  }
  /**
   * @declaredat ASTNode:16
   */
  public ParInterfaceDecl(Modifiers p0, String p1, List<Access> p2) {
    setChild(p0, 0);
    setID(p1);
    setChild(p2, 1);
  }
  /**
   * @declaredat ASTNode:21
   */
  public ParInterfaceDecl(Modifiers p0, beaver.Symbol p1, List<Access> p2) {
    setChild(p0, 0);
    setID(p1);
    setChild(p2, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:27
   */
  @SideEffect.Pure protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:33
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    involvesTypeParameters_reset();
    erasure_reset();
    getSuperInterfaceIdList_reset();
    getBodyDeclList_reset();
    subtype_TypeDecl_reset();
    sameStructure_TypeDecl_reset();
    instanceOf_TypeDecl_reset();
    typeDescriptor_reset();
    constantPoolName_reset();
    needsSignatureAttribute_reset();
    sameSignature_ArrayList_reset();
    usesTypeVariable_reset();
    sourceTypeDecl_reset();
    fullName_reset();
    typeName_reset();
    unimplementedMethods_reset();
    uniqueIndex_reset();
    localMethodsSignatureMap_reset();
    localFields_String_reset();
    localTypeDecls_String_reset();
    constructors_reset();
    genericDecl_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:63
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:67
   */
  @SideEffect.Fresh public ParInterfaceDecl clone() throws CloneNotSupportedException {
    ParInterfaceDecl node = (ParInterfaceDecl) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:72
   */
  @SideEffect.Fresh(group="_ASTNode") public ParInterfaceDecl copy() {
    try {
      ParInterfaceDecl node = (ParInterfaceDecl) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:91
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public ParInterfaceDecl fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:101
   */
  @SideEffect.Fresh(group="_ASTNode") public ParInterfaceDecl treeCopyNoTransform() {
    ParInterfaceDecl tree = (ParInterfaceDecl) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 2:
        case 3:
          tree.children[i] = new List();
          continue;
        }
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:127
   */
  @SideEffect.Fresh(group="_ASTNode") public ParInterfaceDecl treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:132
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_ID == ((ParInterfaceDecl) node).tokenString_ID);    
  }
  /**
   * Replaces the Modifiers child.
   * @param node The new node to replace the Modifiers child.
   * @apilevel high-level
   */
  public void setModifiers(Modifiers node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Modifiers child.
   * @return The current node used as the Modifiers child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Modifiers")
  @SideEffect.Pure public Modifiers getModifiers() {
    return (Modifiers) getChild(0);
  }
  /**
   * Retrieves the Modifiers child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Modifiers child.
   * @apilevel low-level
   */
  public Modifiers getModifiersNoTransform() {
    return (Modifiers) getChildNoTransform(0);
  }
  /**
   * Replaces the lexeme ID.
   * @param value The new value for the lexeme ID.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setID(String value) {
    tokenString_ID = value;
  }
  /**
   * JastAdd-internal setter for lexeme ID using the Beaver parser.
   * @param symbol Symbol containing the new value for the lexeme ID
   * @apilevel internal
   */
  public void setID(beaver.Symbol symbol) {
    if (symbol.value != null && !(symbol.value instanceof String))
    throw new UnsupportedOperationException("setID is only valid for String lexemes");
    tokenString_ID = (String)symbol.value;
    IDstart = symbol.getStart();
    IDend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme ID.
   * @return The value for the lexeme ID.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="ID")
  @SideEffect.Pure(group="_ASTNode") public String getID() {
    return tokenString_ID != null ? tokenString_ID : "";
  }
  /**
   * Replaces the Argument list.
   * @param list The new list node to be used as the Argument list.
   * @apilevel high-level
   */
  public void setArgumentList(List<Access> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the Argument list.
   * @return Number of children in the Argument list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumArgument() {
    return getArgumentList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Argument list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Argument list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumArgumentNoTransform() {
    return getArgumentListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Argument list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Argument list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Access getArgument(int i) {
    return (Access) getArgumentList().getChild(i);
  }
  /**
   * Check whether the Argument list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasArgument() {
    return getArgumentList().getNumChild() != 0;
  }
  /**
   * Append an element to the Argument list.
   * @param node The element to append to the Argument list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addArgument(Access node) {
    List<Access> list = (parent == null) ? getArgumentListNoTransform() : getArgumentList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addArgumentNoTransform(Access node) {
    List<Access> list = getArgumentListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Argument list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setArgument(Access node, int i) {
    List<Access> list = getArgumentList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Argument list.
   * @return The node representing the Argument list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Argument")
  @SideEffect.Pure(group="_ASTNode") public List<Access> getArgumentList() {
    List<Access> list = (List<Access>) getChild(1);
    return list;
  }
  /**
   * Retrieves the Argument list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Argument list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Access> getArgumentListNoTransform() {
    return (List<Access>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the Argument list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Access getArgumentNoTransform(int i) {
    return (Access) getArgumentListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Argument list.
   * @return The node representing the Argument list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Access> getArguments() {
    return getArgumentList();
  }
  /**
   * Retrieves the Argument list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Argument list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Access> getArgumentsNoTransform() {
    return getArgumentListNoTransform();
  }
  /**
   * This method should not be called. This method throws an exception due to
   * the corresponding child being an NTA shadowing a non-NTA child.
   * @param node
   * @apilevel internal
   */
  public void setSuperInterfaceIdList(List<Access> node) {
    throw new Error("Can not replace NTA child SuperInterfaceIdList in ParInterfaceDecl!");
  }
  /**
   * Retrieves the number of children in the SuperInterfaceId list.
   * @return Number of children in the SuperInterfaceId list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumSuperInterfaceId() {
    return getSuperInterfaceIdList().getNumChild();
  }
  /**
   * Retrieves the number of children in the SuperInterfaceId list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the SuperInterfaceId list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumSuperInterfaceIdNoTransform() {
    return getSuperInterfaceIdListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the SuperInterfaceId list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the SuperInterfaceId list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Access getSuperInterfaceId(int i) {
    return (Access) getSuperInterfaceIdList().getChild(i);
  }
  /**
   * Check whether the SuperInterfaceId list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasSuperInterfaceId() {
    return getSuperInterfaceIdList().getNumChild() != 0;
  }
  /**
   * Append an element to the SuperInterfaceId list.
   * @param node The element to append to the SuperInterfaceId list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addSuperInterfaceId(Access node) {
    List<Access> list = (parent == null) ? getSuperInterfaceIdListNoTransform() : getSuperInterfaceIdList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addSuperInterfaceIdNoTransform(Access node) {
    List<Access> list = getSuperInterfaceIdListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the SuperInterfaceId list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setSuperInterfaceId(Access node, int i) {
    List<Access> list = getSuperInterfaceIdList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the child position of the SuperInterfaceId list.
   * @return The the child position of the SuperInterfaceId list.
   * @apilevel low-level
   */
  @SideEffect.Pure protected int getSuperInterfaceIdListChildPosition() {
    return 2;
  }
  /**
   * Retrieves the SuperInterfaceId list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SuperInterfaceId list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Access> getSuperInterfaceIdListNoTransform() {
    return (List<Access>) getChildNoTransform(2);
  }
  /**
   * @return the element at index {@code i} in the SuperInterfaceId list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Access getSuperInterfaceIdNoTransform(int i) {
    return (Access) getSuperInterfaceIdListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the SuperInterfaceId list.
   * @return The node representing the SuperInterfaceId list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Access> getSuperInterfaceIds() {
    return getSuperInterfaceIdList();
  }
  /**
   * Retrieves the SuperInterfaceId list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SuperInterfaceId list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Access> getSuperInterfaceIdsNoTransform() {
    return getSuperInterfaceIdListNoTransform();
  }
  /**
   * This method should not be called. This method throws an exception due to
   * the corresponding child being an NTA shadowing a non-NTA child.
   * @param node
   * @apilevel internal
   */
  public void setBodyDeclList(List<BodyDecl> node) {
    throw new Error("Can not replace NTA child BodyDeclList in ParInterfaceDecl!");
  }
  /**
   * Retrieves the number of children in the BodyDecl list.
   * @return Number of children in the BodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumBodyDecl() {
    return getBodyDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the BodyDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the BodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumBodyDeclNoTransform() {
    return getBodyDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the BodyDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the BodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public BodyDecl getBodyDecl(int i) {
    return (BodyDecl) getBodyDeclList().getChild(i);
  }
  /**
   * Check whether the BodyDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasBodyDecl() {
    return getBodyDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the BodyDecl list.
   * @param node The element to append to the BodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addBodyDecl(BodyDecl node) {
    List<BodyDecl> list = (parent == null) ? getBodyDeclListNoTransform() : getBodyDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addBodyDeclNoTransform(BodyDecl node) {
    List<BodyDecl> list = getBodyDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the BodyDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setBodyDecl(BodyDecl node, int i) {
    List<BodyDecl> list = getBodyDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the child position of the BodyDecl list.
   * @return The the child position of the BodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure protected int getBodyDeclListChildPosition() {
    return 3;
  }
  /**
   * Retrieves the BodyDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the BodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<BodyDecl> getBodyDeclListNoTransform() {
    return (List<BodyDecl>) getChildNoTransform(3);
  }
  /**
   * @return the element at index {@code i} in the BodyDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public BodyDecl getBodyDeclNoTransform(int i) {
    return (BodyDecl) getBodyDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the BodyDecl list.
   * @return The node representing the BodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<BodyDecl> getBodyDecls() {
    return getBodyDeclList();
  }
  /**
   * Retrieves the BodyDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the BodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<BodyDecl> getBodyDeclsNoTransform() {
    return getBodyDeclListNoTransform();
  }
/** @apilevel internal */
protected ASTState.Cycle involvesTypeParameters_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void involvesTypeParameters_reset() {
    involvesTypeParameters_computed = false;
    involvesTypeParameters_initialized = false;
    involvesTypeParameters_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="involvesTypeParameters") protected boolean involvesTypeParameters_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="involvesTypeParameters") protected boolean involvesTypeParameters_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="involvesTypeParameters") protected boolean involvesTypeParameters_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="GenericMethodsInference", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericMethodsInference.jrag:15")
  @SideEffect.Pure(group="involvesTypeParameters") public boolean involvesTypeParameters() {
    if (involvesTypeParameters_computed) {
      return involvesTypeParameters_value;
    }
    ASTState state = state();
    if (!involvesTypeParameters_initialized) {
      involvesTypeParameters_initialized = true;
      involvesTypeParameters_value = false;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      int _boundaries = state.boundariesCrossed;
      boolean isFinal = this.is$Final();
      do {
        involvesTypeParameters_cycle = state.nextCycle();
        boolean new_involvesTypeParameters_value = involvesTypeParameters_compute();
        if (new_involvesTypeParameters_value != involvesTypeParameters_value) {
          state.setChangeInCycle();
        }
        involvesTypeParameters_value = new_involvesTypeParameters_value;
      } while (state.testAndClearChangeInCycle());
      if (isFinal && _boundaries == state().boundariesCrossed) {
        involvesTypeParameters_computed = true;
      } else {
        state.startResetCycle();
        boolean $tmp = involvesTypeParameters_compute();
        involvesTypeParameters_computed = false;
        involvesTypeParameters_initialized = false;
      }
      state.leaveCircle();
    } else if (involvesTypeParameters_cycle != state.cycle()) {
      involvesTypeParameters_cycle = state.cycle();
      if (state.resetCycle()) {
        involvesTypeParameters_computed = false;
        involvesTypeParameters_initialized = false;
        involvesTypeParameters_cycle = null;
        return involvesTypeParameters_value;
      }
      boolean new_involvesTypeParameters_value = involvesTypeParameters_compute();
      if (new_involvesTypeParameters_value != involvesTypeParameters_value) {
        state.setChangeInCycle();
      }
      involvesTypeParameters_value = new_involvesTypeParameters_value;
    } else {
    }
    return involvesTypeParameters_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean involvesTypeParameters_compute() {
      for(int i = 0; i < getNumArgument(); i++)
        if(getArgument(i).type().involvesTypeParameters())
          return true;
      return false;
    }
  /**
   * @attribute syn
   * @aspect Generics
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:222
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Generics", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:222")
  @SideEffect.Pure(group="isRawType") public boolean isRawType() {
    boolean isRawType_value = isNestedType() && enclosingType().isRawType();
    return isRawType_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void erasure_reset() {
    erasure_computed = false;
    
    erasure_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="erasure") protected boolean erasure_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="erasure") protected TypeDecl erasure_value;

  /**
   * @attribute syn
   * @aspect GenericsErasure
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:313
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="GenericsErasure", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:313")
  @SideEffect.Pure(group="erasure") public TypeDecl erasure() {
    ASTState state = state();
    if (erasure_computed) {
      return erasure_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    erasure_value = genericDecl();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    erasure_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return erasure_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void getSuperInterfaceIdList_reset() {
    getSuperInterfaceIdList_computed = false;
    
    getSuperInterfaceIdList_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="getSuperInterfaceIdList") protected boolean getSuperInterfaceIdList_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="getSuperInterfaceIdList") protected List getSuperInterfaceIdList_value;

  /**
   * @attribute syn nta
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:891
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:891")
  @SideEffect.Pure(group="getSuperInterfaceIdList") public List getSuperInterfaceIdList() {
    ASTState state = state();
    if (getSuperInterfaceIdList_computed) {
      return (List) getChild(getSuperInterfaceIdListChildPosition());
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    getSuperInterfaceIdList_value = getSuperInterfaceIdList_compute();
    setChild(getSuperInterfaceIdList_value, getSuperInterfaceIdListChildPosition());
    if (isFinal && _boundaries == state().boundariesCrossed) {
    getSuperInterfaceIdList_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    List node = (List) this.getChild(getSuperInterfaceIdListChildPosition());
    return node;
  }
  /** @apilevel internal */
  @SideEffect.Pure private List getSuperInterfaceIdList_compute() {
      GenericInterfaceDecl decl = (GenericInterfaceDecl)genericDecl();
      //System.err.println("Begin substituting implements list");
      List list = decl.getSuperInterfaceIdList().substitute(this);
      //System.err.println("End substituting implements list");
      return list;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void getBodyDeclList_reset() {
    getBodyDeclList_computed = false;
    
    getBodyDeclList_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="getBodyDeclList") protected boolean getBodyDeclList_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="getBodyDeclList") protected List getBodyDeclList_value;

  /**
   * @attribute syn nta
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:898
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:898")
  @SideEffect.Pure(group="getBodyDeclList") public List getBodyDeclList() {
    ASTState state = state();
    if (getBodyDeclList_computed) {
      return (List) getChild(getBodyDeclListChildPosition());
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    getBodyDeclList_value = new List();
    setChild(getBodyDeclList_value, getBodyDeclListChildPosition());
    if (isFinal && _boundaries == state().boundariesCrossed) {
    getBodyDeclList_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    List node = (List) this.getChild(getBodyDeclListChildPosition());
    return node;
  }
  /**
   * @attribute syn
   * @aspect GenericsSubtype
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:14
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="GenericsSubtype", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:14")
  @SideEffect.Pure(group="supertypeGenericClassDecl_GenericClassDecl") public boolean supertypeGenericClassDecl(GenericClassDecl type) {
    boolean supertypeGenericClassDecl_GenericClassDecl_value = type.subtype(genericDecl().original());
    return supertypeGenericClassDecl_GenericClassDecl_value;
  }
  /**
   * @attribute syn
   * @aspect GenericsSubtype
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:20
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="GenericsSubtype", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:20")
  @SideEffect.Pure(group="supertypeGenericInterfaceDecl_GenericInterfaceDecl") public boolean supertypeGenericInterfaceDecl(GenericInterfaceDecl type) {
    boolean supertypeGenericInterfaceDecl_GenericInterfaceDecl_value = type.subtype(genericDecl().original());
    return supertypeGenericInterfaceDecl_GenericInterfaceDecl_value;
  }
  /**
   * @attribute syn
   * @aspect GenericsSubtype
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:421
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="GenericsSubtype", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:421")
  @SideEffect.Pure(group="supertypeClassDecl_ClassDecl") public boolean supertypeClassDecl(ClassDecl type) {
    boolean supertypeClassDecl_ClassDecl_value = super.supertypeClassDecl(type);
    return supertypeClassDecl_ClassDecl_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void subtype_TypeDecl_reset() {
    subtype_TypeDecl_values = null;
  }
  protected java.util.Map subtype_TypeDecl_values;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="GenericsSubtype", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:405")
  @SideEffect.Pure(group="subtype_TypeDecl") public boolean subtype(TypeDecl type) {
    Object _parameters = type;
    if (subtype_TypeDecl_values == null) subtype_TypeDecl_values = new java.util.HashMap(4);
    ASTState.CircularValue _value;
    if (subtype_TypeDecl_values.containsKey(_parameters)) {
      Object _cache = subtype_TypeDecl_values.get(_parameters);
      if (!(_cache instanceof ASTState.CircularValue)) {
        return (Boolean) _cache;
      } else {
        _value = (ASTState.CircularValue) _cache;
      }
    } else {
      _value = new ASTState.CircularValue();
      subtype_TypeDecl_values.put(_parameters, _value);
      _value.value = true;
    }
    ASTState state = state();
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      int _boundaries = state.boundariesCrossed;
      boolean isFinal = this.is$Final();
      boolean new_subtype_TypeDecl_value;
      do {
        _value.cycle = state.nextCycle();
        new_subtype_TypeDecl_value = type.supertypeParInterfaceDecl(this);
        if (new_subtype_TypeDecl_value != ((Boolean)_value.value)) {
          state.setChangeInCycle();
          _value.value = new_subtype_TypeDecl_value;
        }
      } while (state.testAndClearChangeInCycle());
      if (isFinal && _boundaries == state().boundariesCrossed) {
        subtype_TypeDecl_values.put(_parameters, new_subtype_TypeDecl_value);
      } else {
        subtype_TypeDecl_values.remove(_parameters);
        state.startResetCycle();
        boolean $tmp = type.supertypeParInterfaceDecl(this);
      }
      state.leaveCircle();
      return new_subtype_TypeDecl_value;
    } else if (_value.cycle != state.cycle()) {
      _value.cycle = state.cycle();
      boolean new_subtype_TypeDecl_value = type.supertypeParInterfaceDecl(this);
      if (state.resetCycle()) {
        subtype_TypeDecl_values.remove(_parameters);
      }
      else if (new_subtype_TypeDecl_value != ((Boolean)_value.value)) {
        state.setChangeInCycle();
        _value.value = new_subtype_TypeDecl_value;
      }
      return new_subtype_TypeDecl_value;
    } else {
      return (Boolean) _value.value;
    }
  }
  /**
   * @attribute syn
   * @aspect GenericsSubtype
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:26
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="GenericsSubtype", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:26")
  @SideEffect.Pure(group="supertypeRawClassDecl_RawClassDecl") public boolean supertypeRawClassDecl(RawClassDecl type) {
    boolean supertypeRawClassDecl_RawClassDecl_value = type.genericDecl().original().subtype(genericDecl().original());
    return supertypeRawClassDecl_RawClassDecl_value;
  }
  /**
   * @attribute syn
   * @aspect GenericsSubtype
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:30
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="GenericsSubtype", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:30")
  @SideEffect.Pure(group="supertypeRawInterfaceDecl_RawInterfaceDecl") public boolean supertypeRawInterfaceDecl(RawInterfaceDecl type) {
    boolean supertypeRawInterfaceDecl_RawInterfaceDecl_value = type.genericDecl().original().subtype(genericDecl().original());
    return supertypeRawInterfaceDecl_RawInterfaceDecl_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void sameStructure_TypeDecl_reset() {
    sameStructure_TypeDecl_values = null;
  }
  protected java.util.Map sameStructure_TypeDecl_values;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="GenericsSubtype", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:178")
  @SideEffect.Pure(group="sameStructure_TypeDecl") public boolean sameStructure(TypeDecl t) {
    Object _parameters = t;
    if (sameStructure_TypeDecl_values == null) sameStructure_TypeDecl_values = new java.util.HashMap(4);
    ASTState.CircularValue _value;
    if (sameStructure_TypeDecl_values.containsKey(_parameters)) {
      Object _cache = sameStructure_TypeDecl_values.get(_parameters);
      if (!(_cache instanceof ASTState.CircularValue)) {
        return (Boolean) _cache;
      } else {
        _value = (ASTState.CircularValue) _cache;
      }
    } else {
      _value = new ASTState.CircularValue();
      sameStructure_TypeDecl_values.put(_parameters, _value);
      _value.value = true;
    }
    ASTState state = state();
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      int _boundaries = state.boundariesCrossed;
      boolean isFinal = this.is$Final();
      boolean new_sameStructure_TypeDecl_value;
      do {
        _value.cycle = state.nextCycle();
        new_sameStructure_TypeDecl_value = sameStructure_compute(t);
        if (new_sameStructure_TypeDecl_value != ((Boolean)_value.value)) {
          state.setChangeInCycle();
          _value.value = new_sameStructure_TypeDecl_value;
        }
      } while (state.testAndClearChangeInCycle());
      if (isFinal && _boundaries == state().boundariesCrossed) {
        sameStructure_TypeDecl_values.put(_parameters, new_sameStructure_TypeDecl_value);
      } else {
        sameStructure_TypeDecl_values.remove(_parameters);
        state.startResetCycle();
        boolean $tmp = sameStructure_compute(t);
      }
      state.leaveCircle();
      return new_sameStructure_TypeDecl_value;
    } else if (_value.cycle != state.cycle()) {
      _value.cycle = state.cycle();
      boolean new_sameStructure_TypeDecl_value = sameStructure_compute(t);
      if (state.resetCycle()) {
        sameStructure_TypeDecl_values.remove(_parameters);
      }
      else if (new_sameStructure_TypeDecl_value != ((Boolean)_value.value)) {
        state.setChangeInCycle();
        _value.value = new_sameStructure_TypeDecl_value;
      }
      return new_sameStructure_TypeDecl_value;
    } else {
      return (Boolean) _value.value;
    }
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean sameStructure_compute(TypeDecl t) {
      if(!(t instanceof ParInterfaceDecl))
        return false;
      ParInterfaceDecl type = (ParInterfaceDecl)t;
      if(type.genericDecl().original() == genericDecl().original() &&
         type.getNumArgument() == getNumArgument()) {
        for(int i = 0; i < getNumArgument(); i++)
          if(!type.getArgument(i).type().sameStructure(getArgument(i).type()))
            return false;
        if(isNestedType() && type.isNestedType())
          return type.enclosingType().sameStructure(enclosingType());
        return true;
      }
      return false;
    }
  /**
   * @attribute syn
   * @aspect GenericsSubtype
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:125
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="GenericsSubtype", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:125")
  @SideEffect.Pure(group="supertypeParClassDecl_ParClassDecl") public boolean supertypeParClassDecl(ParClassDecl type) {
    {
        if(type.genericDecl().original() == genericDecl().original() &&
           type.getNumArgument() == getNumArgument()) {
          for(int i = 0; i < getNumArgument(); i++)
            if(!type.getArgument(i).type().containedIn(getArgument(i).type()))
              return false;
          if(isNestedType() && type.isNestedType())
            return type.enclosingType().subtype(enclosingType());
          return true;
        }
        return supertypeClassDecl(type);
      }
  }
  /**
   * @attribute syn
   * @aspect GenericsSubtype
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:129
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="GenericsSubtype", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:129")
  @SideEffect.Pure(group="supertypeParInterfaceDecl_ParInterfaceDecl") public boolean supertypeParInterfaceDecl(ParInterfaceDecl type) {
    {
        if(type.genericDecl().original() == genericDecl().original() &&
           type.getNumArgument() == getNumArgument()) {
          for(int i = 0; i < getNumArgument(); i++)
            if(!type.getArgument(i).type().containedIn(getArgument(i).type()))
              return false;
          if(isNestedType() && type.isNestedType())
            return type.enclosingType().subtype(enclosingType());
          return true;
        }
        return supertypeInterfaceDecl(type);
      }
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void instanceOf_TypeDecl_reset() {
    instanceOf_TypeDecl_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="instanceOf_TypeDecl") protected java.util.Map instanceOf_TypeDecl_values;

  /**
   * @attribute syn
   * @aspect TypeWideningAndIdentity
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:407
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeWideningAndIdentity", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:407")
  @SideEffect.Pure(group="instanceOf_TypeDecl") public boolean instanceOf(TypeDecl type) {
    Object _parameters = type;
    if (instanceOf_TypeDecl_values == null) instanceOf_TypeDecl_values = new java.util.HashMap(4);
    ASTState state = state();
    if (instanceOf_TypeDecl_values.containsKey(_parameters)) {
      return (Boolean) instanceOf_TypeDecl_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boolean instanceOf_TypeDecl_value = subtype(type);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    instanceOf_TypeDecl_values.put(_parameters, instanceOf_TypeDecl_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return instanceOf_TypeDecl_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void typeDescriptor_reset() {
    typeDescriptor_computed = false;
    
    typeDescriptor_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="typeDescriptor") protected boolean typeDescriptor_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="typeDescriptor") protected String typeDescriptor_value;

  /**
   * @attribute syn
   * @aspect ConstantPoolNames
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\ConstantPoolNames.jrag:12
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantPoolNames", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\ConstantPoolNames.jrag:12")
  @SideEffect.Pure(group="typeDescriptor") public String typeDescriptor() {
    ASTState state = state();
    if (typeDescriptor_computed) {
      return typeDescriptor_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    typeDescriptor_value = erasure().typeDescriptor();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    typeDescriptor_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return typeDescriptor_value;
  }
  /**
   * @attribute syn
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:818
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CreateBCode", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:818")
  @SideEffect.Pure(group="arrayTypeDescriptor") public String arrayTypeDescriptor() {
    String arrayTypeDescriptor_value = erasure().arrayTypeDescriptor();
    return arrayTypeDescriptor_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void constantPoolName_reset() {
    constantPoolName_computed = false;
    
    constantPoolName_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="constantPoolName") protected boolean constantPoolName_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="constantPoolName") protected String constantPoolName_value;

  /**
   * @attribute syn
   * @aspect ConstantPool
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\ConstantPool.jrag:16
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantPool", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\ConstantPool.jrag:16")
  @SideEffect.Pure(group="constantPoolName") public String constantPoolName() {
    ASTState state = state();
    if (constantPoolName_computed) {
      return constantPoolName_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    constantPoolName_value = genericDecl().constantPoolName();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    constantPoolName_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return constantPoolName_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void needsSignatureAttribute_reset() {
    needsSignatureAttribute_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="needsSignatureAttribute") protected boolean needsSignatureAttribute_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="needsSignatureAttribute") protected boolean needsSignatureAttribute_value;

  /**
   * @attribute syn
   * @aspect GenericsCodegen
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Backend\\GenericsCodegen.jrag:339
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="GenericsCodegen", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Backend\\GenericsCodegen.jrag:339")
  @SideEffect.Pure(group="needsSignatureAttribute") public boolean needsSignatureAttribute() {
    ASTState state = state();
    if (needsSignatureAttribute_computed) {
      return needsSignatureAttribute_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    needsSignatureAttribute_value = true;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    needsSignatureAttribute_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return needsSignatureAttribute_value;
  }
  /**
   * A type is reifiable if it either refers to a non-parameterized type,
   * is a raw type, is a parameterized type with only unbound wildcard
   * parameters or is an array type with a reifiable type parameter.
   * 
   * @see "JLSv3 &sect;4.7"
   * @attribute syn
   * @aspect SafeVarargs
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\SafeVarargs.jrag:106
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SafeVarargs", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\SafeVarargs.jrag:106")
  @SideEffect.Pure(group="isReifiable") public boolean isReifiable() {
    {
    		if (isRawType())
    			return true;
    		for (int i = 0; i < getNumArgument(); ++i) {
    			if (!getArgument(i).type().isWildcard())
    				return false;
    		}
    		return true;
    	}
  }
  /**
   * @attribute syn
   * @aspect Generics
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:221
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Generics", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:221")
  @SideEffect.Pure(group="isParameterizedType") public boolean isParameterizedType() {
    boolean isParameterizedType_value = true;
    return isParameterizedType_value;
  }
  /**
   * @attribute syn
   * @aspect GenericsTypeCheck
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:350
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="GenericsTypeCheck", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:350")
  @SideEffect.Pure(group="sameArgument_ParTypeDecl") public boolean sameArgument(ParTypeDecl decl) {
    {
        if(this == decl) return true;
        if(genericDecl() != decl.genericDecl())
          return false;
        for(int i = 0; i < getNumArgument(); i++) {
          TypeDecl t1 = getArgument(i).type();
          TypeDecl t2 = decl.getArgument(i).type();
          if(t1 instanceof ParTypeDecl && t2 instanceof ParTypeDecl) {
            if(!((ParTypeDecl)t1).sameArgument((ParTypeDecl)t2))
              return false;
          }
          else {
            if(t1 != t2)
              return false;
          }
        }
        return true;
      }
  }
  /**
   * @attribute syn
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:547
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:547")
  @SideEffect.Pure(group="sameSignature_Access") public boolean sameSignature(Access a) {
    {
        if(a instanceof ParTypeAccess) {
          ParTypeAccess ta = (ParTypeAccess)a;
          if(genericDecl() != ta.genericDecl())
            return false;
          if(getNumArgument() != ta.getNumTypeArgument())
            return false;
          for(int i = 0; i < getNumArgument(); i++)
            if(!getArgument(i).type().sameSignature(ta.getTypeArgument(i)))
              return false;
          return true;
        }
        else if(a instanceof TypeAccess && ((TypeAccess)a).isRaw())
          return false;
        return super.sameSignature(a);
      }
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void sameSignature_ArrayList_reset() {
    sameSignature_ArrayList_values = null;
  }
  protected java.util.Map sameSignature_ArrayList_values;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:582")
  @SideEffect.Pure(group="sameSignature_ArrayList") public boolean sameSignature(ArrayList list) {
    Object _parameters = list;
    if (sameSignature_ArrayList_values == null) sameSignature_ArrayList_values = new java.util.HashMap(4);
    ASTState.CircularValue _value;
    if (sameSignature_ArrayList_values.containsKey(_parameters)) {
      Object _cache = sameSignature_ArrayList_values.get(_parameters);
      if (!(_cache instanceof ASTState.CircularValue)) {
        return (Boolean) _cache;
      } else {
        _value = (ASTState.CircularValue) _cache;
      }
    } else {
      _value = new ASTState.CircularValue();
      sameSignature_ArrayList_values.put(_parameters, _value);
      _value.value = true;
    }
    ASTState state = state();
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      int _boundaries = state.boundariesCrossed;
      boolean isFinal = this.is$Final();
      boolean new_sameSignature_ArrayList_value;
      do {
        _value.cycle = state.nextCycle();
        new_sameSignature_ArrayList_value = sameSignature_compute(list);
        if (new_sameSignature_ArrayList_value != ((Boolean)_value.value)) {
          state.setChangeInCycle();
          _value.value = new_sameSignature_ArrayList_value;
        }
      } while (state.testAndClearChangeInCycle());
      if (isFinal && _boundaries == state().boundariesCrossed) {
        sameSignature_ArrayList_values.put(_parameters, new_sameSignature_ArrayList_value);
      } else {
        sameSignature_ArrayList_values.remove(_parameters);
        state.startResetCycle();
        boolean $tmp = sameSignature_compute(list);
      }
      state.leaveCircle();
      return new_sameSignature_ArrayList_value;
    } else if (_value.cycle != state.cycle()) {
      _value.cycle = state.cycle();
      boolean new_sameSignature_ArrayList_value = sameSignature_compute(list);
      if (state.resetCycle()) {
        sameSignature_ArrayList_values.remove(_parameters);
      }
      else if (new_sameSignature_ArrayList_value != ((Boolean)_value.value)) {
        state.setChangeInCycle();
        _value.value = new_sameSignature_ArrayList_value;
      }
      return new_sameSignature_ArrayList_value;
    } else {
      return (Boolean) _value.value;
    }
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean sameSignature_compute(ArrayList list) {
      if(getNumArgument() != list.size())
        return false;
      for(int i = 0; i < list.size(); i++)
        if(getArgument(i).type() != list.get(i))
          return false;
      return true;
    }
/** @apilevel internal */
protected ASTState.Cycle usesTypeVariable_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void usesTypeVariable_reset() {
    usesTypeVariable_computed = false;
    usesTypeVariable_initialized = false;
    usesTypeVariable_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="usesTypeVariable") protected boolean usesTypeVariable_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="usesTypeVariable") protected boolean usesTypeVariable_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="usesTypeVariable") protected boolean usesTypeVariable_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:912")
  @SideEffect.Pure(group="usesTypeVariable") public boolean usesTypeVariable() {
    if (usesTypeVariable_computed) {
      return usesTypeVariable_value;
    }
    ASTState state = state();
    if (!usesTypeVariable_initialized) {
      usesTypeVariable_initialized = true;
      usesTypeVariable_value = false;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      int _boundaries = state.boundariesCrossed;
      boolean isFinal = this.is$Final();
      do {
        usesTypeVariable_cycle = state.nextCycle();
        boolean new_usesTypeVariable_value = usesTypeVariable_compute();
        if (new_usesTypeVariable_value != usesTypeVariable_value) {
          state.setChangeInCycle();
        }
        usesTypeVariable_value = new_usesTypeVariable_value;
      } while (state.testAndClearChangeInCycle());
      if (isFinal && _boundaries == state().boundariesCrossed) {
        usesTypeVariable_computed = true;
      } else {
        state.startResetCycle();
        boolean $tmp = usesTypeVariable_compute();
        usesTypeVariable_computed = false;
        usesTypeVariable_initialized = false;
      }
      state.leaveCircle();
    } else if (usesTypeVariable_cycle != state.cycle()) {
      usesTypeVariable_cycle = state.cycle();
      if (state.resetCycle()) {
        usesTypeVariable_computed = false;
        usesTypeVariable_initialized = false;
        usesTypeVariable_cycle = null;
        return usesTypeVariable_value;
      }
      boolean new_usesTypeVariable_value = usesTypeVariable_compute();
      if (new_usesTypeVariable_value != usesTypeVariable_value) {
        state.setChangeInCycle();
      }
      usesTypeVariable_value = new_usesTypeVariable_value;
    } else {
    }
    return usesTypeVariable_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean usesTypeVariable_compute() {
      if(super.usesTypeVariable())
        return true;
      for(int i = 0; i < getNumArgument(); i++)
        if(getArgument(i).type().usesTypeVariable())
          return true;
      return false;
    }
  /**
   * @attribute syn
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1068
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1068")
  @SideEffect.Pure(group="original") public TypeDecl original() {
    TypeDecl original_value = genericDecl().original();
    return original_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void sourceTypeDecl_reset() {
    sourceTypeDecl_computed = false;
    
    sourceTypeDecl_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="sourceTypeDecl") protected boolean sourceTypeDecl_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="sourceTypeDecl") protected TypeDecl sourceTypeDecl_value;

  /**
   * @attribute syn
   * @aspect SourceDeclarations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1266
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SourceDeclarations", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1266")
  @SideEffect.Pure(group="sourceTypeDecl") public TypeDecl sourceTypeDecl() {
    ASTState state = state();
    if (sourceTypeDecl_computed) {
      return sourceTypeDecl_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    sourceTypeDecl_value = genericDecl().original().sourceTypeDecl();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    sourceTypeDecl_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return sourceTypeDecl_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void fullName_reset() {
    fullName_computed = false;
    
    fullName_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="fullName") protected boolean fullName_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="fullName") protected String fullName_value;

  /**
   * @attribute syn
   * @aspect TypeName
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\QualifiedNames.jrag:70
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeName", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\QualifiedNames.jrag:70")
  @SideEffect.Pure(group="fullName") public String fullName() {
    ASTState state = state();
    if (fullName_computed) {
      return fullName_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    fullName_value = fullName_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    fullName_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return fullName_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private String fullName_compute() {
      if(isNestedType())
        return enclosingType().fullName() + "." + nameWithArgs();
      String packageName = packageName();
      if(packageName.equals(""))
        return nameWithArgs();
      return packageName + "." + nameWithArgs();
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void typeName_reset() {
    typeName_computed = false;
    
    typeName_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="typeName") protected boolean typeName_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="typeName") protected String typeName_value;

  /**
   * @attribute syn
   * @aspect TypeName
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\QualifiedNames.jrag:79
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeName", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\QualifiedNames.jrag:79")
  @SideEffect.Pure(group="typeName") public String typeName() {
    ASTState state = state();
    if (typeName_computed) {
      return typeName_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    typeName_value = typeName_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    typeName_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return typeName_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private String typeName_compute() {
      if(isNestedType())
        return enclosingType().typeName() + "." + nameWithArgs();
      String packageName = packageName();
      if(packageName.equals("") || packageName.equals(PRIMITIVE_PACKAGE_NAME))
        return nameWithArgs();
      return packageName + "." + nameWithArgs();
    }
  /**
   * @attribute syn
   * @aspect GenericsParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsParTypeDecl.jrag:30
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="GenericsParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsParTypeDecl.jrag:30")
  @SideEffect.Pure(group="nameWithArgs") public String nameWithArgs() {
    {
        StringBuffer s = new StringBuffer();
        s.append(name());
        s.append("<");
        for(int i = 0; i < getNumArgument(); i++) {
          if(i != 0)
            s.append(", ");
          s.append(getArgument(i).type().fullName());
        }
        s.append(">");
        return s.toString();
      }
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void unimplementedMethods_reset() {
    unimplementedMethods_computed = false;
    
    unimplementedMethods_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="unimplementedMethods") protected boolean unimplementedMethods_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="unimplementedMethods") protected Collection unimplementedMethods_value;

  /**
   * @attribute syn
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:16
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:16")
  @SideEffect.Pure(group="unimplementedMethods") public Collection unimplementedMethods() {
    ASTState state = state();
    if (unimplementedMethods_computed) {
      return unimplementedMethods_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    unimplementedMethods_value = unimplementedMethods_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    unimplementedMethods_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return unimplementedMethods_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Collection unimplementedMethods_compute() {
      HashSet set = new HashSet();
      HashSet result = new HashSet();
      for(Iterator iter = genericDecl().unimplementedMethods().iterator(); iter.hasNext(); ) {
        MethodDecl m = (MethodDecl)iter.next();
        set.add(m.sourceMethodDecl());
      }
      for(Iterator iter = super.unimplementedMethods().iterator(); iter.hasNext(); ) {
        MethodDecl m = (MethodDecl)iter.next();
        if(set.contains(m.sourceMethodDecl()))
          result.add(m);
      }
      return result;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void uniqueIndex_reset() {
    uniqueIndex_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="uniqueIndex") protected boolean uniqueIndex_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="uniqueIndex") protected int uniqueIndex_value;

  /**
   * @attribute syn
   * @aspect Java2Rewrites
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Java2Rewrites.jrag:12
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Java2Rewrites", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Java2Rewrites.jrag:12")
  @SideEffect.Pure(group="uniqueIndex") public int uniqueIndex() {
    ASTState state = state();
    if (uniqueIndex_computed) {
      return uniqueIndex_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    uniqueIndex_value = genericDecl().uniqueIndex();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    uniqueIndex_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return uniqueIndex_value;
  }
  /**
   * @attribute syn
   * @aspect GenericsCodegen
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Backend\\GenericsCodegen.jrag:470
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="GenericsCodegen", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Backend\\GenericsCodegen.jrag:470")
  @SideEffect.Pure(group="typeArgumentsOpt") public String typeArgumentsOpt() {
    {
        StringBuffer buf = new StringBuffer();
        buf.append("<");
        for(int i = 0; i < getNumArgument(); i++)
          buf.append(getArgument(i).type().fieldTypeSignature());
        buf.append(">");
        return buf.toString();
      }
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void localMethodsSignatureMap_reset() {
    localMethodsSignatureMap_computed = false;
    
    localMethodsSignatureMap_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="localMethodsSignatureMap") protected boolean localMethodsSignatureMap_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="localMethodsSignatureMap") protected HashMap localMethodsSignatureMap_value;

  /**
   * @attribute syn
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:928
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:928")
  @SideEffect.Pure(group="localMethodsSignatureMap") public HashMap localMethodsSignatureMap() {
    ASTState state = state();
    if (localMethodsSignatureMap_computed) {
      return localMethodsSignatureMap_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    localMethodsSignatureMap_value = localMethodsSignatureMap_compute();
    if (true) {
    localMethodsSignatureMap_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return localMethodsSignatureMap_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private HashMap localMethodsSignatureMap_compute() {
      HashMap map = new HashMap();
      for(Iterator iter = original().localMethodsIterator(); iter.hasNext(); ) {
        MethodDecl decl = (MethodDecl)iter.next();
        if(!decl.isStatic() && (decl.usesTypeVariable() || isRawType())) {
          BodyDecl b = decl.p(this);
          b.is$Final = true;
          addBodyDecl(b);
          decl = (MethodDecl)b;
        }
        map.put(decl.signature(), decl);
      }
      return map;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void localFields_String_reset() {
    localFields_String_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="localFields_String") protected java.util.Map localFields_String_values;

  /**
   * @attribute syn
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:943
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:943")
  @SideEffect.Pure(group="localFields_String") public SimpleSet localFields(String name) {
    Object _parameters = name;
    if (localFields_String_values == null) localFields_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (localFields_String_values.containsKey(_parameters)) {
      return (SimpleSet) localFields_String_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    SimpleSet localFields_String_value = localFields_compute(name);
    if (true) {
    localFields_String_values.put(_parameters, localFields_String_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return localFields_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private SimpleSet localFields_compute(String name) {
      SimpleSet set = SimpleSet.emptySet;
      for(Iterator iter = original().localFields(name).iterator(); iter.hasNext(); ) {
        FieldDeclaration f = (FieldDeclaration)iter.next();
        if(!f.isStatic() && (f.usesTypeVariable() || isRawType())) {
          BodyDecl b = f.p(this);
          b.is$Final = true;
          addBodyDecl(b);
          f = (FieldDeclaration)b;
        }
        set = set.add(f);
      }
      return set;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void localTypeDecls_String_reset() {
    localTypeDecls_String_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="localTypeDecls_String") protected java.util.Map localTypeDecls_String_values;

  /**
   * @attribute syn
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:958
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:958")
  @SideEffect.Pure(group="localTypeDecls_String") public SimpleSet localTypeDecls(String name) {
    Object _parameters = name;
    if (localTypeDecls_String_values == null) localTypeDecls_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (localTypeDecls_String_values.containsKey(_parameters)) {
      return (SimpleSet) localTypeDecls_String_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    SimpleSet localTypeDecls_String_value = localTypeDecls_compute(name);
    if (true) {
    localTypeDecls_String_values.put(_parameters, localTypeDecls_String_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return localTypeDecls_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private SimpleSet localTypeDecls_compute(String name) {
      SimpleSet set = SimpleSet.emptySet;
      for(Iterator iter = original().localTypeDecls(name).iterator(); iter.hasNext(); ) {
        TypeDecl t = (TypeDecl)iter.next();
        if(t.isStatic())
          set = set.add(t);
        else {
          BodyDecl b;
          TypeDecl typeDecl;
          if(t instanceof ClassDecl) {
            ClassDecl classDecl = (ClassDecl)t;
            typeDecl = classDecl.p(this);
            b = new MemberClassDecl((ClassDecl)typeDecl);
            b.is$Final = true;
            addBodyDecl(b);
            set = set.add(typeDecl);
          }
          else if(t instanceof InterfaceDecl) {
            InterfaceDecl interfaceDecl = (InterfaceDecl)t;
            typeDecl = interfaceDecl.p(this);
            b = new MemberInterfaceDecl((InterfaceDecl)typeDecl);
            b.is$Final = true;
            addBodyDecl(b);
            set = set.add(typeDecl);
          }
        }
      }
      return set;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void constructors_reset() {
    constructors_computed = false;
    
    constructors_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="constructors") protected boolean constructors_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="constructors") protected Collection constructors_value;

  /**
   * @attribute syn
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:988
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:988")
  @SideEffect.Pure(group="constructors") public Collection constructors() {
    ASTState state = state();
    if (constructors_computed) {
      return constructors_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    constructors_value = constructors_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    constructors_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return constructors_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Collection constructors_compute() {
      Collection set = new ArrayList();
      for(Iterator iter = original().constructors().iterator(); iter.hasNext(); ) {
        ConstructorDecl c = (ConstructorDecl)iter.next();
        BodyDecl b = c.p(this);
        b.is$Final = true;
        addBodyDecl(b);
        set.add(b);
      }
      return set;
    }
  /**
   * @attribute inh
   * @aspect GenericsParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsParTypeDecl.jrag:45
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="GenericsParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsParTypeDecl.jrag:45")
  @SideEffect.Pure(group="genericDecl") public TypeDecl genericDecl() {
    ASTState state = state();
    if (genericDecl_computed) {
      return genericDecl_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    genericDecl_value = getParent().Define_genericDecl(this, null);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    genericDecl_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return genericDecl_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void genericDecl_reset() {
    genericDecl_computed = false;
    
    genericDecl_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="genericDecl") protected boolean genericDecl_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="genericDecl") protected TypeDecl genericDecl_value;

  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\SyntacticClassification.jrag:20
   * @apilevel internal
   */
 @SideEffect.Pure public NameType Define_nameType(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getArgumentListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:447
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return NameType.TYPE_NAME;
    }
    else {
      return super.Define_nameType(_callerNode, _childNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_nameType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsParTypeDecl.jrag:45
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_genericDecl(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getBodyDeclListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsParTypeDecl.jrag:56
      int index = _callerNode.getIndexOfChild(_childNode);
      {
          if(getBodyDecl(index) instanceof MemberTypeDecl) {
            MemberTypeDecl m = (MemberTypeDecl)getBodyDecl(index);
            return extractSingleType(genericDecl().memberTypes(m.typeDecl().name()));
          }
          return genericDecl();
        }
    }
    else {
      return getParent().Define_genericDecl(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_genericDecl(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Pure public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}

package AST;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast class
 * @declaredat ASTState:2
 */
public class ASTState extends java.lang.Object {
  
  /** @apilevel internal */
  protected static class CircularValue {
	@SideEffect.Ignore Object value;
	@SideEffect.Ignore Cycle cycle;
  }

  

  /**
   * Instances of this class are used to uniquely identify circular evaluation cycles.
   * @apilevel internal
   */
  protected static class Cycle {
  }

  

  /** The cycle ID used outside of circular evaluation. */
  public static final Cycle NON_CYCLE = new Cycle();

  

  /**
   * Tracks the state of the current circular evaluation. This class defines a
   * stack structure where the next element on the stack is pointed to by the
   * {@code next} field.
   * @apilevel internal
   */
  protected static class CircleState {
    final CircleState next;
    boolean inCircle = false;
    boolean resetCycle = false;
    boolean change = false;

    /** Evaluation depth of lazy attributes. */
    int lazyAttribute = 0;

    /** Cycle ID of the latest cycle in this circular evaluation. */
    Cycle cycle = NON_CYCLE;

    protected CircleState(CircleState next) {
      this.next = next;
    }
  }

  

  /** Sentinel circle state representing non-circular evaluation. */
  private static final CircleState CIRCLE_BOTTOM = new CircleState(null);

  

  /**
   * Current circular state.
   * @apilevel internal
   */
   @SideEffect.Secret(group="_ASTNode") private CircleState circle = CIRCLE_BOTTOM;

  

  /** @apilevel internal */
  @SideEffect.Ignore protected boolean inCircle() {
    return circle.inCircle;
  }

  

  /** @apilevel internal */
  @SideEffect.Ignore protected boolean calledByLazyAttribute() {
    return circle.lazyAttribute > 0;
  }

  

  /** @apilevel internal */
  @SideEffect.Ignore protected void enterLazyAttribute() {
    circle.lazyAttribute += 1;
  }

  

  /** @apilevel internal */
 @SideEffect.Ignore  protected void leaveLazyAttribute() {
    circle.lazyAttribute -= 1;
  }

  

  /** @apilevel internal */
  @SideEffect.Ignore protected void enterCircle() {
    CircleState next = new CircleState(circle);
    next.inCircle = true;
    circle = next;
  }

  

  /** @apilevel internal */
  @SideEffect.Ignore protected void leaveCircle() {
    circle = circle.next;
  }

  

  /** @apilevel internal */
  @SideEffect.Ignore protected Cycle nextCycle() {
    Cycle cycle = new Cycle();
    circle.cycle = cycle;
    return cycle;
  }

  

  /** @apilevel internal */
  @SideEffect.Ignore protected Cycle cycle() {
    return circle.cycle;
  }

  

  /** @apilevel internal */
  @SideEffect.Ignore protected CircleState currentCircle() {
    return circle;
  }

  


  /** @apilevel internal */
  @SideEffect.Ignore protected void setChangeInCycle() {
    circle.change = true;
  }

  

  /** @apilevel internal */
  @SideEffect.Ignore protected boolean testAndClearChangeInCycle() {
    boolean change = circle.change;
    circle.change = false;
    return change;
  }

  

  /** @apilevel internal */
  @SideEffect.Ignore protected boolean changeInCycle() {
    return circle.change;
  }

  


  /** @apilevel internal */
  @SideEffect.Ignore protected void startResetCycle() {
    circle.resetCycle = true;
  }

  

  /** @apilevel internal */
  @SideEffect.Ignore protected boolean resetCycle() {
    return circle.resetCycle;
  }

  

  protected ASTState() {
    stack = new int[64];
    pos = 0;
  }

  
  /** @apilevel internal */
  public static final int REWRITE_CHANGE = 1;

  

  /** @apilevel internal */
  public static final int REWRITE_NOCHANGE = 2;

  

  /** @apilevel internal */
  public static final int REWRITE_INTERRUPT = 3;

  

  public int boundariesCrossed = 0;

  

  // state code
  private int[] stack;

  

  private int pos;

  

  private void ensureSize(int size) {
    if (size < stack.length) {
      return;
    }
    int[] newStack = new int[stack.length * 2];
    System.arraycopy(stack, 0, newStack, 0, stack.length);
    stack = newStack;
  }

  

  public void push(int i) {
    ensureSize(pos+1);
    stack[pos++] = i;
  }

  

  public int pop() {
    return stack[--pos];
  }

  

  public int peek() {
    return stack[pos - 1];
  }

  @SideEffect.Ignore public void reset() {
    // Reset circular evaluation state.
    circle = CIRCLE_BOTTOM;
    boundariesCrossed = 0;
  }


}

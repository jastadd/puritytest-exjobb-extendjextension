/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package AST;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.ast:2
 * @production GenericClassDecl : {@link ClassDecl} ::= <span class="component">{@link Modifiers}</span> <span class="component">&lt;ID:String&gt;</span> <span class="component">[SuperClassAccess:{@link Access}]</span> <span class="component">Implements:{@link Access}*</span> <span class="component">{@link BodyDecl}*</span> <span class="component">TypeParameter:{@link TypeVariable}*</span> <span class="component">ParTypeDecl:{@link ParClassDecl}*</span>;

 */
public class GenericClassDecl extends ClassDecl implements Cloneable, GenericTypeDecl {
  /**
   * @aspect GenericsTypeCheck
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:395
   */
  public void typeCheck() {
    super.typeCheck();
    if(instanceOf(typeThrowable()))
      error(" generic class " + typeName() + " may not directly or indirectly inherit java.lang.Throwable");
  }
  /**
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1086
   */
  public ClassDecl p(Parameterization parTypeDecl) {
    GenericClassDecl c = new GenericClassDeclSubstituted(
      (Modifiers)getModifiers().fullCopy(),
      getID(),
      hasSuperClassAccess() ? new Opt(getSuperClassAccess().type().substitute(parTypeDecl)) : new Opt(),
      getImplementsList().substitute(parTypeDecl),
      new List(),
      new List(), // delegates TypeParameter lookup to original 
      this
    );
    return c;
  }
  /**
   * @aspect GenericsPrettyPrint
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsPrettyPrint.jrag:61
   */
  private void ppTypeParameters(StringBuffer s) {
      s.append('<');
      if (getNumTypeParameter() > 0) {
	  getTypeParameter(0).toString(s);
	  for (int i = 1; i < getNumTypeParameter(); i++) {
	      s.append(", ");
	      getTypeParameter(i).toString(s);
	  }
      }
      s.append('>');
  }
  /**
   * @aspect GenericsPrettyPrint
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsPrettyPrint.jrag:73
   */
  public void toString(StringBuffer s) {
		getModifiers().toString(s);
		s.append("class " + getID());
		ppTypeParameters(s);
		if(hasSuperClassAccess()) {
			s.append(" extends ");
			getSuperClassAccess().toString(s);
		}
		if(getNumImplements() > 0) {
			s.append(" implements ");
			getImplements(0).toString(s);
			for(int i = 1; i < getNumImplements(); i++) {
				s.append(", ");
				getImplements(i).toString(s);
			}
		}

    /*
    s.append(" instantiated with: ");
    for(int i = 0; i < getNumParTypeDecl(); i++) {
      if(i != 0) s.append(", ");
      ParTypeDecl decl = getParTypeDecl(i);
      s.append("<");
      for(int j = 0; j < decl.getNumArgument(); j++) {
        if(j != 0) s.append(", ");
        s.append(decl.getArgument(j).type().fullName());
      }
      s.append(">");
    }
    */

		ppBodyDecls(s);    
    
    /*
    for(int i = 0; i < getNumParTypeDecl(); i++) {
      ParClassDecl decl = getParTypeDecl(i);
      decl.toString(s);
    }
    */
    
	}
  /**
   * @aspect Generics
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:212
   */
  public TypeDecl makeGeneric(Signatures.ClassSignature s) {
    return (TypeDecl)this;
  }
  /**
   * @aspect GenericsNameBinding
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:459
   */
  public SimpleSet addTypeVariables(SimpleSet c, String name) {
    GenericTypeDecl original = (GenericTypeDecl)original();
    for(int i = 0; i < original.getNumTypeParameter(); i++) {
      TypeVariable p = original.getTypeParameter(i);
      if(p.name().equals(name))
        c = c.add(p);
    }
    return c;
  }
  /**
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:660
   */
  public List createArgumentList(ArrayList params) {
    GenericTypeDecl original = (GenericTypeDecl)original();
    List list = new List();
    if(params.isEmpty())
      for(int i = 0; i < original.getNumTypeParameter(); i++)
        list.add(original.getTypeParameter(i).erasure().createBoundAccess());
    else
      for(Iterator iter = params.iterator(); iter.hasNext(); )
        list.add(((TypeDecl)iter.next()).createBoundAccess());
    return list;
  }
  /**
   * @declaredat ASTNode:1
   */
  public GenericClassDecl() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[6];
    setChild(new Opt(), 1);
    setChild(new List(), 2);
    setChild(new List(), 3);
    setChild(new List(), 4);
    setChild(new List(), 5);
  }
  /**
   * @declaredat ASTNode:18
   */
  public GenericClassDecl(Modifiers p0, String p1, Opt<Access> p2, List<Access> p3, List<BodyDecl> p4, List<TypeVariable> p5) {
    setChild(p0, 0);
    setID(p1);
    setChild(p2, 1);
    setChild(p3, 2);
    setChild(p4, 3);
    setChild(p5, 4);
  }
  /**
   * @declaredat ASTNode:26
   */
  public GenericClassDecl(Modifiers p0, beaver.Symbol p1, Opt<Access> p2, List<Access> p3, List<BodyDecl> p4, List<TypeVariable> p5) {
    setChild(p0, 0);
    setID(p1);
    setChild(p2, 1);
    setChild(p3, 2);
    setChild(p4, 3);
    setChild(p5, 4);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:35
   */
  @SideEffect.Pure protected int numChildren() {
    return 5;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:41
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    rawType_reset();
    getParTypeDeclList_reset();
    lookupParTypeDecl_ParTypeAccess_reset();
    lookupParTypeDecl_ArrayList_reset();
    usesTypeVariable_reset();
    subtype_TypeDecl_reset();
    instanceOf_TypeDecl_reset();
    constantPoolName_reset();
    needsSignatureAttribute_reset();
    classSignature_reset();
    getPlaceholderMethodList_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:60
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:64
   */
  @SideEffect.Fresh public GenericClassDecl clone() throws CloneNotSupportedException {
    GenericClassDecl node = (GenericClassDecl) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:69
   */
  @SideEffect.Fresh(group="_ASTNode") public GenericClassDecl copy() {
    try {
      GenericClassDecl node = (GenericClassDecl) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:88
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public GenericClassDecl fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:98
   */
  @SideEffect.Fresh(group="_ASTNode") public GenericClassDecl treeCopyNoTransform() {
    GenericClassDecl tree = (GenericClassDecl) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 5:
          tree.children[i] = new List();
          continue;
        }
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:123
   */
  @SideEffect.Fresh(group="_ASTNode") public GenericClassDecl treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:128
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_ID == ((GenericClassDecl) node).tokenString_ID);    
  }
  /**
   * Replaces the Modifiers child.
   * @param node The new node to replace the Modifiers child.
   * @apilevel high-level
   */
  public void setModifiers(Modifiers node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Modifiers child.
   * @return The current node used as the Modifiers child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Modifiers")
  @SideEffect.Pure public Modifiers getModifiers() {
    return (Modifiers) getChild(0);
  }
  /**
   * Retrieves the Modifiers child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Modifiers child.
   * @apilevel low-level
   */
  public Modifiers getModifiersNoTransform() {
    return (Modifiers) getChildNoTransform(0);
  }
  /**
   * Replaces the lexeme ID.
   * @param value The new value for the lexeme ID.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setID(String value) {
    tokenString_ID = value;
  }
  /**
   * JastAdd-internal setter for lexeme ID using the Beaver parser.
   * @param symbol Symbol containing the new value for the lexeme ID
   * @apilevel internal
   */
  public void setID(beaver.Symbol symbol) {
    if (symbol.value != null && !(symbol.value instanceof String))
    throw new UnsupportedOperationException("setID is only valid for String lexemes");
    tokenString_ID = (String)symbol.value;
    IDstart = symbol.getStart();
    IDend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme ID.
   * @return The value for the lexeme ID.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="ID")
  @SideEffect.Pure(group="_ASTNode") public String getID() {
    return tokenString_ID != null ? tokenString_ID : "";
  }
  /**
   * Replaces the optional node for the SuperClassAccess child. This is the <code>Opt</code>
   * node containing the child SuperClassAccess, not the actual child!
   * @param opt The new node to be used as the optional node for the SuperClassAccess child.
   * @apilevel low-level
   */
  public void setSuperClassAccessOpt(Opt<Access> opt) {
    setChild(opt, 1);
  }
  /**
   * Replaces the (optional) SuperClassAccess child.
   * @param node The new node to be used as the SuperClassAccess child.
   * @apilevel high-level
   */
  public void setSuperClassAccess(Access node) {
    getSuperClassAccessOpt().setChild(node, 0);
  }
  /**
   * Check whether the optional SuperClassAccess child exists.
   * @return {@code true} if the optional SuperClassAccess child exists, {@code false} if it does not.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasSuperClassAccess() {
    return getSuperClassAccessOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) SuperClassAccess child.
   * @return The SuperClassAccess child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   */
  @SideEffect.Pure public Access getSuperClassAccess() {
    return (Access) getSuperClassAccessOpt().getChild(0);
  }
  /**
   * Retrieves the optional node for the SuperClassAccess child. This is the <code>Opt</code> node containing the child SuperClassAccess, not the actual child!
   * @return The optional node for child the SuperClassAccess child.
   * @apilevel low-level
   */
  @ASTNodeAnnotation.OptChild(name="SuperClassAccess")
  @SideEffect.Pure public Opt<Access> getSuperClassAccessOpt() {
    return (Opt<Access>) getChild(1);
  }
  /**
   * Retrieves the optional node for child SuperClassAccess. This is the <code>Opt</code> node containing the child SuperClassAccess, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child SuperClassAccess.
   * @apilevel low-level
   */
  @SideEffect.Pure public Opt<Access> getSuperClassAccessOptNoTransform() {
    return (Opt<Access>) getChildNoTransform(1);
  }
  /**
   * Replaces the Implements list.
   * @param list The new list node to be used as the Implements list.
   * @apilevel high-level
   */
  public void setImplementsList(List<Access> list) {
    setChild(list, 2);
  }
  /**
   * Retrieves the number of children in the Implements list.
   * @return Number of children in the Implements list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumImplements() {
    return getImplementsList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Implements list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Implements list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumImplementsNoTransform() {
    return getImplementsListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Implements list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Implements list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Access getImplements(int i) {
    return (Access) getImplementsList().getChild(i);
  }
  /**
   * Check whether the Implements list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasImplements() {
    return getImplementsList().getNumChild() != 0;
  }
  /**
   * Append an element to the Implements list.
   * @param node The element to append to the Implements list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addImplements(Access node) {
    List<Access> list = (parent == null) ? getImplementsListNoTransform() : getImplementsList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addImplementsNoTransform(Access node) {
    List<Access> list = getImplementsListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Implements list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setImplements(Access node, int i) {
    List<Access> list = getImplementsList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Implements list.
   * @return The node representing the Implements list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Implements")
  @SideEffect.Pure(group="_ASTNode") public List<Access> getImplementsList() {
    List<Access> list = (List<Access>) getChild(2);
    return list;
  }
  /**
   * Retrieves the Implements list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Implements list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Access> getImplementsListNoTransform() {
    return (List<Access>) getChildNoTransform(2);
  }
  /**
   * @return the element at index {@code i} in the Implements list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Access getImplementsNoTransform(int i) {
    return (Access) getImplementsListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Implements list.
   * @return The node representing the Implements list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Access> getImplementss() {
    return getImplementsList();
  }
  /**
   * Retrieves the Implements list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Implements list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Access> getImplementssNoTransform() {
    return getImplementsListNoTransform();
  }
  /**
   * Replaces the BodyDecl list.
   * @param list The new list node to be used as the BodyDecl list.
   * @apilevel high-level
   */
  public void setBodyDeclList(List<BodyDecl> list) {
    setChild(list, 3);
  }
  /**
   * Retrieves the number of children in the BodyDecl list.
   * @return Number of children in the BodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumBodyDecl() {
    return getBodyDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the BodyDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the BodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumBodyDeclNoTransform() {
    return getBodyDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the BodyDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the BodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public BodyDecl getBodyDecl(int i) {
    return (BodyDecl) getBodyDeclList().getChild(i);
  }
  /**
   * Check whether the BodyDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasBodyDecl() {
    return getBodyDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the BodyDecl list.
   * @param node The element to append to the BodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addBodyDecl(BodyDecl node) {
    List<BodyDecl> list = (parent == null) ? getBodyDeclListNoTransform() : getBodyDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addBodyDeclNoTransform(BodyDecl node) {
    List<BodyDecl> list = getBodyDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the BodyDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setBodyDecl(BodyDecl node, int i) {
    List<BodyDecl> list = getBodyDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the BodyDecl list.
   * @return The node representing the BodyDecl list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="BodyDecl")
  @SideEffect.Pure(group="_ASTNode") public List<BodyDecl> getBodyDeclList() {
    List<BodyDecl> list = (List<BodyDecl>) getChild(3);
    return list;
  }
  /**
   * Retrieves the BodyDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the BodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<BodyDecl> getBodyDeclListNoTransform() {
    return (List<BodyDecl>) getChildNoTransform(3);
  }
  /**
   * @return the element at index {@code i} in the BodyDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public BodyDecl getBodyDeclNoTransform(int i) {
    return (BodyDecl) getBodyDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the BodyDecl list.
   * @return The node representing the BodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<BodyDecl> getBodyDecls() {
    return getBodyDeclList();
  }
  /**
   * Retrieves the BodyDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the BodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<BodyDecl> getBodyDeclsNoTransform() {
    return getBodyDeclListNoTransform();
  }
  /**
   * Replaces the TypeParameter list.
   * @param list The new list node to be used as the TypeParameter list.
   * @apilevel high-level
   */
  public void setTypeParameterList(List<TypeVariable> list) {
    setChild(list, 4);
  }
  /**
   * Retrieves the number of children in the TypeParameter list.
   * @return Number of children in the TypeParameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumTypeParameter() {
    return getTypeParameterList().getNumChild();
  }
  /**
   * Retrieves the number of children in the TypeParameter list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the TypeParameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumTypeParameterNoTransform() {
    return getTypeParameterListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the TypeParameter list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the TypeParameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public TypeVariable getTypeParameter(int i) {
    return (TypeVariable) getTypeParameterList().getChild(i);
  }
  /**
   * Check whether the TypeParameter list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasTypeParameter() {
    return getTypeParameterList().getNumChild() != 0;
  }
  /**
   * Append an element to the TypeParameter list.
   * @param node The element to append to the TypeParameter list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addTypeParameter(TypeVariable node) {
    List<TypeVariable> list = (parent == null) ? getTypeParameterListNoTransform() : getTypeParameterList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addTypeParameterNoTransform(TypeVariable node) {
    List<TypeVariable> list = getTypeParameterListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the TypeParameter list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setTypeParameter(TypeVariable node, int i) {
    List<TypeVariable> list = getTypeParameterList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the TypeParameter list.
   * @return The node representing the TypeParameter list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="TypeParameter")
  @SideEffect.Pure(group="_ASTNode") public List<TypeVariable> getTypeParameterList() {
    List<TypeVariable> list = (List<TypeVariable>) getChild(4);
    return list;
  }
  /**
   * Retrieves the TypeParameter list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the TypeParameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<TypeVariable> getTypeParameterListNoTransform() {
    return (List<TypeVariable>) getChildNoTransform(4);
  }
  /**
   * @return the element at index {@code i} in the TypeParameter list without
   * triggering rewrites.
   */
  @SideEffect.Pure public TypeVariable getTypeParameterNoTransform(int i) {
    return (TypeVariable) getTypeParameterListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the TypeParameter list.
   * @return The node representing the TypeParameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<TypeVariable> getTypeParameters() {
    return getTypeParameterList();
  }
  /**
   * Retrieves the TypeParameter list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the TypeParameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<TypeVariable> getTypeParametersNoTransform() {
    return getTypeParameterListNoTransform();
  }
  /**
   * Retrieves the number of children in the ParTypeDecl list.
   * @return Number of children in the ParTypeDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumParTypeDecl() {
    return getParTypeDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the ParTypeDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the ParTypeDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumParTypeDeclNoTransform() {
    return getParTypeDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the ParTypeDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the ParTypeDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public ParClassDecl getParTypeDecl(int i) {
    return (ParClassDecl) getParTypeDeclList().getChild(i);
  }
  /**
   * Check whether the ParTypeDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasParTypeDecl() {
    return getParTypeDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the ParTypeDecl list.
   * @param node The element to append to the ParTypeDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addParTypeDecl(ParClassDecl node) {
    List<ParClassDecl> list = (parent == null) ? getParTypeDeclListNoTransform() : getParTypeDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addParTypeDeclNoTransform(ParClassDecl node) {
    List<ParClassDecl> list = getParTypeDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the ParTypeDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setParTypeDecl(ParClassDecl node, int i) {
    List<ParClassDecl> list = getParTypeDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the child position of the ParTypeDecl list.
   * @return The the child position of the ParTypeDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure protected int getParTypeDeclListChildPosition() {
    return 5;
  }
  /**
   * Retrieves the ParTypeDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the ParTypeDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<ParClassDecl> getParTypeDeclListNoTransform() {
    return (List<ParClassDecl>) getChildNoTransform(5);
  }
  /**
   * @return the element at index {@code i} in the ParTypeDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public ParClassDecl getParTypeDeclNoTransform(int i) {
    return (ParClassDecl) getParTypeDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the ParTypeDecl list.
   * @return The node representing the ParTypeDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<ParClassDecl> getParTypeDecls() {
    return getParTypeDeclList();
  }
  /**
   * Retrieves the ParTypeDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the ParTypeDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<ParClassDecl> getParTypeDeclsNoTransform() {
    return getParTypeDeclListNoTransform();
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void rawType_reset() {
    rawType_computed = false;
    
    rawType_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="rawType") protected boolean rawType_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="rawType") protected TypeDecl rawType_value;

  /**
   * @attribute syn
   * @aspect Generics
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:144
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Generics", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:144")
  @SideEffect.Pure(group="rawType") public TypeDecl rawType() {
    ASTState state = state();
    if (rawType_computed) {
      return rawType_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    rawType_value = lookupParTypeDecl(new ArrayList());
    if (isFinal && _boundaries == state().boundariesCrossed) {
    rawType_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return rawType_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void getParTypeDeclList_reset() {
    getParTypeDeclList_computed = false;
    
    getParTypeDeclList_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="getParTypeDeclList") protected boolean getParTypeDeclList_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="getParTypeDeclList") protected List getParTypeDeclList_value;

  /**
   * @attribute syn nta
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:591
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:591")
  @SideEffect.Pure(group="getParTypeDeclList") public List getParTypeDeclList() {
    ASTState state = state();
    if (getParTypeDeclList_computed) {
      return (List) getChild(getParTypeDeclListChildPosition());
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    getParTypeDeclList_value = new List();
    setChild(getParTypeDeclList_value, getParTypeDeclListChildPosition());
    if (true) {
    getParTypeDeclList_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    List node = (List) this.getChild(getParTypeDeclListChildPosition());
    return node;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void lookupParTypeDecl_ParTypeAccess_reset() {
    lookupParTypeDecl_ParTypeAccess_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="lookupParTypeDecl_ParTypeAccess") protected java.util.Map lookupParTypeDecl_ParTypeAccess_values;

  /**
   * @attribute syn
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:594
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:594")
  @SideEffect.Pure(group="lookupParTypeDecl_ParTypeAccess") public TypeDecl lookupParTypeDecl(ParTypeAccess p) {
    Object _parameters = p;
    if (lookupParTypeDecl_ParTypeAccess_values == null) lookupParTypeDecl_ParTypeAccess_values = new java.util.HashMap(4);
    ASTState state = state();
    if (lookupParTypeDecl_ParTypeAccess_values.containsKey(_parameters)) {
      return (TypeDecl) lookupParTypeDecl_ParTypeAccess_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    TypeDecl lookupParTypeDecl_ParTypeAccess_value = lookupParTypeDecl_compute(p);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    lookupParTypeDecl_ParTypeAccess_values.put(_parameters, lookupParTypeDecl_ParTypeAccess_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return lookupParTypeDecl_ParTypeAccess_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private TypeDecl lookupParTypeDecl_compute(ParTypeAccess p) {
      for(int i = 0; i < getNumParTypeDecl(); i++) {
        ParTypeDecl decl = (ParTypeDecl)getParTypeDecl(i);
        if(!decl.isRawType() && decl.sameSignature(p))
          return (TypeDecl)decl;
      }
      ParClassDecl typeDecl = new ParClassDecl();
      typeDecl.setModifiers((Modifiers)getModifiers().fullCopy());
      typeDecl.setID(getID());
      addParTypeDecl(typeDecl);
      List list = new List();
      for(int i = 0; i < p.getNumTypeArgument(); i++)
        list.add(p.getTypeArgument(i).type().createBoundAccess());
      typeDecl.setArgumentList(list);
      typeDecl.is$Final = true;
      return typeDecl;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void lookupParTypeDecl_ArrayList_reset() {
    lookupParTypeDecl_ArrayList_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="lookupParTypeDecl_ArrayList") protected java.util.Map lookupParTypeDecl_ArrayList_values;

  /**
   * @attribute syn
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:631
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:631")
  @SideEffect.Pure(group="lookupParTypeDecl_ArrayList") public TypeDecl lookupParTypeDecl(ArrayList list) {
    Object _parameters = list;
    if (lookupParTypeDecl_ArrayList_values == null) lookupParTypeDecl_ArrayList_values = new java.util.HashMap(4);
    ASTState state = state();
    if (lookupParTypeDecl_ArrayList_values.containsKey(_parameters)) {
      return (TypeDecl) lookupParTypeDecl_ArrayList_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    TypeDecl lookupParTypeDecl_ArrayList_value = lookupParTypeDecl_compute(list);
    if (true) {
    lookupParTypeDecl_ArrayList_values.put(_parameters, lookupParTypeDecl_ArrayList_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return lookupParTypeDecl_ArrayList_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private TypeDecl lookupParTypeDecl_compute(ArrayList list) {
      for(int i = 0; i < getNumParTypeDecl(); i++) {
        ParTypeDecl decl = (ParTypeDecl)getParTypeDecl(i);
        if(decl.isRawType() ? list.isEmpty() : (!list.isEmpty() && decl.sameSignature(list)))
          return (TypeDecl)decl;
      }
      ParClassDecl typeDecl = list.size() == 0 ? new RawClassDecl() : new ParClassDecl();
      typeDecl.setModifiers((Modifiers)getModifiers().fullCopy());
      typeDecl.setID(getID());
      addParTypeDecl(typeDecl);
      typeDecl.setArgumentList(createArgumentList(list));
      typeDecl.is$Final = true;
      return typeDecl;
    }
/** @apilevel internal */
protected ASTState.Cycle usesTypeVariable_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void usesTypeVariable_reset() {
    usesTypeVariable_computed = false;
    usesTypeVariable_initialized = false;
    usesTypeVariable_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="usesTypeVariable") protected boolean usesTypeVariable_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="usesTypeVariable") protected boolean usesTypeVariable_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="usesTypeVariable") protected boolean usesTypeVariable_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:912")
  @SideEffect.Pure(group="usesTypeVariable") public boolean usesTypeVariable() {
    if (usesTypeVariable_computed) {
      return usesTypeVariable_value;
    }
    ASTState state = state();
    if (!usesTypeVariable_initialized) {
      usesTypeVariable_initialized = true;
      usesTypeVariable_value = false;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      int _boundaries = state.boundariesCrossed;
      boolean isFinal = this.is$Final();
      do {
        usesTypeVariable_cycle = state.nextCycle();
        boolean new_usesTypeVariable_value = true;
        if (new_usesTypeVariable_value != usesTypeVariable_value) {
          state.setChangeInCycle();
        }
        usesTypeVariable_value = new_usesTypeVariable_value;
      } while (state.testAndClearChangeInCycle());
      if (isFinal && _boundaries == state().boundariesCrossed) {
        usesTypeVariable_computed = true;
      } else {
        state.startResetCycle();
        boolean $tmp = true;
        usesTypeVariable_computed = false;
        usesTypeVariable_initialized = false;
      }
      state.leaveCircle();
    } else if (usesTypeVariable_cycle != state.cycle()) {
      usesTypeVariable_cycle = state.cycle();
      if (state.resetCycle()) {
        usesTypeVariable_computed = false;
        usesTypeVariable_initialized = false;
        usesTypeVariable_cycle = null;
        return usesTypeVariable_value;
      }
      boolean new_usesTypeVariable_value = true;
      if (new_usesTypeVariable_value != usesTypeVariable_value) {
        state.setChangeInCycle();
      }
      usesTypeVariable_value = new_usesTypeVariable_value;
    } else {
    }
    return usesTypeVariable_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void subtype_TypeDecl_reset() {
    subtype_TypeDecl_values = null;
  }
  protected java.util.Map subtype_TypeDecl_values;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="GenericsSubtype", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:405")
  @SideEffect.Pure(group="subtype_TypeDecl") public boolean subtype(TypeDecl type) {
    Object _parameters = type;
    if (subtype_TypeDecl_values == null) subtype_TypeDecl_values = new java.util.HashMap(4);
    ASTState.CircularValue _value;
    if (subtype_TypeDecl_values.containsKey(_parameters)) {
      Object _cache = subtype_TypeDecl_values.get(_parameters);
      if (!(_cache instanceof ASTState.CircularValue)) {
        return (Boolean) _cache;
      } else {
        _value = (ASTState.CircularValue) _cache;
      }
    } else {
      _value = new ASTState.CircularValue();
      subtype_TypeDecl_values.put(_parameters, _value);
      _value.value = true;
    }
    ASTState state = state();
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      int _boundaries = state.boundariesCrossed;
      boolean isFinal = this.is$Final();
      boolean new_subtype_TypeDecl_value;
      do {
        _value.cycle = state.nextCycle();
        new_subtype_TypeDecl_value = type.supertypeGenericClassDecl(this);
        if (new_subtype_TypeDecl_value != ((Boolean)_value.value)) {
          state.setChangeInCycle();
          _value.value = new_subtype_TypeDecl_value;
        }
      } while (state.testAndClearChangeInCycle());
      if (isFinal && _boundaries == state().boundariesCrossed) {
        subtype_TypeDecl_values.put(_parameters, new_subtype_TypeDecl_value);
      } else {
        subtype_TypeDecl_values.remove(_parameters);
        state.startResetCycle();
        boolean $tmp = type.supertypeGenericClassDecl(this);
      }
      state.leaveCircle();
      return new_subtype_TypeDecl_value;
    } else if (_value.cycle != state.cycle()) {
      _value.cycle = state.cycle();
      boolean new_subtype_TypeDecl_value = type.supertypeGenericClassDecl(this);
      if (state.resetCycle()) {
        subtype_TypeDecl_values.remove(_parameters);
      }
      else if (new_subtype_TypeDecl_value != ((Boolean)_value.value)) {
        state.setChangeInCycle();
        _value.value = new_subtype_TypeDecl_value;
      }
      return new_subtype_TypeDecl_value;
    } else {
      return (Boolean) _value.value;
    }
  }
  /**
   * @attribute syn
   * @aspect GenericsSubtype
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:125
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="GenericsSubtype", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:125")
  @SideEffect.Pure(group="supertypeParClassDecl_ParClassDecl") public boolean supertypeParClassDecl(ParClassDecl type) {
    boolean supertypeParClassDecl_ParClassDecl_value = type.genericDecl().original().subtype(this);
    return supertypeParClassDecl_ParClassDecl_value;
  }
  /**
   * @attribute syn
   * @aspect GenericsSubtype
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:129
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="GenericsSubtype", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsSubtype.jrag:129")
  @SideEffect.Pure(group="supertypeParInterfaceDecl_ParInterfaceDecl") public boolean supertypeParInterfaceDecl(ParInterfaceDecl type) {
    boolean supertypeParInterfaceDecl_ParInterfaceDecl_value = type.genericDecl().original().subtype(this);
    return supertypeParInterfaceDecl_ParInterfaceDecl_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void instanceOf_TypeDecl_reset() {
    instanceOf_TypeDecl_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="instanceOf_TypeDecl") protected java.util.Map instanceOf_TypeDecl_values;

  /**
   * @attribute syn
   * @aspect TypeWideningAndIdentity
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:407
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeWideningAndIdentity", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:407")
  @SideEffect.Pure(group="instanceOf_TypeDecl") public boolean instanceOf(TypeDecl type) {
    Object _parameters = type;
    if (instanceOf_TypeDecl_values == null) instanceOf_TypeDecl_values = new java.util.HashMap(4);
    ASTState state = state();
    if (instanceOf_TypeDecl_values.containsKey(_parameters)) {
      return (Boolean) instanceOf_TypeDecl_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boolean instanceOf_TypeDecl_value = subtype(type);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    instanceOf_TypeDecl_values.put(_parameters, instanceOf_TypeDecl_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return instanceOf_TypeDecl_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void constantPoolName_reset() {
    constantPoolName_computed = false;
    
    constantPoolName_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="constantPoolName") protected boolean constantPoolName_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="constantPoolName") protected String constantPoolName_value;

  /**
   * @attribute syn
   * @aspect ConstantPool
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\ConstantPool.jrag:16
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantPool", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\ConstantPool.jrag:16")
  @SideEffect.Pure(group="constantPoolName") public String constantPoolName() {
    ASTState state = state();
    if (constantPoolName_computed) {
      return constantPoolName_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    constantPoolName_value = constantPoolName_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    constantPoolName_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return constantPoolName_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private String constantPoolName_compute() {
      if(!isNestedType()) {
        String packageName = packageName();
        if(!packageName.equals("")) {
          packageName = packageName.replace('.', '/') + "/";
        }
        return packageName + getID();
      }
      else {
        String prefix = enclosingType().constantPoolName();
        if(isAnonymous()) {
          return prefix + "$" + uniqueIndex();
        }
        else if(isLocalClass()) {
          return prefix + "$" + uniqueIndex() + getID();
        }
        return prefix + "$" + getID();
      }
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void needsSignatureAttribute_reset() {
    needsSignatureAttribute_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="needsSignatureAttribute") protected boolean needsSignatureAttribute_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="needsSignatureAttribute") protected boolean needsSignatureAttribute_value;

  /**
   * @attribute syn
   * @aspect GenericsCodegen
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Backend\\GenericsCodegen.jrag:339
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="GenericsCodegen", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Backend\\GenericsCodegen.jrag:339")
  @SideEffect.Pure(group="needsSignatureAttribute") public boolean needsSignatureAttribute() {
    ASTState state = state();
    if (needsSignatureAttribute_computed) {
      return needsSignatureAttribute_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    needsSignatureAttribute_value = true;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    needsSignatureAttribute_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return needsSignatureAttribute_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void classSignature_reset() {
    classSignature_computed = false;
    
    classSignature_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="classSignature") protected boolean classSignature_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="classSignature") protected String classSignature_value;

  /**
   * @attribute syn
   * @aspect GenericsCodegen
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Backend\\GenericsCodegen.jrag:384
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="GenericsCodegen", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Backend\\GenericsCodegen.jrag:384")
  @SideEffect.Pure(group="classSignature") public String classSignature() {
    ASTState state = state();
    if (classSignature_computed) {
      return classSignature_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    classSignature_value = classSignature_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    classSignature_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return classSignature_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private String classSignature_compute() {
      StringBuffer buf = new StringBuffer();
      // FormalTypeParameters
      buf.append("<");
      for(int i = 0; i < getNumTypeParameter(); i++)
        buf.append(getTypeParameter(i).formalTypeParameter());
      buf.append(">");
      buf.append(super.classSignature());
      return buf.toString();
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void getPlaceholderMethodList_reset() {
    getPlaceholderMethodList_computed = false;
    
    getPlaceholderMethodList_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="getPlaceholderMethodList") protected boolean getPlaceholderMethodList_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="getPlaceholderMethodList") protected List<PlaceholderMethodDecl> getPlaceholderMethodList_value;

  /**
   * The placeholder method list for the constructors of this generic
   * class.
   * 
   * @return list of placeholder methods
   * @attribute syn
   * @aspect TypeInference
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\TypeInference.jrag:124
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="TypeInference", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\TypeInference.jrag:124")
  @SideEffect.Pure(group="getPlaceholderMethodList") public List<PlaceholderMethodDecl> getPlaceholderMethodList() {
    ASTState state = state();
    if (getPlaceholderMethodList_computed) {
      return getPlaceholderMethodList_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    getPlaceholderMethodList_value = getPlaceholderMethodList_compute();
    getPlaceholderMethodList_value.setParent(this);
    getPlaceholderMethodList_value.is$Final = true;
    if (true) {
    getPlaceholderMethodList_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return getPlaceholderMethodList_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private List<PlaceholderMethodDecl> getPlaceholderMethodList_compute() {
  		List<PlaceholderMethodDecl> placeholderMethods =
  			new List<PlaceholderMethodDecl>();
  		List<TypeVariable> typeParams = getTypeParameterList();
  		List<TypeVariable> classTypeVars = new List<TypeVariable>();
  		List<Access> typeArgs = new List<Access>();
  
  		// copy the list of type parameters
  		int arg = 0;
  		for (Iterator iter = typeParams.iterator(); iter.hasNext(); ++arg) {
  			String substName = "#"+arg;
  			typeArgs.add(new TypeAccess(substName));
  
  			TypeVariable typeVar = (TypeVariable) iter.next();
  			List<Access> typeBounds = new List<Access>();
  			for (Access typeBound : typeVar.getTypeBoundList())
  				typeBounds.add((Access) typeBound.cloneSubtree());
  			classTypeVars.add(
  					new TypeVariable(
  						new Modifiers(),
  						substName,
  						new List<BodyDecl>(),
  						typeBounds));
  		}
  
  		ParTypeAccess returnType = new ParTypeAccess(
  				createQualifiedAccess(),
  				typeArgs);
  
  		for (Iterator iter = constructors().iterator(); iter.hasNext(); ) {
  			ConstructorDecl decl = (ConstructorDecl)iter.next();
  			if (decl instanceof ConstructorDeclSubstituted)
  				decl = ((ConstructorDeclSubstituted) decl).getOriginal();
  
  			// filter accessible constructors
  			if (!decl.accessibleFrom(hostType()))
  				continue;
  
  			Collection<TypeVariable> originalTypeVars =
  				new LinkedList<TypeVariable>();
  			List<TypeVariable> typeVars = new List<TypeVariable>();
  			for (TypeVariable typeVar : typeParams)
  				originalTypeVars.add(typeVar);
  			for (TypeVariable typeVar : classTypeVars)
  				typeVars.add((TypeVariable) typeVar.cloneSubtree());
  
  			if (decl instanceof GenericConstructorDecl) {
  				GenericConstructorDecl genericDecl =
  					(GenericConstructorDecl) decl;
  				List<TypeVariable> typeVariables = new List<TypeVariable>();
  				for (int i = 0; i < genericDecl.getNumTypeParameter(); ++i) {
  					String substName = "#" + (arg+i);
  
  					TypeVariable typeVar = genericDecl.getTypeParameter(i);
  					originalTypeVars.add(typeVar);
  					List<Access> typeBounds = new List<Access>();
  					for (Access typeBound : typeVar.getTypeBoundList())
  						typeBounds.add((Access) typeBound.cloneSubtree());
  					typeVars.add(
  							new TypeVariable(
  								new Modifiers(),
  								substName,
  								new List<BodyDecl>(),
  								typeBounds));
  				}
  			}
  
  			List<ParameterDeclaration> substParameters =
  				new List<ParameterDeclaration>();
  			for (ParameterDeclaration param : decl.getParameterList()) {
  				substParameters.add(param.substituted(
  							originalTypeVars, typeVars));
  			}
  
  			List<Access> substExceptions = new List<Access>();
  			for (Access exception : decl.getExceptionList()) {
  				substExceptions.add(exception.substituted(
  							originalTypeVars, typeVars));
  			}
  
  			PlaceholderMethodDecl placeholderMethod =
  				new PlaceholderMethodDecl(
  					(Modifiers) decl.getModifiers().cloneSubtree(),
  					(Access) returnType.cloneSubtree(),
  					"#"+getID(),
  					substParameters,
  					substExceptions,
  					new Opt(new Block()),
  					typeVars);
  
  			placeholderMethods.add(placeholderMethod);
  		}
  		return placeholderMethods;
  	}
  /**
   * @attribute syn
   * @aspect Generics
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:139
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Generics", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:139")
  @SideEffect.Pure(group="isGenericType") public boolean isGenericType() {
    boolean isGenericType_value = true;
    return isGenericType_value;
  }
  /**
   * @attribute inh
   * @aspect GenericsTypeCheck
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:405
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="GenericsTypeCheck", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:405")
  @SideEffect.Pure(group="typeThrowable") public TypeDecl typeThrowable() {
    TypeDecl typeThrowable_value = getParent().Define_typeThrowable(this, null);
    return typeThrowable_value;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:519
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isNestedType(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getTypeParameterListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:451
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return true;
    }
    else {
      return super.Define_isNestedType(_callerNode, _childNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_isNestedType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:497
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_enclosingType(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getTypeParameterListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:452
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return this;
    }
    else {
      return super.Define_enclosingType(_callerNode, _childNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_enclosingType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericMethods.jrag:116
   * @apilevel internal
   */
 @SideEffect.Pure public SimpleSet Define_lookupType(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getBodyDeclListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:501
      int index = _callerNode.getIndexOfChild(_childNode);
      {
          SimpleSet c = memberTypes(name);
          if(getBodyDecl(index).visibleTypeParameters())
            c = addTypeVariables(c, name);
          if(!c.isEmpty())
            return c;
          // 8.5.2
          if(isClassDecl() && isStatic() && !isTopLevelType()) {
            for(Iterator iter = lookupType(name).iterator(); iter.hasNext(); ) {
              TypeDecl d = (TypeDecl)iter.next();
              if(d.isStatic() || (d.enclosingType() != null && instanceOf(d.enclosingType()))) {
                c = c.add(d);
              }
            }
          }
          else
            c = lookupType(name);
          if(!c.isEmpty())
            return c;
          return topLevelType().lookupType(name); // Fix to search imports
          // include type parameters if not static
        }
    }
    else if (_callerNode == getTypeParameterListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:482
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      {
          SimpleSet c = memberTypes(name);
          c = addTypeVariables(c, name);
          if(!c.isEmpty()) return c;
          // 8.5.2
          if(isClassDecl() && isStatic() && !isTopLevelType()) {
            for(Iterator iter = lookupType(name).iterator(); iter.hasNext(); ) {
              TypeDecl d = (TypeDecl)iter.next();
              if(d.isStatic() || (d.enclosingType() != null && instanceOf(d.enclosingType()))) {
                c = c.add(d);
              }
            }
          }
          else
            c = lookupType(name);
          if(!c.isEmpty())
            return c;
          return topLevelType().lookupType(name); // Fix to search imports
        }
    }
    else if (_callerNode == getImplementsListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:477
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      {
          SimpleSet c = addTypeVariables(SimpleSet.emptySet, name);
          return !c.isEmpty() ? c : lookupType(name);
        }
    }
    else if (_callerNode == getSuperClassAccessOptNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:473
      {
          SimpleSet c = addTypeVariables(SimpleSet.emptySet, name);
          return !c.isEmpty() ? c : lookupType(name);
        }
    }
    else {
      return super.Define_lookupType(_callerNode, _childNode, name);
    }
  }
  @SideEffect.Pure protected boolean canDefine_lookupType(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsParTypeDecl.jrag:45
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_genericDecl(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getParTypeDeclListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericsParTypeDecl.jrag:47
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return this;
    }
    else {
      return getParent().Define_genericDecl(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_genericDecl(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\MultiCatch.jrag:49
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_hostType(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getTypeParameterListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\src\\TypeAnalysisGenericsPatch.jrag:13
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return hostType();
    }
    else {
      return super.Define_hostType(_callerNode, _childNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_hostType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Pure public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}

/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package AST;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\java.ast:59
 * @production FloatType : {@link FloatingPointType};

 */
public class FloatType extends FloatingPointType implements Cloneable {
  /**
   * @aspect PrettyPrint
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\PrettyPrint.jadd:849
   */
  public void toString(StringBuffer s) {
		s.append("float");
	}
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Attributes.jrag:65
   */
  public int addConstant(ConstantPool p, Constant c)    { return p.addConstant(c.floatValue()); }
  /**
   * @aspect CodeGeneration
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:546
   */
  public void emitPushConstant(CodeGeneration gen, int value) { FloatingPointLiteral.push(gen, value); }
  /**
   * @aspect CodeGeneration
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:616
   */
  public void emitReturn(CodeGeneration gen)     { gen.emit(Bytecode.FRETURN);}
  /**
   * @aspect CodeGeneration
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:653
   */
  public void emitLoadLocal(CodeGeneration gen, int pos) {
    gen.maxLocals = Math.max(gen.maxLocals, pos+1);
    if(pos == 0) gen.emit(Bytecode.FLOAD_0);
    else if(pos == 1) gen.emit(Bytecode.FLOAD_1);
    else if(pos == 2) gen.emit(Bytecode.FLOAD_2);
    else if(pos == 3) gen.emit(Bytecode.FLOAD_3);
    else if(pos < 256) gen.emit(Bytecode.FLOAD).add(pos);
    else gen.emit(Bytecode.WIDE).emit(Bytecode.FLOAD).add2(pos);
  }
  /**
   * @aspect CodeGeneration
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:766
   */
  public void emitStoreLocal(CodeGeneration gen, int pos) {
    gen.maxLocals = Math.max(gen.maxLocals, pos+1);
    if(pos == 0) gen.emit(Bytecode.FSTORE_0);
    else if(pos == 1) gen.emit(Bytecode.FSTORE_1);
    else if(pos == 2) gen.emit(Bytecode.FSTORE_2);
    else if(pos == 3) gen.emit(Bytecode.FSTORE_3);
    else if(pos < 256) gen.emit(Bytecode.FSTORE).add(pos);
    else gen.emit(Bytecode.WIDE).emit(Bytecode.FSTORE).add2(pos);
  }
  /**
   * @aspect CodeGenerationConversions
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:944
   */
  void emitCastTo(CodeGeneration gen, TypeDecl type)    { type.floatToThis(gen); }
  /**
   * @aspect CodeGenerationConversions
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:956
   */
  void intToThis(CodeGeneration gen)  { gen.emit(Bytecode.I2F); }
  /**
   * @aspect CodeGenerationConversions
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:967
   */
  void floatToThis(CodeGeneration gen)  { }
  /**
   * @aspect CodeGenerationConversions
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:976
   */
  void doubleToThis(CodeGeneration gen)  { gen.emit(Bytecode.D2F); }
  /**
   * @aspect CodeGenerationConversions
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:985
   */
  void longToThis(CodeGeneration gen)  { gen.emit(Bytecode.L2F); }
  /**
   * @aspect CodeGenerationConversions
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:992
   */
  void byteToThis(CodeGeneration gen)    { gen.emit(Bytecode.I2F); }
  /**
   * @aspect CodeGenerationConversions
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:1000
   */
  void charToThis(CodeGeneration gen)    { gen.emit(Bytecode.I2F); }
  /**
   * @aspect CodeGenerationConversions
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:1008
   */
  void shortToThis(CodeGeneration gen)    { gen.emit(Bytecode.I2F); }
  /**
   * @aspect CodeGenerationBinaryOperations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:1040
   */
  void neg(CodeGeneration gen)    { gen.emit(Bytecode.FNEG); }
  /**
   * @aspect CodeGenerationBinaryOperations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:1052
   */
  void add(CodeGeneration gen) {gen.emit(Bytecode.FADD);}
  /**
   * @aspect CodeGenerationBinaryOperations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:1058
   */
  void sub(CodeGeneration gen) {gen.emit(Bytecode.FSUB);}
  /**
   * @aspect CodeGenerationBinaryOperations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:1064
   */
  void mul(CodeGeneration gen) {gen.emit(Bytecode.FMUL);}
  /**
   * @aspect CodeGenerationBinaryOperations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:1070
   */
  void div(CodeGeneration gen) {gen.emit(Bytecode.FDIV);}
  /**
   * @aspect CodeGenerationBinaryOperations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:1076
   */
  void rem(CodeGeneration gen) {gen.emit(Bytecode.FREM);}
  /**
   * @aspect CodeGenerationBranch
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:1111
   */
  public void branchLT(CodeGeneration gen, int label)    { gen.emit(Bytecode.FCMPG).emitCompare(Bytecode.IFLT, label); }
  /**
   * @aspect CodeGenerationBranch
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:1117
   */
  public void branchLE(CodeGeneration gen, int label)    { gen.emit(Bytecode.FCMPG).emitCompare(Bytecode.IFLE, label); }
  /**
   * @aspect CodeGenerationBranch
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:1123
   */
  public void branchGE(CodeGeneration gen, int label)    { gen.emit(Bytecode.FCMPL).emitCompare(Bytecode.IFGE, label); }
  /**
   * @aspect CodeGenerationBranch
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:1129
   */
  public void branchGT(CodeGeneration gen, int label)    { gen.emit(Bytecode.FCMPL).emitCompare(Bytecode.IFGT, label); }
  /**
   * @aspect CodeGenerationBranch
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:1135
   */
  public void branchEQ(CodeGeneration gen, int label)     { gen.emit(Bytecode.FCMPL).emitCompare(Bytecode.IFEQ, label); }
  /**
   * @aspect CodeGenerationBranch
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:1144
   */
  public void branchNE(CodeGeneration gen, int label)     { gen.emit(Bytecode.FCMPL).emitCompare(Bytecode.IFNE, label); }
  /**
   * @aspect AnnotationsCodegen
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Backend\\AnnotationsCodegen.jrag:186
   */
  public int addAnnotConstant(ConstantPool p, Constant c) {
    return addConstant(p, c);
  }
  /**
   * @declaredat ASTNode:1
   */
  public FloatType() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[3];
    setChild(new Opt(), 1);
    setChild(new List(), 2);
  }
  /**
   * @declaredat ASTNode:15
   */
  public FloatType(Modifiers p0, String p1, Opt<Access> p2, List<BodyDecl> p3) {
    setChild(p0, 0);
    setID(p1);
    setChild(p2, 1);
    setChild(p3, 2);
  }
  /**
   * @declaredat ASTNode:21
   */
  public FloatType(Modifiers p0, beaver.Symbol p1, Opt<Access> p2, List<BodyDecl> p3) {
    setChild(p0, 0);
    setID(p1);
    setChild(p2, 1);
    setChild(p3, 2);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:28
   */
  @SideEffect.Pure protected int numChildren() {
    return 3;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:34
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    typeDescriptor_reset();
    jvmName_reset();
    boxed_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:49
   */
  @SideEffect.Fresh public FloatType clone() throws CloneNotSupportedException {
    FloatType node = (FloatType) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:54
   */
  @SideEffect.Fresh(group="_ASTNode") public FloatType copy() {
    try {
      FloatType node = (FloatType) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:73
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public FloatType fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:83
   */
  @SideEffect.Fresh(group="_ASTNode") public FloatType treeCopyNoTransform() {
    FloatType tree = (FloatType) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:103
   */
  @SideEffect.Fresh(group="_ASTNode") public FloatType treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:108
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_ID == ((FloatType) node).tokenString_ID);    
  }
  /**
   * Replaces the Modifiers child.
   * @param node The new node to replace the Modifiers child.
   * @apilevel high-level
   */
  public void setModifiers(Modifiers node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Modifiers child.
   * @return The current node used as the Modifiers child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Modifiers")
  @SideEffect.Pure public Modifiers getModifiers() {
    return (Modifiers) getChild(0);
  }
  /**
   * Retrieves the Modifiers child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Modifiers child.
   * @apilevel low-level
   */
  public Modifiers getModifiersNoTransform() {
    return (Modifiers) getChildNoTransform(0);
  }
  /**
   * Replaces the lexeme ID.
   * @param value The new value for the lexeme ID.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setID(String value) {
    tokenString_ID = value;
  }
  /**
   * JastAdd-internal setter for lexeme ID using the Beaver parser.
   * @param symbol Symbol containing the new value for the lexeme ID
   * @apilevel internal
   */
  public void setID(beaver.Symbol symbol) {
    if (symbol.value != null && !(symbol.value instanceof String))
    throw new UnsupportedOperationException("setID is only valid for String lexemes");
    tokenString_ID = (String)symbol.value;
    IDstart = symbol.getStart();
    IDend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme ID.
   * @return The value for the lexeme ID.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="ID")
  @SideEffect.Pure(group="_ASTNode") public String getID() {
    return tokenString_ID != null ? tokenString_ID : "";
  }
  /**
   * Replaces the optional node for the SuperClassAccess child. This is the <code>Opt</code>
   * node containing the child SuperClassAccess, not the actual child!
   * @param opt The new node to be used as the optional node for the SuperClassAccess child.
   * @apilevel low-level
   */
  public void setSuperClassAccessOpt(Opt<Access> opt) {
    setChild(opt, 1);
  }
  /**
   * Replaces the (optional) SuperClassAccess child.
   * @param node The new node to be used as the SuperClassAccess child.
   * @apilevel high-level
   */
  public void setSuperClassAccess(Access node) {
    getSuperClassAccessOpt().setChild(node, 0);
  }
  /**
   * Check whether the optional SuperClassAccess child exists.
   * @return {@code true} if the optional SuperClassAccess child exists, {@code false} if it does not.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasSuperClassAccess() {
    return getSuperClassAccessOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) SuperClassAccess child.
   * @return The SuperClassAccess child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   */
  @SideEffect.Pure public Access getSuperClassAccess() {
    return (Access) getSuperClassAccessOpt().getChild(0);
  }
  /**
   * Retrieves the optional node for the SuperClassAccess child. This is the <code>Opt</code> node containing the child SuperClassAccess, not the actual child!
   * @return The optional node for child the SuperClassAccess child.
   * @apilevel low-level
   */
  @ASTNodeAnnotation.OptChild(name="SuperClassAccess")
  @SideEffect.Pure public Opt<Access> getSuperClassAccessOpt() {
    return (Opt<Access>) getChild(1);
  }
  /**
   * Retrieves the optional node for child SuperClassAccess. This is the <code>Opt</code> node containing the child SuperClassAccess, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child SuperClassAccess.
   * @apilevel low-level
   */
  @SideEffect.Pure public Opt<Access> getSuperClassAccessOptNoTransform() {
    return (Opt<Access>) getChildNoTransform(1);
  }
  /**
   * Replaces the BodyDecl list.
   * @param list The new list node to be used as the BodyDecl list.
   * @apilevel high-level
   */
  public void setBodyDeclList(List<BodyDecl> list) {
    setChild(list, 2);
  }
  /**
   * Retrieves the number of children in the BodyDecl list.
   * @return Number of children in the BodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumBodyDecl() {
    return getBodyDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the BodyDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the BodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumBodyDeclNoTransform() {
    return getBodyDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the BodyDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the BodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public BodyDecl getBodyDecl(int i) {
    return (BodyDecl) getBodyDeclList().getChild(i);
  }
  /**
   * Check whether the BodyDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasBodyDecl() {
    return getBodyDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the BodyDecl list.
   * @param node The element to append to the BodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addBodyDecl(BodyDecl node) {
    List<BodyDecl> list = (parent == null) ? getBodyDeclListNoTransform() : getBodyDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addBodyDeclNoTransform(BodyDecl node) {
    List<BodyDecl> list = getBodyDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the BodyDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setBodyDecl(BodyDecl node, int i) {
    List<BodyDecl> list = getBodyDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the BodyDecl list.
   * @return The node representing the BodyDecl list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="BodyDecl")
  @SideEffect.Pure(group="_ASTNode") public List<BodyDecl> getBodyDeclList() {
    List<BodyDecl> list = (List<BodyDecl>) getChild(2);
    return list;
  }
  /**
   * Retrieves the BodyDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the BodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<BodyDecl> getBodyDeclListNoTransform() {
    return (List<BodyDecl>) getChildNoTransform(2);
  }
  /**
   * @return the element at index {@code i} in the BodyDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public BodyDecl getBodyDeclNoTransform(int i) {
    return (BodyDecl) getBodyDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the BodyDecl list.
   * @return The node representing the BodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<BodyDecl> getBodyDecls() {
    return getBodyDeclList();
  }
  /**
   * Retrieves the BodyDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the BodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<BodyDecl> getBodyDeclsNoTransform() {
    return getBodyDeclListNoTransform();
  }
  /**
   * @attribute syn
   * @aspect ConstantExpression
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:160
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantExpression", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:160")
  @SideEffect.Pure(group="cast_Constant") public Constant cast(Constant c) {
    Constant cast_Constant_value = Constant.create(c.floatValue());
    return cast_Constant_value;
  }
  /**
   * @attribute syn
   * @aspect ConstantExpression
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:174
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantExpression", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:174")
  @SideEffect.Pure(group="plus_Constant") public Constant plus(Constant c) {
    Constant plus_Constant_value = c;
    return plus_Constant_value;
  }
  /**
   * @attribute syn
   * @aspect ConstantExpression
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:183
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantExpression", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:183")
  @SideEffect.Pure(group="minus_Constant") public Constant minus(Constant c) {
    Constant minus_Constant_value = Constant.create(-c.floatValue());
    return minus_Constant_value;
  }
  /**
   * @attribute syn
   * @aspect ConstantExpression
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:199
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantExpression", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:199")
  @SideEffect.Pure(group="mul_Constant_Constant") public Constant mul(Constant c1, Constant c2) {
    Constant mul_Constant_Constant_value = Constant.create(c1.floatValue() * c2.floatValue());
    return mul_Constant_Constant_value;
  }
  /**
   * @attribute syn
   * @aspect ConstantExpression
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:208
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantExpression", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:208")
  @SideEffect.Pure(group="div_Constant_Constant") public Constant div(Constant c1, Constant c2) {
    Constant div_Constant_Constant_value = Constant.create(c1.floatValue() / c2.floatValue());
    return div_Constant_Constant_value;
  }
  /**
   * @attribute syn
   * @aspect ConstantExpression
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:217
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantExpression", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:217")
  @SideEffect.Pure(group="mod_Constant_Constant") public Constant mod(Constant c1, Constant c2) {
    Constant mod_Constant_Constant_value = Constant.create(c1.floatValue() % c2.floatValue());
    return mod_Constant_Constant_value;
  }
  /**
   * @attribute syn
   * @aspect ConstantExpression
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:226
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantExpression", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:226")
  @SideEffect.Pure(group="add_Constant_Constant") public Constant add(Constant c1, Constant c2) {
    Constant add_Constant_Constant_value = Constant.create(c1.floatValue() + c2.floatValue());
    return add_Constant_Constant_value;
  }
  /**
   * @attribute syn
   * @aspect ConstantExpression
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:236
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantExpression", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:236")
  @SideEffect.Pure(group="sub_Constant_Constant") public Constant sub(Constant c1, Constant c2) {
    Constant sub_Constant_Constant_value = Constant.create(c1.floatValue() - c2.floatValue());
    return sub_Constant_Constant_value;
  }
  /**
   * @attribute syn
   * @aspect ConstantExpression
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:290
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantExpression", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:290")
  @SideEffect.Pure(group="questionColon_Constant_Constant_Constant") public Constant questionColon(Constant cond, Constant c1, Constant c2) {
    Constant questionColon_Constant_Constant_Constant_value = Constant.create(cond.booleanValue() ? c1.floatValue() : c2.floatValue());
    return questionColon_Constant_Constant_Constant_value;
  }
  /**
   * @attribute syn
   * @aspect ConstantExpression
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:394
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantExpression", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:394")
  @SideEffect.Pure(group="eqIsTrue_Expr_Expr") public boolean eqIsTrue(Expr left, Expr right) {
    boolean eqIsTrue_Expr_Expr_value = left.constant().floatValue() == right.constant().floatValue();
    return eqIsTrue_Expr_Expr_value;
  }
  /**
   * @attribute syn
   * @aspect ConstantExpression
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:405
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantExpression", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:405")
  @SideEffect.Pure(group="ltIsTrue_Expr_Expr") public boolean ltIsTrue(Expr left, Expr right) {
    boolean ltIsTrue_Expr_Expr_value = left.constant().floatValue() < right.constant().floatValue();
    return ltIsTrue_Expr_Expr_value;
  }
  /**
   * @attribute syn
   * @aspect ConstantExpression
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:411
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantExpression", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:411")
  @SideEffect.Pure(group="leIsTrue_Expr_Expr") public boolean leIsTrue(Expr left, Expr right) {
    boolean leIsTrue_Expr_Expr_value = left.constant().floatValue() <= right.constant().floatValue();
    return leIsTrue_Expr_Expr_value;
  }
  /**
   * @attribute syn
   * @aspect TypeAnalysis
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:195
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:195")
  @SideEffect.Pure(group="isFloat") public boolean isFloat() {
    boolean isFloat_value = true;
    return isFloat_value;
  }
  /**
   * @attribute syn
   * @aspect CodeGeneration
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:621
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CodeGeneration", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:621")
  @SideEffect.Pure(group="arrayLoad") public byte arrayLoad() {
    byte arrayLoad_value = Bytecode.FALOAD;
    return arrayLoad_value;
  }
  /**
   * @attribute syn
   * @aspect CodeGeneration
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:723
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CodeGeneration", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:723")
  @SideEffect.Pure(group="arrayStore") public byte arrayStore() {
    byte arrayStore_value = Bytecode.FASTORE;
    return arrayStore_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void typeDescriptor_reset() {
    typeDescriptor_computed = false;
    
    typeDescriptor_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="typeDescriptor") protected boolean typeDescriptor_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="typeDescriptor") protected String typeDescriptor_value;

  /**
   * @attribute syn
   * @aspect ConstantPoolNames
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\ConstantPoolNames.jrag:12
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantPoolNames", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\ConstantPoolNames.jrag:12")
  @SideEffect.Pure(group="typeDescriptor") public String typeDescriptor() {
    ASTState state = state();
    if (typeDescriptor_computed) {
      return typeDescriptor_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    typeDescriptor_value = "F";
    if (isFinal && _boundaries == state().boundariesCrossed) {
    typeDescriptor_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return typeDescriptor_value;
  }
  /**
   * @attribute syn
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:823
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CreateBCode", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:823")
  @SideEffect.Pure(group="arrayPrimitiveTypeDescriptor") public int arrayPrimitiveTypeDescriptor() {
    int arrayPrimitiveTypeDescriptor_value = 6;
    return arrayPrimitiveTypeDescriptor_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void jvmName_reset() {
    jvmName_computed = false;
    
    jvmName_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="jvmName") protected boolean jvmName_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="jvmName") protected String jvmName_value;

  /**
   * @attribute syn
   * @aspect Java2Rewrites
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Java2Rewrites.jrag:15
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Java2Rewrites", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Java2Rewrites.jrag:15")
  @SideEffect.Pure(group="jvmName") public String jvmName() {
    ASTState state = state();
    if (jvmName_computed) {
      return jvmName_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    jvmName_value = "F";
    if (isFinal && _boundaries == state().boundariesCrossed) {
    jvmName_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return jvmName_value;
  }
  /**
   * @attribute syn
   * @aspect Java2Rewrites
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Java2Rewrites.jrag:44
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Java2Rewrites", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Java2Rewrites.jrag:44")
  @SideEffect.Pure(group="primitiveClassName") public String primitiveClassName() {
    String primitiveClassName_value = "Float";
    return primitiveClassName_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void boxed_reset() {
    boxed_computed = false;
    
    boxed_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="boxed") protected boolean boxed_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="boxed") protected TypeDecl boxed_value;

  /**
   * @attribute syn
   * @aspect AutoBoxing
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\AutoBoxing.jrag:35
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AutoBoxing", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\AutoBoxing.jrag:35")
  @SideEffect.Pure(group="boxed") public TypeDecl boxed() {
    ASTState state = state();
    if (boxed_computed) {
      return boxed_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boxed_value = lookupType("java.lang", "Float");
    if (isFinal && _boundaries == state().boundariesCrossed) {
    boxed_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return boxed_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}

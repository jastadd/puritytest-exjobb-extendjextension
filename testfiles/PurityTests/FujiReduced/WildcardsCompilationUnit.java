/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package AST;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.ast:41
 * @production WildcardsCompilationUnit : {@link CompilationUnit};

 */
public class WildcardsCompilationUnit extends CompilationUnit implements Cloneable {
  /**
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1176
   */
  public static LUBType createLUBType(Collection bounds) {
    List boundList = new List();
    StringBuffer name = new StringBuffer();
    for(Iterator iter = bounds.iterator(); iter.hasNext(); ) {
      TypeDecl typeDecl = (TypeDecl)iter.next();
      boundList.add(typeDecl.createBoundAccess());
      name.append("& " + typeDecl.typeName());
    }
    LUBType decl = new LUBType(
      new Modifiers(new List().add(new Modifier("public"))),
      name.toString(),
      new List(),
      boundList
    );
    return decl;
  }
  /**
   * @declaredat ASTNode:1
   */
  public WildcardsCompilationUnit() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
    setChild(new List(), 0);
    setChild(new List(), 1);
  }
  /**
   * @declaredat ASTNode:15
   */
  public WildcardsCompilationUnit(java.lang.String p0, List<ImportDecl> p1, List<TypeDecl> p2) {
    setPackageDecl(p0);
    setChild(p1, 0);
    setChild(p2, 1);
  }
  /**
   * @declaredat ASTNode:20
   */
  public WildcardsCompilationUnit(beaver.Symbol p0, List<ImportDecl> p1, List<TypeDecl> p2) {
    setPackageDecl(p0);
    setChild(p1, 0);
    setChild(p2, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:26
   */
  @SideEffect.Pure protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:32
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:36
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    typeWildcard_reset();
    lookupWildcardExtends_TypeDecl_reset();
    lookupWildcardSuper_TypeDecl_reset();
    lookupLUBType_Collection_reset();
    lookupGLBType_ArrayList_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:49
   */
  @SideEffect.Fresh public WildcardsCompilationUnit clone() throws CloneNotSupportedException {
    WildcardsCompilationUnit node = (WildcardsCompilationUnit) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:54
   */
  @SideEffect.Fresh(group="_ASTNode") public WildcardsCompilationUnit copy() {
    try {
      WildcardsCompilationUnit node = (WildcardsCompilationUnit) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:73
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public WildcardsCompilationUnit fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:83
   */
  @SideEffect.Fresh(group="_ASTNode") public WildcardsCompilationUnit treeCopyNoTransform() {
    WildcardsCompilationUnit tree = (WildcardsCompilationUnit) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:103
   */
  @SideEffect.Fresh(group="_ASTNode") public WildcardsCompilationUnit treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:108
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenjava_lang_String_PackageDecl == ((WildcardsCompilationUnit) node).tokenjava_lang_String_PackageDecl);    
  }
  /**
   * Replaces the lexeme PackageDecl.
   * @param value The new value for the lexeme PackageDecl.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setPackageDecl(java.lang.String value) {
    tokenjava_lang_String_PackageDecl = value;
  }
  /**
   * JastAdd-internal setter for lexeme PackageDecl using the Beaver parser.
   * @param symbol Symbol containing the new value for the lexeme PackageDecl
   * @apilevel internal
   */
  public void setPackageDecl(beaver.Symbol symbol) {
    if (symbol.value != null && !(symbol.value instanceof String))
    throw new UnsupportedOperationException("setPackageDecl is only valid for String lexemes");
    tokenjava_lang_String_PackageDecl = (String)symbol.value;
    PackageDeclstart = symbol.getStart();
    PackageDeclend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme PackageDecl.
   * @return The value for the lexeme PackageDecl.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="PackageDecl")
  @SideEffect.Pure(group="_ASTNode") public java.lang.String getPackageDecl() {
    return tokenjava_lang_String_PackageDecl != null ? tokenjava_lang_String_PackageDecl : "";
  }
  /**
   * Replaces the ImportDecl list.
   * @param list The new list node to be used as the ImportDecl list.
   * @apilevel high-level
   */
  public void setImportDeclList(List<ImportDecl> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the ImportDecl list.
   * @return Number of children in the ImportDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumImportDecl() {
    return getImportDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the ImportDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the ImportDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumImportDeclNoTransform() {
    return getImportDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the ImportDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the ImportDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public ImportDecl getImportDecl(int i) {
    return (ImportDecl) getImportDeclList().getChild(i);
  }
  /**
   * Check whether the ImportDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasImportDecl() {
    return getImportDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the ImportDecl list.
   * @param node The element to append to the ImportDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addImportDecl(ImportDecl node) {
    List<ImportDecl> list = (parent == null) ? getImportDeclListNoTransform() : getImportDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addImportDeclNoTransform(ImportDecl node) {
    List<ImportDecl> list = getImportDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the ImportDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setImportDecl(ImportDecl node, int i) {
    List<ImportDecl> list = getImportDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the ImportDecl list.
   * @return The node representing the ImportDecl list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="ImportDecl")
  @SideEffect.Pure(group="_ASTNode") public List<ImportDecl> getImportDeclList() {
    List<ImportDecl> list = (List<ImportDecl>) getChild(0);
    return list;
  }
  /**
   * Retrieves the ImportDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the ImportDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<ImportDecl> getImportDeclListNoTransform() {
    return (List<ImportDecl>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the ImportDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public ImportDecl getImportDeclNoTransform(int i) {
    return (ImportDecl) getImportDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the ImportDecl list.
   * @return The node representing the ImportDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<ImportDecl> getImportDecls() {
    return getImportDeclList();
  }
  /**
   * Retrieves the ImportDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the ImportDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<ImportDecl> getImportDeclsNoTransform() {
    return getImportDeclListNoTransform();
  }
  /**
   * Replaces the TypeDecl list.
   * @param list The new list node to be used as the TypeDecl list.
   * @apilevel high-level
   */
  public void setTypeDeclList(List<TypeDecl> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the TypeDecl list.
   * @return Number of children in the TypeDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumTypeDecl() {
    return getTypeDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the TypeDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the TypeDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumTypeDeclNoTransform() {
    return getTypeDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the TypeDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the TypeDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public TypeDecl getTypeDecl(int i) {
    return (TypeDecl) getTypeDeclList().getChild(i);
  }
  /**
   * Check whether the TypeDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasTypeDecl() {
    return getTypeDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the TypeDecl list.
   * @param node The element to append to the TypeDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addTypeDecl(TypeDecl node) {
    List<TypeDecl> list = (parent == null) ? getTypeDeclListNoTransform() : getTypeDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addTypeDeclNoTransform(TypeDecl node) {
    List<TypeDecl> list = getTypeDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the TypeDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setTypeDecl(TypeDecl node, int i) {
    List<TypeDecl> list = getTypeDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the TypeDecl list.
   * @return The node representing the TypeDecl list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="TypeDecl")
  @SideEffect.Pure(group="_ASTNode") public List<TypeDecl> getTypeDeclList() {
    List<TypeDecl> list = (List<TypeDecl>) getChild(1);
    return list;
  }
  /**
   * Retrieves the TypeDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the TypeDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<TypeDecl> getTypeDeclListNoTransform() {
    return (List<TypeDecl>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the TypeDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public TypeDecl getTypeDeclNoTransform(int i) {
    return (TypeDecl) getTypeDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the TypeDecl list.
   * @return The node representing the TypeDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<TypeDecl> getTypeDecls() {
    return getTypeDeclList();
  }
  /**
   * Retrieves the TypeDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the TypeDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<TypeDecl> getTypeDeclsNoTransform() {
    return getTypeDeclListNoTransform();
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void typeWildcard_reset() {
    typeWildcard_computed = false;
    
    typeWildcard_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="typeWildcard") protected boolean typeWildcard_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="typeWildcard") protected TypeDecl typeWildcard_value;

  /**
   * @attribute syn
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1138
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1138")
  @SideEffect.Pure(group="typeWildcard") public TypeDecl typeWildcard() {
    ASTState state = state();
    if (typeWildcard_computed) {
      return typeWildcard_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    typeWildcard_value = typeWildcard_compute();
    typeWildcard_value.setParent(this);
    typeWildcard_value.is$Final = true;
    if (true) {
    typeWildcard_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return typeWildcard_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private TypeDecl typeWildcard_compute() {
      TypeDecl decl = new WildcardType(
        new Modifiers(new List().add(new Modifier("public"))),
        "?",
        new List()
      );
      return decl;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void lookupWildcardExtends_TypeDecl_reset() {
    lookupWildcardExtends_TypeDecl_values = null;
    lookupWildcardExtends_TypeDecl_proxy = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="lookupWildcardExtends_TypeDecl") protected ASTNode lookupWildcardExtends_TypeDecl_proxy;
  /** @apilevel internal */
   @SideEffect.Secret(group="lookupWildcardExtends_TypeDecl") protected java.util.Map lookupWildcardExtends_TypeDecl_values;

  /**
   * @attribute syn
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1149
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1149")
  @SideEffect.Pure(group="lookupWildcardExtends_TypeDecl") public TypeDecl lookupWildcardExtends(TypeDecl bound) {
    Object _parameters = bound;
    if (lookupWildcardExtends_TypeDecl_values == null) lookupWildcardExtends_TypeDecl_values = new java.util.HashMap(4);
    ASTState state = state();
    if (lookupWildcardExtends_TypeDecl_values.containsKey(_parameters)) {
      return (TypeDecl) lookupWildcardExtends_TypeDecl_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    TypeDecl lookupWildcardExtends_TypeDecl_value = lookupWildcardExtends_compute(bound);
    if (lookupWildcardExtends_TypeDecl_proxy == null) {
      lookupWildcardExtends_TypeDecl_proxy = new ASTNode();
      lookupWildcardExtends_TypeDecl_proxy.is$Final = true;
      lookupWildcardExtends_TypeDecl_proxy.setParent(this);
    }
    if (lookupWildcardExtends_TypeDecl_value != null) {
      lookupWildcardExtends_TypeDecl_proxy.addChild(lookupWildcardExtends_TypeDecl_value);
      // Proxy child access is used to trigger rewrite of NTA value.
      lookupWildcardExtends_TypeDecl_value = (TypeDecl) lookupWildcardExtends_TypeDecl_proxy.getChild(lookupWildcardExtends_TypeDecl_proxy.numChildren - 1);
      lookupWildcardExtends_TypeDecl_value.is$Final = true;
    }
    if (true) {
    lookupWildcardExtends_TypeDecl_values.put(_parameters, lookupWildcardExtends_TypeDecl_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return lookupWildcardExtends_TypeDecl_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private TypeDecl lookupWildcardExtends_compute(TypeDecl bound) {
      TypeDecl decl = new WildcardExtendsType(
        new Modifiers(new List().add(new Modifier("public"))),
        "? extends " + bound.fullName(),
        new List(),
        bound.createBoundAccess()
      );
      return decl;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void lookupWildcardSuper_TypeDecl_reset() {
    lookupWildcardSuper_TypeDecl_values = null;
    lookupWildcardSuper_TypeDecl_proxy = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="lookupWildcardSuper_TypeDecl") protected ASTNode lookupWildcardSuper_TypeDecl_proxy;
  /** @apilevel internal */
   @SideEffect.Secret(group="lookupWildcardSuper_TypeDecl") protected java.util.Map lookupWildcardSuper_TypeDecl_values;

  /**
   * @attribute syn
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1162
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1162")
  @SideEffect.Pure(group="lookupWildcardSuper_TypeDecl") public TypeDecl lookupWildcardSuper(TypeDecl bound) {
    Object _parameters = bound;
    if (lookupWildcardSuper_TypeDecl_values == null) lookupWildcardSuper_TypeDecl_values = new java.util.HashMap(4);
    ASTState state = state();
    if (lookupWildcardSuper_TypeDecl_values.containsKey(_parameters)) {
      return (TypeDecl) lookupWildcardSuper_TypeDecl_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    TypeDecl lookupWildcardSuper_TypeDecl_value = lookupWildcardSuper_compute(bound);
    if (lookupWildcardSuper_TypeDecl_proxy == null) {
      lookupWildcardSuper_TypeDecl_proxy = new ASTNode();
      lookupWildcardSuper_TypeDecl_proxy.is$Final = true;
      lookupWildcardSuper_TypeDecl_proxy.setParent(this);
    }
    if (lookupWildcardSuper_TypeDecl_value != null) {
      lookupWildcardSuper_TypeDecl_proxy.addChild(lookupWildcardSuper_TypeDecl_value);
      // Proxy child access is used to trigger rewrite of NTA value.
      lookupWildcardSuper_TypeDecl_value = (TypeDecl) lookupWildcardSuper_TypeDecl_proxy.getChild(lookupWildcardSuper_TypeDecl_proxy.numChildren - 1);
      lookupWildcardSuper_TypeDecl_value.is$Final = true;
    }
    if (true) {
    lookupWildcardSuper_TypeDecl_values.put(_parameters, lookupWildcardSuper_TypeDecl_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return lookupWildcardSuper_TypeDecl_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private TypeDecl lookupWildcardSuper_compute(TypeDecl bound) {
      TypeDecl decl = new WildcardSuperType(
        new Modifiers(new List().add(new Modifier("public"))),
        "? super " + bound.fullName(),
        new List(),
        bound.createBoundAccess()
      );
      return decl;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void lookupLUBType_Collection_reset() {
    lookupLUBType_Collection_values = null;
    lookupLUBType_Collection_proxy = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="lookupLUBType_Collection") protected ASTNode lookupLUBType_Collection_proxy;
  /** @apilevel internal */
   @SideEffect.Secret(group="lookupLUBType_Collection") protected java.util.Map lookupLUBType_Collection_values;

  /**
   * @attribute syn
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1175
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1175")
  @SideEffect.Pure(group="lookupLUBType_Collection") public LUBType lookupLUBType(Collection bounds) {
    Object _parameters = bounds;
    if (lookupLUBType_Collection_values == null) lookupLUBType_Collection_values = new java.util.HashMap(4);
    ASTState state = state();
    if (lookupLUBType_Collection_values.containsKey(_parameters)) {
      return (LUBType) lookupLUBType_Collection_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    LUBType lookupLUBType_Collection_value = createLUBType(bounds);
    if (lookupLUBType_Collection_proxy == null) {
      lookupLUBType_Collection_proxy = new ASTNode();
      lookupLUBType_Collection_proxy.is$Final = true;
      lookupLUBType_Collection_proxy.setParent(this);
    }
    if (lookupLUBType_Collection_value != null) {
      lookupLUBType_Collection_proxy.addChild(lookupLUBType_Collection_value);
      // Proxy child access is used to trigger rewrite of NTA value.
      lookupLUBType_Collection_value = (LUBType) lookupLUBType_Collection_proxy.getChild(lookupLUBType_Collection_proxy.numChildren - 1);
      lookupLUBType_Collection_value.is$Final = true;
    }
    if (true) {
    lookupLUBType_Collection_values.put(_parameters, lookupLUBType_Collection_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return lookupLUBType_Collection_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void lookupGLBType_ArrayList_reset() {
    lookupGLBType_ArrayList_values = null;
    lookupGLBType_ArrayList_proxy = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="lookupGLBType_ArrayList") protected ASTNode lookupGLBType_ArrayList_proxy;
  /** @apilevel internal */
   @SideEffect.Secret(group="lookupGLBType_ArrayList") protected java.util.Map lookupGLBType_ArrayList_values;

  /**
   * @attribute syn
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1214
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="LookupParTypeDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1214")
  @SideEffect.Pure(group="lookupGLBType_ArrayList") public GLBType lookupGLBType(ArrayList bounds) {
    Object _parameters = bounds;
    if (lookupGLBType_ArrayList_values == null) lookupGLBType_ArrayList_values = new java.util.HashMap(4);
    ASTState state = state();
    if (lookupGLBType_ArrayList_values.containsKey(_parameters)) {
      return (GLBType) lookupGLBType_ArrayList_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    GLBType lookupGLBType_ArrayList_value = lookupGLBType_compute(bounds);
    if (lookupGLBType_ArrayList_proxy == null) {
      lookupGLBType_ArrayList_proxy = new ASTNode();
      lookupGLBType_ArrayList_proxy.is$Final = true;
      lookupGLBType_ArrayList_proxy.setParent(this);
    }
    if (lookupGLBType_ArrayList_value != null) {
      lookupGLBType_ArrayList_proxy.addChild(lookupGLBType_ArrayList_value);
      // Proxy child access is used to trigger rewrite of NTA value.
      lookupGLBType_ArrayList_value = (GLBType) lookupGLBType_ArrayList_proxy.getChild(lookupGLBType_ArrayList_proxy.numChildren - 1);
      lookupGLBType_ArrayList_value.is$Final = true;
    }
    if (true) {
    lookupGLBType_ArrayList_values.put(_parameters, lookupGLBType_ArrayList_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return lookupGLBType_ArrayList_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private GLBType lookupGLBType_compute(ArrayList bounds) {
      List boundList = new List();
      StringBuffer name = new StringBuffer();
      for(Iterator iter = bounds.iterator(); iter.hasNext(); ) {
        TypeDecl typeDecl = (TypeDecl)iter.next();
        boundList.add(typeDecl.createBoundAccess());
        name.append("& " + typeDecl.typeName());
      }
      GLBType decl = new GLBType(
        new Modifiers(new List().add(new Modifier("public"))),
        name.toString(),
        new List(),
        boundList
      );
      return decl;
    }
  /** @apilevel internal */
  @SideEffect.Pure public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}

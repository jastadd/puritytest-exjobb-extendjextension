/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package AST;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\java.ast:193
 * @production BranchTargetStmt : {@link Stmt};

 */
public abstract class BranchTargetStmt extends Stmt implements Cloneable, BranchPropagation {
  /**
   * @aspect BranchTarget
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\BranchTarget.jrag:58
   */
  public void collectBranches(Collection c) {
    c.addAll(escapedBranches());
  }
  /**
   * @aspect BranchTarget
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\BranchTarget.jrag:157
   */
  public Stmt branchTarget(Stmt branchStmt) {
    if(targetBranches().contains(branchStmt))
      return this;
    return super.branchTarget(branchStmt);
  }
  /**
   * @aspect BranchTarget
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\BranchTarget.jrag:195
   */
  public void collectFinally(Stmt branchStmt, ArrayList list) {
    if(targetBranches().contains(branchStmt))
      return;
    super.collectFinally(branchStmt, list);
  }
  /**
   * @declaredat ASTNode:1
   */
  public BranchTargetStmt() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:13
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:19
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:23
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    reachableBreak_reset();
    reachableContinue_reset();
    targetBranches_reset();
    escapedBranches_reset();
    branches_reset();
    targetContinues_reset();
    targetBreaks_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:34
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  @SideEffect.Fresh public BranchTargetStmt clone() throws CloneNotSupportedException {
    BranchTargetStmt node = (BranchTargetStmt) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:49
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public abstract BranchTargetStmt fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:57
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract BranchTargetStmt treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:65
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract BranchTargetStmt treeCopy();
  /**
   * @attribute syn
   * @aspect BranchTarget
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\BranchTarget.jrag:40
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="BranchTarget", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\BranchTarget.jrag:40")
  @SideEffect.Pure(group="targetOf_ContinueStmt") public abstract boolean targetOf(ContinueStmt stmt);
  /**
   * @attribute syn
   * @aspect BranchTarget
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\BranchTarget.jrag:41
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="BranchTarget", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\BranchTarget.jrag:41")
  @SideEffect.Pure(group="targetOf_BreakStmt") public abstract boolean targetOf(BreakStmt stmt);
  /** @apilevel internal */
  @SideEffect.Ignore private void reachableBreak_reset() {
    reachableBreak_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="reachableBreak") protected boolean reachableBreak_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="reachableBreak") protected boolean reachableBreak_value;

  /**
   * @attribute syn
   * @aspect UnreachableStatements
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\UnreachableStatements.jrag:49
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="UnreachableStatements", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\UnreachableStatements.jrag:49")
  @SideEffect.Pure(group="reachableBreak") public boolean reachableBreak() {
    ASTState state = state();
    if (reachableBreak_computed) {
      return reachableBreak_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    reachableBreak_value = reachableBreak_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    reachableBreak_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return reachableBreak_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean reachableBreak_compute() {
      for(Iterator iter = targetBreaks().iterator(); iter.hasNext(); ) {
        BreakStmt stmt = (BreakStmt)iter.next();
        if(stmt.reachable())
          return true;
      }
      return false;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void reachableContinue_reset() {
    reachableContinue_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="reachableContinue") protected boolean reachableContinue_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="reachableContinue") protected boolean reachableContinue_value;

  /**
   * @attribute syn
   * @aspect UnreachableStatements
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\UnreachableStatements.jrag:91
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="UnreachableStatements", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\UnreachableStatements.jrag:91")
  @SideEffect.Pure(group="reachableContinue") public boolean reachableContinue() {
    ASTState state = state();
    if (reachableContinue_computed) {
      return reachableContinue_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    reachableContinue_value = reachableContinue_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    reachableContinue_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return reachableContinue_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean reachableContinue_compute() {
      for(Iterator iter = targetContinues().iterator(); iter.hasNext(); ) {
        Stmt stmt = (Stmt)iter.next();
        if(stmt.reachable())
          return true;
      }
      return false;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void targetBranches_reset() {
    targetBranches_computed = false;
    
    targetBranches_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="targetBranches") protected boolean targetBranches_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="targetBranches") protected Collection targetBranches_value;

  /**
   * @attribute syn
   * @aspect BranchTarget
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\BranchTarget.jrag:35
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="BranchTarget", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\BranchTarget.jrag:35")
  @SideEffect.Pure(group="targetBranches") public Collection targetBranches() {
    ASTState state = state();
    if (targetBranches_computed) {
      return targetBranches_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    targetBranches_value = targetBranches_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    targetBranches_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return targetBranches_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Collection targetBranches_compute() {
      HashSet set = new HashSet();
      for(Iterator iter = branches().iterator(); iter.hasNext(); ) {
        Object o = iter.next();
        if(o instanceof ContinueStmt && targetOf((ContinueStmt)o))
          set.add(o);
        if(o instanceof BreakStmt && targetOf((BreakStmt)o))
          set.add(o);
      }
      return set;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void escapedBranches_reset() {
    escapedBranches_computed = false;
    
    escapedBranches_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="escapedBranches") protected boolean escapedBranches_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="escapedBranches") protected Collection escapedBranches_value;

  /**
   * @attribute syn
   * @aspect BranchTarget
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\BranchTarget.jrag:36
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="BranchTarget", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\BranchTarget.jrag:36")
  @SideEffect.Pure(group="escapedBranches") public Collection escapedBranches() {
    ASTState state = state();
    if (escapedBranches_computed) {
      return escapedBranches_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    escapedBranches_value = escapedBranches_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    escapedBranches_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return escapedBranches_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Collection escapedBranches_compute() {
      HashSet set = new HashSet();
      for(Iterator iter = branches().iterator(); iter.hasNext(); ) {
        Object o = iter.next();
        if(o instanceof ContinueStmt && !targetOf((ContinueStmt)o))
          set.add(o);
        if(o instanceof BreakStmt && !targetOf((BreakStmt)o))
          set.add(o);
        if(o instanceof ReturnStmt)
          set.add(o);
      }
      return set;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void branches_reset() {
    branches_computed = false;
    
    branches_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="branches") protected boolean branches_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="branches") protected Collection branches_value;

  /**
   * @attribute syn
   * @aspect BranchTarget
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\BranchTarget.jrag:37
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="BranchTarget", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\BranchTarget.jrag:37")
  @SideEffect.Pure(group="branches") public Collection branches() {
    ASTState state = state();
    if (branches_computed) {
      return branches_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    branches_value = branches_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    branches_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return branches_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Collection branches_compute() {
      HashSet set = new HashSet();
      super.collectBranches(set);
      return set;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void targetContinues_reset() {
    targetContinues_computed = false;
    
    targetContinues_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="targetContinues") protected boolean targetContinues_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="targetContinues") protected Collection targetContinues_value;

  /**
   * @attribute syn
   * @aspect BranchTarget
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\BranchTarget.jrag:33
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="BranchTarget", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\BranchTarget.jrag:33")
  @SideEffect.Pure(group="targetContinues") public Collection targetContinues() {
    ASTState state = state();
    if (targetContinues_computed) {
      return targetContinues_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    targetContinues_value = targetContinues_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    targetContinues_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return targetContinues_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Collection targetContinues_compute() {
      HashSet set = new HashSet();
      for(Iterator iter = targetBranches().iterator(); iter.hasNext(); ) {
        Object o = iter.next();
        if(o instanceof ContinueStmt)
          set.add(o);
      }
      if(getParent() instanceof LabeledStmt) {
        for(Iterator iter = ((LabeledStmt)getParent()).targetBranches().iterator(); iter.hasNext(); ) {
          Object o = iter.next();
          if(o instanceof ContinueStmt)
            set.add(o);
        }
      }
      return set;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void targetBreaks_reset() {
    targetBreaks_computed = false;
    
    targetBreaks_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="targetBreaks") protected boolean targetBreaks_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="targetBreaks") protected Collection targetBreaks_value;

  /**
   * @attribute syn
   * @aspect BranchTarget
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\BranchTarget.jrag:34
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="BranchTarget", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\BranchTarget.jrag:34")
  @SideEffect.Pure(group="targetBreaks") public Collection targetBreaks() {
    ASTState state = state();
    if (targetBreaks_computed) {
      return targetBreaks_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    targetBreaks_value = targetBreaks_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    targetBreaks_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return targetBreaks_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Collection targetBreaks_compute() {
      HashSet set = new HashSet();
      for(Iterator iter = targetBranches().iterator(); iter.hasNext(); ) {
        Object o = iter.next();
        if(o instanceof BreakStmt)
          set.add(o);
      }
      return set;
    }
  /** @apilevel internal */
  @SideEffect.Pure public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}

package AST;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast class
 * @aspect ComposingVisitorRSF
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\src\\ComposingVisitors.jrag:649
 */
public class ComposingVisitorRSF extends java.lang.Object implements ComposingVisitor {
  
        public boolean visit(CompilationUnit baseCU, CompilationUnit featCU) {

            /* Check package declarations. */
            if (!baseCU.getPackageDecl().equals(featCU.getPackageDecl())) {
                throw new CompositionErrorException("Composition of '"
                        + baseCU.relativeName() + "' and '"
                        + featCU.relativeName() + " failed! Package names do not match!");
            }

            /* Import derlarations. */
            List<ImportDecl> baseIDs = baseCU.getImportDeclList();
            for (ImportDecl featID : featCU.getImportDeclList()) {
                baseIDs.add(featID);
            }

            /*
             * Add accumulated imports to the refinement CU to prevent possible
             * failed external type resolutions.
             */
            try {
                featCU.setImportDeclList(baseIDs.clone());
            } catch (CloneNotSupportedException e) {
                throw new RuntimeException(e);
            }
            //featCU.flushCaches();

            /* Type declarations */
            for (TypeDecl featTD : featCU.getTypeDeclList()) {
                for (TypeDecl baseTD : baseCU.getTypeDeclList()) {
                    baseTD.accept(this, featTD);
                }
            }
            
            /* Setting formSource to false exclude this refinement CU from compilation. */
            featCU.setFromSource(false);
            
            // TODO return value should depend on success of the composition.
            return true;
        }

  

        public boolean visit(TypeDecl baseTD, TypeDecl featTD) {
            if (baseTD instanceof ClassDecl && featTD instanceof ClassDecl) {

                /* Preserve refining role. Used while generating introduces. */
                baseTD.compilationUnit().addRefiningType((ClassDecl) featTD);
                return ((ClassDecl) baseTD).accept(this, (ClassDecl) featTD);
            } else if (baseTD instanceof InterfaceDecl
                       && featTD instanceof InterfaceDecl) {

                /* Preserve refining role. Used while generating introduces. */
                baseTD.compilationUnit().addRefiningType((InterfaceDecl) featTD);
                return ((InterfaceDecl) baseTD).accept(this, (InterfaceDecl) featTD);
            }

            // TODO return value should depend on success of the composition.
            return true;
        }

  

        public boolean visit(InterfaceDecl baseND, InterfaceDecl featND) {

            /* Check interface names */
            if (! baseND.getID().equals(featND.getID())) {
                return false; //TODO Unchecked exception here.
            }

            /* Modifiers. Feature Modifiers override thoes of the base. */
//            if (featND.getModifiers().getModifierList()
//                .getNumChildNoTransform() != 0) {
//                Modifiers mods = featND.getModifiersNoTransform();
//                mods.setOldParent(mods.getParent());
//                baseND.setModifiers(mods);
//            }

            /*
             *'extends' in the interface declaration. Preserving Accesses of
             * the base type.
             */
            if (featND.getSuperInterfaceIdListNoTransform()
                .getNumChild() != 0) { // getNumChild with trasform OK
                List<Access> featSup =
                    featND.getSuperInterfaceIdListNoTransform();
                List<Access> baseSup =
                    baseND.getSuperInterfaceIdListNoTransform();

                for (int i = 0; i < featSup.getNumChild(); ++i) { // getNumChild with trasform OK
                    Access ac = featSup.getChild(i); // getChild with trasform OK
                    ac.setOldParent(ac.getParent());
                    baseSup.add(ac);
                }
            }

            // BodyDecl
            /*
             * Sort base and refinement BodyDecl by their types.
             */
            ArrayList<FieldDecl> baseFDs = new ArrayList<FieldDecl>();
            ArrayList<FieldDeclaration> baseFDEs =
                new ArrayList<FieldDeclaration>();
            ArrayList<MethodDecl> baseMDs = new ArrayList<MethodDecl>();
            ArrayList<ConstructorDecl> baseCons = new ArrayList<ConstructorDecl>();

            ArrayList<FieldDecl> featFDs = new ArrayList<FieldDecl>();
            ArrayList<FieldDeclaration> featFDEs =
                new ArrayList<FieldDeclaration>();
            ArrayList<MethodDecl> featMDs = new ArrayList<MethodDecl>();
            ArrayList<ConstructorDecl> featCons = new ArrayList<ConstructorDecl>();

            for (BodyDecl featBD : featND.getBodyDeclList()) {
                if (featBD instanceof FieldDecl) {
                    featFDs.add((FieldDecl) featBD);
                } else if (featBD instanceof FieldDeclaration) {
                    featFDEs.add((FieldDeclaration) featBD);
                } else if (featBD instanceof MethodDecl) {
                    featMDs.add((MethodDecl) featBD);
                } else if (featBD instanceof ConstructorDecl) {
                    featCons.add((ConstructorDecl) featBD);
                } else {
                    notImplemented(featBD);
                }
            }
            for (BodyDecl baseBD : baseND.getBodyDeclList()) {
                if (baseBD instanceof FieldDecl) {
                    baseFDs.add((FieldDecl) baseBD);
                } else if (baseBD instanceof FieldDeclaration) {
                    baseFDEs.add((FieldDeclaration) baseBD);
                } else if (baseBD instanceof MethodDecl) {
                    baseMDs.add((MethodDecl) baseBD);
                } else if (baseBD instanceof ConstructorDecl) {
                    baseCons.add((ConstructorDecl) baseBD);
                } else {
                    notImplemented(baseBD);
                }
            }

            /* Process FieldDecls */
            // TODO
            for (FieldDecl featFD : featFDs) {
                notImplemented(featFD);
                // TODO not forget to set old parent
            }

            /* Process FieldDeclarations */
            for (FieldDeclaration featFDE : featFDEs) {
                featFDE.setOldParent(featFDE.getParent());
                baseND.addBodyDecl(featFDE);
            }

            /* Process MethodDecl */
            for (MethodDecl featMD : featMDs) {
                featMD.setOldParent(featMD.getParent());
                baseND.addBodyDecl(featMD);
            }

            /*
             * Flushing the caches is needed because the structure of the class
             * might have been changed due to composition of refinements.  So
             * the old data in the caches must be flushed for new data to be
             * generated.
             */
            //baseND.flushCaches();
            baseND.flushCachesNoTransform();

            // TODO return value should depend on success of the composition.
            return true;
        }

  

        public boolean visit(ClassDecl baseCD, ClassDecl featCD) {

            /* Check class names */
            if (! baseCD.getID().equals(featCD.getID())) {
                return false; //TODO Unchecked exception here.
            }

            /* Modifiers. Feature Modifiers override thoes of the base. */
//            if (featCD.getModifiers().getModifierList()
//                .getNumChildNoTransform() != 0) {
//                Modifiers mods = featCD.getModifiersNoTransform();
//                mods.setOldParent(mods.getParent());
//                baseCD.setModifiers(mods);
//            }

            /* SuperClassAccess. 'extends' in the class declaration. */
            if (featCD.hasSuperClassAccess()) {
                if (baseCD.hasSuperClassAccess()) {

                    /*
                     * Preserving Accesses of the base type for refs generation.
                     * TODO It is a hack: The Access to the super class is added
                     * to the list of implemented interfaces, but the super
                     * class is not an interface.
                     */
                    Access baseSup = baseCD.getSuperClassAccess();
                    baseCD.addPreservedSuperClassAccess(baseSup);
                    //baseCD.getImplementsListNoTransform().add(baseSup);
                }
                Access featSup = featCD.getSuperClassAccess();
                featSup.setOldParent(featSup.getParent());
                baseCD.setSuperClassAccess(featSup);
            }

            /* Implements */
            if (featCD.getImplementsList().getNumChild() != 0) { // getNumChild with transform is OK.
                List<Access> featImp
                    = featCD.getImplementsListNoTransform();
                List<Access> baseImp
                    = baseCD.getImplementsListNoTransform();

                /* Preserving Accesses of the base type. */
                for (int i = 0; i < featImp.getNumChild(); ++i) { // getNumChild with transform is OK.
                    Access ac = featImp.getChild(i); // getChild with transform is OK.
                    ac.setOldParent(ac.getParent());
                    baseImp.add(ac);
                }
            }

            // BodyDecl
            /*
             * Sort base and refinement BodyDecl by their types.
             */
            ArrayList<FieldDecl> baseFDs = new ArrayList<FieldDecl>();
            ArrayList<FieldDeclaration> baseFDEs =
                new ArrayList<FieldDeclaration>();
            ArrayList<MethodDecl> baseMDs = new ArrayList<MethodDecl>();
            ArrayList<ConstructorDecl> baseCons = new ArrayList<ConstructorDecl>();
            ArrayList<InstanceInitializer> baseInstInits = new ArrayList<InstanceInitializer>();
            ArrayList<StaticInitializer> baseStatInits = new ArrayList<StaticInitializer>();
            ArrayList<MemberClassDecl> baseMemberCDs = new ArrayList<MemberClassDecl>();
            ArrayList<MemberInterfaceDecl> baseMemberIDs = new ArrayList<MemberInterfaceDecl>();

            ArrayList<FieldDecl> featFDs = new ArrayList<FieldDecl>();
            ArrayList<FieldDeclaration> featFDEs =
                new ArrayList<FieldDeclaration>();
            ArrayList<MethodDecl> featMDs = new ArrayList<MethodDecl>();
            ArrayList<ConstructorDecl> featCons = new ArrayList<ConstructorDecl>();
            ArrayList<InstanceInitializer> featInstInits = new ArrayList<InstanceInitializer>();
            ArrayList<StaticInitializer> featStatInits = new ArrayList<StaticInitializer>();
            ArrayList<MemberClassDecl> featMemberCDs = new ArrayList<MemberClassDecl>();
            ArrayList<MemberInterfaceDecl> featMemberIDs = new ArrayList<MemberInterfaceDecl>();

            for (BodyDecl featBD : featCD.getBodyDeclList()) {
                if (featBD instanceof FieldDecl) {
                    featFDs.add((FieldDecl) featBD);
                } else if (featBD instanceof FieldDeclaration) {
                    featFDEs.add((FieldDeclaration) featBD);
                } else if (featBD instanceof MethodDecl) {
                    featMDs.add((MethodDecl) featBD);
                } else if (featBD instanceof ConstructorDecl) {
                    featCons.add((ConstructorDecl) featBD);
                } else if (featBD instanceof InstanceInitializer) {
                    featInstInits.add((InstanceInitializer) featBD);
                } else if (featBD instanceof StaticInitializer) {
                    featStatInits.add((StaticInitializer) featBD);
                } else if (featBD instanceof MemberClassDecl) {
                    featMemberCDs.add((MemberClassDecl) featBD);
                } else if (featBD instanceof MemberInterfaceDecl) {
                    featMemberIDs.add((MemberInterfaceDecl) featBD);
                } else {
                    notImplemented(featBD);
                }
            }
            for (BodyDecl baseBD : baseCD.getBodyDeclList()) {
                if (baseBD instanceof FieldDecl) {
                    baseFDs.add((FieldDecl) baseBD);
                } else if (baseBD instanceof FieldDeclaration) {
                    baseFDEs.add((FieldDeclaration) baseBD);
                } else if (baseBD instanceof MethodDecl) {
                    baseMDs.add((MethodDecl) baseBD);
                } else if (baseBD instanceof ConstructorDecl) {
                    baseCons.add((ConstructorDecl) baseBD);
                } else if (baseBD instanceof InstanceInitializer) {
                    baseInstInits.add((InstanceInitializer) baseBD);
                } else if (baseBD instanceof StaticInitializer) {
                    baseStatInits.add((StaticInitializer) baseBD);
                } else if (baseBD instanceof MemberClassDecl) {
                    baseMemberCDs.add((MemberClassDecl) baseBD);
                } else if (baseBD instanceof MemberInterfaceDecl) {
                    baseMemberIDs.add((MemberInterfaceDecl) baseBD);
                } else {
                    notImplemented(baseBD);
                }
            }

            /* Process FieldDecls */
            // TODO
            for (FieldDecl featFD : featFDs) {
                notImplemented(featFD);
                // TODO not forget to set old parent
            }

            /* Process FieldDeclarations */
            for (FieldDeclaration featFDE : featFDEs) {
                featFDE.setOldParent(featFDE.getParent());
                baseCD.addBodyDecl(featFDE);
            }

            /* Process MethodDecl */
            for (MethodDecl featMD : featMDs) {
                featMD.setOldParent(featMD.getParent());
                baseCD.addBodyDecl(featMD);
            }

            /* Process ConstructorDecls
             * TODO every refinement constructor
             * contains an impicit original() as the very first instruction.
             */
            for (ConstructorDecl featCon : featCons) {
                featCon.setOldParent(featCon.getParent());
                baseCD.addBodyDecl(featCon);
            }

            /* Process StaticInitializers */
            for (StaticInitializer featStatInit : featStatInits) {
                featStatInit.setOldParent(featStatInit.getParent());
                baseCD.addBodyDecl(featStatInit);
            }

            /* Process InstanceInitializers */
            for (InstanceInitializer featInstInit : featInstInits) {
                featInstInit.setOldParent(featInstInit.getParent());
                baseCD.addBodyDecl(featInstInit);
            }

            /* Process MemberClassDecls */
            for (MemberClassDecl featMemberCD : featMemberCDs) {
                ClassDecl fCD = featMemberCD.getClassDeclNoTransform();
                boolean composed = false;
                for (MemberClassDecl baseMemberCD : baseMemberCDs) {
                    ClassDecl bCD = baseMemberCD.getClassDeclNoTransform();
                    if (bCD.getID().equals(fCD.getID())) {
                        fCD.setOldParent(fCD.getParent());
                        bCD.accept(this, fCD);
                        composed = true;
                    }
                }
                if (!composed) {
                    featMemberCD.setOldParent(featMemberCD.getParent());
                    baseCD.addBodyDecl(featMemberCD);
                }
            }

            /* Process MemberInterfaceDecls */
            // TODO
            for (MemberInterfaceDecl featMemberID : featMemberIDs) {
                notImplemented(featMemberID);
                // TODO not forget to set old parent
            }

            /*
             * Flushing the caches is needed because the structure of the class
             * might have been changed due to composition of refinements.  So
             * the old data in the caches must be flushed for new data to be
             * generated.
             */
            //baseCD.flushCaches();
            baseCD.flushCachesNoTransform();
            // TODO return value should depend on success of the composition.
            return true;
        }

  

        public boolean visit(MethodDecl baseMD, MethodDecl featMD){
            throw new UnsupportedOperationException();
        }

  

        private void notImplemented(ASTNode<? extends ASTNode> node) {
            System.err.println("E: " + node.getClass().getName()
                               + " composition is not implemented!");
        }


}

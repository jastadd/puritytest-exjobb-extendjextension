/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package AST;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\java.ast:147
 * @production Binary : {@link Expr} ::= <span class="component">LeftOperand:{@link Expr}</span> <span class="component">RightOperand:{@link Expr}</span>;

 */
public abstract class Binary extends Expr implements Cloneable {
  /**
   * @aspect PrettyPrint
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\PrettyPrint.jadd:394
   */
  public void toString(StringBuffer s) {
    getLeftOperand().toString(s);
    s.append(printOp());
    getRightOperand().toString(s);
  }
  /**
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:879
   */
  public void createBCode(CodeGeneration gen) {   
    getLeftOperand().createBCode(gen);
    getLeftOperand().type().emitCastTo(gen, type()); // Binary numeric promotion
    getRightOperand().createBCode(gen);
    getRightOperand().type().emitCastTo(gen, type()); // Binary numeric promotion
    emitOperation(gen);
  }
  /**
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:887
   */
  public void emitShiftExpr(CodeGeneration gen) {
    getLeftOperand().createBCode(gen);
    getLeftOperand().type().emitCastTo(gen, type()); // Binary numeric promotion
    getRightOperand().createBCode(gen);
    getRightOperand().type().emitCastTo(gen, typeInt());
    emitOperation(gen);
  }
  /**
   * @declaredat ASTNode:1
   */
  public Binary() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
  }
  /**
   * @declaredat ASTNode:13
   */
  public Binary(Expr p0, Expr p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:18
   */
  @SideEffect.Pure protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:24
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    isConstant_reset();
    isDAafterTrue_Variable_reset();
    isDAafterFalse_Variable_reset();
    isDAafter_Variable_reset();
    isDUafter_Variable_reset();
    isDUbefore_Variable_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:42
   */
  @SideEffect.Fresh public Binary clone() throws CloneNotSupportedException {
    Binary node = (Binary) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:53
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public abstract Binary fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:61
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract Binary treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:69
   */
  @SideEffect.Fresh(group="_ASTNode") public abstract Binary treeCopy();
  /**
   * Replaces the LeftOperand child.
   * @param node The new node to replace the LeftOperand child.
   * @apilevel high-level
   */
  public void setLeftOperand(Expr node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the LeftOperand child.
   * @return The current node used as the LeftOperand child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="LeftOperand")
  @SideEffect.Pure public Expr getLeftOperand() {
    return (Expr) getChild(0);
  }
  /**
   * Retrieves the LeftOperand child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the LeftOperand child.
   * @apilevel low-level
   */
  public Expr getLeftOperandNoTransform() {
    return (Expr) getChildNoTransform(0);
  }
  /**
   * Replaces the RightOperand child.
   * @param node The new node to replace the RightOperand child.
   * @apilevel high-level
   */
  public void setRightOperand(Expr node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the RightOperand child.
   * @return The current node used as the RightOperand child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="RightOperand")
  @SideEffect.Pure public Expr getRightOperand() {
    return (Expr) getChild(1);
  }
  /**
   * Retrieves the RightOperand child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the RightOperand child.
   * @apilevel low-level
   */
  public Expr getRightOperandNoTransform() {
    return (Expr) getChildNoTransform(1);
  }
  /**
   * @aspect ConstantExpression
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:370
   */
  private TypeDecl refined_ConstantExpression_Binary_binaryNumericPromotedType()
{
    TypeDecl leftType = left().type();
    TypeDecl rightType = right().type();
    if(leftType.isString())
      return leftType;
    if(rightType.isString())
      return rightType;
    if(leftType.isNumericType() && rightType.isNumericType())
      return leftType.binaryNumericPromotion(rightType);
    if(leftType.isBoolean() && rightType.isBoolean())
      return leftType;
    return unknownType();
  }
  /**
   * @attribute syn
   * @aspect PrettyPrint
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\PrettyPrint.jadd:400
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="PrettyPrint", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\PrettyPrint.jadd:400")
  @SideEffect.Pure(group="printOp") public abstract String printOp();
/** @apilevel internal */
protected ASTState.Cycle isConstant_cycle = null;
  /** @apilevel internal */
  @SideEffect.Ignore private void isConstant_reset() {
    isConstant_computed = false;
    isConstant_initialized = false;
    isConstant_cycle = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="isConstant") protected boolean isConstant_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="isConstant") protected boolean isConstant_value;
  /** @apilevel internal */
  @SideEffect.Secret(group="isConstant") protected boolean isConstant_initialized = false;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="ConstantExpression", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:345")
  @SideEffect.Pure(group="isConstant") public boolean isConstant() {
    if (isConstant_computed) {
      return isConstant_value;
    }
    ASTState state = state();
    if (!isConstant_initialized) {
      isConstant_initialized = true;
      isConstant_value = false;
    }
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      int _boundaries = state.boundariesCrossed;
      boolean isFinal = this.is$Final();
      do {
        isConstant_cycle = state.nextCycle();
        boolean new_isConstant_value = getLeftOperand().isConstant() && getRightOperand().isConstant();
        if (new_isConstant_value != isConstant_value) {
          state.setChangeInCycle();
        }
        isConstant_value = new_isConstant_value;
      } while (state.testAndClearChangeInCycle());
      if (isFinal && _boundaries == state().boundariesCrossed) {
        isConstant_computed = true;
      } else {
        state.startResetCycle();
        boolean $tmp = getLeftOperand().isConstant() && getRightOperand().isConstant();
        isConstant_computed = false;
        isConstant_initialized = false;
      }
      state.leaveCircle();
    } else if (isConstant_cycle != state.cycle()) {
      isConstant_cycle = state.cycle();
      if (state.resetCycle()) {
        isConstant_computed = false;
        isConstant_initialized = false;
        isConstant_cycle = null;
        return isConstant_value;
      }
      boolean new_isConstant_value = getLeftOperand().isConstant() && getRightOperand().isConstant();
      if (new_isConstant_value != isConstant_value) {
        state.setChangeInCycle();
      }
      isConstant_value = new_isConstant_value;
    } else {
    }
    return isConstant_value;
  }
  /**
   * @attribute syn
   * @aspect ConstantExpression
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:368
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantExpression", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:368")
  @SideEffect.Pure(group="left") public Expr left() {
    Expr left_value = getLeftOperand();
    return left_value;
  }
  /**
   * @attribute syn
   * @aspect ConstantExpression
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:369
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantExpression", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:369")
  @SideEffect.Pure(group="right") public Expr right() {
    Expr right_value = getRightOperand();
    return right_value;
  }
  /**
   * @attribute syn
   * @aspect ConstantExpression
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:370
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantExpression", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:370")
  @SideEffect.Pure(group="binaryNumericPromotedType") public TypeDecl binaryNumericPromotedType() {
    {
        TypeDecl leftType = left().type();
        TypeDecl rightType = right().type();
        if(leftType.isBoolean() && rightType.isBoolean()) {
          return leftType.isReferenceType() ? leftType.unboxed() : leftType;
        }
        return refined_ConstantExpression_Binary_binaryNumericPromotedType();
      }
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void isDAafterTrue_Variable_reset() {
    isDAafterTrue_Variable_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="isDAafterTrue_Variable") protected java.util.Map isDAafterTrue_Variable_values;

  /**
   * @attribute syn
   * @aspect DA
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:401
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="DA", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:401")
  @SideEffect.Pure(group="isDAafterTrue_Variable") public boolean isDAafterTrue(Variable v) {
    Object _parameters = v;
    if (isDAafterTrue_Variable_values == null) isDAafterTrue_Variable_values = new java.util.HashMap(4);
    ASTState state = state();
    if (isDAafterTrue_Variable_values.containsKey(_parameters)) {
      return (Boolean) isDAafterTrue_Variable_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boolean isDAafterTrue_Variable_value = getRightOperand().isDAafter(v) || isFalse();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isDAafterTrue_Variable_values.put(_parameters, isDAafterTrue_Variable_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return isDAafterTrue_Variable_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void isDAafterFalse_Variable_reset() {
    isDAafterFalse_Variable_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="isDAafterFalse_Variable") protected java.util.Map isDAafterFalse_Variable_values;

  /**
   * @attribute syn
   * @aspect DA
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:402
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="DA", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:402")
  @SideEffect.Pure(group="isDAafterFalse_Variable") public boolean isDAafterFalse(Variable v) {
    Object _parameters = v;
    if (isDAafterFalse_Variable_values == null) isDAafterFalse_Variable_values = new java.util.HashMap(4);
    ASTState state = state();
    if (isDAafterFalse_Variable_values.containsKey(_parameters)) {
      return (Boolean) isDAafterFalse_Variable_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boolean isDAafterFalse_Variable_value = getRightOperand().isDAafter(v) || isTrue();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isDAafterFalse_Variable_values.put(_parameters, isDAafterFalse_Variable_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return isDAafterFalse_Variable_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void isDAafter_Variable_reset() {
    isDAafter_Variable_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="isDAafter_Variable") protected java.util.Map isDAafter_Variable_values;

  /**
   * @attribute syn
   * @aspect DA
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:237
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="DA", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:237")
  @SideEffect.Pure(group="isDAafter_Variable") public boolean isDAafter(Variable v) {
    Object _parameters = v;
    if (isDAafter_Variable_values == null) isDAafter_Variable_values = new java.util.HashMap(4);
    ASTState state = state();
    if (isDAafter_Variable_values.containsKey(_parameters)) {
      return (Boolean) isDAafter_Variable_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boolean isDAafter_Variable_value = getRightOperand().isDAafter(v);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isDAafter_Variable_values.put(_parameters, isDAafter_Variable_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return isDAafter_Variable_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void isDUafter_Variable_reset() {
    isDUafter_Variable_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="isDUafter_Variable") protected java.util.Map isDUafter_Variable_values;

  /**
   * @attribute syn
   * @aspect DU
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:698
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="DU", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:698")
  @SideEffect.Pure(group="isDUafter_Variable") public boolean isDUafter(Variable v) {
    Object _parameters = v;
    if (isDUafter_Variable_values == null) isDUafter_Variable_values = new java.util.HashMap(4);
    ASTState state = state();
    if (isDUafter_Variable_values.containsKey(_parameters)) {
      return (Boolean) isDUafter_Variable_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boolean isDUafter_Variable_value = getRightOperand().isDUafter(v);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isDUafter_Variable_values.put(_parameters, isDUafter_Variable_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return isDUafter_Variable_value;
  }
  /**
   * @attribute inh
   * @aspect DU
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:695
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="DU", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:695")
  @SideEffect.Pure(group="isDUbefore_Variable") public boolean isDUbefore(Variable v) {
    Object _parameters = v;
    if (isDUbefore_Variable_values == null) isDUbefore_Variable_values = new java.util.HashMap(4);
    ASTState state = state();
    if (isDUbefore_Variable_values.containsKey(_parameters)) {
      return (Boolean) isDUbefore_Variable_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boolean isDUbefore_Variable_value = getParent().Define_isDUbefore(this, null, v);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isDUbefore_Variable_values.put(_parameters, isDUbefore_Variable_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return isDUbefore_Variable_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void isDUbefore_Variable_reset() {
    isDUbefore_Variable_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="isDUbefore_Variable") protected java.util.Map isDUbefore_Variable_values;

  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:232
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isDAbefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    if (_callerNode == getRightOperandNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:405
      return getLeftOperand().isDAafter(v);
    }
    else {
      return getParent().Define_isDAbefore(this, _callerNode, v);
    }
  }
  @SideEffect.Pure protected boolean canDefine_isDAbefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:690
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isDUbefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    if (_callerNode == getRightOperandNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:845
      return getLeftOperand().isDUafter(v);
    }
    else {
      return getParent().Define_isDUbefore(this, _callerNode, v);
    }
  }
  @SideEffect.Pure protected boolean canDefine_isDUbefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Pure public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}

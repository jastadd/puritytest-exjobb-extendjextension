package AST;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast class
 * @aspect StringsInSwitch
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Backend\\StringsInSwitch.jrag:68
 */
 class CaseLbl extends java.lang.Object {
  
		int lbl;

  
		int serial;

  
		String value;

  
		java.util.List<Stmt> stmts = new ArrayList<Stmt>();

  

		CaseLbl(int lbl) {
			this.lbl = lbl;
		}

  

		CaseLbl(ConstCase cc, CodeGeneration gen) {
			lbl = cc.label(gen);
			value = cc.getValue().constant().stringValue();
		}

  

		void addStmt(Stmt stmt) {
			stmts.add(stmt);
		}

  

		/**
 		 * Code generation for case label.
 		 */
		void createBCode(CodeGeneration gen) {
			for (Stmt stmt : stmts) {
				stmt.createBCode(gen);
			}
		}


}

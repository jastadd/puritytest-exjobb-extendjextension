/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package AST;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\java.ast:21
 * @production PrimitiveTypeAccess : {@link TypeAccess} ::= <span class="component">&lt;Package:String&gt;</span> <span class="component">&lt;ID:String&gt;</span> <span class="component">&lt;Name:String&gt;</span>;

 */
public class PrimitiveTypeAccess extends TypeAccess implements Cloneable {
  /**
   * @declaredat ASTNode:1
   */
  public PrimitiveTypeAccess() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
  }
  /**
   * @declaredat ASTNode:12
   */
  public PrimitiveTypeAccess(String p0) {
    setName(p0);
  }
  /**
   * @declaredat ASTNode:15
   */
  public PrimitiveTypeAccess(beaver.Symbol p0) {
    setName(p0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:19
   */
  @SideEffect.Pure protected int numChildren() {
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:25
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:29
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    decls_reset();
    getPackage_reset();
    getID_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:36
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  @SideEffect.Fresh public PrimitiveTypeAccess clone() throws CloneNotSupportedException {
    PrimitiveTypeAccess node = (PrimitiveTypeAccess) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  @SideEffect.Fresh(group="_ASTNode") public PrimitiveTypeAccess copy() {
    try {
      PrimitiveTypeAccess node = (PrimitiveTypeAccess) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:64
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public PrimitiveTypeAccess fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:74
   */
  @SideEffect.Fresh(group="_ASTNode") public PrimitiveTypeAccess treeCopyNoTransform() {
    PrimitiveTypeAccess tree = (PrimitiveTypeAccess) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:94
   */
  @SideEffect.Fresh(group="_ASTNode") public PrimitiveTypeAccess treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:99
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_Name == ((PrimitiveTypeAccess) node).tokenString_Name);    
  }
  /**
   * Replaces the lexeme Name.
   * @param value The new value for the lexeme Name.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setName(String value) {
    tokenString_Name = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_Name;
  /**
   */
  public int Namestart;
  /**
   */
  public int Nameend;
  /**
   * JastAdd-internal setter for lexeme Name using the Beaver parser.
   * @param symbol Symbol containing the new value for the lexeme Name
   * @apilevel internal
   */
  public void setName(beaver.Symbol symbol) {
    if (symbol.value != null && !(symbol.value instanceof String))
    throw new UnsupportedOperationException("setName is only valid for String lexemes");
    tokenString_Name = (String)symbol.value;
    Namestart = symbol.getStart();
    Nameend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme Name.
   * @return The value for the lexeme Name.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Name")
  @SideEffect.Pure(group="_ASTNode") public String getName() {
    return tokenString_Name != null ? tokenString_Name : "";
  }
  /**
   * This method should not be called. This method throws an exception due to
   * the corresponding child being an NTA shadowing a non-NTA child.
   * @param node
   * @apilevel internal
   */
  @SideEffect.Local public void setPackage(String node) {
    throw new Error("Can not replace NTA child Package in PrimitiveTypeAccess!");
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_Package;
  /**
   * This method should not be called. This method throws an exception due to
   * the corresponding child being an NTA shadowing a non-NTA child.
   * @param node
   * @apilevel internal
   */
  @SideEffect.Local public void setID(String node) {
    throw new Error("Can not replace NTA child ID in PrimitiveTypeAccess!");
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_ID;
  /** @apilevel internal */
  @SideEffect.Ignore private void decls_reset() {
    decls_computed = false;
    
    decls_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="decls") protected boolean decls_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="decls") protected SimpleSet decls_value;

  /**
   * @attribute syn
   * @aspect TypeScopePropagation
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupType.jrag:146
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeScopePropagation", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupType.jrag:146")
  @SideEffect.Pure(group="decls") public SimpleSet decls() {
    ASTState state = state();
    if (decls_computed) {
      return decls_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    decls_value = lookupType(PRIMITIVE_PACKAGE_NAME, name());
    if (isFinal && _boundaries == state().boundariesCrossed) {
    decls_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return decls_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void getPackage_reset() {
    getPackage_computed = false;
    
    getPackage_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="getPackage") protected boolean getPackage_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="getPackage") protected String getPackage_value;

  /**
   * @attribute syn nta
   * @aspect TypeScopePropagation
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupType.jrag:147
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="TypeScopePropagation", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupType.jrag:147")
  @SideEffect.Pure(group="getPackage") public String getPackage() {
    ASTState state = state();
    if (getPackage_computed) {
      return getPackage_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    getPackage_value = PRIMITIVE_PACKAGE_NAME;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    getPackage_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return getPackage_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void getID_reset() {
    getID_computed = false;
    
    getID_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="getID") protected boolean getID_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="getID") protected String getID_value;

  /**
   * @attribute syn nta
   * @aspect TypeScopePropagation
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupType.jrag:148
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="TypeScopePropagation", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupType.jrag:148")
  @SideEffect.Pure(group="getID") public String getID() {
    ASTState state = state();
    if (getID_computed) {
      return getID_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    getID_value = getName();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    getID_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return getID_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}

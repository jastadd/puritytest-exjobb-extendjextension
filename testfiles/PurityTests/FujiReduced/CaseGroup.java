package AST;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast class
 * @aspect StringsInSwitch
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Backend\\StringsInSwitch.jrag:50
 */
 class CaseGroup extends java.lang.Object {
  
		int lbl;

  
		int hashCode;

  
		java.util.List<CaseLbl> lbls = new LinkedList<CaseLbl>();

  

		public CaseGroup(SwitchStmt ss, int hash) {
			lbl = ss.hostType().constantPool().newLabel();
			hashCode = hash;
		}

  

		public void addCase(CaseLbl lbl) {
			lbls.add(lbl);
		}


}

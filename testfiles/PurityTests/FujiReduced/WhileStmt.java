/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package AST;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\java.ast:205
 * @production WhileStmt : {@link BranchTargetStmt} ::= <span class="component">Condition:{@link Expr}</span> <span class="component">{@link Stmt}</span>;

 */
public class WhileStmt extends BranchTargetStmt implements Cloneable {
  /**
   * @aspect PrettyPrint
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\PrettyPrint.jadd:587
   */
  public void toString(StringBuffer s) {
    s.append(indent());
    s.append("while(");
    getCondition().toString(s);
    s.append(")");
    getStmt().toString(s);
  }
  /**
   * @aspect TypeCheck
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeCheck.jrag:322
   */
  public void typeCheck() {
    TypeDecl cond = getCondition().type();
    if(!cond.isBoolean()) {
      error("the type of \"" + getCondition() + "\" is " + cond.name() + " which is not boolean");
    }
  }
  /**
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:1344
   */
  public void createBCode(CodeGeneration gen) {
    super.createBCode(gen);
    gen.addLabel(cond_label());
    getCondition().emitEvalBranch(gen);
    gen.addLabel(stmt_label());
    if(getCondition().canBeTrue()) {
      getStmt().createBCode(gen);
      if(getStmt().canCompleteNormally())
        gen.emitGoto(cond_label());
    }
    gen.addLabel(end_label());
  }
  /**
   * @declaredat ASTNode:1
   */
  public WhileStmt() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
  }
  /**
   * @declaredat ASTNode:13
   */
  public WhileStmt(Expr p0, Stmt p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:18
   */
  @SideEffect.Pure protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:24
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    targetOf_ContinueStmt_reset();
    targetOf_BreakStmt_reset();
    isDAafter_Variable_reset();
    isDUafter_Variable_reset();
    isDUbeforeCondition_Variable_reset();
    canCompleteNormally_reset();
    cond_label_reset();
    end_label_reset();
    stmt_label_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  @SideEffect.Fresh public WhileStmt clone() throws CloneNotSupportedException {
    WhileStmt node = (WhileStmt) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:50
   */
  @SideEffect.Fresh(group="_ASTNode") public WhileStmt copy() {
    try {
      WhileStmt node = (WhileStmt) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:69
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public WhileStmt fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:79
   */
  @SideEffect.Fresh(group="_ASTNode") public WhileStmt treeCopyNoTransform() {
    WhileStmt tree = (WhileStmt) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:99
   */
  @SideEffect.Fresh(group="_ASTNode") public WhileStmt treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:104
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the Condition child.
   * @param node The new node to replace the Condition child.
   * @apilevel high-level
   */
  public void setCondition(Expr node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Condition child.
   * @return The current node used as the Condition child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Condition")
  @SideEffect.Pure public Expr getCondition() {
    return (Expr) getChild(0);
  }
  /**
   * Retrieves the Condition child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Condition child.
   * @apilevel low-level
   */
  public Expr getConditionNoTransform() {
    return (Expr) getChildNoTransform(0);
  }
  /**
   * Replaces the Stmt child.
   * @param node The new node to replace the Stmt child.
   * @apilevel high-level
   */
  public void setStmt(Stmt node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the Stmt child.
   * @return The current node used as the Stmt child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Stmt")
  @SideEffect.Pure public Stmt getStmt() {
    return (Stmt) getChild(1);
  }
  /**
   * Retrieves the Stmt child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Stmt child.
   * @apilevel low-level
   */
  public Stmt getStmtNoTransform() {
    return (Stmt) getChildNoTransform(1);
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void targetOf_ContinueStmt_reset() {
    targetOf_ContinueStmt_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="targetOf_ContinueStmt") protected java.util.Map targetOf_ContinueStmt_values;

  /**
   * @attribute syn
   * @aspect BranchTarget
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\BranchTarget.jrag:40
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="BranchTarget", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\BranchTarget.jrag:40")
  @SideEffect.Pure(group="targetOf_ContinueStmt") public boolean targetOf(ContinueStmt stmt) {
    Object _parameters = stmt;
    if (targetOf_ContinueStmt_values == null) targetOf_ContinueStmt_values = new java.util.HashMap(4);
    ASTState state = state();
    if (targetOf_ContinueStmt_values.containsKey(_parameters)) {
      return (Boolean) targetOf_ContinueStmt_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boolean targetOf_ContinueStmt_value = !stmt.hasLabel();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    targetOf_ContinueStmt_values.put(_parameters, targetOf_ContinueStmt_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return targetOf_ContinueStmt_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void targetOf_BreakStmt_reset() {
    targetOf_BreakStmt_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="targetOf_BreakStmt") protected java.util.Map targetOf_BreakStmt_values;

  /**
   * @attribute syn
   * @aspect BranchTarget
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\BranchTarget.jrag:41
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="BranchTarget", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\BranchTarget.jrag:41")
  @SideEffect.Pure(group="targetOf_BreakStmt") public boolean targetOf(BreakStmt stmt) {
    Object _parameters = stmt;
    if (targetOf_BreakStmt_values == null) targetOf_BreakStmt_values = new java.util.HashMap(4);
    ASTState state = state();
    if (targetOf_BreakStmt_values.containsKey(_parameters)) {
      return (Boolean) targetOf_BreakStmt_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boolean targetOf_BreakStmt_value = !stmt.hasLabel();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    targetOf_BreakStmt_values.put(_parameters, targetOf_BreakStmt_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return targetOf_BreakStmt_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void isDAafter_Variable_reset() {
    isDAafter_Variable_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="isDAafter_Variable") protected java.util.Map isDAafter_Variable_values;

  /**
   * @attribute syn
   * @aspect DA
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:233
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="DA", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:233")
  @SideEffect.Pure(group="isDAafter_Variable") public boolean isDAafter(Variable v) {
    Object _parameters = v;
    if (isDAafter_Variable_values == null) isDAafter_Variable_values = new java.util.HashMap(4);
    ASTState state = state();
    if (isDAafter_Variable_values.containsKey(_parameters)) {
      return (Boolean) isDAafter_Variable_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boolean isDAafter_Variable_value = isDAafter_compute(v);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isDAafter_Variable_values.put(_parameters, isDAafter_Variable_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return isDAafter_Variable_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean isDAafter_compute(Variable v) {
      if(!getCondition().isDAafterFalse(v))
        return false;
      for(Iterator iter = targetBreaks().iterator(); iter.hasNext(); ) {
        BreakStmt stmt = (BreakStmt)iter.next();
        if(!stmt.isDAafterReachedFinallyBlocks(v))
          return false;
      }
      return true;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void isDUafter_Variable_reset() {
    isDUafter_Variable_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="isDUafter_Variable") protected java.util.Map isDUafter_Variable_values;

  /**
   * @attribute syn
   * @aspect DU
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:691
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="DU", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:691")
  @SideEffect.Pure(group="isDUafter_Variable") public boolean isDUafter(Variable v) {
    Object _parameters = v;
    if (isDUafter_Variable_values == null) isDUafter_Variable_values = new java.util.HashMap(4);
    ASTState state = state();
    if (isDUafter_Variable_values.containsKey(_parameters)) {
      return (Boolean) isDUafter_Variable_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boolean isDUafter_Variable_value = isDUafter_compute(v);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isDUafter_Variable_values.put(_parameters, isDUafter_Variable_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return isDUafter_Variable_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean isDUafter_compute(Variable v) {
      if(!isDUbeforeCondition(v)) // start a circular evaluation here
        return false;
      if(!getCondition().isDUafterFalse(v))
        return false;
      for(Iterator iter = targetBreaks().iterator(); iter.hasNext(); ) {
        BreakStmt stmt = (BreakStmt)iter.next();
        if(!stmt.isDUafterReachedFinallyBlocks(v))
          return false;
      }
      return true;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void isDUbeforeCondition_Variable_reset() {
    isDUbeforeCondition_Variable_values = null;
  }
  protected java.util.Map isDUbeforeCondition_Variable_values;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="DU", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:1048")
  @SideEffect.Pure(group="isDUbeforeCondition_Variable") public boolean isDUbeforeCondition(Variable v) {
    Object _parameters = v;
    if (isDUbeforeCondition_Variable_values == null) isDUbeforeCondition_Variable_values = new java.util.HashMap(4);
    ASTState.CircularValue _value;
    if (isDUbeforeCondition_Variable_values.containsKey(_parameters)) {
      Object _cache = isDUbeforeCondition_Variable_values.get(_parameters);
      if (!(_cache instanceof ASTState.CircularValue)) {
        return (Boolean) _cache;
      } else {
        _value = (ASTState.CircularValue) _cache;
      }
    } else {
      _value = new ASTState.CircularValue();
      isDUbeforeCondition_Variable_values.put(_parameters, _value);
      _value.value = true;
    }
    ASTState state = state();
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      int _boundaries = state.boundariesCrossed;
      boolean isFinal = this.is$Final();
      boolean new_isDUbeforeCondition_Variable_value;
      do {
        _value.cycle = state.nextCycle();
        new_isDUbeforeCondition_Variable_value = isDUbeforeCondition_compute(v);
        if (new_isDUbeforeCondition_Variable_value != ((Boolean)_value.value)) {
          state.setChangeInCycle();
          _value.value = new_isDUbeforeCondition_Variable_value;
        }
      } while (state.testAndClearChangeInCycle());
      if (isFinal && _boundaries == state().boundariesCrossed) {
        isDUbeforeCondition_Variable_values.put(_parameters, new_isDUbeforeCondition_Variable_value);
      } else {
        isDUbeforeCondition_Variable_values.remove(_parameters);
        state.startResetCycle();
        boolean $tmp = isDUbeforeCondition_compute(v);
      }
      state.leaveCircle();
      return new_isDUbeforeCondition_Variable_value;
    } else if (_value.cycle != state.cycle()) {
      _value.cycle = state.cycle();
      boolean new_isDUbeforeCondition_Variable_value = isDUbeforeCondition_compute(v);
      if (state.resetCycle()) {
        isDUbeforeCondition_Variable_values.remove(_parameters);
      }
      else if (new_isDUbeforeCondition_Variable_value != ((Boolean)_value.value)) {
        state.setChangeInCycle();
        _value.value = new_isDUbeforeCondition_Variable_value;
      }
      return new_isDUbeforeCondition_Variable_value;
    } else {
      return (Boolean) _value.value;
    }
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean isDUbeforeCondition_compute(Variable v) {
      // 1st
      if(!isDUbefore(v))
        return false;
      else if(!getStmt().isDUafter(v))
        return false;
      else {
        for(Iterator iter = targetContinues().iterator(); iter.hasNext(); ) {
          ContinueStmt stmt = (ContinueStmt)iter.next();
          if(!stmt.isDUafterReachedFinallyBlocks(v))
            return false;
        }
      }
      return true;
    }
  /**
   * @attribute syn
   * @aspect NameCheck
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\NameCheck.jrag:396
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameCheck", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\NameCheck.jrag:396")
  @SideEffect.Pure(group="continueLabel") public boolean continueLabel() {
    boolean continueLabel_value = true;
    return continueLabel_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void canCompleteNormally_reset() {
    canCompleteNormally_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="canCompleteNormally") protected boolean canCompleteNormally_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="canCompleteNormally") protected boolean canCompleteNormally_value;

  /**
   * @attribute syn
   * @aspect UnreachableStatements
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\UnreachableStatements.jrag:29
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="UnreachableStatements", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\UnreachableStatements.jrag:29")
  @SideEffect.Pure(group="canCompleteNormally") public boolean canCompleteNormally() {
    ASTState state = state();
    if (canCompleteNormally_computed) {
      return canCompleteNormally_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    canCompleteNormally_value = reachable() && (!getCondition().isConstant() || !getCondition().isTrue()) || reachableBreak();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    canCompleteNormally_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return canCompleteNormally_value;
  }
  /**
   * @attribute syn
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:944
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CreateBCode", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:944")
  @SideEffect.Pure(group="definesLabel") public boolean definesLabel() {
    boolean definesLabel_value = true;
    return definesLabel_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void cond_label_reset() {
    cond_label_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="cond_label") protected boolean cond_label_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="cond_label") protected int cond_label_value;

  /**
   * @attribute syn
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:1340
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CreateBCode", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:1340")
  @SideEffect.Pure(group="cond_label") public int cond_label() {
    ASTState state = state();
    if (cond_label_computed) {
      return cond_label_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    cond_label_value = hostType().constantPool().newLabel();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    cond_label_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return cond_label_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void end_label_reset() {
    end_label_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="end_label") protected boolean end_label_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="end_label") protected int end_label_value;

  /**
   * @attribute syn
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:1341
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CreateBCode", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:1341")
  @SideEffect.Pure(group="end_label") public int end_label() {
    ASTState state = state();
    if (end_label_computed) {
      return end_label_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    end_label_value = hostType().constantPool().newLabel();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    end_label_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return end_label_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void stmt_label_reset() {
    stmt_label_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="stmt_label") protected boolean stmt_label_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="stmt_label") protected int stmt_label_value;

  /**
   * @attribute syn
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:1342
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CreateBCode", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:1342")
  @SideEffect.Pure(group="stmt_label") public int stmt_label() {
    ASTState state = state();
    if (stmt_label_computed) {
      return stmt_label_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    stmt_label_value = hostType().constantPool().newLabel();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    stmt_label_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return stmt_label_value;
  }
  /**
   * @attribute syn
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:1396
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CreateBCode", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:1396")
  @SideEffect.Pure(group="break_label") public int break_label() {
    int break_label_value = end_label();
    return break_label_value;
  }
  /**
   * @attribute syn
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:1415
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CreateBCode", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:1415")
  @SideEffect.Pure(group="continue_label") public int continue_label() {
    int continue_label_value = cond_label();
    return continue_label_value;
  }
  /**
   * @attribute syn
   * @aspect PreciseRethrow
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\PreciseRethrow.jrag:55
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="PreciseRethrow", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\PreciseRethrow.jrag:55")
  @SideEffect.Pure(group="modifiedInScope_Variable") public boolean modifiedInScope(Variable var) {
    boolean modifiedInScope_Variable_value = getStmt().modifiedInScope(var);
    return modifiedInScope_Variable_value;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:232
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isDAbefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    if (_callerNode == getStmtNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:586
      return getCondition().isDAafterTrue(v);
    }
    else if (_callerNode == getConditionNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:585
      return isDAbefore(v);
    }
    else {
      return getParent().Define_isDAbefore(this, _callerNode, v);
    }
  }
  @SideEffect.Pure protected boolean canDefine_isDAbefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:690
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isDUbefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    if (_callerNode == getStmtNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:1065
      return getCondition().isDUafterTrue(v);
    }
    else if (_callerNode == getConditionNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:1045
      return isDUbeforeCondition(v);
    }
    else {
      return getParent().Define_isDUbefore(this, _callerNode, v);
    }
  }
  @SideEffect.Pure protected boolean canDefine_isDUbefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\NameCheck.jrag:361
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_insideLoop(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getStmtNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\NameCheck.jrag:366
      return true;
    }
    else {
      return getParent().Define_insideLoop(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_insideLoop(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\UnreachableStatements.jrag:28
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_reachable(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getStmtNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\UnreachableStatements.jrag:86
      return reachable() && !getCondition().isFalse();
    }
    else {
      return getParent().Define_reachable(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_reachable(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\PreciseRethrow.jrag:195
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_reportUnreachable(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getStmtNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\UnreachableStatements.jrag:153
      return reachable();
    }
    else {
      return getParent().Define_reportUnreachable(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_reportUnreachable(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:971
   * @apilevel internal
   */
 @SideEffect.Pure public int Define_condition_false_label(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getConditionNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:963
      return end_label();
    }
    else {
      return getParent().Define_condition_false_label(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_condition_false_label(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:975
   * @apilevel internal
   */
 @SideEffect.Pure public int Define_condition_true_label(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getConditionNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:964
      return stmt_label();
    }
    else {
      return getParent().Define_condition_true_label(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_condition_true_label(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Pure public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}

/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package AST;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\java.ast:187
 * @production Modifiers : {@link ASTNode} ::= <span class="component">{@link Modifier}*</span>;

 */
public class Modifiers extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:314
   */
  public void checkModifiers() {
    super.checkModifiers();
    if(numProtectionModifiers() > 1)
      error("only one public, protected, private allowed");
    if(numModifier("static") > 1)
      error("only one static allowed");
    // 8.4.3.1
    // 8.4.3.2
    // 8.1.1.2
    if(numCompletenessModifiers() > 1)
      error("only one of final, abstract, volatile allowed");
    if(numModifier("synchronized") > 1)
      error("only one synchronized allowed");
    if(numModifier("transient") > 1)
      error("only one transient allowed");
    if(numModifier("native") > 1)
      error("only one native allowed");
    if(numModifier("strictfp") > 1)
      error("only one strictfp allowed");

    if(isPublic() && !mayBePublic())
      error("modifier public not allowed in this context");
    if(isPrivate() && !mayBePrivate())
      error("modifier private not allowed in this context");
    if(isProtected() && !mayBeProtected())
      error("modifier protected not allowed in this context");
    if(isStatic() && !mayBeStatic())
      error("modifier static not allowed in this context");
    if(isFinal() && !mayBeFinal())
      error("modifier final not allowed in this context");
    if(isAbstract() && !mayBeAbstract())
      error("modifier abstract not allowed in this context");
    if(isVolatile() && !mayBeVolatile())
      error("modifier volatile not allowed in this context");
    if(isTransient() && !mayBeTransient())
      error("modifier transient not allowed in this context");
    if(isStrictfp() && !mayBeStrictfp())
      error("modifier strictfp not allowed in this context");
    if(isSynchronized() && !mayBeSynchronized())
      error("modifier synchronized not allowed in this context");
    if(isNative() && !mayBeNative())
      error("modifier native not allowed in this context");
  }
  /**
   * @aspect PrettyPrint
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\PrettyPrint.jadd:435
   */
  public void toString(StringBuffer s) {
    for(int i = 0; i < getNumModifier(); i++) {
      getModifier(i).toString(s);
      s.append(" ");
    }
  }
  /**
   * @aspect Flags
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Flags.jrag:11
   */
  public static final int ACC_PUBLIC       = 0x0001;
  /**
   * @aspect Flags
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Flags.jrag:12
   */
  public static final int ACC_PRIVATE      = 0x0002;
  /**
   * @aspect Flags
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Flags.jrag:13
   */
  public static final int ACC_PROTECTED    = 0x0004;
  /**
   * @aspect Flags
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Flags.jrag:14
   */
  public static final int ACC_STATIC       = 0x0008;
  /**
   * @aspect Flags
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Flags.jrag:15
   */
  public static final int ACC_FINAL        = 0x0010;
  /**
   * @aspect Flags
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Flags.jrag:16
   */
  public static final int ACC_SYNCHRONIZED = 0x0020;
  /**
   * @aspect Flags
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Flags.jrag:17
   */
  public static final int ACC_SUPER        = 0x0020;
  /**
   * @aspect Flags
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Flags.jrag:18
   */
  public static final int ACC_VOLATILE     = 0x0040;
  /**
   * @aspect Flags
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Flags.jrag:19
   */
  public static final int ACC_TRANSIENT    = 0x0080;
  /**
   * @aspect Flags
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Flags.jrag:20
   */
  public static final int ACC_NATIVE       = 0x0100;
  /**
   * @aspect Flags
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Flags.jrag:21
   */
  public static final int ACC_INTERFACE    = 0x0200;
  /**
   * @aspect Flags
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Flags.jrag:22
   */
  public static final int ACC_ABSTRACT     = 0x0400;
  /**
   * @aspect Flags
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Flags.jrag:23
   */
  public static final int ACC_SYNTHETIC    = 0x1000;
  /**
   * @aspect Flags
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Flags.jrag:24
   */
  public static final int ACC_STRICT       = 0x0800;
  /**
   * @aspect AnnotationsCodegen
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Backend\\AnnotationsCodegen.jrag:39
   */
  public void addRuntimeVisibleAnnotationsAttribute(Collection c) {
    ConstantPool cp = hostType().constantPool();
    Collection annotations = runtimeVisibleAnnotations();
    if(!annotations.isEmpty())
      c.add(new AnnotationsAttribute(cp, annotations, "RuntimeVisibleAnnotations"));
  }
  /**
   * @aspect AnnotationsCodegen
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Backend\\AnnotationsCodegen.jrag:47
   */
  public void addRuntimeInvisibleAnnotationsAttribute(Collection c) {
    ConstantPool cp = hostType().constantPool();
    Collection annotations = runtimeInvisibleAnnotations();
    if(!annotations.isEmpty())
      c.add(new AnnotationsAttribute(cp, annotations, "RuntimeInvisibleAnnotations"));
  }
  /**
   * @aspect AnnotationsCodegen
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Backend\\AnnotationsCodegen.jrag:96
   */
  public Collection runtimeVisibleAnnotations() {
    Collection annotations = new ArrayList();
    for(int i = 0; i < getNumModifier(); i++)
      if(getModifier(i).isRuntimeVisible())
        annotations.add(getModifier(i));
    return annotations;
  }
  /**
   * @aspect AnnotationsCodegen
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Backend\\AnnotationsCodegen.jrag:117
   */
  public Collection runtimeInvisibleAnnotations() {
    Collection annotations = new ArrayList();
    for(int i = 0; i < getNumModifier(); i++)
      if(getModifier(i).isRuntimeInvisible())
        annotations.add(getModifier(i));
    return annotations;
  }
  /**
   * @aspect AnnotationsCodegen
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Backend\\AnnotationsCodegen.jrag:140
   */
  public static final int ACC_ANNOTATION = 0x2000;
  /**
   * @aspect EnumsCodegen
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Backend\\EnumsCodegen.jrag:12
   */
  public static final int ACC_ENUM = 0x4000;
  /**
   * @aspect GenericsCodegen
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Backend\\GenericsCodegen.jrag:218
   */
  public static final int ACC_BRIDGE = 0x0040;
  /**
   * @aspect VariableArityParametersCodegen
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Backend\\VariableArityParametersCodegen.jrag:79
   */
  public static final int ACC_VARARGS = 0x0080;
  /**
   * @declaredat ASTNode:1
   */
  public Modifiers() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
    setChild(new List(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  public Modifiers(List<Modifier> p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:18
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:24
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    isPublic_reset();
    isPrivate_reset();
    isProtected_reset();
    isStatic_reset();
    isFinal_reset();
    isAbstract_reset();
    isVolatile_reset();
    isTransient_reset();
    isStrictfp_reset();
    isSynchronized_reset();
    isNative_reset();
    isSynthetic_reset();
    numModifier_String_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:49
   */
  @SideEffect.Fresh public Modifiers clone() throws CloneNotSupportedException {
    Modifiers node = (Modifiers) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:54
   */
  @SideEffect.Fresh(group="_ASTNode") public Modifiers copy() {
    try {
      Modifiers node = (Modifiers) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:73
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public Modifiers fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:83
   */
  @SideEffect.Fresh(group="_ASTNode") public Modifiers treeCopyNoTransform() {
    Modifiers tree = (Modifiers) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:103
   */
  @SideEffect.Fresh(group="_ASTNode") public Modifiers treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:108
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the Modifier list.
   * @param list The new list node to be used as the Modifier list.
   * @apilevel high-level
   */
  public void setModifierList(List<Modifier> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the Modifier list.
   * @return Number of children in the Modifier list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumModifier() {
    return getModifierList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Modifier list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Modifier list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumModifierNoTransform() {
    return getModifierListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Modifier list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Modifier list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Modifier getModifier(int i) {
    return (Modifier) getModifierList().getChild(i);
  }
  /**
   * Check whether the Modifier list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasModifier() {
    return getModifierList().getNumChild() != 0;
  }
  /**
   * Append an element to the Modifier list.
   * @param node The element to append to the Modifier list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addModifier(Modifier node) {
    List<Modifier> list = (parent == null) ? getModifierListNoTransform() : getModifierList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addModifierNoTransform(Modifier node) {
    List<Modifier> list = getModifierListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Modifier list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setModifier(Modifier node, int i) {
    List<Modifier> list = getModifierList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Modifier list.
   * @return The node representing the Modifier list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Modifier")
  @SideEffect.Pure(group="_ASTNode") public List<Modifier> getModifierList() {
    List<Modifier> list = (List<Modifier>) getChild(0);
    return list;
  }
  /**
   * Retrieves the Modifier list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Modifier list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Modifier> getModifierListNoTransform() {
    return (List<Modifier>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the Modifier list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Modifier getModifierNoTransform(int i) {
    return (Modifier) getModifierListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Modifier list.
   * @return The node representing the Modifier list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Modifier> getModifiers() {
    return getModifierList();
  }
  /**
   * Retrieves the Modifier list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Modifier list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Modifier> getModifiersNoTransform() {
    return getModifierListNoTransform();
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void isPublic_reset() {
    isPublic_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="isPublic") protected boolean isPublic_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="isPublic") protected boolean isPublic_value;

  /**
   * @attribute syn
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:372
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:372")
  @SideEffect.Pure(group="isPublic") public boolean isPublic() {
    ASTState state = state();
    if (isPublic_computed) {
      return isPublic_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    isPublic_value = numModifier("public") != 0;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isPublic_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return isPublic_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void isPrivate_reset() {
    isPrivate_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="isPrivate") protected boolean isPrivate_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="isPrivate") protected boolean isPrivate_value;

  /**
   * @attribute syn
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:373
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:373")
  @SideEffect.Pure(group="isPrivate") public boolean isPrivate() {
    ASTState state = state();
    if (isPrivate_computed) {
      return isPrivate_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    isPrivate_value = numModifier("private") != 0;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isPrivate_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return isPrivate_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void isProtected_reset() {
    isProtected_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="isProtected") protected boolean isProtected_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="isProtected") protected boolean isProtected_value;

  /**
   * @attribute syn
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:374
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:374")
  @SideEffect.Pure(group="isProtected") public boolean isProtected() {
    ASTState state = state();
    if (isProtected_computed) {
      return isProtected_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    isProtected_value = numModifier("protected") != 0;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isProtected_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return isProtected_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void isStatic_reset() {
    isStatic_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="isStatic") protected boolean isStatic_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="isStatic") protected boolean isStatic_value;

  /**
   * @attribute syn
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:375
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:375")
  @SideEffect.Pure(group="isStatic") public boolean isStatic() {
    ASTState state = state();
    if (isStatic_computed) {
      return isStatic_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    isStatic_value = numModifier("static") != 0;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isStatic_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return isStatic_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void isFinal_reset() {
    isFinal_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="isFinal") protected boolean isFinal_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="isFinal") protected boolean isFinal_value;

  /**
   * @attribute syn
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:376
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:376")
  @SideEffect.Pure(group="isFinal") public boolean isFinal() {
    ASTState state = state();
    if (isFinal_computed) {
      return isFinal_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    isFinal_value = numModifier("final") != 0;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isFinal_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return isFinal_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void isAbstract_reset() {
    isAbstract_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="isAbstract") protected boolean isAbstract_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="isAbstract") protected boolean isAbstract_value;

  /**
   * @attribute syn
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:377
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:377")
  @SideEffect.Pure(group="isAbstract") public boolean isAbstract() {
    ASTState state = state();
    if (isAbstract_computed) {
      return isAbstract_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    isAbstract_value = numModifier("abstract") != 0;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isAbstract_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return isAbstract_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void isVolatile_reset() {
    isVolatile_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="isVolatile") protected boolean isVolatile_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="isVolatile") protected boolean isVolatile_value;

  /**
   * @attribute syn
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:378
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:378")
  @SideEffect.Pure(group="isVolatile") public boolean isVolatile() {
    ASTState state = state();
    if (isVolatile_computed) {
      return isVolatile_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    isVolatile_value = numModifier("volatile") != 0;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isVolatile_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return isVolatile_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void isTransient_reset() {
    isTransient_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="isTransient") protected boolean isTransient_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="isTransient") protected boolean isTransient_value;

  /**
   * @attribute syn
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:379
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:379")
  @SideEffect.Pure(group="isTransient") public boolean isTransient() {
    ASTState state = state();
    if (isTransient_computed) {
      return isTransient_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    isTransient_value = numModifier("transient") != 0;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isTransient_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return isTransient_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void isStrictfp_reset() {
    isStrictfp_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="isStrictfp") protected boolean isStrictfp_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="isStrictfp") protected boolean isStrictfp_value;

  /**
   * @attribute syn
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:380
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:380")
  @SideEffect.Pure(group="isStrictfp") public boolean isStrictfp() {
    ASTState state = state();
    if (isStrictfp_computed) {
      return isStrictfp_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    isStrictfp_value = numModifier("strictfp") != 0;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isStrictfp_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return isStrictfp_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void isSynchronized_reset() {
    isSynchronized_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="isSynchronized") protected boolean isSynchronized_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="isSynchronized") protected boolean isSynchronized_value;

  /**
   * @attribute syn
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:381
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:381")
  @SideEffect.Pure(group="isSynchronized") public boolean isSynchronized() {
    ASTState state = state();
    if (isSynchronized_computed) {
      return isSynchronized_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    isSynchronized_value = numModifier("synchronized") != 0;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isSynchronized_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return isSynchronized_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void isNative_reset() {
    isNative_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="isNative") protected boolean isNative_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="isNative") protected boolean isNative_value;

  /**
   * @attribute syn
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:382
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:382")
  @SideEffect.Pure(group="isNative") public boolean isNative() {
    ASTState state = state();
    if (isNative_computed) {
      return isNative_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    isNative_value = numModifier("native") != 0;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isNative_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return isNative_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void isSynthetic_reset() {
    isSynthetic_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="isSynthetic") protected boolean isSynthetic_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="isSynthetic") protected boolean isSynthetic_value;

  /**
   * @attribute syn
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:384
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:384")
  @SideEffect.Pure(group="isSynthetic") public boolean isSynthetic() {
    ASTState state = state();
    if (isSynthetic_computed) {
      return isSynthetic_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    isSynthetic_value = numModifier("synthetic") != 0;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isSynthetic_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return isSynthetic_value;
  }
  /**
   * @attribute syn
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:386
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:386")
  @SideEffect.Pure(group="numProtectionModifiers") public int numProtectionModifiers() {
    int numProtectionModifiers_value = numModifier("public") + numModifier("protected") + numModifier("private");
    return numProtectionModifiers_value;
  }
  /**
   * @attribute syn
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:389
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:389")
  @SideEffect.Pure(group="numCompletenessModifiers") public int numCompletenessModifiers() {
    int numCompletenessModifiers_value = numModifier("abstract") + numModifier("final") + numModifier("volatile");
    return numCompletenessModifiers_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void numModifier_String_reset() {
    numModifier_String_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="numModifier_String") protected java.util.Map numModifier_String_values;

  /**
   * @attribute syn
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:392
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:392")
  @SideEffect.Pure(group="numModifier_String") public int numModifier(String name) {
    Object _parameters = name;
    if (numModifier_String_values == null) numModifier_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (numModifier_String_values.containsKey(_parameters)) {
      return (Integer) numModifier_String_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    int numModifier_String_value = numModifier_compute(name);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    numModifier_String_values.put(_parameters, numModifier_String_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return numModifier_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private int numModifier_compute(String name) {
      int n = 0;
      for(int i = 0; i < getNumModifier(); i++) {
        String s = getModifier(i).getID();
        if(s.equals(name))
          n++;
      }
      return n;
    }
  /**
   * @attribute syn
   * @aspect Annotations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:214
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Annotations", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:214")
  @SideEffect.Pure(group="annotation_TypeDecl") public Annotation annotation(TypeDecl typeDecl) {
    {
        for(int i = 0; i < getNumModifier(); i++) {
          if(getModifier(i) instanceof Annotation) {
            Annotation a = (Annotation)getModifier(i);
            if(a.type() == typeDecl)
              return a;
          }
        }
        return null;
      }
  }
  /**
   * @attribute syn
   * @aspect Annotations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:289
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Annotations", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:289")
  @SideEffect.Pure(group="hasAnnotationSuppressWarnings_String") public boolean hasAnnotationSuppressWarnings(String s) {
    {
        Annotation a = annotation(lookupType("java.lang", "SuppressWarnings"));
        if(a != null && a.getNumElementValuePair() == 1 && a.getElementValuePair(0).getName().equals("value"))
          return a.getElementValuePair(0).getElementValue().hasValue(s);
        return false;
      }
  }
  /**
   * @attribute syn
   * @aspect Annotations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:319
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Annotations", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:319")
  @SideEffect.Pure(group="hasDeprecatedAnnotation") public boolean hasDeprecatedAnnotation() {
    boolean hasDeprecatedAnnotation_value = annotation(lookupType("java.lang", "Deprecated")) != null;
    return hasDeprecatedAnnotation_value;
  }
  /**
   * @return true if the modifier list includes the SafeVarargs annotation
   * @attribute syn
   * @aspect SafeVarargs
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\SafeVarargs.jrag:49
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SafeVarargs", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\SafeVarargs.jrag:49")
  @SideEffect.Pure(group="hasAnnotationSafeVarargs") public boolean hasAnnotationSafeVarargs() {
    boolean hasAnnotationSafeVarargs_value = annotation(lookupType("java.lang", "SafeVarargs")) != null;
    return hasAnnotationSafeVarargs_value;
  }
  /**
   * @attribute inh
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:358
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:358")
  @SideEffect.Pure(group="hostType") public TypeDecl hostType() {
    TypeDecl hostType_value = getParent().Define_hostType(this, null);
    return hostType_value;
  }
  /**
   * @attribute inh
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:360
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:360")
  @SideEffect.Pure(group="mayBePublic") public boolean mayBePublic() {
    boolean mayBePublic_value = getParent().Define_mayBePublic(this, null);
    return mayBePublic_value;
  }
  /**
   * @attribute inh
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:361
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:361")
  @SideEffect.Pure(group="mayBePrivate") public boolean mayBePrivate() {
    boolean mayBePrivate_value = getParent().Define_mayBePrivate(this, null);
    return mayBePrivate_value;
  }
  /**
   * @attribute inh
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:362
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:362")
  @SideEffect.Pure(group="mayBeProtected") public boolean mayBeProtected() {
    boolean mayBeProtected_value = getParent().Define_mayBeProtected(this, null);
    return mayBeProtected_value;
  }
  /**
   * @attribute inh
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:363
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:363")
  @SideEffect.Pure(group="mayBeStatic") public boolean mayBeStatic() {
    boolean mayBeStatic_value = getParent().Define_mayBeStatic(this, null);
    return mayBeStatic_value;
  }
  /**
   * @attribute inh
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:364
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:364")
  @SideEffect.Pure(group="mayBeFinal") public boolean mayBeFinal() {
    boolean mayBeFinal_value = getParent().Define_mayBeFinal(this, null);
    return mayBeFinal_value;
  }
  /**
   * @attribute inh
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:365
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:365")
  @SideEffect.Pure(group="mayBeAbstract") public boolean mayBeAbstract() {
    boolean mayBeAbstract_value = getParent().Define_mayBeAbstract(this, null);
    return mayBeAbstract_value;
  }
  /**
   * @attribute inh
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:366
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:366")
  @SideEffect.Pure(group="mayBeVolatile") public boolean mayBeVolatile() {
    boolean mayBeVolatile_value = getParent().Define_mayBeVolatile(this, null);
    return mayBeVolatile_value;
  }
  /**
   * @attribute inh
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:367
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:367")
  @SideEffect.Pure(group="mayBeTransient") public boolean mayBeTransient() {
    boolean mayBeTransient_value = getParent().Define_mayBeTransient(this, null);
    return mayBeTransient_value;
  }
  /**
   * @attribute inh
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:368
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:368")
  @SideEffect.Pure(group="mayBeStrictfp") public boolean mayBeStrictfp() {
    boolean mayBeStrictfp_value = getParent().Define_mayBeStrictfp(this, null);
    return mayBeStrictfp_value;
  }
  /**
   * @attribute inh
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:369
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:369")
  @SideEffect.Pure(group="mayBeSynchronized") public boolean mayBeSynchronized() {
    boolean mayBeSynchronized_value = getParent().Define_mayBeSynchronized(this, null);
    return mayBeSynchronized_value;
  }
  /**
   * @attribute inh
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:370
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:370")
  @SideEffect.Pure(group="mayBeNative") public boolean mayBeNative() {
    boolean mayBeNative_value = getParent().Define_mayBeNative(this, null);
    return mayBeNative_value;
  }
  /**
   * @attribute inh
   * @aspect Annotations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:56
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="Annotations", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:56")
  @SideEffect.Pure(group="lookupType_String_String") public TypeDecl lookupType(String packageName, String typeName) {
    TypeDecl lookupType_String_String_value = getParent().Define_lookupType(this, null, packageName, typeName);
    return lookupType_String_String_value;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:423
   * @apilevel internal
   */
 @SideEffect.Pure public Annotation Define_lookupAnnotation(ASTNode _callerNode, ASTNode _childNode, TypeDecl typeDecl) {
    if (_callerNode == getModifierListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:424
      int index = _callerNode.getIndexOfChild(_childNode);
      {
          return annotation(typeDecl);
        }
    }
    else {
      return getParent().Define_lookupAnnotation(this, _callerNode, typeDecl);
    }
  }
  @SideEffect.Pure protected boolean canDefine_lookupAnnotation(ASTNode _callerNode, ASTNode _childNode, TypeDecl typeDecl) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Pure public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}

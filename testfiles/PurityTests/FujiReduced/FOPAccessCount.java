package AST;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast class
 * @aspect FOPAccessCount
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\src\\FOPCounter.jrag:13
 */
 class FOPAccessCount extends java.lang.Object {
  

        // Order of the OOP modifiers.
        private static java.util.ArrayList<String> oopModOrder
            = new ArrayList<String>();

  

        // Order of the FOP modifiers.
        private static java.util.ArrayList<String> fopModOrder
            = new ArrayList<String>();

  

        static {
            oopModOrder.add(0,"private");
            oopModOrder.add(1,"package");
            oopModOrder.add(2,"protected");
            oopModOrder.add(3,"public");
            fopModOrder.add(0,"feature");
            fopModOrder.add(1,"subsequent");
            fopModOrder.add(2,"program");
        }

  

        private boolean isMethod = true;

  
        private String memberQName = null;

  
        private String origModifier = null;

  
        private String minOOPMod = "private";

  
        private String minFOPMod = "feature";

  
        private java.util.HashMap<String,Integer> accessCounter = new java.util.HashMap<String, Integer>();

  

        public FOPAccessCount(String memberQName, String origModifier) {
            this(true, memberQName, origModifier);
        }

  

        public FOPAccessCount(boolean isMethod, String memberQName,
                              String origModifier) {

            this.isMethod = isMethod;
            this.memberQName = memberQName;
            this.origModifier = origModifier;
            for (String oop : FOPAccessCount.oopModOrder) {
                for (String fop : FOPAccessCount.fopModOrder) {
                    accessCounter.put(oop+" "+fop, 0);
                }
            }
        }

  

        public boolean isMethod() {
            return isMethod;
        }

  

        public String getMemberQName() {
            return memberQName;
        }

  

        public String getMemberOrigModifier() {
            return origModifier;
        }

  

        public void addAccess(String oopModifier, String fopModifier) {
            String oopfop = (oopModifier + " " +fopModifier);
            int n = accessCounter.get(oopfop);
            accessCounter.put(oopfop, ++n);

            minOOPMod = calcMinOOPMod(oopModifier, minOOPMod);
            minFOPMod = calcMinFOPMod(fopModifier, minFOPMod);
        }

  

        public void addAllAccess(FOPAccessCount fopAC) {
            Map<String, Integer> aCounter = fopAC.getAccessCount();
            for (String key : aCounter.keySet()) {
                Integer n = accessCounter.get(key) + aCounter.get(key);
                accessCounter.put(key, n);
            }
            minOOPMod = calcMinOOPMod(minOOPMod, fopAC.getMinOOPModifier());
            minFOPMod = calcMinFOPMod(minFOPMod, fopAC.getMinFOPModifier());
        }

  

        private String calcMinOOPMod(String oop1, String oop2) {
            if (FOPAccessCount.oopModOrder.indexOf(oop1)
                < FOPAccessCount.oopModOrder.indexOf(oop2)) {
                return oop2;
            }
            return oop1;
        }

  

        private String calcMinFOPMod(String fop1, String fop2) {
            if (FOPAccessCount.fopModOrder.indexOf(fop1)
                < FOPAccessCount.fopModOrder.indexOf(fop2)) {
                return fop2;
            }
            return fop1;
        }

  

        public String getMinFOPModifier() {
            return minFOPMod;
        }

  

        public String getMinOOPModifier() {
            return minOOPMod;
        }

  

        /**
         * Get the count of all the accesses to this field aggregated by the
         * required OOP-FOP modifier combination.
         * e.g. public program - 10 access, ...
         */
        public java.util.Map<String,Integer> getAccessCount() {
            return new java.util.TreeMap<String,Integer>(accessCounter);
        }

  

        public String toString() {
            return accessCounter.toString();
        }


}

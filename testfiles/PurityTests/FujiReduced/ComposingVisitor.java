package AST;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast interface
 * @aspect ComposingVisitorInterface
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\src\\ComposingVisitors.jrag:14
 */
public interface ComposingVisitor {

         
        String ORIGINAL_OPERATOR = "original";

         
        String ORIGINAL_DELIM = "_$_";

         
        boolean visit(CompilationUnit baseCU, CompilationUnit featCU);

         
        boolean visit(TypeDecl baseTD, TypeDecl featTD);

         
        boolean visit(InterfaceDecl baseND, InterfaceDecl featND);

         
        boolean visit(ClassDecl baseCD, ClassDecl featCD);

         
        boolean visit(MethodDecl baseMD, MethodDecl featMD);
}

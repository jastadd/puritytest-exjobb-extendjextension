package AST;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast class
 * @aspect AccessAnalyzer
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\src\\FOPCounter.jrag:192
 */
 class AccessAnalyzer extends java.lang.Object {
  
        public static final String MODSTAT_OPT = "-fopStatistic";

  

        private static AccessAnalyzer instance = null;

  

        /* Keeps the last 3 accesse allows to filter out duplicate accesses. */
        private LinkedList<AccessPair> lastAccesses
            = new LinkedList<AccessPair>();

  

        static AccessAnalyzer instance() {
            if (instance == null)
                instance = new AccessAnalyzer();
            return instance;
        }

  

        public void analyze(BodyDecl target, Expr source) {

            // Duplicates avoidance
            AccessPair ap = new AccessPair(source, target);
            if (lastAccesses.contains(ap))
                return;
            if (lastAccesses.size() == 3)
                lastAccesses.remove();
            lastAccesses.add(ap);

            FieldDeclaration fd = target instanceof FieldDeclaration
                ? (FieldDeclaration) target : null;
            MethodDecl ma = target instanceof MethodDecl
                ? (MethodDecl) target : null;

            String targetID;
            if (fd != null) {
                targetID = fd.getID();
            } else if (ma != null) {
                targetID = ma.signature();
            } else {
                //TODO exception error
                return;
            }

            String accessModifier = null;
            if (fd != null && fd.isPrivate()
                || ma != null && ma.isPrivate()) {
                accessModifier = "private";
            } else if (fd != null && fd.isPublic()
                       || ma != null && ma.isPublic()) {
                accessModifier = "public";
            } else if (fd != null && fd.isProtected()
                       || ma != null && ma.isProtected()) {
                accessModifier = "protected";
            } else {
                accessModifier = "package";
            }

            String tQName = target.featureID()
                + ":" + target.hostPackage()
                + "." + target.hostType().getID()
                + "." + targetID;
            String sQName = source.featureID()
                + ":" + source.hostPackage()
                + "." + source.hostType().getID();


            String minOOPModifier = null;
            // The Logic is from Java1_4Frontend/LookupVariable.jrag
            if (target.hostType().topLevelType() ==
                source.hostType().topLevelType()) {
                minOOPModifier = "private";
            } else if (target.hostPackage()
                       .equals(source.hostType().hostPackage())) {
                minOOPModifier = "package";
            }
            if (minOOPModifier == null &&
                source.hostType().subclassWithinBody(target.hostType())
                != null) {
                minOOPModifier = "protected";
            } else if (minOOPModifier == null) {
                minOOPModifier = "public";
            }

            String minFOPModifier = null;
            if (source.featureID() != -1 && target.featureID() != -1) {

                if (source.featureID() == target.featureID()) {
                    minFOPModifier = "feature";
                } else if (source.featureID() >= target.featureID()) {
                    minFOPModifier = "subsequent";
                } else {
                    minFOPModifier = "program";
                }

                FOPAccessCount fopAC =
                    (FOPAccessCount) target.getFOPAccs().get(tQName);
                boolean isMethod = ma != null;
                if (fopAC == null) {
                    fopAC = new FOPAccessCount(isMethod,
                                               tQName, accessModifier);
                    target.getFOPAccs().put(tQName, fopAC);
                }
                fopAC.addAccess(minOOPModifier, minFOPModifier);

                if (System.getProperty("debug") != null) {
                    System.out.println("* " + sQName
                                       //+ "(" + source.enclosingStmt() + ")"
                                       + " -> " + tQName
                                       + "; " + accessModifier
                                       + "; " + minOOPModifier
                                       + " " + minFOPModifier + ";");
                }
            }
        }

  

        private class AccessPair {
            Object source = null;
            Object target = null;
            public AccessPair(Object source, Object target) {
                this.source = source;
                this.target = target;
            }
            public boolean equals(Object obj) {
                if ( ! (obj instanceof AccessPair))
                    return false;
                AccessPair ap = (AccessPair) obj;
                return (ap.source == source && ap.target == target);
            }
        }


}

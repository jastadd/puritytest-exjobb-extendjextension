/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package AST;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.ast:2
 * @production AnnotationDecl : {@link InterfaceDecl} ::= <span class="component">SuperInterfaceId:{@link Access}*</span>;

 */
public class AnnotationDecl extends InterfaceDecl implements Cloneable {
  /**
   * @aspect Annotations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:103
   */
  public void typeCheck() {
    super.typeCheck();
    for(int i = 0; i < getNumBodyDecl(); i++) {
      if(getBodyDecl(i) instanceof MethodDecl) {
        MethodDecl m = (MethodDecl)getBodyDecl(i);
        if(!m.type().isValidAnnotationMethodReturnType())
          m.error("invalid type for annotation member");
        if(m.annotationMethodOverride())
          m.error("annotation method overrides " + m.signature());
      }
    }
    if(containsElementOf(this))
      error("cyclic annotation element type");
  }
  /**
   * @aspect Annotations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:558
   */
  public void toString(StringBuffer s) {
    getModifiers().toString(s);
    s.append("@interface " + name());
    s.append(" {");
    for(int i=0; i < getNumBodyDecl(); i++) {
      getBodyDecl(i).toString(s);
    }
    s.append(indent() + "}");
  }
  /**
   * @declaredat ASTNode:1
   */
  public AnnotationDecl() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[3];
    setChild(new List(), 1);
    setChild(new List(), 2);
  }
  /**
   * @declaredat ASTNode:15
   */
  public AnnotationDecl(Modifiers p0, String p1, List<BodyDecl> p2) {
    setChild(p0, 0);
    setID(p1);
    setChild(p2, 1);
  }
  /**
   * @declaredat ASTNode:20
   */
  public AnnotationDecl(Modifiers p0, beaver.Symbol p1, List<BodyDecl> p2) {
    setChild(p0, 0);
    setID(p1);
    setChild(p2, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:26
   */
  @SideEffect.Pure protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:32
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:36
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    getSuperInterfaceIdList_reset();
    containsElementOf_TypeDecl_reset();
    flags_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:47
   */
  @SideEffect.Fresh public AnnotationDecl clone() throws CloneNotSupportedException {
    AnnotationDecl node = (AnnotationDecl) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:52
   */
  @SideEffect.Fresh(group="_ASTNode") public AnnotationDecl copy() {
    try {
      AnnotationDecl node = (AnnotationDecl) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:71
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public AnnotationDecl fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:81
   */
  @SideEffect.Fresh(group="_ASTNode") public AnnotationDecl treeCopyNoTransform() {
    AnnotationDecl tree = (AnnotationDecl) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        switch (i) {
        case 2:
          tree.children[i] = new List();
          continue;
        }
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:106
   */
  @SideEffect.Fresh(group="_ASTNode") public AnnotationDecl treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:111
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_ID == ((AnnotationDecl) node).tokenString_ID);    
  }
  /**
   * Replaces the Modifiers child.
   * @param node The new node to replace the Modifiers child.
   * @apilevel high-level
   */
  public void setModifiers(Modifiers node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Modifiers child.
   * @return The current node used as the Modifiers child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Modifiers")
  @SideEffect.Pure public Modifiers getModifiers() {
    return (Modifiers) getChild(0);
  }
  /**
   * Retrieves the Modifiers child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Modifiers child.
   * @apilevel low-level
   */
  public Modifiers getModifiersNoTransform() {
    return (Modifiers) getChildNoTransform(0);
  }
  /**
   * Replaces the lexeme ID.
   * @param value The new value for the lexeme ID.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setID(String value) {
    tokenString_ID = value;
  }
  /**
   * JastAdd-internal setter for lexeme ID using the Beaver parser.
   * @param symbol Symbol containing the new value for the lexeme ID
   * @apilevel internal
   */
  public void setID(beaver.Symbol symbol) {
    if (symbol.value != null && !(symbol.value instanceof String))
    throw new UnsupportedOperationException("setID is only valid for String lexemes");
    tokenString_ID = (String)symbol.value;
    IDstart = symbol.getStart();
    IDend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme ID.
   * @return The value for the lexeme ID.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="ID")
  @SideEffect.Pure(group="_ASTNode") public String getID() {
    return tokenString_ID != null ? tokenString_ID : "";
  }
  /**
   * Replaces the BodyDecl list.
   * @param list The new list node to be used as the BodyDecl list.
   * @apilevel high-level
   */
  public void setBodyDeclList(List<BodyDecl> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the BodyDecl list.
   * @return Number of children in the BodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumBodyDecl() {
    return getBodyDeclList().getNumChild();
  }
  /**
   * Retrieves the number of children in the BodyDecl list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the BodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumBodyDeclNoTransform() {
    return getBodyDeclListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the BodyDecl list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the BodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public BodyDecl getBodyDecl(int i) {
    return (BodyDecl) getBodyDeclList().getChild(i);
  }
  /**
   * Check whether the BodyDecl list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasBodyDecl() {
    return getBodyDeclList().getNumChild() != 0;
  }
  /**
   * Append an element to the BodyDecl list.
   * @param node The element to append to the BodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addBodyDecl(BodyDecl node) {
    List<BodyDecl> list = (parent == null) ? getBodyDeclListNoTransform() : getBodyDeclList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addBodyDeclNoTransform(BodyDecl node) {
    List<BodyDecl> list = getBodyDeclListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the BodyDecl list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setBodyDecl(BodyDecl node, int i) {
    List<BodyDecl> list = getBodyDeclList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the BodyDecl list.
   * @return The node representing the BodyDecl list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="BodyDecl")
  @SideEffect.Pure(group="_ASTNode") public List<BodyDecl> getBodyDeclList() {
    List<BodyDecl> list = (List<BodyDecl>) getChild(1);
    return list;
  }
  /**
   * Retrieves the BodyDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the BodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<BodyDecl> getBodyDeclListNoTransform() {
    return (List<BodyDecl>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the BodyDecl list without
   * triggering rewrites.
   */
  @SideEffect.Pure public BodyDecl getBodyDeclNoTransform(int i) {
    return (BodyDecl) getBodyDeclListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the BodyDecl list.
   * @return The node representing the BodyDecl list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<BodyDecl> getBodyDecls() {
    return getBodyDeclList();
  }
  /**
   * Retrieves the BodyDecl list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the BodyDecl list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<BodyDecl> getBodyDeclsNoTransform() {
    return getBodyDeclListNoTransform();
  }
  /**
   * This method should not be called. This method throws an exception due to
   * the corresponding child being an NTA shadowing a non-NTA child.
   * @param node
   * @apilevel internal
   */
  public void setSuperInterfaceIdList(List<Access> node) {
    throw new Error("Can not replace NTA child SuperInterfaceIdList in AnnotationDecl!");
  }
  /**
   * Retrieves the number of children in the SuperInterfaceId list.
   * @return Number of children in the SuperInterfaceId list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumSuperInterfaceId() {
    return getSuperInterfaceIdList().getNumChild();
  }
  /**
   * Retrieves the number of children in the SuperInterfaceId list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the SuperInterfaceId list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumSuperInterfaceIdNoTransform() {
    return getSuperInterfaceIdListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the SuperInterfaceId list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the SuperInterfaceId list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Access getSuperInterfaceId(int i) {
    return (Access) getSuperInterfaceIdList().getChild(i);
  }
  /**
   * Check whether the SuperInterfaceId list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasSuperInterfaceId() {
    return getSuperInterfaceIdList().getNumChild() != 0;
  }
  /**
   * Append an element to the SuperInterfaceId list.
   * @param node The element to append to the SuperInterfaceId list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addSuperInterfaceId(Access node) {
    List<Access> list = (parent == null) ? getSuperInterfaceIdListNoTransform() : getSuperInterfaceIdList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addSuperInterfaceIdNoTransform(Access node) {
    List<Access> list = getSuperInterfaceIdListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the SuperInterfaceId list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setSuperInterfaceId(Access node, int i) {
    List<Access> list = getSuperInterfaceIdList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the child position of the SuperInterfaceId list.
   * @return The the child position of the SuperInterfaceId list.
   * @apilevel low-level
   */
  @SideEffect.Pure protected int getSuperInterfaceIdListChildPosition() {
    return 2;
  }
  /**
   * Retrieves the SuperInterfaceId list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SuperInterfaceId list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Access> getSuperInterfaceIdListNoTransform() {
    return (List<Access>) getChildNoTransform(2);
  }
  /**
   * @return the element at index {@code i} in the SuperInterfaceId list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Access getSuperInterfaceIdNoTransform(int i) {
    return (Access) getSuperInterfaceIdListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the SuperInterfaceId list.
   * @return The node representing the SuperInterfaceId list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Access> getSuperInterfaceIds() {
    return getSuperInterfaceIdList();
  }
  /**
   * Retrieves the SuperInterfaceId list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the SuperInterfaceId list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Access> getSuperInterfaceIdsNoTransform() {
    return getSuperInterfaceIdListNoTransform();
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void getSuperInterfaceIdList_reset() {
    getSuperInterfaceIdList_computed = false;
    
    getSuperInterfaceIdList_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="getSuperInterfaceIdList") protected boolean getSuperInterfaceIdList_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="getSuperInterfaceIdList") protected List getSuperInterfaceIdList_value;

  /**
   * @attribute syn nta
   * @aspect Annotations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:99
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isNTA=true)
  @ASTNodeAnnotation.Source(aspect="Annotations", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:99")
  @SideEffect.Pure(group="getSuperInterfaceIdList") public List getSuperInterfaceIdList() {
    ASTState state = state();
    if (getSuperInterfaceIdList_computed) {
      return (List) getChild(getSuperInterfaceIdListChildPosition());
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    getSuperInterfaceIdList_value = getSuperInterfaceIdList_compute();
    setChild(getSuperInterfaceIdList_value, getSuperInterfaceIdListChildPosition());
    if (isFinal && _boundaries == state().boundariesCrossed) {
    getSuperInterfaceIdList_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    List node = (List) this.getChild(getSuperInterfaceIdListChildPosition());
    return node;
  }
  /** @apilevel internal */
  @SideEffect.Pure private List getSuperInterfaceIdList_compute() {
      return new List().add(new TypeAccess("java.lang.annotation", "Annotation"));
    }
  /**
   * @attribute syn
   * @aspect Annotations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:121
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Annotations", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:121")
  @SideEffect.Pure(group="isValidAnnotationMethodReturnType") public boolean isValidAnnotationMethodReturnType() {
    boolean isValidAnnotationMethodReturnType_value = true;
    return isValidAnnotationMethodReturnType_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void containsElementOf_TypeDecl_reset() {
    containsElementOf_TypeDecl_values = null;
  }
  protected java.util.Map containsElementOf_TypeDecl_values;
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN, isCircular=true)
  @ASTNodeAnnotation.Source(aspect="Annotations", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:144")
  @SideEffect.Pure(group="containsElementOf_TypeDecl") public boolean containsElementOf(TypeDecl typeDecl) {
    Object _parameters = typeDecl;
    if (containsElementOf_TypeDecl_values == null) containsElementOf_TypeDecl_values = new java.util.HashMap(4);
    ASTState.CircularValue _value;
    if (containsElementOf_TypeDecl_values.containsKey(_parameters)) {
      Object _cache = containsElementOf_TypeDecl_values.get(_parameters);
      if (!(_cache instanceof ASTState.CircularValue)) {
        return (Boolean) _cache;
      } else {
        _value = (ASTState.CircularValue) _cache;
      }
    } else {
      _value = new ASTState.CircularValue();
      containsElementOf_TypeDecl_values.put(_parameters, _value);
      _value.value = false;
    }
    ASTState state = state();
    if (!state.inCircle() || state.calledByLazyAttribute()) {
      state.enterCircle();
      int _boundaries = state.boundariesCrossed;
      boolean isFinal = this.is$Final();
      boolean new_containsElementOf_TypeDecl_value;
      do {
        _value.cycle = state.nextCycle();
        new_containsElementOf_TypeDecl_value = containsElementOf_compute(typeDecl);
        if (new_containsElementOf_TypeDecl_value != ((Boolean)_value.value)) {
          state.setChangeInCycle();
          _value.value = new_containsElementOf_TypeDecl_value;
        }
      } while (state.testAndClearChangeInCycle());
      if (isFinal && _boundaries == state().boundariesCrossed) {
        containsElementOf_TypeDecl_values.put(_parameters, new_containsElementOf_TypeDecl_value);
      } else {
        containsElementOf_TypeDecl_values.remove(_parameters);
        state.startResetCycle();
        boolean $tmp = containsElementOf_compute(typeDecl);
      }
      state.leaveCircle();
      return new_containsElementOf_TypeDecl_value;
    } else if (_value.cycle != state.cycle()) {
      _value.cycle = state.cycle();
      boolean new_containsElementOf_TypeDecl_value = containsElementOf_compute(typeDecl);
      if (state.resetCycle()) {
        containsElementOf_TypeDecl_values.remove(_parameters);
      }
      else if (new_containsElementOf_TypeDecl_value != ((Boolean)_value.value)) {
        state.setChangeInCycle();
        _value.value = new_containsElementOf_TypeDecl_value;
      }
      return new_containsElementOf_TypeDecl_value;
    } else {
      return (Boolean) _value.value;
    }
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean containsElementOf_compute(TypeDecl typeDecl) {
      for(int i = 0; i < getNumBodyDecl(); i++) {
        if(getBodyDecl(i) instanceof MethodDecl) {
          MethodDecl m = (MethodDecl)getBodyDecl(i);
          if(m.type() == typeDecl)
            return true;
          if(m.type() instanceof AnnotationDecl && ((AnnotationDecl)m.type()).containsElementOf(typeDecl))
            return true;
        }
      }
      return false;
    }
  /**
   * @attribute syn
   * @aspect Annotations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:541
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Annotations", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:541")
  @SideEffect.Pure(group="isAnnotationDecl") public boolean isAnnotationDecl() {
    boolean isAnnotationDecl_value = true;
    return isAnnotationDecl_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void flags_reset() {
    flags_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="flags") protected boolean flags_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="flags") protected int flags_value;

  /**
   * @attribute syn
   * @aspect Flags
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Flags.jrag:64
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Flags", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Flags.jrag:64")
  @SideEffect.Pure(group="flags") public int flags() {
    ASTState state = state();
    if (flags_computed) {
      return flags_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    flags_value = super.flags() | Modifiers.ACC_ANNOTATION;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    flags_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return flags_value;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:69
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_mayUseAnnotationTarget(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getModifiersNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:77
      return name.equals("ANNOTATION_TYPE") || name.equals("TYPE");
    }
    else {
      return super.Define_mayUseAnnotationTarget(_callerNode, _childNode, name);
    }
  }
  @SideEffect.Pure protected boolean canDefine_mayUseAnnotationTarget(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Pure public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}

/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package AST;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\java.ast:90
 * @production ArrayInit : {@link Expr} ::= <span class="component">Init:{@link Expr}*</span>;

 */
public class ArrayInit extends Expr implements Cloneable {
  /**
   * @aspect PrettyPrint
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\PrettyPrint.jadd:221
   */
  public void toString(StringBuffer s) {
    s.append("{ ");
    if(getNumInit() > 0) {
      getInit(0).toString(s);
      for(int i = 1; i < getNumInit(); i++) {
        s.append(", ");
        getInit(i).toString(s);
      }
    }
    s.append(" } ");
  }
  /**
   * @aspect TypeCheck
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeCheck.jrag:144
   */
  public void typeCheck() {
    TypeDecl initializerType = declType().componentType();
    if(initializerType.isUnknown())
      error("the dimension of the initializer is larger than the expected dimension");
    for(int i = 0; i < getNumInit(); i++) {
      Expr e = getInit(i);
      if(!e.type().assignConversionTo(initializerType, e))
        error("the type " + e.type().name() + " of the initializer is not compatible with " + initializerType.name()); 
    }
  }
  /**
   * @declaredat ASTNode:1
   */
  public ArrayInit() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[1];
    setChild(new List(), 0);
  }
  /**
   * @declaredat ASTNode:14
   */
  public ArrayInit(List<Expr> p0) {
    setChild(p0, 0);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:18
   */
  @SideEffect.Pure protected int numChildren() {
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:24
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    computeDABefore_int_Variable_reset();
    computeDUbefore_int_Variable_reset();
    type_reset();
    declType_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:36
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  @SideEffect.Fresh public ArrayInit clone() throws CloneNotSupportedException {
    ArrayInit node = (ArrayInit) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  @SideEffect.Fresh(group="_ASTNode") public ArrayInit copy() {
    try {
      ArrayInit node = (ArrayInit) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:64
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public ArrayInit fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:74
   */
  @SideEffect.Fresh(group="_ASTNode") public ArrayInit treeCopyNoTransform() {
    ArrayInit tree = (ArrayInit) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:94
   */
  @SideEffect.Fresh(group="_ASTNode") public ArrayInit treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:99
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the Init list.
   * @param list The new list node to be used as the Init list.
   * @apilevel high-level
   */
  public void setInitList(List<Expr> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the Init list.
   * @return Number of children in the Init list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumInit() {
    return getInitList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Init list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Init list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumInitNoTransform() {
    return getInitListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Init list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Init list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Expr getInit(int i) {
    return (Expr) getInitList().getChild(i);
  }
  /**
   * Check whether the Init list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasInit() {
    return getInitList().getNumChild() != 0;
  }
  /**
   * Append an element to the Init list.
   * @param node The element to append to the Init list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addInit(Expr node) {
    List<Expr> list = (parent == null) ? getInitListNoTransform() : getInitList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addInitNoTransform(Expr node) {
    List<Expr> list = getInitListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Init list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setInit(Expr node, int i) {
    List<Expr> list = getInitList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Init list.
   * @return The node representing the Init list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Init")
  @SideEffect.Pure(group="_ASTNode") public List<Expr> getInitList() {
    List<Expr> list = (List<Expr>) getChild(0);
    return list;
  }
  /**
   * Retrieves the Init list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Init list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Expr> getInitListNoTransform() {
    return (List<Expr>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the Init list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Expr getInitNoTransform(int i) {
    return (Expr) getInitListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Init list.
   * @return The node representing the Init list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Expr> getInits() {
    return getInitList();
  }
  /**
   * Retrieves the Init list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Init list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Expr> getInitsNoTransform() {
    return getInitListNoTransform();
  }
  /**
   * @aspect AutoBoxingCodegen
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Backend\\AutoBoxingCodegen.jrag:266
   */
    public void createBCode(CodeGeneration gen) {
    IntegerLiteral.push(gen, getNumInit());
    if(type().componentType().isPrimitive() && !type().componentType().isReferenceType()) {
      gen.emit(Bytecode.NEWARRAY).add(type().componentType().arrayPrimitiveTypeDescriptor());
    } 
    else {
      String n = type().componentType().arrayTypeDescriptor();
      int index = gen.constantPool().addClass(n);
      gen.emit(Bytecode.ANEWARRAY).add2(index);
    }
    for(int i = 0; i < getNumInit(); i++) {
      gen.emitDup();
      IntegerLiteral.push(gen, i);
      getInit(i).createBCode(gen);
      if(getInit(i) instanceof ArrayInit)
        gen.emit(Bytecode.AASTORE);
      else {
        getInit(i).type().emitAssignConvTo(gen, expectedType()); // AssignConversion
        gen.emit(expectedType().arrayStore());
      }
    }
  }
  /**
   * @attribute syn
   * @aspect ConstantExpression
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:308
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantExpression", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\ConstantExpression.jrag:308")
  @SideEffect.Pure(group="representableIn_TypeDecl") public boolean representableIn(TypeDecl t) {
    {
        for(int i = 0; i < getNumInit(); i++)
          if(!getInit(i).representableIn(t))
            return false;
        return true;
      }
  }
  /**
   * @attribute syn
   * @aspect DA
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:235
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="DA", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:235")
  @SideEffect.Pure(group="isDAafter_Variable") public boolean isDAafter(Variable v) {
    boolean isDAafter_Variable_value = getNumInit() == 0 ? isDAbefore(v) : getInit(getNumInit()-1).isDAafter(v);
    return isDAafter_Variable_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void computeDABefore_int_Variable_reset() {
    computeDABefore_int_Variable_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="computeDABefore_int_Variable") protected java.util.Map computeDABefore_int_Variable_values;

  /**
   * @attribute syn
   * @aspect DA
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:501
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="DA", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:501")
  @SideEffect.Pure(group="computeDABefore_int_Variable") public boolean computeDABefore(int childIndex, Variable v) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(childIndex);
    _parameters.add(v);
    if (computeDABefore_int_Variable_values == null) computeDABefore_int_Variable_values = new java.util.HashMap(4);
    ASTState state = state();
    if (computeDABefore_int_Variable_values.containsKey(_parameters)) {
      return (Boolean) computeDABefore_int_Variable_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boolean computeDABefore_int_Variable_value = computeDABefore_compute(childIndex, v);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    computeDABefore_int_Variable_values.put(_parameters, computeDABefore_int_Variable_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return computeDABefore_int_Variable_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean computeDABefore_compute(int childIndex, Variable v) {
      if(childIndex == 0) return isDAbefore(v);
      int index = childIndex-1;
      while(index > 0 && getInit(index).isConstant())
        index--;
      return getInit(childIndex-1).isDAafter(v);
    }
  /**
   * @attribute syn
   * @aspect DU
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:693
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="DU", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:693")
  @SideEffect.Pure(group="isDUafter_Variable") public boolean isDUafter(Variable v) {
    boolean isDUafter_Variable_value = getNumInit() == 0 ? isDUbefore(v) : getInit(getNumInit()-1).isDUafter(v);
    return isDUafter_Variable_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void computeDUbefore_int_Variable_reset() {
    computeDUbefore_int_Variable_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="computeDUbefore_int_Variable") protected java.util.Map computeDUbefore_int_Variable_values;

  /**
   * @attribute syn
   * @aspect DU
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:884
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="DU", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:884")
  @SideEffect.Pure(group="computeDUbefore_int_Variable") public boolean computeDUbefore(int childIndex, Variable v) {
    java.util.List _parameters = new java.util.ArrayList(2);
    _parameters.add(childIndex);
    _parameters.add(v);
    if (computeDUbefore_int_Variable_values == null) computeDUbefore_int_Variable_values = new java.util.HashMap(4);
    ASTState state = state();
    if (computeDUbefore_int_Variable_values.containsKey(_parameters)) {
      return (Boolean) computeDUbefore_int_Variable_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boolean computeDUbefore_int_Variable_value = computeDUbefore_compute(childIndex, v);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    computeDUbefore_int_Variable_values.put(_parameters, computeDUbefore_int_Variable_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return computeDUbefore_int_Variable_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean computeDUbefore_compute(int childIndex, Variable v) {
      if(childIndex == 0) return isDUbefore(v);
      int index = childIndex-1;
      while(index > 0 && getInit(index).isConstant())
        index--;
      return getInit(childIndex-1).isDUafter(v);
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void type_reset() {
    type_computed = false;
    
    type_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="type") protected boolean type_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="type") protected TypeDecl type_value;

  /**
   * @attribute syn
   * @aspect TypeAnalysis
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:276
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:276")
  @SideEffect.Pure(group="type") public TypeDecl type() {
    ASTState state = state();
    if (type_computed) {
      return type_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    type_value = declType();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    type_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return type_value;
  }
  /**
   * @attribute inh
   * @aspect TypeAnalysis
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:255
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:255")
  @SideEffect.Pure(group="declType") public TypeDecl declType() {
    ASTState state = state();
    if (declType_computed) {
      return declType_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    declType_value = getParent().Define_declType(this, null);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    declType_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return declType_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void declType_reset() {
    declType_computed = false;
    
    declType_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="declType") protected boolean declType_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="declType") protected TypeDecl declType_value;

  /**
   * @attribute inh
   * @aspect InnerClasses
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\InnerClasses.jrag:61
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="InnerClasses", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\InnerClasses.jrag:61")
  @SideEffect.Pure(group="expectedType") public TypeDecl expectedType() {
    TypeDecl expectedType_value = getParent().Define_expectedType(this, null);
    return expectedType_value;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:25
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isSource(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getInitListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:42
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return true;
    }
    else {
      return getParent().Define_isSource(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_isSource(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:232
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isDAbefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    if (_callerNode == getInitListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:499
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return computeDABefore(childIndex, v);
    }
    else {
      return getParent().Define_isDAbefore(this, _callerNode, v);
    }
  }
  @SideEffect.Pure protected boolean canDefine_isDAbefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:690
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isDUbefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    if (_callerNode == getInitListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:882
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return computeDUbefore(childIndex, v);
    }
    else {
      return getParent().Define_isDUbefore(this, _callerNode, v);
    }
  }
  @SideEffect.Pure protected boolean canDefine_isDUbefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:255
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_declType(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getInitListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:263
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return declType().componentType();
    }
    else {
      return getParent().Define_declType(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_declType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\InnerClasses.jrag:61
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_expectedType(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getInitListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\InnerClasses.jrag:67
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return expectedType().componentType();
    }
    else {
      return getParent().Define_expectedType(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_expectedType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericMethodsInference.jrag:33
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_assignConvertedType(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getInitListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericMethodsInference.jrag:37
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return declType().componentType();
    }
    else {
      return getParent().Define_assignConvertedType(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_assignConvertedType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Pure public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}

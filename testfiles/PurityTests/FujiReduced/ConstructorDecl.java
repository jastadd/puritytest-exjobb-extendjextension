/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package AST;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\java.ast:72
 * @production ConstructorDecl : {@link BodyDecl} ::= <span class="component">{@link Modifiers}</span> <span class="component">&lt;ID:String&gt;</span> <span class="component">Parameter:{@link ParameterDeclaration}*</span> <span class="component">Exception:{@link Access}*</span> <span class="component">[ConstructorInvocation:{@link Stmt}]</span> <span class="component">{@link Block}</span>;

 */
public class ConstructorDecl extends BodyDecl implements Cloneable, ExceptionHolder {
  /**
   * @aspect ConstructorDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupConstructor.jrag:144
   */
  public boolean applicable(List argList) {
    if(getNumParameter() != argList.getNumChild())
      return false;
    for(int i = 0; i < getNumParameter(); i++) {
      TypeDecl arg = ((Expr)argList.getChild(i)).type();
      TypeDecl parameter = getParameter(i).type();
      if(!arg.instanceOf(parameter)) {
        return false;
      }  
    }
    return true;
  }
  /**
   * Flag to indicate if this constructor is an auto-generated
   * default constructor. Default constructors are not pretty
   * printed.
   * @aspect ImplicitConstructor
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupConstructor.jrag:171
   */
  private boolean isDefaultConstructor = false;
  /**
   * Set the default constructor flag. Causes this constructor
   * to not be pretty printed.
   * @aspect ImplicitConstructor
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupConstructor.jrag:176
   */
  public void setDefaultConstructor() {
	  isDefaultConstructor = true;
  }
  /**
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:110
   */
  public void checkModifiers() {
    super.checkModifiers();
  }
  /**
   * @aspect NameCheck
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\NameCheck.jrag:68
   */
  public void nameCheck() {
    super.nameCheck();
    // 8.8
    if(!hostType().name().equals(name()))
      error("constructor " + name() +" does not have the same name as the simple name of the host class " + hostType().name());
    
    // 8.8.2
    if(hostType().lookupConstructor(this) != this)
      error("constructor with signature " + signature() + " is multiply declared in type " + hostType().typeName());

    if(circularThisInvocation(this))
      error("The constructor " + signature() + " may not directly or indirectly invoke itself");
  }
  /**
   * @aspect PrettyPrint
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\PrettyPrint.jadd:119
   */
  public void toString(StringBuffer s) {
    if(isDefaultConstructor()) return;
    s.append(indent());
    getModifiers().toString(s);
    s.append(name() + "(");
    if(getNumParameter() > 0) {
      getParameter(0).toString(s);
      for(int i = 1; i < getNumParameter(); i++) {
        s.append(", ");
        getParameter(i).toString(s);
      }
    }
    s.append(")");
    if(getNumException() > 0) {
      s.append(" throws ");
      getException(0).toString(s);
      for(int i = 1; i < getNumException(); i++) {
        s.append(", ");
        getException(i).toString(s);
      }
    }
    
    s.append(" {");
    if(hasConstructorInvocation()) {
      getConstructorInvocation().toString(s);
    }
    for(int i = 0; i < getBlock().getNumStmt(); i++) {
      getBlock().getStmt(i).toString(s);
    }
    s.append(indent());
    s.append("}");
  }
  /**
   * @aspect TypeCheck
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeCheck.jrag:424
   */
  public void typeCheck() {
    // 8.8.4 (8.4.4)
    TypeDecl exceptionType = typeThrowable();
    for(int i = 0; i < getNumException(); i++) {
      TypeDecl typeDecl = getException(i).type();
      if(!typeDecl.instanceOf(exceptionType))
        error(signature() + " throws non throwable type " + typeDecl.fullName());
    }
  }
  /**
   * @aspect CodeGeneration
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CodeGeneration.jrag:912
   */
  public void emitInvokeConstructor(CodeGeneration gen) {
    int size = -1;
    for(int i = 0; i < getNumParameter(); i++)
      size -= getParameter(i).type().variableSize();
    if(hostType().needsEnclosing())
      size--;
    if(hostType().needsSuperEnclosing()) {
      size--;
    }
    String classname = hostType().constantPoolName();
    String      desc = descName();
    String      name = "<init>";
    int index = gen.constantPool().addMethodref(classname, name, desc);
    gen.emit(Bytecode.INVOKESPECIAL, size).add2(index);
  }
  /**
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:111
   */
  private void generateBytecodes(CodeGeneration gen) {
    int label = gen.variableScopeLabel();
    gen.addLocalVariableEntryAtCurrentPC("this", hostType().typeDescriptor(), 0, label);
    for(int i = 0; i < getNumParameter(); i++) {
      ParameterDeclaration p = (ParameterDeclaration)getParameter(i);
      gen.addLocalVariableEntryAtCurrentPC(
        p.name(), p.type().typeDescriptor(), p.localNum(), label
      );
    }
    createBCode(gen);
    gen.emitReturn();
    gen.addVariableScopeLabel(label);
  }
  /**
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:137
   */
  public void createBCode(CodeGeneration gen) {
    try {
    boolean needsInit = true;

    if(hasConstructorInvocation()) {
      getConstructorInvocation().createBCode(gen);
      Stmt stmt = getConstructorInvocation();
      if(stmt instanceof ExprStmt) {
        ExprStmt exprStmt = (ExprStmt)stmt;
        Expr expr = exprStmt.getExpr();
        if(!expr.isSuperConstructorAccess())
          needsInit = false;

      }
    }

    if(needsEnclosing()) {
      gen.emitLoadReference(0);
      gen.emitLoadReference(1);
      String classname = hostType().constantPoolName();
      String desc = enclosing().typeDescriptor();
      String name = "this$0";
      int index = gen.constantPool().addFieldref(classname, name, desc);
      gen.emit(Bytecode.PUTFIELD, -2).add2(index);
    }

    int localIndex = offsetFirstEnclosingVariable();
    for(Iterator iter = hostType().enclosingVariables().iterator(); iter.hasNext(); ) {
        Variable v = (Variable)iter.next();
        gen.emitLoadReference(0);
        v.type().emitLoadLocal(gen, localIndex);
        String classname = hostType().constantPoolName();
        String desc = v.type().typeDescriptor();
        String name = "val$" + v.name();
        int index = gen.constantPool().addFieldref(classname, name, desc);
        gen.emit(Bytecode.PUTFIELD, -1 - v.type().variableSize()).add2(index);
        localIndex += v.type().variableSize();
    }

    if(needsInit) {
      TypeDecl typeDecl = hostType();
      for(int i = 0; i < typeDecl.getNumBodyDecl(); i++) {
        BodyDecl b = typeDecl.getBodyDecl(i);
        if(b instanceof FieldDeclaration && b.isBytecodeField() && b.generate()) {
          FieldDeclaration f = (FieldDeclaration)b;
          if(!f.isStatic() && f.hasInit()) {
            gen.emit(Bytecode.ALOAD_0);
            f.getInit().createBCode(gen);
            f.getInit().type().emitAssignConvTo(gen, f.type()); // AssignConversion
            f.emitStoreField(gen, hostType());
          }
        }
        else if(b instanceof InstanceInitializer) {
          b.createBCode(gen);
        }
      }
    }
    gen.maxLocals = Math.max(gen.maxLocals, getBlock().localNum());
    getBlock().createBCode(gen);
    } catch (Error e) {
      System.err.println(hostType().typeName() + ": " + this);
      throw e;
    }
  }
  /**
   * @aspect GenerateClassfile
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\GenerateClassfile.jrag:252
   */
  public void generateMethod(DataOutputStream out, ConstantPool cp) throws IOException {
    out.writeChar(flags());
    out.writeChar(cp.addUtf8("<init>"));
    out.writeChar(cp.addUtf8(descName()));
    out.writeChar(attributes().size());
    for(Iterator itera = attributes().iterator(); itera.hasNext();)
      ((Attribute)itera.next()).emit(out);
  }
  /**
   * @aspect GenerateClassfile
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\GenerateClassfile.jrag:268
   */
  public void touchMethod(ConstantPool cp) {
    cp.addUtf8("<init>");
    cp.addUtf8(descName());
    attributes();
  }
  /**
   * @aspect GenerateClassfile
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\GenerateClassfile.jrag:376
   */
  public boolean clear() {
    getBlock().clear();
    setBlock(new Block(new List()));
    bytecodes_ConstantPool_values = null;
    return false;
  }
  /**
   * @aspect InnerClasses
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\InnerClasses.jrag:445
   */
  protected boolean addEnclosingVariables = true;
  /**
   * @aspect InnerClasses
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\InnerClasses.jrag:446
   */
  public void addEnclosingVariables() {
    if(!addEnclosingVariables) return;
    addEnclosingVariables = false;
    hostType().addEnclosingVariables();
    for(Iterator iter = hostType().enclosingVariables().iterator(); iter.hasNext(); ) {
      Variable v = (Variable)iter.next();
      getParameterList().add(new ParameterDeclaration(v.type(), "val$" + v.name()));
    }
  }
  /**
   * @aspect InnerClasses
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\InnerClasses.jrag:480
   */
  public ConstructorDecl createAccessor() {
    ConstructorDecl c = (ConstructorDecl)hostType().getAccessor(this, "constructor");
    if(c != null) return c;

    // make sure enclosing varibles are added as parameters prior to building accessor
    addEnclosingVariables();

    Modifiers modifiers = new Modifiers(new List());
    modifiers.addModifier(new Modifier("synthetic"));
    modifiers.addModifier(new Modifier("public"));

    List parameters = createAccessorParameters();

    List exceptionList = new List(); 
    for(int i = 0; i < getNumException(); i++)
      exceptionList.add(getException(i).type().createQualifiedAccess());
    
    // add all parameters as arguments except for the dummy parameter
    List args = new List();
    for(int i = 0; i < parameters.getNumChildNoTransform() - 1; i++)
      args.add(new VarAccess(((ParameterDeclaration)parameters.getChildNoTransform(i)).name()));
    ConstructorAccess access = new ConstructorAccess("this", args);
    access.addEnclosingVariables = false;

    c = new ConstructorDecl(
      modifiers,
      name(),
      parameters,
      exceptionList,
      new Opt(
        new ExprStmt(
          access
        )
      ),
      new Block(
        new List().add(new ReturnStmt(new Opt()))
      )
    );
    c = hostType().addConstructor(c);
    c.addEnclosingVariables = false;
    hostType().addAccessor(this, "constructor", c);
    return c;
  }
  /**
   * @aspect InnerClasses
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\InnerClasses.jrag:524
   */
  protected List createAccessorParameters() {
    List parameters = new List();
    for (int i=0; i<getNumParameter(); i++)
      parameters.add(new ParameterDeclaration(getParameter(i).type(), getParameter(i).name()));
    parameters.add(new ParameterDeclaration(createAnonymousJavaTypeDecl().createBoundAccess(), ("p" + getNumParameter())));
    return parameters;
  }
  /**
   * @aspect InnerClasses
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\InnerClasses.jrag:532
   */
  protected TypeDecl createAnonymousJavaTypeDecl() {
    ClassDecl classDecl =
      new ClassDecl(
          new Modifiers(new List().add(new Modifier("synthetic"))),
          "" + hostType().nextAnonymousIndex(),
          new Opt(),
          new List(),
          new List()
      );
    classDecl = hostType().addMemberClass(classDecl);
    hostType().addNestedType(classDecl);
    return classDecl;
  }
  /**
   * @aspect Transformations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Transformations.jrag:119
   */
  public void transformation() {
    // this$val as fields and constructor parameters
    addEnclosingVariables();
    super.transformation();
  }
  /**
   * @aspect Enums
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Enums.jrag:138
   */
  protected void transformEnumConstructors() {
    // make sure constructor is private
    Modifiers newModifiers = new Modifiers(new List());
    for (int i = 0; i < getModifiers().getNumModifier(); ++i) {
      String modifier = getModifiers().getModifier(i).getID();
      if (modifier.equals("public") || modifier.equals("private") ||
        modifier.equals("protected"))
          continue;
      newModifiers.addModifier(new Modifier(modifier));
    }
    newModifiers.addModifier(new Modifier("private"));
    setModifiers(newModifiers);

    // add implicit super constructor access since we are traversing
    // without doing rewrites
    if(!hasConstructorInvocation()) {
      setConstructorInvocation(
        new ExprStmt(
          new SuperConstructorAccess("super", new List())
        )
      );
    }
    super.transformEnumConstructors();
    getParameterList().insertChild(
      new ParameterDeclaration(new TypeAccess("java.lang", "String"), "@p0"),
      0
    );
    getParameterList().insertChild(
      new ParameterDeclaration(new TypeAccess("int"), "@p1"),
      1
    );
  }
  /**
   * @aspect LookupParTypeDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1045
   */
  public BodyDecl p(Parameterization parTypeDecl) {
    ConstructorDecl c = new ConstructorDeclSubstituted(
      (Modifiers)getModifiers().fullCopy(),
      getID(),
      getParameterList().substitute(parTypeDecl),
      getExceptionList().substitute(parTypeDecl),
      new Opt(),
      new Block(),
      this
    );
    return c;
  }
  /**
   * @aspect Intros
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\src\\ExtIntrosRefs.jrag:77
   */
  public void printIntros(java.util.List<String> featureModulePathnames) {
        TypeDecl host = hostType();
        if (!(host.isAnonymous() || host.isLocalClass())) {
            System.out.print(IntrosRefsUtil.introPrefix(this, featureModulePathnames));
            System.out.print(IntrosRefsUtil.typeDeclQName(host));
            System.out.println(IntrosRefsUtil.DELIM_CONS + IntrosRefsUtil.signature(this));
        }
    }
  /**
   * @declaredat ASTNode:1
   */
  public ConstructorDecl() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[5];
    setChild(new List(), 1);
    setChild(new List(), 2);
    setChild(new Opt(), 3);
  }
  /**
   * @declaredat ASTNode:16
   */
  public ConstructorDecl(Modifiers p0, String p1, List<ParameterDeclaration> p2, List<Access> p3, Opt<Stmt> p4, Block p5) {
    setChild(p0, 0);
    setID(p1);
    setChild(p2, 1);
    setChild(p3, 2);
    setChild(p4, 3);
    setChild(p5, 4);
  }
  /**
   * @declaredat ASTNode:24
   */
  public ConstructorDecl(Modifiers p0, beaver.Symbol p1, List<ParameterDeclaration> p2, List<Access> p3, Opt<Stmt> p4, Block p5) {
    setChild(p0, 0);
    setID(p1);
    setChild(p2, 1);
    setChild(p3, 2);
    setChild(p4, 3);
    setChild(p5, 4);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:33
   */
  @SideEffect.Pure protected int numChildren() {
    return 5;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:39
   */
  public boolean mayHaveRewrite() {
    return true;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    accessibleFrom_TypeDecl_reset();
    isDAafter_Variable_reset();
    isDUafter_Variable_reset();
    throwsException_TypeDecl_reset();
    name_reset();
    signature_reset();
    sameSignature_ConstructorDecl_reset();
    moreSpecificThan_ConstructorDecl_reset();
    parameterDeclaration_String_reset();
    circularThisInvocation_ConstructorDecl_reset();
    attributes_reset();
    descName_reset();
    bytecodes_ConstantPool_reset();
    flags_reset();
    localNumOfFirstParameter_reset();
    offsetFirstEnclosingVariable_reset();
    sourceConstructorDecl_reset();
    handlesException_TypeDecl_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:65
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:69
   */
  @SideEffect.Fresh public ConstructorDecl clone() throws CloneNotSupportedException {
    ConstructorDecl node = (ConstructorDecl) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:74
   */
  @SideEffect.Fresh(group="_ASTNode") public ConstructorDecl copy() {
    try {
      ConstructorDecl node = (ConstructorDecl) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:93
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public ConstructorDecl fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:103
   */
  @SideEffect.Fresh(group="_ASTNode") public ConstructorDecl treeCopyNoTransform() {
    ConstructorDecl tree = (ConstructorDecl) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:123
   */
  @SideEffect.Fresh(group="_ASTNode") public ConstructorDecl treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:128
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_ID == ((ConstructorDecl) node).tokenString_ID);    
  }
  /**
   * Replaces the Modifiers child.
   * @param node The new node to replace the Modifiers child.
   * @apilevel high-level
   */
  public void setModifiers(Modifiers node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Modifiers child.
   * @return The current node used as the Modifiers child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Modifiers")
  @SideEffect.Pure public Modifiers getModifiers() {
    return (Modifiers) getChild(0);
  }
  /**
   * Retrieves the Modifiers child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Modifiers child.
   * @apilevel low-level
   */
  public Modifiers getModifiersNoTransform() {
    return (Modifiers) getChildNoTransform(0);
  }
  /**
   * Replaces the lexeme ID.
   * @param value The new value for the lexeme ID.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setID(String value) {
    tokenString_ID = value;
  }
  /** @apilevel internal 
   */
  @SideEffect.Secret(group="_ASTNode") protected String tokenString_ID;
  /**
   */
  public int IDstart;
  /**
   */
  public int IDend;
  /**
   * JastAdd-internal setter for lexeme ID using the Beaver parser.
   * @param symbol Symbol containing the new value for the lexeme ID
   * @apilevel internal
   */
  public void setID(beaver.Symbol symbol) {
    if (symbol.value != null && !(symbol.value instanceof String))
    throw new UnsupportedOperationException("setID is only valid for String lexemes");
    tokenString_ID = (String)symbol.value;
    IDstart = symbol.getStart();
    IDend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme ID.
   * @return The value for the lexeme ID.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="ID")
  @SideEffect.Pure(group="_ASTNode") public String getID() {
    return tokenString_ID != null ? tokenString_ID : "";
  }
  /**
   * Replaces the Parameter list.
   * @param list The new list node to be used as the Parameter list.
   * @apilevel high-level
   */
  public void setParameterList(List<ParameterDeclaration> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the Parameter list.
   * @return Number of children in the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumParameter() {
    return getParameterList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Parameter list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumParameterNoTransform() {
    return getParameterListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Parameter list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public ParameterDeclaration getParameter(int i) {
    return (ParameterDeclaration) getParameterList().getChild(i);
  }
  /**
   * Check whether the Parameter list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasParameter() {
    return getParameterList().getNumChild() != 0;
  }
  /**
   * Append an element to the Parameter list.
   * @param node The element to append to the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addParameter(ParameterDeclaration node) {
    List<ParameterDeclaration> list = (parent == null) ? getParameterListNoTransform() : getParameterList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addParameterNoTransform(ParameterDeclaration node) {
    List<ParameterDeclaration> list = getParameterListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Parameter list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setParameter(ParameterDeclaration node, int i) {
    List<ParameterDeclaration> list = getParameterList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Parameter list.
   * @return The node representing the Parameter list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Parameter")
  @SideEffect.Pure(group="_ASTNode") public List<ParameterDeclaration> getParameterList() {
    List<ParameterDeclaration> list = (List<ParameterDeclaration>) getChild(1);
    return list;
  }
  /**
   * Retrieves the Parameter list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<ParameterDeclaration> getParameterListNoTransform() {
    return (List<ParameterDeclaration>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the Parameter list without
   * triggering rewrites.
   */
  @SideEffect.Pure public ParameterDeclaration getParameterNoTransform(int i) {
    return (ParameterDeclaration) getParameterListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Parameter list.
   * @return The node representing the Parameter list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<ParameterDeclaration> getParameters() {
    return getParameterList();
  }
  /**
   * Retrieves the Parameter list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Parameter list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<ParameterDeclaration> getParametersNoTransform() {
    return getParameterListNoTransform();
  }
  /**
   * Replaces the Exception list.
   * @param list The new list node to be used as the Exception list.
   * @apilevel high-level
   */
  public void setExceptionList(List<Access> list) {
    setChild(list, 2);
  }
  /**
   * Retrieves the number of children in the Exception list.
   * @return Number of children in the Exception list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumException() {
    return getExceptionList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Exception list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Exception list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumExceptionNoTransform() {
    return getExceptionListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Exception list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Exception list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Access getException(int i) {
    return (Access) getExceptionList().getChild(i);
  }
  /**
   * Check whether the Exception list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasException() {
    return getExceptionList().getNumChild() != 0;
  }
  /**
   * Append an element to the Exception list.
   * @param node The element to append to the Exception list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addException(Access node) {
    List<Access> list = (parent == null) ? getExceptionListNoTransform() : getExceptionList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addExceptionNoTransform(Access node) {
    List<Access> list = getExceptionListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Exception list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setException(Access node, int i) {
    List<Access> list = getExceptionList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Exception list.
   * @return The node representing the Exception list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Exception")
  @SideEffect.Pure(group="_ASTNode") public List<Access> getExceptionList() {
    List<Access> list = (List<Access>) getChild(2);
    return list;
  }
  /**
   * Retrieves the Exception list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Exception list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Access> getExceptionListNoTransform() {
    return (List<Access>) getChildNoTransform(2);
  }
  /**
   * @return the element at index {@code i} in the Exception list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Access getExceptionNoTransform(int i) {
    return (Access) getExceptionListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Exception list.
   * @return The node representing the Exception list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Access> getExceptions() {
    return getExceptionList();
  }
  /**
   * Retrieves the Exception list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Exception list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Access> getExceptionsNoTransform() {
    return getExceptionListNoTransform();
  }
  /**
   * Replaces the optional node for the ConstructorInvocation child. This is the <code>Opt</code>
   * node containing the child ConstructorInvocation, not the actual child!
   * @param opt The new node to be used as the optional node for the ConstructorInvocation child.
   * @apilevel low-level
   */
  public void setConstructorInvocationOpt(Opt<Stmt> opt) {
    setChild(opt, 3);
  }
  /**
   * Replaces the (optional) ConstructorInvocation child.
   * @param node The new node to be used as the ConstructorInvocation child.
   * @apilevel high-level
   */
  public void setConstructorInvocation(Stmt node) {
    getConstructorInvocationOpt().setChild(node, 0);
  }
  /**
   * Check whether the optional ConstructorInvocation child exists.
   * @return {@code true} if the optional ConstructorInvocation child exists, {@code false} if it does not.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasConstructorInvocation() {
    return getConstructorInvocationOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) ConstructorInvocation child.
   * @return The ConstructorInvocation child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   */
  @SideEffect.Pure public Stmt getConstructorInvocation() {
    return (Stmt) getConstructorInvocationOpt().getChild(0);
  }
  /**
   * Retrieves the optional node for the ConstructorInvocation child. This is the <code>Opt</code> node containing the child ConstructorInvocation, not the actual child!
   * @return The optional node for child the ConstructorInvocation child.
   * @apilevel low-level
   */
  @ASTNodeAnnotation.OptChild(name="ConstructorInvocation")
  @SideEffect.Pure public Opt<Stmt> getConstructorInvocationOpt() {
    return (Opt<Stmt>) getChild(3);
  }
  /**
   * Retrieves the optional node for child ConstructorInvocation. This is the <code>Opt</code> node containing the child ConstructorInvocation, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child ConstructorInvocation.
   * @apilevel low-level
   */
  @SideEffect.Pure public Opt<Stmt> getConstructorInvocationOptNoTransform() {
    return (Opt<Stmt>) getChildNoTransform(3);
  }
  /**
   * Replaces the Block child.
   * @param node The new node to replace the Block child.
   * @apilevel high-level
   */
  public void setBlock(Block node) {
    setChild(node, 4);
  }
  /**
   * Retrieves the Block child.
   * @return The current node used as the Block child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Block")
  @SideEffect.Pure public Block getBlock() {
    return (Block) getChild(4);
  }
  /**
   * Retrieves the Block child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Block child.
   * @apilevel low-level
   */
  public Block getBlockNoTransform() {
    return (Block) getChildNoTransform(4);
  }
  /**
   * @aspect ConstructorDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupConstructor.jrag:136
   */
  private boolean refined_ConstructorDecl_ConstructorDecl_moreSpecificThan_ConstructorDecl(ConstructorDecl m)
{
    for(int i = 0; i < getNumParameter(); i++) {
      if(!getParameter(i).type().instanceOf(m.getParameter(i).type()))
        return false;
    }
    return true;
  }
  /**
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Attributes.jrag:198
   */
  private Collection refined_Attributes_ConstructorDecl_attributes()
{
    ArrayList l = new ArrayList();
    l.add(new CodeAttribute(bytecodes(hostType().constantPool()), null));
    l.add(new ExceptionsAttribute(bytecodes(hostType().constantPool()), this));
    if(getModifiers().isSynthetic())
      l.add(new SyntheticAttribute(hostType().constantPool()));
    return l;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void accessibleFrom_TypeDecl_reset() {
    accessibleFrom_TypeDecl_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="accessibleFrom_TypeDecl") protected java.util.Map accessibleFrom_TypeDecl_values;

  /**
   * @attribute syn
   * @aspect AccessControl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\AccessControl.jrag:94
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="AccessControl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\AccessControl.jrag:94")
  @SideEffect.Pure(group="accessibleFrom_TypeDecl") public boolean accessibleFrom(TypeDecl type) {
    Object _parameters = type;
    if (accessibleFrom_TypeDecl_values == null) accessibleFrom_TypeDecl_values = new java.util.HashMap(4);
    ASTState state = state();
    if (accessibleFrom_TypeDecl_values.containsKey(_parameters)) {
      return (Boolean) accessibleFrom_TypeDecl_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boolean accessibleFrom_TypeDecl_value = accessibleFrom_compute(type);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    accessibleFrom_TypeDecl_values.put(_parameters, accessibleFrom_TypeDecl_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return accessibleFrom_TypeDecl_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean accessibleFrom_compute(TypeDecl type) {
      if(!hostType().accessibleFrom(type))
        return false;
      else if(isPublic())
        return true;
      else if(isProtected()) {
        return true;
      }
      else if(isPrivate()) {
        return hostType().topLevelType() == type.topLevelType();
      }
      else
        return hostPackage().equals(type.hostPackage());
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void isDAafter_Variable_reset() {
    isDAafter_Variable_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="isDAafter_Variable") protected java.util.Map isDAafter_Variable_values;

  /**
   * @attribute syn
   * @aspect DA
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:243
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="DA", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:243")
  @SideEffect.Pure(group="isDAafter_Variable") public boolean isDAafter(Variable v) {
    Object _parameters = v;
    if (isDAafter_Variable_values == null) isDAafter_Variable_values = new java.util.HashMap(4);
    ASTState state = state();
    if (isDAafter_Variable_values.containsKey(_parameters)) {
      return (Boolean) isDAafter_Variable_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boolean isDAafter_Variable_value = getBlock().isDAafter(v) && getBlock().checkReturnDA(v);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isDAafter_Variable_values.put(_parameters, isDAafter_Variable_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return isDAafter_Variable_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void isDUafter_Variable_reset() {
    isDUafter_Variable_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="isDUafter_Variable") protected java.util.Map isDUafter_Variable_values;

  /**
   * @attribute syn
   * @aspect DU
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:707
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="DU", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:707")
  @SideEffect.Pure(group="isDUafter_Variable") public boolean isDUafter(Variable v) {
    Object _parameters = v;
    if (isDUafter_Variable_values == null) isDUafter_Variable_values = new java.util.HashMap(4);
    ASTState state = state();
    if (isDUafter_Variable_values.containsKey(_parameters)) {
      return (Boolean) isDUafter_Variable_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boolean isDUafter_Variable_value = getBlock().isDUafter(v) && getBlock().checkReturnDU(v);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    isDUafter_Variable_values.put(_parameters, isDUafter_Variable_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return isDUafter_Variable_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void throwsException_TypeDecl_reset() {
    throwsException_TypeDecl_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="throwsException_TypeDecl") protected java.util.Map throwsException_TypeDecl_values;

  /**
   * @attribute syn
   * @aspect ExceptionHandling
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\ExceptionHandling.jrag:145
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ExceptionHandling", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\ExceptionHandling.jrag:145")
  @SideEffect.Pure(group="throwsException_TypeDecl") public boolean throwsException(TypeDecl exceptionType) {
    Object _parameters = exceptionType;
    if (throwsException_TypeDecl_values == null) throwsException_TypeDecl_values = new java.util.HashMap(4);
    ASTState state = state();
    if (throwsException_TypeDecl_values.containsKey(_parameters)) {
      return (Boolean) throwsException_TypeDecl_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boolean throwsException_TypeDecl_value = throwsException_compute(exceptionType);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    throwsException_TypeDecl_values.put(_parameters, throwsException_TypeDecl_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return throwsException_TypeDecl_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean throwsException_compute(TypeDecl exceptionType) {
      for(int i = 0; i < getNumException(); i++)
        if(exceptionType.instanceOf(getException(i).type()))
          return true;
      return false;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void name_reset() {
    name_computed = false;
    
    name_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="name") protected boolean name_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="name") protected String name_value;

  /**
   * @attribute syn
   * @aspect ConstructorDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupConstructor.jrag:110
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstructorDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupConstructor.jrag:110")
  @SideEffect.Pure(group="name") public String name() {
    ASTState state = state();
    if (name_computed) {
      return name_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    name_value = getID();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    name_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return name_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void signature_reset() {
    signature_computed = false;
    
    signature_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="signature") protected boolean signature_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="signature") protected String signature_value;

  /**
   * @attribute syn
   * @aspect ConstructorDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupConstructor.jrag:112
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstructorDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupConstructor.jrag:112")
  @SideEffect.Pure(group="signature") public String signature() {
    ASTState state = state();
    if (signature_computed) {
      return signature_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    signature_value = signature_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    signature_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return signature_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private String signature_compute() {
      StringBuffer s = new StringBuffer();
      s.append(name() + "(");
      for(int i = 0; i < getNumParameter(); i++) {
        s.append(getParameter(i));
        if(i != getNumParameter() - 1)
          s.append(", ");
      }
      s.append(")");
      return s.toString();
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void sameSignature_ConstructorDecl_reset() {
    sameSignature_ConstructorDecl_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="sameSignature_ConstructorDecl") protected java.util.Map sameSignature_ConstructorDecl_values;

  /**
   * @attribute syn
   * @aspect ConstructorDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupConstructor.jrag:125
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstructorDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupConstructor.jrag:125")
  @SideEffect.Pure(group="sameSignature_ConstructorDecl") public boolean sameSignature(ConstructorDecl c) {
    Object _parameters = c;
    if (sameSignature_ConstructorDecl_values == null) sameSignature_ConstructorDecl_values = new java.util.HashMap(4);
    ASTState state = state();
    if (sameSignature_ConstructorDecl_values.containsKey(_parameters)) {
      return (Boolean) sameSignature_ConstructorDecl_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boolean sameSignature_ConstructorDecl_value = sameSignature_compute(c);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    sameSignature_ConstructorDecl_values.put(_parameters, sameSignature_ConstructorDecl_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return sameSignature_ConstructorDecl_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean sameSignature_compute(ConstructorDecl c) {
      if(!name().equals(c.name()))
        return false;
      if(c.getNumParameter() != getNumParameter())
        return false;
      for(int i = 0; i < getNumParameter(); i++)
        if(!c.getParameter(i).type().equals(getParameter(i).type()))
          return false;
      return true;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void moreSpecificThan_ConstructorDecl_reset() {
    moreSpecificThan_ConstructorDecl_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="moreSpecificThan_ConstructorDecl") protected java.util.Map moreSpecificThan_ConstructorDecl_values;

  /**
   * @attribute syn
   * @aspect ConstructorDecl
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupConstructor.jrag:136
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstructorDecl", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupConstructor.jrag:136")
  @SideEffect.Pure(group="moreSpecificThan_ConstructorDecl") public boolean moreSpecificThan(ConstructorDecl m) {
    Object _parameters = m;
    if (moreSpecificThan_ConstructorDecl_values == null) moreSpecificThan_ConstructorDecl_values = new java.util.HashMap(4);
    ASTState state = state();
    if (moreSpecificThan_ConstructorDecl_values.containsKey(_parameters)) {
      return (Boolean) moreSpecificThan_ConstructorDecl_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boolean moreSpecificThan_ConstructorDecl_value = moreSpecificThan_compute(m);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    moreSpecificThan_ConstructorDecl_values.put(_parameters, moreSpecificThan_ConstructorDecl_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return moreSpecificThan_ConstructorDecl_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean moreSpecificThan_compute(ConstructorDecl m) {
      if(!isVariableArity() && !m.isVariableArity())
        return refined_ConstructorDecl_ConstructorDecl_moreSpecificThan_ConstructorDecl(m);
      int num = Math.max(getNumParameter(), m.getNumParameter());
      for(int i = 0; i < num; i++) {
        TypeDecl t1 = i < getNumParameter() - 1 ? getParameter(i).type() : getParameter(getNumParameter()-1).type().componentType();
        TypeDecl t2 = i < m.getNumParameter() - 1 ? m.getParameter(i).type() : m.getParameter(m.getNumParameter()-1).type().componentType();
        if(!t1.instanceOf(t2))
          return false;
      }
      return true;
    }
  /**
   * @return true if this is an auto-generated default constructor
   * @attribute syn
   * @aspect ImplicitConstructor
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupConstructor.jrag:182
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ImplicitConstructor", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupConstructor.jrag:182")
  @SideEffect.Pure(group="isDefaultConstructor") public boolean isDefaultConstructor() {
    boolean isDefaultConstructor_value = isDefaultConstructor;
    return isDefaultConstructor_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void parameterDeclaration_String_reset() {
    parameterDeclaration_String_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="parameterDeclaration_String") protected java.util.Map parameterDeclaration_String_values;

  /**
   * @attribute syn
   * @aspect VariableScope
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupVariable.jrag:105
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="VariableScope", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupVariable.jrag:105")
  @SideEffect.Pure(group="parameterDeclaration_String") public SimpleSet parameterDeclaration(String name) {
    Object _parameters = name;
    if (parameterDeclaration_String_values == null) parameterDeclaration_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (parameterDeclaration_String_values.containsKey(_parameters)) {
      return (SimpleSet) parameterDeclaration_String_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    SimpleSet parameterDeclaration_String_value = parameterDeclaration_compute(name);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    parameterDeclaration_String_values.put(_parameters, parameterDeclaration_String_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return parameterDeclaration_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private SimpleSet parameterDeclaration_compute(String name) {
      for(int i = 0; i < getNumParameter(); i++)
        if(getParameter(i).name().equals(name))
          return (ParameterDeclaration)getParameter(i);
      return SimpleSet.emptySet;
    }
  /**
   * @attribute syn
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:217
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:217")
  @SideEffect.Pure(group="isSynthetic") public boolean isSynthetic() {
    boolean isSynthetic_value = getModifiers().isSynthetic();
    return isSynthetic_value;
  }
  /**
   * @attribute syn
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:235
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:235")
  @SideEffect.Pure(group="isPublic") public boolean isPublic() {
    boolean isPublic_value = getModifiers().isPublic();
    return isPublic_value;
  }
  /**
   * @attribute syn
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:236
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:236")
  @SideEffect.Pure(group="isPrivate") public boolean isPrivate() {
    boolean isPrivate_value = getModifiers().isPrivate();
    return isPrivate_value;
  }
  /**
   * @attribute syn
   * @aspect Modifiers
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:237
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Modifiers", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:237")
  @SideEffect.Pure(group="isProtected") public boolean isProtected() {
    boolean isProtected_value = getModifiers().isProtected();
    return isProtected_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void circularThisInvocation_ConstructorDecl_reset() {
    circularThisInvocation_ConstructorDecl_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="circularThisInvocation_ConstructorDecl") protected java.util.Map circularThisInvocation_ConstructorDecl_values;

  /**
   * @attribute syn
   * @aspect NameCheck
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\NameCheck.jrag:83
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="NameCheck", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\NameCheck.jrag:83")
  @SideEffect.Pure(group="circularThisInvocation_ConstructorDecl") public boolean circularThisInvocation(ConstructorDecl decl) {
    Object _parameters = decl;
    if (circularThisInvocation_ConstructorDecl_values == null) circularThisInvocation_ConstructorDecl_values = new java.util.HashMap(4);
    ASTState state = state();
    if (circularThisInvocation_ConstructorDecl_values.containsKey(_parameters)) {
      return (Boolean) circularThisInvocation_ConstructorDecl_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boolean circularThisInvocation_ConstructorDecl_value = circularThisInvocation_compute(decl);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    circularThisInvocation_ConstructorDecl_values.put(_parameters, circularThisInvocation_ConstructorDecl_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return circularThisInvocation_ConstructorDecl_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private boolean circularThisInvocation_compute(ConstructorDecl decl) {
      if(hasConstructorInvocation()) {
        Expr e = ((ExprStmt)getConstructorInvocation()).getExpr();
        if(e instanceof ConstructorAccess) {
          ConstructorDecl constructorDecl = ((ConstructorAccess)e).decl();
          if(constructorDecl == decl)
            return true;
          return constructorDecl.circularThisInvocation(decl);
        }
      }
      return false;
    }
  /**
   * @attribute syn
   * @aspect TypeAnalysis
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:268
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:268")
  @SideEffect.Pure(group="type") public TypeDecl type() {
    TypeDecl type_value = unknownType();
    return type_value;
  }
  /**
   * @attribute syn
   * @aspect TypeAnalysis
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:271
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:271")
  @SideEffect.Pure(group="isVoid") public boolean isVoid() {
    boolean isVoid_value = true;
    return isVoid_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void attributes_reset() {
    attributes_computed = false;
    
    attributes_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="attributes") protected boolean attributes_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="attributes") protected Collection attributes_value;

  /**
   * @attribute syn
   * @aspect Attributes
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Attributes.jrag:182
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Attributes", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Attributes.jrag:182")
  @SideEffect.Pure(group="attributes") public Collection attributes() {
    ASTState state = state();
    if (attributes_computed) {
      return attributes_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    attributes_value = attributes_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    attributes_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return attributes_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private Collection attributes_compute() {
      Collection c = refined_Attributes_ConstructorDecl_attributes();
      getModifiers().addRuntimeVisibleAnnotationsAttribute(c);
      getModifiers().addRuntimeInvisibleAnnotationsAttribute(c);
      return c;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void descName_reset() {
    descName_computed = false;
    
    descName_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="descName") protected boolean descName_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="descName") protected String descName_value;

  /**
   * @attribute syn
   * @aspect ConstantPoolNames
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\ConstantPoolNames.jrag:48
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="ConstantPoolNames", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\ConstantPoolNames.jrag:48")
  @SideEffect.Pure(group="descName") public String descName() {
    ASTState state = state();
    if (descName_computed) {
      return descName_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    descName_value = descName_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    descName_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return descName_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private String descName_compute() {
      StringBuffer b = new StringBuffer();
      b.append("(");
      // this$0
      if(needsEnclosing())
        b.append(enclosing().typeDescriptor());
      if(needsSuperEnclosing())
        b.append(superEnclosing().typeDescriptor());
      // args
      for (int i=0; i<getNumParameter(); i++)
        b.append(getParameter(i).type().typeDescriptor());
      b.append(")V");
      return b.toString();
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void bytecodes_ConstantPool_reset() {
    bytecodes_ConstantPool_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="bytecodes_ConstantPool") protected java.util.Map bytecodes_ConstantPool_values;

  /**
   * @attribute syn
   * @aspect CreateBCode
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:100
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="CreateBCode", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\CreateBCode.jrag:100")
  @SideEffect.Pure(group="bytecodes_ConstantPool") public CodeGeneration bytecodes(ConstantPool constantPool) {
    Object _parameters = constantPool;
    if (bytecodes_ConstantPool_values == null) bytecodes_ConstantPool_values = new java.util.HashMap(4);
    ASTState state = state();
    if (bytecodes_ConstantPool_values.containsKey(_parameters)) {
      return (CodeGeneration) bytecodes_ConstantPool_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    CodeGeneration bytecodes_ConstantPool_value = bytecodes_compute(constantPool);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    bytecodes_ConstantPool_values.put(_parameters, bytecodes_ConstantPool_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return bytecodes_ConstantPool_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private CodeGeneration bytecodes_compute(ConstantPool constantPool) {
      CodeGeneration gen = new CodeGeneration(constantPool);
      generateBytecodes(gen);
      if(!gen.numberFormatError())
        return gen;
      gen = new CodeGeneration(constantPool, true);
      generateBytecodes(gen);
      if(!gen.numberFormatError())
        return gen;
      throw new Error("Could not generate code for " + signature() + " in " + hostType().typeName());
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void flags_reset() {
    flags_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="flags") protected boolean flags_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="flags") protected int flags_value;

  /**
   * @attribute syn
   * @aspect Flags
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Flags.jrag:54
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Flags", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Flags.jrag:54")
  @SideEffect.Pure(group="flags") public int flags() {
    ASTState state = state();
    if (flags_computed) {
      return flags_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    flags_value = flags_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    flags_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return flags_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private int flags_compute() {
      int res = 0;
      if(isPublic()) res |= Modifiers.ACC_PUBLIC;
      if(isPrivate()) res |= Modifiers.ACC_PRIVATE;
      if(isProtected()) res |= Modifiers.ACC_PROTECTED;
      //if(isSynchronized()) res |= Modifiers.ACC_SYNCHRONIZED;
      //if(isStrictfp()) res |= Modifiers.ACC_STRICT;
      return res;
    }
  /**
   * @attribute syn
   * @aspect GenerateClassfile
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\GenerateClassfile.jrag:296
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="GenerateClassfile", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\GenerateClassfile.jrag:296")
  @SideEffect.Pure(group="isBytecodeMethod") public boolean isBytecodeMethod() {
    boolean isBytecodeMethod_value = true;
    return isBytecodeMethod_value;
  }
  /**
   * @attribute syn
   * @aspect GenerateClassfile
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\GenerateClassfile.jrag:325
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="GenerateClassfile", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\GenerateClassfile.jrag:325")
  @SideEffect.Pure(group="flush") public boolean flush() {
    boolean flush_value = false;
    return flush_value;
  }
  /**
   * @attribute syn
   * @aspect InnerClasses
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\InnerClasses.jrag:421
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="InnerClasses", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\InnerClasses.jrag:421")
  @SideEffect.Pure(group="needsEnclosing") public boolean needsEnclosing() {
    boolean needsEnclosing_value = hostType().needsEnclosing();
    return needsEnclosing_value;
  }
  /**
   * @attribute syn
   * @aspect InnerClasses
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\InnerClasses.jrag:422
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="InnerClasses", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\InnerClasses.jrag:422")
  @SideEffect.Pure(group="needsSuperEnclosing") public boolean needsSuperEnclosing() {
    boolean needsSuperEnclosing_value = hostType().needsSuperEnclosing();
    return needsSuperEnclosing_value;
  }
  /**
   * @attribute syn
   * @aspect InnerClasses
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\InnerClasses.jrag:424
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="InnerClasses", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\InnerClasses.jrag:424")
  @SideEffect.Pure(group="enclosing") public TypeDecl enclosing() {
    TypeDecl enclosing_value = hostType().enclosing();
    return enclosing_value;
  }
  /**
   * @attribute syn
   * @aspect InnerClasses
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\InnerClasses.jrag:425
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="InnerClasses", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\InnerClasses.jrag:425")
  @SideEffect.Pure(group="superEnclosing") public TypeDecl superEnclosing() {
    TypeDecl superEnclosing_value = hostType().superEnclosing();
    return superEnclosing_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void localNumOfFirstParameter_reset() {
    localNumOfFirstParameter_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="localNumOfFirstParameter") protected boolean localNumOfFirstParameter_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="localNumOfFirstParameter") protected int localNumOfFirstParameter_value;

  /**
   * @attribute syn
   * @aspect LocalNum
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\LocalNum.jrag:55
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="LocalNum", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\LocalNum.jrag:55")
  @SideEffect.Pure(group="localNumOfFirstParameter") public int localNumOfFirstParameter() {
    ASTState state = state();
    if (localNumOfFirstParameter_computed) {
      return localNumOfFirstParameter_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    localNumOfFirstParameter_value = localNumOfFirstParameter_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    localNumOfFirstParameter_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return localNumOfFirstParameter_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private int localNumOfFirstParameter_compute() {
      int i = 1;
      if(hostType().needsEnclosing())
        i++;
      if(hostType().needsSuperEnclosing())
        i++;
      return i;
    }
  /** @apilevel internal */
  @SideEffect.Ignore private void offsetFirstEnclosingVariable_reset() {
    offsetFirstEnclosingVariable_computed = false;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="offsetFirstEnclosingVariable") protected boolean offsetFirstEnclosingVariable_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="offsetFirstEnclosingVariable") protected int offsetFirstEnclosingVariable_value;

  /**
   * @attribute syn
   * @aspect LocalNum
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\LocalNum.jrag:64
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="LocalNum", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\LocalNum.jrag:64")
  @SideEffect.Pure(group="offsetFirstEnclosingVariable") public int offsetFirstEnclosingVariable() {
    ASTState state = state();
    if (offsetFirstEnclosingVariable_computed) {
      return offsetFirstEnclosingVariable_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    offsetFirstEnclosingVariable_value = offsetFirstEnclosingVariable_compute();
    if (isFinal && _boundaries == state().boundariesCrossed) {
    offsetFirstEnclosingVariable_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return offsetFirstEnclosingVariable_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private int offsetFirstEnclosingVariable_compute() {
      int localIndex = localNumOfFirstParameter();
      Collection vars = hostType().enclosingVariables();
      if(vars.isEmpty())
        return localIndex;
      String name = "val$" + ((Variable)vars.iterator().next()).name();
      for(int i = 0; !getParameter(i).name().equals(name); i++)
        localIndex += getParameter(i).type().variableSize();
      return localIndex;
    }
  /**
   * @attribute syn
   * @aspect LocalNum
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\LocalNum.jrag:75
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="LocalNum", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\LocalNum.jrag:75")
  @SideEffect.Pure(group="localIndexOfEnclosingVariable_Variable") public int localIndexOfEnclosingVariable(Variable v) {
    {
        int localIndex  = offsetFirstEnclosingVariable();
        Iterator iter = hostType().enclosingVariables().iterator();
        Variable varDecl = (Variable)iter.next();
        while(varDecl != v && iter.hasNext()) {
          localIndex += varDecl.type().variableSize();
          varDecl = (Variable)iter.next();
        }
        return localIndex;
      }
  }
  /**
   * @attribute syn
   * @aspect Annotations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:283
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Annotations", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:283")
  @SideEffect.Pure(group="hasAnnotationSuppressWarnings_String") public boolean hasAnnotationSuppressWarnings(String s) {
    boolean hasAnnotationSuppressWarnings_String_value = getModifiers().hasAnnotationSuppressWarnings(s);
    return hasAnnotationSuppressWarnings_String_value;
  }
  /**
   * @attribute syn
   * @aspect Annotations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:324
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Annotations", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:324")
  @SideEffect.Pure(group="isDeprecated") public boolean isDeprecated() {
    boolean isDeprecated_value = getModifiers().hasDeprecatedAnnotation();
    return isDeprecated_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void sourceConstructorDecl_reset() {
    sourceConstructorDecl_computed = false;
    
    sourceConstructorDecl_value = null;
  }
  /** @apilevel internal */
  @SideEffect.Secret(group="sourceConstructorDecl") protected boolean sourceConstructorDecl_computed = false;

  /** @apilevel internal */
  @SideEffect.Secret(group="sourceConstructorDecl") protected ConstructorDecl sourceConstructorDecl_value;

  /**
   * @attribute syn
   * @aspect SourceDeclarations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1277
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SourceDeclarations", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1277")
  @SideEffect.Pure(group="sourceConstructorDecl") public ConstructorDecl sourceConstructorDecl() {
    ASTState state = state();
    if (sourceConstructorDecl_computed) {
      return sourceConstructorDecl_value;
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    sourceConstructorDecl_value = this;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    sourceConstructorDecl_computed = true;
    
    } else {
    }
    state().leaveLazyAttribute();
    return sourceConstructorDecl_value;
  }
  /**
   * @attribute syn
   * @aspect MethodSignature15
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\MethodSignature.jrag:190
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="MethodSignature15", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\MethodSignature.jrag:190")
  @SideEffect.Pure(group="applicableBySubtyping_List") public boolean applicableBySubtyping(List argList) {
    {
        if(getNumParameter() != argList.getNumChild())
          return false;
        for(int i = 0; i < getNumParameter(); i++) {
          TypeDecl arg = ((Expr)argList.getChild(i)).type();
          if(!arg.instanceOf(getParameter(i).type()))
            return false;
        }
        return true;
      }
  }
  /**
   * @attribute syn
   * @aspect MethodSignature15
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\MethodSignature.jrag:210
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="MethodSignature15", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\MethodSignature.jrag:210")
  @SideEffect.Pure(group="applicableByMethodInvocationConversion_List") public boolean applicableByMethodInvocationConversion(List argList) {
    {
        if(getNumParameter() != argList.getNumChild())
          return false;
        for(int i = 0; i < getNumParameter(); i++) {
          TypeDecl arg = ((Expr)argList.getChild(i)).type();
          if(!arg.methodInvocationConversionTo(getParameter(i).type()))
            return false;
        }
        return true;
      }
  }
  /**
   * @attribute syn
   * @aspect MethodSignature15
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\MethodSignature.jrag:231
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="MethodSignature15", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\MethodSignature.jrag:231")
  @SideEffect.Pure(group="applicableVariableArity_List") public boolean applicableVariableArity(List argList) {
    {
        for(int i = 0; i < getNumParameter() - 1; i++) {
          TypeDecl arg = ((Expr)argList.getChild(i)).type();
          if(!arg.methodInvocationConversionTo(getParameter(i).type()))
            return false;
        }
        for(int i = getNumParameter() - 1; i < argList.getNumChild(); i++) {
          TypeDecl arg = ((Expr)argList.getChild(i)).type();
          if(!arg.methodInvocationConversionTo(lastParameter().type().componentType()))
            return false;
        }
        return true;
      }
  }
  /**
   * @attribute syn
   * @aspect MethodSignature15
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\MethodSignature.jrag:318
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="MethodSignature15", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\MethodSignature.jrag:318")
  @SideEffect.Pure(group="potentiallyApplicable_List") public boolean potentiallyApplicable(List argList) {
    {
        if(isVariableArity() && !(argList.getNumChild() >= arity()-1))
          return false;
        if(!isVariableArity() && !(arity() == argList.getNumChild()))
          return false;
        return true;
      }
  }
  /**
   * @attribute syn
   * @aspect MethodSignature15
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\MethodSignature.jrag:325
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="MethodSignature15", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\MethodSignature.jrag:325")
  @SideEffect.Pure(group="arity") public int arity() {
    int arity_value = getNumParameter();
    return arity_value;
  }
  /**
   * @attribute syn
   * @aspect VariableArityParameters
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\VariableArityParameters.jrag:34
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="VariableArityParameters", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\VariableArityParameters.jrag:34")
  @SideEffect.Pure(group="isVariableArity") public boolean isVariableArity() {
    boolean isVariableArity_value = getNumParameter() == 0 ? false : getParameter(getNumParameter()-1).isVariableArity();
    return isVariableArity_value;
  }
  /**
   * @attribute syn
   * @aspect VariableArityParameters
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\VariableArityParameters.jrag:63
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="VariableArityParameters", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\VariableArityParameters.jrag:63")
  @SideEffect.Pure(group="lastParameter") public ParameterDeclaration lastParameter() {
    ParameterDeclaration lastParameter_value = getParameter(getNumParameter() - 1);
    return lastParameter_value;
  }
  /**
   * @attribute syn
   * @aspect GenericsCodegen
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Backend\\GenericsCodegen.jrag:168
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="GenericsCodegen", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Backend\\GenericsCodegen.jrag:168")
  @SideEffect.Pure(group="erasedConstructor") public ConstructorDecl erasedConstructor() {
    ConstructorDecl erasedConstructor_value = this;
    return erasedConstructor_value;
  }
  /**
   * @attribute syn
   * @aspect GenericsCodegen
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Backend\\GenericsCodegen.jrag:373
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="GenericsCodegen", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Backend\\GenericsCodegen.jrag:373")
  @SideEffect.Pure(group="needsSignatureAttribute") public boolean needsSignatureAttribute() {
    {
        for(int i = 0; i < getNumParameter(); i++)
          if(getParameter(i).type().needsSignatureAttribute())
            return true;
        return false;
      }
  }
  /**
   * @return true if the modifier list includes the SafeVarargs annotation
   * @attribute syn
   * @aspect SafeVarargs
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\SafeVarargs.jrag:20
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SafeVarargs", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\SafeVarargs.jrag:20")
  @SideEffect.Pure(group="hasAnnotationSafeVarargs") public boolean hasAnnotationSafeVarargs() {
    boolean hasAnnotationSafeVarargs_value = getModifiers().hasAnnotationSafeVarargs();
    return hasAnnotationSafeVarargs_value;
  }
  /**
   * It is an error if the SafeVarargs annotation is used on something
   * that is not a variable arity method or constructor.
   * @attribute syn
   * @aspect SafeVarargs
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\SafeVarargs.jrag:56
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SafeVarargs", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\SafeVarargs.jrag:56")
  @SideEffect.Pure(group="hasIllegalAnnotationSafeVarargs") public boolean hasIllegalAnnotationSafeVarargs() {
    boolean hasIllegalAnnotationSafeVarargs_value = hasAnnotationSafeVarargs() && !isVariableArity();
    return hasIllegalAnnotationSafeVarargs_value;
  }
  /**
   * @attribute inh
   * @aspect ExceptionHandling
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\ExceptionHandling.jrag:36
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="ExceptionHandling", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\ExceptionHandling.jrag:36")
  @SideEffect.Pure(group="handlesException_TypeDecl") public boolean handlesException(TypeDecl exceptionType) {
    Object _parameters = exceptionType;
    if (handlesException_TypeDecl_values == null) handlesException_TypeDecl_values = new java.util.HashMap(4);
    ASTState state = state();
    if (handlesException_TypeDecl_values.containsKey(_parameters)) {
      return (Boolean) handlesException_TypeDecl_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    boolean handlesException_TypeDecl_value = getParent().Define_handlesException(this, null, exceptionType);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    handlesException_TypeDecl_values.put(_parameters, handlesException_TypeDecl_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return handlesException_TypeDecl_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void handlesException_TypeDecl_reset() {
    handlesException_TypeDecl_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="handlesException_TypeDecl") protected java.util.Map handlesException_TypeDecl_values;

  /**
   * @attribute inh
   * @aspect TypeAnalysis
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:267
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="TypeAnalysis", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeAnalysis.jrag:267")
  @SideEffect.Pure(group="unknownType") public TypeDecl unknownType() {
    TypeDecl unknownType_value = getParent().Define_unknownType(this, null);
    return unknownType_value;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:232
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isDAbefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    if (_callerNode == getBlockNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:298
      return hasConstructorInvocation() ? getConstructorInvocation().isDAafter(v) : isDAbefore(v);
    }
    else {
      return getParent().Define_isDAbefore(this, _callerNode, v);
    }
  }
  @SideEffect.Pure protected boolean canDefine_isDAbefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:690
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isDUbefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    if (_callerNode == getBlockNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\DefiniteAssignment.jrag:754
      return hasConstructorInvocation() ? getConstructorInvocation().isDUafter(v) : isDUbefore(v);
    }
    else {
      return getParent().Define_isDUbefore(this, _callerNode, v);
    }
  }
  @SideEffect.Pure protected boolean canDefine_isDUbefore(ASTNode _callerNode, ASTNode _childNode, Variable v) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\TryWithResources.jrag:83
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_handlesException(ASTNode _callerNode, ASTNode _childNode, TypeDecl exceptionType) {
    if (_callerNode == getConstructorInvocationOptNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\ExceptionHandling.jrag:142
      return throwsException(exceptionType) || handlesException(exceptionType);
    }
    else if (_callerNode == getBlockNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\ExceptionHandling.jrag:139
      return throwsException(exceptionType) || handlesException(exceptionType);
    }
    else {
      return getParent().Define_handlesException(this, _callerNode, exceptionType);
    }
  }
  @SideEffect.Pure protected boolean canDefine_handlesException(ASTNode _callerNode, ASTNode _childNode, TypeDecl exceptionType) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupMethod.jrag:24
   * @apilevel internal
   */
 @SideEffect.Pure public Collection Define_lookupMethod(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getConstructorInvocationOptNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupMethod.jrag:45
      {
          Collection c = new ArrayList();
          for(Iterator iter = lookupMethod(name).iterator(); iter.hasNext(); ) {
            MethodDecl m = (MethodDecl)iter.next();
            if(!hostType().memberMethods(name).contains(m) || m.isStatic())
              c.add(m);
          }
          return c;
        }
    }
    else {
      return getParent().Define_lookupMethod(this, _callerNode, name);
    }
  }
  @SideEffect.Pure protected boolean canDefine_lookupMethod(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Backend\\MultiCatch.jrag:87
   * @apilevel internal
   */
 @SideEffect.Pure public SimpleSet Define_lookupVariable(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getParameterListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupVariable.jrag:64
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return parameterDeclaration(name);
    }
    else if (_callerNode == getConstructorInvocationOptNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupVariable.jrag:53
      {
          SimpleSet set = parameterDeclaration(name);
          if(!set.isEmpty()) return set;
          for(Iterator iter = lookupVariable(name).iterator(); iter.hasNext(); ) {
            Variable v = (Variable)iter.next();
            if(!hostType().memberFields(name).contains(v) || v.isStatic())
              set = set.add(v);
          }
          return set;
        }
    }
    else if (_callerNode == getBlockNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupVariable.jrag:48
      {
          SimpleSet set = parameterDeclaration(name);
          if(!set.isEmpty()) return set;
          return lookupVariable(name);
        }
    }
    else {
      return getParent().Define_lookupVariable(this, _callerNode, name);
    }
  }
  @SideEffect.Pure protected boolean canDefine_lookupVariable(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:360
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_mayBePublic(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getModifiersNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:282
      return true;
    }
    else {
      return getParent().Define_mayBePublic(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_mayBePublic(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:362
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_mayBeProtected(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getModifiersNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:283
      return true;
    }
    else {
      return getParent().Define_mayBeProtected(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_mayBeProtected(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:361
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_mayBePrivate(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getModifiersNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\Modifiers.jrag:284
      return true;
    }
    else {
      return getParent().Define_mayBePrivate(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_mayBePrivate(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\NameCheck.jrag:240
   * @apilevel internal
   */
 @SideEffect.Pure public ASTNode Define_enclosingBlock(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getBlockNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\NameCheck.jrag:242
      return this;
    }
    else {
      return getParent().Define_enclosingBlock(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_enclosingBlock(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\SyntacticClassification.jrag:20
   * @apilevel internal
   */
 @SideEffect.Pure public NameType Define_nameType(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getConstructorInvocationOptNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\SyntacticClassification.jrag:117
      return NameType.EXPRESSION_NAME;
    }
    else if (_callerNode == getExceptionListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\SyntacticClassification.jrag:83
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return NameType.TYPE_NAME;
    }
    else if (_callerNode == getParameterListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\SyntacticClassification.jrag:81
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return NameType.TYPE_NAME;
    }
    else {
      return getParent().Define_nameType(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_nameType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeCheck.jrag:505
   * @apilevel internal
   */
 @SideEffect.Pure public TypeDecl Define_enclosingInstance(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getConstructorInvocationOptNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeCheck.jrag:517
      return unknownType();
    }
    else {
      return getParent().Define_enclosingInstance(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_enclosingInstance(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeHierarchyCheck.jrag:127
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_inExplicitConstructorInvocation(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getConstructorInvocationOptNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeHierarchyCheck.jrag:132
      return true;
    }
    else {
      return getParent().Define_inExplicitConstructorInvocation(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_inExplicitConstructorInvocation(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeHierarchyCheck.jrag:134
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_inStaticContext(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getConstructorInvocationOptNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeHierarchyCheck.jrag:144
      return false;
    }
    else if (_callerNode == getBlockNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\TypeHierarchyCheck.jrag:143
      return false;
    }
    else {
      return getParent().Define_inStaticContext(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_inStaticContext(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\UnreachableStatements.jrag:28
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_reachable(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getBlockNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\UnreachableStatements.jrag:32
      return !hasConstructorInvocation() ? true : getConstructorInvocation().canCompleteNormally();
    }
    else if (_callerNode == getConstructorInvocationOptNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\UnreachableStatements.jrag:31
      return true;
    }
    else {
      return getParent().Define_reachable(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_reachable(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\MultiCatch.jrag:22
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isMethodParameter(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getParameterListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\VariableDeclaration.jrag:58
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return false;
    }
    else {
      return getParent().Define_isMethodParameter(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_isMethodParameter(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\MultiCatch.jrag:23
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isConstructorParameter(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getParameterListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\VariableDeclaration.jrag:59
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return true;
    }
    else {
      return getParent().Define_isConstructorParameter(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_isConstructorParameter(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\MultiCatch.jrag:24
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isExceptionHandlerParameter(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getParameterListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\VariableDeclaration.jrag:60
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return false;
    }
    else {
      return getParent().Define_isExceptionHandlerParameter(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_isExceptionHandlerParameter(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Backend\\MultiCatch.jrag:42
   * @apilevel internal
   */
 @SideEffect.Pure public int Define_localNum(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getBlockNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\LocalNum.jrag:90
      return getNumParameter() == 0 ? 
          localNumOfFirstParameter() :
          getParameter(getNumParameter()-1).localNum() + getParameter(getNumParameter()-1).type().variableSize();
    }
    else if (_callerNode == getParameterListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\LocalNum.jrag:86
      int index = _callerNode.getIndexOfChild(_childNode);
      return index == 0 ?
          localNumOfFirstParameter() :
          getParameter(index-1).localNum() + getParameter(index-1).type().variableSize();
    }
    else {
      return getParent().Define_localNum(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_localNum(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:69
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_mayUseAnnotationTarget(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getModifiersNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Annotations.jrag:89
      return name.equals("CONSTRUCTOR");
    }
    else {
      return getParent().Define_mayUseAnnotationTarget(this, _callerNode, name);
    }
  }
  @SideEffect.Pure protected boolean canDefine_mayUseAnnotationTarget(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\VariableArityParameters.jrag:26
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_variableArityValid(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getParameterListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\VariableArityParameters.jrag:21
      int i = _callerNode.getIndexOfChild(_childNode);
      return i == getNumParameter() - 1;
    }
    else {
      return getParent().Define_variableArityValid(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_variableArityValid(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\PreciseRethrow.jrag:47
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_inhModifiedInScope(ASTNode _callerNode, ASTNode _childNode, Variable var) {
    if (_callerNode == getParameterListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\PreciseRethrow.jrag:48
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return getBlock().modifiedInScope(var);
    }
    else {
      return getParent().Define_inhModifiedInScope(this, _callerNode, var);
    }
  }
  @SideEffect.Pure protected boolean canDefine_inhModifiedInScope(ASTNode _callerNode, ASTNode _childNode, Variable var) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\PreciseRethrow.jrag:121
   * @apilevel internal
   */
 @SideEffect.Pure public boolean Define_isCatchParam(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getParameterListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\PreciseRethrow.jrag:123
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return false;
    }
    else {
      return getParent().Define_isCatchParam(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_isCatchParam(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Pure public ASTNode rewriteTo() {
    // Declared at C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupConstructor.jrag:218
    if (!hasConstructorInvocation() && !hostType().isObject()) {
      return rewriteRule0();
    }
    return super.rewriteTo();
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\LookupConstructor.jrag:218
   * @apilevel internal
   */
  private ConstructorDecl rewriteRule0() {
{
      setConstructorInvocation(
        new ExprStmt(
          new SuperConstructorAccess("super", new List())
          )
        );
      return this;
    }  }
}

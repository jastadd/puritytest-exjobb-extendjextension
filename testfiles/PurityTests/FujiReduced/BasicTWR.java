/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package AST;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Backend\\BasicTWR.ast:1
 * @production BasicTWR : {@link Stmt} ::= <span class="component">Resource:{@link ResourceDeclaration}</span> <span class="component">{@link Block}</span>;

 */
public class BasicTWR extends Stmt implements Cloneable, VariableScope {
  /**
   * The general structure of the basic try-with-resources:
   * 
   * <pre><code>
   * RESOURCE
   * BLOCK
   * 
   * Primary Exception Handler
   * Automatic Closing of Resource
   * Suppressed Exception Handler
   * re-throw primary exception
   * Automatic Closing of Resource
   * </pre></code>
   * 
   * Pseudocode for basic try-with-resources:
   * 
   * <pre><code>
   * 0  .resourceBegin
   * 1  emit RESOURCE
   * 0  store resource
   * 0  .resourceEnd
   * 
   * 0  .blockBegin
   * 0  emit BLOCK
   * 0  .blockEnd
   * 0  goto outerFinally
   * 
   * 1  .resourceException
   * 1  throw
   * 
   * #if BLOCK is not empty:
   * 
   * 1  .catchPrimary
   * 0  store primary
   * 
   * 0  .tryCloseBegin
   * 1  load resource
   * 0  ifnull innerFinally
   * 1  load resource
   * 0  invoke java.lang.AutoCloseable.close()
   * 0  .tryCloseEnd
   * 
   * 0  goto innerFinally
   * 
   * 1  .catchSuppressed
   * 0  store suppressed
   * 1  load primary
   * 2  load suppressed
   * 0  invoke java.lang.Throwable.addSuppressed(Throwable)
   * 
   * 0  .innerFinally
   * 1  load primary
   * 1  throw
   * 
   * #endif BLOCK is not empty
   * 
   * 0  .outerFinally
   * 1  load resource
   * 0  ifnull tryEnd
   * 1  load resource
   * 0  invoke java.lang.AutoCloseable.close()
   * 
   * 0  .tryEnd
   * 
   * Exception Table:
   * resourceBegin .. resourceEnd : resourceException
   * blockBegin .. blockEnd : catchPrimary
   * tryCloseBegin .. tryCloseEnd : catchSuppressed
   * </pre></code>
   * 
   * @aspect TryWithResources
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Backend\\TryWithResources.jrag:136
   */
  public void createBCode(CodeGeneration gen) {
		ResourceDeclaration resource = getResource();

		int resourceBeginLbl = hostType().constantPool().newLabel();
		int resourceEndLbl = hostType().constantPool().newLabel();
		int blockBeginLbl = hostType().constantPool().newLabel();
		int blockEndLbl = hostType().constantPool().newLabel();
		int tryCloseBeginLbl = hostType().constantPool().newLabel();
		int tryCloseEndLbl = hostType().constantPool().newLabel();

		int resourceExceptionLbl = hostType().constantPool().newLabel();
		int catchPrimaryLbl = hostType().constantPool().newLabel();
		int catchSuppressedLbl = hostType().constantPool().newLabel();
		int innerFinallyLbl = hostType().constantPool().newLabel();
		int outerFinallyLbl = hostType().constantPool().newLabel();
		int tryEndLbl = hostType().constantPool().newLabel();

		TypeDecl throwableType = lookupType("java.lang", "Throwable");
		TypeDecl resourceType = resource.type();
		TypeDecl autoCloseableType = lookupType("java.lang", "AutoCloseable");

		gen.changeStackDepth(3);
		int resourceIndex = resource.localNum();
		int primaryIndex = resourceIndex+resourceType.variableSize();
		int suppressedIndex = primaryIndex+throwableType.variableSize();

		// store the resource in local
		gen.addLabel(resourceBeginLbl);
		resource.createBCode(gen);
		gen.addLabel(resourceEndLbl);
		gen.emit(Bytecode.NOP);

		gen.addLabel(blockBeginLbl);
		getBlock().createBCode(gen);
		gen.addLabel(blockEndLbl);
		gen.emitGoto(outerFinallyLbl);

		// If there was an exception when initializing the resource
		// we need to directly rethrow the exception
		gen.addLabel(resourceExceptionLbl);
		gen.emitThrow();
		gen.addException(
			gen.addressOf(resourceBeginLbl),
			gen.addressOf(resourceEndLbl),
			gen.addressOf(resourceExceptionLbl),
			0);

		if (gen.addressOf(blockBeginLbl) != gen.addressOf(blockEndLbl)) {

			// catch primary exception
			// operand stack: .., #primary
			gen.addLabel(catchPrimaryLbl);
			throwableType.emitStoreLocal(gen, primaryIndex);

			// try-close resource
			gen.addLabel(tryCloseBeginLbl);
			{
				// if resource != null
				resourceType.emitLoadLocal(gen, resourceIndex);
				gen.emitCompare(Bytecode.IFNULL, innerFinallyLbl);
				resourceType.emitLoadLocal(gen, resourceIndex);
				closeMethod().emitInvokeMethod(gen, autoCloseableType);
			}
			gen.addLabel(tryCloseEndLbl);
			gen.emitGoto(innerFinallyLbl);
			
			// catch suppressed exception
			// operand stack: .., #primary, #suppressed
			gen.addLabel(catchSuppressedLbl);
			throwableType.emitStoreLocal(gen, suppressedIndex);
			throwableType.emitLoadLocal(gen, primaryIndex);
			throwableType.emitLoadLocal(gen, suppressedIndex);
			addSuppressedMethod().emitInvokeMethod(gen, throwableType);

			// inner finally
			// operand stack: .., #primary
			gen.addLabel(innerFinallyLbl);
			throwableType.emitLoadLocal(gen, primaryIndex);
			gen.emitThrow();

			// If there was an exception during the block of the try
			// statement, then we should try to close the resource
			gen.addException(
				gen.addressOf(blockBeginLbl),
				gen.addressOf(blockEndLbl),
				gen.addressOf(catchPrimaryLbl),
				0);

			// If an exception occurrs during the automatic closing
			// of a resource after an exception in the try block...
			gen.addException(
				gen.addressOf(tryCloseBeginLbl),
				gen.addressOf(tryCloseEndLbl),
				gen.addressOf(catchSuppressedLbl),
				0);
		}

		// outer finally
		gen.addLabel(outerFinallyLbl);
		{
			// if resource != null
			resourceType.emitLoadLocal(gen, resourceIndex);
			gen.emitCompare(Bytecode.IFNULL, tryEndLbl);
			resourceType.emitLoadLocal(gen, resourceIndex);
			closeMethod().emitInvokeMethod(gen, autoCloseableType);
		}

		gen.addLabel(tryEndLbl);
		gen.emit(Bytecode.NOP);
	}
  /**
   * Lookup the java.lang.Throwable.close() method.
   * @aspect TryWithResources
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Backend\\TryWithResources.jrag:250
   */
  private MethodDecl closeMethod() {
		TypeDecl autoCloseableType = lookupType("java.lang", "AutoCloseable");
		if (autoCloseableType == null)
			throw new Error("Could not find java.lang.AutoCloseable");
		for (MethodDecl method : (Collection<MethodDecl>)
				autoCloseableType.memberMethods("close")) {
			if (method.getNumParameter() == 0)
				return method;
		}
		throw new Error("Could not find java.lang.AutoCloseable.close()");
	}
  /**
   * Lookup the java.lang.Throwable.addSuppressed(Throwable) method.
   * @aspect TryWithResources
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Backend\\TryWithResources.jrag:265
   */
  private MethodDecl addSuppressedMethod() {
		TypeDecl throwableType = lookupType("java.lang", "Throwable");
		if (throwableType == null)
			throw new Error("Could not find java.lang.Throwable");
		for (MethodDecl method : (Collection<MethodDecl>)
				throwableType.memberMethods("addSuppressed")) {
			if (method.getNumParameter() == 1 &&
					method.getParameter(0).getTypeAccess().type() ==
					throwableType) {
				return method;
			}
		}
		throw new Error("Could not find java.lang.Throwable.addSuppressed()");
	}
  /**
   * @declaredat ASTNode:1
   */
  public BasicTWR() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
  }
  /**
   * @declaredat ASTNode:13
   */
  public BasicTWR(ResourceDeclaration p0, Block p1) {
    setChild(p0, 0);
    setChild(p1, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:18
   */
  @SideEffect.Pure protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:24
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:28
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    localLookup_String_reset();
    localVariableDeclaration_String_reset();
    lookupVariable_String_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:35
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  @SideEffect.Fresh public BasicTWR clone() throws CloneNotSupportedException {
    BasicTWR node = (BasicTWR) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:44
   */
  @SideEffect.Fresh(group="_ASTNode") public BasicTWR copy() {
    try {
      BasicTWR node = (BasicTWR) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:63
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public BasicTWR fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:73
   */
  @SideEffect.Fresh(group="_ASTNode") public BasicTWR treeCopyNoTransform() {
    BasicTWR tree = (BasicTWR) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:93
   */
  @SideEffect.Fresh(group="_ASTNode") public BasicTWR treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:98
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node);    
  }
  /**
   * Replaces the Resource child.
   * @param node The new node to replace the Resource child.
   * @apilevel high-level
   */
  public void setResource(ResourceDeclaration node) {
    setChild(node, 0);
  }
  /**
   * Retrieves the Resource child.
   * @return The current node used as the Resource child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Resource")
  @SideEffect.Pure public ResourceDeclaration getResource() {
    return (ResourceDeclaration) getChild(0);
  }
  /**
   * Retrieves the Resource child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Resource child.
   * @apilevel low-level
   */
  public ResourceDeclaration getResourceNoTransform() {
    return (ResourceDeclaration) getChildNoTransform(0);
  }
  /**
   * Replaces the Block child.
   * @param node The new node to replace the Block child.
   * @apilevel high-level
   */
  public void setBlock(Block node) {
    setChild(node, 1);
  }
  /**
   * Retrieves the Block child.
   * @return The current node used as the Block child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Block")
  @SideEffect.Pure public Block getBlock() {
    return (Block) getChild(1);
  }
  /**
   * Retrieves the Block child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Block child.
   * @apilevel low-level
   */
  public Block getBlockNoTransform() {
    return (Block) getChildNoTransform(1);
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void localLookup_String_reset() {
    localLookup_String_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="localLookup_String") protected java.util.Map localLookup_String_values;

  /**
   * @attribute syn
   * @aspect MultiCatch
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Backend\\MultiCatch.jrag:78
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="MultiCatch", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Backend\\MultiCatch.jrag:78")
  @SideEffect.Pure(group="localLookup_String") public SimpleSet localLookup(String name) {
    Object _parameters = name;
    if (localLookup_String_values == null) localLookup_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (localLookup_String_values.containsKey(_parameters)) {
      return (SimpleSet) localLookup_String_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    SimpleSet localLookup_String_value = localLookup_compute(name);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    localLookup_String_values.put(_parameters, localLookup_String_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return localLookup_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private SimpleSet localLookup_compute(String name) {
  		VariableDeclaration v = localVariableDeclaration(name);
  		if (v != null) return v;
  		return lookupVariable(name);
  	}
  /** @apilevel internal */
  @SideEffect.Ignore private void localVariableDeclaration_String_reset() {
    localVariableDeclaration_String_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="localVariableDeclaration_String") protected java.util.Map localVariableDeclaration_String_values;

  /**
   * @attribute syn
   * @aspect MultiCatch
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Backend\\MultiCatch.jrag:83
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="MultiCatch", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Backend\\MultiCatch.jrag:83")
  @SideEffect.Pure(group="localVariableDeclaration_String") public VariableDeclaration localVariableDeclaration(String name) {
    Object _parameters = name;
    if (localVariableDeclaration_String_values == null) localVariableDeclaration_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (localVariableDeclaration_String_values.containsKey(_parameters)) {
      return (VariableDeclaration) localVariableDeclaration_String_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    VariableDeclaration localVariableDeclaration_String_value = getResource().declaresVariable(name) ? getResource() : null;
    if (isFinal && _boundaries == state().boundariesCrossed) {
    localVariableDeclaration_String_values.put(_parameters, localVariableDeclaration_String_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return localVariableDeclaration_String_value;
  }
  /**
   * @attribute syn
   * @aspect PreciseRethrow
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\PreciseRethrow.jrag:55
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="PreciseRethrow", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\PreciseRethrow.jrag:55")
  @SideEffect.Pure(group="modifiedInScope_Variable") public boolean modifiedInScope(Variable var) {
    boolean modifiedInScope_Variable_value = getBlock().modifiedInScope(var);
    return modifiedInScope_Variable_value;
  }
  /**
   * @attribute inh
   * @aspect MultiCatch
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Backend\\MultiCatch.jrag:87
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.INH)
  @ASTNodeAnnotation.Source(aspect="MultiCatch", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Backend\\MultiCatch.jrag:87")
  @SideEffect.Pure(group="lookupVariable_String") public SimpleSet lookupVariable(String name) {
    Object _parameters = name;
    if (lookupVariable_String_values == null) lookupVariable_String_values = new java.util.HashMap(4);
    ASTState state = state();
    if (lookupVariable_String_values.containsKey(_parameters)) {
      return (SimpleSet) lookupVariable_String_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    SimpleSet lookupVariable_String_value = getParent().Define_lookupVariable(this, null, name);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    lookupVariable_String_values.put(_parameters, lookupVariable_String_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return lookupVariable_String_value;
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void lookupVariable_String_reset() {
    lookupVariable_String_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="lookupVariable_String") protected java.util.Map lookupVariable_String_values;

  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Backend\\MultiCatch.jrag:87
   * @apilevel internal
   */
 @SideEffect.Pure public SimpleSet Define_lookupVariable(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getBlockNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Backend\\MultiCatch.jrag:76
      return localLookup(name);
    }
    else {
      return getParent().Define_lookupVariable(this, _callerNode, name);
    }
  }
  @SideEffect.Pure protected boolean canDefine_lookupVariable(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Backend\\MultiCatch.jrag:42
   * @apilevel internal
   */
 @SideEffect.Pure public int Define_localNum(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getBlockNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Backend\\TryWithResources.jrag:292
      return getResource().localNum() +
      		getResource().type().variableSize() +
      		2 * lookupType("java.lang", "Throwable").variableSize();
    }
    else if (_callerNode == getResourceNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Backend\\TryWithResources.jrag:283
      return localNum();
    }
    else {
      return getParent().Define_localNum(this, _callerNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_localNum(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Pure public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}

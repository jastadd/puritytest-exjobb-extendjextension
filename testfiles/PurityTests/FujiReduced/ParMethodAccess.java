/* This file was generated with JastAdd2 (http://jastadd.org) version 2.2.2 */
package AST;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast node
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericMethods.ast:10
 * @production ParMethodAccess : {@link MethodAccess} ::= <span class="component">TypeArgument:{@link Access}*</span>;

 */
public class ParMethodAccess extends MethodAccess implements Cloneable {
  /**
   * @aspect GenericMethods
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericMethods.jrag:11
   */
  public void typeCheck() {
    super.typeCheck();
    if(!decl().hostType().isUnknown()) {
      if(!(decl() instanceof ParMethodDecl))
        error("can not have type parameters on a non generic method");
      else {
        ParMethodDecl m = (ParMethodDecl)decl();
        if(!(m instanceof RawMethodDecl) && m.numTypeParameter() != getNumTypeArgument())
          error("generic method " + m.signature() + " requires " + m.numTypeParameter() + " type arguments");
        else {
        }
      }
    }
  }
  /**
   * @aspect GenericMethodsPrettyPrint
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericMethods.jrag:133
   */
  public void toString(StringBuffer s) {
    s.append("<");
    for(int i = 0; i < getNumTypeArgument(); i++) {
      if(i != 0) s.append(", ");
      getTypeArgument(i).toString(s);
    }
    s.append(">");
    super.toString(s);
  }
  /**
   * @declaredat ASTNode:1
   */
  public ParMethodAccess() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  @SideEffect.Ignore public void init$Children() {
    children = new ASTNode[2];
    setChild(new List(), 0);
    setChild(new List(), 1);
  }
  /**
   * @declaredat ASTNode:15
   */
  public ParMethodAccess(String p0, List<Expr> p1, List<Access> p2) {
    setID(p0);
    setChild(p1, 0);
    setChild(p2, 1);
  }
  /**
   * @declaredat ASTNode:20
   */
  public ParMethodAccess(beaver.Symbol p0, List<Expr> p1, List<Access> p2) {
    setID(p0);
    setChild(p1, 0);
    setChild(p2, 1);
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:26
   */
  @SideEffect.Pure protected int numChildren() {
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:32
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:36
   */
  @SideEffect.Ignore public void flushAttrCache() {
    super.flushAttrCache();
    typeArguments_MethodDecl_reset();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  @SideEffect.Ignore public void flushCollectionCache() {
    super.flushCollectionCache();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  @SideEffect.Fresh public ParMethodAccess clone() throws CloneNotSupportedException {
    ParMethodAccess node = (ParMethodAccess) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:50
   */
  @SideEffect.Fresh(group="_ASTNode") public ParMethodAccess copy() {
    try {
      ParMethodAccess node = (ParMethodAccess) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:69
   */
  @Deprecated
  @SideEffect.Fresh(group="_ASTNode") public ParMethodAccess fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:79
   */
  @SideEffect.Fresh(group="_ASTNode") public ParMethodAccess treeCopyNoTransform() {
    ParMethodAccess tree = (ParMethodAccess) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.setChild(child, i);
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:99
   */
  @SideEffect.Fresh(group="_ASTNode") public ParMethodAccess treeCopy() {
    doFullTraversal();
    return treeCopyNoTransform();
  }
  /** @apilevel internal 
   * @declaredat ASTNode:104
   */
  @SideEffect.Pure(group="_ASTNode") protected boolean is$Equal(ASTNode node) {
    return super.is$Equal(node) && (tokenString_ID == ((ParMethodAccess) node).tokenString_ID);    
  }
  /**
   * Replaces the lexeme ID.
   * @param value The new value for the lexeme ID.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void setID(String value) {
    tokenString_ID = value;
  }
  /**
   * JastAdd-internal setter for lexeme ID using the Beaver parser.
   * @param symbol Symbol containing the new value for the lexeme ID
   * @apilevel internal
   */
  public void setID(beaver.Symbol symbol) {
    if (symbol.value != null && !(symbol.value instanceof String))
    throw new UnsupportedOperationException("setID is only valid for String lexemes");
    tokenString_ID = (String)symbol.value;
    IDstart = symbol.getStart();
    IDend = symbol.getEnd();
  }
  /**
   * Retrieves the value for the lexeme ID.
   * @return The value for the lexeme ID.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="ID")
  @SideEffect.Pure(group="_ASTNode") public String getID() {
    return tokenString_ID != null ? tokenString_ID : "";
  }
  /**
   * Replaces the Arg list.
   * @param list The new list node to be used as the Arg list.
   * @apilevel high-level
   */
  public void setArgList(List<Expr> list) {
    setChild(list, 0);
  }
  /**
   * Retrieves the number of children in the Arg list.
   * @return Number of children in the Arg list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumArg() {
    return getArgList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Arg list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Arg list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumArgNoTransform() {
    return getArgListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Arg list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Arg list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Expr getArg(int i) {
    return (Expr) getArgList().getChild(i);
  }
  /**
   * Check whether the Arg list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasArg() {
    return getArgList().getNumChild() != 0;
  }
  /**
   * Append an element to the Arg list.
   * @param node The element to append to the Arg list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addArg(Expr node) {
    List<Expr> list = (parent == null) ? getArgListNoTransform() : getArgList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addArgNoTransform(Expr node) {
    List<Expr> list = getArgListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the Arg list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setArg(Expr node, int i) {
    List<Expr> list = getArgList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the Arg list.
   * @return The node representing the Arg list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Arg")
  @SideEffect.Pure(group="_ASTNode") public List<Expr> getArgList() {
    List<Expr> list = (List<Expr>) getChild(0);
    return list;
  }
  /**
   * Retrieves the Arg list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Arg list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Expr> getArgListNoTransform() {
    return (List<Expr>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the Arg list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Expr getArgNoTransform(int i) {
    return (Expr) getArgListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Arg list.
   * @return The node representing the Arg list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Expr> getArgs() {
    return getArgList();
  }
  /**
   * Retrieves the Arg list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Arg list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Expr> getArgsNoTransform() {
    return getArgListNoTransform();
  }
  /**
   * Replaces the TypeArgument list.
   * @param list The new list node to be used as the TypeArgument list.
   * @apilevel high-level
   */
  public void setTypeArgumentList(List<Access> list) {
    setChild(list, 1);
  }
  /**
   * Retrieves the number of children in the TypeArgument list.
   * @return Number of children in the TypeArgument list.
   * @apilevel high-level
   */
  @SideEffect.Pure public int getNumTypeArgument() {
    return getTypeArgumentList().getNumChild();
  }
  /**
   * Retrieves the number of children in the TypeArgument list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the TypeArgument list.
   * @apilevel low-level
   */
  @SideEffect.Pure public int getNumTypeArgumentNoTransform() {
    return getTypeArgumentListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the TypeArgument list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the TypeArgument list.
   * @apilevel high-level
   */
  @SideEffect.Pure public Access getTypeArgument(int i) {
    return (Access) getTypeArgumentList().getChild(i);
  }
  /**
   * Check whether the TypeArgument list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  @SideEffect.Pure public boolean hasTypeArgument() {
    return getTypeArgumentList().getNumChild() != 0;
  }
  /**
   * Append an element to the TypeArgument list.
   * @param node The element to append to the TypeArgument list.
   * @apilevel high-level
   */
  @SideEffect.Local(group="_ASTNode") public void addTypeArgument(Access node) {
    List<Access> list = (parent == null) ? getTypeArgumentListNoTransform() : getTypeArgumentList();
    list.addChild(node);
  }
  /** @apilevel low-level 
   */
  public void addTypeArgumentNoTransform(Access node) {
    List<Access> list = getTypeArgumentListNoTransform();
    list.addChild(node);
  }
  /**
   * Replaces the TypeArgument list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public void setTypeArgument(Access node, int i) {
    List<Access> list = getTypeArgumentList();
    list.setChild(node, i);
  }
  /**
   * Retrieves the TypeArgument list.
   * @return The node representing the TypeArgument list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="TypeArgument")
  @SideEffect.Pure(group="_ASTNode") public List<Access> getTypeArgumentList() {
    List<Access> list = (List<Access>) getChild(1);
    return list;
  }
  /**
   * Retrieves the TypeArgument list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the TypeArgument list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Access> getTypeArgumentListNoTransform() {
    return (List<Access>) getChildNoTransform(1);
  }
  /**
   * @return the element at index {@code i} in the TypeArgument list without
   * triggering rewrites.
   */
  @SideEffect.Pure public Access getTypeArgumentNoTransform(int i) {
    return (Access) getTypeArgumentListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the TypeArgument list.
   * @return The node representing the TypeArgument list.
   * @apilevel high-level
   */
  @SideEffect.Pure public List<Access> getTypeArguments() {
    return getTypeArgumentList();
  }
  /**
   * Retrieves the TypeArgument list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the TypeArgument list.
   * @apilevel low-level
   */
  @SideEffect.Pure public List<Access> getTypeArgumentsNoTransform() {
    return getTypeArgumentListNoTransform();
  }
  /** @apilevel internal */
  @SideEffect.Ignore private void typeArguments_MethodDecl_reset() {
    typeArguments_MethodDecl_values = null;
  }
  /** @apilevel internal */
   @SideEffect.Secret(group="typeArguments_MethodDecl") protected java.util.Map typeArguments_MethodDecl_values;

  /**
   * @attribute syn
   * @aspect MethodSignature15
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\MethodSignature.jrag:287
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="MethodSignature15", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\MethodSignature.jrag:287")
  @SideEffect.Pure(group="typeArguments_MethodDecl") public ArrayList typeArguments(MethodDecl m) {
    Object _parameters = m;
    if (typeArguments_MethodDecl_values == null) typeArguments_MethodDecl_values = new java.util.HashMap(4);
    ASTState state = state();
    if (typeArguments_MethodDecl_values.containsKey(_parameters)) {
      return (ArrayList) typeArguments_MethodDecl_values.get(_parameters);
    }
    int _boundaries = state.boundariesCrossed;
    boolean isFinal = this.is$Final();
    state().enterLazyAttribute();
    ArrayList typeArguments_MethodDecl_value = typeArguments_compute(m);
    if (isFinal && _boundaries == state().boundariesCrossed) {
    typeArguments_MethodDecl_values.put(_parameters, typeArguments_MethodDecl_value);
    
    } else {
    }
    state().leaveLazyAttribute();
    return typeArguments_MethodDecl_value;
  }
  /** @apilevel internal */
  @SideEffect.Pure private ArrayList typeArguments_compute(MethodDecl m) {
      ArrayList typeArguments = new ArrayList();
      for(int i = 0; i < getNumTypeArgument(); i++)
        typeArguments.add(getTypeArgument(i).type());
      return typeArguments;
    }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Frontend\\SyntacticClassification.jrag:20
   * @apilevel internal
   */
 @SideEffect.Pure public NameType Define_nameType(ASTNode _callerNode, ASTNode _childNode) {
    if (_callerNode == getTypeArgumentListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericMethods.jrag:96
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return NameType.TYPE_NAME;
    }
    else {
      return super.Define_nameType(_callerNode, _childNode);
    }
  }
  @SideEffect.Pure protected boolean canDefine_nameType(ASTNode _callerNode, ASTNode _childNode) {
    return true;
  }
  /**
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericMethods.jrag:116
   * @apilevel internal
   */
 @SideEffect.Pure public SimpleSet Define_lookupType(ASTNode _callerNode, ASTNode _childNode, String name) {
    if (_callerNode == getTypeArgumentListNoTransform()) {
      // @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\GenericMethods.jrag:97
      int childIndex = _callerNode.getIndexOfChild(_childNode);
      return unqualifiedScope().lookupType(name);
    }
    else {
      return super.Define_lookupType(_callerNode, _childNode, name);
    }
  }
  @SideEffect.Pure protected boolean canDefine_lookupType(ASTNode _callerNode, ASTNode _childNode, String name) {
    return true;
  }
  /** @apilevel internal */
  @SideEffect.Pure public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
}

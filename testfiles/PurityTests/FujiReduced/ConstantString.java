package AST;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast class
 * @aspect ConstantPool
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\ConstantPool.jrag:370
 */
public class ConstantString extends CPInfo {
  
    private int name;

  
    public ConstantString(int name) {
      this.name = name;
    }

  
    public void emit(DataOutputStream out) throws IOException {
      out.writeByte(ConstantPool.CONSTANT_String);
      out.writeChar(name);
    }

  
    public String toString() {
      return pos + " ConstantString: tag " + ConstantPool.CONSTANT_String + ", string_index: " + name;
    }


}

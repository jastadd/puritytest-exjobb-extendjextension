package AST;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast interface
 * @aspect Variable
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java7Frontend\\Variable.jadd:12
 */
 interface Variable {

		 
		public String name();

		 
		public TypeDecl type();

		 
		public Collection<TypeDecl> throwTypes();

		 
		public boolean isParameter();

		// 4.5.3
		 
		// 4.5.3
		public boolean isClassVariable();

		 
		public boolean isInstanceVariable();

		 
		public boolean isMethodParameter();

		 
		public boolean isConstructorParameter();

		 
		public boolean isExceptionHandlerParameter();

		 
		public boolean isLocalVariable();

		// 4.5.4
		 
		// 4.5.4
		public boolean isFinal();

		 
		public boolean isVolatile();


		 

		public boolean isBlank();

		 
		public boolean isStatic();

		 
		public boolean isSynthetic();


		 

		public TypeDecl hostType();


		 

		public Expr getInit();

		 
		public boolean hasInit();


		 

		public Constant constant();


		 

		public Modifiers getModifiers();
  /**
   * @attribute syn
   * @aspect SourceDeclarations
   * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1281
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="SourceDeclarations", declaredAt="C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.5Frontend\\Generics.jrag:1281")
  @SideEffect.Pure(group="sourceVariableDecl") public Variable sourceVariableDecl();
}

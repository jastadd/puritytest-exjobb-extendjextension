package AST;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast class
 * @aspect Attributes
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\lib\\JastAddJ\\Java1.4Backend\\Attributes.jrag:50
 */
 class ConstantValueAttribute extends Attribute {
  
    public ConstantValueAttribute(ConstantPool p, FieldDeclaration f) {
      super(p, "ConstantValue");
      int constantvalue_index = f.type().addConstant(p, f.getInit().constant());
      u2(constantvalue_index);
    }


}

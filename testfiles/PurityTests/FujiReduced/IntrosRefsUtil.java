package AST;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.io.File;
import java.util.*;
import beaver.*;
import java.util.ArrayList;
import java.util.zip.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Collection;
import sun.awt.im.CompositionArea;
import fuji.CompositionErrorException;
/**
 * @ast class
 * @aspect IntrosRefsUtil
 * @declaredat C:\\Users\\Mikael\\Downloads\\fuji-sources-2012-03-26\\fuji-sources-2012-03-26\\FujiCompiler\\src\\ExtIntrosRefs.jrag:365
 */
public class IntrosRefsUtil extends java.lang.Object {
  
        
        public static final String ALLOW_MULTIPLE_DECLARATIONS = "-allowMultipleDeclarations";

  
        public static final String DELIM = " ";

  
        public static final String INTRO = "introduces";

  
        public static final String REF = "REF";

  
        public static final String ORIGREF = "ORI";

  
        public static final String EXTREF = "EXT";

  
        public static final String IMPREF = "IMP";

  
        public static final String THRREF = "THR";

  

        
        public static final String DELIM_FEATURE = "{feature}";

  
        public static final String DELIM_STATIC = "{static}";

  
        public static final String DELIM_TYPE = "{type}";

  
        public static final String DELIM_PKG = "{package}";

  
        public static final String DELIM_FIELD = "{field}";

  
        public static final String DELIM_METHOD = "{method}";

  
        public static final String DELIM_CONS = "{constructor}";

  

        /*
         * Calculates a prefix for introduces relation containing a feature
         * name.
         */
        public static String introPrefix(ASTNode node, java.util.List<String> featureModulePathnames) {
            String featureModuleName = 
                    new File(featureModulePathnames.get(node.featureID())).getName();
            return INTRO + DELIM + featureModuleName + DELIM;
        }

  

        /*
         * Calculates a fully qualified name of a type.  For inner (i.e. nested 
         * non-static) or nested (i.e. nested static) classes the name includes 
         * all the enclosing types.
         */
        public static String typeDeclQName(TypeDecl td) {
            String id = td.getID();
            if (td.isInnerType() || td.isNestedType()) {
                return typeDeclQName(td.enclosingType()) + DELIM_TYPE + id;
            } else {
                return DELIM_PKG + td.packageName() + DELIM_TYPE + id;
            }
        }

  

        /*
         * Calculates a signature for the constructor declaration.
         */
        public static String signature(ConstructorDecl cond) {
            return sigHelper(cond.name(), cond.getParameterList()); 
        }

  

        /*
         * Calculates a signature for the method declaration.
         */
        public static String signature(MethodDecl md) {
            return sigHelper(md.name(), md.getParameterList());
        }

  
        
        private static String sigHelper(String name,
                                        AST.List<ParameterDeclaration> params) {
            StringBuffer s = new StringBuffer();
            s.append(name + "(");
            for(int i = 0; i < params.getNumChild(); i++) {
                if(i != 0) s.append(", ");
                // add erasure() after type() to remove generic parameter type.
                s.append(params.getChild(i).type().typeName());
                // add following lines to have parameter names in the signature.
                //.append(" ")
                //.append(params.getChild(i).getID());
            }
            s.append(")");
            return s.toString();
        }

  

        /*
         * Calculates a visible enclosing type for the given body declaration.
         * The enclosing types are processed recursively untill the first
         * non-anonymous and not-nested type (i.e. visible type) is found.
         */
        public static TypeDecl visibleHostType(BodyDecl bd) {
            TypeDecl host = bd.hostType();
            return visibleHostType(host);
        }

  

        /*
         * Calculates a visible enclosing type for the given type declaration.
         * The enclosing types are processed recursively untill the first
         * non-anonymous and not-nested type (i.e. visible type) is found.
         */
        public static TypeDecl visibleHostType(TypeDecl td) {
            while (td.isAnonymous() || td.isLocalClass())
                td=td.enclosingType();
            return td;
        }

  

        /*
         * Returns a string representation of a source in a REF relation for 
         * the given expression.
         */
        public static String formatRefSource(String featureName, Expr expr) {
            StringBuilder source = new StringBuilder();
            TypeDecl host = visibleHostType(expr.hostType());
            if (! featureName.isEmpty())
                source.append(DELIM_FEATURE + featureName);
            source.append(typeDeclQName(host));
            BodyDecl enclosing = expr.enclosingBodyDecl();
            if (enclosing != null) {
                // TODO else if branch for FieldDecl
                if (enclosing instanceof MethodDecl) {
                    source.append(DELIM_METHOD);
                    source.append(signature((MethodDecl) enclosing));
                } else if (enclosing instanceof ConstructorDecl) {
                    source.append(DELIM_CONS);
                    source.append(signature((ConstructorDecl) enclosing));
                } else if (enclosing instanceof FieldDeclaration) {
                    source.append(DELIM_FIELD);
                    source.append(((FieldDeclaration) enclosing).getID());
                } else {
                    // static or dynamic initialitzers, do nothing.
                }
            }
            return source.toString();
        }


}

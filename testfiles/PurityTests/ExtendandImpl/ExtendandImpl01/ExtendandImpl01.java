import lang.ast.SideEffect.*;
public class ExtendandImpl01 extends Parent implements Interface01{
	 public int freshMethod(){return 1;}
	 public int pureMethod(){return 1;}
}

public class Parent{
	@Fresh public int freshMethod(){return 1;}
	@Pure public int pureMethod(){return 1;}
}

public interface Interface01{
	public int freshMethod();
	public int pureMethod();
}
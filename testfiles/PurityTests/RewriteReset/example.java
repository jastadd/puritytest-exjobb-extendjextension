//Annotation example
import lang.ast.SideEffect.*;

public class ASTNode{
	@Secret(group="A") int[] stateA=null;
	@Secret(group="A") ASTNode stateB=null;
	@Secret(group="B") int stateC=0;
	@Ignore private void stateoperations(){
		stateB=null;
	}
		
	@Pure(group="A") public void setStateB(ASTNode b){
		stateB=b;
	}
	
	@Pure public ASTNode(@Fresh ASTNode b){
		setStateB(b);
	}
	
	@Pure(group="B") public int calcC() {
		if (stateC!=0)
			return stateC;
		stateC=calcC_compute();
		return stateC;
	}
	
	@Pure(group="A") private int calcC_compute(){
		return stateB.calcC()+stateA[0]*stateA[1];
	}
	
	@Ignore private void reset(){
		
	}
	
	@Pure public ASTNode rewriteTo(){
		return this;
	}
}
public class example extends ASTNode {

	@Pure public ASTNode rewriteTo(){
		if (condition())
			rewriterule01();
		super.rewriteTo();
	}
	@Pure public boolean condition(){
		return super.calcC()>6;
	}
	@Fresh(group="A",self=true) public ASTNode rewriterule01(){
		stateB=this;
		return new example(stateB);
	}
	
	@Pure public example(@Fresh ASTNode b){
		super(b);
	}
	
	@Secret(group="X") private boolean b=false;
	@Secret(group="X") private Set<ASTNode> visited=null;
 	@Secret(group="X") private List<ASTNode> X_value=null;
 	
 	@Pure(group="X") public List<ASTNode> X(){
 		return null;
 	}
	
}
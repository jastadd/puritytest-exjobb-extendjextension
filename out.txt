Tests: 1
FreshCheck:time:2047ms
testfiles\PurityTests\ReportExamples\FreshCheck\freshAnalysisb.java
Method is annotated Fresh.
For Java method not part of attribute calculation

Purity problem in category:FRESHRETURN
22:return test.LT;of Freshness Maybe can't be returned from a @Fresh method because is not fresh because 
->10_test.LT
->the variable test is initizised to new Test(2) that's not fresh.
It's only of freshness Fresh
-> the constructor new Test(2) is not Fresh
->the variable test is initizised to new Test(2) that's not fresh.
It's only of freshness Fresh.

in testfiles\PurityTests\ReportExamples\FreshCheck\freshAnalysisb.java:Test

1 warnings generated

6:this.x is Fresh
UnknownType in calculation of FreshAnalysis#param
Test.Test(int) callokey Any methad annotated @Impure may call any other
12:this.x is Fresh
13:10_test is Fresh
Test callokey Any methad annotated @Impure may call any other
14:param is Fresh
16:10_test is Fresh
18:10_test is Fresh
20:10_test is Fresh
Test callokey Any methad annotated @Impure may call any other
22 10_test+preds::[else-end, then-end]
0
11 10_test+preds::[Definition 20]
0
11 10_test+preds::[Definition 18]
22 10_test+preds::[else-end, then-end]
0
11 10_test+preds::[Definition 20]
0
11 10_test+preds::[Definition 18]
22 All definers::[Definition 20, Definition 18]
Here: Test
Fresh
18 param+preds::[else-end, then-end]
0
12 param+preds::[Definition 16]
16 param+preds::[if (x = 10)]
12 param+preds::[if (x > param.x)]
11 param+preds::[Test()]
10 param+preds::[Definition 10]
10 param+preds::[entry]
0
12 param+preds::[Definition 14]
18 param+preds::[else-end, then-end]
0
12 param+preds::[Definition 16]
16 param+preds::[if (x = 10)]
12 param+preds::[if (x > param.x)]
11 param+preds::[Test()]
10 param+preds::[Definition 10]
10 param+preds::[entry]
0
12 param+preds::[Definition 14]
18 All definers::[entry, Definition 14]
Maybe
Maybe
22 10_test.10_test+preds::[else-end, then-end]
0
11 10_test.10_test+preds::[Definition 20]
20 10_test.10_test+preds::[@primitive.Unknown()]
20 10_test.10_test+preds::[if (x > param.x)]
11 10_test.10_test+preds::[Test()]
10 10_test.10_test+preds::[Definition 10]
0
11 10_test.10_test+preds::[Definition 18]
18 10_test.10_test+preds::[else-end, then-end]
0
12 10_test.10_test+preds::[Definition 16]
16 10_test.10_test+preds::[if (x = 10)]
12 10_test.10_test+preds::[if (x > param.x)]
0
12 10_test.10_test+preds::[Definition 14]
14 10_test.10_test+preds::[Definition 13]
13 10_test.10_test+preds::[@primitive.Unknown()]
13 10_test.10_test+preds::[if (x = 10)]
22 10_test.10_test+preds::[else-end, then-end]
0
11 10_test.10_test+preds::[Definition 20]
20 10_test.10_test+preds::[@primitive.Unknown()]
20 10_test.10_test+preds::[if (x > param.x)]
11 10_test.10_test+preds::[Test()]
10 10_test.10_test+preds::[Definition 10]
0
11 10_test.10_test+preds::[Definition 18]
18 10_test.10_test+preds::[else-end, then-end]
0
12 10_test.10_test+preds::[Definition 16]
16 10_test.10_test+preds::[if (x = 10)]
12 10_test.10_test+preds::[if (x > param.x)]
0
12 10_test.10_test+preds::[Definition 14]
14 10_test.10_test+preds::[Definition 13]
13 10_test.10_test+preds::[@primitive.Unknown()]
13 10_test.10_test+preds::[if (x = 10)]
22 All definers::[Definition 10]
Here: Test
Maybe
22 checked Maybe
10_test.10_test.LT 11:NOCHANGE%0
11 10_test.10_test.LT+preds::[Definition 20]
20 10_test.10_test.LT+preds::[@primitive.Unknown()]
20 10_test.10_test.LT+preds::[if (x > param.x)]
11 10_test.10_test.LT+preds::[Test()]
10 10_test.10_test.LT+preds::[Definition 10]
11 10_test.10_test.LT+preds::[Definition 20]
20 10_test.10_test.LT+preds::[@primitive.Unknown()]
20 10_test.10_test.LT+preds::[if (x > param.x)]
11 10_test.10_test.LT+preds::[Test()]
10 10_test.10_test.LT+preds::[Definition 10]
11 All definers::[Definition 10]
Maybe
10_test.10_test.LT 11:NOCHANGE%0
11 10_test.10_test.LT+preds::[Definition 18]
18 10_test.10_test.LT+preds::[else-end, then-end]
0
12 10_test.10_test.LT+preds::[Definition 16]
16 10_test.10_test.LT+preds::[if (x = 10)]
12 10_test.10_test.LT+preds::[if (x > param.x)]
11 10_test.10_test.LT+preds::[Test()]
10 10_test.10_test.LT+preds::[Definition 10]
0
12 10_test.10_test.LT+preds::[Definition 14]
14 10_test.10_test.LT+preds::[Definition 13]
13 10_test.10_test.LT+preds::[@primitive.Unknown()]
13 10_test.10_test.LT+preds::[if (x = 10)]
11 10_test.10_test.LT+preds::[Definition 18]
18 10_test.10_test.LT+preds::[else-end, then-end]
0
12 10_test.10_test.LT+preds::[Definition 16]
16 10_test.10_test.LT+preds::[if (x = 10)]
12 10_test.10_test.LT+preds::[if (x > param.x)]
11 10_test.10_test.LT+preds::[Test()]
10 10_test.10_test.LT+preds::[Definition 10]
0
12 10_test.10_test.LT+preds::[Definition 14]
14 10_test.10_test.LT+preds::[Definition 13]
13 10_test.10_test.LT+preds::[@primitive.Unknown()]
13 10_test.10_test.LT+preds::[if (x = 10)]
11 All definers::[Definition 10]
Maybe
10_test.10_test.LT 20:Fresh%0
20 10_test.10_test.LT+preds::[@primitive.Unknown()]
20 10_test.10_test.LT+preds::[if (x > param.x)]
11 10_test.10_test.LT+preds::[Test()]
10 10_test.10_test.LT+preds::[Definition 10]
20 All definers::[Definition 10]
Maybe
10_test.10_test.LT 18:Maybe%0
18 10_test.10_test.LT+preds::[else-end, then-end]
0
12 10_test.10_test.LT+preds::[Definition 16]
16 10_test.10_test.LT+preds::[if (x = 10)]
12 10_test.10_test.LT+preds::[if (x > param.x)]
11 10_test.10_test.LT+preds::[Test()]
10 10_test.10_test.LT+preds::[Definition 10]
0
12 10_test.10_test.LT+preds::[Definition 14]
14 10_test.10_test.LT+preds::[Definition 13]
13 10_test.10_test.LT+preds::[@primitive.Unknown()]
13 10_test.10_test.LT+preds::[if (x = 10)]
18 All definers::[Definition 10]
Maybe
10_test.10_test.LT 20:NOCHANGE%0
20 10_test.10_test.LT+preds::[if (x > param.x)]
11 10_test.10_test.LT+preds::[Test()]
10 10_test.10_test.LT+preds::[Definition 10]
20 All definers::[Definition 10]
Maybe
10_test.10_test.LT 12:NOCHANGE%0
12 10_test.10_test.LT+preds::[Definition 16]
16 10_test.10_test.LT+preds::[if (x = 10)]
12 10_test.10_test.LT+preds::[if (x > param.x)]
11 10_test.10_test.LT+preds::[Test()]
10 10_test.10_test.LT+preds::[Definition 10]
12 All definers::[Definition 10]
Maybe
10_test.10_test.LT 12:NOCHANGE%0
12 10_test.10_test.LT+preds::[Definition 14]
14 10_test.10_test.LT+preds::[Definition 13]
13 10_test.10_test.LT+preds::[@primitive.Unknown()]
13 10_test.10_test.LT+preds::[if (x = 10)]
12 10_test.10_test.LT+preds::[if (x > param.x)]
11 10_test.10_test.LT+preds::[Test()]
10 10_test.10_test.LT+preds::[Definition 10]
12 All definers::[Definition 10]
Maybe
10_test.10_test.LT 11:NOCHANGE%0
11 10_test.10_test.LT+preds::[Test()]
10 10_test.10_test.LT+preds::[Definition 10]
11 All definers::[Definition 10]
Maybe
16 LT+preds::[if (x = 10)]
12 LT+preds::[if (x > param.x)]
11 LT+preds::[Test()]
10 LT+preds::[Definition 10]
10 LT+preds::[entry]
16 LT+preds::[if (x = 10)]
12 LT+preds::[if (x > param.x)]
11 LT+preds::[Test()]
10 LT+preds::[Definition 10]
10 LT+preds::[entry]
16 All definers::[entry]
LOCAL
Finished at LOCAL
10_test.10_test.LT 16:LOCAL%0
16 10_test.10_test.LT+preds::[if (x = 10)]
12 10_test.10_test.LT+preds::[if (x > param.x)]
16 All definers::[Definition 10]
Maybe
14 10_test+preds::[Definition 13]
14 10_test+preds::[Definition 13]
14 All definers::[Definition 13]
Here: Test
Fresh
Finished at Fresh
10_test.10_test.LT 14:Fresh%0
14 10_test.10_test.LT+preds::[Definition 13]
13 10_test.10_test.LT+preds::[@primitive.Unknown()]
13 10_test.10_test.LT+preds::[if (x = 10)]
12 10_test.10_test.LT+preds::[if (x > param.x)]
14 All definers::[Definition 10]
Maybe
10_test.10_test.LT 10:NOCHANGE%0
10 10_test.10_test.LT+preds::[Definition 10]
10 All definers::[Definition 10]
Maybe
10_test.10_test.LT 12:NOCHANGE%0
12 10_test.10_test.LT+preds::[if (x > param.x)]
12 All definers::[Definition 10]
Maybe
10_test.10_test.LT 13:Fresh%0
13 10_test.10_test.LT+preds::[@primitive.Unknown()]
13 10_test.10_test.LT+preds::[if (x = 10)]
13 All definers::[Definition 10]
Maybe
10_test.10_test.LT 10:Maybe%0
10compare not similar:10_test.10_test!=new Test.Test(int)
Inital:10_test.10_test.LT
org.extendj.ast.VarAccess
10_test.10_test.LT
org.extendj.ast.VariableDeclarator
10_test.10_test.LT
org.extendj.ast.ClassInstanceExpr
10compare not similar:10_test.10_test!=new Test.Test(int)
10_test.10_test.LT
org.extendj.ast.ConstructorDecl
10_test.10_test.LT
22determined fresh because:is not fresh because 
->10_test.LT
->the variable test is initizised to new Test(2) that's not fresh.
It's only of freshness Fresh
-> the constructor new Test(2) is not Fresh
->the variable test is initizised to new Test(2) that's not fresh.
It's only of freshness Fresh.
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 20:Fresh%0
10_test.10_test.LT 18:Maybe%0
10_test.10_test.LT 20:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 16:LOCAL%0
10_test.10_test.LT 14:Fresh%0
10_test.10_test.LT 10:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 13:Fresh%0
10_test.10_test.LT 10:Maybe%0
10compare not similar:10_test.10_test!=new Test.Test(int)
The defining node wasorg.extendj.ast.ConstructorDecl
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 20:Fresh%0
10_test.10_test.LT 18:Maybe%0
10_test.10_test.LT 20:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 16:LOCAL%0
10_test.10_test.LT 14:Fresh%0
10_test.10_test.LT 10:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 13:Fresh%0
10_test.10_test.LT 10:Maybe%0
10compare not similar:10_test.10_test!=new Test.Test(int)
Inital:10_test.10_test.LT
org.extendj.ast.VarAccess
10_test.10_test.LT
org.extendj.ast.VariableDeclarator
10_test.10_test.LT
org.extendj.ast.ClassInstanceExpr
10compare not similar:10_test.10_test!=new Test.Test(int)
10_test.10_test.LT
org.extendj.ast.ConstructorDecl
10_test.10_test.LT
22 checked Maybe
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 20:Fresh%0
10_test.10_test.LT 18:Maybe%0
10_test.10_test.LT 20:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 16:LOCAL%0
10_test.10_test.LT 14:Fresh%0
10_test.10_test.LT 10:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 13:Fresh%0
10_test.10_test.LT 10:Maybe%0
10compare not similar:10_test.10_test!=new Test.Test(int)
Inital:10_test.10_test.LT
org.extendj.ast.VarAccess
10_test.10_test.LT
org.extendj.ast.VariableDeclarator
10_test.10_test.LT
org.extendj.ast.ClassInstanceExpr
10compare not similar:10_test.10_test!=new Test.Test(int)
10_test.10_test.LT
org.extendj.ast.ConstructorDecl
10_test.10_test.LT
22determined fresh because:is not fresh because 
->10_test.LT
->the variable test is initizised to new Test(2) that's not fresh.
It's only of freshness Fresh
-> the constructor new Test(2) is not Fresh
->the variable test is initizised to new Test(2) that's not fresh.
It's only of freshness Fresh.
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 20:Fresh%0
10_test.10_test.LT 18:Maybe%0
10_test.10_test.LT 20:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 16:LOCAL%0
10_test.10_test.LT 14:Fresh%0
10_test.10_test.LT 10:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 13:Fresh%0
10_test.10_test.LT 10:Maybe%0
10compare not similar:10_test.10_test!=new Test.Test(int)
The defining node wasorg.extendj.ast.ConstructorDecl
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 20:Fresh%0
10_test.10_test.LT 18:Maybe%0
10_test.10_test.LT 20:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 16:LOCAL%0
10_test.10_test.LT 14:Fresh%0
10_test.10_test.LT 10:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 13:Fresh%0
10_test.10_test.LT 10:Maybe%0
10compare not similar:10_test.10_test!=new Test.Test(int)
Inital:10_test.10_test.LT
org.extendj.ast.VarAccess
10_test.10_test.LT
org.extendj.ast.VariableDeclarator
10_test.10_test.LT
org.extendj.ast.ClassInstanceExpr
10compare not similar:10_test.10_test!=new Test.Test(int)
10_test.10_test.LT
org.extendj.ast.ConstructorDecl
10_test.10_test.LT
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 20:Fresh%0
10_test.10_test.LT 18:Maybe%0
10_test.10_test.LT 20:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 16:LOCAL%0
10_test.10_test.LT 14:Fresh%0
10_test.10_test.LT 10:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 13:Fresh%0
10_test.10_test.LT 10:Maybe%0
10compare not similar:10_test.10_test!=new Test.Test(int)
22 checked Maybe
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 20:Fresh%0
10_test.10_test.LT 18:Maybe%0
10_test.10_test.LT 20:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 16:LOCAL%0
10_test.10_test.LT 14:Fresh%0
10_test.10_test.LT 10:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 13:Fresh%0
10_test.10_test.LT 10:Maybe%0
10compare not similar:10_test.10_test!=new Test.Test(int)
Inital:10_test.10_test.LT
org.extendj.ast.VarAccess
10_test.10_test.LT
org.extendj.ast.VariableDeclarator
10_test.10_test.LT
org.extendj.ast.ClassInstanceExpr
10compare not similar:10_test.10_test!=new Test.Test(int)
10_test.10_test.LT
org.extendj.ast.ConstructorDecl
10_test.10_test.LT
22determined fresh because:is not fresh because 
->10_test.LT
->the variable test is initizised to new Test(2) that's not fresh.
It's only of freshness Fresh
-> the constructor new Test(2) is not Fresh
->the variable test is initizised to new Test(2) that's not fresh.
It's only of freshness Fresh.
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 20:Fresh%0
10_test.10_test.LT 18:Maybe%0
10_test.10_test.LT 20:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 16:LOCAL%0
10_test.10_test.LT 14:Fresh%0
10_test.10_test.LT 10:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 13:Fresh%0
10_test.10_test.LT 10:Maybe%0
10compare not similar:10_test.10_test!=new Test.Test(int)
The defining node wasorg.extendj.ast.ConstructorDecl
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 20:Fresh%0
10_test.10_test.LT 18:Maybe%0
10_test.10_test.LT 20:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 11:NOCHANGE%0
10_test.10_test.LT 16:LOCAL%0
10_test.10_test.LT 14:Fresh%0
10_test.10_test.LT 10:NOCHANGE%0
10_test.10_test.LT 12:NOCHANGE%0
10_test.10_test.LT 13:Fresh%0
10_test.10_test.LT 10:Maybe%0
10compare not similar:10_test.10_test!=new Test.Test(int)
Inital:10_test.10_test.LT
org.extendj.ast.VarAccess
10_test.10_test.LT
org.extendj.ast.VariableDeclarator
10_test.10_test.LT
org.extendj.ast.ClassInstanceExpr
10compare not similar:10_test.10_test!=new Test.Test(int)
10_test.10_test.LT
org.extendj.ast.ConstructorDecl
10_test.10_test.LT
failed:FreshCheck
Failed test summary:
FreshCheck
Test result: 0/1
Test Done!

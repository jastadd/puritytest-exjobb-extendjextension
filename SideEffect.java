package lang.ast;
/**
 * @ast class
 * @declaredat ASTNode:259
 */
public class SideEffect extends java.lang.Object {	
	  /**
	   * Propagate side effects transivly to this goal annotation. 
	   * Detailed results printed for this method. A obtion could regulate deephs
	   * and put effect on ntas automatically.
	   * 
	   *  (To be implemented)
	   *
	   */
	  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
	  @java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD,java.lang.annotation.ElementType.CONSTRUCTOR,
	  java.lang.annotation.ElementType.PARAMETER,java.lang.annotation.ElementType.LOCAL_VARIABLE,java.lang.annotation.ElementType.FIELD})
	  @java.lang.annotation.Documented
	  public @interface Goal {
	  }
	  
	
	/**
	 * Used for object with Very high Purity requirements. All Internal assignments must be completely Fresh.
	 * Automatically assume local/Fresh requirement for all inner state. All Fields.
	 * Have a group parameter for purity checking when implicitly pure (for access to cache).
	 *
	 */
	  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
	  @java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD,java.lang.annotation.ElementType.CONSTRUCTOR,
	  java.lang.annotation.ElementType.PARAMETER,java.lang.annotation.ElementType.LOCAL_VARIABLE,java.lang.annotation.ElementType.FIELD})
	  @java.lang.annotation.Documented
	  public @interface FreshEx {
		String group() default "";
	  }
	  
	  /**
	   * Mark the return or require the input of a as a fresh object. 
	   * Have a group parameter for purity checking when implicitly pure (for access to cache).
	   * 
	   */
	  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
	  @java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD,java.lang.annotation.ElementType.CONSTRUCTOR,
	  java.lang.annotation.ElementType.PARAMETER,java.lang.annotation.ElementType.LOCAL_VARIABLE,java.lang.annotation.ElementType.FIELD})
	  @java.lang.annotation.Documented
	  public @interface Fresh {
		String group() default "";
		boolean self() default false; 
	  }
	  
	  /**
	   * Mark the return or require the input of a as atleast new object. 
	   * Have a group parameter for purity checking when implicitly pure (for access to cache).
	   * 
	   */
	  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
	  @java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD,java.lang.annotation.ElementType.CONSTRUCTOR,
	  java.lang.annotation.ElementType.PARAMETER,java.lang.annotation.ElementType.LOCAL_VARIABLE,java.lang.annotation.ElementType.FIELD})
	  @java.lang.annotation.Documented
	  public @interface New {
		String group() default "";
		boolean self() default false; 
	  }
	  
	  
	  /**
	   * When the freshness status on the returned object changes depending on the object passed. 
	   * The exact parameter denotes if the freshness is exactly equal to the minimal
	   *
	   */
	  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
	  @java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD,java.lang.annotation.ElementType.CONSTRUCTOR,
	  java.lang.annotation.ElementType.PARAMETER})
	  @java.lang.annotation.Documented
	  public @interface FreshIf {
		  boolean exact();
	  }
	  
	  /**
	   * When the freshness status on the returned object are equal to the minimum of them. 
	   *
	   */
	  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
	  @java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD,java.lang.annotation.ElementType.CONSTRUCTOR,
	  java.lang.annotation.ElementType.PARAMETER})
	  @java.lang.annotation.Documented
	  public @interface FreshAs {
	  }
	  
	  /**
	   * Extra attention to Inner state 
	   *
	   */
	  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
	  @java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD,java.lang.annotation.ElementType.CONSTRUCTOR,
	  java.lang.annotation.ElementType.PARAMETER})
	  @java.lang.annotation.Documented
	  public @interface Inner {
	  }
	  
	  /**
	   * Mark as extra dangerous (may not be used as part of any pure method/Attribute calculation).
	   * Give a warning for all attribute methods using by any extension.
	   *
	   */
	  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
	  @java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD,java.lang.annotation.ElementType.CONSTRUCTOR})
	  @java.lang.annotation.Documented
	  public @interface Impure {
	  }


	  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
	  @java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD,java.lang.annotation.ElementType.CONSTRUCTOR,
	  java.lang.annotation.ElementType.PARAMETER,java.lang.annotation.ElementType.LOCAL_VARIABLE,java.lang.annotation.ElementType.FIELD})
	  @java.lang.annotation.Documented
	  public @interface NonFresh {
		String group() default "";
	  }

	  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
	  @java.lang.annotation.Target({java.lang.annotation.ElementType.FIELD,java.lang.annotation.ElementType.METHOD})
	  @java.lang.annotation.Documented
	  public @interface Secret {
		String group() default "";
	  }
	  
	  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
	  @java.lang.annotation.Target({java.lang.annotation.ElementType.TYPE})
	  @java.lang.annotation.Documented
	  public @interface Entity {
	  }

	  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
	  @java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD,java.lang.annotation.ElementType.CONSTRUCTOR,
	  java.lang.annotation.ElementType.PARAMETER,java.lang.annotation.ElementType.LOCAL_VARIABLE,java.lang.annotation.ElementType.FIELD})
	  @java.lang.annotation.Documented
	  public @interface Ignore {
	  }

	  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
	  @java.lang.annotation.Target({java.lang.annotation.ElementType.METHOD,java.lang.annotation.ElementType.CONSTRUCTOR,
	  java.lang.annotation.ElementType.FIELD,java.lang.annotation.ElementType.PARAMETER})
	  @java.lang.annotation.Documented
	  public @interface Local {
		String group() default "";
	  }

	  /*
	   * 
	   */
	  @java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
	  @java.lang.annotation.Target(java.lang.annotation.ElementType.METHOD)
	  @java.lang.annotation.Documented
	  public @interface Pure {
		String group() default "";
		boolean Ignore() default false;
	  }
}
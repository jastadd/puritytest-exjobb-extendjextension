aspect PurityAnnotatable{
	// All MethodCalls of some kind (
	MethodAccess implements MethodCall;
	ConstructorAccess implements MethodCall;
	ClassInstanceExpr implements MethodCall;
	SuperConstructorAccess implements MethodCall;

	/** 
	 * Any type of method call. Represent MethodAccess , ClassInstanceExpr , ConstructorAccess.
	 * The following functionallity is common.
	 */
	interface MethodCall{
		public List<Expr> getArgs(); // Recive Arguments for all MethodCalls
		
		/**
		 *  This allows checking against the non parameterized typing.
		 *  Will always return a object cast able to MethodCall.
		 */
		public Access erasedCopy(); 
		public BodyDecl enclosingBodyDecl();
		public BodyDecl decl();
		public int arity();
	}
	
	//Effected by PurityAnnotations (Can have Pure,Fresh etc)
	MethodDecl implements PurityAnnotatable;
	ConstructorDecl implements PurityAnnotatable;
	Variable extends PurityAnnotatable;
	ParameterDeclaration implements PurityAnnotatable;
	Declarator implements PurityAnnotatable;
	CatchParameterDeclaration implements PurityAnnotatable;
	InferredParameterDeclaration implements PurityAnnotatable;
	EnumConstant implements PurityAnnotatable;
	
	// Any thing Annotated for Purity
	interface PurityAnnotatable{
		public String signature();
		// HasModifiers(); 
		 public Modifiers getModifiers();
		//Purity Annotation 
		public int purityLevel();
		// Freshness Annotation
		public int freshnessLevel();
	}	
	
	Expr implements Freshness;
	Declarator implements Freshness;
	interface Freshness{
		public boolean isFresh();
		public boolean isLocal();
		public LContext freshness();
	}

	
	syn boolean BodyDecl.hasAnnot(String annot)=getAnnots().hasAnnot(annot);
	syn boolean TypeDecl.hasAnnot(String annot)= PurityUtil.hasAnnot(annot,getModifiers());
	syn boolean PurityAnnotatable.hasAnnot(int i)=getAnnots().hasAnnot(i);
	syn boolean PurityAnnotatable.hasAnnot(String annot){
		boolean b=getAnnots().hasAnnot(annot);
		if(b)
			return b;
		if (getModifiers()==null)
			return false;
		return PurityUtil.hasAnnot(annot,getModifiers()); 
	}
	
	syn String BodyDecl.isGroup()="";
	syn String PurityAnnotatable.isGroup();
	eq CatchParameterDeclaration.isGroup()="";
	eq EnumConstant.isGroup()="";
	eq ParameterDeclaration.isGroup()="";
	eq InferredParameterDeclaration.isGroup()="";
	eq Declarator.isGroup()=purityLevel()>=LOCAL ? getAnnots().isGroup() : "";
	eq FieldDeclarator.isGroup()=fieldDecl().getAnnots().isGroup();
	eq MethodDecl.isGroup()=purityLevel()>=LOCAL ? getAnnots().isGroup() : "";
	eq ConstructorDecl.isGroup()=purityLevel()>=LOCAL ? 
			getAnnots().isGroup() : "";
	eq FieldDecl.isGroup()=getAnnots().isGroup();
	syn boolean BodyDecl.hasGroup(String s)=false;
	syn boolean PurityAnnotatable.hasGroup(String s)= purityLevel()>=LOCAL ? getAnnots().isGroup().equals(s) : false;
	//Search A specific annotation
	syn boolean PurityAnnotatable.hasFresh();
	eq ConstructorDecl.hasFresh()= getAnnots().freshnessLevel()!=1 || 
			getAnnots().purityLevel()!=1 ;
	eq MethodDecl.hasFresh()=getAnnots().hasFresh();
	eq FieldDecl.hasFresh()=getAnnots().hasFresh();
	eq Declarator.hasFresh()=getAnnots().hasFresh();
	eq ParameterDeclaration.hasFresh(){
		if (isConstructorParameter() && isASTChild())
			return true;
		return getAnnots().hasFresh();
	}
	eq VariableArityParameterDeclaration.hasFresh()=getAnnots().hasFresh() || getAnnots().hasLocal();
	eq InferredParameterDeclaration.hasFresh()=getAnnots().hasFresh();
	eq CatchParameterDeclaration.hasFresh()=getAnnots().hasFresh();
	eq EnumConstant.hasFresh()=getAnnots().hasFresh();
	
	syn boolean BodyDecl.hasSelfFresh()=false;
	eq MethodDecl.hasSelfFresh()=hasFresh() &&  PurityUtil.selfFresh(getModifiers().getModifiers());
	eq ConstructorDecl.hasSelfFresh()=true; // Automatically true!!
	
	syn boolean PurityAnnotatable.hasIgnore()=getAnnots().hasIgnore();
	syn boolean PurityAnnotatable.hasNonFresh()=getAnnots().hasNonFresh();
	syn boolean PurityAnnotatable.hasLocal()=getAnnots().hasLocal();
	syn boolean PurityAnnotatable.hasPure()=getAnnots().hasPure();
	syn boolean PurityAnnotatable.hasFreshIf()=getAnnots().hasFreshIf();
	syn boolean PurityAnnotatable.hasFreshEx()=getAnnots().hasFreshEx();
	syn boolean PurityAnnotatable.hasFreshAs()=getAnnots().hasFreshAs();
	

	syn boolean MethodCall.hasFreshIf()=getAnnots().hasFreshIf();
	syn boolean MethodCall.hasFresh()=getAnnots().hasFresh(); 
	syn boolean MethodCall.hasPure()=getAnnots().hasPure();
	syn boolean MethodCall.hasIgnore()=getAnnots().hasIgnore();
	syn boolean MethodCall.hasNonFresh()=getAnnots().hasNonFresh();
	syn boolean MethodCall.hasLocal()=getAnnots().hasLocal();
    syn boolean MethodCall.hasFreshAs()=getAnnots().hasFreshAs();

	syn boolean BodyDecl.hasFresh()=false;
	syn boolean BodyDecl.hasIgnore()=false;
	syn boolean BodyDecl.hasNonFresh()=false;
	syn boolean BodyDecl.hasPure()=false;
	syn boolean BodyDecl.hasLocal()=false;
	syn boolean BodyDecl.hasFreshIf()=false;

}
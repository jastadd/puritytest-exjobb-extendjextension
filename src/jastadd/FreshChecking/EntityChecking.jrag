import java.util.ArrayList;

/** 
* Validates That Entities are assigned or mainpulated correctly.
* Store tests related to Entities.
*/ 
aspect EntityChecking{
	
	/**
	* The (access) field with the name is in the locality of the type of the 
	* parent access.
	* @param name - a name of a field.
	*/ 
	syn boolean Expr.inLocality(Expr field)=true;
	eq Access.inLocality(Expr field){
		if (field instanceof VarAccess)
			return field.varDecl().hasLocal(); 
		return true;
	}
	eq VarAccess.inLocality(Expr field){
		if (decl().isStatic()) 
			return false;
		if (field instanceof VarAccess)
			return varDecl().hasLocal();
		// Array always in Locality of its parent!!
		return true;
	}
	
	/**
	* Debug why not determined as entity by search through class herariky.
	* Mirrors the isEntity() method.
	*/
	syn String TypeDecl.whyNotEntity()="the type "+ name() +" is not entity.";
	eq ArrayDecl.whyNotEntity()="array types can't be annotated (Currently).";
	/**
	* Search both locally, in superclass and in all interfaces. 
	* The annotation only has to be mentioned once.
	*/
	eq ClassDecl.whyNotEntity(){
		String reason = "";
		
		if (hasSuperClass()) {
			if (getSuperClass() instanceof TypeAccess)
				return name() + ":" + ((TypeAccess)getSuperClass()).decl().whyNotEntity();
			if (getSuperClass() instanceof ParTypeAccess)
				return name() + ":" + ((ParTypeAccess)getSuperClass()).genericDecl().whyNotEntity();
			reason=name() + ":A unhandled type of super class " + getSuperClass() + " of type" + getSuperClass().getClass().getName();
		}
		
		for (TypeDecl superInterface: superInterfaces()) {
			if (superInterface.isEntity()) {
				reason += " " + superInterface.name();
			}
		}
		return reason +" "+ hasAnnot("Entity");
	}
	
	eq InterfaceDecl.whyNotEntity(){
		String reason = "";
		if (hasAnnot("Entity"))
			return "interface is entity.";
		for (TypeDecl superInterface: superInterfaces())
			reason += ":" + superInterface.whyNotEntity();
		return reason;
	}
	
	
	/** 
	* Determine if class is of Entity type.  
	*/ 
	syn boolean TypeDecl.isEntity()=false;
	eq AbstractWildcardType.isEntity()=true;
	eq ArrayDecl.isEntity()=false;
	
	/**
	* The Alternatives are either the class have the annotation or the superclass have annotation 
	* or one of the interfaces.
	* The search after the annotation is through out the class herarky.
	* lazy since may be invoked may times might but speed up with place sacrifices.
	*/
	syn lazy boolean ClassDecl.isEntity(){
		if (hasSuperClass()){
			if (getSuperClass() instanceof TypeAccess)
				return hasAnnot("Entity") || ((TypeAccess)getSuperClass()).decl().isEntity();
			if (getSuperClass() instanceof ParTypeAccess)
				return  hasAnnot("Entity") || ((ParTypeAccess)getSuperClass()).genericDecl().isEntity();
			System.out.println("Unhandled "+getSuperClass());
		}
		for (TypeDecl f: superInterfaces())
			if (f.isEntity())
				return true;
		return hasAnnot("Entity");
	}
	
	eq InterfaceDecl.isEntity(){
		if (hasAnnot("Entity"))
			return true;
		for (TypeDecl f: superInterfaces())
			if (f.isEntity())
				return true;
		return false;
	}

	/**
	* Check if All EntityFields AssignedFreshObjects Needs 2 helper functions 
	* 1. All Method Calls in the Constructors.
	* 2. All fields in the constructor.
	* Does the Work to Assure All Fields has been AssignedFresh at End of a ASTNode constructor!!!
	*/
	syn lazy String ConstructorDecl.EntityFieldsFreshWarning(){
		StringBuilder sb=new StringBuilder();
		Map<String,SimpleSet<Variable>> locals = hostType().localFieldsMap();
		for (String s: locals.keySet()){
			 for(Variable var: locals.get(s)){
				 if (!var.hasAnnot("Secret")){
					 if (var.hasLocal() && var.type().isEntity()){
						 LContext con=exit().heapStatus(getHeapObject(new Variable[]{var}));
						 if(con!=LContext.FRESH){
							 if (con==LContext.NOCHANGE || con==LContext.FRESHEX){
							 }else{
								 sb.append(lineNumber()+": The field "+ s +" needs to be assigned fresh inorder for the entity subtree to be completly freshly created.");
								 sb.append("It's assigned only" + con.toString());
							 }
						}
					 }else if(var.type().isEntity() && assignedAfter(var)){
							 sb.append(lineNumber()+": The field "+s+" needs to be assigned a fresh object inorder for the entity subtree to compleat");	
					 }
				}
			}
		}
		return sb.toString();
	} 

	/**
	 * Fields left to assign in nta.
	 */
	syn lazy Set<Variable> ConstructorDecl.leftToAssign(Variable v) {
		Map<String,SimpleSet<Variable>> locals = hostType().localFieldsMap();
		Set<Variable> varstoAssign = Collections.newSetFromMap(
				new IdentityHashMap<Variable, Boolean>());
		for (String s: locals.keySet()){
			 for(Variable var: locals.get(s)){
				 if (!var.hasAnnot("Secret")){
					 if (var.hasLocal() && var.type().isEntity()){
						 LContext con=exit().heapStatus(getHeapObject(new Variable[]{var}));
						 if(con==LContext.NOCHANGE){
								 varstoAssign.add(var);
						}
					 }
				 }
			 }
		}
		return varstoAssign;
	}
	
//	syn lazy Set<Variable> ReturnStmt.leftToAssign(Variable v){
		
//	}
	
	/**
	*
	*/
	ReturnStmt contributes new PurityWarning(hostType(),Program.count ? ""
			: ((ConstructorDecl)enclosingBodyDecl()).EntityFieldsFreshWarning())
	when returnType().isEntity() && (enclosingBodyDecl() instanceof ConstructorDecl) && enclosingBodyDecl().hasFresh() && !((ConstructorDecl)enclosingBodyDecl()).EntityFieldsFreshWarning().equals("") && false
	to BodyDecl.purityWarnings();
	
	
	// Creation of Entity Objects
	ClassInstanceExpr contributes new PurityWarning(enclosingBodyDecl(),hostType(),
			Program.count ? "" : lineNumber() + ":" +enclosingStmt().prettyPrint()+System.lineSeparator()
	+"Only @Fresh/@Local/@Ignore methods or Nta's may create new Entity objects."+System.lineSeparator()+
	"The current methods annotation is "+purityLevelString(enclosingBodyDecl().purityLevel()),PurityComplaint.NODECREATION)
	when entityNotAllowed()
	to BodyDecl.purityWarnings();
	
	private boolean ClassInstanceExpr.entityNotAllowed(){
		BodyDecl bd=enclosingBodyDecl();
		boolean b=bd instanceof ConstructorDecl || bd.hasIgnore() || bd.hasLocal() 
				|| bd.hasNtaAnnot();
		Stmt stmt=enclosingStmt();
		if (b || (stmt instanceof ReturnStmt  && !((ReturnStmt)stmt).entityFreshnessProblem())) 
			return false;
		int freshnessLevel =bd.freshnessLevel();
		return  (type().isEntity() && (freshnessLevel==IMPURE ||  freshnessLevel==NONFRESH)) ;
	}
	 
	
	ReturnStmt contributes new PurityWarning(enclosingBodyDecl(), hostType(),
			Program.count ? "" : lineNumber() + ":"+prettyPrint()+
			"which is fresh can't be returned from a Entity typed method with no freshness specifier."+
			System.lineSeparator()+"Entity types are supposed to only be used in strict contexts demanding"+
			" either FRESH or NONFRESH depending on usage."+System.lineSeparator()+
			"Default is NONFRESH which applies unless used for Tokens and NTAs."+System.lineSeparator()+
			getResult().whyFresh("This is fresh because")+".")		
	when Program.extendOn && returnType().isEntity() && entityFreshnessProblem()
	to BodyDecl.purityWarnings();
			
	AssignSimpleExpr contributes new PurityWarning(enclosingBodyDecl(), hostType(),
			Program.count ? "" : lineNumber() + ":"+prettyPrint()+
			" which is not fresh can't be returned from a Entity typed NTA method."+
			System.lineSeparator()+"NTAs are supposed to point to new objects."+System.lineSeparator()+
			"Return is not Fresh because of "+getSource().whyUnFreshMessage(getSource().getHeapObject())+".")
	when getDest() instanceof VarAccess && enclosingBodyDecl().hasNtaAnnot() && 
	getDest().varDecl().hasAnnot("Secret") && getDest().varDecl().name().contains("_value") && 
	!assignFreshNotNull()
	to BodyDecl.purityWarnings();
	
	/**
	* Combines 2 different tests into one (entity return rules when in
	*	attribute calculation,not in attribute calculation).
	* The question is if its allowed to return Fresh or Not.
	*/ 
	public boolean ReturnStmt.entityFreshnessProblem(){
		BodyDecl bd=enclosingBodyDecl();
		if (bd.freshnessLevel()==IMPURE){	
			Expr result = getResult();
			if (!bd.AttributeCalculation())
				if (result.varDecl() !=null && result.varDecl().hasAnnot("Secret"))
					return false;
			return result.isFresh() && result.freshness()!=LContext.IGNORE && 
					result.freshness()!=LContext.NULL && !bd.onlyFromFresh();
		}
		return false;
	}
	
	ConstructorDecl contributes new PurityWarning(hostType(),Program.count ? "" : EntityFieldsFreshWarning())
	when hasFresh() && hostType().isEntity() && !EntityFieldsFreshWarning().equals("") && false
	to BodyDecl.purityWarnings();
}
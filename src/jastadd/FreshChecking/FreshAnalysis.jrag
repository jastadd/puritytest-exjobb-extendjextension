aspect FreshTracking{
	/** 
	* Obtain the MethodCall responsible for unfreshness if any. 
	* Search throw the defining expression to see if any MethodCall is present
	* and if it is then returning the Methodcall. Doesn't consider FreshIf.
	*/	
	public MethodCall Access.unFreshDefining(){
		ASTNode node=getFreshnessDefiningASTNode();
		if (node==null) {
			PurityUtil.debug(lineNumber()+ "Tracing MethodCall defining Unfreshness:No Method");
			return null;
		}
		if (node instanceof MethodCall)
			return (MethodCall)node;
		Expr expr=null;
		if (node instanceof Expr)
			expr = ((Expr)node).purifyAccess();
		if (expr instanceof Access){
			Access access = ((Access)expr).lastAccess();
			access = (Access)access.LocalityTop();
			if (access instanceof MethodCall)
				return (MethodCall)access;
			while(access.isQualified()){
				access=(Access)access.qualifier().purifyAccess();
				if (access instanceof MethodCall)
					return (MethodCall)access;
			}
		}
		return null;
	}
	
	/**
	* Obtains The Freshness defining Node!! -> ErgoWhyUnFresh!!!
	*/
	syn ASTNode ASTNode.getFreshnessDefiningASTNode(){
		ArrayList<ASTNode> list=valueOrgin(LContext.FRESH); 
		return list.get(list.size()-1);
	}
	
	/**
	* Searches For Any diviation From Known Fresh
	* Provide FRESH to search for absence Fresh
	* Provide NONFRESH to search for Absense of NONFRESH
	*/
	
	//Find Value Orgin 
	syn ArrayList<ASTNode> ASTNode.valueOrgin(LContext v){
		ArrayList<ASTNode> chain = new ArrayList<ASTNode>(2);
		Set<ASTNode> visited=new HashSet<ASTNode>(2); 
		ASTNode node=valueOrgins(v, getHeapObject());
		if (node instanceof Expr)
			node = ((Expr)node).purifyAccess();
		HeapObject h = getHeapObject();
		while(!visited.contains(node)){
			chain.add(node);
			visited.add(node);
			node=node.valueOrgins(v,h);
			if (node instanceof Expr)
				node = ((Expr)node).purifyAccess(); 
			h=node.currentHeapObject(h);
		}
		return chain;
	}

	syn ASTNode ASTNode.valueOrgins(LContext v,HeapObject h)=this;
	eq MethodDecl.valueOrgins(LContext v,HeapObject h){
		HeapObject h2 = h != null ? h.LocalityTop() : h;
		if ( h2 != null) {
		PurityUtil.debug(lineNumber()+ "Tracing Orgin for Freshness:" + h2.toString() + ":current:" + v.toString());
		}else {
			PurityUtil.debug(lineNumber()+ "Tracing Orgin for Freshness: No HeapObject For Value"  + ":current:" + v.toString());
		}
		Variable var=null;
			if (h2 instanceof HeapVariable){
				var=h2.var;
				if (var.isField()){
					if (!var.isClassVariable() && (onlyFromConstructor() || unchanged(var)))
						return var.hasInit() ? var.getInit() :  (ASTNode)var;
					if (var.hasAnnot("Secret")){
						if (!PurityUtil.secretAssign(program(),var))
							return var.hasInit() ? var.getInit() : this;
						if (!var.hasInit())
						return this;
					}
				}
				return (ASTNode)var;
			}else if (h2 instanceof HeapNewObject) {
				PurityUtil.debug(PurityUtil.debug ? lineNumber()+ "NewObject added to heap from InstanceClassExpr:"+ ((HeapNewObject)h2).getFirstExpr() : "");
				return ((HeapNewObject)h2).getFirstExpr();
			}
	return this;
	}
	
	eq AssignExpr.valueOrgins(LContext v,HeapObject h)=getSource(); // 
	eq ParExpr.valueOrgins(LContext v,HeapObject h)=purifyAccess().valueOrgins(v,h);
	eq CastExpr.valueOrgins(LContext v,HeapObject h)=typeWiseFresh() ? this : purifyAccess().valueOrgins(v,h);
	eq InstanceOfExpr.valueOrgins(LContext v,HeapObject h)=getExpr().purifyAccess().valueOrgins(v,h);
	eq ExprStmt.valueOrgins(LContext v,HeapObject h)=getExpr().valueOrgins(v,h);
	eq Dot.valueOrgins(LContext v,HeapObject h)=lastAccess(); 
	
	eq AssignSimpleExpr.valueOrgins(LContext v,HeapObject h){
		return (h==null) ? this : getSource().valueOrgins(v,getSource().obtainHeapObject());
	}
	eq ConditionalExpr.valueOrgins(LContext v,HeapObject h){
		LContext ifTrue=getTrueExpr().freshness();
		LContext ifFalse=getFalseExpr().freshness();
		LContext totalResult=ifTrue.value>ifFalse.value ? ifTrue : ifFalse;
		boolean factor=ifTrue.value>ifFalse.value ? true : false;	
		return (factor ? getTrueExpr() : getFalseExpr());
	}
	
	syn ASTNode VarAccess.quickFreshOrgins(){
		Variable var = varDecl();
		if (var.hasFresh())
			return this;
		if (var.isLocalVariable() && var.isEffectivelyFinal()){
			if(!((var instanceof VariableDeclarator) && ((VariableDeclarator)var).outerScope() instanceof EnhancedForStmt)){
				if (!var.isBlank())
					return var.getInit()!=null ? var.getInit() : this;
				if (isDest() && enclosingStmt() instanceof ExprStmt){
					Expr expr = ((ExprStmt)enclosingStmt()).getExpr();
					if (expr instanceof AssignExpr)
						return ((AssignExpr)expr).getSource();
				}
			}
		}
		return this;
	}
	
	public ASTNode Access.valueOrginCommon(){
		LContext context = varDecl() == null || varDecl().hasLocal() ? context() : selfContext();
		boolean isMaybe=context.isMaybeFresh();
		if ((context.value<5 && context.value>1) || isMaybe){
			Expr locTop=LocalityTop();
			 if (locTop instanceof VarAccess){
				return (ASTNode)locTop.varDecl();
			 }
			return locTop;
		}
		if (varDecl() !=null && varDecl().hasLocal() && searchContext()!=this) 
			return searchContext();
		return null;
	}	
	
	eq Access.valueOrgins(LContext v,HeapObject h){
		if(typeWiseFresh())
			return this;
		LContext common = common();
		if (common.value<5 && common.value>1){
			ASTNode potential = valueOrginCommon();
			if (potential!=null)
				return potential;
		}
		if (!isQualified() && varDecl()!=null)
			return ((ASTNode)varDecl());
		return this;
	}
	
	eq Binary.valueOrgins(LContext v,HeapObject h)=this; // Special Attention Needed --->
	eq Unary.valueOrgins(LContext v,HeapObject h)=getOperand();
	eq ClassInstanceExpr.valueOrgins(LContext v,HeapObject h){
		if (hasConditionalFresh() && freshCondsMeet() != 2) {
		 	if (isQualified()) {
		 		return prevExpr();
			}
		}	
		return decl();
	}
	eq FieldDeclarator.valueOrgins(LContext v,HeapObject h)=this ; // Terminal without Extra check!!!
	eq VariableDeclarator.valueOrgins(LContext v,HeapObject h){
		return  hasInit() ? getInit() : this;   
	}
	eq VarDeclStmt.valueOrgins(LContext v,HeapObject h){
		return  getDeclarator(0).hasInit() ? getDeclarator(0).getInit() : this;
	}
	eq EnhancedForStmt.valueOrgins(LContext v,HeapObject h){
		return getExpr();  
	}
	eq ArrayAccess.valueOrgins(LContext v,HeapObject h){
		if (typeWiseFresh())
			return this;
		String potential = commonWhyUnFresh(context());
		if (!potential.equals(""))
			return LocalityTop();
		String id=identityString();
		HeapObject x=enclosingBodyDecl().getHeapObject(effectedObjects());
		if  (x!=null && x.first.var==null)
			return qualifier(); 
		if (x!=null){
			CfgNode cfg=hasCfgDef() ? cfgDef() : entry();
			ASTNode node = cfg.ObtainDefiningASTNODE(v,x);
			if (node instanceof MethodDecl && type().isReferenceType())
				return node;
			}
		return qualifier();
	}
		

	
	eq ThisAccess.valueOrgins(LContext v,HeapObject h)=this;
	
	eq MethodAccess.valueOrgins(LContext v,HeapObject h){
		if(typeWiseFresh())
			return this;
		LContext context = context();
		if (context==LContext.NOCHANGE && context.isNew() || context.isMaybeFresh())
			return LocalityTop(); //Will give this
		if  (context.isNonFresh()){
			if (hasNonFresh())
				return this;
			if (isQualified()){
				if(selfContext()==LContext.THIS)
					return enclosingBodyDecl();
				}	
				return LocalityTop();
		}
			return this;
	}
	
	
	
	eq VarAccess.valueOrgins(LContext v,HeapObject h){
		if(typeWiseFresh())
			return this;
		LContext common = common();
		if (common.value<5 && common.value>1){
			ASTNode potential = valueOrginCommon();
				if (potential!=null)
					return potential;
		}
		String id=identityString();
		cfg().initPredecessors();
		CfgNode cfg=hasCfgDef() ? cfgDef() : entry();
		HeapObject x=enclosingBodyDecl().getHeapObject(effectedObjects());
		ASTNode node = cfg.ObtainDefiningASTNODE(v,x);
		return node!=null ? node : this;
	}
	
}

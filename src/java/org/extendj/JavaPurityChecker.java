
package org.extendj;

import org.extendj.ast.HashMapOne;
import org.extendj.ast.CompilationUnit;
import org.extendj.ast.PurityDescription;
import org.extendj.ast.Frontend;
import org.extendj.ast.BodyDecl;
import org.extendj.ast.PurityWarnings;
import org.extendj.ast.Problem;
import org.extendj.ast.Program;
import org.extendj.ast.PurityUtil;
import org.extendj.ast.FieldMap;
import org.extendj.ast.CallGraph;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FileInputStream;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Map.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.Properties;

/**
 * Java Purity Checker : Preforms the Purity Checking and Generate the default
 * properties. 
 * Java Purity Debug : Preforms debug actions.
 * TestPurity in the test folder preforms testing.
 */
public class JavaPurityChecker extends Frontend {

	/**
	 *  Container for a File and the load number.
	 */
	public class FileInt{
		File f;
		int i;
		
		/**
		 * Constructor for the container type.
		 * @param f
		 * 			the file to be stored.
		 * @param i
		 * 			the load number.
		 */
		public FileInt(File f,int i){
			this.f = f;
			this.i = i;
		}
		
	}

	/**
	 * Variables for storing all the statuses
	 */
	public List<FileInt> toLoad = new ArrayList<FileInt>(4);
	public Properties propGen = new Properties();
	public boolean update = false;
	public Program prog=null;
	int loadNumber=0;
	private boolean count = false;
	public boolean outScreen = true;
	public boolean outFile = true;
	public boolean oneFile = false;
	public boolean list = false;
	public boolean allIgnore=false;
	public boolean filecreated = false;
	public boolean step = false;
	private boolean showCallGraph = false;
	public static PrintStream writeOut = System.out;

	public static HashMap<String, Boolean> options = new HashMap<String, Boolean>(); 
	
	static Log log = new Log(); 
	
	public static class Log {
			FileWriter f;
			File file = new File("log.txt");
			public static boolean doLog=true;
		public Log() {
			try {
				f = new FileWriter(file);
			} catch (IOException e) {
				System.err.println("can't create log");
				e.printStackTrace();
			}
		}
		
		public void logClose() {
			try {
				print("Log has been written to " + file.getCanonicalPath());
				f.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		public void print(String s){
			try {
				if (doLog) { 
				    System.out.println(s);
				}
				f.write(s);
				f.write(System.lineSeparator());
				f.flush();
			} catch (IOException e) {
				System.err.println("can't write log");
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Entry point for the compiler frontend.
	 * 
	 * @param args
	 *            command-line arguments
	 */
	public static void main(String args[]) {
		int exitCode = startChecker(args);
		log.logClose();
		if (exitCode!=0) {
			System.exit(exitCode); 
		}
	}
	
	public static int startChecker(String args[]) {
		int exitCode;
		Properties prop = new Properties();
		InputStream input = null;
		String path="ExtJPure.properties";
		JavaPurityChecker checker = new JavaPurityChecker();
		Program.pOptions = options;
		String prelogged = "";
		try {
		if (args!=null && args.length>1 && args[0].equals("-config")){
			checker.ReadConfiguration(args[1]);
			prelogged = "have a configuration -config provided" + args[1];
			path=args[1];
		}else{
			checker.ReadConfiguration("");
		}
		} catch (IOException e) {
			e.printStackTrace();
			return -100;
		}
		int i = checker.PurityOptions(args); // Applies the effect of provided
		log.print(prelogged);
		log.print("Done appling options");
		// flags
		if (i<0){
			if (args.length>i+1) {
				displayHelp(args[((i+1)*-1)]);
			}else {
				displayHelp();
			}
			log.print("Display help and exit");
			return 0;
		}

		try {

			File fields = path.contains("ExtJPure.properties") ? 
				new File(path.replace("ExtJPure.properties", "FieldsTrusted.properties")) :
				new File("FieldsTrusted.properties");
			if (fields.exists()){
				log.print("Load Field file: "+fields.getName());
				Properties fieldprops = new Properties();
				input = new FileInputStream(fields);
				fieldprops.load(input);
				input.close();
				FieldMap FM=new FieldMap(fieldprops);
				Program.AnnotListFields=FM;
			}
			checker.loadExternalAnnots();
		
			log.print("Running the checker");
			if (i != 0) {
				String[] part = new String[args.length - i];
				System.arraycopy(args, i, part, 0, args.length - i);
				exitCode = checker.run(part);
			} else {
				exitCode = checker.run(args);
			}
			if (exitCode != 0) {
				return exitCode;
			}
			log.print("Analysis Done!");
			
			if (checker.list) {
				checker.prog.constructAnnotList();
				writeOut.println("Number of problem methods:"+checker.prog.AllpurityWarnings().size());
				for (Entry<String, PurityDescription> entry : checker.prog.AnnotList().entrySet()) {
					writeOut.println("Generated: " + entry.getKey()+" Given:"+entry.getValue().name());
					checker.propGen.setProperty(entry.getKey(), entry.getValue().getValue());
				}
				Properties propNeeded=new Properties();
				for (Entry<String, PurityDescription> entry : checker.prog.AnnotListCurrent().entrySet()) {
					writeOut.println("NeededLocally: " + entry.getKey() +" Given:"+ entry.getValue().name());
					propNeeded.setProperty(entry.getKey(), entry.getValue().getValue());
				}	
				FileOutputStream fos3 = new FileOutputStream("WhyInfered.result", false);
				PrintStream ps3 = new PrintStream(fos3, false, "UTF-8");
				for (Entry<BodyDecl,PurityWarnings> entry : checker.prog.Triggers.entrySet()){
					ps3.println(entry.getKey().puritySignature()+" = "+entry.getValue().toString());
				}
				ps3.close();
				int loadNumber=loadNumber(path,checker.propGen,propNeeded);
				prop=new Properties();
				input=new FileInputStream(path);
				prop.load(input);
				if (!checker.propGen.isEmpty() || !propNeeded.isEmpty())
					prop.put("trusted"+loadNumber*2,"ExtJPureTrusted"+loadNumber+".properties");
				if (!propNeeded.isEmpty())
					prop.put("trusted"+(loadNumber*2+1),"ExtJPureNeeded"+loadNumber+".properties");
				prop.load(input);
				FileOutputStream fos = new FileOutputStream(path, false);
				PrintStream ps = new PrintStream(fos, false, "UTF-8");
				prop.store(ps, null);
				fos.close();
			} else {
				log.print("The .Trusted files has not been updated since -list was not provided.");
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return 1;
		}
		return 0;
	}
		

	public void loadExternalAnnots() {
		Properties prop = new Properties();
		InputStream input = null;
		HashMapOne hMO=new HashMapOne(0);
		for (FileInt fileToLoad: toLoad) {
			if (fileToLoad.f.exists()) {
				try {
					input = new FileInputStream(fileToLoad.f);
					prop.load(input);
					input.close();
					hMO.loadFile(prop,fileToLoad.i);
					prop.clear();
					log.print("loaded file: "+ fileToLoad.f.getCanonicalPath());
				} catch (IOException e) {
					log.print("A exception occured during loading external annotations specifications:" + fileToLoad.f.getName());
					e.printStackTrace();
				}
				if (hMO.hasProblems()) {
					log.print("has annotation parsing problems");
					boolean[] writeMode = new boolean[] { outScreen, outFile };
					PrintStream[] PrintStreams = new PrintStream[2];
					PrintStreams[0] = writeOut;
					try {
						PrintStreams[1] = OutputFileStream("ExternalAnnot" , "ExternalAnnot.");
					}catch (IOException e){
						log.print("A exception occured when creating printStreams");
						e.printStackTrace();
					}
					if (writeMode[0]) {
						PrintStreams[0].println("The following annotation problems where detected:");
						PrintStreams[0].print(hMO.problemInfo());
					}
					if (writeMode[1]) {
						PrintStreams[1].println("The following annotation problems where detected:");
						PrintStreams[1].print(hMO.problemInfo());
					}
					log.print("Printed out warnings in annotations");
					System.exit(0);
				}
			}
		}
		Program.AnnotListExternal=hMO;
	}

	/**
	 * Store the library information from on property table to a file.
	 * @param p
	 * 		The property file containing the table to store.	
	 * @param f
	 * 		The file to store in.
	 */
	public static void StoreLibraryInfo(Properties p,File f) throws IOException {
		try {
			if (!p.isEmpty()){
				FileOutputStream fos = new FileOutputStream(f, false);
				PrintStream ps = new PrintStream(fos, false, "UTF-8");
				p.store(ps, null);
				fos.close();
				ps.close();
				log.print("Stored new library info in :" + f.getPath());
			}
		} catch (IOException e){
			log.print("Exception during storing of library info: " + f.getName());
			writeOut.println("Exception during storing of library info:" + f.getName());
			throw e;
		}
	}
	
	/**
	 * Obtain the number of next iterator of externally function information to load.
	 */
	public static int loadNumber(String path,Properties p,Properties p2) throws IOException{
		String currentFolder="";
		try{
			if (path.equals("ExtJPure.properties")){
				currentFolder = new File("").getAbsolutePath();
			}else{
				currentFolder = new File(path).getParentFile().getAbsolutePath();
			}
		}catch(NullPointerException e){
			writeOut.println(path+" is not valid");
			System.exit(-1);
		}
		int loadNumber=0;
		Path currentRelativePath = Paths.get("");
		String sx = currentRelativePath.toAbsolutePath().toString();
		boolean difference = sx.startsWith(currentFolder);
		String pathModifier =  difference ? currentFolder+File.separatorChar : "";
		File file2 = new File(pathModifier+"ExtJPureTrusted"+loadNumber+".properties");
		File file3 = new File(pathModifier+"ExtJPureNeeded"+loadNumber+".properties");
		while (file2.exists() || file3.exists() ){
			loadNumber++;
			file2=new File(pathModifier+"ExtJPureTrusted"+loadNumber+".properties");
			file3=new File(pathModifier+"ExtJPureNeeded"+loadNumber+".properties");
		}
		StoreLibraryInfo(p,file2);
		StoreLibraryInfo(p2,file3);
		return loadNumber;
	}
	
	/**
	 * Read the Configuration Provided from ExtJPure.properties
	 */
	public void ReadConfiguration(String path) throws IOException{
		try {	
			File f = new File(path);
			if (!f.isFile()) {
				f = new File(".","ExtJPure.properties");
			}
			if (path.equals("") || !f.exists()) {
				path="ExtJPure.properties";
			}
			if (!f.exists()) {
				System.err.println("Configuration " + f.getCanonicalPath() +" not found, creating new");
			}
			f = new File(path);
			if (!f.exists()) {
				CreateConfiguration(path);
				System.err.println("Configuration " + f.getCanonicalPath() + " created");
			}
			InputStream input = new FileInputStream(path);
			Properties prop = new Properties();
			// load a properties file
			prop.load(input);
			Program.setPurityProperties(prop);
			options.put("step", prop.getProperty("step").equals("1"));
			options.put("outFile", prop.getProperty("outFile").equals("1"));
			options.put("outScreen", prop.getProperty("outScreen").equals("1"));
			options.put("oneFile", prop.getProperty("oneFile").equals("1"));
			PurityUtil.debug= prop.getProperty("oneFile").equals("1");
			loadNumber=0;
			int tryload=0;
			File testfolder = new File(path);
			String pathModifier="";
			if (testfolder != null && testfolder.getParentFile()!=null){
				String currentFolder = testfolder.getParentFile().getAbsolutePath();
				Path currentRelativePath = Paths.get("");
				String sx = currentRelativePath.toAbsolutePath().toString();
				boolean difference = sx.equals(currentFolder);
				pathModifier =  difference ? "" : currentFolder+File.separatorChar;
			}
			while (tryload<4){
				if (prop.getProperty("trusted"+loadNumber)!=null){
					toLoad.add(new FileInt(new File(pathModifier+
						prop.getProperty("trusted"+loadNumber)),loadNumber));
					tryload=0;
				}else{
					tryload++;
				}
				loadNumber++;
			}
			input.close();
			prop = new Properties();
		} catch (IOException ex) {
			ex.printStackTrace();
			writeOut.println("Configuration File doesn't exist or is corrupt");
			throw ex;
		}
	}
	/**
	 * Provides the tools help information. Instructions on how to use the tool. 
	 */
	public static void displayHelp(){
		printHelpMessage(writeOut);
	}
	
	public static void printHelpMessage(PrintStream writeOut) {
		writeOut.println("This tool purity checks Java programs and JastAdd generated java programs.");
		writeOut.println("Annotations to check may be provided in source code or in .properties files.");
		writeOut.println("Usage: [Purity options] [ExtendJ options] javasources ");
		writeOut.println("");
		writeOut.println("Example:");
		writeOut.println("-config testfiles/PurityTests/.. -debug SideEffect.java testfiles/PurityTests/..*.java ");
		writeOut.println("");
		writeOut.println("Tool behaviour and processed files are specified in .properties files or source code");
		writeOut.println("ExtPure.properties contains the main settings. If it doesn't exists it will be auto generated when this tool is runned.");
		writeOut.println("The settings provided could also be provided via command line a command option -oneFile could be provided as oneFile=1 in the file.");
		writeOut.println("Index of all options:");
		writeOut.println("-config: Specify which configuration file will be used");
		writeOut.println("-ignore: provide a list of methods to ignore.");
		writeOut.println("-debug: Output extra debug information to the screen");
		writeOut.println("-outF / outFile: Print out to file");
		writeOut.println("-noScreen: Don't print out to the console");
		writeOut.println("-count: Show only number of error");
		writeOut.println("-list: Infer the needed annotations on more level than present.");
		writeOut.println("-log: Print all log messages to screen.");
		writeOut.println("-noneAsPure: Treat all unannotated methods with source as Pure but only evaled if called by.");
		writeOut.println("-noneAsPureAll: Treat all unannotated methods as Pure and eval.");
		writeOut.println("-simpleNameAnnot: Annotation doesn't have to be given as a full name");
		writeOut.println("");
	}
	
	public static String displayHelp(OutputStream outStream) {
		PrintStream printStream = new PrintStream(outStream);
		printHelpMessage(printStream);
		return outStream.toString();
	}

	public static void displayHelp(String command) {
		switch (command) {
		case "-config" : {
			writeOut.println("The settings file to use.");
			writeOut.println("Overrides defaults settings or setting provided in the same folder as this tool.");
			writeOut.println("This option must be the first option provideded as a [PurityOption]");
			writeOut.println("Most options can be provided in the configuration");
		} break;
		case "-list": {
			writeOut.println("Infer the needed annotations.");
			writeOut.println("The infered list is dumped in the same folder as this tool");
		} break;
		default: displayHelp();
		}
	}
	
	/**
	 * Options Given On Command Line. Alternatives are
	 * -config:
	 * -ignore:
	 * -debug:
	 * -outF:
	 * -noScreen:
	 * -count:
	 * -noP: 
	 * -listF:
	 * -list:
	 * -trusted:
	 * -help/-h:
	 */
	public int PurityOptions(String args[]) {
		if (args==null || args.length<1)
			return -1;
		int filesToLoad = 0;
		int i = 0;
		while (i <= args.length) {
			switch (args[i]) {
			case "-help":
			case "-h":
				return (-i-2);
			case "noneAsPure": case "naPure":
				putOption("noneAsPure");
				break;
			case "-dprog":
				Program.dprog=true;
				break;
			case "-config":
				i++;	
				break;
			case "-nolog":
				Log.doLog=false;
				i++;
				break;
			case "-ignore":
				putOption("allIgnore");
				break;
				
			case "-step":
				putOption("step");
				break;
			case "-MethodTest" : case "-Method":
				putOption("MethodTest");
				break;
				
			case "-debug":
			case "-d":
				PurityUtil.debug=true;
				break;
			case "-outF": case "-outputInOneFile": case "-oneFile":
				oneFile = true;
				break;
			case "-callGraph": case "-cG":
				showCallGraph = true;
				break;
			case "-noScreen":
			case "-noOutputScreen":
				outScreen = false;
				outFile = true;
				break;
			case "-count":
				Program.purityProperties.put("count","1");
				Program.count=true;
				count = true;
				break;
			case "-noP":
				outScreen = true;
				outFile = false;
				break;
			case "-listF":
				list = true;
				filesToLoad = 2;
				break;
			case "-list":
				list = true;
				break;
			case "-trusted":
				filesToLoad = 1;
				update = true;
				break;
			default:
				switch (filesToLoad) {
				case 0:
					return i;
				case 1: {
					if (args[i].startsWith("-"))
						return i;
					File f = new File(args[i]);
					if (f.exists() && args[i].endsWith(".properties")) {
						toLoad.add(new FileInt(f,loadNumber++));
					} else {
						filesToLoad = 0;
					}
					if (args[i].endsWith(".java"))
						return i--;
				}
				break;
				case 2: {
					File f = new File(args[i]);
					filesToLoad = 0;
				}
				break;
				}
				break;
			}
			i++;
		}
		if (filesToLoad!=0){
			writeOut.println("Expected to be provided with .properties to load");
			displayHelp();
			System.exit(0);
		}

		return i;
	}
	
	private void putOption(String key) {
		options.put(key, true);
	}
	
	// Options which expect a methodName
	public int expectMethodName(int i, String[] args, String key) {
		return 0;
	}
	
	//Option which expects a fileName
	public int expectAFileName(int i, String[] args, String ending, String key) {
		return 0;
	}
	
	public int expectManyFileNames(int i, String[] args, String fileEnding, String key) {
		return 0;
	}

	/**
	 * Create a configuration with the default settings/options. 
	 */
	public void CreateConfiguration(String path) {
		try {
			Properties prop = new Properties();
			File file = new File(path);
			file.createNewFile();
			FileOutputStream fos2 = new FileOutputStream(file, false);
			PrintStream ps2 = new PrintStream(fos2, false, "UTF-8");
			prop.setProperty("annotation_package", "lang.ast");
			prop.setProperty("jpure_annotation_package", "jpure.annotations");
			prop.setProperty("step", "0");
			prop.setProperty("outScreen", "1");
			prop.setProperty("outFile", "0");
			prop.setProperty("oneFile", "0");
			prop.setProperty("step", "0");
			prop.setProperty("debug", "0"); //display debug messages!!
			prop.setProperty("externalConstructorsPure", "1");
			prop.setProperty("trusted0", "ExtJPureTrusted0.properties");
			prop.store(ps2, null);
			fos2.close();
			file = new File("FieldsTrusted.properties");
			if (!file.exists())
				file.createNewFile();
		} catch (IOException ex) {
			ex.printStackTrace();
			writeOut.println("Configuration File doesn't exist or is corrupt");
			System.exit(1);
		}
	}

	/**
	 * Initialize the compiler.
	 */
	public JavaPurityChecker() {
		super("ExtendJ Java Purity Checker", ExtendJVersion.getVersion());
	}

	/**
	 * Initialize the compiler from further extension.
	 * @param text
	 * 			The name of the checker extension.
	 * 		
	 */
	public JavaPurityChecker(String text) {
		super(text, ExtendJVersion.getVersion());
	}
	
	/**
	 * @param args
	 *            command-line arguments
	 * @return {@code true} on success, {@code false} on error
	 * @deprecated Use run instead!
	 */
	@Deprecated
	public static boolean compile(String args[]) {
		return 0 == new JavaPurityChecker().run(args);
	}

	/**
	 * Purity Checking the source files.
	 * 
	 * @param args
	 *            command-line arguments
	 * @return 0 on success, 1 on error, 2 on configuration error, 3 on system
	 */
	public int run(String args[]) {
		// ParseArguments

		return run(args, Program.defaultBytecodeReader(), Program.defaultJavaParser());
	}

	@Override
	protected int processCompilationUnit(CompilationUnit unit) {
		if (unit != null && unit.fromSource()) {
			try {
				Collection<Problem> errors = unit.parseErrors();
				if (!errors.isEmpty()) {
					processErrors(errors, unit);
					return EXIT_ERROR;
				}
			} catch (Throwable t) {
				System.err.println("Errors:");
				System.err.format("Fatal exception while processing %s:\n", unit.pathName());
				t.printStackTrace(System.err);
				return EXIT_UNHANDLED_ERROR;
			}
		}
		return EXIT_SUCCESS;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected void processErrors(Collection<Problem> errors, CompilationUnit unit) {
		super.processErrors(errors, unit);
		writeOut.println("Java Errors");
	}

	protected PrintStream OutputFileStream(String fileName, String relativeName)
			throws IOException {
		if (!outFile)
			return null;
		if (!oneFile) {
			File file = new File(fileName + ".txt");
			FileOutputStream fos = new FileOutputStream(file, false);
			return new PrintStream(fos, false, "UTF-8");
		}
		int indexOfLast = relativeName.lastIndexOf("\\");
		String newString = "";
		if (indexOfLast >= 0) {
			newString = relativeName.substring(0, indexOfLast+1);
		}
		File file = new File(newString+"PurityWarnings.txt");
		if (!filecreated) {
			file.delete();
			file.createNewFile();
			writeOut.println(file.getAbsolutePath());
			filecreated = true;
		}
		FileOutputStream fos = new FileOutputStream(file, true);
		return new PrintStream(fos, false, "UTF-8");
	}

	/**
	 * The current operating system beening windows or not.
	 */
	public boolean RunOnWindows(){
		 String OS = System.getProperty("os.name").toLowerCase();
		 return OS.contains("win");
	}
	
	/**
	 * Preform purity checking if no errors was detected during Java parsing
	 */
	@Override
	protected void processNoErrors(CompilationUnit unit) {
		prog=unit.program();
		String relativeName = unit.relativeName().replace("/", "\\");
		String fullName=RunOnWindows() ? relativeName : unit.relativeName();
		int index = relativeName.indexOf("testfiles");
		if (index != -1)
			relativeName = relativeName.substring(index);

		boolean[] writeMode = new boolean[] { outScreen, outFile };
		PrintStream[] PrintStreams = new PrintStream[2];
		PrintStream ps;
		PrintStreams[0] = writeOut;
		try {
			PrintStreams[1] = OutputFileStream(unit.relativeName(), fullName);
			if (writeMode[1] && PrintStreams[1]==null)
				return;
			for (int i = 0; i < 2; i++) {
				if (!writeMode[i])
					continue;
				ps = PrintStreams[i];
				if (Program.AnnotListExternal.hasProblems()){
					ps.println(Program.AnnotListExternal.problemInfo());
				} 
				if (options.containsKey("MethodTest")){
					
				}
				if (showCallGraph) {
					callGraphTask(prog, ps);
				} else {
					generateWarningTask(prog,unit,relativeName,ps);
				}
				ps.flush();
			}
			if (writeMode[1]) {
				PrintStreams[1].flush();
				PrintStreams[1].close();
			}
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}
	
	/**
	 * Check the call graph. The size of it.
	 */
	public void callGraphTask(Program prog, PrintStream ps) {
		CallGraph graph=prog.callGraph();
		ps.println(graph.getCallGraphSizeMessage());
	}
	
	/**
	 * Test a method
	 */
	public void testMethodTask() {
		
	}
	
	/**
	 * Full Deduce completely all the annotations for fields and Lists 
	 * Requires Reseting of State Such That PurityDrops and drops.
	 */
	public void FullIterateWithHeuristics() {
		
	}
	
	private boolean raiseNoCallsWarning=true; 
	/**
	 * Generate warnings for the function.
	 */
	public void generateWarningTask(final Program prog, final CompilationUnit unit, final String relativeName,
			final PrintStream ps) {
		
		if (unit.purityWarningsX().isEmpty() || list) {
			if (raiseNoCallsWarning) {
				final int annotMethod = prog.allPureAnnotMethods().size();
				final CallGraph graph = prog.callGraph();
				final int size = graph.getNoCallers();
				log.print("Simple caller statistic: call graph was:" + graph.getNoCallers());
				log.print(graph.getCallGraphSizeMessage());
				if (annotMethod == 0 && !graph.hasSecrets) {
					log.print("Nothing was calculated. might setup correctly please check options.");
					log.print(" Missing annotations? , imports of SideEffect.*;?");
					log.print("Rules such as @Goal , -noneAsPure, -externalConstructorsPure might be intended.");
					log.print("Ignore if example is empty");
				}
				raiseNoCallsWarning=false;
			}
		} else {

			int noWarns = 0;
			for (final PurityWarnings s : unit.purityWarningsX())
				if (s != null)
					noWarns += s.noWarnings();
			if (noWarns != 0) {
				ps.println(relativeName);
				for (final PurityWarnings s : unit.purityWarningsX())
					if (!count)
						s.printWith(ps, relativeName);
				ps.println(noWarns + " warnings generated");
			}
		
		}
		if (step) {
			final Scanner scan = new Scanner(System.in);
			if (scan.hasNext())
				scan.next();
		}
	}
}
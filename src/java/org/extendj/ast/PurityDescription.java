package org.extendj.ast;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.lang.Integer;
import java.util.Properties;

import org.extendj.ast.PurityAnnotations;
import org.extendj.ast.ASTNode;

 /**
  *  A PurityDescription
  * MethodAnnotations + Parameter Annotations
  */ 
public class PurityDescription {
	public static ArrayList<String> allConstraintFailtures; //Constraint Failtures!!! 
	public String name; // Method Name
	public int fileIndex=-1;
	
	// List with all the purity Annotations for the different part of the method
	public ArrayList<PurityAnnotations> Annotations=new ArrayList<PurityAnnotations>(1); 
	// 0=Method,1=Freshness, 2=parameter0, 3=parameter1 etc
	int[] returnConditionals;
	
	/**
	 * The signature and the purity level, all other information will be default
	 */
	public PurityDescription(String name, int level){
		this.name=name;
		PurityAnnotations mAnnot=new PurityAnnotations();
		mAnnot.annots[level]=true;
		Annotations.add(mAnnot);
	}
	
	public String toString(){
		return name;
	}
	
	public boolean hasAs() {
		return Annotations.get(0).hasFreshAs();
	}
	
	/**
	 * Find the purity Annotation 
	 */
	public PurityAnnotations getParameter(int i){
		if (i+1 >= Annotations.size())
			return PurityAnnotations.Empty; 
		return Annotations.get(i+1);			
	}
	
	public String name(){
		return name;
	}
	
	/**
	 * Check if any problem was detected during parsing
	 */
	public boolean hasProblems(){
		return problems == null;
	}
	
	/**
	 * Checks for any form of 
	 */
	public boolean hasConditionalFresh(){
		for (PurityAnnotations pA: Annotations)
			if (pA.hasFreshIf())
				return true;
		return false;
	}
	
	public String Problem(){
		return problems.toString();
	}
	public StringBuilder problems=null;
	
	// Constructed from String info
	public PurityDescription(String name,int filesIndex, String[] info){
		this.name=name; // For Debug Purpose Only!!
		this.fileIndex=filesIndex;
		PurityAnnotations pa=new PurityAnnotations();
		Annotations.add(pa);
		int num; int parm=0;
		for (String annotations : info){
			String[] annots=annotations.split(",");
			int k=0;
			if (parm==0 && annots[0].startsWith("\"")){
				pa = new PAnnotGroup(annots[0].substring(1));
				k=1;
			}
			for (int i=k; i<annots.length; i++){
				String s=annots[i];
				num=ASTNode.purityStringLevel(s);
				if (num==ASTNode.INVALID){
					 if (problems==null)
						 problems = new StringBuilder();
					 problems.append("file"+filesIndex+" "+name+"=..."+s+System.lineSeparator());
					num=0;
				}
				pa.annots[num]=true;
			}
			parm++;
			if (parm<info.length){
				pa=new PurityAnnotations();	
				Annotations.add(pa);
			}
		}
	}
	
	public void imposeReturnCond(int i){
				
	}
	public void imposeUnCond(int aspect,int level){
		PurityAnnotations pa = Annotations.get(aspect);
		if (level!=-1)
		pa.annots[level]=true;
	}

	public void imposeCond(int aspect,int level,int upperbound){
		PurityAnnotations pa = Annotations.get(aspect);
		int puritylevel = pa.purityLevel();
		int freshlevel = pa.freshnessLevel();
		if (level < 4){
				if (puritylevel==1){
					pa.annots[level]=true;
			}else{
				switch(upperbound){
					case 0:{
						if (level > puritylevel)
							for (int i=1 ; i<=puritylevel;i++)	
								pa.annots[i]=false;
							pa.annots[level]=true;
						return;
					}
					case 1:{
						if (level < puritylevel)
							for (int i=puritylevel ; i>1;i++)	
								pa.annots[i]=false;
							pa.annots[level]=true;
						return;
					}
				}
			}
		}
		//A Freshness annotation simply allow
		pa.annots[level]=true;
	}
	
	/** 
	 * String representation on the form (Annotations p =)
	 */
	public String getValue(){
		StringBuilder s=new StringBuilder();
		int i=Annotations.size();
		for (PurityAnnotations p : Annotations){
				i--;
				s.append(p.toString());
				if( i>0)
					s.append("-");
		}
		return s.toString();
	}
	
	/**
	 *  The signature which identifies this purity Description
	 */
	public String getKey(){
		return name;
	}
}
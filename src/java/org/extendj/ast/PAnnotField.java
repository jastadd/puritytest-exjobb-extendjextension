package org.extendj.ast;
import java.util.Map;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;
import java.lang.Integer;
import java.util.Properties;

// Purity Annotation Method (Store as 8 booleans)
// (Ignore,*Impure,Local,*Pure,NonFresh,*FreshIf, New, Fresh,FreshEx)
 public class PAnnotField extends PurityAnnotations{
	 public static final PAnnotField Local = new PAnnotField(
			 new boolean[]{false,true,false,false,false,false});
	 public static final PAnnotField Fresh = new PAnnotField();
	 public static final PAnnotField Ignore = new PAnnotField();
			 
	 public static final int[] freshAnnot = new int[]{2,3};
	 
 	 public PAnnotField(){
		 annots=new boolean[5];
	 }
	 
	 public PAnnotField(boolean[] annots){
		 this.annots=annots;
	 } 
	 
	 public PAnnotField(String[] annots){
		 int secret=0;
		 this.annots=new boolean[10];
		 if (annots[0].startsWith("")){
			 group=annots[0].substring(1);
			 secret=1;
		 }
		 for (int i=secret;i<annots.length;i++){
			 String s=annots[i];
			 int x = ASTNode.purityStringLevel(s);
			 this.annots[x]=true;	 
		 }
	 } 
	 
 	 private String group;
 	
 	 public String toString(){
 		String s=group!=null ? "with group \"" + group + "\"" : "";
 		 for (int i=0;i<annots.length;i++){
 			 if (annots[i]){
 				if (!s.equals(""))
 					s+=",";
 				 s+=ASTNode.purityLevelString(i);
 			 }
 		 }
 		 return s;
 	 }
 	 
 	 public String Annot_ConsistencyWarnings(){
 		 return "";
 	 }
 	 
 	public PurityAnnotatable getAnnots(){
 		return this;
 	}
 	
 	// Invalid on this type
 	public Modifiers getModifiers(){
 		return null;
 	}
 	 
 	 public int freshnessLevel(){
 	 	for (int i : freshAnnot){
 	 		if (annots[i])
 	 			return i;
 	 	}
 	 	return 1; 
 	 }
 	 
 	 public int purityLevel(){
 	 	 return 1;
 	 }
 	 
 	public String signature(){
 		return "Not Applicable on summary"; 
 	}
	
	public boolean hasAnnot(String annot){
		int i=ASTNode.purityStringLevel(annot);
		if (i<0 || i>=annots.length)
			return i==ASTNode.SECRET ? group!=null : false;
		return annots[i];
	}
	
	public boolean hasAnnot(int a){
		if (a>annots.length)
			return a==ASTNode.SECRET ? group!=null : false;
		return annots[a];
	}
 	 
 	 public boolean hasFreshEx(){
 	 	 return annots[4];
 	 }
 	 public boolean isFresh(){
 	 	 return annots[3];
 	 } 
 	 public boolean hasFresh(){
 	 	 return annots[3];
 	 } 
 	 public boolean hasFreshIf(){
 	 	 return annots[4];
 	 }
 	 public boolean hasNonFresh(){
 	 	 return annots[2]; 
 	 }
	 public boolean isNonFresh(){
 	 	 return annots[2]; 
 	 }
 	 public boolean hasPure(){
 	 	 return false;
 	 }
 	  public boolean hasLocal(){
 	 	 return annots[1];
 	 }
 	 public boolean hasIgnore(){
 	 	 return annots[0];
 	 }
 	 
 	 public boolean hasGroup(String s){
 		 return s.equals(group);
 	 }
 	 
 	 public String isGroup(){
 		 return group!=null ? group : ""; 
 	 }
 	 
 }

package org.extendj.ast;
import java.util.Map;
import java.util.HashMap;
import java.util.Map.Entry;
import java.lang.Integer;
import java.util.Properties;
import java.util.ArrayList;

/* ValuePair-Map Inorder to put Pairs into collections
 * Stores the Greatest Needed Purity For the methods over all contexts. 
 */
public class FieldMap extends HashMap<String,PAnnotField> {
	public ArrayList<String> problems = null;
	public FieldMap(Properties p){
		for (String name: p.stringPropertyNames()){
			String[] d= (String[])((String) p.get(name)).split(",");
			PAnnotField pA = new PAnnotField(d);
		    put(name,pA);
		}
	}
	
	public void loadFile(Properties p,int i){
		for (String name: p.stringPropertyNames()){
			String[] d= (String[])((String) p.get(name)).split(",");
			PAnnotField pA = new PAnnotField(d);
		    put(name,pA);
		}
	}
	
	public FieldMap(int i){
		
	}
	
	public boolean putField(String name,int annot){
		return true;
	}
}
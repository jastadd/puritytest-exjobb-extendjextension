package org.extendj.ast;

import java.util.*;
import java.io.PrintStream;
/** 
* Store relevant callgraph information inform of Callsites and destinations.
* Must be constructed via the CallGraph functions 
*/
public class CallGraph{
	/**
	 *  Number of methods analyze when constructing the call graph and considered 
	 *  by the checker.
	 */
	public static int noallPureAnnotMethods=0;
	public static int totalMethods=0;
	public boolean hasSecrets=false;
	/**
	*The method and all calling call sites. Needed to provide information for debugging 
	* identifing calling contexts such as nta constructiong, from constructor.
	*/
	HashMap<BodyDecl,HashSet<Access>> Callies=new HashMap<BodyDecl,HashSet<Access>>();
			
	/**
	* Part of Data flow graph. Variable and Method change pair for secret datastructures.
	* Should only belong to  
	*/
	HashMap<Variable, Object> destination=new HashMap<Variable, Object>();
	
	/**
	 * Constructor for the call graph. Constructs the call graph for the given MethodList.
	 * @param Methodlist
	 * 		List with methods to include in the call graph. 
	 */
	public CallGraph(ArrayList<BodyDecl> Methodlist) {
		totalMethods=Methodlist.size();
		int noAnnotedMethods=0;
		HashSet<BodyDecl> visited = new HashSet<>();
		visited.addAll(Methodlist);
		for(BodyDecl bd : Methodlist) {
		
			if (bd.getAnnots() != PurityAnnotations.Empty) {
				noAnnotedMethods++;
			}
			for (MethodCall ed : bd.calledMethods()) {
 				addCall(ed.decl(),(Access)ed);
 				if (!visited.contains(ed.decl())){	
 					visited.add(ed.decl());
 					ed.decl().setCalled();
 					callsAdd(ed.decl(), visited);
 				}
			}
		} 
		for(BodyDecl bd : Methodlist) {
			if (!bd.isGroup().equals("")){
				hasSecrets = true;
				for (Variable v: bd.assignedSecrets()) {
					addField(v,bd);
				}
				bd.secretAssigned_reset();
			}
		}
		noallPureAnnotMethods=noAnnotedMethods;
	}
	
	private void callsAdd(BodyDecl bd, HashSet<BodyDecl> visited) {
		for (MethodCall ed : bd.calledMethods()) {
				addCall(ed.decl(),(Access)ed);
				if (!visited.contains(ed.decl())){
 					visited.add(ed.decl());
 					ed.decl().setCalled();
 					callsAdd(ed.decl(), visited);
				}
		}
	}
	
	public Object modifier(Variable v) {
		return destination.get(v);
	}

	/**
	* Obtain all the callers of the given method.
	* @param bd 
	*		The method or constructor for which the callers should be optained.
	*/
	public HashSet<Access> callies(BodyDecl bd){
		return Callies.get(bd);
	}
	
	/**
	* add a destination and a call site
	* @param bd 
	*		The method that performs the call "a". 	
	* @param a 
	*		The call access that is called in the method bd.
	*		
	*/
	public void addCall(BodyDecl bd, Access a){
		HashSet<Access> sm=Callies.get(bd); //previous call site to bd
		if (sm==null)
			sm=new HashSet<Access>();
		sm.add(a);
		Callies.put(bd,sm);
	}
	
	/**
	 * Add the field to the graph
	 * @param v
	 * 		The variable that should be added.
	 * @param bd 
	 * 		The containing method that does the changing.
	 */
	public void addField(Variable v,BodyDecl bd){
		Object sm=destination.containsKey(v) ?
				destination.get(v) : bd; //previous call site to bd
		if (sm!=null && sm!=bd){
			if (sm instanceof BodyDecl){
				BodyDecl bdO = (BodyDecl)sm;
				sm=new HashSet<BodyDecl>(3);
				((HashSet<BodyDecl>)sm).add(bdO);
			}
			((HashSet<BodyDecl>)sm).add(bd);
		}
		destination.put(v,sm);
	}

	public int getNoCallers() {
		return Callies.size();
	}
	public String getCallGraphSizeMessage() {
		int noObjects = Callies.size();
		for (HashSet<Access> calls : Callies.values()) {
			noObjects+= calls.size();
		}
		return "The size of the call graph is " + Callies.size() + 
				" methods assigned form " + noObjects + " locations.";
	}
	
	/**
	* print the CallGraph over all tracked methods.
	*/
	public void printCallGraph(PrintStream ps){
		for (BodyDecl b: Callies.keySet()){
			ps.println(b.puritySignature()+":"+(b.isJastAddAttr() ? b.JastAddattrProp() : "Not Attribute"));
			for (Access a: Callies.get(b))
				ps.println("<-"+((MethodCall)a).decl().puritySignature());
		}
	}
}

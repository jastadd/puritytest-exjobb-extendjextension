package org.extendj.ast;

 public class PAnnotGroup extends PurityAnnotations {
	 private final String group; 
	 
 	 public PAnnotGroup(String group){
		 super();
		 this.group=group;
	 }
	 
	 public PAnnotGroup(boolean[] annots,String group){
		 super(annots);
		 this.group=group;
	 } 
	 
	 public PAnnotGroup(String[] annots,String group){
		 super(annots);
		 this.group=group;
	 } 
	 
 	 public String toString(){
 		String s="with group \"" + group + "\"";
 		 for (int i=0;i<annots.length;i++){
 			 if (annots[i]){
 				if (!s.equals(""))
 					s+=",";
 				 s+=ASTNode.purityLevelString(i);
 			 }
 		 }
 		 return s;
 	 }
 	
 	 
 	@Override public boolean hasAnnot(String annot){
		int i=ASTNode.purityStringLevel(annot);
		if (i<0 || i>=annots.length)
			return i==ASTNode.SECRET ? group!=null : false;
		return annots[i];
	}
 	
 	@Override public boolean hasAnnot(int a){
 		if (a<0 || a>annots.length)
			return a==ASTNode.SECRET ? group!=null : false;
		return annots[a];
	} 
 	 
 	public String signature(){
 		return "Not Applicable on summary"; 
 	}
 	 
 	 public boolean hasGroup(String s){
 		 return s.equals(group);
 	 }
 	 
 	 public String isGroup(){
 		 return group; 
 	 }
 	 
 }

package org.extendj.ast;
import java.util.Map;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;

import org.extendj.ast.ASTNode;

import java.lang.Integer;
import java.util.Properties;

// Purity Annotation for methods (Store as 8 booleans)
// (Ignore,Impure,Local,Pure,NonFresh,FreshIf,New,Fresh,FreshEx, FreshAs)
 public class PurityAnnotations implements PurityAnnotatable{
 	 public boolean[] annots=new boolean[10];
 	 
 	 //The empty annotated 
	 public static final PurityAnnotations Empty = new PurityAnnotations(); 
	 //The static pure annotated
	 public static final PurityAnnotations Pure = new PurityAnnotations().setAnnotation(ASTNode.PURE);
	 //Only fresh
	 public static final PurityAnnotations Fresh = new PurityAnnotations().setAnnotation(ASTNode.FRESH);
	 public static final PurityAnnotations New = new PurityAnnotations().setAnnotation(ASTNode.NEW); // Used When Constructor violates local. 
	 
	 
	 //Keep track of annotations
	 public static final int[] freshAnnot = new int[]{8,7,6,5, 4};
 	 public static final int[] pureAnnot = new int[]{3,2, ASTNode.IGNORE ,1};
	 
 	 // The blank constructor
 	 public PurityAnnotations() {
		 
	 }
	 
	 public PurityAnnotations(boolean[] annots){
		 this.annots=annots;
	 } 
	 

	 
	 /**
	  * Construct purity annotations from annots
	  * @param annots
	  */
	 public PurityAnnotations(String[] annots){
		 for (String s: annots){
			 if (ASTNode.purityStringLevel(s) == ASTNode.INVALID)
				 System.err.println("Error when creating annotation object "+s+" is invalid");
			 this.annots[ASTNode.purityStringLevel(s)]=true;
		 }
	 } 
	 

 	 /**
	  *   Set the 
	  */
	 public PurityAnnotations setAnnotation(int i){
		 annots[i]=true;
		 return this;
	 }
 	 public int numPurityAnnots(){
 		 int sum=0;
 		 for (int i : pureAnnot){
 	 	 	 if(annots[i])
 	 	 	 	 sum++;
 	 	 }
 		 return sum;
 	 }
 	 
 	 public String toString(){
 		String s="";
 		 for (int i=0;i<9;i++){
 			 if (annots[i]){
 				if (!s.equals(""))
 					s+=",";
 				 s+=ASTNode.purityLevelString(i);
 			 }
 		 }
 		 return s;
 	 }
 	 
 	 public String Annot_ConsistencyWarnings(){
 		 return "";
 	 }
 	 
 	public PurityAnnotatable getAnnots(){
 		return this;
 	}
 	
 	// Invalid on this type
 	public Modifiers getModifiers(){
 		return null;
 	}
 	 
 	/**
 	 * Freshness 
 	 * @return
 	 */
 	 public int freshnessLevel(){
 	 	for (int i : freshAnnot){
 	 		if (annots[i])
 	 			return i;
 	 	}
 	 	return 1; 
 	 }
 	 
 	 public int purityLevel(){
 	 	 for (int i : pureAnnot){
 	 	 	 if(annots[i])
 	 	 	 	 return i;
 	 	 }
 	 	 return freshnessLevel()!=0 && !annots[0] ? 1 : 3;
 	 }
 	 
 	public String signature(){
 		return "Not Applicable on summary"; 
 	}
	
	public boolean hasAnnot(String annot){
		int i=ASTNode.purityStringLevel(annot);
		if (i<0 || i>=annots.length)
			return false;
		return annots[i];
	}
	
	public boolean hasAnnot(int a){
		if (a<0 || a>=annots.length)
			return false;
		return annots[a];
	}
 	 
	
	//Obtain the different annotations
	
 	 public boolean hasFreshEx(){
 	 	 return annots[8];
 	 }
 	 public boolean isFresh(){
 	 	 return annots[7];
 	 } 
 	 public boolean hasFresh(){
 	 	 return annots[7];
 	 } 
 	 
 	 public boolean hasNew() {
 		 return annots[6];
 	 }
 	 
 	 public boolean hasFreshIf(){
 	 	 return annots[5];
 	 }
 	 public boolean hasNonFresh(){
 	 	 return annots[4]; 
 	 }
	 public boolean isNonFresh(){
 	 	 return annots[4]; 
 	 }
 	 public boolean hasPure(){
 	 	 return annots[3];
 	 }
 	  public boolean hasLocal(){
 	 	 return annots[2];
 	 }
 	 public boolean hasIgnore(){
 	 	 return annots[0];
 	 }
 	 
 	 public boolean hasFreshAs() {
 		 return annots.length>9 ? annots[9] : false; 
 	 }
 	 // Secret: Annotations
 	 
 	 /**
 	  * If this contains any secret group. 
 	  * @param group
 	  * @return
 	  */
 	 public boolean hasGroup(String group){
 		 return false;
 	 }
 	 
 	 public String isGroup(){
 		 return ""; 
 	 }
 	 
 }

package org.extendj.ast;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

/* ValuePair-Map Inorder to put Pairs into collections
 * Stores the Greatest Needed Purity For the methods over all contexts. 
 */
public class HashMapOne extends HashMap<String,PurityDescription> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public HashMap<String, PurityDescription> oldData; // Contains all old data  
	public ArrayList<String> problems = null; // Stores any problems
	
	/**
	 * Construct a map over all the methods or fields in this properties file.
	 * (String = valuePair).  
	 * @param p - properites map with all Properites
	 * @param i
	 */
	public HashMapOne(Properties p, int i) {
		oldData = new HashMap<String, PurityDescription>();
		for (String name: p.stringPropertyNames()){
			String[] d= (String[])((String) p.get(name)).split("-");
			PurityDescription pd = new PurityDescription(name,i,d);
			if(pd.hasProblems()){
				 if (problems==null)
						 problems = new ArrayList<String>();
				 problems.add(pd.Problem());
			}
		    oldData.put(name,pd);
		}
	}
	
	/**
	 * Check if there are any problems.
	 */
	public boolean hasProblems(){
		return problems != null;
	}
	
	/**
	 * Print all the problems.
	 */
	public String problemInfo() {
		StringBuilder sb = new StringBuilder();
		sb.append("There were "+problems.size()+" problems parsing the external library file");
		for (String s: problems)
			sb.append(s+System.lineSeparator());
		return sb.toString();
	}
	
	/**
	 * Sorts All methods according to class and prints them to properties files.
	 * @return list over all classes so files can be loaded by configuration file.
	 */
	public ArrayList<String> printMethodsPerClassToFiles() {
		ArrayList<String> result = new ArrayList<String>();
		ArrayList<PurityDescription> dataset = new ArrayList<PurityDescription>();
		String currentMethodName="";
		for (String entry : oldData.keySet()){
			int findPar = entry.indexOf('(');
			String theMethod = entry.substring(0,findPar);
			int findLastDot = theMethod.lastIndexOf('.');
			if (findLastDot>0){
				theMethod = theMethod.substring(0, findLastDot);
			}else{
				theMethod="";
			}
			
			if (theMethod.equals(currentMethodName)){
				dataset.add(oldData.get(entry));
			}else{
			//Save file , change dataset ,update currentMethodName) 
			}
		}
		return result;
		
	}
	
	
	public void loadFile(Properties p,int i){
		for (String name: p.stringPropertyNames()){
			String[] d= (String[])((String) p.get(name)).split("-");
			PurityDescription pd = new PurityDescription(name,i,d);
		    oldData.put(name,pd);
		}
	}
	
	public HashMapOne(int i){
		oldData = new HashMap<String,PurityDescription>();
	}
	
	public HashMapOne(){
		oldData = new HashMap<String,PurityDescription>();
	}
	
	public PurityDescription getAll(String key){
		PurityDescription p=oldData.get(key);
		if (p==null)
			return get(key);
		return p;
	}
	
	public boolean putParam(String name,int current, int params,int param, int purity){
		PurityDescription pd= getAll(name);
		if(pd!=null)
			return putEntry(name,params,current);
		pd=getAll(name);
		//pd.impose();
		return true;
	}
	
	// Put Method entry
		public boolean putEntry(String name,int params, int purity) {
		
			return putEntry(name,params,purity,0);
		}
		
		public void putEntryForcing(String name,int params, int purity){
			PurityDescription pd= getAll(name);	
			if (pd != null){
				pd.imposeUnCond(0,purity); //MethodPurity forces the specified puritylevel
				put(name,pd);
				return;
			}
			pd = new PurityDescription(name,purity);
			pd.imposeUnCond(0,purity); 
			put(name, pd);
		}
	
	// Put Method entry
	public boolean putEntry(String name,int params, int purity,int upperbound) {
		if (purity == 1)
			return false;
		int i=0;
		PurityDescription pd= getAll(name);	
		if (pd != null){
			i=purity<4 ? pd.Annotations.get(0).purityLevel() : pd.Annotations.get(0).freshnessLevel();
			pd.imposeCond(0,purity,upperbound); //MethodPurity forces the specified puritylevel
			if (purity != i){
				put(name,pd);
				return true;
			}
		return false;
		}
		pd = new PurityDescription(name,purity);
		PurityDescription in=put(name, pd);
		return  in==null;
	}
}
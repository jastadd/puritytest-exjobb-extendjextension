
package org.extendj;

import org.extendj.ast.HashMapOne;
import org.extendj.ast.CompilationUnit;
import org.extendj.ast.PurityWarnings;
import org.extendj.ast.PurityDescription;
import org.extendj.ast.Frontend;
import org.extendj.ast.BodyDecl;
import org.extendj.ast.MethodDecl;
import org.extendj.ast.ConstructorDecl;
import org.extendj.ast.Problem;
import org.extendj.ast.Program;
import org.extendj.ast.ASTNode;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Map.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;
import java.util.Properties;

import tests.TestPurity;

/**
 * Java Purity Checker : Preforms the Purity Checking and Generate the default
 * properties. Testing / Infering and Statistics collecting is handled by
 * JavaPurityTester and JavaPurityInferer
 */
public class JavaPurityDebug extends JavaPurityChecker {

	/**
	 *  Debug specific objects.
	 */
	public int mode=0;
	public int line=0;
	public String varname="";
	

	private File listOut = new File("ExtJPureTrusted.properties");
	public ArrayList<FileInt> toLoad = new ArrayList<FileInt>(4);
	
	
	/**
	 * Entry point for the compiler frontend.
	 * 
	 * @param args
	 *            command-line arguments
	 */
	public static void main(String args[]) {
		int exitCode;
		Properties prop = new Properties();
		InputStream input = null;
		JavaPurityDebug checker = new JavaPurityDebug();
		int i= 0;
		try{
		checker.ReadConfiguration("");
		i = checker.PurityOptions(args); // Applies the effect of provided
		}catch (IOException e){
			// flags
		}
		try {
			if (checker.update) {
				HashMapOne hMO=new HashMapOne(0);
				for (FileInt fileToLoad: checker.toLoad){
					if (fileToLoad.f.exists()){
						input = new FileInputStream(fileToLoad.f);
						prop.load(input);
						input.close();
						hMO.loadFile(prop,fileToLoad.i);
						prop.clear();
					}
				}
				Program.AnnotListExternal=hMO;
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			System.exit(1);
		}

		if (i != 0) {
			String[] part = new String[args.length - i];
			System.arraycopy(args, i, part, 0, args.length - i);
			exitCode = checker.run(part);
		} else {
			exitCode = checker.run(args);
		}
		if (exitCode != 0) {
			System.exit(exitCode);
		}
	}

	/**
	 * Read the Configuration Provided from ExtJPure.properties
	 */
	public void ReadDebugConfiguration(String path) {
		try {
			if (path.equals(""))
				path="ExtJPure.properties";
			InputStream input = null;
			Properties prop = new Properties();
			File f = new File(path);
			if (!f.exists())
				CreateConfiguration("");
			input = new FileInputStream(path);
			// load a properties file
			prop.load(input);
			// get the property value and print it out
			Program.sideEffectPackage = prop.getProperty("annotation_package");
			Program.jpureSideEffectPackage = prop.getProperty("jpure_annotation_package");
			if (prop.containsKey("externalConstructorsPure"))
				Program.externalConstructorsPure=prop.getProperty("externalConstructorsPure").equals("1");
			step = prop.getProperty("step").equals("1");
			outFile = prop.getProperty("outFile").equals("1");
			outScreen = prop.getProperty("outScreen").equals("1");
			oneFile = prop.getProperty("oneFile").equals("1");
		} catch (IOException ex) {
			ex.printStackTrace();
			System.out.println("Configuration File doesn't exist or is corrupt");
			System.exit(1);
		}
	}

	/**
	 * Provides the tools help information. Instructions on how to use the tool. 
	 */
	public static void displayHelp(){
		System.out.println("This tool provides debug information for the ");
		System.out.println("purity checker.");
		System.out.println("Annotations to check may be provided in source code or in .properties files.");
		System.out.println("Usage: [Purity options][debug options][ExtendJ options] javasources ");
		System.out.println("");
		System.out.println("Tool behaviour and processed files are specified in .properties files or source code");
		System.out.println("Debug.properties contains the setting for which debug information should be provided.");
		System.out.println("If it doesn't exists it will be auto generated when this tool is runned.");
		System.out.println("The settings provided could also be provided via command line a command option -oneFile could be provided as oneFile=1 in the file.");
		System.out.println("Index of all debug options:");
		System.out.println("-showCG: Show the Call Graph");
		System.out.println("-check [Variable]: Check the freshness and status of only one variable.");
		System.out.println("-checkLine [LineNumber]: Check the freshness and status of only one line.");
	}
	
	/**
	 * Options Given On Command Line
	 */
	public int PurityOptions(String args[]) {
		int i = super.PurityOptions(args);
		if (i==-1){
			displayHelp();
			System.exit(0);
		}
		int filesToLoad = 0;
		while (i<args.length) {
			switch (args[i]) {
			case "-showCG": mode=1; break;
			case "-check": mode=2; break;
			case "-checkLine": mode=3; break;
			default:
				switch (filesToLoad) {
				case 0:
					return i;
				case 1: {
					File f = new File(args[i]);
					if (f.exists()) {
						toLoad.add(new FileInt(f,loadNumber));
						filesToLoad = 0;
					} else {
						filesToLoad = 0;
					}
				}
					break;
				case 2: {
					File f = new File(args[i]);
					listOut = f;
					filesToLoad = 0;
				}
					break;
				}
				break;
			}
			i++;
		}
		return i;
	}

	public void CreateDebugConfiguration() {
		try {
			Properties prop = new Properties();
			File file = new File("Debug.properties");
			file.createNewFile();
			FileOutputStream fos2 = new FileOutputStream(file, false);
			PrintStream ps2 = new PrintStream(fos2, false, "UTF-8");
			prop.setProperty("annotation_package", "lang.ast");
			prop.setProperty("jpure_annotation_package", "jpure.annotations");
			prop.setProperty("step", "0");
			prop.setProperty("outScreen", "1");
			prop.setProperty("outFile", "0");
			prop.setProperty("oneFile", "0");
			prop.setProperty("step", "0");
			prop.setProperty("trusted0", "ExtJPureTrusted.properties");
			prop.store(ps2, null);
			fos2.close();
		} catch (IOException ex) {
			ex.printStackTrace();
			System.out.println("Configuration File doesn't exist or is corrupt");
			System.exit(1);
		}
	}

	/**
	 * Initialize the compiler.
	 */
	public JavaPurityDebug() {
		super("ExtendJ Java Purity Debug");
	}

	/**
	 * @param args
	 *            command-line arguments
	 * @return {@code true} on success, {@code false} on error
	 * @deprecated Use run instead!
	 */
	@Deprecated
	public static boolean compile(String args[]) {
		return 0 == new JavaPurityChecker().run(args);
	}

	/**
	 * Purity Checking the source files.
	 * 
	 * @param args
	 *            command-line arguments
	 * @return 0 on success, 1 on error, 2 on configuration error, 3 on system
	 */
	public int run(String args[]) {
		// ParseArguments

		return run(args, Program.defaultBytecodeReader(), Program.defaultJavaParser());
	}

	@Override
	protected int processCompilationUnit(CompilationUnit unit) {
		if (unit != null && unit.fromSource()) {
			try {
				Collection<Problem> errors = unit.parseErrors();
				if (!errors.isEmpty()) {
					processErrors(errors, unit);
					return EXIT_ERROR;
				}
			} catch (Throwable t) {
				System.err.println("Errors:");
				System.err.format("Fatal exception while processing %s:\n", unit.pathName());
				t.printStackTrace(System.err);
				return EXIT_UNHANDLED_ERROR;
			}
		}
		return EXIT_SUCCESS;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected void processErrors(Collection<Problem> errors, CompilationUnit unit) {
		super.processErrors(errors, unit);
		System.out.println("Java Errors");
	}

	protected PrintStream OutputFileStream(boolean oneFile,CompilationUnit unit,String relativeName) throws IOException{
		if (!outFile)
			return null;
		if (!oneFile) {
			File file = new File(unit.relativeName() + ".debug");
			FileOutputStream fos = new FileOutputStream(file, false);
			return new PrintStream(fos, false, "UTF-8");
		}
		int indexOfLast = relativeName.lastIndexOf("\\");
		String newString = "";
		if (indexOfLast >= 0)
			newString = relativeName.substring(0, indexOfLast);
		File file = new File(newString + "\\PurityWarnings.debug");
		if (!filecreated) {
			file.delete();
			file.createNewFile();
			filecreated = true;
		}
		FileOutputStream fos = new FileOutputStream(file, true);
		return new PrintStream(fos, false, "UTF-8");
	}

	@Override
	protected void processNoErrors(CompilationUnit unit) {
		prog=unit.program();
		String relativeName = unit.relativeName().replace("/", "\\");
		int index=relativeName.indexOf("testfiles");
		if (index!=-1)
		relativeName=relativeName.substring(index);
		boolean[] writeMode = new boolean[] { outScreen, outFile };
		PrintStream[] PrintStreams = new PrintStream[2];
		PrintStream ps;
		PrintStreams[0] = System.out;
			try {
				PrintStreams[1] = OutputFileStream(oneFile,unit,relativeName);
				for (int i = 0; i < 2; i++) {
					if (!writeMode[i])
						continue;
					ps = PrintStreams[i];
					ps.flush();
					if (prog.AnnotListExternal.hasProblems()){
						ps.println(prog.AnnotListExternal.problemInfo());
					} else {
						if(mode==0){
							if (unit.purityWarningsX().isEmpty()) {
							} else {
								ps.println(relativeName);
								int noWarns=0;
								for (PurityWarnings s : unit.purityWarningsX()) {
									s.printWith(ps,relativeName);
									noWarns+=s.noWarnings();
								}	
								ps.println(noWarns + " warnings generated");
							}
							if (step) {
								Scanner scan = new Scanner(System.in);
								scan.next();
							}
						}
					  if(mode==1){
						  prog.callGraph().printCallGraph(ps);
					  }
					}
				}
				if (writeMode[1]){
				PrintStreams[1].flush();
				PrintStreams[1].close();
				}
			} catch (IOException e) {
				e.printStackTrace(System.err);
			}
	}
}
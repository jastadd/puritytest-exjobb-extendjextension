package tests;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import org.extendj.JavaPurityChecker;
import java.io.OutputStream;

/**
 * A parameterized test suite. Adds helper methods for parameterized testing.
 */
abstract public class AbstractParameterizedTest extends AbstractTestSuite {

	/**
	 * The time out time for waiting for the Purity Checker to Complete the test
	 */

	private static final int TEST_TIMEOUT = 35000;

	/**
	 * File extension for test input files. EDIT ME
	 */
	protected static final String IN_EXTENSION = ".java";
	/**
	 * Test output is written to a file with this extension
	 */
	private static final String OUT_EXTENSION = ".java.txt";
	/**
	 * File extension for expected test output
	 */
	private static final String EXPECTED_EXTENSION = ".expected";

	/**
	 * @param testDirectory
	 * @param testFile
	 */
	public AbstractParameterizedTest(String testDirectory, String testFile) {
		super(testDirectory);
	}
	/**
	 * TestRunner For The PurityTests return 0 = test failed return 1 = Success!
	 */

	public static int testPurity(String filename, File folder,boolean count,boolean debug,boolean prog , boolean noPrint, boolean replace) {
		try {
			File[] Files = folder.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.toLowerCase().endsWith(".java");
				}
			});
			File[] Trusted = folder.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.toLowerCase().endsWith(".properties") &&
							!name.equals("ExtJPure.properties") && 
							!name.equals("FieldsTrusted.properties");
				}
			});
			ArrayList<String> files = new ArrayList<>();
			for (File file : Files) {
				files.add(file.getCanonicalPath());
			}
			ArrayList<String> trustedfiles = new ArrayList<>();
			for (File file : Trusted) {
				trustedfiles.add(file.getCanonicalPath());
			}

			boolean hasTrusted = Trusted.length != 0;
			if (files.isEmpty()) {
				System.out.print("Nothing to test in folder" + folder.toPath());
				return 0;
			}

			ArrayList<String> commandL = new ArrayList<String>();
			boolean hasConfig = new File(folder.getCanonicalPath() + File.separator + "ExtJPure.properties").exists(); 
			if (hasConfig){
				commandL.add("-config");
				commandL.add(folder.getCanonicalPath()+File.separator+"ExtJPure.properties");
			}
			commandL.add("-nolog");
			
			if (count){
				commandL.add("-count");
			}
			if (debug){
				commandL.add("-debug");
			}
			if (noPrint) {
				commandL.add("-noP");
			}
			if (files.size() > 1){
				commandL.add("-outF");
			}
			if (hasTrusted && hasConfig) {
				commandL.add("-trusted");
				commandL.addAll(trustedfiles);
			}
			commandL.add("-classpath");
			commandL.add("beaver-rt.jar");
			commandL.add("testfiles" + File.separator + "PurityTests" + File.separator + "SideEffect.java");
			commandL.addAll(files);
			
			String[] command = commandL.toArray(new String[0]);
			OutputStream out = new ByteArrayOutputStream();
			OutputStream errOut = new ByteArrayOutputStream();
			String commandString = Arrays.toString(command);
			commandString = commandString.substring(1, commandString.length()-1).replace(',', ' ');
			ProcessThread pthread = new ProcessThread("java -jar puritytest-exjobb-extendjextension.jar "+ commandString,
					out, errOut);
			
			pthread.start();
			long timestart = System.currentTimeMillis();
			pthread.join(TEST_TIMEOUT);
			long duration = System.currentTimeMillis() - timestart;
			synchronized (System.out){ 
				System.out.println(filename+":time:"+duration+"ms");
			}
			 int exitValue =0;
			if (!pthread.processFinished) {
				synchronized (System.out){ 
					System.out.println(out.toString());
					System.out.println(errOut.toString());
				}
				pthread.interrupt();
				pthread.killProcess();

				System.out.println("Timed out while waiting for test execution (timeout = " + TEST_TIMEOUT + ")");
				
				if (TestPurity.failsafe)
					exitValue=testerfailSafe(command,filename,folder);
				synchronized (System.out){ 
					System.out.println(out.toString());
					System.out.println(errOut.toString());
				}
				return 0;
			}
			if (exitValue==0)
			exitValue = pthread.exitCode;
			if (exitValue == 0) {
				int result = compareOutput(out.toString(), getTestOutputFile(filename,folder,files.size()==1),
						getTestExpectedOutputFile(filename,folder,files.size()==1));
				
				if (result == 0) {
					if(!noPrint) {
						System.out.println(out.toString());
					}
					System.out.print(errOut.toString());
					if (files.size() == 1)
					getTestOutputFile(filename,folder,files.size() == 1).delete();
					FileWriter fp = new FileWriter(getTestOutputFile(filename,folder,files.size()==1));
					fp.write(out.toString());
					fp.close();
					
				}
				return result == 1 ? 1 : 0;
			}
			System.out.println("Exitcode "+exitValue);
			
		} catch (InterruptedException e) {
			System.out.println("Interrupted while waiting for test process.");
		} catch (IOException e) {
			System.out.println("Can't write out");
		}
		// JavaPurityChecker.main(command);
		// compareOutput(readFileToString(getTestOutputFile(filename)),getTestOutputFile(filename),
		// getTestExpectedOutputFile(filename) );
		return 0;
	}

	
	/** OutPut all info to Screen stop after first fail
	 * 
	 */
	
	protected static int testerfailSafe(String[] command,String filename,File folder){
	try{
	 JavaPurityChecker.main(command);
	 int b=compareOutput(readFileToString(getTestOutputFile(filename,folder,true)),getTestOutputFile(filename,folder,true),
		 getTestExpectedOutputFile(filename,folder,true));
	 if(b==0)
		System.exit(-1);
	 return b;
	}catch (Exception e){
		e.printStackTrace();
		System.out.println(filename + " failed");
		System.exit(-1);
	}
	 return 0;
	}
		
	protected static File getTestInputFile(String filename,File folder,boolean files) throws IOException{
		return new File(folder.getCanonicalPath(), filename);
	}

	protected static File getTestOutputFile(String filename,File folder,boolean files) throws IOException{
		if (files)
		return new File(folder.getCanonicalPath(), filename + ".java.txt");
		return new File(folder.getCanonicalFile(),"PurityProblems.result");
	}

	protected static File getTestExpectedOutputFile(String filename,File folder,boolean files) throws IOException {
		File file = new File(folder.getCanonicalPath(),filename + ".expected");
		if (!files)
		file =new File(folder.getCanonicalFile(),"PurityProblems.expected");
		file.createNewFile();
		return file;
	}

	protected static File getTestFile(String filename,File folder,boolean files) throws IOException {
		return new File(folder.getCanonicalPath(),filename + ".java");
	}


	@SuppressWarnings("javadoc")
	public static Iterable<Object[]> getTestParameters(String testDirectory) {
		Collection<Object[]> tests = new LinkedList<Object[]>();
		File testDir = new File(testDirectory);
		if (!testDir.isDirectory()) {
			throw new Error("Could not find '" + testDirectory + "' directory!");
		}
		for (File f : testDir.listFiles()) {
			if (f.getName().endsWith(IN_EXTENSION)) {
				tests.add(new Object[] { f.getName() });
			}
		}
		return tests;
	}
}

package tests;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.util.Scanner;


/**
 * Utility methods for running tests
 */
abstract class AbstractTestSuite {
	private static String SYS_LINE_SEP = System.getProperty("line.separator");

	public AbstractTestSuite(String testDirectory) {
	}

	/**
	 * Parses the given file
	 * @param file
	 * @return parser result, if everything went OK
	 * @throws IOException
	 * @throws Exception
	 */
	protected static Object parse(File file) throws IOException, Exception {
		return null;
	}
	
	/**
	 * Check that the string matches the contents of the given file.
	 * Also writes the actual output to file.
	 * @param actual actual output
	 * @param out file where output should be written
	 * @param expected file containing expected output
	 */
	
	private static boolean compareText(File text,String text2)  throws FileNotFoundException {
		Scanner scanner = new Scanner(readFileToString(text));
		Scanner refScanner = new Scanner(normalizeText(text2));
		while (scanner.hasNextLine() && refScanner.hasNextLine()) {
			String lineA=scanner.nextLine();
			String lineB=refScanner.nextLine();
			if (!lineA.equals(lineB)) {
				scanner.close();
				refScanner.close();
				System.err.println(lineA +" :Not:" + lineB);
				return false;
			}
		}
		boolean res =(scanner.hasNextLine() || refScanner.hasNextLine());
		scanner.close();
		refScanner.close();
		return res;
	}
	
	/**
	 * Compare the output from the test with the refernce (expected)
	 * @param actual actual result of the test
	 * @param out file to store result in
	 * @param expected the refernce to compare with
	 * @return
	 */
	protected static int compareOutput(String actual,File out, File expected) {
		try {
				if(!compareText(expected,normalizeText(actual))){
				System.err.println("Test of "+ out.getName() + "failed");
				return 0;
				}
				
		} catch (IOException e) {
			System.out.println("IOException occurred while comparing output: " + e.getMessage());
		}
		return 1;
	}

	/**
	 * Reads an entire file to a string object.
	 * <p>If the file does not exist an empty string is returned.
	 * <p>The system dependent line separator char sequence is replaced by
	 * the newline character.
	 *
	 * @param file
	 * @return normalized text from file
	 * @throws FileNotFoundException
	 */
	public static String readFileToString(File file) throws FileNotFoundException {
		if (!file.isFile()) {
			return "";
		}

		Scanner scanner = new Scanner(file);
		scanner.useDelimiter("\\Z");
		String text = normalizeText(scanner.hasNext() ? scanner.next() : "");
		scanner.close();
		return text;
	}

	/**
	 * Trim whitespace and normalize newline characters to be only \n
	 * @param text
	 * @return normalized text
	 */
	protected static String normalizeText(String text) {
		return text.replace(SYS_LINE_SEP, "\n").trim();
	}

}

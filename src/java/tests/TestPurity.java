package tests;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import org.extendj.JavaPurityChecker;
import org.extendj.JavaPurityDebug;

@SuppressWarnings("javadoc")
public class TestPurity extends AbstractParameterizedTest {
	/**
	 * Constructor for the automatic purity tests.
	 * Delegates to the actual test preformer by giving the testfolder to the
	 * super class.
	 */
	public TestPurity() {
		super("testfiles" + File.separator + "PurityTests", "SecretExam"); 
		//where test input files are placed here.
	}
	
	/**
	 * The options for the testing  
	 */
	public static int notests; //store total number of tests
	public static boolean debug=false;//show debug information
	public static boolean prog=false;//show progress debug information
	public static boolean failsafe=false;
	public static boolean noPrint=true; // print normal test output to console.
	public static boolean failstop=false;//run in main thread after failure in subthread.
	public static boolean createConf=false; // create new configs.
	//stop as fast as possible after a failure.
	
	//Update tests:
	public static boolean replace=false; //Replace Successful
	
	public static int threads=4;
	//number of threads
	
	public static short[] passes=new short[threads];
	//store result from each thead.
	public static LinkedList<String[]> tests = null;
	public static ArrayList<String> failedTests=new ArrayList<String>();
	// lock object
	public static final TestPurity lock=new TestPurity(); 
	// has a test failed.
	public static boolean failed=false;
	private static boolean wait; 

	
	/**
	 * Main method for the purity testing. 
	 * Parses options before delegating to the actual testing via start(x,x) 
	 * The options are explained in the help(x) method.
	 */
	public static void main(String[] args) {
		String test="";
		boolean count=true;
		for (int i=0;i<args.length;i++){
			switch(args[i].trim()){
				case "-run" : {
					String[] newArgs=new String[args.length-i];
					System.arraycopy(args,i+1,newArgs,0,newArgs.length);
				    JavaPurityChecker.main(newArgs);
				    System.exit(0);
				}
				case "-d+": {
					String[] newArgs=new String[args.length-i];
					System.arraycopy(args,i+1,newArgs,0,newArgs.length);
					JavaPurityDebug.main(newArgs);
				    System.exit(0);
				}
				case "-thread": threads=Integer.parseInt(args[i+1]); break;
				case "-nofs": failsafe=false; break;
				case "-fs": failsafe=true; break;
				case "-d": debug=true; break;
				case "-prog": prog=true; break;
				
				case "-print": 
				case "-p":noPrint=false; 
					break;
				
				case "-wait": wait=true; break;

				case "-replace": replace=true; break;
				case "-createConfig" : createConf=true; break;
				case "-noc": count=false; break; 	
				case "-c": count=true; break;
				case "-fstop" : failstop=true; break;
				case "": break;
				default: {
					if (args[i].startsWith("-")){
						System.out.println("Option :" + args[i] + " is unknown.");
						help(""); System.exit(-2);
					}
					test=args[i]; break;
				}
			}
		}
		start(test,count);
	}
	
	/**
	 * Present help information in case of faulty arguments or if the specified 
	 * tested folder can't be found.
	 * @param test
	 * 			Subpath to the testfolder starting from the default location.
	 * 			Only relative path from initized location.
	 */
	public static void help(String test){
		if (!test.equals("")){
			System.out.println("The test case folder "+test+" couln't be found");
			System.out.println("");
		}
		System.out.println("Tester for the purity checker");
		System.out.println("-run: switch to the purity checker with remaining commands passed");
		System.out.println("to the purity checker." );
		System.out.println("-d+: switch to the debug checker with remaining commands passed");
		System.out.println("to the debug checker." );
		System.out.println("-nofs:(default) no failsafe run of frozen/timeouted test cases.");
		System.out.println("-fs: failsafe run of frozen/timeouted test cases.");
		System.out.println("-d: display debug info from failed test cases.");
		System.out.println("-prog: display progress debug info from test cases.");
		System.out.println("-noc: display debug info from failed test cases.");
		System.out.println("-c:(default) don't display debug info from failed test cases.");
		System.out.println("-print / p: display normal test output to console.");
		System.out.println("-fstop: stop the testing after one test failure.");
		System.out.println("[testfolder]: only run the tests in the subfolders to [testfolder]."
		+"Example ArrayOk\\ to run the ArrayOk tests. Must be the last argument. ");
	}

	/**
	 * Start the testing process by collecting all the tests,
	 * starting threads and wait for results. 
	 * Finally display number of cleared tests as fraction of total.
	 * @param test  
	 * 			Subpath to the testfolder starting from the default location.
	 * @param count 
	 * 			Obtion for if the warnings should be counted or displayed. 
	 */
	public static void start(String test,boolean count) {
		int sthreads=threads;
		try{
			
			collect(test);	
			System.out.println("Tests: "+notests);
			if (notests==1)
				sthreads=1;
			TestPurity tp=new TestPurity();
			Thread[] workers = new Thread[sthreads];
			for (int x=0;x<sthreads;x++){
				workers[x]= tp.new Worker(x,count);
				workers[x].start();
			}
			for (int x=0;x<sthreads;x++)
				workers[x].join();
		}catch(NullPointerException e){
			help(test);
			System.exit(-1);
		}catch(IOException IE){
			IE.printStackTrace();
			System.exit(-1);
		}catch (InterruptedException e){
			e.printStackTrace();
			System.exit(-1);
		}catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
		int total=0; 
		for (int j=0;j<sthreads;j++)
			total+=passes[j];
		System.out.println("Failed test summary:");
		for (String s: failedTests)
			System.out.println(s);
		System.out.println("Test result: "+total+ "/"+notests);
	}

	/**
	 * Helper method that preforms test collecting storing them in a list for every thread. 
	 * The tests will then be runned by worker threads inorder to achive parallism 
	 * and a faster testing process.
	 * @param test 
	 * 			Subpath to the testfolder starting from the default location.
	 */
	public static void collect(String test)  throws IOException, Exception{
		tests=new LinkedList<String[]>();
		File[] fList = null;
		File fileTop=new File("testfiles" + File.separator + "PurityTests");
		if (!test.equals("")) 
			fileTop = new File("testfiles" + File.separator + "PurityTests" + File.separator + test);
		collect(fileTop);
	}
	
	/**
	 * Search for all tests (folders with Java).
	 */
	public static void collect(File directory) throws IOException, Exception{
		boolean here=false;
		boolean propertyFile=false;
		File[] fList = directory.listFiles();
		for (File file : fList) {	
			if (file.isFile() && !here && file.getName().endsWith(".java")) {
				notests++;
				here=true;
				tests.add(new String[]{directory.getName(), directory.getCanonicalPath()});
			} else if (file.isDirectory()) {
				collect(file);
			} else if (file.isFile()  && file.getName().endsWith(".properties")) {
				propertyFile=true;
			}
		}
		
		if (here) {
			if (!propertyFile) {
				if (!createConf) {
				//	throw new Exception("Missing a property File at" + directory.getAbsolutePath());
				}
			}
		}
	}
	
	/**
	 * Inner worker thread that preforms tests. 
	 */
	private class Worker extends Thread {
		int id; // which number belongs to this thread.
		boolean count; //count errors or not
	
		/**
		 * A worker thread that preforms a set of tests. 
		 * @param id
		 * 			The number associated with this thread. 
		 * 			Needed to store number of cleared tests without monitors.
		 * @param count
		 * 			Obtion for if the warnings should be counted or displayed.
		 * 
		 */
		public Worker(int id,boolean count) {
			this.id=id;
			this.count=count;
		}

		/**
		 * Actually run a testing thread. 
		 * Pops test from the TestPurity.tests list and preform test until 
		 * all tests run or stopped by test failure if that setting is used.
		 */
		public void run() {
			short localpasses = 0;
			String[] s=null;
			while(!failed){
				synchronized(lock) {
					if (tests.isEmpty()){
						failed=true;
						break;
					}
					s = tests.pop();
				}
				int result = testPurity(s[0], new File(s[1]),count, debug, prog, noPrint , replace);
				if (result != 1){
					System.out.println("failed:"+s[0]);
					TestPurity.failedTests.add(s[0]);
					if (TestPurity.failstop){
						failed=true;
						TestPurity.passes[id] = localpasses;
						return;
					}
				}
				localpasses += result;
			}
			TestPurity.passes[id] = localpasses;
		}
	}
}
